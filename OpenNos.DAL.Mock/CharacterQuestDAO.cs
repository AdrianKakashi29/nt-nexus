﻿using OpenNos.DAL.Interface;
using System;
using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Mock
{
    public class CharacterQuestDAO : ICharacterQuestDAO
    {
        public DeleteResult Delete(long characterId, long questId)
        {
            throw new NotImplementedException();
        }

        public CharacterQuestDTO InsertOrUpdate(CharacterQuestDTO character)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CharacterQuestDTO> LoadByCharacterId(long characterId)
        {
            throw new NotImplementedException();
        }
    }
}
