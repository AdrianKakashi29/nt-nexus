﻿using OpenNos.DAL.Interface;
using System;
using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Mock
{
    public class QuestObjectiveDAO : IQuestObjectiveDAO
    {
        public QuestObjectiveDTO Insert(QuestObjectiveDTO questObjective)
        {
            throw new NotImplementedException();
        }

        public void Insert(List<QuestObjectiveDTO> questObjectives)
        {
            throw new NotImplementedException();
        }

        public List<QuestObjectiveDTO> LoadAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<QuestObjectiveDTO> LoadByQuestId(long questId)
        {
            throw new NotImplementedException();
        }
    }
}
