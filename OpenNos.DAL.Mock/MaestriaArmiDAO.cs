﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.Mock
{
    public class MaestriaArmiDAO : BaseDAO<MaestriaArmiDTO>, IMaestriaArmiDAO
    {
        #region Methods

        public DeleteResult Delete(short Vnum, short CharacterId)
        {
            throw new NotImplementedException();
        }

        public void Insert(List<MaestriaArmiDTO> MaestriaArmis)
        {
            throw new NotImplementedException();
        }

        public MaestriaArmiDTO LoadById(short MaestriArmiId)
        {
            throw new NotImplementedException();
        }

        public MaestriaArmiDTO LoadByVnumAndCharacterId(short Vnum, long CharacterId)
        {
            throw new NotImplementedException();
        }

        public MaestriaArmiDTO Update(MaestriaArmiDTO maestriaArmi)
        {
            throw new NotImplementedException();
        }


        #endregion
    }
}