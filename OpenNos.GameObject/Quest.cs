﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Data;
using System;
using System.Collections.Generic;
using OpenNos.GameObject.Networking;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using System.Linq;

namespace OpenNos.GameObject
{
    public class Quest : QuestDTO
    {
        #region Instantiation

        public Quest()
        {

        }

        #endregion

        #region Properties

        public List<QuestObjectiveDTO> QuestObjectives { get; set; }

        public List<QuestRewardDTO> QuestRewards { get; set; }

        #endregion

        #region Methods

        public string GetRewardPacket(Character character)
        {
            if (!QuestRewards.Any())
            {
                return string.Empty;
            }

            return $"qr {GetRewardPacket()} {InfoId}";

            string GetRewardPacket()
            {
                string str = "";
                for (int a = 0; a < 4; a++)
                {
                    QuestRewardDTO reward = QuestRewards.Skip(a).FirstOrDefault();
                    if (reward == null)
                    {
                        str += "0 0 0 ";
                        continue;
                    }
                    switch ((QuestRewardType)reward.RewardType)
                    {
                        // Item
                        case QuestRewardType.WearItem:
                        case QuestRewardType.EtcMainItem:
                            int rewarda = reward.Data;

                            if (reward.Data == 901)
                            {
                                switch (character.Class)
                                {
                                    case ClassType.Archer:
                                        rewarda = 903;
                                        break;
                                    case ClassType.Magician:
                                        rewarda = 905;
                                        break;
                                    case ClassType.Fighter:
                                        rewarda = 4486;
                                        break;
                                }
                            }
                            if (reward.Data == 902)
                            {
                                switch (character.Class)
                                {
                                    case ClassType.Archer:
                                        rewarda = 904;
                                        break;
                                    case ClassType.Magician:
                                        rewarda = 906;
                                        break;
                                    case ClassType.Fighter:
                                        rewarda = 4485;
                                        break;
                                }
                            }
                            if (reward.Data == 909)
                            {
                                switch (character.Class)
                                {
                                    case ClassType.Archer:
                                        rewarda = 911;
                                        break;
                                    case ClassType.Magician:
                                        rewarda = 913;
                                        break;
                                    case ClassType.Fighter:
                                        rewarda = 4437;
                                        break;
                                }
                            }
                            if (reward.Data == 910)
                            {
                                switch (character.Class)
                                {
                                    case ClassType.Archer:
                                        rewarda = 912;
                                        break;
                                    case ClassType.Magician:
                                        rewarda = 914;
                                        break;
                                }
                            }

                            if (rewarda >= 901 && rewarda <= 914)
                            {
                                character.InsertSpLog(rewarda);
                            }

                            if (reward.Data == 5000)
                            {
                                character.Session.Character.AddBuff(new Buff(378, character.Session.Character.SwitchLevel()), duration: 30);
                            }

                            character.GiftAdd((short)rewarda, (byte)(reward.Amount == 0 ? 1 : reward.Amount), reward.Design, reward.Upgrade, (sbyte)reward.Rarity, true);
                            str += $"{reward.RewardType} {rewarda} {(reward.Amount == 0 ? 1 : reward.Amount)} ";
                            break;

                        // Gold
                        case QuestRewardType.Gold:
                        case QuestRewardType.SecondGold:
                        case QuestRewardType.ThirdGold:
                        case QuestRewardType.FourthGold:
                            character.Gold += reward.Amount;
                            character.Session.SendPacket(character.GenerateGold());
                            str += $"{reward.RewardType} 0 {(reward.Amount == 0 ? 1 : reward.Amount)} ";
                            break;

                        case QuestRewardType.Reput: // Reputation
                            character.Reputation += reward.Amount;
                            str += $"{reward.RewardType} 0 0";
                            break;

                        case QuestRewardType.Exp: // Experience
                            if (character.SwitchLevel() >= ServerManager.Instance.Configuration.MaxLevel)
                            {
                                str += "0 0 0 ";
                                break;
                            }
                            character.AddLevelXp((long)(CharacterHelper.XPData[reward.Data > 255 ? 255 : reward.Data] * reward.Amount / 100D));
                            str += $"{reward.RewardType} 0 0 ";
                            break;

                        case QuestRewardType.SecondExp: // % Experience
                            if (character.SwitchLevel() >= ServerManager.Instance.Configuration.MaxLevel)
                            {
                                str += "0 0 0 ";
                                break;
                            }
                            character.AddLevelXp((long)(CharacterHelper.XPData[character.SwitchLevel()] * reward.Amount / 100D));
                            str += $"{reward.RewardType} 0 0 ";
                            break;

                        case QuestRewardType.JobExp: // JobExperiencem
                            character.JobLevelXp += (long)((character.Class == (byte)ClassType.Adventurer ? CharacterHelper.FirstJobXPData[reward.Data > 255 ? 255 : reward.Data] : CharacterHelper.SecondJobXPData[reward.Data > 255 ? 255 : reward.Data]) * reward.Amount / 100D);
                            str += $"{reward.RewardType} 0 0 ";
                            break;

                        case QuestRewardType.SecondJobExp: // % JobExperience
                            character.JobLevelXp += (long)((character.Class == (byte)ClassType.Adventurer ? CharacterHelper.FirstJobXPData[character.JobLevel] : CharacterHelper.SecondJobXPData[character.JobLevel]) * reward.Amount / 100D);
                            str += $"{reward.RewardType} 0 0 ";
                            break;

                        default:
                            str += "0 0 0 ";
                            break;
                    }
                }
                return str;
            }
        }

        public string TargetPacket()
        {
            return $"target {TargetX} {TargetY} {TargetMap} {QuestId}";
        }

        public string RemoveTargetPacket()
        {
            return $"targetoff {TargetX} {TargetY} {TargetMap} {QuestId}";
        }

        #endregion

    }
}