﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.Master.Library.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OpenNos.GameObject.Networking;
using OpenNos.Core.Extensions;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Globalization;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.GameObject.Networking;


namespace OpenNos.GameObject
{
    public static class NRunHandler
    {
        #region Methods


        // This presumes that weeks start with Monday.
        // Week 1 is the 1st week of the year with a Thursday in it.
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static void NRun(ClientSession Session, NRunPacket packet)
        {
            if (!Session.HasCurrentMapInstance)
            {
                return;
            }
            MapNpc npc = Session.CurrentMapInstance.Npcs.Find(s => s.MapNpcId == packet.NpcId);
            TeleporterDTO tp;
            Random rand = new Random();
            switch (packet.Runner)
            {
                //case 1:
                //    if (Session.Character.Class != (byte)ClassType.Adventurer)
                //    {
                //        return;
                //    }

                //    if (packet.Type == (byte)Session.Character.Class)
                //    {
                //        return;
                //    }

                //    if (packet.Type > (byte)ClassType.Magician)
                //    {
                //        ServerManager.Instance.Kick(Session.Character.Name);
                //        PenaltyLogDTO log = new PenaltyLogDTO
                //        {
                //            AccountId = Session.Character.AccountId,
                //            Reason = "PacketLogger",
                //            Penalty = PenaltyType.Banned,
                //            DateStart = DateTime.Now,
                //            DateEnd = DateTime.Now.AddYears(15),
                //            AdminName = "Administrator"
                //        };
                //        Character.InsertOrUpdatePenalty(log);
                //        return;
                //    }

                //    if (Session.Character.Inventory.All(i => i.Type != InventoryType.Wear))
                //    {
                //        if (Session.Character.Gold < costo)
                //        {
                //            Session.SendPacket(
                //                Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MONEY"),
                //                    10));
                //            return;
                //        }
                //        else
                //        {
                //            Session.Character.Gold -= costo;
                //            Session.SendPacket(Session.Character.GenerateGold());
                //        }

                //        ItemInstance inv = new ItemInstance();
                //        inv = Session.Character.Inventory
                //            .AddNewToInventory(packet.Type == 4 ? (short)4719 : (short)(4 + (packet.Type * 14)),
                //                type: InventoryType.Wear, Rare: 7, Upgrade: 10).FirstOrDefault();
                //        ItemInstance wearable = Session.Character.Inventory.LoadBySlotAndType(inv.Slot, inv.Type);
                //        Session.Character.Inventory.AddNewToInventory(884, 1);
                //        Session.Character.Inventory.AddNewToInventory(885, 1);
                //        Session.Character.Inventory.AddNewToInventory(886, 1);
                //        Session.Character.Inventory.AddNewToInventory(887, 1);
                //        wearable.SetRarityPoint(Session);
                //        if (packet.Type != 4)
                //        {
                //            inv = Session.Character.Inventory.AddNewToInventory((short)(81 + (packet.Type * 13)),
                //                type: InventoryType.Wear, Rare: 7, Upgrade: 10).FirstOrDefault();
                //            wearable = Session.Character.Inventory.LoadBySlotAndType(inv.Slot, inv.Type);
                //            wearable.SetRarityPoint(Session);
                //        }

                //        switch (packet.Type)
                //        {
                //            case 1:
                //                inv = Session.Character.Inventory
                //                    .AddNewToInventory(68, type: InventoryType.Wear, Rare: 7, Upgrade: 10)
                //                    .FirstOrDefault();
                //                Session.Character.Inventory.AddNewToInventory(2082, 10);
                //                Session.Character.Inventory.AddNewToInventory(901, Rare: 21, Upgrade: 5);
                //                wearable = Session.Character.Inventory.LoadBySlotAndType(inv.Slot, inv.Type);
                //                wearable.SetRarityPoint(Session);
                //                break;

                //            case 2:
                //                inv = Session.Character.Inventory
                //                    .AddNewToInventory(78, type: InventoryType.Wear, Rare: 7, Upgrade: 10)
                //                    .FirstOrDefault();
                //                Session.Character.Inventory.AddNewToInventory(2083, 10);
                //                Session.Character.Inventory.AddNewToInventory(903, Rare: 21, Upgrade: 5);
                //                wearable = Session.Character.Inventory.LoadBySlotAndType(inv.Slot, inv.Type);
                //                wearable.SetRarityPoint(Session);
                //                break;

                //            case 3:
                //                inv = Session.Character.Inventory
                //                    .AddNewToInventory(86, type: InventoryType.Wear, Rare: 7, Upgrade: 10)
                //                    .FirstOrDefault();
                //                wearable = Session.Character.Inventory.LoadBySlotAndType(inv.Slot, inv.Type);
                //                Session.Character.Inventory.AddNewToInventory(905, Rare: 21, Upgrade: 5);
                //                wearable.SetRarityPoint(Session);
                //                break;

                //            case 4:
                //                inv = Session.Character.Inventory
                //                    .AddNewToInventory(4737, type: InventoryType.Wear, Rare: 7, Upgrade: 10)
                //                    .FirstOrDefault();
                //                wearable = Session.Character.Inventory.LoadBySlotAndType(inv.Slot, inv.Type);
                //                wearable.SetRarityPoint(Session);
                //                break;
                //        }

                //        Session.CurrentMapInstance?.Broadcast(Session.Character.GenerateEq());
                //        Session.SendPacket(Session.Character.GenerateEquipment());
                //        Session.Character.ChangeClass((ClassType)packet.Type);
                //    }
                //    else
                //    {
                //        Session.SendPacket(
                //            UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("EQ_NOT_EMPTY"), 0));
                //    }
                case 1:
                    if (packet.Type > 4 || packet.Type < 1)
                    {
                        return;
                    }
                    if (Session.Character.Class != (byte)ClassType.Adventurer)
                    {
                        Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NOT_ADVENTURER"), 0));
                        return;
                    }
                    if (packet.Type == (byte)Session.Character.Class || packet.Type > 3 && Session.Account.Authority < AuthorityType.GameMaster || packet.Type < 0)
                    {
                        return;
                    }

                    if (Session.Character.Inventory.All(i => i.Type != InventoryType.Wear))
                    {
                        Session.Character.Inventory.AddNewToInventory((short)(4 + (packet.Type * 14)), type: InventoryType.Wear);
                        Session.Character.Inventory.AddNewToInventory((short)(81 + (packet.Type * 13)), type: InventoryType.Wear);
                        switch (packet.Type)
                        {
                            case 1:
                                Session.Character.Inventory.AddNewToInventory(68, type: InventoryType.Wear);
                                Session.Character.Inventory.AddNewToInventory(2082, 10);
                                break;

                            case 2:
                                Session.Character.Inventory.AddNewToInventory(78, type: InventoryType.Wear);
                                Session.Character.Inventory.AddNewToInventory(2083, 10);
                                break;

                            case 3:
                                Session.Character.Inventory.AddNewToInventory(86, type: InventoryType.Wear);
                                break;
                        }
                        Session.CurrentMapInstance?.Broadcast(Session.Character.GenerateEq());
                        Session.SendPacket(Session.Character.GenerateEquipment());
                        Session.Character.ChangeClass((ClassType)packet.Type);
                    }
                    else
                    {
                        Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("EQ_NOT_EMPTY"), 0));
                    }
                    break;

                case 2:
                    Session.SendPacket("wopen 1 0");
                    break;

                //case 300:
                //    if (npc == null)
                //    {
                //        return;
                //    }

                //    Session.Character.AddQuest(6040);
                //    break;

                //case 65:
                //    if (npc == null)
                //    {
                //        return;
                //    }

                //    Session.Character.AddQuest(5514);
                //    break;
                //case 66:
                //    if (npc == null)
                //    {
                //        return;
                //    }

                //    Session.Character.AddQuest(5914);
                //    break;
                //case 67:
                //    if (npc == null)
                //    {
                //        return;
                //    }

                //    Session.Character.AddQuest(5908);
                //    break;
                //case 194:
                //    {
                //        if (npc != null)
                //        {
                //            if (Session.Character.Inventory.CountItem(5986) < 3)
                //            {
                //                Session.SendPacket(
                //                    UserInterfaceHelper.GenerateInfo(
                //                        Language.Instance.GetMessageFromKey("NO_SCHEGGIA_ANIMA")));
                //                return;
                //            }
                //            else
                //            {
                //                ItemInstance PolverePurificazione = Session.Character.Inventory
                //                    .AddNewToInventory(5984, 3, Rare: 0, Upgrade: 0).FirstOrDefault();
                //                if (PolverePurificazione != null)
                //                {

                //                    Item iteminfo = ServerManager.GetItem(5984);
                //                    // pdti {WindowType} {inv.ItemVNum} {recipe.Amount} {Unknown} {inv.Upgrade} {inv.Rare}
                //                    Session.SendPacket(
                //                        $"pdti 11 {PolverePurificazione.ItemVNum} {3} 29 {PolverePurificazione.Upgrade} {PolverePurificazione.Rare}");
                //                    Session.SendPacket(Session.Character.GenerateSay(
                //                        $"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: {iteminfo.Name} x 3",
                //                        12));
                //                    Session.Character.Inventory.RemoveItemAmount(5986, 3);
                //                }
                //            }
                //        }
                //    }
                //    break;
                //case 195:
                //    {
                //        if (npc != null)
                //        {
                //            if (Session.Character.Inventory.CountItem(5986) < 3)
                //            {
                //                Session.SendPacket(
                //                    UserInterfaceHelper.GenerateInfo(
                //                        Language.Instance.GetMessageFromKey("NO_SCHEGGIA_ANIMA")));
                //                return;
                //            }
                //            else
                //            {
                //                ItemInstance SemeDannazione = Session.Character.Inventory
                //                    .AddNewToInventory(5987, 1, Rare: 0, Upgrade: 0).FirstOrDefault();
                //                if (SemeDannazione != null)
                //                {

                //                    Item iteminfo = ServerManager.GetItem(5987);
                //                    // pdti {WindowType} {inv.ItemVNum} {recipe.Amount} {Unknown} {inv.Upgrade} {inv.Rare}
                //                    Session.SendPacket(
                //                        $"pdti 11 {SemeDannazione.ItemVNum} {1} 29 {SemeDannazione.Upgrade} {SemeDannazione.Rare}");
                //                    Session.SendPacket(Session.Character.GenerateSay(
                //                        $"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: {iteminfo.Name} x 1",
                //                        12));
                //                    Session.Character.Inventory.RemoveItemAmount(5986, 3);
                //                }
                //            }
                //        }
                //    }
                //    break;

                //case 193:
                //    {
                //        if (npc != null)
                //        {
                //            if (Session.Character.Inventory.CountItem(5987) < 5)
                //            {
                //                Session.SendPacket(
                //                    UserInterfaceHelper.GenerateInfo(
                //                        Language.Instance.GetMessageFromKey("NO_SEMI_DANNAZIONE")));
                //                return;
                //            }
                //            else
                //            {
                //                ItemInstance SigilloLaurena = Session.Character.Inventory
                //                    .AddNewToInventory(5977, 2, Rare: 0, Upgrade: 0).FirstOrDefault();
                //                if (SigilloLaurena != null)
                //                {

                //                    Item iteminfo = ServerManager.GetItem(5977);
                //                    // pdti {WindowType} {inv.ItemVNum} {recipe.Amount} {Unknown} {inv.Upgrade} {inv.Rare}
                //                    Session.SendPacket(
                //                        $"pdti 11 {SigilloLaurena.ItemVNum} {2} 29 {SigilloLaurena.Upgrade} {SigilloLaurena.Rare}");
                //                    Session.SendPacket(Session.Character.GenerateSay(
                //                        $"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: {iteminfo.Name} x 2",
                //                        12));
                //                    Session.Character.Inventory.RemoveItemAmount(5987, 5);
                //                }
                //            }
                //        }
                //    }
                //    break;

                case 3000:
                    if (npc == null)
                    {
                        return;
                    }
                    else if (npc.MapNpcId == 8208)
                    {
                        //Session.SendPacket("it 3");
                        if (ServerManager.Instance.ChannelId == 51)
                        {
                            ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 1, 80, 117);
                            Thread.Sleep(1000);
                            ServerManager.Instance.Kick(Session.Character.Name);
                        }
                        else
                        {
                            Console.WriteLine("test5");
                        }
                    }
                    else if (npc.MapNpcId == 15000)
                    {
                        ItemInstance fairy1 =
                            Session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Fairy,
                                InventoryType.Wear);

                        if (fairy1 != null)
                        {
                            if (fairy1.ItemVNum == 4129 || fairy1.ItemVNum == 4130 || fairy1.ItemVNum == 4131 ||
                                     fairy1.ItemVNum == 4132)
                            {
                                if (fairy1.ElementRate == 30)
                                {
                                    fairy1.ElementRate = 100;
                                }
                                else
                                {
                                    Session.SendPacket(Session.Character.GenerateSay(
                                        $"{Language.Instance.GetMessageFromKey("FAIRY_NOT_80")}", 10));
                                }
                            }
                            else if (fairy1.ItemVNum == 4706 || fairy1.ItemVNum == 4707 || fairy1.ItemVNum == 4708 ||
                                     fairy1.ItemVNum == 4705)
                            {
                                if (fairy1.ElementRate == 80)
                                {
                                    fairy1.ElementRate = 120;
                                }
                                else
                                {
                                    Session.SendPacket(Session.Character.GenerateSay(
                                        $"{Language.Instance.GetMessageFromKey("FAIRY_NOT_80")}", 10));
                                }
                            }
                            else if (fairy1.ItemVNum == 4709 || fairy1.ItemVNum == 4710 || fairy1.ItemVNum == 4711 ||
                                     fairy1.ItemVNum == 4712)
                            {
                                if (fairy1.ElementRate == 80)
                                {
                                    fairy1.ElementRate = 160;
                                }
                                else
                                {
                                    Session.SendPacket(Session.Character.GenerateSay(
                                        $"{Language.Instance.GetMessageFromKey("FAIRY_NOT_80")}", 10));
                                }
                            }
                            else if (fairy1.ItemVNum == 4713 || fairy1.ItemVNum == 4714 || fairy1.ItemVNum == 4715 ||
                                     fairy1.ItemVNum == 4716)
                            {
                                if (fairy1.ElementRate == 80)
                                {
                                    fairy1.ElementRate = 200;
                                }
                                else
                                {
                                    Session.SendPacket(Session.Character.GenerateSay(
                                        $"{Language.Instance.GetMessageFromKey("FAIRY_NOT_80")}", 10));
                                }
                            }
                            else
                            {
                                Session.SendPacket(Session.Character.GenerateSay(
                                    $"{Language.Instance.GetMessageFromKey("NOT_SUPPORTED_FAIRY")}", 10));
                            }
                        }
                        else
                        {
                            Session.SendPacket(
                                Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("FAIRY_NULL")}",
                                    10));
                        }

                    }
                    else if (npc.MapNpcId == 8339)
                    {
                        //Session.SendPacket("it 3");
                        if (ServerManager.Instance.ChannelId == 51)
                        {
                            ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 1, 80, 117);
                            ServerManager.Instance.Kick(Session.Character.Name);
                        }
                        else
                        {
                            Console.WriteLine("test5");
                        }
                    }
                    else if (npc.MapNpcId == 15007) // sp2
                    {
                        Session.Character.AddQuest(2014);
                    }
                    else if (npc.MapNpcId == 15008) // sp3
                    {
                        Session.Character.AddQuest(2071);
                    }
                    else if (npc.MapNpcId == 15009) // sp4
                    {
                        Session.Character.AddQuest(2126);
                    }
                    else if (npc.MapNpcId == 15010) // sp5
                    {
                        Session.Character.AddQuest(5956);
                    }
                    else if (npc.MapNpcId == 15011) // sp6
                    {
                        Session.Character.AddQuest(5987);
                    }
                    else if (npc.MapNpcId == 15023)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 43, 12, 178);
                    }
                    else if (npc.MapNpcId == 15024)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 44, 12, 9);
                    }
                    else if (npc.MapNpcId == 15020)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 42, 162, 86);
                    }
                    else if (npc.MapNpcId == 15025)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 43, 162, 8);
                    }
                    else if (npc.MapNpcId == 15022)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 42, 162, 165);
                    }
                    else if (npc.MapNpcId == 15021)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 42, 162, 64);
                    }
                    else if (npc.MapNpcId == 15165)
                    {
                        int randomquest = ServerManager.RandomNumber(1, 4);

                        if (randomquest == 1)
                        {
                            Session.Character.AddQuest(5515);
                        }
                        else if (randomquest == 2)
                        {
                            Session.Character.AddQuest(5905);
                        }
                        else if (randomquest == 3)
                        {
                            Session.Character.AddQuest(5906);
                        }
                        else if (randomquest == 4)
                        {
                            Session.Character.AddQuest(5908);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else if (npc.MapNpcId == 15205)
                    {
                                        Session.SendPacket(UserInterfaceHelper.GenerateMsg("Castle Zombie will be open in next week!", 0));
                                        Session.SendPacket(Session.Character.GenerateSay("Castle Zombie will be open in next week!", 10));
                        //if (DateTime.Now.Hour >= 18 && DateTime.Now.Hour <= 19)
                        //{
                        //        if (ServerManager.Instance.ZombieCastleMembers.All(s => s.Session != Session))
                        //        {
                        //            if (ServerManager.Instance.IsCharacterMemberOfGroup(Session.Character.CharacterId))
                        //            {
                        //                Session.SendPacket(UserInterfaceHelper.GenerateMsg("You are in group!", 0));
                        //                Session.SendPacket(Session.Character.GenerateSay("You are in group!", 10));
                        //            }
                        //            else
                        //            {
                        //                ServerManager.Instance.ZombieCastleMembers.Add(new ZombieCastleMember
                        //                {
                        //                    ArenaType = EventType.ZOMBIECASTLE,
                        //                    Session = Session,
                        //                    GroupId = null,
                        //                    Time = 0
                        //                });
                        //            }
                        //        }
                        //}
                        //else
                        //{
                        //    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CASTLE_ZOMBIE_OPEN"), 0));
                        //}
                    }
                    else if (npc.MapNpcId == 15192)
                    {
                        Session.SendPacket(Session.Character.GenerateGB(3));
                        Session.SendPacket(Session.Character.GenerateSMemo(6, "A warm welcome to the Cuarry Bank. You can deposit or withdraw from 1,000 to 100 billion units of gold."));
                    }
                    else if (npc.MapNpcId == 15166)
                    {
                        GameObject.Event.PTS.GeneratePTS(1805, Session);
                    }
                    else if (npc.MapNpcId == 15167)
                    {
                        GameObject.Event.PTS.GeneratePTS(1824, Session);
                    }
                    else if (npc.MapNpcId == 1898)
                    {
                        Session.SendPacket("wopen 7 0");
                    }
                    else if (npc.MapNpcId == 15203)
                    {
                        Session.SendPacket("guri 18 1");
                    }
                    else if (npc.MapNpcId == 15168)
                    {
                        GameObject.Event.PTS.GeneratePTS(1825, Session);
                    }
                    else if (npc.MapNpcId == 15169)
                    {
                        GameObject.Event.PTS.GeneratePTS(1826, Session);
                    }
                    else if (npc.MapNpcId == 15222) // tarot
                    {
                        GameObject.Event.DROPMAP_TAROT.GenerateDROPMAP_TAROT(1, Session);
                    }
                    else if (npc.MapNpcId == 15182)
                    {
                        //GameObject.Event.HEROMAP.GenerateHEROMAP(1805, Session);
                        GameObject.Event.HEROEXPMAP.GenerateHEROEXPMAP(1805, Session);
                    }
                    else if (npc.MapNpcId == 15198) // 30
                    {
                        if (Session.Character.HeroLevel >= 30)
                        {
                            GameObject.Event.HEROEXPMAP30.GenerateHEROEXPMAP30(1, Session);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else if (npc.MapNpcId == 15199) // 60
                    {
                        if (Session.Character.HeroLevel >= 60)
                        {
                            GameObject.Event.HEROEXPMAP60.GenerateHEROEXPMAP60(1, Session);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else if (npc.MapNpcId == 15200) // 90
                    {
                        if (Session.Character.HeroLevel >= 90)
                        {
                            GameObject.Event.HEROEXPMAP90.GenerateHEROEXPMAP90(1, Session);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else if (npc.MapNpcId == 15201) // 120
                    {
                        if (Session.Character.HeroLevel >= 120)
                        {
                            GameObject.Event.HEROEXPMAP120.GenerateHEROEXPMAP120(1, Session);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else if (npc.MapNpcId == 15202) // 150
                    {
                        if (Session.Character.HeroLevel >= 150)
                        {
                            GameObject.Event.HEROEXPMAP150.GenerateHEROEXPMAP150(1, Session);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else if (npc.MapNpcId == 15204) // 180
                    {
                        if (Session.Character.HeroLevel >= 180)
                        {
                            GameObject.Event.HEROEXPMAP180.GenerateHEROEXPMAP180(1, Session);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else if (npc.MapNpcId == 15184)
                    {
                        if (Session.Character.Inventory.CountItem(5959) >= 15)
                        {
                            switch (Session.Character.Class)
                            {
                                case ClassType.Adventurer:
                                    return;

                                case ClassType.Fighter:
                                    return;

                                case ClassType.Archer:
                                    Session.Character.GiftAdd(4492, 1);
                                    Session.Character.Inventory.RemoveItemAmount(5959, 15);

                                    return;

                                case ClassType.Swordman:
                                    Session.Character.GiftAdd(4493, 1);
                                    Session.Character.Inventory.RemoveItemAmount(5959, 15);

                                    return;

                                case ClassType.Magician:
                                    Session.Character.GiftAdd(4491, 1);
                                    Session.Character.Inventory.RemoveItemAmount(5959, 15);

                                    return;
                            }
                        }
                        else
                        {
                            Session.SendPacket(Session.Character.GenerateSay(
                                $"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: Caligor's Spirit", 10));
                        }
                    }
                    else if (npc.MapNpcId == 15186)
                    {
                        if (Session.Character.Inventory.CountItem(5983) >= 15)
                        {
                            switch (Session.Character.Class)
                            {
                                case ClassType.Adventurer:
                                    return;

                                case ClassType.Fighter:
                                    return;

                                case ClassType.Archer:
                                    Session.Character.GiftAdd(4488, 1);
                                    Session.Character.Inventory.RemoveItemAmount(5983, 15);
                                    return;

                                case ClassType.Swordman:
                                    Session.Character.GiftAdd(4489, 1);
                                    Session.Character.Inventory.RemoveItemAmount(5983, 15);
                                    return;

                                case ClassType.Magician:
                                    Session.Character.GiftAdd(4487, 1);
                                    Session.Character.Inventory.RemoveItemAmount(5983, 15);
                                    return;
                            }
                        }
                        else
                        {
                            Session.SendPacket(Session.Character.GenerateSay(
                                $"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: Laurena's Spirit", 10));
                        }
                    }
                    else if (npc.MapNpcId == 15187)
                    {
                        if (Session.Character.Inventory.CountItem(2523) >= 15)
                        {
                            switch (Session.Character.Class)
                            {
                                case ClassType.Adventurer:
                                    return;

                                case ClassType.Fighter:
                                    Session.Character.GiftAdd(4485, 1);
                                    Session.Character.Inventory.RemoveItemAmount(2523, 15);

                                    return;

                                case ClassType.Archer:
                                    Session.Character.GiftAdd(4498, 1);
                                    Session.Character.Inventory.RemoveItemAmount(2523, 15);

                                    return;

                                case ClassType.Swordman:
                                    Session.Character.GiftAdd(4497, 1);
                                    Session.Character.Inventory.RemoveItemAmount(2523, 15);

                                    return;

                                case ClassType.Magician:
                                    Session.Character.GiftAdd(4499, 1);
                                    Session.Character.Inventory.RemoveItemAmount(2523, 15);

                                    return;
                            }
                        }
                        else
                        {
                            Session.SendPacket(Session.Character.GenerateSay(
                                $"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: Glacerus's Spirit", 10));
                        }
                    }
                    else if (npc.MapNpcId == 15188)
                    {
                        if (Session.Character.Inventory.CountItem(2522) >= 15)
                        {
                            switch (Session.Character.Class)
                            {
                                case ClassType.Adventurer:
                                    return;

                                case ClassType.Fighter:
                                    Session.Character.GiftAdd(4486, 1);
                                    Session.Character.Inventory.RemoveItemAmount(2522, 15);

                                    return;

                                case ClassType.Archer:
                                    Session.Character.GiftAdd(4501, 1);
                                    Session.Character.Inventory.RemoveItemAmount(2522, 15);

                                    return;

                                case ClassType.Swordman:
                                    Session.Character.GiftAdd(4500, 1);
                                    Session.Character.Inventory.RemoveItemAmount(2522, 15);

                                    return;

                                case ClassType.Magician:
                                    Session.Character.GiftAdd(4502, 1);
                                    Session.Character.Inventory.RemoveItemAmount(2522, 15);

                                    return;
                            }
                        }
                        else
                        {
                            Session.SendPacket(Session.Character.GenerateSay(
                                $"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: Draco's Spirit", 10));
                        }
                    }
                    else if (npc.MapNpcId == 15191)
                    {
                        if (Session.Character.Faction == FactionType.Demon)
                        {
                            Session.Character.Ach3 = 2;
                            Session.SendPacket($"qna #guri^test Congratulations you got Demon Faction icon!");


                        }
                        else if (Session.Character.Faction == FactionType.Angel)
                        {
                            Session.Character.Ach3 = 1;
                            Session.SendPacket($"qna #guri^test Congratulations you got Angel Faction icon!");

                        }
                        else
                        {
                            return;
                        }
                    }
                    else if (npc.MapNpcId == 15193) // fm
                    {
                        if (Session.Character.Level >= 55)
                        {
                            GameObject.Event.DROPMAP_FM.GenerateDROPMAP_FM(1, Session);
                        }

                        //Session.Character.GeneralLogs.Add(new GeneralLogDTO
                        //{
                        //    AccountId = Session.Account.AccountId,
                        //    CharacterId = Session.Character.CharacterId,
                        //    IpAddress = Session.IpAddress,
                        //    LogData = "DROPMAP_FM",
                        //    LogType = Session.Character.Name,
                        //    Timestamp = DateTime.Now
                        //});

                    }
                    else if (npc.MapNpcId == 15194) // piorka
                    {
                        if (Session.Character.Level >= 55)
                        {
                            GameObject.Event.DROPMAP_ANGELS.GenerateDROPMAP_ANGELS(1, Session);

                        }

                        //Session.Character.GeneralLogs.Add(new GeneralLogDTO
                        //{
                        //    AccountId = Session.Account.AccountId,
                        //    CharacterId = Session.Character.CharacterId,
                        //    IpAddress = Session.IpAddress,
                        //    LogData = "DROPMAP_ANGELS",
                        //    LogType = Session.Character.Name,
                        //    Timestamp = DateTime.Now
                        //});

                    }
                        else if (npc.MapNpcId == 15195) // souls
                        {
                        if (Session.Character.Level >= 55)
                        {
                            GameObject.Event.DROPMAP_SOULS.GenerateDROPMAP_SOULS(1, Session);

                        }


                        //Session.Character.GeneralLogs.Add(new GeneralLogDTO
                        //{
                        //    AccountId = Session.Account.AccountId,
                        //    CharacterId = Session.Character.CharacterId,
                        //    IpAddress = Session.IpAddress,
                        //    LogData = "DROPMAP_SOULS",
                        //    LogType = Session.Character.Name,
                        //    Timestamp = DateTime.Now
                        //});
                    }
                        else if (npc.MapNpcId == 15196) // fairy
                        {
                        if (Session.Character.Level >= 55)
                        {
                            GameObject.Event.FAIRYMAP.GenerateFAIRYMAP(1, Session);

                        }


                        //Session.Character.GeneralLogs.Add(new GeneralLogDTO
                        //{
                        //    AccountId = Session.Account.AccountId,
                        //    CharacterId = Session.Character.CharacterId,
                        //    IpAddress = Session.IpAddress,
                        //    LogData = "DROPMAP_FAIRY",
                        //    LogType = Session.Character.Name,
                        //    Timestamp = DateTime.Now
                        //});
                    }
                        else if (npc.MapNpcId == 15197)
                        {
                            GameObject.Event.JOBMAP.GenerateJOBMAP(1, Session);
                        }
                    else if (npc.MapNpcId == 20000)
                    {
                        Session.SendPacket(npc?.GenerateSay("Wow, this Server is cool!", 10));
                    }
                    else if (npc.MapNpcId == 15206)
                    {
                        if (Session.Character.Family != null)
                        {
                            if (Session.Character.Family.FamilyLevel >= 3 && Session.Character.Family.MaxSize == 20)
                            {
                                if (Session.Character.Gold >= 10000000)
                                {
                                    Session.Character.Gold -= 10000000;
                                    Session.SendPacket(Session.Character.GenerateGold());
                                    Session.Character.Family.MaxSize = 30;
                                    FamilyDTO fam = Session.Character.Family;
                                    DAOFactory.FamilyDAO.InsertOrUpdate(ref fam);
                                    ServerManager.Instance.FamilyRefresh(Session.Character.Family.FamilyId);
                                }
                                else
                                {
                                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("FAM_GOLD_3LVL"), 0));
                                }
                            }
                            else if (Session.Character.Family.FamilyLevel >= 6 && Session.Character.Family.MaxSize == 30)
                            {
                                if (Session.Character.Gold >= 100000000)
                                {
                                    Session.Character.Gold -= 100000000;
                                    Session.SendPacket(Session.Character.GenerateGold());
                                    Session.Character.Family.MaxSize = 40;
                                    FamilyDTO fam = Session.Character.Family;
                                    DAOFactory.FamilyDAO.InsertOrUpdate(ref fam);
                                    ServerManager.Instance.FamilyRefresh(Session.Character.Family.FamilyId);
                                }
                                else
                                {
                                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("FAM_GOLD_6LVL"), 0));

                                }
                            }
                            else if (Session.Character.Family.FamilyLevel >= 10 && Session.Character.Family.MaxSize == 40)
                            {
                                if (Session.Character.Gold >= 500000000)
                                {
                                    Session.Character.Gold -= 500000000;
                                    Session.SendPacket(Session.Character.GenerateGold());
                                    Session.Character.Family.MaxSize = 50;
                                    FamilyDTO fam = Session.Character.Family;
                                    DAOFactory.FamilyDAO.InsertOrUpdate(ref fam);
                                    ServerManager.Instance.FamilyRefresh(Session.Character.Family.FamilyId);
                                }
                                else
                                {
                                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("FAM_GOLD_10LVL"), 0));
                                }
                            }
                            else
                            {
                                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SMALL_FAMILY_LEVEL_OR_YOU_HAVE_UPGRADE"), 0));
                            }
                        }
                        else
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("YOU_NEED_FAMILY"), 0));
                        }
                    }
                        else
                        {
                            return;
                        }
                    break;


                //case 68:
                //    if (npc == null)
                //    {
                //        return;
                //    }
                //    Session.Character.AddQuest(5919);
                //    break;
                case 4:
                    Mate mate = Session.Character.Mates.Find(s => s.MateTransportId == packet.NpcId);
                    switch (packet.Type)
                    {
                        case 2:
                            if (mate != null)
                            {
                                if (Session.Character.SwitchLevel() >= mate.Level)
                                {
                                    Mate teammate = Session.Character.Mates.Where(s => s.IsTeamMember).FirstOrDefault(s => s.MateType == mate.MateType);
                                    if (teammate != null)
                                    {
                                        teammate.IsTeamMember = false;
                                        Session.SendPacket(Session.Character.GenerateSki());
                                        Session.SendPackets(Session.Character.GenerateQuicklist());
                                        teammate.MapX = teammate.PositionX;
                                        teammate.MapY = teammate.PositionY;
                                    }
                                    mate.IsAlive = true;
                                    mate.Hp = mate.HpLoad();
                                    mate.Mp = mate.MpLoad();
                                    mate.IsTeamMember = true;
                                    Session.SendPacket(Session.Character.GenerateSki());
                                    Session.SendPackets(Session.Character.GenerateQuicklist());
                                }
                                else
                                {
                                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PET_HIGHER_LEVEL"), 0));
                                }
                            }
                            break;

                        case 3:
                            if (mate != null && Session.Character.Miniland == Session.Character.MapInstance)
                            {
                                mate.IsTeamMember = false;
                                Session.SendPacket(Session.Character.GenerateSki());
                                Session.SendPackets(Session.Character.GenerateQuicklist());
                                mate.MapX = mate.PositionX;
                                mate.MapY = mate.PositionY;
                            }
                            break;
                        case 4:
                            if (mate != null)
                            {
                                if (Session.Character.Miniland == Session.Character.MapInstance)
                                {
                                    mate.IsTeamMember = false;
                                    Session.SendPacket(Session.Character.GenerateSki());
                                    Session.SendPackets(Session.Character.GenerateQuicklist());
                                    mate.MapX = mate.PositionX;
                                    mate.MapY = mate.PositionY;
                                }
                                else
                                {
                                    Session.SendPacket($"qna #n_run^4^5^3^{mate.MateTransportId} {Language.Instance.GetMessageFromKey("ASK_KICK_PET")}");
                                }
                                break;
                            }
                            break;

                        case 5:
                            if (mate != null)
                            {

                                if (Session.Character.MapInstance.Map.MapTypes.Any(m => m.MapTypeId == (short)MapTypeEnum.Act4))
                                {
                                    Session.SendPacket(UserInterfaceHelper.GenerateDelay(3000, 10, $"#n_run^4^6^3^{mate.MateTransportId}"));
                                }
                                else
                                {

                                    Session.SendPacket(UserInterfaceHelper.GenerateDelay(100, 10, $"#n_run^4^6^3^{mate.MateTransportId}"));
                                }
                            }
                            break;

                        case 6:
                            if (mate != null && Session.Character.Miniland != Session.Character.MapInstance)
                            {
                                mate.IsTeamMember = false;
                                Session.SendPacket(Session.Character.GenerateSki());
                                Session.SendPackets(Session.Character.GenerateQuicklist());
                                Session.CurrentMapInstance.Broadcast(mate.GenerateOut());
                                Session.SendPacket(Session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("PET_KICKED"), mate.Name), 11));
                                Session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("PET_KICKED"), mate.Name), 0));
                            }
                            break;

                        case 7:
                            if (mate != null)
                            {
                                if (Session.Character.Mates.Any(s => s.MateType == mate.MateType && s.IsTeamMember))
                                {
                                    Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("ALREADY_PET_IN_TEAM"), 11));
                                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ALREADY_PET_IN_TEAM"), 0));
                                }
                                else
                                {


                                    if (Session.Character.MapInstance.Map.MapTypes.Any(m => m.MapTypeId == (short)MapTypeEnum.Act4))
                                    {
                                        Session.SendPacket(UserInterfaceHelper.GenerateDelay(3000, 10, $"#n_run^4^9^3^{mate.MateTransportId}"));
                                    }
                                    else
                                    {
                                        Session.SendPacket(UserInterfaceHelper.GenerateDelay(100, 10, $"#n_run^4^9^3^{mate.MateTransportId}"));
                                    }
                                }
                            }
                            break;

                        case 9:
                            if (mate != null && Session.Character.SwitchLevel() >= mate.Level)
                            {
                                mate.PositionX = (short)(Session.Character.PositionX + (mate.MateType == MateType.Partner ? -1 : 1));
                                mate.PositionY = (short)(Session.Character.PositionY + 1);

                                mate.IsAlive = true;
                                mate.Hp = mate.HpLoad();
                                mate.Mp = mate.MpLoad();
                                mate.IsTeamMember = true;
                                Session.SendPacket(Session.Character.GenerateSki());
                                Session.SendPackets(Session.Character.GenerateQuicklist());
                                Session.CurrentMapInstance?.Broadcast(mate.GenerateIn());
                            }
                            else
                            {
                                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PET_HIGHER_LEVEL"), 0));
                            }
                            break;
                    }
                    Session.SendPacket(Session.Character.GeneratePinit());
                    Session.SendPackets(Session.Character.GeneratePst());
                    break;

                case 10:
                    Session.SendPacket("wopen 3 0");
                    break;

                case 12:
                    Session.SendPacket($"wopen {packet.Type} 0");
                    break;

                case 14:
                    Session.SendPacket("wopen 27 0");
                    string recipelist = "m_list 2";
                    if (npc != null)
                    {
                        if (npc.Shop?.Name == "Item Ruota Della Fortuna")
                        {
                            DateTime now = DateTime.Now;
                            int Settimana = GetIso8601WeekOfYear(now) % 8;
                            List<RuotaFortunaDTO> roll = DAOFactory.RuotaFortunaDAO.LoadBySettimana((byte)Settimana).ToList();
                            List<Recipe> recipeweek = new List<Recipe>();
                            foreach (RuotaFortunaDTO rollitem in roll)
                            {
                                Recipe recipedto = new Recipe
                                {
                                    Amount = rollitem.ItemGeneratedAmount,
                                    ItemVNum = rollitem.ItemGeneratedVNum
                                };
                                recipeweek.Add(recipedto);
                            }
                            foreach (Recipe ite in recipeweek)
                            {
                                if (ite.Amount > 0) recipelist = recipelist + $" {ite.ItemVNum}";
                            }
                            recipelist += " -100";
                            Session.SendPacket(recipelist);
                        }
                        if (npc.Shop?.Name == "Darvo")
                        {
                            DarvoDTO darvoDTO = DAOFactory.DarvoDAO.LoadById(ServerManager.Instance.IdDarvo);
                            IEnumerable<DarvoListDTO> roll = DAOFactory.DarvoListDAO.LoadByDarvo(ServerManager.Instance.IdDarvo);
                            List<Recipe> recipedaily = new List<Recipe>();
                            foreach (DarvoListDTO rollitem in roll)
                            {
                                Recipe recipedto = new Recipe
                                {
                                    Amount = 1,
                                    ItemVNum = rollitem.ItemVNum,
                                    RecipeRarity = darvoDTO.Rare,
                                    RecipeUpgrade = darvoDTO.Upgrade
                                };
                                recipedaily.Add(recipedto);
                            }
                            foreach (Recipe ite in recipedaily)
                            {
                                if (ite.Amount > 0) recipelist = recipelist + $" {ite.ItemVNum}";
                            }
                            recipelist += " -100";
                            Session.SendPacket(recipelist);
                        }
                        else
                        {
                            List<Recipe> tps = npc.Recipes;
                            recipelist = tps.Where(s => s.Amount > 0).Aggregate(recipelist, (current, s) => current + $" {s.ItemVNum}");
                            recipelist += " -100";
                            Session.SendPacket(recipelist);
                        }
                    }
                    break;

                case 15:
                    //if (npc != null)
                    //{
                    //    if (packet.Value == 2)
                    //    {
                    //        Session.SendPacket($"qna #n_run^15^1^1^{npc.MapNpcId} {Language.Instance.GetMessageFromKey("ASK_CHANGE_SPAWNLOCATION")}");
                    //    }
                    //    else
                    //    {
                    //        switch (npc.MapId)
                    //        {
                    //            case 1:
                    //                Session.Character.SetRespawnPoint(1, 79, 116);
                    //                break;

                    //            case 20:
                    //                Session.Character.SetRespawnPoint(20, 9, 92);
                    //                break;

                    //            case 145:
                    //                Session.Character.SetRespawnPoint(145, 13, 110);
                    //                break;
                    //        }
                    //        Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("RESPAWNLOCATION_CHANGED"), 0));
                    //    }
                    //}
                    break;

                case 16:
                    tp = npc?.Teleporters?.FirstOrDefault(s => s.Index == packet.Type);
                    if (tp != null)
                    {
                        if (packet.Type >= 50)
                        {
                            ServerManager.Instance.ChangeMap(Session.Character.CharacterId, tp.MapId, tp.MapX, tp.MapY);
                        }
                        else if (packet.Type >= 20)
                        {
                            ServerManager.Instance.ChangeMap(Session.Character.CharacterId, tp.MapId, tp.MapX, tp.MapY);
                            if (Session.Character.MapId == 228)
                            {

                            }
                        }
                        else if (Session.Character.Gold >= 1000 * packet.Type)
                        {
                            Session.Character.Gold -= 1000 * packet.Type;
                            Session.SendPacket(Session.Character.GenerateGold());
                            ServerManager.Instance.ChangeMap(Session.Character.CharacterId, tp.MapId, tp.MapX, tp.MapY);
                            if (Session.Character.MapId == 228)
                            {

                            }
                        }
                        else
                        {
                            Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MONEY"), 10));
                        }
                    }
                    break;

                case 17:
                    double currentRunningSeconds = (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(-50)).TotalSeconds;
                    double timeSpanSinceLastPortal = currentRunningSeconds - Session.Character.LastPortal;
                    if (!(timeSpanSinceLastPortal >= 4) || !Session.HasCurrentMapInstance || ServerManager.Instance.ChannelId == 51 || Session.CurrentMapInstance.MapInstanceId == ServerManager.Instance.ArenaInstance.MapInstanceId || Session.CurrentMapInstance.MapInstanceId == ServerManager.Instance.FamilyArenaInstance.MapInstanceId)
                    {
                        Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("CANT_MOVE"), 10));
                        return;
                    }
                    if (Session.Character.Gold >= 500 * (1 + packet.Type))
                    {
                        Session.Character.LastPortal = currentRunningSeconds;
                        Session.Character.Gold -= 500 * (1 + packet.Type);
                        Session.SendPacket(Session.Character.GenerateGold());
                        MapCell pos = packet.Type == 0 ? ServerManager.Instance.ArenaInstance.Map.GetRandomPosition() : ServerManager.Instance.FamilyArenaInstance.Map.GetRandomPosition();
                        ServerManager.Instance.ChangeMapInstance(Session.Character.CharacterId, packet.Type == 0 ? ServerManager.Instance.ArenaInstance.MapInstanceId : ServerManager.Instance.FamilyArenaInstance.MapInstanceId, pos.X, pos.Y);
                    }
                    else
                    {
                        Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MONEY"), 10));
                    }
                    break;

                //case 111:
                //    if (npc != null)
                //    {
                //        Session.SendPacket($"qna #guri^{8886} {string.Format(Language.Instance.GetMessageFromKey("CRAFT_DRACO_SIGILLO"))}");
                //    }
                //    break;

                //case 133:
                //    if (npc != null)
                //    {
                //        Session.SendPacket($"qna #guri^{7886} {string.Format(Language.Instance.GetMessageFromKey("CRAFT_GLACE_SIGILLO"))}");
                //    }
                //    break;

                //case 147:
                //    if (npc != null)
                //    {
                //        Session.SendPacket($"qna #guri^{7885} {string.Format(Language.Instance.GetMessageFromKey("CRAFT_SP_GLACE"))}");
                //    }
                //    break;

                //case 148:
                //    if (npc != null)
                //    {
                //        Session.SendPacket($"qna #guri^{7884} {string.Format(Language.Instance.GetMessageFromKey("CRAFT_PERF_GLACE"))}");
                //    }
                //    break;

                //case 145:
                //    if (npc != null)
                //    {
                //        Session.SendPacket($"qna #guri^{8885} {string.Format(Language.Instance.GetMessageFromKey("CRAFT_SP_DRACO"))}");
                //    }
                //    break;

                //case 146:
                //    if (npc != null)
                //    {
                //        Session.SendPacket($"qna #guri^{8884} {string.Format(Language.Instance.GetMessageFromKey("CRAFT_PERF_DRACO"))}");
                //    }
                //    break;

                case 18:
                    Session.SendPacket(Session.Character.GenerateNpcDialog(17));
                    break;

                case 26:
                    tp = npc?.Teleporters?.FirstOrDefault(s => s.Index == packet.Type);
                    if (tp != null)
                    {
                        if (Session.Character.Gold >= 5000 * packet.Type)
                        {
                            Session.Character.Gold -= 5000 * packet.Type;
                            ServerManager.Instance.ChangeMap(Session.Character.CharacterId, tp.MapId, tp.MapX, tp.MapY);
                        }
                        else
                        {
                            Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MONEY"), 10));
                        }
                    }
                    break;

                case 45:
                    tp = npc?.Teleporters?.FirstOrDefault(s => s.Index == packet.Type);
                    if (tp != null)
                    {
                        if (Session.Character.Gold >= 500)
                        {
                            Session.Character.Gold -= 500;
                            Session.SendPacket(Session.Character.GenerateGold());
                            ServerManager.Instance.ChangeMap(Session.Character.CharacterId, tp.MapId, tp.MapX, tp.MapY);
                        }
                        else
                        {
                            Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MONEY"), 10));
                        }
                    }
                    break;

                case 132:
                    tp = npc?.Teleporters?.FirstOrDefault(s => s.Index == packet.Type);
                    if (tp != null)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, tp.MapId, tp.MapX, tp.MapY);
                    }
                    break;

                case 137:
                    Session.SendPacket("taw_open");
                    break;

                case 138:
                    ConcurrentBag<ArenaTeamMember> at = ServerManager.Instance.ArenaTeams.OrderBy(s => rand.Next()).FirstOrDefault();
                    if (at != null)
                    {
                        ServerManager.Instance.ChangeMapInstance(Session.Character.CharacterId, at.FirstOrDefault(s => s.Session != null).Session.CurrentMapInstance.MapInstanceId, 69, 100);

                        ArenaTeamMember zenas = at.OrderBy(s => s.Order).FirstOrDefault(s => s.Session != null && !s.Dead && s.ArenaTeamType == ArenaTeamType.ZENAS);
                        ArenaTeamMember erenia = at.OrderBy(s => s.Order).FirstOrDefault(s => s.Session != null && !s.Dead && s.ArenaTeamType == ArenaTeamType.ERENIA);
                        Session.SendPacket(erenia.Session.Character.GenerateTaM(0));
                        Session.SendPacket(erenia.Session.Character.GenerateTaM(3));
                        Session.SendPacket("taw_sv 0");
                        Session.SendPacket(zenas.Session.Character.GenerateTaP(0, true));
                        Session.SendPacket(erenia.Session.Character.GenerateTaP(2, true));
                        Session.SendPacket(zenas.Session.Character.GenerateTaFc(0));
                        Session.SendPacket(erenia.Session.Character.GenerateTaFc(1));
                    }
                    else
                    {
                        Session.SendPacket(UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("NO_TALENT_ARENA")));
                    }
                    break;
                case 135:
                    if (!ServerManager.Instance.StartedEvents.Contains(EventType.TALENTARENA))
                    {
                        Session.SendPacket(npc?.GenerateSay(Language.Instance.GetMessageFromKey("ARENA_NOT_OPEN"), 10));
                    }
                    else
                    {
                        int tickets = 5 - Session.Character.GeneralLogs.CountLinq(s => s.LogType == "TalentArena" && s.Timestamp.Date == DateTime.Today);
                        if (TalentArena.RegisteredParticipants.All(s => s != Session))
                        {
                            if (ServerManager.Instance.IsCharacterMemberOfGroup(Session.Character.CharacterId))
                            {
                                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("TALENT_ARENA_GROUP"), 0));
                                Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("TALENT_ARENA_GROUP"), 10));
                            }
                            else
                            {
                                Session.SendPacket(Session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("ARENA_TICKET_LEFT"), tickets), 10));
                                //TalentArena.RegisteredParticipants.Add; .Instance.Reg.Add(new ArenaMember
                                //{
                                //    ArenaType = EventType.TALENTARENA,
                                //    Session = Session,
                                //    GroupId = null,
                                //    Time = 0
                                //});
                                //TalentArena.RegisteredParticipants
                                
                            }
                        }
                        else
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("TALENT_ARENA_NO_MORE_TICKET"), 0));
                            Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("TALENT_ARENA_NO_MORE_TICKET"), 10));
                        }
                    }
                    break;

                case 4000:
                    if (npc != null)
                    {
                        if (ServerManager.Instance.CanRegisterArcobaleno)
                        {
                            if (Session.Character.Group != null)
                            {
                                if (Session.Character.Group.Raid == null)
                                {
                                    if (Session.Character.Group.GroupType == GroupType.BigTeam)
                                    {
                                        if (Session.Character.Group.IsLeader(Session))
                                        {
                                            if (ServerManager.Instance.ArcobalenoMembersRegistered.Count() > 0)
                                            {
                                                if (ServerManager.Instance.ArcobalenoMembersRegistered?.Where(s => s.Session == Session).Count() > 0)
                                                {
                                                    return;
                                                }
                                            }
                                            Parallel.ForEach(
                                                                                ServerManager.Instance.ArcobalenoMembers.Where(s => s.GroupId == Session.Character.Group.GroupId), s =>
                                                                                {
                                                                                    ServerManager.Instance.ArcobalenoMembersRegistered.Add(s);
                                                                                }
                                                                                );
                                            Session.SendPacket(npc?.GenerateSay(Language.Instance.GetMessageFromKey("BA_REGISTERED"), 10));

                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Session.SendPacket(npc?.GenerateSay(Language.Instance.GetMessageFromKey("BA_NOT_OPEN"), 10));
                        }
                    }
                    break;

                //case 110:
                //    if (npc != null)
                //    {
                //        Session.Character.AddQuest(5954);
                //    }
                //    break;

                case 150:
                    if (Session.Character?.Family == null)
                    {
                        Session.SendPacket(
                            UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NEED_FAMILY"),
                                0));
                        break;
                    }
                    if (Session.Character.MapId == 150)
                    {
                        return;
                    }
                    else
                    {
                        ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 150, 153, 145);
                    }
                    break;

                case 301:
                    tp = npc?.Teleporters?.FirstOrDefault(s => s.Index == packet.Type);
                    if (tp != null)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, tp.MapId, tp.MapX, tp.MapY);
                    }
                    break;

                case 1600:
                    //Session.SendPacket(Session.Character.OpenFamilyWarehouse());
                    Session.SendPacket(UserInterfaceHelper.GenerateModal(Language.Instance.GetMessageFromKey("THIS_FUNC_NOT_WORK"), 1));
                    break;

                case 1601:
                    //Session.SendPackets(Session.Character.OpenFamilyWarehouseHist());
                    Session.SendPacket(UserInterfaceHelper.GenerateModal(Language.Instance.GetMessageFromKey("THIS_FUNC_NOT_WORK"), 1));

                    break;

                case 1602:
                    //if (Session.Character.Family?.FamilyLevel >= 3 && Session.Character.Family.WarehouseSize < 21)
                    //{
                    //    if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head)
                    //    {
                    //        if (500000 >= Session.Character.Gold)
                    //        {
                    //            Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MONEY"), 10));
                    //            return;
                    //        }
                    //        Session.Character.Family.WarehouseSize = 21;
                    //        Session.Character.Gold -= 500000;
                    //        Session.SendPacket(Session.Character.GenerateGold());
                    //        FamilyDTO fam = Session.Character.Family;
                    //        DAOFactory.FamilyDAO.InsertOrUpdate(ref fam);
                    //        ServerManager.Instance.FamilyRefresh(Session.Character.Family.FamilyId);
                    //    }
                    //    else
                    //    {
                    //        Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("ONLY_HEAD_CAN_BUY"), 10));
                    //        Session.SendPacket(UserInterfaceHelper.GenerateModal(Language.Instance.GetMessageFromKey("ONLY_HEAD_CAN_BUY"), 1));
                    //    }
                    //}
                    Session.SendPacket(UserInterfaceHelper.GenerateModal(Language.Instance.GetMessageFromKey("THIS_FUNC_NOT_WORK"), 1));
                    break;

                case 1603:
                    //if (Session.Character.Family?.FamilyLevel >= 7 && Session.Character.Family.WarehouseSize < 49)
                    //{
                    //    if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head)
                    //    {
                    //        if (2000000 >= Session.Character.Gold)
                    //        {
                    //            Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MONEY"), 10));
                    //            return;
                    //        }
                    //        Session.Character.Family.WarehouseSize = 49;
                    //        Session.Character.Gold -= 2000000;
                    //        Session.SendPacket(Session.Character.GenerateGold());
                    //        FamilyDTO fam = Session.Character.Family;
                    //        DAOFactory.FamilyDAO.InsertOrUpdate(ref fam);
                    //        ServerManager.Instance.FamilyRefresh(Session.Character.Family.FamilyId);
                    //    }
                    //    else
                    //    {
                    //        Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("ONLY_HEAD_CAN_BUY"), 10));
                    //        Session.SendPacket(UserInterfaceHelper.GenerateModal(Language.Instance.GetMessageFromKey("ONLY_HEAD_CAN_BUY"), 1));
                    //    }
                    //}
                    break;

                case 1604:
                    //if (Session.Character.Family?.FamilyLevel >= 5 && Session.Character.Family.MaxSize < 30)
                    //{
                    //    if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Manager)
                    //    {
                    //        if (5000000 >= Session.Character.Gold)
                    //        {
                    //            Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MONEY"), 10));
                    //            return;
                    //        }
                    //        Session.Character.Family.MaxSize = 30;
                    //        Session.Character.Gold -= 5000000;
                    //        Session.SendPacket(Session.Character.GenerateGold());
                    //        FamilyDTO fam = Session.Character.Family;
                    //        DAOFactory.FamilyDAO.InsertOrUpdate(ref fam);
                    //        ServerManager.Instance.FamilyRefresh(Session.Character.Family.FamilyId);
                    //    }
                    //    else
                    //    {
                    //        Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("ONLY_HEAD_CAN_BUY"), 10));
                    //        Session.SendPacket(UserInterfaceHelper.GenerateModal(Language.Instance.GetMessageFromKey("ONLY_HEAD_CAN_BUY"), 1));
                    //    }
                    //}
                    break;

                case 1605:
                    //if (Session.Character.Family?.FamilyLevel >= 9 && Session.Character.Family.MaxSize < 40)
                    //{
                    //    if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Manager)
                    //    {
                    //        if (10000000 >= Session.Character.Gold)
                    //        {
                    //            Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MONEY"), 10));
                    //            return;
                    //        }
                    //        Session.Character.Family.MaxSize = 40;
                    //        Session.Character.Gold -= 10000000;
                    //        Session.SendPacket(Session.Character.GenerateGold());
                    //        FamilyDTO fam = Session.Character.Family;
                    //        DAOFactory.FamilyDAO.InsertOrUpdate(ref fam);
                    //        ServerManager.Instance.FamilyRefresh(Session.Character.Family.FamilyId);
                    //    }
                    //    else
                    //    {
                    //        Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("ONLY_HEAD_CAN_BUY"), 10));
                    //        Session.SendPacket(UserInterfaceHelper.GenerateModal(Language.Instance.GetMessageFromKey("ONLY_HEAD_CAN_BUY"), 1));
                    //    }
                    //}
                    break;

                case 23:
                    if (packet.Type == 0)
                    {
                        if (Session.Character.Group?.CharacterCount == 3)
                        {
                            foreach (ClientSession s in Session.Character.Group.Characters.GetAllItems())
                            {
                                if (s.Character.Family != null)
                                {
                                    Session.SendPacket(UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("GROUP_MEMBER_ALREADY_IN_FAMILY")));
                                    return;
                                }
                            }
                        }
                        if (Session.Character.Group == null || Session.Character.Group.CharacterCount != 3)
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("FAMILY_GROUP_NOT_FULL")));
                            return;
                        }
                        Session.SendPacket(UserInterfaceHelper.GenerateInbox($"#glmk^ {14} 1 {Language.Instance.GetMessageFromKey("CREATE_FAMILY").Replace(' ', '^')}"));
                    }
                    else
                    {
                        if (Session.Character.Family == null)
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("NOT_IN_FAMILY")));
                            return;
                        }
                        if (Session.Character.Family != null && Session.Character.FamilyCharacter != null && Session.Character.FamilyCharacter.Authority != FamilyAuthority.Head)
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("NOT_FAMILY_HEAD")));
                            return;
                        }
                        Session.SendPacket($"qna #glrm^1 {Language.Instance.GetMessageFromKey("DISMISS_FAMILY")}");
                    }

                    break;

                case 60:
                    StaticBonusDTO medal = Session.Character.StaticBonusList.Find(s => s.StaticBonusType == StaticBonusType.BazaarMedalGold || s.StaticBonusType == StaticBonusType.BazaarMedalSilver);
                    byte Medal = 0;
                    int Time = 0;
                    if (medal != null)
                    {
                        Medal = medal.StaticBonusType == StaticBonusType.BazaarMedalGold ? (byte)MedalType.Gold : (byte)MedalType.Silver;
                        Time = (int)(medal.DateEnd - DateTime.Now).TotalHours;
                    }
                    Session.SendPacket($"wopen 32 {Medal} {Time}");
                    break;

                case 131:
                    if (npc != null)
                    {
                        Session.Character.AddQuest(5984);
                    }
                    break;


                case 321:
                    if (npc != null)
                    {
                        Session.SendPacket(Session.Character.GenerateGB(3));
                        Session.SendPacket(Session.Character.GenerateSMemo(6, "Un caloroso benvenuto alla Banca Cuarry. Puoi versare o prelevare da 1.000 a 100 miliardi di unità di oro."));

                    }
                    break;

                case 322:
                    if (npc != null)
                    {
                        short libretto = 5836;
                        ItemInstance invlibre = Session.Character.Inventory.AddNewToInventory(libretto, 1, Rare: 0, Upgrade: 0)
                        .FirstOrDefault();
                        if (invlibre != null)
                        {

                            Item iteminfo = ServerManager.GetItem(libretto);
                            // pdti {WindowType} {inv.ItemVNum} {recipe.Amount} {Unknown} {inv.Upgrade} {inv.Rare}
                            Session.SendPacket($"pdti 11 {invlibre.ItemVNum} {1} 29 {invlibre.Upgrade} {invlibre.Rare}");
                            Session.SendPacket(Session.Character.GenerateSay(
                                $"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: {iteminfo.Name} x 1", 12));
                        }
                        break;

                    }
                    break;

                case 5002:
                    tp = npc?.Teleporters?.FirstOrDefault(s => s.Index == packet.Type);
                    if (tp != null)
                    {
                        //Session.SendPacket("it 3");
                        if (ServerManager.Instance.ChannelId == 51)
                        {
                            string connection = CommunicationServiceClient.Instance.RetrieveOriginWorld(Session.Account.AccountId);
                            if (string.IsNullOrWhiteSpace(connection))
                            {
                                return;
                            }
                            Session.Character.MapId = tp.MapId;
                            Session.Character.MapX = tp.MapX;
                            Session.Character.MapY = tp.MapY;
                            int port = Convert.ToInt32(connection.Split(':')[1]);
                            Session.Character.ChangeChannel(connection.Split(':')[0], port, 3);
                        }
                        else
                        {
                            ServerManager.Instance.ChangeMap(Session.Character.CharacterId, tp.MapId, tp.MapX, tp.MapY);
                        }
                    }
                    break;

                //case 1000:
                //    if (npc != null)
                //    {
                //        if (npc.NpcVNum == 933)
                //        {
                //            ScriptedInstance timespace = Session.CurrentMapInstance.ScriptedInstances
                //                .Find(s => s.LevelMinimum == 251).Copy();
                //            ScriptedInstance instance = timespace.Copy();
                //            instance.LoadScript(MapInstanceType.TimeSpaceInstance);
                //            if (instance.FirstMap == null)
                //            {
                //                return;
                //            }

                //            Session.Character.MapX = instance.PositionX;
                //            Session.Character.MapY = instance.PositionY;
                //            ServerManager.Instance.TeleportOnRandomPlaceInMap(Session, instance.FirstMap.MapInstanceId);
                //            instance.InstanceBag.CreatorId = Session.Character.CharacterId;
                //            Session.SendPackets(instance.GenerateMinimap());
                //            Session.SendPacket(instance.GenerateMainInfo());
                //            Session.SendPacket(instance.FirstMap.InstanceBag.GenerateScore());

                //            Session.Character.Timespace = instance;
                //        }
                //        if (npc.NpcVNum == 934)
                //        {
                //            ScriptedInstance timespace = Session.CurrentMapInstance.ScriptedInstances
                //                .Find(s => s.LevelMinimum == 252).Copy();
                //            ScriptedInstance instance = timespace.Copy();
                //            instance.LoadScript(MapInstanceType.TimeSpaceInstance);
                //            if (instance.FirstMap == null)
                //            {
                //                return;
                //            }

                //            Session.Character.MapX = instance.PositionX;
                //            Session.Character.MapY = instance.PositionY;
                //            ServerManager.Instance.TeleportOnRandomPlaceInMap(Session, instance.FirstMap.MapInstanceId);
                //            instance.InstanceBag.CreatorId = Session.Character.CharacterId;
                //            Session.SendPackets(instance.GenerateMinimap());
                //            Session.SendPacket(instance.GenerateMainInfo());
                //            Session.SendPacket(instance.FirstMap.InstanceBag.GenerateScore());

                //            Session.Character.Timespace = instance;
                //        }
                //    }
                //    break;

                //case 2000:
                //    if (npc != null)
                //    {
                //        if (npc.MapNpcId == 419)
                //        {
                //            Session.Character.AddQuest(2008);
                //        }
                //        else if (npc.MapNpcId == 428)
                //        {
                //            Session.Character.AddQuest(2014);
                //        }
                //        else if (npc.MapNpcId == 429)
                //        {
                //            Session.Character.AddQuest(2060);
                //        }
                //        else if (npc.MapNpcId == 246)
                //        {
                //            Session.Character.AddQuest(2100);
                //        }
                //        else if (npc.MapNpcId == 49)
                //        {
                //            Session.Character.AddQuest(2030);
                //        }
                //    }
                //    break;

                //case 3006:
                //    if (npc != null)
                //    {
                //        Session.Character.AddQuest(packet.Type);
                //    }
                //    break;

                //case 5002:
                //    //else if (npc.MapNpcId == 8332)
                //    //{
                //    //tp = npc?.Teleporters?.FirstOrDefault(s => s.Index == packet.Type);
                //    //if (tp != null)
                //    //{
                //    //    //Session.SendPacket("it 3");
                //    //    if (ServerManager.Instance.ChannelId == 51)
                //    //    {
                //    //        string connection = CommunicationServiceClient.Instance.RetrieveOriginWorld(Session.Account.AccountId);
                //    //        if (string.IsNullOrWhiteSpace(connection))
                //    //        {
                //    //            return;
                //    //        }
                //    //        Session.Character.MapId = tp.MapId;
                //    //        Session.Character.MapX = tp.MapX;
                //    //        Session.Character.MapY = tp.MapY;
                //    //        int port = Convert.ToInt32(connection.Split(':')[1]);
                //    //        Session.Character.ChangeChannel(connection.Split(':')[0], port, 3);
                //    //    }
                //    //    else
                //    //    {
                //    //        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, tp.MapId, tp.MapX, tp.MapY);
                //    //    }
                //    //}
                //    //}
                //    //else
                //    //{
                //    //    return;
                //    //}
                //    //break;
                //    switch (packet.Type)
                //    {
                //        case 0:
                //            switch (packet.Value)
                //            {
                //                case 2:
                //                    switch (packet.NpcId)
                //                    {
                //                        case 8208: // npc id
                //                            {
                //                                tp = npc?.Teleporters?.FirstOrDefault(s => s.Index == packet.Type);
                //                                if (tp != null)
                //                                {
                //                                    Console.WriteLine("test133");

                //                                    //Session.SendPacket("it 3");
                //                                    if (ServerManager.Instance.ChannelId == 51)
                //                                    {
                //                                        string connection = CommunicationServiceClient.Instance.RetrieveOriginWorld(Session.Account.AccountId);
                //                                        if (string.IsNullOrWhiteSpace(connection))
                //                                        {
                //                                            return;
                //                                        }
                //                                        Session.Character.MapId = tp.MapId;
                //                                        Session.Character.MapX = tp.MapX;
                //                                        Session.Character.MapY = tp.MapY;
                //                                        int port = Convert.ToInt32(connection.Split(':')[1]);
                //                                        Session.Character.ChangeChannel(connection.Split(':')[0], port, 3);
                //                                        Console.WriteLine("test1");
                //                                    }
                //                                    else
                //                                    {
                //                                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, tp.MapId, tp.MapX, tp.MapY);
                //                                        Console.WriteLine("test2");
                //                                    }
                //                                }
                //                            }
                //                            break;
                //                        case 8339: // npc id
                //                            {
                //                                tp = npc?.Teleporters?.FirstOrDefault(s => s.Index == packet.Type);
                //                                if (tp != null)
                //                                {
                //                                    Console.WriteLine("test133");

                //                                    //Session.SendPacket("it 3");
                //                                    if (ServerManager.Instance.ChannelId == 51)
                //                                    {
                //                                        string connection = CommunicationServiceClient.Instance.RetrieveOriginWorld(Session.Account.AccountId);
                //                                        if (string.IsNullOrWhiteSpace(connection))
                //                                        {
                //                                            return;
                //                                        }
                //                                        Session.Character.MapId = tp.MapId;
                //                                        Session.Character.MapX = tp.MapX;
                //                                        Session.Character.MapY = tp.MapY;
                //                                        int port = Convert.ToInt32(connection.Split(':')[1]);
                //                                        Session.Character.ChangeChannel(connection.Split(':')[0], port, 3);
                //                                        Console.WriteLine("test1");
                //                                    }
                //                                    else
                //                                    {
                //                                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, tp.MapId, tp.MapX, tp.MapY);
                //                                        Console.WriteLine("test2");
                //                                    }
                //                                }
                //                            }
                //                            break;

                //                    }
                //                    break;
                //            }
                //            break;
                //    }
                //    break;

                case 5001:
                    if (npc != null)
                    {
                        switch (Session.Character.Faction)
                        {
                            case FactionType.None:
                                Session.SendPacket(UserInterfaceHelper.GenerateInfo("You need to be part of a faction to join Act 4"));
                                return;

                            case FactionType.Angel:
                                Session.Character.MapId = 130;
                                Session.Character.MapX = 12;
                                Session.Character.MapY = 40;
                                break;

                            case FactionType.Demon:
                                Session.Character.MapId = 131;
                                Session.Character.MapX = 12;
                                Session.Character.MapY = 40;
                                break;
                        }

                        Session.Character.ChangeChannel(ServerManager.Instance.Configuration.Act4IP, ServerManager.Instance.Configuration.Act4Port, 1);


                        break;

                    }
                    break;

                case 5004:
                    if (npc != null)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 145, 50, 41);
                    }
                    break;

                case 5011:
                    if (npc != null)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 170, 127, 46);
                    }
                    break;

                case 5012:
                    tp = npc?.Teleporters?.FirstOrDefault(s => s.Index == packet.Type);
                    if (tp != null)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, tp.MapId, tp.MapX, tp.MapY);
                    }
                    break;

                default:
                    Logger.Warn(string.Format(Language.Instance.GetMessageFromKey("NO_NRUN_HANDLER"), packet.Runner));
                    break;
            }
        }

        #endregion
    }
}