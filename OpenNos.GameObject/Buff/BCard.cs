﻿/*
/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using OpenNos.GameObject.Networking;
using static OpenNos.Domain.BCardType;

namespace OpenNos.GameObject
{
    public class BCard : BCardDTO
    {
        public BCard()
        {

        }

        public BCard(BCardDTO input)
        {
            BCardId = input.BCardId;
            CardId = input.CardId;
            CastType = input.CastType;
            FirstData = input.FirstData;
            IsLevelDivided = input.IsLevelDivided;
            IsLevelScaled = input.IsLevelScaled;
            ItemVNum = input.ItemVNum;
            NpcMonsterVNum = input.NpcMonsterVNum;
            SecondData = input.SecondData;
            SkillVNum = input.SkillVNum;
            SubType = input.SubType;
            ThirdData = input.ThirdData;
            Type = input.Type;
        }

        #region Methods

        public void ApplyBCards(object session, object sender = null, object target = null, int rare = 0, short mapx = 0, short mapy = 0)
        {
            Type type = session.GetType();
            Type typetarget = target?.GetType();

            // int counterBuff = 0;
            if (type == null)
            {
                return;
            }
            switch ((BCardType.CardType)Type)
            {
                case BCardType.CardType.Buff:
                    {
                        if (type == typeof(Character) && session is Character character)
                        {
                            Buff buff = null;
                            if (sender != null)
                            {
                                Type sType = sender.GetType();
                                if (sType != null)
                                {
                                    if (sType == typeof(Character) && sender is Character sendingCharacter)
                                    {
                                        buff = new Buff((short)(SecondData + (rare > 0 ? rare - 1 : 0)), sendingCharacter.SwitchLevel(), sender: sendingCharacter.Session.SessionId);
                                        if (buff.Card.BuffType == BuffType.Good)
                                        {
                                            if (ServerManager.RandomNumber() < FirstData)
                                            {
                                                if (buff.Card.CardId == 546)
                                                {
                                                    sendingCharacter.AddBuff(buff);
                                                }
                                                else
                                                {
                                                    character.AddBuff(buff);
                                                }
                                            }
                                            return;
                                        }
                                        //Todo: Get anti stats from BCard
                                    }
                                }
                            }
                            else
                            {
                                buff = new Buff((short)(SecondData + (rare > 0 ? rare - 1 : 0)), character.SwitchLevel());
                            }

                            if (buff.Card.BuffType == BuffType.Bad || buff.Card.BuffType == BuffType.Neutral)
                            {
                                #region Chance
                                int Chance = FirstData;

                                #region ShellAntiMalusGlobal
                                double Debuffmultipicator = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedAllNegativeEffect)?.Value / 100D) ?? 0;
                                double DebuffReal = 1 - Debuffmultipicator;
                                Chance = (int)(Chance * DebuffReal);
                                #endregion

                                if (buff.Card.BuffTipo == BuffTipo.MinorBleed)
                                {
                                    double DebuffmultipicatorMinorBleed = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedMinorBleeding)?.Value / 100D) ?? 0;
                                    double DebuffRealMinorBleed = 1 - DebuffmultipicatorMinorBleed;
                                    Chance = (int)(Chance * DebuffRealMinorBleed);

                                    double DebuffmultipicatorBleed = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedBleedingAndMinorBleeding)?.Value / 100D) ?? 0;
                                    double DebuffRealBleed = 1 - DebuffmultipicatorBleed;
                                    Chance = (int)(Chance * DebuffRealBleed);

                                    double DebuffmultiplicatorAllBleed = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedAllBleedingType)?.Value / 100D) ?? 0;
                                    double DebuffRealAllBleed = 1 - DebuffmultiplicatorAllBleed;
                                    Chance = (int)(Chance * DebuffRealAllBleed);
                                }

                                if (buff.Card.BuffTipo == BuffTipo.Bleed)
                                {
                                    double DebuffmultipicatorBleed = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedBleedingAndMinorBleeding)?.Value / 100D) ?? 0;
                                    double DebuffRealBleed = 1 - DebuffmultipicatorBleed;
                                    Chance = (int)(Chance * DebuffRealBleed);

                                    double DebuffmultiplicatorAllBleed = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedAllBleedingType)?.Value / 100D) ?? 0;
                                    double DebuffRealAllBleed = 1 - DebuffmultiplicatorAllBleed;
                                    Chance = (int)(Chance * DebuffRealAllBleed);
                                }

                                if (buff.Card.BuffTipo == BuffTipo.Stun)
                                {
                                    double DebuffmultipicatorStun = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedStun)?.Value / 100D) ?? 0;
                                    double DebuffRealStun = 1 - DebuffmultipicatorStun;
                                    Chance = (int)(Chance * DebuffRealStun);

                                    double DebuffmultiplicatorAllStun = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedAllStun)?.Value / 100D) ?? 0;
                                    double DebuffRealAllStun = 1 - DebuffmultiplicatorAllStun;
                                    Chance = (int)(Chance * DebuffRealAllStun);
                                }

                                if (buff.Card.BuffTipo == BuffTipo.Paralysis)
                                {
                                    double DebuffmultipicatorParalysis = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedParalysis)?.Value / 100D) ?? 0;
                                    double DebuffRealParalysis = 1 - DebuffmultipicatorParalysis;
                                    Chance = (int)(Chance * DebuffRealParalysis);
                                }


                                if (buff.Card.BuffTipo == BuffTipo.PoisonParalysis)
                                {
                                    double DebuffmultipicatorPoisonParalysis = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedPoisonParalysis)?.Value / 100D) ?? 0;
                                    double DebuffRealPoisonParalysis = 1 - DebuffmultipicatorPoisonParalysis;
                                    Chance = (int)(Chance * DebuffRealPoisonParalysis);
                                }


                                if (buff.Card.BuffTipo == BuffTipo.Freeze)
                                {
                                    double DebuffmultipicatorFreeze = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedFreeze)?.Value / 100D) ?? 0;
                                    double DebuffRealFreeze = 1 - DebuffmultipicatorFreeze;
                                    Chance = (int)(Chance * DebuffRealFreeze);
                                }

                                //if (buff.Card.BuffTipo == BuffTipo.Blind)
                                //{
                                //    double DebuffmultipicatorBlind = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedBlind)?.Value / 100D) ?? 0;
                                //    double DebuffRealBlind = 1 - DebuffmultipicatorBlind;
                                //    Chance = (int)(Chance * DebuffRealBlind);
                                //}

                                if (buff.Card.BuffTipo == BuffTipo.Slow)
                                {
                                    double DebuffmultipicatorSlow = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedSlow)?.Value / 100D) ?? 0;
                                    double DebuffRealSlow = 1 - DebuffmultipicatorSlow;
                                    Chance = (int)(Chance * DebuffRealSlow);
                                }

                                if (buff.Card.BuffTipo == BuffTipo.ArmorDebuff)
                                {
                                    double DebuffmultipicatorArmorDebuff = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedArmorDeBuff)?.Value / 100D) ?? 0;
                                    double DebuffRealArmorDuff = 1 - DebuffmultipicatorArmorDebuff;
                                    Chance = (int)(Chance * DebuffRealArmorDuff);
                                }

                                if (buff.Card.BuffTipo == BuffTipo.Shock)
                                {
                                    double DebuffmultipicatorShock = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedShock)?.Value / 100D) ?? 0;
                                    double DebuffRealShock = 1 - DebuffmultipicatorShock;
                                    Chance = (int)(Chance * DebuffRealShock);
                                }
                                #endregion

                                if (ServerManager.RandomNumber() < Chance)
                                {
                                    foreach (BCard entry in character.EquipmentBCards.Where(s => s?.Type.Equals((byte)24) == true && s.SubType.Equals((byte)3)))
                                    {
                                        if (entry.SecondData == buff.Card.CardId)
                                        {
                                            if (entry.FirstData > ServerManager.RandomNumber(0, 100))
                                            {
                                                return;
                                            }
                                        }
                                    }
                                    int AntimalusLevel = Math.Abs(character.GetBuff(CardType.DebuffResistance, 10)[0]);
                                    int AntimalusChance = character.GetBuff(CardType.DebuffResistance, 10)[1];
                                    if (sender.GetType() == typeof(Character) && sender is Character sendingCharacter && buff.Card.BuffType == BuffType.Bad && character.Buff.ContainsKey(502))
                                        character = sendingCharacter;
                                    if (buff.Card.Level <= AntimalusLevel)
                                    {
                                        if (ServerManager.RandomNumber(0, 100) >= AntimalusChance)
                                        {
                                            character.AddBuff(buff);
                                            break;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }

                                    character.AddBuff(buff);
                                }
                            }
                            else
                            {
                                int Chance = FirstData;
                                if (ServerManager.RandomNumber() < Chance)
                                {
                                    character.AddBuff(buff);
                                }
                            }
                        }
                        else if (type == typeof(MapMonster))
                        {
                            if (ServerManager.RandomNumber() < FirstData && session is MapMonster mapMonster)
                            {
                                Buff buffalo = new Buff((short)(SecondData + (rare > 0 ? rare - 1 : 0)), mapMonster.Monster.Level);
                                if (buffalo.Card.BuffType == BuffType.Bad || buffalo.Card.BuffType == BuffType.Neutral)
                                {
                                    if (typetarget != null)
                                    {
                                        if (typetarget == typeof(Character) && target is Character chartarget)
                                        {

                                            foreach (BCard entry in chartarget.EquipmentBCards.Where(s => s?.Type.Equals((byte)24) == true && s.SubType.Equals((byte)3)))
                                            {
                                                if (entry.SecondData == buffalo.Card.CardId)
                                                {
                                                    if (entry.FirstData > ServerManager.RandomNumber(0, 100))
                                                    {
                                                        return;
                                                    }
                                                }
                                            }
                                            int AntimalusLevel = Math.Abs(chartarget.GetBuff(CardType.DebuffResistance, 10)[0]);
                                            int AntimalusChance = chartarget.GetBuff(CardType.DebuffResistance, 10)[1];

                                            #region Chance
                                            int Chance = FirstData;

                                            #region ShellAntiMalusGlobal
                                            double Debuffmultipicator = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedAllNegativeEffect)?.Value / 100D) ?? 0;
                                            double DebuffReal = 1 - Debuffmultipicator;
                                            Chance = (int)(Chance * DebuffReal);
                                            #endregion

                                            if (buffalo.Card.BuffTipo == BuffTipo.MinorBleed)
                                            {
                                                double DebuffmultipicatorMinorBleed = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedMinorBleeding)?.Value / 100D) ?? 0;
                                                double DebuffRealMinorBleed = 1 - DebuffmultipicatorMinorBleed;
                                                Chance = (int)(Chance * DebuffRealMinorBleed);

                                                double DebuffmultipicatorBleed = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedBleedingAndMinorBleeding)?.Value / 100D) ?? 0;
                                                double DebuffRealBleed = 1 - DebuffmultipicatorBleed;
                                                Chance = (int)(Chance * DebuffRealBleed);

                                                double DebuffmultiplicatorAllBleed = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedAllBleedingType)?.Value / 100D) ?? 0;
                                                double DebuffRealAllBleed = 1 - DebuffmultiplicatorAllBleed;
                                                Chance = (int)(Chance * DebuffRealAllBleed);
                                            }

                                            if (buffalo.Card.BuffTipo == BuffTipo.Bleed)
                                            {
                                                double DebuffmultipicatorBleed = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedBleedingAndMinorBleeding)?.Value / 100D) ?? 0;
                                                double DebuffRealBleed = 1 - DebuffmultipicatorBleed;
                                                Chance = (int)(Chance * DebuffRealBleed);

                                                double DebuffmultiplicatorAllBleed = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedAllBleedingType)?.Value / 100D) ?? 0;
                                                double DebuffRealAllBleed = 1 - DebuffmultiplicatorAllBleed;
                                                Chance = (int)(Chance * DebuffRealAllBleed);
                                            }

                                            if (buffalo.Card.BuffTipo == BuffTipo.Stun)
                                            {
                                                double DebuffmultipicatorStun = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedStun)?.Value / 100D) ?? 0;
                                                double DebuffRealStun = 1 - DebuffmultipicatorStun;
                                                Chance = (int)(Chance * DebuffRealStun);

                                                double DebuffmultiplicatorAllStun = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedAllStun)?.Value / 100D) ?? 0;
                                                double DebuffRealAllStun = 1 - DebuffmultiplicatorAllStun;
                                                Chance = (int)(Chance * DebuffRealAllStun);
                                                //Lo faccio per voi
                                                AntimalusLevel = 100000;
                                                AntimalusChance = 100000;
                                            }

                                            if (buffalo.Card.BuffTipo == BuffTipo.Paralysis)
                                            {
                                                double DebuffmultipicatorParalysis = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedParalysis)?.Value / 100D) ?? 0;
                                                double DebuffRealParalysis = 1 - DebuffmultipicatorParalysis;
                                                Chance = (int)(Chance * DebuffRealParalysis);
                                                //Lo faccio per voi
                                                AntimalusLevel = 100000;
                                                AntimalusChance = 100000;
                                            }


                                            if (buffalo.Card.BuffTipo == BuffTipo.PoisonParalysis)
                                            {
                                                double DebuffmultipicatorPoisonParalysis = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedPoisonParalysis)?.Value / 100D) ?? 0;
                                                double DebuffRealPoisonParalysis = 1 - DebuffmultipicatorPoisonParalysis;
                                                Chance = (int)(Chance * DebuffRealPoisonParalysis);
                                            }


                                            if (buffalo.Card.BuffTipo == BuffTipo.Freeze)
                                            {
                                                double DebuffmultipicatorFreeze = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedFreeze)?.Value / 100D) ?? 0;
                                                double DebuffRealFreeze = 1 - DebuffmultipicatorFreeze;
                                                Chance = (int)(Chance * DebuffRealFreeze);
                                                //Lo faccio per voi
                                                AntimalusLevel = 100000;
                                                AntimalusChance = 100000;
                                            }

                                            //if (buffalo.Card.BuffTipo == BuffTipo.Blind)
                                            //{
                                            //    double DebuffmultipicatorBlind = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedBlind)?.Value / 100D) ?? 0;
                                            //    double DebuffRealBlind = 1 - DebuffmultipicatorBlind;
                                            //    Chance = (int)(Chance * DebuffRealBlind);
                                            //}

                                            if (buffalo.Card.BuffTipo == BuffTipo.Slow)
                                            {
                                                double DebuffmultipicatorSlow = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedSlow)?.Value / 100D) ?? 0;
                                                double DebuffRealSlow = 1 - DebuffmultipicatorSlow;
                                                Chance = (int)(Chance * DebuffRealSlow);
                                            }

                                            if (buffalo.Card.BuffTipo == BuffTipo.ArmorDebuff)
                                            {
                                                double DebuffmultipicatorArmorDebuff = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedArmorDeBuff)?.Value / 100D) ?? 0;
                                                double DebuffRealArmorDuff = 1 - DebuffmultipicatorArmorDebuff;
                                                Chance = (int)(Chance * DebuffRealArmorDuff);
                                            }

                                            if (buffalo.Card.BuffTipo == BuffTipo.Shock)
                                            {
                                                double DebuffmultipicatorShock = (chartarget.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedShock)?.Value / 100D) ?? 0;
                                                double DebuffRealShock = 1 - DebuffmultipicatorShock;
                                                Chance = (int)(Chance * DebuffRealShock);
                                                //Lo faccio per voi
                                                AntimalusLevel = 100000;
                                                AntimalusChance = 100000;
                                            }
                                            #endregion
                                            if (Chance >= ServerManager.RandomNumber(0, 100))
                                            {

                                                if (buffalo.Card.Level <= AntimalusLevel)
                                                {
                                                    if (ServerManager.RandomNumber(0, 100) >= AntimalusChance)
                                                    {
                                                        chartarget.AddBuff(buffalo);
                                                    }
                                                    else
                                                    {
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    chartarget.AddBuff(buffalo);
                                                }
                                            }
                                            else
                                            {
                                                break;
                                            }
                                            chartarget.AddBuff(buffalo);
                                        }
                                        else if (typetarget == typeof(MapMonster) && target is MapMonster mmmmm)
                                        {
                                            Buff buff = new Buff((short)(SecondData + (rare > 0 ? rare - 1 : 0)), mapMonster.Monster.Level);
                                            int AntimalusLevel = Math.Abs(mmmmm.GetBuff(CardType.DebuffResistance, 0)[0]);
                                            int AntimalusChance = mmmmm.GetBuff(CardType.DebuffResistance, 0)[1];
                                            int NoAntimalus = mmmmm.GetBuff(CardType.NoAntimalus, 0)[0];

                                            if (buff.Card.Level <= AntimalusLevel && NoAntimalus == 0)
                                            {
                                                if (ServerManager.RandomNumber(0, 100) >= AntimalusChance)
                                                {
                                                    mmmmm.AddBuff(buff);
                                                }
                                            }
                                            else
                                            {
                                                mmmmm.AddBuff(buff);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Buff buff = new Buff((short)(SecondData + (rare > 0 ? rare - 1 : 0)), mapMonster.Monster.Level);
                                        int AntimalusLevel = Math.Abs(mapMonster.GetBuff(CardType.DebuffResistance, 0)[0]);
                                        int AntimalusChance = mapMonster.GetBuff(CardType.DebuffResistance, 0)[1];
                                        int NoAntimalus = mapMonster.GetBuff(CardType.NoAntimalus, 0)[0];

                                        if (buff.Card.Level <= AntimalusLevel && NoAntimalus == 0)
                                        {
                                            if (ServerManager.RandomNumber(0, 100) >= AntimalusChance)
                                            {
                                                mapMonster.AddBuff(buff);
                                            }
                                        }
                                        else
                                        {
                                            mapMonster.AddBuff(buff);
                                        }
                                    }
                                }
                                else
                                {
                                    if (sender != null)
                                    {
                                        Type sType = sender.GetType();
                                        if (sType != null)
                                        {
                                            if (sType == typeof(Character) && sender is Character sendingCharacter)
                                            {
                                                sendingCharacter.AddBuff(new Buff((short)(SecondData + (rare > 0 ? rare - 1 : 0)), sendingCharacter.SwitchLevel()));

                                                //Todo: Get anti stats from BCard
                                            }
                                        }
                                    }
                                    else
                                    {
                                        mapMonster.AddBuff(new Buff((short)(SecondData + (rare > 0 ? rare - 1 : 0)), mapMonster.Monster.Level));
                                    }
                                }
                            }
                        }
                        else if (type == typeof(MapNpc))
                        {
                        }
                        else if (type == typeof(Mate))
                        {
                            if (ServerManager.RandomNumber() < FirstData && session is Mate matemonster)
                            {
                                Buff buffalo = new Buff((short)(SecondData + (rare > 0 ? rare - 1 : 0)), matemonster.Level);
                                matemonster.AddBuff(buffalo);
                            }
                        }
                        break;
                    }
                case BCardType.CardType.Move:
                    {
                        if (type == typeof(Character) && session is Character character)
                        {
                            character.LastSpeedChange = DateTime.Now;
                            character.Session.SendPacket(character.GenerateCond());
                        }
                    }
                    break;

                case BCardType.CardType.Summons:
                    if (type == typeof(Character))
                    {
                    }
                    else if (type == typeof(MapMonster))
                    {
                        if (session is MapMonster mapMonster)
                        {
                            List<MonsterToSummon> summonParameters = new List<MonsterToSummon>();
                            for (int i = 0; i < FirstData; i++)
                            {
                                short x = (short)(ServerManager.RandomNumber(-3, 3) + mapMonster.MapX);
                                short y = (short)(ServerManager.RandomNumber(-3, 3) + mapMonster.MapY);
                                summonParameters.Add(new MonsterToSummon((short)SecondData, new MapCell { X = x, Y = y }, -1, true));
                            }
                            if (ServerManager.RandomNumber() <= Math.Abs(ThirdData) || ThirdData == 0)
                            {
                                switch (SubType)
                                {
                                    case 2:
                                        EventHelper.Instance.RunEvent(new EventContainer(mapMonster.MapInstance, EventActionType.SPAWNMONSTERS, summonParameters));
                                        break;

                                    default:
                                        if (!mapMonster.OnDeathEvents.Any(s => s.EventActionType == EventActionType.SPAWNMONSTERS))
                                        {
                                            mapMonster.OnDeathEvents.Add(new EventContainer(mapMonster.MapInstance, EventActionType.SPAWNMONSTERS, summonParameters));
                                        }
                                        break;
                                }
                            }
                        }
                    }
                    else if (type == typeof(MapNpc))
                    {
                    }
                    else if (type == typeof(Mate))
                    {
                    }
                    break;

                case BCardType.CardType.SpecialAttack:
                    if (type == typeof(Character) && session is Character targetchara)
                    {
                        if (SubType == 3)
                        {
                            targetchara.RangedDisable = true;
                        }
                    }
                    break;

                case BCardType.CardType.SpecialDefence:
                    break;

                case BCardType.CardType.AttackPower:
                    break;

                case BCardType.CardType.Target:
                    break;

                case BCardType.CardType.Critical:
                    break;

                case BCardType.CardType.SpecialCritical:
                    break;

                case BCardType.CardType.Element:
                    break;

                case BCardType.CardType.IncreaseDamage:
                    break;

                case BCardType.CardType.Defence:
                    break;

                case BCardType.CardType.DodgeAndDefencePercent:
                    break;

                case BCardType.CardType.Block:
                    break;

                case BCardType.CardType.Absorption:
                    break;

                case BCardType.CardType.ElementResistance:
                    break;

                case BCardType.CardType.EnemyElementResistance:
                    break;

                case BCardType.CardType.Damage:
                    break;

                case BCardType.CardType.GuarantedDodgeRangedAttack:
                    break;

                case BCardType.CardType.Morale:
                    break;

                case BCardType.CardType.Casting:
                    break;

                case BCardType.CardType.Reflection:

                    if (type == typeof(Character))
                    {
                        if (session is Character character)
                        {
                            int percentage = 0;
                            if (SubType == 5)
                            {
                                percentage = -FirstData;
                                if (percentage >= ServerManager.RandomNumber(0, 100))
                                {
                                    int Mp = character.Mp;
                                    int MpReduce = (Mp / 100) * SecondData;

                                    int MpProtect = character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ProtectMPInPVP)?.Value ?? 0;
                                    MpReduce -= MpProtect;
                                    if (MpReduce > 0)
                                    {
                                        if (character.Mp - MpReduce < 0)
                                        {
                                            character.Mp = 0;
                                        }
                                        else
                                        {
                                            character.Mp -= MpReduce;
                                        }
                                    }
                                }
                            }
                            character.GenerateStat();
                        }
                    }
                    else if (type == typeof(MapMonster))
                    {
                        if (session is MapMonster mapm)
                        {
                            int percentage = 0;
                            if (SubType == 5)
                            {
                                percentage = -FirstData;
                                if (percentage >= ServerManager.RandomNumber(0, 100))
                                {
                                    int Mp = mapm.CurrentMp;
                                    int MpReduce = (Mp / 100) * SecondData;
                                    if (mapm.CurrentMp - MpReduce < 0)
                                    {
                                        mapm.CurrentMp = 0;
                                    }
                                    else
                                    {
                                        mapm.CurrentMp -= MpReduce;
                                    }
                                }
                            }
                        }
                    }
                    else if (type == typeof(MapNpc))
                    {
                    }
                    else if (type == typeof(Mate))
                    {
                        if (session is Mate mapm)
                        {
                            int percentage = 0;
                            if (SubType == 5)
                            {
                                percentage = -FirstData;
                                if (percentage >= ServerManager.RandomNumber(0, 100))
                                {
                                    int Mp = mapm.Mp;
                                    int MpReduce = (Mp / 100) * SecondData;
                                    if (mapm.Mp - MpReduce < 0)
                                    {
                                        mapm.Mp = 0;
                                    }
                                    else
                                    {
                                        mapm.Mp -= MpReduce;
                                    }
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.DrainAndSteal:
                    {
                        if (type == typeof(Character))
                        {
                            if (session is Character character && character.Hp > 0)
                            {
                                if (SubType == (byte)AdditionalTypes.DrainAndSteal.LeechEnemyHP / 10)
                                {
                                    if (sender != null)
                                    {
                                        Type sType = sender.GetType();
                                        if (sType != null)
                                        {
                                            if (sType == typeof(Character) && sender is Character sendingCharacter)
                                            {
                                                if (ServerManager.RandomNumber() < -FirstData)
                                                {
                                                    character.Hp -= SecondData * sendingCharacter.SwitchLevel();
                                                    if (character.Hp <= 0)
                                                    {
                                                        character.Hp = 1;
                                                    }
                                                    character.Session.CurrentMapInstance?.Broadcast($"dm 1 {character.CharacterId} {SecondData * sendingCharacter.SwitchLevel()}");
                                                    character.Session.SendPacket(character.GenerateStat());
                                                    sendingCharacter.Hp += SecondData * sendingCharacter.SwitchLevel();
                                                    if (sendingCharacter.Hp > sendingCharacter.HPLoad())
                                                    {
                                                        sendingCharacter.Hp = (int)sendingCharacter.HPLoad();
                                                    }
                                                    sendingCharacter.Session.CurrentMapInstance?.Broadcast(sendingCharacter.GenerateRc(SecondData * sendingCharacter.SwitchLevel()));
                                                    sendingCharacter.Session.SendPacket(sendingCharacter.GenerateStat());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (type == typeof(MapMonster))
                        {
                            if (SubType == (byte)AdditionalTypes.DrainAndSteal.LeechEnemyHP / 10)
                            {
                                if (session is MapMonster mapMonster)
                                {
                                    if (sender != null)
                                    {
                                        Type sType = sender.GetType();
                                        if (sType != null)
                                        {
                                            if (sType == typeof(Character) && sender is Character sendingCharacter)
                                            {
                                                if (ServerManager.RandomNumber() < -FirstData)
                                                {
                                                    mapMonster.CurrentHp -= SecondData * sendingCharacter.SwitchLevel();
                                                    if (mapMonster.CurrentHp <= 0)
                                                    {
                                                        mapMonster.CurrentHp = 1;
                                                    }
                                                    sendingCharacter.Session.CurrentMapInstance?.Broadcast($"dm 1 {mapMonster.MapMonsterId} {SecondData * sendingCharacter.SwitchLevel()}");
                                                    sendingCharacter.Hp += SecondData * sendingCharacter.SwitchLevel();
                                                    if (sendingCharacter.Hp > sendingCharacter.HPLoad())
                                                    {
                                                        sendingCharacter.Hp = (int)sendingCharacter.HPLoad();
                                                    }
                                                    sendingCharacter.Session.CurrentMapInstance?.Broadcast(sendingCharacter.GenerateRc(SecondData * sendingCharacter.SwitchLevel()));
                                                    sendingCharacter.Session.SendPacket(sendingCharacter.GenerateStat());
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.HealingBurningAndCasting:
                    if (type == typeof(Character))
                    {
                        if (session is Character character && character.Hp > 0)
                        {
                            int bonus = 0;
                            if (SubType == (byte)AdditionalTypes.HealingBurningAndCasting.RestoreHP / 10)
                            {
                                if (IsLevelScaled)
                                {
                                    bonus = character.SwitchLevel() * FirstData;
                                }
                                else
                                {
                                    bonus = FirstData;
                                }
                                if (character.Hp + bonus <= character.HPLoad())
                                {
                                    character.Hp += bonus;
                                }
                                else
                                {
                                    bonus = (int)character.HPLoad() - character.Hp;
                                    character.Hp = (int)character.HPLoad();
                                }
                                character.Session.CurrentMapInstance?.Broadcast(character.Session, character.GenerateRc(bonus));
                            }
                            if (SubType == (byte)AdditionalTypes.HealingBurningAndCasting.RestoreMP / 10)
                            {
                                if (IsLevelScaled)
                                {
                                    bonus = character.SwitchLevel() * FirstData;
                                }
                                else
                                {
                                    bonus = FirstData;
                                }
                                if (character.Mp + bonus <= character.MPLoad())
                                {
                                    character.Mp += bonus;
                                }
                                else
                                {
                                    bonus = (int)character.MPLoad() - character.Mp;
                                    character.Mp = (int)character.MPLoad();
                                }
                            }
                            character.Session.SendPacket(character.GenerateStat());
                        }
                    }
                    else if (type == typeof(MapMonster))
                    {
                        if (ServerManager.RandomNumber() < FirstData && session is MapMonster mapMonster)
                        {
                            mapMonster.AddBuff(new Buff((short)SecondData, mapMonster.Monster.Level));
                        }
                    }
                    else if (type == typeof(MapNpc))
                    {
                    }
                    else if (type == typeof(Mate))
                    {
                    }
                    break;

                case BCardType.CardType.HPMP:
                    if (type == typeof(Character))
                    {
                        if (session is Character character)
                        {
                            if (SubType == ((byte)AdditionalTypes.HPMP.DecreaseRemainingMP / 10))
                            {
                                character.Mp += (int)(character.Mp * (FirstData / 100D));
                                if (character.Mp < 0)
                                {
                                    character.Mp = 0;
                                }
                            }
                        }
                    }
                    else if (type == typeof(Mate))
                    {
                        if (session is Mate matemp)
                        {
                            if (SubType == ((byte)AdditionalTypes.HPMP.DecreaseRemainingMP / 10))
                            {
                                matemp.Mp += (int)(matemp.Mp * (FirstData / 100D));
                                if (matemp.Mp < 0)
                                {
                                    matemp.Mp = 0;
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.SpecialisationBuffResistance:
                    if (type == typeof(Character))
                    {
                        if (session is Character character)
                        {
                            if (SubType == ((byte)AdditionalTypes.SpecialisationBuffResistance.RemoveBadEffects / 10))
                            {
                                if (FirstData > 0)
                                {
                                    if (FirstData > ServerManager.RandomNumber(0, 100))
                                    {
                                        foreach (Buff b in character.Buff.Where(s => s.Card.Level <= SecondData && s.Card.BuffType == BuffType.Good && !s.StaticBuff))
                                        {
                                            character.RemoveBuff(b.Card.CardId);
                                        }
                                    }
                                }
                                else
                                {
                                    int FirstDat = Math.Abs(FirstData);
                                    if (FirstDat > ServerManager.RandomNumber(0, 100))
                                    {
                                        foreach (Buff b in character.Buff.Where(s => s.Card.Level <= SecondData && s.Card.BuffType == BuffType.Bad && !s.StaticBuff))
                                        {
                                            character.RemoveBuff(b.Card.CardId);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.SpecialEffects:
                    if (session is Character character3 && SubType.Equals((byte)AdditionalTypes.SpecialEffects.ShadowAppears / 10))
                    {
                        character3.MapInstance.Broadcast($"guri 0 {(short)UserType.Player} {character3.MapId} {FirstData} {SecondData}");
                    }
                    break;

                case BCardType.CardType.Capture:
                    if (type == typeof(MapMonster))
                    {
                        if (session is MapMonster mapMonster && sender is ClientSession senderSession)
                        {
                            NpcMonster mateNpc = ServerManager.GetNpc(mapMonster.MonsterVNum);
                            if (mateNpc != null)
                            {
                                if (mapMonster.Monster.Catch)
                                {
                                    if (mapMonster.IsAlive && mapMonster.CurrentHp <= (int)((double)mapMonster.MaxHp / 2))
                                    {
                                        if (mapMonster.Monster.Level < senderSession.Character.SwitchLevel())
                                        {
#warning find a new algorithm
                                            int[] chance = { 100, 80, 60, 40, 20, 0 };
                                            if (ServerManager.RandomNumber() < chance[ServerManager.RandomNumber(0, 5)])
                                            {
                                                if (senderSession.Character.Quests.Any(q => q.Quest.QuestType == (int)QuestType.Capture1 && q.Quest.QuestObjectives.Any(d => d.Data == mapMonster.MonsterVNum)))
                                                {
                                                    senderSession.Character.IncrementQuests(QuestType.Capture1, mapMonster.MonsterVNum);
                                                    return;
                                                }
                                                senderSession.Character.IncrementQuests(QuestType.Capture2, mapMonster.MonsterVNum);
                                                Mate mate = new Mate(senderSession.Character, mateNpc, (byte)(mapMonster.Monster.Level - 15 > 0 ? mapMonster.Monster.Level - 15 : 1), MateType.Pet);
                                                if (senderSession.Character.CanAddMate(mate))
                                                {
                                                    senderSession.Character.AddPetWithSkill(mate);
                                                    senderSession.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CATCH_SUCCESS"), 0));
                                                    senderSession.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, senderSession.Character.CharacterId, 197));
                                                    senderSession.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player, senderSession.Character.CharacterId, 3, mapMonster.MapMonsterId, -1, 0, 15, -1, -1, -1, true, (int)((float)mapMonster.CurrentHp / (float)mapMonster.MaxHp * 100), 0, -1, 0));
                                                    mapMonster.SetDeathStatement();
                                                    senderSession.CurrentMapInstance?.Broadcast(StaticPacketHelper.Out(UserType.Monster, mapMonster.MapMonsterId));
                                                }
                                                else
                                                {
                                                    senderSession.SendPacket(senderSession.Character.GenerateSay(Language.Instance.GetMessageFromKey("PET_SLOT_FULL"), 10));
                                                    senderSession.SendPacket(StaticPacketHelper.Cancel(2, mapMonster.MapMonsterId));
                                                }
                                            }
                                            else
                                            {
                                                senderSession.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CATCH_FAIL"), 0));
                                                senderSession.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player, senderSession.Character.CharacterId, 3, mapMonster.MapMonsterId, -1, 0, 15, -1, -1, -1, true, (int)((float)mapMonster.CurrentHp / (float)mapMonster.MaxHp * 100), 0, -1, 0));
                                            }
                                        }
                                        else
                                        {
                                            senderSession.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("LEVEL_LOWER_THAN_MONSTER"), 0));
                                            senderSession.SendPacket(StaticPacketHelper.Cancel(2, mapMonster.MapMonsterId));
                                        }
                                    }
                                    else
                                    {
                                        senderSession.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CURRENT_HP_TOO_HIGH"), 0));
                                        senderSession.SendPacket(StaticPacketHelper.Cancel(2, mapMonster.MapMonsterId));
                                    }
                                }
                                else
                                {
                                    senderSession.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("MONSTER_CANT_BE_CAPTURED"), 0));
                                    senderSession.SendPacket(StaticPacketHelper.Cancel(2, mapMonster.MapMonsterId));
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.SpecialDamageAndExplosions:
                    break;

                case BCardType.CardType.SpecialEffects2:
                    if (type == typeof(Character))
                    {
                        if (session is Character character)
                        {
                            if (SubType == ((byte)AdditionalTypes.SpecialEffects2.TeleportInRadius / 10))
                            {
                                switch (character.Direction)
                                {
                                    case 0:
                                        // -y
                                        ServerManager.Instance.TeleportForward(character.Session, character.MapInstanceId, character.PositionX, (short)(character.PositionY - FirstData));
                                        break;
                                    case 1:
                                        // +x
                                        ServerManager.Instance.TeleportForward(character.Session, character.MapInstanceId, (short)(character.PositionX + FirstData), character.PositionY);
                                        break;
                                    case 2:
                                        // +y
                                        ServerManager.Instance.TeleportForward(character.Session, character.MapInstanceId, character.PositionX, (short)(character.PositionY + FirstData));
                                        break;
                                    case 3:
                                        // -x
                                        ServerManager.Instance.TeleportForward(character.Session, character.MapInstanceId, (short)(character.PositionX - FirstData), character.PositionY);
                                        break;
                                    case 4:
                                        ServerManager.Instance.TeleportForward(character.Session, character.MapInstanceId, (short)(character.PositionX - FirstData), (short)(character.PositionY - FirstData));
                                        // -x -y
                                        break;
                                    case 5:
                                        // +x +y
                                        ServerManager.Instance.TeleportForward(character.Session, character.MapInstanceId, (short)(character.PositionX - FirstData), (short)(character.PositionY - FirstData));
                                        break;
                                    case 6:
                                        // +x -y
                                        ServerManager.Instance.TeleportForward(character.Session, character.MapInstanceId, (short)(character.PositionX + FirstData), (short)(character.PositionY + FirstData));
                                        break;
                                    case 7:
                                        // -x +y
                                        ServerManager.Instance.TeleportForward(character.Session, character.MapInstanceId, (short)(character.PositionX - FirstData), (short)(character.PositionY + FirstData));
                                        break;
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.CalculatingLevel:
                    break;

                case BCardType.CardType.Recovery:
                    break;

                case BCardType.CardType.MaxHPMP:
                    {
                        if (session is Character characta)
                        {
                            characta.Session.SendPacket(characta.GenerateStat());
                        }
                    }
                    break;

                case BCardType.CardType.MultAttack:
                    break;

                case BCardType.CardType.MultDefence:
                    break;

                case BCardType.CardType.TimeCircleSkills:
                    break;

                case BCardType.CardType.RecoveryAndDamagePercent:
                    if (type == typeof(Character))
                    {
                        if (session is Character character && character.Hp > 0)
                        {
                            if (SubType == (byte)AdditionalTypes.RecoveryAndDamagePercent.HPRecovered/10)
                            {
                                int currentHP = character.Hp;
                                int maxHP = character.HPMax;
                                int restHp = maxHP - currentHP;
                                restHp /= 5;

                                for (int i = 0; i <= 8000; i += 2000)
                                    System.Threading.Tasks.Task.Delay(i).ContinueWith(t => {
                                        character.Hp += restHp;
                                        character.Session.CurrentMapInstance.Broadcast($"rc 1 {character.Session.Character.CharacterId} {restHp} 0");
                                    });
                            }
                        }
                    }
                    break;

                case BCardType.CardType.Count:
                    break;

                case BCardType.CardType.NoDefeatAndNoDamage:
                    break;

                case BCardType.CardType.SpecialActions:
                    {
                        if (session is Character charact)
                        {
                            if (SubType == ((byte)AdditionalTypes.SpecialActions.Hide / 10))
                            {
                                charact.Invisible = true;
                                charact.Mates.Where(s => s.IsTeamMember).ToList().ForEach(s => charact.Session.CurrentMapInstance?.Broadcast(s.GenerateOut()));
                                charact.Session.CurrentMapInstance?.Broadcast(charact.GenerateInvisible());
                            }
                            if (SubType == 2)
                            {
                                if (sender != null)
                                {
                                    Type sType = sender.GetType();
                                    if (sType != null)
                                    {
                                        int Forza = charact.Session.Character.GetBuff(CardType.AbsorbedSpirit, 20)[0];
                                        if (Forza <= ServerManager.RandomNumber(0, 100))
                                        {
                                            if (sType == typeof(Character) && sender is Character sendingCharacter)
                                            {
                                                charact.PositionX = sendingCharacter.PositionX;
                                                charact.PositionY = sendingCharacter.PositionY;
                                                charact.Session.CurrentMapInstance?.Broadcast($"guri 3 1 {charact.CharacterId} {sendingCharacter.PositionX} {sendingCharacter.PositionY} 3 4 2 -1");
                                            }
                                            else if (sType == typeof(MapMonster) && sender is MapMonster sendingMob)
                                            {
                                                charact.PositionX = sendingMob.MapX;
                                                charact.PositionY = sendingMob.MapY;
                                                charact.Session.CurrentMapInstance?.Broadcast($"guri 3 1 {charact.CharacterId} {sendingMob.MapX} {sendingMob.MapY} 3 4 2 -1");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (session is MapMonster mapmonsta)
                        {
                            if (SubType == 2)
                            {
                                if (target != null)
                                {
                                    Type sType = target.GetType();
                                    if (sType != null)
                                    {
                                        if (sType == typeof(Character) && target is Character sendingCharacter)
                                        {
                                            sendingCharacter.Session.CurrentMapInstance?.Broadcast($"guri 3 1 {sendingCharacter.CharacterId} {mapmonsta.MapX} {mapmonsta.MapY} 3 8 2 -1");
                                        }
                                        else if (sType == typeof(MapMonster) && target is MapMonster mapmm)
                                        {
                                            mapmonsta.MapInstance?.Broadcast($"guri 3 3 {mapmm.MapMonsterId} {mapmonsta.MapX} {mapmonsta.MapY} 3 8 2 -1");
                                        }
                                    }
                                }

                                if (sender != null)
                                {
                                    Type sType = sender.GetType();
                                    if (sType != null)
                                    {
                                        if (sType == typeof(Character) && sender is Character sendingCharacter)
                                        {
                                            sendingCharacter.Session.CurrentMapInstance?.Broadcast($"guri 3 3 {mapmonsta.MapMonsterId} {sendingCharacter.PositionX} {sendingCharacter.PositionY} 3 8 2 -1");
                                        }
                                        else if (sType == typeof(MapMonster) && sender is MapMonster mapmm)
                                        {
                                            mapmonsta.MapInstance?.Broadcast($"guri 3 1 {mapmm.MapMonsterId} {mapmonsta.MapX} {mapmonsta.MapY} 3 8 2 -1");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.Mode:
                    break;

                case BCardType.CardType.NoCharacteristicValue:
                    break;

                case BCardType.CardType.LightAndShadow:
                    if (type == typeof(Character))
                    {
                        if (session is Character character)
                        {
                            if (SubType == 2)
                            {
                                foreach (Buff b in character.Buff.Where(s => s.Card.Level <= FirstData && s.Card.BuffType == BuffType.Bad))
                                {
                                    character.RemoveBuff(b.Card.CardId);
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.Item:
                    break;

                case BCardType.CardType.DebuffResistance:
                    break;

                case BCardType.CardType.SpecialBehaviour:
                    break;

                case BCardType.CardType.Quest:
                    break;

                case BCardType.CardType.SecondSPCard:
                    if (type == typeof(Character))
                    {
                        if (session is Character character)
                        {
                            List<MonsterToSummon> summonParameters = new List<MonsterToSummon>();
                            for (int i = 0; i < FirstData; i++)
                            {
                                short x = (short)(ServerManager.RandomNumber(-3, 3) + character.MapX);
                                short y = (short)(ServerManager.RandomNumber(-3, 3) + character.MapY);
                                summonParameters.Add(new MonsterToSummon((short)SecondData, new MapCell { X = x, Y = y }, -1, false, owner: character));
                            }
                            if (ServerManager.RandomNumber() <= Math.Abs(ThirdData) || ThirdData == 0)
                            {
                                switch (SubType)
                                {
                                    case 2:
                                        EventHelper.Instance.RunEvent(new EventContainer(character.MapInstance, EventActionType.SPAWNMONSTERS, summonParameters));
                                        break;
                                }
                            }
                        }
                    }
                    else if (type == typeof(MapMonster))
                    {
                    }
                    else if (type == typeof(MapNpc))
                    {
                    }
                    else if (type == typeof(Mate))
                    {
                    }
                    break;

                case BCardType.CardType.SPCardUpgrade:
                    break;

                case BCardType.CardType.HugeSnowman:
                    break;

                case BCardType.CardType.Drain:
                    break;

                case BCardType.CardType.BossMonstersSkill:
                    break;

                case BCardType.CardType.LordHatus:
                    break;

                case BCardType.CardType.LordCalvinas:
                    break;

                case BCardType.CardType.SESpecialist:
                    {
                        if (type == typeof(Character) && session is Character character)
                        {
                            if (SubType == ((byte)AdditionalTypes.SESpecialist.LowerHPStrongerEffect / 10))
                            {
                                Buff buff = null;
                                if (character.Hp >= character.HPLoad() / 2)
                                {
                                    buff = new Buff((short)272, character.SwitchLevel());
                                    character.AddBuff(buff);
                                }
                                else if (character.Hp >= character.HPLoad() / 4)
                                {
                                    buff = new Buff((short)273, character.SwitchLevel());
                                    character.AddBuff(buff);
                                }
                                else
                                {
                                    buff = new Buff((short)274, character.SwitchLevel());
                                    character.AddBuff(buff);
                                }
                            }
                            if (SubType == ((byte)AdditionalTypes.SESpecialist.MovingAura / 10))
                            {
                                Buff buff = null;
                                if (sender != null)
                                {
                                    Type sType = sender.GetType();
                                    if (sType != null)
                                    {
                                        if (sType == typeof(Character) && sender is Character sendingCharacter)
                                        {
                                            if (sendingCharacter.CharacterId != character.CharacterId)
                                            {
                                                if (sendingCharacter.Buff.Any(s => s.Card.CardId == 138))
                                                {
                                                    buff = new Buff(270, character.SwitchLevel());
                                                    character.AddBuff(buff);
                                                }
                                                if (sendingCharacter.Buff.Any(s => s.Card.CardId == 139))
                                                {
                                                    buff = new Buff(271, character.SwitchLevel());
                                                    character.AddBuff(buff);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.FourthGlacernonFamilyRaid:
                    break;

                case BCardType.CardType.SummonedMonsterAttack:
                    break;

                case BCardType.CardType.BearSpirit:
                    break;

                case BCardType.CardType.SummonSkill:
                    break;

                case BCardType.CardType.InflictSkill:
                    break;

                case BCardType.CardType.HideBarrelSkill:
                    break;

                case BCardType.CardType.FocusEnemyAttentionSkill:
                    break;

                case BCardType.CardType.TauntSkill:
                    {
                        if (type == typeof(Character) && session is Character character)
                        {
                            if (SubType == ((byte)AdditionalTypes.TauntSkill.TauntWhenKnockdown / 10))
                            {
                                int KnockDown = character.Buff.Where(s => s.Card.CardId == 500).Count();


                                #region Chance
                                int Chance = 100;

                                #region ShellAntiMalusGlobal
                                double Debuffmultipicator = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedAllNegativeEffect)?.Value / 100D) ?? 0;
                                double DebuffReal = 1 - Debuffmultipicator;
                                Chance = (int)(Chance * DebuffReal);
                                #endregion
                                double DebuffmultipicatorStun = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedStun)?.Value / 100D) ?? 0;
                                double DebuffRealStun = 1 - DebuffmultipicatorStun;
                                Chance = (int)(Chance * DebuffRealStun);

                                double DebuffmultiplicatorAllStun = (character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.ReducedAllStun)?.Value / 100D) ?? 0;
                                double DebuffRealAllStun = 1 - DebuffmultiplicatorAllStun;
                                Chance = (int)(Chance * DebuffRealAllStun);
                                #endregion

                                if (KnockDown > 0)
                                {
                                    if (Chance >= ServerManager.RandomNumber())
                                    {
                                        character.AddBuff(new Buff(501, character.SwitchLevel()));
                                    }
                                }
                                else
                                {
                                    if (Chance >= ServerManager.RandomNumber())
                                    {
                                        character.AddBuff(new Buff(503, character.SwitchLevel()));
                                    }
                                }
                            }
                        }
                        if (type == typeof(MapMonster) && session is MapMonster mapmonster)
                        {
                            if (SubType == ((byte)AdditionalTypes.TauntSkill.TauntWhenKnockdown / 10))
                            {
                                int KnockDown = mapmonster.Buff.Where(s => s.Card.CardId == 500).Count();
                                if (KnockDown > 0)
                                {
                                    Buff buff = new Buff(501, mapmonster.Monster.Level);
                                    int AntimalusLevel = Math.Abs(mapmonster.GetBuff(CardType.DebuffResistance, 0)[0]);
                                    int AntimalusChance = mapmonster.GetBuff(CardType.DebuffResistance, 0)[1];
                                    int NoAntimalus = mapmonster.GetBuff(CardType.NoAntimalus, 0)[1];

                                    if (buff.Card.Level <= AntimalusLevel && NoAntimalus == 0)
                                    {
                                        if (ServerManager.RandomNumber(0, 100) >= AntimalusChance)
                                        {
                                            mapmonster.AddBuff(buff);
                                        }
                                    }
                                    else
                                    {
                                        mapmonster.AddBuff(buff);
                                    }
                                }
                                else
                                {
                                    Buff buff = new Buff(503, mapmonster.Monster.Level);
                                    int AntimalusLevel = Math.Abs(mapmonster.GetBuff(CardType.DebuffResistance, 0)[0]);
                                    int AntimalusChance = mapmonster.GetBuff(CardType.DebuffResistance, 0)[1];
                                    int NoAntimalus = mapmonster.GetBuff(CardType.NoAntimalus, 0)[1];

                                    if (buff.Card.Level <= AntimalusLevel && NoAntimalus == 0)
                                    {
                                        if (ServerManager.RandomNumber(0, 100) >= AntimalusChance)
                                        {
                                            mapmonster.AddBuff(buff);
                                        }
                                    }
                                    else
                                    {
                                        mapmonster.AddBuff(buff);
                                    }
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.FireCannoneerRangeBuff:
                    break;

                case BCardType.CardType.VulcanoElementBuff:
                    break;

                case BCardType.CardType.DamageConvertingSkill:
                    break;

                case BCardType.CardType.MeditationSkill:
                    {
                        if (type == typeof(Character) && session is Character character)
                        {
                            if (SkillVNum.HasValue && SubType.Equals((byte)AdditionalTypes.MeditationSkill.CausingChance / 10) && ServerManager.RandomNumber() < FirstData)
                            {
                                Skill skill = ServerManager.GetSkill(SkillVNum.Value);
                                Skill newSkill = ServerManager.GetSkill((short)SecondData);
                                Observable.Timer(TimeSpan.FromMilliseconds(100)).Subscribe(observer =>
                                {
                                    foreach (QuicklistEntryDTO quicklistEntry in character.QuicklistEntries.Where(s => s.Pos.Equals(skill.CastId) && s.Morph == character.Morph))
                                    {
                                        character.Session.SendPacket($"qset {quicklistEntry.Q1} {quicklistEntry.Q2} {quicklistEntry.Type}.{quicklistEntry.Slot}.{newSkill.CastId}.0");
                                    }
                                    character.Session.SendPacket($"mslot {newSkill.CastId} -1");
                                });
                                character.SkillComboCount++;
                                character.LastSkillComboUse = DateTime.Now;
                                if (skill.CastId > 10)
                                {
                                    // HACK this way
                                    Observable.Timer(TimeSpan.FromMilliseconds((skill.Cooldown * 100) + 500)).Subscribe(observer => character.Session.SendPacket(StaticPacketHelper.SkillReset(skill.CastId)));
                                }
                            }
                            switch (SubType)
                            {
                                case 2:
                                    character.MeditationDictionary[(short)SecondData] = DateTime.Now.AddSeconds(4);
                                    break;

                                case 3:
                                    character.MeditationDictionary[(short)SecondData] = DateTime.Now.AddSeconds(8);
                                    break;

                                case 4:
                                    character.MeditationDictionary[(short)SecondData] = DateTime.Now.AddSeconds(12);
                                    break;
                            }
                        }
                    }
                    break;

                case BCardType.CardType.FalconSkill:
                    {
                        if (session is Character charact)
                        {
                            if (SubType == ((byte)AdditionalTypes.FalconSkill.CausingChanceLocation / 10))
                            {
                                if (FirstData > ServerManager.RandomNumber(0, 100))
                                {
                                    if (session is Character character)
                                    {
                                        if (mapx == 0 && mapy == 0)
                                        {
                                            mapx = character.PositionX;
                                            mapy = character.PositionY;
                                        }
                                        MonsterToSummon mob = new MonsterToSummon((short)SecondData, new MapCell { X = mapx, Y = mapy }, -1, false, owner: character);
                                        EventHelper.Instance.RunEvent(new EventContainer(character.MapInstance, EventActionType.SPAWNMONSTER, mob));
                                    }
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.AbsorptionAndPowerSkill:
                    break;

                case BCardType.CardType.LeonaPassiveSkill:
                    break;

                case BCardType.CardType.FearSkill:
                    break;

                case BCardType.CardType.SniperAttack:
                    break;

                case BCardType.CardType.FrozenDebuff:
                    break;

                case BCardType.CardType.JumpBackPush:
                    {
                        if (type == typeof(Character) && session is Character character)
                        {
                            if (SubType == ((byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased / 10))
                            {
                                character.HitRate -= FirstData;
                            }
                            if (SubType == ((byte)AdditionalTypes.JumpBackPush.PushBackChance / 10))
                            {
                                if (sender != null)
                                {
                                    Type sType = sender.GetType();
                                    if (sType == typeof(Character) && sender is Character sendingCharacter)
                                    {
                                        if (ServerManager.RandomNumber() < FirstData)
                                        {
                                            switch (sendingCharacter.Direction)
                                            {
                                                case 0:
                                                    // -y
                                                    ServerManager.Instance.Push(character.Session, character.MapInstanceId, character.PositionX, (short)(character.PositionY - SecondData));
                                                    break;
                                                case 1:
                                                    // +x
                                                    ServerManager.Instance.Push(character.Session, character.MapInstanceId, (short)(character.PositionX + SecondData), character.PositionY);
                                                    break;
                                                case 2:
                                                    // +y
                                                    ServerManager.Instance.Push(character.Session, character.MapInstanceId, character.PositionX, (short)(character.PositionY + SecondData));
                                                    break;
                                                case 3:
                                                    // -x
                                                    ServerManager.Instance.Push(character.Session, character.MapInstanceId, (short)(character.PositionX - SecondData), character.PositionY);
                                                    break;
                                                case 4:
                                                    // -x -y
                                                    ServerManager.Instance.Push(character.Session, character.MapInstanceId, (short)(character.PositionX - SecondData), (short)(character.PositionY - SecondData));
                                                    break;
                                                case 5:
                                                    // +x +y
                                                    ServerManager.Instance.Push(character.Session, character.MapInstanceId, (short)(character.PositionX - SecondData), (short)(character.PositionY - SecondData));
                                                    break;
                                                case 6:
                                                    // +x -y
                                                    ServerManager.Instance.Push(character.Session, character.MapInstanceId, (short)(character.PositionX + SecondData), (short)(character.PositionY + SecondData));
                                                    break;
                                                case 7:
                                                    // -x +y
                                                    ServerManager.Instance.Push(character.Session, character.MapInstanceId, (short)(character.PositionX - SecondData), (short)(character.PositionY + SecondData));
                                                    break;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            if (SubType == ((byte)AdditionalTypes.JumpBackPush.JumpBackChance / 10))
                            {
                                if (sender != null)
                                {
                                    Type sType = sender.GetType();
                                    if (sType == typeof(Character) && sender is Character sendingCharacter)
                                    {
                                        if (ServerManager.RandomNumber() < FirstData)
                                        {
                                            switch (sendingCharacter.Direction)
                                            {
                                                case 0:
                                                    // -y
                                                    ServerManager.Instance.Push(sendingCharacter.Session, sendingCharacter.MapInstanceId, sendingCharacter.PositionX, (short)(sendingCharacter.PositionY + SecondData));
                                                    break;
                                                case 1:
                                                    // +x
                                                    ServerManager.Instance.Push(sendingCharacter.Session, sendingCharacter.MapInstanceId, (short)(sendingCharacter.PositionX - SecondData), sendingCharacter.PositionY);
                                                    break;
                                                case 2:
                                                    // +y
                                                    ServerManager.Instance.Push(sendingCharacter.Session, sendingCharacter.MapInstanceId, sendingCharacter.PositionX, (short)(sendingCharacter.PositionY - SecondData));
                                                    break;
                                                case 3:
                                                    // -x
                                                    ServerManager.Instance.Push(sendingCharacter.Session, sendingCharacter.MapInstanceId, (short)(sendingCharacter.PositionX + SecondData), sendingCharacter.PositionY);
                                                    break;
                                                case 4:
                                                    // -x -y
                                                    ServerManager.Instance.Push(sendingCharacter.Session, sendingCharacter.MapInstanceId, (short)(sendingCharacter.PositionX + SecondData), (short)(sendingCharacter.PositionY + SecondData));
                                                    break;
                                                case 5:
                                                    // +x +y
                                                    ServerManager.Instance.Push(sendingCharacter.Session, sendingCharacter.MapInstanceId, (short)(sendingCharacter.PositionX - SecondData), (short)(sendingCharacter.PositionY - SecondData));
                                                    break;
                                                case 6:
                                                    // +x -y
                                                    ServerManager.Instance.Push(sendingCharacter.Session, sendingCharacter.MapInstanceId, (short)(sendingCharacter.PositionX - SecondData), (short)(sendingCharacter.PositionY + SecondData));
                                                    break;
                                                case 7:
                                                    // -x +y
                                                    ServerManager.Instance.Push(sendingCharacter.Session, sendingCharacter.MapInstanceId, (short)(sendingCharacter.PositionX + SecondData), (short)(sendingCharacter.PositionY - SecondData));
                                                    break;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.FairyXPIncrease:
                    {
                        if (type == typeof(Character) && session is Character character)
                        {
                            if (SubType == ((byte)AdditionalTypes.FairyXPIncrease.TeleportToLocation / 10))
                            {
                                character.PositionX = mapx;
                                character.PositionY = mapy;
                                character.Session.CurrentMapInstance.Broadcast(character.GenerateTp());
                            }
                        }
                    }
                    break;

                case BCardType.CardType.SummonAndRecoverHP:
                    break;

                case BCardType.CardType.TeamArenaBuff:
                    break;

                case BCardType.CardType.ArenaCamera:
                    break;

                case BCardType.CardType.DarkCloneSummon:
                    break;

                case BCardType.CardType.AbsorbedSpirit:
                    break;

                case BCardType.CardType.AngerSkill:
                    if (type == typeof(Character) && session is Character targetchar)
                    {
                        if (SubType == 4)
                        {
                            targetchar.OnlyNormalAttacks = true;
                        }
                    }
                    break;

                case BCardType.CardType.MeteoriteTeleport:
                    {
                        if (type == typeof(Character) && session is Character character)
                        {
                            if (SubType == ((byte)AdditionalTypes.MeteoriteTeleport.CauseMeteoriteFall / 10))
                            {
                                if (character.Session.CurrentMapInstance != null)
                                {
                                    int numerometoriti = 10 + (character.SwitchLevel() / 4);
                                    Observable.Timer(TimeSpan.FromMilliseconds(1)).Subscribe(observere =>
                                    {
                                        MapInstance map = character.Session.CurrentMapInstance;
                                        short x = character.PositionX;
                                        short y = character.PositionY;
                                        for (int i = 0; i < numerometoriti; i++)
                                        {
                                            Observable.Timer(TimeSpan.FromMilliseconds(150)).Subscribe(observerds =>
                                            {
                                                try
                                                {
                                                    x += (short)ServerManager.RandomNumber(-5, 5);
                                                    y += (short)ServerManager.RandomNumber(-5, 5);
                                                    int circleId = map.GetNextMonsterId();
                                                    MapMonster circle = new MapMonster { MonsterVNum = 2018, MapX = x, MapY = y, MapMonsterId = circleId, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                                    circle.Initialize(map);
                                                    circle.NoAggresiveIcon = true;
                                                    map.AddMonster(circle);
                                                    map.Broadcast(circle.GenerateIn());
                                                    map.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, circleId, ServerManager.RandomNumber(4488, 4493)));
                                                    if (map.IsPVP || map.Map.MapTypes.Any(s =>
                                                        s.MapTypeId == (short)MapTypeEnum.Act4))
                                                    {
                                                        foreach (Character Character in map.GetCharactersInRange((short)x, (short)y, 1))
                                                        {
                                                            if (Character.CharacterId != character.CharacterId)
                                                            {
                                                                if (map.Map.MapTypes.Any(s =>
                                                                    s.MapTypeId == (short)MapTypeEnum.Act4))
                                                                {
                                                                    if (Character.Faction != character.Faction)
                                                                    {
                                                                        Character.GetDamage(character.SwitchLevel() * 10);
                                                                        if (Character.Hp <= 0)
                                                                        {
                                                                            Character.Hp = 1;
                                                                        }
                                                                        Character.Session.SendPacket(Character.GenerateStat());
                                                                    }
                                                                }
                                                                else if (map.Map.MapId == 2106)
                                                                {
                                                                    if (Character.Family != character.Family)
                                                                    {
                                                                        Character.GetDamage(character.SwitchLevel() * 10);
                                                                        if (Character.Hp <= 0)
                                                                        {
                                                                            Character.Hp = 1;
                                                                        }
                                                                        Character.Session.SendPacket(Character.GenerateStat());
                                                                    }
                                                                }
                                                                else if (Character.Group != null)
                                                                {
                                                                    if (character.Group != null)
                                                                    {
                                                                        if (character.Group != Character.Group)
                                                                        {
                                                                            Character.GetDamage(character.SwitchLevel() * 10);
                                                                            if (Character.Hp <= 0)
                                                                            {
                                                                                Character.Hp = 1;
                                                                            }
                                                                            Character.Session.SendPacket(Character.GenerateStat());
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        Character.GetDamage(character.SwitchLevel() * 10);
                                                                        if (Character.Hp <= 0)
                                                                        {
                                                                            Character.Hp = 1;
                                                                        }
                                                                        Character.Session.SendPacket(Character.GenerateStat());
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    Character.GetDamage(character.SwitchLevel() * 10);
                                                                    if (Character.Hp <= 0)
                                                                    {
                                                                        Character.Hp = 1;
                                                                    }
                                                                    Character.Session.SendPacket(Character.GenerateStat());

                                                                }
                                                            }
                                                        }
                                                    }
                                                    foreach (MapMonster mapm in map.GetListMonsterInRange((short)x, (short)y, 1))
                                                    {
                                                        if (mapm.MapMonsterId != circle.MapMonsterId)
                                                        {
                                                            mapm.CurrentHp -= character.SwitchLevel() * 10;
                                                            if (mapm.CurrentHp <= 0)
                                                            {
                                                                mapm.CurrentHp = 1;
                                                            }
                                                        }
                                                    }

                                                    map.RemoveMonster(circle);
                                                    map.Broadcast(StaticPacketHelper.Out(UserType.Monster, circle.MapMonsterId));
                                                    System.Threading.Thread.Sleep(100);
                                                }

                                                catch (Exception ex)
                                                {

                                                }
                                            });
                                        }
                                    });
                                }
                            }
                            if (SubType == ((byte)AdditionalTypes.MeteoriteTeleport.TeleportYouAndGroupToSavedLocation / 10))
                            {
                                if(character.MapInstance != character.MassTeleportMap)
                                {
                                    character.MassTeleportMap = character.MapInstance;
                                    character.MassTeleportMapX = character.PositionX;
                                    character.MassTeleportMapY = character.PositionY;
                                }
                                else
                                {
                                    IEnumerable<Character> charinrange = character.MapInstance.GetCharactersInRange(character.PositionX, character.PositionY, 5);
                                    foreach(Character c in charinrange)
                                    {
                                        c.PositionX = character.MassTeleportMapX;
                                        c.PositionY = character.MassTeleportMapY;
                                        c.Session.CurrentMapInstance.Broadcast(c.GenerateTp());
                                    }
                                    character.RemoveBuff(620);
                                    character.MassTeleportMap = null;
                                }
                            }
                        }
                    }
                    break;

                case BCardType.CardType.StealBuff:
                    break;

                case BCardType.CardType.Unknown:
                    break;

                case BCardType.CardType.EffectSummon:
                    break;

                case BCardType.CardType.Transform:
                    if (type == typeof(Character) && session is Character character2)
                    {
                        switch (SubType)
                        {
                            case 1:
                                {
                                    if (character2.Morph == 29)

                                    {
                                        character2.Morph = 30;
                                        character2.MapInstance.Broadcast(character2.GenerateCMode());
                                        character2.Session.SendPackets(character2.GenerateQuicklist());
                                        character2.RemoveBuff(677);
                                        character2.RemoveBuff(679);
                                        character2.Skills[1526] = new CharacterSkill
                                        {
                                            SkillVNum = 1526,
                                            CharacterId = character2.CharacterId
                                        };
                                    }
                                    else if (character2.Morph == 30)

                                    {
                                        character2.Morph = 29;
                                        character2.MapInstance.Broadcast(character2.GenerateCMode());
                                        character2.Session.SendPackets(character2.GenerateQuicklist());
                                        character2.RemoveBuff(676);
                                    }
                                }
                                break;
                        }
                    }

                    break;

                case BCardType.CardType.TeleportEnemy:
                    {
                        if (type == typeof(Character) && session is Character chara)
                        {

                            if (sender != null)
                            {
                                Type sType = sender.GetType();
                                if (sType == typeof(Character) && sender is Character sendingCharacter)
                                {
                                    if (SubType == 1)
                                    {
                                        chara.PositionX = sendingCharacter.PositionX;
                                        chara.PositionY = sendingCharacter.PositionY;
                                        chara.Session.CurrentMapInstance.Broadcast(chara.GenerateTp());
                                    }
                                }
                            }
                        }
                    }
                    break;

                default:
                    Logger.Warn($"Card Type {Type} not defined!");
                    break;
            }
        }

        #endregion
    }
}