﻿using OpenNos.Core;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("$Ch", PassNonParseablePacket = true, Authority = AuthorityType.nottogot)]
    public class DirectConnectPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(0)]
        public int Port { get; set; }

        #endregion

        #region Methods

        public override string ToString() => $"CH Command CH: {Port}";

        #endregion
    }
}