﻿using OpenNos.Core;
using OpenNos.Domain;

namespace OpenNos.GameObject.Packets.CommandPackets
{
    [PacketHeader("$HelpBuy", PassNonParseablePacket = true, Authority = AuthorityType.nottogot)]
    public class HelpBuyPacket : PacketDefinition
    {
        [PacketIndex(0, SerializeToEnd = true)]
        public string Contents { get; set; }
    }
}
