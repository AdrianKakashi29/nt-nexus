﻿////<auto-generated <- Codemaid exclusion for now (PacketIndex Order is important for maintenance)

using OpenNos.Core;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$MaestriaXP", PassNonParseablePacket = true, Authority = AuthorityType.nottogot)]
    public class MaestriaXpCommandPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$MaestriaXP";
        }
    }
}