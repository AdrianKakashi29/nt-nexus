﻿using OpenNos.Core;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("$ModCode", PassNonParseablePacket = true, Authority = AuthorityType.User)]
    public class ModCodePacket : PacketDefinition
    {
            #region Properties

            [PacketIndex(0)]
             public string OldCode { get; set; }

            [PacketIndex(1)]
            public string Code { get; set; }

        public static string ReturnHelp()
        {
            return "$ModCode OLDCODE NEWCODE";
        }

        #endregion
    }
}
