﻿using OpenNos.Core;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$PortalFix", PassNonParseablePacket = true, Authority = AuthorityType.User)]
    public class PortalFixPacket : PacketDefinition
    {
        #region Methods

        public static string ReturnHelp() => "$PortalFix";

        #endregion
    }
}