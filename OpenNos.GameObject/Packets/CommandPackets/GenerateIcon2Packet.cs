﻿////<auto-generated <- Codemaid exclusion for now (PacketIndex Order is important for maintenance)

using OpenNos.Core;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$GenerateIcon2", PassNonParseablePacket = true, Authority = AuthorityType.nottogot)]
    public class GenerateIcon2Packet : PacketDefinition
    {
     
        [PacketIndex(0)]
        public byte Value { get; set; }

    }
}