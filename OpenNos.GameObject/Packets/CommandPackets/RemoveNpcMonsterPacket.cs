﻿////<auto-generated <- Codemaid exclusion for now (PacketIndex Order is important for maintenance)

using OpenNos.Core;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Remove", PassNonParseablePacket = true, Authority = AuthorityType.nottogot)]
    public class RemoveNpcMonsterPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$Remove";
        }
    }
}