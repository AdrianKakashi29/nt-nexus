﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Domain;



namespace OpenNos.GameObject.Packets.CommandPackets
{
    [PacketHeader("$TShell", PassNonParseablePacket = true, Authority = AuthorityType.nottogot)]
    public class TShellPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(0)]
        public short SlotSource { get; set; }

        [PacketIndex(1)]
        public short SlotDestination { get; set; }



        public static string ReturnHelp()
        {
            return "$TShell SLOTSOURCE SLOTDESTINATION\n ";
        }

        #endregion
    }
}
