﻿using OpenNos.Core;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("$UseBlock", PassNonParseablePacket = true, Authority = AuthorityType.GameMaster)]
    public class UsaBloccoPacket : PacketDefinition
    {
    }
}