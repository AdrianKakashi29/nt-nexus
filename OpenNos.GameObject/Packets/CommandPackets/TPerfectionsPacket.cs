﻿using OpenNos.Core;
using OpenNos.Domain;
namespace OpenNos.GameObject.Packets.CommandPackets
{
    [PacketHeader("$TPerfections", PassNonParseablePacket = true, Authority = AuthorityType.GameMaster)]
    public class TPerfections : PacketDefinition
    {
        #region Properties

        [PacketIndex(0)]
        public short SlotSource { get; set; }

        [PacketIndex(1)]
        public short SlotDestination { get; set; }



        public static string ReturnHelp()
        {
            return "$TPerfections SLOTSOURCE SLOTDESTINATION\n check ItemType!\n ";
        }

        #endregion
    }
}
