﻿using OpenNos.Core;

namespace OpenNos.GameObject
{
    [PacketHeader("codiceblocco")]
    public class CodiceBloccoPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(0)]
        public string Codice { get; set; }

        #endregion
    }
}