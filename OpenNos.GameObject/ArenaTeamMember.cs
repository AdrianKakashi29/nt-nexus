﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace OpenNos.GameObject
{
    public class ArenaTeamMember
    {
        public ClientSession Session { get; set; }
        public ArenaTeamType ArenaTeamType { get; set; }
        public byte? Order { get; set; }
        public bool Dead { get; set; }
        public DateTime? LastSummoned { get; set; }
        public byte SummonCount { get; set; }

        public ArenaTeamMember(ClientSession session, ArenaTeamType arenaTeamType, byte? order)
        {
            Session = session;
            ArenaTeamType = arenaTeamType;
            Order = order;
        }
    }
}