﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Domain;
using System;
using System.Collections.Generic;

namespace OpenNos.GameObject.Helpers
{
        public class QuestHelper
        {
            #region Instantiation

            public QuestHelper()
            {
                LoadSkipQuests();
            }

            #endregion

            #region Properties

            public List<int> SkipQuests { get; set; }

            #endregion

            #region Methods

            public void LoadSkipQuests()
            {
                SkipQuests = new List<int>();
                SkipQuests.AddRange(new List<int> { 1676, 1677, 1698, 1714, 1715, 1719, 3014, 3019 });
            }

            #endregion

            #region Singleton

            private static QuestHelper _instance;

            public static QuestHelper Instance
            {
                get { return _instance ?? (_instance = new QuestHelper()); }
            }

            #endregion
        }
}
