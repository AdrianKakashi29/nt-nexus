﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Domain;
using System;
using System.Collections.Generic;

namespace OpenNos.GameObject.Helpers
{
    public class PassiveSkillHelper
    {
        public List<BCard> PassiveSkillToBcards(IEnumerable<CharacterSkill> skills)
        {
            List<BCard> bcards = new List<BCard>();

            foreach (CharacterSkill skill in skills)
            {
                switch (skill.Skill.CastId)
                {
                    case 4:
                        bcards.Add(new BCard
                        {
                            FirstData = skill.Skill.UpgradeSkill,
                            Type = (byte)BCardType.CardType.MaxHPMP,
                            SubType = (byte)AdditionalTypes.MaxHPMP.MaximumHPIncreased,
                        });
                        break;
                    case 5:
                        bcards.Add(new BCard
                        {
                            FirstData = skill.Skill.UpgradeSkill,
                            Type = (byte)BCardType.CardType.MaxHPMP,
                            SubType = (byte)AdditionalTypes.MaxHPMP.MaximumMPIncreased,
                        });
                        break;
                    case 8:
                        bcards.Add(new BCard
                        {
                            FirstData = skill.Skill.UpgradeSkill,
                            Type = (byte)BCardType.CardType.Recovery,
                            SubType = (byte)AdditionalTypes.Recovery.HPRecoveryIncreased,
                        });
                        break;
                    case 9:
                        bcards.Add(new BCard
                        {
                            FirstData = skill.Skill.UpgradeSkill,
                            Type = (byte)BCardType.CardType.Recovery,
                            SubType = (byte)AdditionalTypes.Recovery.MPRecoveryIncreased,
                        });
                        break;
                }

            }
            return bcards;
        }

        #region Singleton

        private static PassiveSkillHelper _instance;

        public static PassiveSkillHelper Instance
        {
            get { return _instance ?? (_instance = new PassiveSkillHelper()); }
        }

        #endregion
    }
}