﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Event.GAMES;
using OpenNos.PathFinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using OpenNos.GameObject.Networking;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;
using System.Threading;
using OpenNos.Data;
using OpenNos.DAL;
using OpenNos.GameObject.Event.Fortnite;
using OpenNos.GameObject.Event.BattleRoyale;

namespace OpenNos.GameObject.Helpers
{
    public class EventHelper
    {
        #region Members

        private static EventHelper _instance;

        #endregion

        #region Properties

        public static EventHelper Instance => _instance ?? (_instance = new EventHelper());
        public List<Character> Save = new List<Character>();

        #endregion

        #region Methods

        public static int CalculateComboPoint(int n)
        {
            int a = 4;
            int b = 7;
            for (int i = 0; i < n; i++)
            {
                int temp = a;
                a = b;
                b = temp + b;
            }
            return a;
        }

        public static void GenerateEvent(EventType type, bool userTime = true)
        {
            if (!ServerManager.Instance.StartedEvents.Contains(type))
            {
                Task.Factory.StartNew(() =>
                {
                    ServerManager.Instance.StartedEvents.Add(type);
                    switch (type)
                    {
                        case EventType.RANKINGREFRESH:
                            ServerManager.Instance.RefreshRanking();
                            ServerManager.Instance.StartedEvents.Remove(EventType.RANKINGREFRESH);
                            break;

                        /*case EventType.LOD:
                            LOD.GenerateLod();
                            break;*/

                        case EventType.MINILANDREFRESHEVENT:
                            MinilandRefresh.GenerateMinilandEvent();
                            break;

                        case EventType.INSTANTBATTLE:
                            InstantBattle.GenerateInstantBattle();
                            break;

                        /*case EventType.LODDH:
                            LOD.GenerateLod(55);
                            break;*/

                        case EventType.ICEBREAKER:
                            IceBreaker.GenerateIceBreaker(userTime);
                            break;

                        case EventType.METEORITEGAME:
                            MeteoriteGame.GenerateMeteoriteGame();
                            break;

                        case EventType.ACT4SHIP:
                            ACT4SHIP.GenerateAct4Ship(1);
                            ACT4SHIP.GenerateAct4Ship(2);
                            break;

                        case EventType.TALENTARENA:
                            TalentArena.Run();
                            break;

                        case EventType.BATTLEROYAL:
                            BattleRoyaleManager.Instance.Prepare(userTime);
                            break;

                        case EventType.FORTNITE:
                            FortniteManager.Instance.Prepare(userTime);
                            break;

                        case EventType.SHEEP:
                            Sheep.GenerateSheepGames();
                            break;

                        case EventType.ZOMBIECASTLE:
                            ZOMBIECASTLE.GenerateZombieCastleLobby();
                            break;

                        case EventType.MAPPAXPMAESTRIA:
                            MAPPAPERMAESTRIA.GenerateMappaperMaestria();
                            break;

                        case EventType.MAPPAXPMAESTRIASOLO:
                            MAPPAPERMAESTRIASOLA.GenerateMappaperMaestriaSola();
                            break;

                        case EventType.CALIGOR:
                            CaligorRaid.Run();
                            break;

                        case EventType.ARCOBALENO:
                            Arcobaleno.GenerateArcobaleno();
                            break;
                    }
                });
            }
        }

        public static TimeSpan GetMilisecondsBeforeTime(TimeSpan time)
        {
            TimeSpan now = TimeSpan.Parse(DateTime.Now.ToString("HH:mm"));
            TimeSpan timeLeftUntilFirstRun = time - now;
            if (timeLeftUntilFirstRun.TotalHours < 0)
            {
                timeLeftUntilFirstRun += new TimeSpan(24, 0, 0);
            }
            return timeLeftUntilFirstRun;
        }

        public void RunEvent(EventContainer evt, ClientSession session = null, MapMonster monster = null)
        {
            if (evt != null)
            {
                if (session != null)
                {
                    evt.MapInstance = session.CurrentMapInstance;
                    switch (evt.EventActionType)
                    {
                        #region EventForUser

                        case EventActionType.NPCDIALOG:
                            session.SendPacket(session.Character.GenerateNpcDialog((int)evt.Parameter));
                            break;

                        case EventActionType.SENDPACKET:
                            session.SendPacket((string)evt.Parameter);
                            break;

                            #endregion
                    }
                }
                if (evt.MapInstance != null)
                {
                    switch (evt.EventActionType)
                    {
                        #region EventForUser

                        case EventActionType.NPCDIALOG:
                        case EventActionType.SENDPACKET:
                            if (session == null)
                            {
                                evt.MapInstance.Sessions.ToList().ForEach(e => RunEvent(evt, e));
                            }
                            break;

                        #endregion

                        #region MapInstanceEvent

                        case EventActionType.REGISTEREVENT:
                            Tuple<string, List<EventContainer>> even = (Tuple<string, List<EventContainer>>)evt.Parameter;
                            switch (even.Item1)
                            {
                                case "OnCharacterDiscoveringMap":
                                    even.Item2.ForEach(s => evt.MapInstance.OnCharacterDiscoveringMapEvents.Add(new Tuple<EventContainer, List<long>>(s, new List<long>())));
                                    break;

                                case "OnMoveOnMap":
                                    evt.MapInstance.OnMoveOnMapEvents.AddRange(even.Item2);
                                    break;

                                case "OnMapClean":
                                    evt.MapInstance.OnMapClean.AddRange(even.Item2);
                                    break;

                                case "OnLockerOpen":
                                    evt.MapInstance.UnlockEvents.AddRange(even.Item2);
                                    break;
                            }
                            break;

                        case EventActionType.REGISTERWAVE:
                            evt.MapInstance.WaveEvents.Add((EventWave)evt.Parameter);
                            break;



                        case EventActionType.SETAREAENTRY:
                            ZoneEvent even2 = (ZoneEvent)evt.Parameter;
                            evt.MapInstance.OnAreaEntryEvents.Add(even2);
                            break;

                        case EventActionType.REMOVEMONSTERLOCKER:
                            EventContainer evt2 = (EventContainer)evt.Parameter;
                            if (evt.MapInstance.InstanceBag.MonsterLocker.Current > 0)
                            {
                                evt.MapInstance.InstanceBag.MonsterLocker.Current--;
                            }
                            if (evt.MapInstance.InstanceBag.MonsterLocker.Current == 0 && evt.MapInstance.InstanceBag.ButtonLocker.Current == 0)
                            {
                                evt.MapInstance.UnlockEvents.ForEach(s => RunEvent(s));
                                evt.MapInstance.UnlockEvents.RemoveAll(s => s != null);
                            }
                            break;

                        case EventActionType.CLEARMAPMONSTERS:
                            foreach (MapMonster m in evt.MapInstance.Monsters)
                            {
                                if (m != null)
                                {
                                    evt.MapInstance.RemoveMonster(m);
                                    evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, m.MapMonsterId));
                                }
                            }
                            break;

                        case EventActionType.REMOVEBUTTONLOCKER:
                            evt2 = (EventContainer)evt.Parameter;
                            if (evt.MapInstance.InstanceBag.ButtonLocker.Current > 0)
                            {
                                evt.MapInstance.InstanceBag.ButtonLocker.Current--;
                            }
                            if (evt.MapInstance.InstanceBag.MonsterLocker.Current == 0 && evt.MapInstance.InstanceBag.ButtonLocker.Current == 0)
                            {
                                evt.MapInstance.UnlockEvents.ForEach(s => RunEvent(s));
                                evt.MapInstance.UnlockEvents.RemoveAll(s => s != null);
                            }
                            break;

                        case EventActionType.EFFECT:
                            short evt3 = (short)evt.Parameter;
                            if (monster != null)
                            {
                                monster.LastEffect = DateTime.Now;
                                evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, monster.MapMonsterId, evt3));
                            }
                            break;

                        case EventActionType.CONTROLEMONSTERINRANGE:
                            if (monster != null)
                            {
                                Tuple<short, byte, List<EventContainer>> evnt = (Tuple<short, byte, List<EventContainer>>)evt.Parameter;
                                List<MapMonster> MapMonsters = evt.MapInstance.GetListMonsterInRange(monster.MapX, monster.MapY, evnt.Item2);
                                if (evnt.Item1 != 0)
                                {
                                    MapMonsters.RemoveAll(s => s.MonsterVNum != evnt.Item1);
                                }
                                MapMonsters.ForEach(s => evnt.Item3.ForEach(e => RunEvent(e, monster: s)));
                            }
                            break;

                        case EventActionType.ONTARGET:
                            if (monster.MoveEvent?.InZone(monster.MapX, monster.MapY) == true)
                            {
                                monster.MoveEvent = null;
                                monster.Path = new List<Node>();
                                ((List<EventContainer>)evt.Parameter).ForEach(s => RunEvent(s, monster: monster));
                            }
                            break;

                        case EventActionType.MOVE:
                            ZoneEvent evt4 = (ZoneEvent)evt.Parameter;
                            if (monster != null)
                            {
                                monster.MoveEvent = evt4;
                                monster.Path = BestFirstSearch.FindPathJagged(new Node { X = monster.MapX, Y = monster.MapY }, new Node { X = evt4.X, Y = evt4.Y }, evt.MapInstance?.Map.JaggedGrid);
                            }
                            break;

                        case EventActionType.CLOCK:
                            evt.MapInstance.InstanceBag.Clock.TotalSecondsAmount = Convert.ToInt32(evt.Parameter);
                            evt.MapInstance.InstanceBag.Clock.SecondsRemaining = Convert.ToInt32(evt.Parameter);
                            break;

                        case EventActionType.SETMONSTERLOCKERS:
                            evt.MapInstance.InstanceBag.MonsterLocker.Current = Convert.ToByte(evt.Parameter);
                            evt.MapInstance.InstanceBag.MonsterLocker.Initial = Convert.ToByte(evt.Parameter);
                            break;

                        case EventActionType.SETBUTTONLOCKERS:
                            evt.MapInstance.InstanceBag.ButtonLocker.Current = Convert.ToByte(evt.Parameter);
                            evt.MapInstance.InstanceBag.ButtonLocker.Initial = Convert.ToByte(evt.Parameter);
                            break;

                        case EventActionType.SCRIPTENDSP:
                            switch (evt.MapInstance.MapInstanceType)
                            {
                                case MapInstanceType.TimeSpaceInstance:
                                    evt.MapInstance.InstanceBag.EndState = (byte)evt.Parameter;
                                    ClientSession client = evt.MapInstance.Sessions.FirstOrDefault();
                                    if (client != null)
                                    {
                                        Guid MapInstanceId = ServerManager.GetBaseMapInstanceIdByMapId(client.Character.MapId);
                                        MapInstance map = ServerManager.GetMapInstance(MapInstanceId);
                                        ScriptedInstance si = map.ScriptedInstances.Find(s => s.ScriptedInstanceId == 121);
                                        string perfection = string.Empty;
                                        perfection += evt.MapInstance.InstanceBag.MonstersKilled >= si.MonsterAmount ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.NpcsKilled == 0 ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.RoomsVisited >= si.RoomAmount ? 1 : 0;
                                        client.Character.IncrementQuests(QuestType.TimesSpace, Convert.ToInt32(si.Label));
                                        evt.MapInstance.Broadcast($"score  {evt.MapInstance.InstanceBag.EndState} 25 27 47 18 {si.DrawItems.Count} {evt.MapInstance.InstanceBag.MonstersKilled} {si.NpcAmount - evt.MapInstance.InstanceBag.NpcsKilled} {evt.MapInstance.InstanceBag.RoomsVisited} {perfection} 1 15");
                                    }
                                    break;
                            }
                            break;


                        case EventActionType.SCRIPTEND3SP:
                            switch (evt.MapInstance.MapInstanceType)
                            {
                                case MapInstanceType.TimeSpaceInstance:
                                    evt.MapInstance.InstanceBag.EndState = (byte)evt.Parameter;
                                    ClientSession client = evt.MapInstance.Sessions.FirstOrDefault();
                                    if (client != null)
                                    {
                                        Guid MapInstanceId = ServerManager.GetBaseMapInstanceIdByMapId(client.Character.MapId);
                                        MapInstance map = ServerManager.GetMapInstance(MapInstanceId);
                                        ScriptedInstance si = map.ScriptedInstances.Find(s => s.LevelMinimum == 253);
                                        byte penalty = 0;
                                        if (penalty > (client.Character.SwitchLevel() - (si.LevelMinimum > 99 ? 0 : si.LevelMinimum)) * 2)
                                        {
                                            penalty = penalty > 100 ? (byte)100 : penalty;
                                            client.SendPacket(client.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("TS_PENALTY"), penalty), 10));
                                        }

                                        int point = evt.MapInstance.InstanceBag.Point * (100 - penalty) / 100;
                                        string perfection = string.Empty;
                                        perfection += evt.MapInstance.InstanceBag.MonstersKilled >= si.MonsterAmount ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.NpcsKilled == 0 ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.RoomsVisited >= si.RoomAmount ? 1 : 0;
                                        client.Character.IncrementQuests(QuestType.TimesSpace, Convert.ToInt32(si.Label));
                                        evt.MapInstance.Broadcast($"score  {evt.MapInstance.InstanceBag.EndState} {point} 27 47 18 {si.DrawItems.Count} {evt.MapInstance.InstanceBag.MonstersKilled} {si.NpcAmount - evt.MapInstance.InstanceBag.NpcsKilled} {evt.MapInstance.InstanceBag.RoomsVisited} {perfection} 1 15");
                                    }
                                    break;
                            }
                            break;

                        case EventActionType.SCRIPTEND2SP:
                            switch (evt.MapInstance.MapInstanceType)
                            {
                                case MapInstanceType.TimeSpaceInstance:
                                    evt.MapInstance.InstanceBag.EndState = (byte)evt.Parameter;
                                    ClientSession client = evt.MapInstance.Sessions.FirstOrDefault();
                                    if (client != null)
                                    {
                                        Guid MapInstanceId = ServerManager.GetBaseMapInstanceIdByMapId(client.Character.MapId);
                                        MapInstance map = ServerManager.GetMapInstance(MapInstanceId);
                                        ScriptedInstance si = map.ScriptedInstances.Find(s => s.LevelMinimum == 252);
                                        byte penalty = 0;
                                        if (penalty > (client.Character.SwitchLevel() - (si.LevelMinimum > 99 ? 0 : si.LevelMinimum)) * 2)
                                        {
                                            penalty = penalty > 100 ? (byte)100 : penalty;
                                            client.SendPacket(client.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("TS_PENALTY"), penalty), 10));
                                        }

                                        int point = evt.MapInstance.InstanceBag.Point * (100 - penalty) / 100;
                                        string perfection = string.Empty;
                                        perfection += evt.MapInstance.InstanceBag.MonstersKilled >= si.MonsterAmount ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.NpcsKilled == 0 ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.RoomsVisited >= si.RoomAmount ? 1 : 0;
                                        client.Character.IncrementQuests(QuestType.TimesSpace, Convert.ToInt32(si.Label));
                                        evt.MapInstance.Broadcast($"score  {evt.MapInstance.InstanceBag.EndState} {point} 27 47 18 {si.DrawItems.Count} {evt.MapInstance.InstanceBag.MonstersKilled} {si.NpcAmount - evt.MapInstance.InstanceBag.NpcsKilled} {evt.MapInstance.InstanceBag.RoomsVisited} {perfection} 1 15");
                                    }
                                    break;
                            }
                            break;


                        case EventActionType.SCRIPTEND41SP:
                            switch (evt.MapInstance.MapInstanceType)
                            {
                                case MapInstanceType.TimeSpaceInstance:
                                    evt.MapInstance.InstanceBag.EndState = (byte)evt.Parameter;
                                    ClientSession client = evt.MapInstance.Sessions.FirstOrDefault();
                                    if (client != null)
                                    {
                                        Guid MapInstanceId = ServerManager.GetBaseMapInstanceIdByMapId(client.Character.MapId);
                                        MapInstance map = ServerManager.GetMapInstance(MapInstanceId);
                                        ScriptedInstance si = map.ScriptedInstances.Find(s => s.LevelMinimum == 254);
                                        byte penalty = 0;
                                        if (penalty > (client.Character.SwitchLevel() - (si.LevelMinimum > 99 ? 0 : si.LevelMinimum)) * 2)
                                        {
                                            penalty = penalty > 100 ? (byte)100 : penalty;
                                            client.SendPacket(client.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("TS_PENALTY"), penalty), 10));
                                        }

                                        int point = evt.MapInstance.InstanceBag.Point * (100 - penalty) / 100;
                                        string perfection = string.Empty;
                                        perfection += evt.MapInstance.InstanceBag.MonstersKilled >= si.MonsterAmount ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.NpcsKilled == 0 ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.RoomsVisited >= si.RoomAmount ? 1 : 0;
                                        client.Character.IncrementQuests(QuestType.TimesSpace, Convert.ToInt32(si.Label));
                                        evt.MapInstance.Broadcast($"score  {evt.MapInstance.InstanceBag.EndState} {point} 27 47 18 {si.DrawItems.Count} {evt.MapInstance.InstanceBag.MonstersKilled} {si.NpcAmount - evt.MapInstance.InstanceBag.NpcsKilled} {evt.MapInstance.InstanceBag.RoomsVisited} {perfection} 1 15");
                                    }
                                    break;
                            }
                            break;

                        case EventActionType.SCRIPTEND5SP:
                            switch (evt.MapInstance.MapInstanceType)
                            {
                                case MapInstanceType.TimeSpaceInstance:
                                    evt.MapInstance.InstanceBag.EndState = (byte)evt.Parameter;
                                    ClientSession client = evt.MapInstance.Sessions.FirstOrDefault();
                                    if (client != null)
                                    {
                                        Guid MapInstanceId = ServerManager.GetBaseMapInstanceIdByMapId(client.Character.MapId);
                                        MapInstance map = ServerManager.GetMapInstance(MapInstanceId);
                                        ScriptedInstance si = map.ScriptedInstances.Find(s => s.LevelMinimum == 255);
                                        byte penalty = 0;
                                        if (penalty > (client.Character.SwitchLevel() - (si.LevelMinimum > 99 ? 0 : si.LevelMinimum)) * 2)
                                        {
                                            penalty = penalty > 100 ? (byte)100 : penalty;
                                            client.SendPacket(client.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("TS_PENALTY"), penalty), 10));
                                        }

                                        int point = evt.MapInstance.InstanceBag.Point * (100 - penalty) / 100;
                                        string perfection = string.Empty;
                                        perfection += evt.MapInstance.InstanceBag.MonstersKilled >= si.MonsterAmount ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.NpcsKilled == 0 ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.RoomsVisited >= si.RoomAmount ? 1 : 0;
                                        client.Character.IncrementQuests(QuestType.TimesSpace, Convert.ToInt32(si.Label));
                                        evt.MapInstance.Broadcast($"score  {evt.MapInstance.InstanceBag.EndState} {point} 27 47 18 {si.DrawItems.Count} {evt.MapInstance.InstanceBag.MonstersKilled} {si.NpcAmount - evt.MapInstance.InstanceBag.NpcsKilled} {evt.MapInstance.InstanceBag.RoomsVisited} {perfection} 1 15");
                                    }
                                    break;
                            }
                            break;

                        case EventActionType.SCRIPTEND:
                            switch (evt.MapInstance.MapInstanceType)
                            {
                                case MapInstanceType.TimeSpaceInstance:
                                    evt.MapInstance.InstanceBag.EndState = (byte)evt.Parameter;
                                    ClientSession client = evt.MapInstance.Sessions.FirstOrDefault();
                                    if (client != null)
                                    {
                                        Guid MapInstanceId = ServerManager.GetBaseMapInstanceIdByMapId(client.Character.MapId);
                                        MapInstance map = ServerManager.GetMapInstance(MapInstanceId);
                                        ScriptedInstance si = map.ScriptedInstances.Find(s => s.PositionX == client.Character.MapX && s.PositionY == client.Character.MapY);
                                        byte penalty = 0;
                                        if (penalty > (client.Character.SwitchLevel() - (si.LevelMinimum > 99 ? 0 : si.LevelMinimum)) * 2)
                                        {
                                            penalty = penalty > 100 ? (byte)100 : penalty;
                                            client.SendPacket(client.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("TS_PENALTY"), penalty), 10));
                                        }
                                        int point = evt.MapInstance.InstanceBag.Point * (100 - penalty) / 100;
                                        string perfection = string.Empty;
                                        perfection += evt.MapInstance.InstanceBag.MonstersKilled >= si.MonsterAmount ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.NpcsKilled == 0 ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.RoomsVisited >= si.RoomAmount ? 1 : 0;
                                        client.Character.IncrementQuests(QuestType.TimesSpace, si.LevelMinimum);
                                        evt.MapInstance.Broadcast($"score  {evt.MapInstance.InstanceBag.EndState} {point} 27 47 18 {si.DrawItems.Count} {evt.MapInstance.InstanceBag.MonstersKilled} {si.NpcAmount - evt.MapInstance.InstanceBag.NpcsKilled} {evt.MapInstance.InstanceBag.RoomsVisited} {perfection} 1 1");
                                    }
                                    break;

                                case MapInstanceType.RaidInstance:
                                    evt.MapInstance.InstanceBag.EndState = (byte)evt.Parameter;
                                    client = evt.MapInstance.Sessions.FirstOrDefault();
                                    if (client != null)
                                    {
                                        Group grp = client?.Character?.Group;
                                        if (grp == null)
                                        {
                                            return;
                                        }
                                        if (evt.MapInstance.InstanceBag.EndState == 1 && evt.MapInstance.Monsters.Any(s => s.IsHostile))
                                        {
                                            bool FamilyRaid = true;
                                            foreach (ClientSession sess in grp.Characters.Where(s => s.CurrentMapInstance.Monsters.Any(e => e.IsHostile)))
                                            {
                                                foreach (Gift gift in grp?.Raid?.GiftItems)
                                                {
                                                    const byte rare = 0;
                                                    if (sess.Character.SwitchLevel() > grp?.Raid?.LevelMaximum)
                                                    {
                                                        if (sess.Character.GeneralLogs.CountLinq(s => s.LogType == "Raid Supporter" && s.LogData == grp?.Raid?.Label && s.Timestamp.Date == DateTime.Today) > 0)
                                                        {
                                                        }
                                                        else
                                                        {
                                                            sess.Character.GeneralLogs.Add(new GeneralLogDTO
                                                            {
                                                                AccountId = sess.Account.AccountId,
                                                                CharacterId = sess.Character.CharacterId,
                                                                IpAddress = sess.IpAddress,
                                                                LogData = grp?.Raid?.Label,
                                                                LogType = "Raid Supporter",
                                                                Timestamp = DateTime.Now
                                                            });
                                                            sess.Character.GiftAdd(2320, 1, 0, 0);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        // TODO: add random rarity for some object
                                                        sess.Character.GiftAdd(gift.VNum, gift.Amount, rare, 0, gift.Design, gift.IsRandomRare, raid: grp?.Raid.Label);
                                                    }
                                                }
                                                if (grp?.Raid?.FXP > 0)
                                                {
                                                    if (sess.Character.Family != null)
                                                    {
                                                        sess.Character.GenerateFamilyXp(grp.Raid.FXP);
                                                    }
                                                }

                                                if (grp?.Raid?.Reputation > 0)
                                                {
                                                    sess.Character.SetReputation((short)(grp.Raid.Reputation));
                                                }
                                                if (grp.Characters.ElementAt(0).Character?.Family != null)
                                                {
                                                    if (sess.Character.Family != null)
                                                    {
                                                        if (sess.Character.Family != grp.Characters.ElementAt(0).Character?.Family)
                                                        {
                                                            FamilyRaid = false;

                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    FamilyRaid = false;
                                                }
                                            }
                                            try
                                            {
                                                if (FamilyRaid == true)
                                                {

                                                    int numero = 9000;
                                                    switch (grp.Raid.Id)
                                                    {
                                                        case 5:
                                                            numero = 9007;
                                                            break;
                                                        case 0:
                                                            numero = 9008;
                                                            break;
                                                        case 1:
                                                            numero = 9009;
                                                            break;
                                                        case 2:
                                                            numero = 9010;
                                                            break;
                                                        case 3:
                                                            numero = 9011;
                                                            break;
                                                        case 4:
                                                            numero = 9012;
                                                            break;
                                                        case 9:
                                                            numero = 9013;
                                                            break;
                                                        case 13:
                                                            numero = 9014;
                                                            break;
                                                        case 14:
                                                            numero = 9015;
                                                            break;
                                                        case 15:
                                                            numero = 9016;
                                                            break;
                                                    }
                                                    if (grp.Characters.ElementAt(0).Character.Family?.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == numero.ToString() && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year).Count() == 0)
                                                    {
                                                        if (grp.Characters.ElementAt(0).Character.Family?.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.RaidWon && s.FamilyLogData == grp?.Raid?.Id.ToString()).Count() > 3)
                                                        {
                                                            if (numero != 9000)
                                                            {
                                                                grp.Characters.ElementAt(0).Character.Family?.InsertFamilyLog(FamilyLogType.SfideGiornaliere, raidType: numero);
                                                                foreach (FamilyLogDTO famlogtodelete in DAOFactory.FamilyLogDAO.LoadByFamilyId(grp.Characters.ElementAt(0).Character.Family.FamilyId).Where(s => s.FamilyLogType == FamilyLogType.RaidWon && s.FamilyLogData == grp?.Raid?.Id.ToString()).ToList())
                                                                {
                                                                    DAOFactory.FamilyLogDAO.Delete(famlogtodelete.FamilyLogId);
                                                                }
                                                                FamilyDTO fam = grp.Characters.ElementAt(0).Character.Family;
                                                                int FXP = 0;
                                                                switch (grp.Raid.Id)
                                                                {
                                                                    case 5:
                                                                    case 0:
                                                                        FXP = 1000;
                                                                        break;
                                                                    case 1:
                                                                    case 2:
                                                                        FXP = 1500;
                                                                        break;
                                                                    case 3:
                                                                    case 4:
                                                                    case 9:
                                                                        FXP = 2000;
                                                                        break;
                                                                    case 13:
                                                                    case 14:
                                                                        FXP = 2500;
                                                                        break;
                                                                    case 15:
                                                                        FXP = 3000;
                                                                        break;
                                                                }
                                                                fam.FamilyExperience += FXP;
                                                                DAOFactory.FamilyDAO.InsertOrUpdate(ref fam);
                                                                ServerManager.Instance.FamilyRefresh(grp.Characters.ElementAt(0).Character.Family.FamilyId);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            int LivelloNecessario = 0;
                                                            switch (grp.Raid.Id)
                                                            {
                                                                case 5:
                                                                    LivelloNecessario = 1;
                                                                    break;
                                                                case 0:
                                                                    LivelloNecessario = 1;
                                                                    break;
                                                                case 1:
                                                                    LivelloNecessario = 1;
                                                                    break;
                                                                case 2:
                                                                    LivelloNecessario = 1;
                                                                    break;
                                                                case 3:
                                                                    LivelloNecessario = 1;
                                                                    break;
                                                                case 4:
                                                                    LivelloNecessario = 2;
                                                                    break;
                                                                case 9:
                                                                    LivelloNecessario = 2;
                                                                    break;
                                                                case 13:
                                                                    LivelloNecessario = 3;
                                                                    break;
                                                                case 14:
                                                                    LivelloNecessario = 3;
                                                                    break;
                                                                case 15:
                                                                    LivelloNecessario = 3;
                                                                    break;
                                                            }
                                                            if (grp.Characters.ElementAt(0).Character.Family?.FamilyLevel >= LivelloNecessario && LivelloNecessario > 0)
                                                            {
                                                                grp.Characters.ElementAt(0).Character.Family?.InsertFamilyLog(FamilyLogType.RaidWon, raidType: (int)grp?.Raid?.Id);
                                                                ServerManager.Instance.FamilyRefresh(grp.Characters.ElementAt(0).Character.Family.FamilyId);
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {

                                            }

                                            foreach (MapMonster mon in evt.MapInstance.Monsters)
                                            {
                                                mon.CurrentHp = 0;
                                                evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, mon.MapMonsterId));
                                                evt.MapInstance.RemoveMonster(mon);
                                            }
                                            Logger.LogUserEvent("RAID_SUCCESS", grp.Characters.ElementAt(0).Character.Name, $"RaidId: {grp.GroupId}");

                                            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("RAID_SUCCEED"), grp?.Raid?.Label, grp.Characters.ElementAt(0).Character.Name), 0));
                                        }

                                        Observable.Timer(TimeSpan.FromSeconds(evt.MapInstance.InstanceBag.EndState == 1 ? 30 : 0)).Subscribe(o =>
                                        {
                                            ClientSession[] grpmembers = new ClientSession[40];
                                            grp.Characters.CopyTo(grpmembers);
                                            foreach (ClientSession targetSession in grpmembers)
                                            {
                                                if (targetSession != null)
                                                {
                                                    if (targetSession.Character.Hp <= 0)
                                                    {
                                                        targetSession.Character.Hp = 1;
                                                        targetSession.Character.Mp = 1;
                                                    }
                                                    targetSession.SendPacket(Character.GenerateRaidBf(evt.MapInstance.InstanceBag.EndState));
                                                    targetSession.SendPacket(targetSession.Character.GenerateRaid(1, true));
                                                    targetSession.SendPacket(targetSession.Character.GenerateRaid(2, true));
                                                    grp.LeaveGroup(targetSession);
                                                }
                                            }
                                            ServerManager.Instance.GroupList.RemoveAll(s => s.GroupId == grp.GroupId);
                                            ServerManager.Instance.GroupsThreadSafe.Remove(grp.GroupId);
                                            evt.MapInstance.Dispose();
                                        });
                                    }
                                    break;



                                case MapInstanceType.Act4Morcos:
                                case MapInstanceType.Act4Hatus:
                                case MapInstanceType.Act4Calvina:
                                case MapInstanceType.Act4Berios:
                                    client = evt.MapInstance.Sessions.FirstOrDefault();
                                    if (client != null)
                                    {
                                        Family fam = evt.MapInstance.Sessions.FirstOrDefault(s => s?.Character?.Family != null)?.Character.Family;
                                        if (fam != null)
                                        {
                                            fam.AlreadyWinA4 = true;
                                            short rewardVNum = 882;
                                            switch (evt.MapInstance.MapInstanceType)
                                            {
                                                //Morcos is default
                                                case MapInstanceType.Act4Hatus:
                                                    rewardVNum = 185;
                                                    break;

                                                case MapInstanceType.Act4Calvina:
                                                    rewardVNum = 942;
                                                    break;

                                                case MapInstanceType.Act4Berios:
                                                    rewardVNum = 999;
                                                    break;
                                            }
                                            int count = evt.MapInstance.Sessions.Count(s => s?.Character != null);
                                            foreach (ClientSession sess in evt.MapInstance.Sessions)
                                            {
                                                if (sess?.Character != null)
                                                {
                                                    sess.Character.SetReputation(15000);
                                                    sess.Character.GiftAdd(rewardVNum, 1, forceRandom: true, minRare: 6, design: 255);
                                                    short fxp = 200;
                                                    if (count > 50)
                                                    {
                                                        fxp = (short)(fxp * 1.5);
                                                    }
                                                    else if (count > 39)
                                                    {
                                                        fxp = (short)(fxp * 1.4);
                                                    }
                                                    else if (count > 29)
                                                    {
                                                        fxp = (short)(fxp * 1.3);
                                                    }
                                                    else if (count > 19)
                                                    {
                                                        fxp = (short)(fxp * 1.2);
                                                    }
                                                    else if (count > 9)
                                                    {
                                                        fxp = (short)(fxp * 1.1);
                                                    }
                                                    sess.Character.GenerateFamilyXp(fxp);
                                                }
                                            }
                                            Logger.LogEvent("FAMILYRAID_SUCCESS", $"[fam.Name]FamilyRaidId: {evt.MapInstance.MapInstanceType.ToString()}");

                                            if (fam.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9006" && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year).Count() == 0)
                                            {
                                                fam.InsertFamilyLog(FamilyLogType.SfideGiornaliere, raidType: 9006);
                                                fam.FamilyExperience += 5000;
                                                FamilyDTO ff = fam;
                                                DAOFactory.FamilyDAO.InsertOrUpdate(ref ff);
                                                ServerManager.Instance.FamilyRefresh(ff.FamilyId);
                                            }


                                            //client.Character.Family?.InsertFamilyLog(FamilyLogType.RaidWon, raidType: 3);

                                            CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                                            {
                                                DestinationCharacterId = fam.FamilyId,
                                                SourceCharacterId = client.Character.CharacterId,
                                                SourceWorldId = ServerManager.Instance.WorldId,
                                                Message = UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("FAMILYRAID_SUCCESS"), 0),
                                                Type = MessageType.Family
                                            });
                                            //ServerManager.Instance.Broadcast(UserInterfaceHelper.Instance.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("FAMILYRAID_SUCCESS"), grp?.Raid?.Label, grp.Characters.ElementAt(0).Character.Name), 0));

                                            Observable.Timer(TimeSpan.FromSeconds(30)).Subscribe(o =>
                                            {
                                                foreach (ClientSession targetSession in evt.MapInstance.Sessions.ToArray())
                                                {
                                                    if (targetSession != null)
                                                    {
                                                        if (targetSession.Character.Hp <= 0)
                                                        {
                                                            targetSession.Character.Hp = 1;
                                                            targetSession.Character.Mp = 1;
                                                        }

                                                        ServerManager.Instance.ChangeMap(targetSession.Character.CharacterId, 134, 140, 100);
                                                    }
                                                }
                                                evt.MapInstance.Dispose();
                                            });
                                        }
                                    }
                                    break;
                                case MapInstanceType.CaligorInstance:

                                    FactionType winningFaction = CaligorRaid.AngelDamage > CaligorRaid.DemonDamage ? FactionType.Angel : FactionType.Demon;

                                    foreach (ClientSession sess in evt.MapInstance.Sessions)
                                    {
                                        if (sess?.Character != null)
                                        {
                                            /*
                                            if (CaligorRaid.RemainingTime > 2400)
                                            {
                                                if (sess.Character.Faction == winningFaction)
                                                {
                                                    sess.Character.SetReputation(10000);
                                                    sess.Character.GiftAdd(5960, 1);
                                                }
                                                else
                                                {
                                                    sess.Character.GiftAdd(5961, 1);
                                                }
                                            }
                                            else
                                            {
                                                if (sess.Character.Faction == winningFaction)
                                                {
                                                    sess.Character.SetReputation(10000);
                                                    sess.Character.GiftAdd(5961, 1);
                                                }
                                                else
                                                {
                                                    sess.Character.GiftAdd(5958, 1);
                                                }
                                            }*/
                                            sess.Character.SetReputation(4000);
                                            sess.Character.GiftAdd(5960, 1);
                                            sess.Character.GiftAdd(5959, 1);
                                            sess.Character.GenerateFamilyXp(10000);
                                        }
                                    }
                                    evt.MapInstance.IsPVP = true;
                                    evt.MapInstance.CreatePortal(new Portal
                                    {
                                        SourceMapInstanceId = evt.MapInstance.MapInstanceId,
                                        SourceX = 110,
                                        SourceY = 159,
                                        DestinationMapId = 153,
                                        DestinationX = 110,
                                        DestinationY = 159,
                                        Type = -1
                                    });

                                    evt.MapInstance.CreatePortal(new Portal
                                    {
                                        SourceMapInstanceId = evt.MapInstance.MapInstanceId,
                                        SourceX = 70,
                                        SourceY = 159,
                                        DestinationMapId = 153,
                                        DestinationX = 70,
                                        DestinationY = 159,
                                        Type = -1
                                    });
                                    evt.MapInstance.Broadcast(UserInterfaceHelper.GenerateCHDM(ServerManager.GetNpc(2305).MaxHP, CaligorRaid.AngelDamage, CaligorRaid.DemonDamage, CaligorRaid.RemainingTime));
                                    break;
                            }
                            break;

                        case EventActionType.MAPCLOCK:
                            evt.MapInstance.Clock.TotalSecondsAmount = Convert.ToInt32(evt.Parameter);
                            evt.MapInstance.Clock.SecondsRemaining = Convert.ToInt32(evt.Parameter);
                            break;

                        case EventActionType.STARTCLOCK:
                            Tuple<List<EventContainer>, List<EventContainer>> eve = (Tuple<List<EventContainer>, List<EventContainer>>)evt.Parameter;
                            evt.MapInstance.InstanceBag.Clock.StopEvents = eve.Item1;
                            evt.MapInstance.InstanceBag.Clock.TimeoutEvents = eve.Item2;
                            evt.MapInstance.InstanceBag.Clock.StartClock();
                            evt.MapInstance.Broadcast(evt.MapInstance.InstanceBag.Clock.GetClock());
                            break;

                        case EventActionType.ENDLAURENA:
                            {
                                ClientSession clientlaurena = evt.MapInstance.Sessions.FirstOrDefault();
                                if (clientlaurena != null)
                                {
                                    Group grp = clientlaurena?.Character?.Group;
                                    if (grp == null)
                                    {
                                        return;
                                    }
                                    short vnum = 5989;

                                    if (evt.MapInstance.Monsters.Any(s => s.IsBoss))
                                    {
                                        foreach (ClientSession sess in grp.Characters.Where(s => s.CurrentMapInstance.Monsters.Any(e => e.IsBoss)))
                                        {
                                            const byte rare = 0;

                                            // TODO: add random rarity for some object
                                            sess.Character.GiftAdd(vnum, 1, rare, 0, 0, false, raid: grp?.Raid.Label);
                                        }
                                        foreach (MapMonster mon in evt.MapInstance.Monsters)
                                        {
                                            mon.CurrentHp = 0;
                                            evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, mon.MapMonsterId));
                                            evt.MapInstance.RemoveMonster(mon);
                                        }
                                        Logger.LogUserEvent("RAID_SUCCESS", grp.Characters.ElementAt(0).Character.Name, $"RaidId: {grp.GroupId}");

                                        ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("RAID_SUCCEED"), grp?.Raid?.Label, grp.Characters.ElementAt(0).Character.Name), 0));
                                    }

                                    Observable.Timer(TimeSpan.FromSeconds(evt.MapInstance.InstanceBag.EndState == 1 ? 30 : 0)).Subscribe(o =>
                                    {
                                        ClientSession[] grpmembers = new ClientSession[40];
                                        grp.Characters.CopyTo(grpmembers);
                                        foreach (ClientSession targetSession in grpmembers)
                                        {
                                            if (targetSession != null)
                                            {
                                                if (targetSession.Character.Hp <= 0)
                                                {
                                                    targetSession.Character.Hp = 1;
                                                    targetSession.Character.Mp = 1;
                                                }
                                                targetSession.SendPacket(Character.GenerateRaidBf(evt.MapInstance.InstanceBag.EndState));
                                                targetSession.SendPacket(targetSession.Character.GenerateRaid(1, true));
                                                targetSession.SendPacket(targetSession.Character.GenerateRaid(2, true));
                                                grp.LeaveGroup(targetSession);
                                            }
                                        }
                                        ServerManager.Instance.GroupList.RemoveAll(s => s.GroupId == grp.GroupId);
                                        ServerManager.Instance.GroupsThreadSafe.Remove(grp.GroupId);
                                        evt.MapInstance.Dispose();
                                    });
                                }
                            }
                            break;

                        case EventActionType.ADDPLAYERCARD:
                            foreach (ClientSession targetSession in evt.MapInstance.Sessions)
                            {
                                targetSession.Character.AddBuff(new Buff((short)(evt.Parameter), targetSession.Character.SwitchLevel()));
                            }
                            break;

                        case EventActionType.CONGELATUTTI:
                            Tuple<short> congela = (Tuple<short>)evt.Parameter;
                            Observable.Timer(TimeSpan.FromSeconds(congela.Item1)).Subscribe(observerss =>
                            {
                                if (evt.MapInstance != null)
                                {
                                    ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("GLACE_FROST"), 0), ReceiverType.Group);

                                    MapCell cell1 = evt.MapInstance.Map.GetRandomPosition();
                                    MapCell cell2 = evt.MapInstance.Map.GetRandomPosition();
                                    MapCell cell3 = evt.MapInstance.Map.GetRandomPosition();

                                    int sp1id = evt.MapInstance.GetNextMonsterId();
                                    MapMonster sp1 = new MapMonster { MonsterVNum = 2018, MapX = cell1.X, MapY = cell1.Y, MapMonsterId = sp1id, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                    sp1.Initialize(evt.MapInstance);
                                    sp1.NoAggresiveIcon = true;
                                    evt.MapInstance.AddMonster(sp1);
                                    evt.MapInstance.Broadcast(sp1.GenerateIn());
                                    evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, sp1id, 4280));

                                    Observable.Timer(TimeSpan.FromMilliseconds(100)).Subscribe(observers2 =>
                                    {
                                        int sp2id = evt.MapInstance.GetNextMonsterId();
                                        MapMonster sp2 = new MapMonster { MonsterVNum = 2018, MapX = cell2.X, MapY = cell2.Y, MapMonsterId = sp2id, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                        sp2.Initialize(evt.MapInstance);
                                        sp2.NoAggresiveIcon = true;
                                        evt.MapInstance.AddMonster(sp2);
                                        evt.MapInstance.Broadcast(sp2.GenerateIn());
                                        evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, sp2id, 4281));


                                        Observable.Timer(TimeSpan.FromMilliseconds(100)).Subscribe(observers3 =>
                                        {
                                            int sp3id = evt.MapInstance.GetNextMonsterId();
                                            MapMonster sp3 = new MapMonster { MonsterVNum = 2018, MapX = cell3.X, MapY = cell3.Y, MapMonsterId = sp3id, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                            sp3.Initialize(evt.MapInstance);
                                            sp3.NoAggresiveIcon = true;
                                            evt.MapInstance.AddMonster(sp3);
                                            evt.MapInstance.Broadcast(sp3.GenerateIn());
                                            evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, sp3id, 4282));

                                            Observable.Timer(TimeSpan.FromMilliseconds(100)).Subscribe(observers4 =>
                                            {
                                                Save = new List<Character>();
                                                Observable.Timer(TimeSpan.FromSeconds(6)).Subscribe(observer =>
                                                {
                                                    int circleId = evt.MapInstance.GetNextMonsterId();
                                                    MapMonster circle = new MapMonster { MonsterVNum = 2018, MapX = 31, MapY = 28, MapMonsterId = circleId, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                                    circle.Initialize(evt.MapInstance);
                                                    circle.NoAggresiveIcon = true;
                                                    evt.MapInstance.AddMonster(circle);
                                                    evt.MapInstance.Broadcast(circle.GenerateIn());
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, circleId, 4293));
                                                    foreach (Character character in evt.MapInstance.GetCharactersInRange(cell1.X, cell1.Y, 3))
                                                    {
                                                        Save.Add(character);
                                                    }

                                                    foreach (Character character in evt.MapInstance.GetCharactersInRange(cell2.X, cell2.Y, 3))
                                                    {
                                                        Save.Add(character);
                                                    }

                                                    foreach (Character character in evt.MapInstance.GetCharactersInRange(cell3.X, cell3.Y, 3))
                                                    {
                                                        Save.Add(character);
                                                    }
                                                    Observable.Timer(TimeSpan.FromMilliseconds(100)).Subscribe(observer2 =>
                                                    {
                                                        foreach (ClientSession sessione in evt.MapInstance.Sessions)
                                                        {
                                                            if (!Save.Contains(sessione.Character))
                                                            {
                                                                if (sessione.Character.Hp > 0)
                                                                {
                                                                    sessione.Character.Hp = 1;
                                                                    sessione.Character.Mp = 1;
                                                                    sessione.SendPacket(sessione.Character.GenerateStat());
                                                                }
                                                            }
                                                        }
                                                    });
                                                    evt.MapInstance.RemoveMonster(circle);
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, circle.MapMonsterId));
                                                });
                                            });
                                            evt.MapInstance.RemoveMonster(sp3);
                                            evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, sp3.MapMonsterId));
                                        });
                                        evt.MapInstance.RemoveMonster(sp2);
                                        evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, sp2.MapMonsterId));
                                    });
                                    evt.MapInstance.RemoveMonster(sp1);
                                    evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, sp1.MapMonsterId));
                                    MapMonster Glace = evt.MapInstance.Monsters.Where(s => s.MonsterVNum == 2049).FirstOrDefault();
                                    if (Glace != null)
                                    {
                                        Glace.AddBuff(new Buff(43, Glace.Monster.Level));
                                        Glace.AddBuff(new Buff(570, Glace.Monster.Level));
                                    }


                                }
                            });

                            break;

                        case EventActionType.PIRATAMAP:
                            {
                                if (evt.MapInstance != null)
                                {
                                    for (int mm = 0; mm <= 30; mm++)
                                    {
                                        MapCell cell = evt.MapInstance.Map.GetRandomPosition();

                                        if (cell != null)
                                        {
                                            int circleId = evt.MapInstance.GetNextMonsterId();
                                            MapMonster circle = new MapMonster { MonsterVNum = 2018, MapX = cell.X, MapY = cell.Y, MapMonsterId = circleId, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                            circle.Initialize(evt.MapInstance);
                                            circle.NoAggresiveIcon = true;
                                            evt.MapInstance.AddMonster(circle);
                                            evt.MapInstance.Broadcast(circle.GenerateIn());
                                            evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, circleId, 4430));
                                            Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe(observer =>
                                            {
                                                if (evt.MapInstance != null)
                                                {
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Monster, circleId, 3, circleId, 1220, 220, 0, 4569, cell.X, cell.Y, true, 0, 5000, 0, 0));
                                                    foreach (Character character in evt.MapInstance.GetCharactersInRange(cell.X, cell.Y, 2))
                                                    {
                                                        if (character.Hp > 5000)
                                                        {
                                                            character.GetDamage(5000);
                                                            character.AddBuff(new Buff(7, character.SwitchLevel()));
                                                            character.Session.SendPacket(character.GenerateStat());
                                                        }
                                                        else
                                                        {
                                                            character.Hp = 1;
                                                            character.AddBuff(new Buff(7, character.SwitchLevel()));
                                                            character.Session.SendPacket(character.GenerateStat());
                                                        }
                                                    }
                                                    evt.MapInstance.RemoveMonster(circle);
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, circle.MapMonsterId));
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                            break;

                        case EventActionType.SAETTAMAP:
                            {
                                if (evt.MapInstance != null)
                                {
                                    for (int mm = 0; mm <= 40; mm++)
                                    {
                                        MapCell cell = evt.MapInstance.Map.GetRandomPosition();

                                        if (cell != null)
                                        {
                                            int circleId = evt.MapInstance.GetNextMonsterId();
                                            MapMonster circle = new MapMonster { MonsterVNum = 2018, MapX = cell.X, MapY = cell.Y, MapMonsterId = circleId, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                            circle.Initialize(evt.MapInstance);
                                            circle.NoAggresiveIcon = true;
                                            evt.MapInstance.AddMonster(circle);
                                            evt.MapInstance.Broadcast(circle.GenerateIn());
                                            evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, circleId, 4495));
                                            Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe(observer =>
                                            {
                                                if (evt.MapInstance != null)
                                                {
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Monster, circleId, 3, circleId, 1220, 220, 0, 4496, cell.X, cell.Y, true, 0, 5000, 0, 0));
                                                    foreach (Character character in evt.MapInstance.GetCharactersInRange(cell.X, cell.Y, 2))
                                                    {
                                                        if (character.Hp > 5000)
                                                        {
                                                            character.GetDamage(5000);
                                                            character.AddBuff(new Buff(7, character.SwitchLevel()));
                                                            character.Session.SendPacket(character.GenerateStat());
                                                        }
                                                        else
                                                        {
                                                            character.Hp = 1;
                                                            character.AddBuff(new Buff(7, character.SwitchLevel()));
                                                            character.Session.SendPacket(character.GenerateStat());
                                                        }
                                                    }
                                                    evt.MapInstance.RemoveMonster(circle);
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, circle.MapMonsterId));
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                            break;

                        case EventActionType.MAZZACHIODATAMAP:
                            {
                                if (evt.MapInstance != null)
                                {
                                    for (int mm = 0; mm <= 40; mm++)
                                    {
                                        MapCell cell = evt.MapInstance.Map.GetRandomPosition();

                                        if (cell != null)
                                        {
                                            int circleId = evt.MapInstance.GetNextMonsterId();
                                            MapMonster circle = new MapMonster { MonsterVNum = 2018, MapX = cell.X, MapY = cell.Y, MapMonsterId = circleId, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                            circle.Initialize(evt.MapInstance);
                                            circle.NoAggresiveIcon = true;
                                            evt.MapInstance.AddMonster(circle);
                                            evt.MapInstance.Broadcast(circle.GenerateIn());
                                            evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, circleId, 4430));
                                            Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe(observer =>
                                            {
                                                if (evt.MapInstance != null)
                                                {
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Monster, circleId, 3, circleId, 1220, 220, 0, 4618, cell.X, cell.Y, true, 0, 4000, 0, 0));
                                                    foreach (Character character in evt.MapInstance.GetCharactersInRange(cell.X, cell.Y, 2))
                                                    {
                                                        if (character.Hp > 4000)
                                                        {
                                                            character.GetDamage(4000);
                                                            character.AddBuff(new Buff(193, character.SwitchLevel()));
                                                            character.Session.SendPacket(character.GenerateStat());
                                                        }
                                                        else
                                                        {
                                                            character.Hp = 1;
                                                            character.AddBuff(new Buff(193, character.SwitchLevel()));
                                                            character.Session.SendPacket(character.GenerateStat());
                                                        }
                                                    }
                                                    evt.MapInstance.RemoveMonster(circle);
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, circle.MapMonsterId));
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                            break;

                        case EventActionType.BOMBAIBRA:
                            {
                                if (evt.MapInstance != null)
                                {
                                    for (int mm = 0; mm <= 200; mm++)
                                    {
                                        MapCell cell = evt.MapInstance.Map.GetRandomPosition();

                                        if (cell != null)
                                        {
                                            int circleId = evt.MapInstance.GetNextMonsterId();
                                            MapMonster circle = new MapMonster { MonsterVNum = 2018, MapX = cell.X, MapY = cell.Y, MapMonsterId = circleId, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                            circle.Initialize(evt.MapInstance);
                                            circle.NoAggresiveIcon = true;
                                            evt.MapInstance.AddMonster(circle);
                                            evt.MapInstance.Broadcast(circle.GenerateIn());
                                            evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, circleId, 4430));
                                            Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe(observer =>
                                            {
                                                if (evt.MapInstance != null)
                                                {
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Monster, circleId, 3, circleId, 1220, 220, 0, 4894, cell.X, cell.Y, true, 0, 5000, 0, 0));
                                                    MapMonster mapmonster = evt.MapInstance.Monsters.Where(s => s.MonsterVNum == 1028).FirstOrDefault();

                                                    foreach (Character character in evt.MapInstance.GetCharactersInRange(cell.X, cell.Y, 2))
                                                    {
                                                        if (character.Hp > 10000)
                                                        {
                                                            character.GetDamage(10000);
                                                            if (character.Mp > 4500)
                                                            {
                                                                character.Mp -= 4500;
                                                            }
                                                            else
                                                            {
                                                                character.Mp = 1;
                                                            }
                                                            character.AddBuff(new Buff(232, character.SwitchLevel()));
                                                            character.Session.SendPacket(character.GenerateStat());
                                                        }
                                                        else
                                                        {
                                                            character.Hp = 1;
                                                            if (character.Mp > 4500)
                                                            {
                                                                character.Mp -= 4500;
                                                            }
                                                            else
                                                            {
                                                                character.Mp = 1;
                                                            }
                                                            character.AddBuff(new Buff(232, character.SwitchLevel()));
                                                            character.Session.SendPacket(character.GenerateStat());
                                                        }
                                                    }
                                                    evt.MapInstance.RemoveMonster(circle);
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, circle.MapMonsterId));
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                            break;

                        case EventActionType.DROPMETEORITE:
                            {
                                if (evt.MapInstance != null)
                                {
                                    Tuple<short> meteo = (Tuple<short>)evt.Parameter;
                                    short VNUM = 0;
                                    switch (meteo.Item1)
                                    {
                                        case 55:
                                            VNUM = 1464;
                                            break;
                                        case 80:
                                            VNUM = 1463;
                                            break;
                                        case 99:
                                            VNUM = 1462;
                                            break;
                                    }
                                    foreach (ClientSession c in evt.MapInstance.Sessions)
                                    {
                                        if ((c.Character.SwitchLevel() + 10) >= meteo.Item1)
                                        {
                                            c.Character.GiftAdd(VNUM, 1, 0, 0);
                                        }
                                    }
                                }
                            }
                            break;

                        case EventActionType.BOMBARDAMENTOMETEORITE:
                            {
                                if (evt.MapInstance != null)
                                {
                                    Tuple<short> meteo = (Tuple<short>)evt.Parameter;
                                    short perc = 0;
                                    switch (meteo.Item1)
                                    {
                                        case 55:
                                            perc = 25;
                                            break;
                                        case 80:
                                            perc = 60;
                                            break;
                                        case 99:
                                            perc = 80;
                                            break;
                                    }
                                    foreach (ClientSession c in evt.MapInstance.Sessions)
                                    {
                                        int circleId = evt.MapInstance.GetNextMonsterId();
                                        MapMonster circle = new MapMonster { MonsterVNum = 2018, MapX = c.Character.PositionX, MapY = c.Character.PositionY, MapMonsterId = circleId, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                        circle.Initialize(evt.MapInstance);
                                        circle.NoAggresiveIcon = true;
                                        evt.MapInstance.AddMonster(circle);
                                        evt.MapInstance.Broadcast(circle.GenerateIn());
                                        evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, circleId, 4495));
                                        Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe(observer =>
                                        {
                                            if (evt.MapInstance != null)
                                            {
                                                evt.MapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Monster, circleId, 3, circleId, 1220, 220, 0, 4496, c.Character.PositionX, c.Character.PositionY, true, 0, 5000, 0, 0));
                                                MapMonster mapmonster = evt.MapInstance.Monsters.Where(s => s.MonsterVNum == 1046).FirstOrDefault();

                                                foreach (Character character in evt.MapInstance.GetCharactersInRange(c.Character.PositionX, c.Character.PositionY, 2))
                                                {
                                                    character.GetDamageInPercentage(perc);
                                                    character.GetMPPercentage(perc);
                                                    character.Session.SendPacket(character.GenerateStat());

                                                }
                                                evt.MapInstance.RemoveMonster(circle);
                                                evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, circle.MapMonsterId));
                                            }
                                        });
                                    }
                                }
                            }
                            break;

                        case EventActionType.BOMBAKERTOS:
                            {
                                if (evt.MapInstance != null)
                                {
                                    for (int mm = 0; mm <= 50; mm++)
                                    {
                                        MapCell cell = evt.MapInstance.Map.GetRandomPosition();

                                        if (cell != null)
                                        {
                                            int circleId = evt.MapInstance.GetNextMonsterId();
                                            MapMonster circle = new MapMonster { MonsterVNum = 2018, MapX = cell.X, MapY = cell.Y, MapMonsterId = circleId, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                            circle.Initialize(evt.MapInstance);
                                            circle.NoAggresiveIcon = true;
                                            evt.MapInstance.AddMonster(circle);
                                            evt.MapInstance.Broadcast(circle.GenerateIn());
                                            evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, circleId, 4430));
                                            Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe(observer =>
                                            {
                                                if (evt.MapInstance != null)
                                                {
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Monster, circleId, 3, circleId, 1220, 220, 0, 4920, cell.X, cell.Y, true, 0, 5000, 0, 0));
                                                    MapMonster mapmonster = evt.MapInstance.Monsters.Where(s => s.MonsterVNum == 1046).FirstOrDefault();

                                                    foreach (Character character in evt.MapInstance.GetCharactersInRange(cell.X, cell.Y, 2))
                                                    {
                                                        character.GetDamageInPercentage(25);
                                                        character.GetMPPercentage(25);
                                                        character.AddBuff(new Buff(197, character.SwitchLevel()));
                                                        character.Session.SendPacket(character.GenerateStat());

                                                    }
                                                    evt.MapInstance.RemoveMonster(circle);
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, circle.MapMonsterId));
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                            break;

                        case EventActionType.METEORITESLADE:
                            {
                                if (evt.MapInstance != null)
                                {
                                    for (int mm = 0; mm <= 20; mm++)
                                    {
                                        MapCell cell = evt.MapInstance.Map.GetRandomPosition();

                                        if (cell != null)
                                        {
                                            int circleId = evt.MapInstance.GetNextMonsterId();
                                            MapMonster circle = new MapMonster { MonsterVNum = 2018, MapX = cell.X, MapY = cell.Y, MapMonsterId = circleId, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                            circle.Initialize(evt.MapInstance);
                                            circle.NoAggresiveIcon = true;
                                            evt.MapInstance.AddMonster(circle);
                                            evt.MapInstance.Broadcast(circle.GenerateIn());
                                            evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, circleId, 4430));
                                            Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe(observer =>
                                            {
                                                if (evt.MapInstance != null)
                                                {
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Monster, circleId, 3, circleId, 1220, 220, 0, 4612, cell.X, cell.Y, true, 0, 2000, 0, 0));
                                                    foreach (Character character in evt.MapInstance.GetCharactersInRange(cell.X, cell.Y, 2))
                                                    {
                                                        if (character.Hp > 2000)
                                                        {
                                                            character.GetDamage(2000);
                                                            if (character.Mp > 1500)
                                                            {
                                                                character.Mp -= 1500;
                                                            }
                                                            else
                                                            {
                                                                character.Mp = 1;
                                                            }
                                                            character.Session.SendPacket(character.GenerateStat());
                                                        }
                                                        else
                                                        {
                                                            character.Hp = 1;
                                                            if (character.Mp > 1500)
                                                            {
                                                                character.Mp -= 1500;
                                                            }
                                                            else
                                                            {
                                                                character.Mp = 1;
                                                            }
                                                            character.Session.SendPacket(character.GenerateStat());
                                                        }
                                                    }
                                                    evt.MapInstance.RemoveMonster(circle);
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, circle.MapMonsterId));
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                            break;

                        case EventActionType.METEORITEMAP:
                            {
                                Tuple<short, short> meteo = (Tuple<short, short>)evt.Parameter;
                                if (evt.MapInstance != null)
                                {
                                    for (int mm = 0; mm <= meteo.Item2; mm++)
                                    {
                                        MapCell cell = evt.MapInstance.Map.GetRandomPosition();

                                        if (cell != null)
                                        {
                                            int circleId = evt.MapInstance.GetNextMonsterId();
                                            MapMonster circle = new MapMonster { MonsterVNum = meteo.Item1, MapX = cell.X, MapY = cell.Y, MapMonsterId = circleId, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                            circle.Initialize(evt.MapInstance);
                                            circle.NoAggresiveIcon = true;
                                            evt.MapInstance.AddMonster(circle);
                                            evt.MapInstance.Broadcast(circle.GenerateIn());
                                            evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, circleId, 4430));
                                            Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe(observer =>
                                            {
                                                if (evt.MapInstance != null)
                                                {
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Monster, circleId, 3, circleId, 1220, 220, 0, 4983, cell.X, cell.Y, true, 0, 65535, 0, 0));
                                                    foreach (Character character in evt.MapInstance.GetCharactersInRange(cell.X, cell.Y, 2))
                                                    {
                                                        character.Hp = 1;
                                                        character.Mp = 1;
                                                        character.Session.SendPacket(character.GenerateStat());
                                                    }
                                                    evt.MapInstance.RemoveMonster(circle);
                                                    evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, circle.MapMonsterId));
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                            break;

                        case EventActionType.TELEPORTPLAYERS:
                            Tuple<short, short, short, short, short> tp = (Tuple<short, short, short, short, short>)evt.Parameter;
                            int i = 0;

                            foreach (ClientSession targetSession in evt.MapInstance.Sessions)
                            {
                                i++;
                                if (i <= tp.Item1)
                                {
                                    targetSession.Character.PositionX = tp.Item4;
                                    targetSession.Character.PositionY = tp.Item5;
                                    evt.MapInstance?.Broadcast(targetSession.Character.Session, targetSession.Character.GenerateTp(), ReceiverType.Group);
                                }
                            }
                            break;

                        case EventActionType.ADDBUFF:
                            {
                                Tuple<short, short> mom = (Tuple<short, short>)evt.Parameter;
                                if (evt.MapInstance != null)
                                {
                                    MapMonster mapmonster = evt.MapInstance.Monsters.Where(s => s.MonsterVNum == mom.Item2).FirstOrDefault();
                                    if (mapmonster != null)
                                    {
                                        mapmonster.AddBuff(new Buff(mom.Item1, mapmonster.Monster.Level));
                                    }
                                }
                            }
                            break;


                        case EventActionType.REMOVEBUFF:
                            {
                                Tuple<short, short> mom = (Tuple<short, short>)evt.Parameter;
                                if (evt.MapInstance != null)
                                {
                                    MapMonster mapmonster = evt.MapInstance.Monsters.Where(s => s.MonsterVNum == mom.Item2).FirstOrDefault();
                                    if (mapmonster != null)
                                    {
                                        mapmonster.RemoveBuff(mom.Item1);
                                    }
                                }
                            }
                            break;

                        case EventActionType.MONSTERCENTER:
                            {
                                Tuple<short> mom = (Tuple<short>)evt.Parameter;
                                if (evt.MapInstance != null)
                                {
                                    MapMonster mapmonster = evt.MapInstance.Monsters.Where(s => s.MonsterVNum == mom.Item1).FirstOrDefault();
                                    if (mapmonster != null)
                                    {
                                        if (mapmonster.Buff != null)
                                        {
                                            mapmonster.Buff.ClearAll();
                                        }
                                        evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, mapmonster.MapMonsterId));
                                        mapmonster.MapX = 53;
                                        mapmonster.MapY = 58;
                                        evt.MapInstance.Broadcast(mapmonster.GenerateIn());
                                        mapmonster.AddBuff(new Buff(475, mapmonster.Monster.Level));
                                    }
                                }
                            }
                            break;

                        case EventActionType.MONSTEROUT:
                            Tuple<short, short> mo = (Tuple<short, short>)evt.Parameter;
                            if (evt.MapInstance != null)
                            {
                                MapMonster mapmonster = evt.MapInstance.Monsters.Where(s => s.MonsterVNum == mo.Item1).FirstOrDefault();
                                if (mapmonster != null)
                                {
                                    evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, mapmonster.MapMonsterId));
                                    mapmonster.RemoveTarget();
                                    mapmonster.IsHostile = false;
                                    Observable.Timer(TimeSpan.FromSeconds(mo.Item2)).Subscribe(observer =>
                                    {
                                        if (evt.MapInstance != null)
                                        {
                                            MapCell cell = evt.MapInstance.Map.GetRandomPosition();
                                            if (cell != null)
                                            {
                                                int circleId = evt.MapInstance.GetNextMonsterId();
                                                MapMonster circle = new MapMonster { MonsterVNum = 2018, MapX = cell.X, MapY = cell.Y, MapMonsterId = circleId, IsHostile = false, IsMoving = false, ShouldRespawn = false };
                                                circle.Initialize(evt.MapInstance);
                                                circle.NoAggresiveIcon = true;
                                                evt.MapInstance.AddMonster(circle);
                                                evt.MapInstance.Broadcast(circle.GenerateIn());
                                                evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, circleId, 4431));
                                                Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe(observer2 =>
                                                {
                                                    if (evt.MapInstance != null)
                                                    {
                                                        evt.MapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Monster, circleId, 3, circleId, 1220, 220, 0, 4983, cell.X, cell.Y, true, 0, 65535, 0, 0));
                                                        foreach (Character character in evt.MapInstance.GetCharactersInRange(cell.X, cell.Y, 4))
                                                        {
                                                            character.GetDamage(655350);
                                                            Observable.Timer(TimeSpan.FromMilliseconds(1000)).Subscribe(o => ServerManager.Instance.AskRevive(character.CharacterId));
                                                        }
                                                        evt.MapInstance.RemoveMonster(circle);
                                                        evt.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, circle.MapMonsterId));
                                                        mapmonster.MapX = cell.X;
                                                        mapmonster.MapY = cell.Y;
                                                        evt.MapInstance.Broadcast(mapmonster.GenerateIn());
                                                        mapmonster.IsHostile = true;
                                                        evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, mapmonster.MapMonsterId, 4426));
                                                        evt.MapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Monster, mapmonster.MapMonsterId, 4427));
                                                        mapmonster.AddBuff(new Buff(529, mapmonster.Monster.Level));
                                                        mapmonster.AddBuff(new Buff(530, mapmonster.Monster.Level));
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }

                            }
                            break;

                        case EventActionType.TELEPORT:
                            Tuple<short, short, short, short> tps = (Tuple<short, short, short, short>)evt.Parameter;
                            List<Character> characters = evt.MapInstance.GetCharactersInRange(tps.Item1, tps.Item2, 5).ToList();
                            characters.ForEach(s =>
                            {
                                s.PositionX = tps.Item3;
                                s.PositionY = tps.Item4;
                                evt.MapInstance?.Broadcast(s.Session, s.GenerateTp(), ReceiverType.Group);
                            });
                            break;

                        case EventActionType.STOPCLOCK:
                            evt.MapInstance.InstanceBag.Clock.StopClock();
                            evt.MapInstance.Broadcast(evt.MapInstance.InstanceBag.Clock.GetClock());
                            break;

                        case EventActionType.STARTMAPCLOCK:
                            eve = (Tuple<List<EventContainer>, List<EventContainer>>)evt.Parameter;
                            evt.MapInstance.Clock.StopEvents = eve.Item1;
                            evt.MapInstance.Clock.TimeoutEvents = eve.Item2;
                            evt.MapInstance.Clock.StartClock();
                            evt.MapInstance.Broadcast(evt.MapInstance.Clock.GetClock());
                            break;

                        case EventActionType.STOPMAPCLOCK:
                            evt.MapInstance.Clock.StopClock();
                            evt.MapInstance.Broadcast(evt.MapInstance.Clock.GetClock());
                            break;

                        case EventActionType.SPAWNPORTAL:
                            evt.MapInstance.CreatePortal((Portal)evt.Parameter);
                            break;

                        case EventActionType.REFRESHMAPITEMS:
                            evt.MapInstance.MapClear(session);
                            break;

                        case EventActionType.NPCSEFFECTCHANGESTATE:
                            evt.MapInstance.Npcs.ForEach(s => s.EffectActivated = (bool)evt.Parameter);
                            break;

                        case EventActionType.CHANGEPORTALTYPE:
                            try
                            {
                                Tuple<int, PortalType> param = (Tuple<int, PortalType>)evt.Parameter;
                                Portal portal = evt.MapInstance.Portals.Find(s => s.PortalId == param.Item1);
                                if (portal != null)
                                {
                                    portal.Type = (short)param.Item2;
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            break;

                        case EventActionType.CHANGEDROPRATE:
                            evt.MapInstance.DropRate = (int)evt.Parameter;
                            break;

                        case EventActionType.CHANGEXPRATE:
                            evt.MapInstance.XpRate = (int)evt.Parameter;
                            break;

                        case EventActionType.DISPOSEMAP:
                            evt.MapInstance.Dispose();
                            break;

                        case EventActionType.SPAWNBUTTON:
                            evt.MapInstance.SpawnButton((MapButton)evt.Parameter);
                            break;

                        case EventActionType.UNSPAWNMONSTERS:
                            evt.MapInstance.DespawnMonster((int)evt.Parameter);
                            break;

                        case EventActionType.SPAWNMONSTER:
                            evt.MapInstance.SummonMonster((MonsterToSummon)evt.Parameter);
                            break;

                        case EventActionType.SPAWNMONSTERS:
                            evt.MapInstance.SummonMonsters((List<MonsterToSummon>)evt.Parameter);
                            break;

                        case EventActionType.REFRESHRAIDGOAL:
                            ClientSession cl = evt.MapInstance.Sessions.FirstOrDefault();
                            if (cl?.Character != null)
                            {
                                ServerManager.Instance.Broadcast(cl, cl.Character?.Group?.GeneraterRaidmbf(cl), ReceiverType.Group);
                                ServerManager.Instance.Broadcast(cl, UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NEW_MISSION"), 0), ReceiverType.Group);
                            }
                            break;

                        case EventActionType.SPAWNNPC:
                            evt.MapInstance.SummonNpc((NpcToSummon)evt.Parameter);
                            break;

                        case EventActionType.SPAWNNPCS:
                            evt.MapInstance.SummonNpcs((List<NpcToSummon>)evt.Parameter);
                            break;

                        case EventActionType.DROPITEMS:
                            evt.MapInstance.DropItems((List<Tuple<short, int, short, short>>)evt.Parameter);
                            break;

                        case EventActionType.THROWITEMS:
                            Tuple<int, short, byte, int, int> parameters = (Tuple<int, short, byte, int, int>)evt.Parameter;
                            if (monster != null)
                            {
                                parameters = new Tuple<int, short, byte, int, int>(monster.MapMonsterId, parameters.Item2, parameters.Item3, parameters.Item4, parameters.Item5);
                            }
                            evt.MapInstance.ThrowItems(parameters);
                            break;

                        case EventActionType.SPAWNONLASTENTRY:
                            Character lastincharacter = evt.MapInstance.Sessions.OrderByDescending(s => s.RegisterTime).FirstOrDefault()?.Character;
                            List<MonsterToSummon> summonParameters = new List<MonsterToSummon>();
                            MapCell hornSpawn = new MapCell
                            {
                                X = lastincharacter?.PositionX ?? 154,
                                Y = lastincharacter?.PositionY ?? 140
                            };
                            long hornTarget = lastincharacter?.CharacterId ?? -1;
                            summonParameters.Add(new MonsterToSummon(Convert.ToInt16(evt.Parameter), hornSpawn, hornTarget, true));
                            evt.MapInstance.SummonMonsters(summonParameters);
                            break;

                            #endregion
                    }
                }
            }
        }

        public void ScheduleEvent(TimeSpan timeSpan, EventContainer evt) => Observable.Timer(timeSpan).Subscribe(x => RunEvent(evt));

        #endregion
    }
}