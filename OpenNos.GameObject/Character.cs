﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Battle;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Packets.ServerPackets;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;
using OpenNos.PathFinder;
using OpenNos.XMLModel.Models.Quest;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using static OpenNos.Domain.BCardType;
using OpenNos.Core.ConcurrencyExtensions;
using OpenNos.GameObject.Networking;
using OpenNos.Core.Extensions;
using OpenNos.Core;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.CommandPackets;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.GameObject.Packets.CommandPackets;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace OpenNos.GameObject
{
    public class Character : CharacterDTO
    {
        #region Members

        private readonly object _syncObj = new object();

        private bool _isStaticBuffListInitial;

        private Random _random;

        private int _slhpbonus;

        private byte _speed;

        #endregion

        #region Instantiation

        public Character()
        {
            GroupSentRequestCharacterIds = new ThreadSafeGenericList<long>();
            FamilyInviteCharacters = new ThreadSafeGenericList<long>();
            TradeRequests = new ThreadSafeGenericList<long>();
            FriendRequestCharacters = new ThreadSafeGenericList<long>();
            StaticBonusList = new List<StaticBonusDTO>();
            MinilandObjects = new List<MinilandObject>();
            Mates = new List<Mate>();
            LastMonsterAggro = DateTime.Now;
            LastPulse = DateTime.Now;
            MTListTargetQueue = new ConcurrentStack<MTListHitTarget>();
            EquipmentBCards = new ThreadSafeGenericList<BCard>();
            MeditationDictionary = new Dictionary<short, DateTime>();
            Buff = new ThreadSafeSortedList<short, Buff>();
            BuffObservables = new ThreadSafeSortedList<short, IDisposable>();
            CellonOptions = new ThreadSafeGenericList<CellonOptionDTO>();
            PVELockObject = new object();
            ShellEffectArmor = new ConcurrentBag<ShellEffectDTO>();
            ShellEffectMain = new ConcurrentBag<ShellEffectDTO>();
            ShellEffectSecondary = new ConcurrentBag<ShellEffectDTO>();
            Quests = new ConcurrentBag<CharacterQuest>();
        }

        public Character(CharacterDTO input)
        {
            AccountId = input.AccountId;
            Act4Dead = input.Act4Dead;
            Act4Kill = input.Act4Kill;
            Act4Points = input.Act4Points;
            ArenaWinner = input.ArenaWinner;
            EffettoPrestigio = input.EffettoPrestigio;
            Biography = input.Biography;
            BuffBlocked = input.BuffBlocked;
            CharacterId = input.CharacterId;
            Class = input.Class;
            Compliment = input.Compliment;
            Dignity = input.Dignity;
            EmoticonsBlocked = input.EmoticonsBlocked;
            ExchangeBlocked = input.ExchangeBlocked;
            Faction = input.Faction;
            FamilyRequestBlocked = input.FamilyRequestBlocked;
            FriendRequestBlocked = input.FriendRequestBlocked;
            Gender = input.Gender;
            Gold = input.Gold;
            GoldBank = input.GoldBank;
            GroupRequestBlocked = input.GroupRequestBlocked;
            HairColor = input.HairColor;
            HairStyle = input.HairStyle;
            HeroChatBlocked = input.HeroChatBlocked;
            Hp = input.Hp;
            HpBlocked = input.HpBlocked;
            JobLevel = input.JobLevel;
            JobLevelXp = input.JobLevelXp;
            LastFamilyLeave = input.LastFamilyLeave;
            MapId = input.MapId;
            MapX = input.MapX;
            MapY = input.MapY;
            MasterPoints = input.MasterPoints;
            MasterTicket = input.MasterTicket;
            MaxMateCount = input.MaxMateCount;
            MaxPartnerCount = input.MaxPartnerCount;
            MinilandInviteBlocked = input.MinilandInviteBlocked;
            MinilandMessage = input.MinilandMessage;
            MinilandPoint = input.MinilandPoint;
            MinilandState = input.MinilandState;
            MouseAimLock = input.MouseAimLock;
            Mp = input.Mp;
            Name = input.Name;
            QuickGetUp = input.QuickGetUp;
            RagePoint = input.RagePoint;
            Reputation = input.Reputation;
            Slot = input.Slot;
            SpAdditionPoint = input.SpAdditionPoint;
            SpPoint = input.SpPoint;
            State = input.State;
            TalentLose = input.TalentLose;
            TalentSurrender = input.TalentSurrender;
            TalentWin = input.TalentWin;
            WhisperBlocked = input.WhisperBlocked;
            GroupSentRequestCharacterIds = new ThreadSafeGenericList<long>();
            FamilyInviteCharacters = new ThreadSafeGenericList<long>();
            TradeRequests = new ThreadSafeGenericList<long>();
            FriendRequestCharacters = new ThreadSafeGenericList<long>();
            StaticBonusList = new List<StaticBonusDTO>();
            MinilandObjects = new List<MinilandObject>();
            Mates = new List<Mate>();
            LastMonsterAggro = DateTime.Now;
            LastPulse = DateTime.Now;
            MTListTargetQueue = new ConcurrentStack<MTListHitTarget>();
            EquipmentBCards = new ThreadSafeGenericList<BCard>();
            MeditationDictionary = new Dictionary<short, DateTime>();
            Buff = new ThreadSafeSortedList<short, Buff>();
            BuffObservables = new ThreadSafeSortedList<short, IDisposable>();
            CellonOptions = new ThreadSafeGenericList<CellonOptionDTO>();
            PVELockObject = new object();
            ShellEffectArmor = new ConcurrentBag<ShellEffectDTO>();
            ShellEffectMain = new ConcurrentBag<ShellEffectDTO>();
            ShellEffectSecondary = new ConcurrentBag<ShellEffectDTO>();
            Quests = new ConcurrentBag<CharacterQuest>();
            Contributi = input.Contributi;
            IsPetAutoRelive = input.IsPetAutoRelive;
            IsPartnerAutoRelive = input.IsPartnerAutoRelive;
            FirstLogin = input.FirstLogin;
            FirstReputLog = input.FirstReputLog;
            SecondReputLog = input.SecondReputLog;
            ThirdReputLog = input.ThirdReputLog;
            Level = input.Level;
            LevelXp = input.LevelXp;
            HeroLevel = input.HeroLevel;
            HeroXp = input.HeroXp;
            QuestsFinished = input.QuestsFinished;
            Daily = input.Daily;
            Prestigio = input.Prestigio;
            Ach1 = input.Ach1;
            Ach2 = input.Ach2;
            Ach3 = input.Ach3;
            V1 = input.V1;
            V2 = input.V2;
        }

        #endregion

        #region Properties

        public AuthorityType Authority { get; set; }

        public Node[][] BrushFireJagged { get; set; }

        public ThreadSafeSortedList<short, Buff> Buff { get; internal set; }

        public DateTime LastQuest { get; set; }

        public bool RangedDisable { get; set; }

        public bool OnlyNormalAttacks { get; set; }

        public DateTime LastAncelloan { get; set; }

        public DateTime LastAction { get; set; }

        public DateTime LastActionWear { get; set; }

        public ThreadSafeSortedList<short, IDisposable> BuffObservables { get; internal set; }

        public bool CanFight => !IsSitting && ExchangeInfo == null;

        public ThreadSafeGenericList<CellonOptionDTO> CellonOptions { get; set; }

        public List<CharacterRelationDTO> CharacterRelations
        {
            get
            {
                lock (ServerManager.Instance.CharacterRelations)
                {
                    return ServerManager.Instance.CharacterRelations == null ? new List<CharacterRelationDTO>() : ServerManager.Instance.CharacterRelations.Where(s => s.CharacterId == CharacterId || s.RelatedCharacterId == CharacterId).ToList();
                }
            }
        }

        public short CurrentMinigame { get; set; }

        public int DarkResistance { get; set; }

        public int Defence { get; set; }

        public int DefenceRate { get; set; }

        public byte Direction { get; set; }

        public int DistanceCritical { get; set; }

        public int DistanceCriticalRate { get; set; }

        public int DistanceDefence { get; set; }

        public int DistanceDefenceRate { get; set; }

        public int DistanceRate { get; set; }

        public byte Element { get; set; }

        public int ElementRate { get; set; }

        public int ElementRateSP { get; private set; }

        public int Ricarica { get; set; }

        public ThreadSafeGenericList<BCard> EquipmentBCards { get; set; }

        public ExchangeInfo ExchangeInfo { get; set; }

        public Family Family { get; set; }

        public FamilyCharacterDTO FamilyCharacter => Family?.FamilyCharacters.Find(s => s.CharacterId == CharacterId);

        public ThreadSafeGenericList<long> FamilyInviteCharacters { get; set; }

        public int FireResistance { get; set; }

        public int FoodAmount { get; set; }

        public int FoodHp { get; set; }

        public int FoodMp { get; set; }

        public ThreadSafeGenericList<long> FriendRequestCharacters { get; set; }

        public ThreadSafeGenericList<GeneralLogDTO> GeneralLogs { get; set; }

        public bool GmPvtBlock { get; set; }

        public Group Group { get; set; }

        public ThreadSafeGenericList<long> GroupSentRequestCharacterIds { get; set; }

        public bool HasGodMode { get; set; }

        public bool HasShopOpened { get; set; }

        public int HitCritical { get; set; }

        public int HitCriticalRate { get; set; }

        public int HitRate { get; set; }

        public int HPMax { get; set; }

        public bool InExchangeOrTrade => ExchangeInfo != null || Speed == 0;

        public Inventory Inventory { get; set; }

        public bool Invisible { get; set; }

        public bool InvisibleGm { get; set; }

        public bool IsChangingMapInstance { get; set; }

        public bool IsCustomSpeed { get; set; }

        public bool IsDancing { get; set; }

        /// <summary>
        /// Defines if the Character Is currently sending or getting items thru exchange.
        /// </summary>
        public bool IsExchanging { get; set; }

        public bool IsShopping { get; set; }

        public bool IsSitting { get; set; }

        public bool IsUsingBlessingMate => _isStaticBuffListInitial ? Buff.ContainsKey(122) : DAOFactory.StaticBuffDAO.LoadByCharacterId(CharacterId).Any(s => s.CardId.Equals(122));

        public bool IsUsingFairyBooster => _isStaticBuffListInitial ? Buff.ContainsKey(131) : DAOFactory.StaticBuffDAO.LoadByCharacterId(CharacterId).Any(s => s.CardId.Equals(131));

        public bool IsVehicled { get; set; }

        public bool IsWaitingForEvent { get; set; }

        public bool IsFumetto { get; set; }

        public DateTime FirstFumetto { get; set; }

        public string Fumetto { get; set; }

        public DateTime LastDefence { get; set; }

        public DateTime LastEffect { get; set; }

        public DateTime LastVehicleEffect { get; set; }

        public DateTime LastHealth { get; set; }

        public DateTime LastSanguinamento { get; set; }

        public short LastItemVNum { get; set; }

        public DateTime LastMapObject { get; set; }

        public DateTime LastFumetto { get; set; }

        public DateTime LastMonsterAggro { get; set; }

        public DateTime LastMove { get; set; }

        public int LastNpcMonsterId { get; set; }

        public int LastNRunId { get; set; }

        public DateTime LastPermBuffRefresh { get; set; }

        public double LastPortal { get; set; }

        public DateTime LastPotion { get; set; }

        public DateTime LastPulse { get; set; }

        public DateTime LastPVPRevive { get; set; }

        public DateTime LastQuestSummon { get; set; }

        public DateTime LastPst { get; set; }

        public DateTime LastSkillComboUse { get; set; }

        public DateTime LastSkillUse { get; set; }

        public DateTime LastSpeakerUse { get; set; }

        public DateTime LastTs { get; set; }

        public double LastSp { get; set; }

        public DateTime LastSpeedChange { get; set; }

        public DateTime LastSpGaugeRemove { get; set; }

        public DateTime LastTransform { get; set; }

        public DateTime LastVessel { get; set; }

        public int LightResistance { get; set; }

        public int MagicalDefence { get; set; }

        public IDictionary<int, MailDTO> MailList { get; set; }

        public MapInstance MapInstance => ServerManager.GetMapInstance(MapInstanceId);

        public Guid MapInstanceId { get; set; }

        public List<Mate> Mates { get; set; }

        public int MaxDistance { get; set; }

        public int BuffRandomTime { get; set; }

        public int MaxFood { get; set; }

        public int MaxHit { get; set; }

        public int MaxSnack { get; set; }

        public Dictionary<short, DateTime> MeditationDictionary { get; set; }

        public int MessageCounter { get; set; }

        public int MinDistance { get; set; }

        public int MinHit { get; set; }

        public MinigameLogDTO MinigameLog { get; set; }

        public MapInstance Miniland { get; private set; }

        public MapInstance Expmap1 { get; private set; }


        public List<MinilandObject> MinilandObjects { get; set; }

        public int Morph { get; set; }

        public int MorphUpgrade { get; set; }

        public int MorphUpgrade2 { get; set; }

        public int MPMax { get; set; }

        public ConcurrentStack<MTListHitTarget> MTListTargetQueue { get; set; }

        public bool NoAttack { get; set; }

        public bool NoMove { get; set; }

        public short PositionX { get; set; }

        public short PositionY { get; set; }

        public object PVELockObject { get; set; }

        public List<QuicklistEntryDTO> QuicklistEntries { get; private set; }

        public ConcurrentBag<CharacterQuest> Quests { get; set; }

        public RespawnMapTypeDTO Respawn
        {
            get
            {
                RespawnMapTypeDTO respawn = new RespawnMapTypeDTO
                {
                    DefaultX = 79,
                    DefaultY = 116,
                    DefaultMapId = 1,
                    RespawnMapTypeId = -1
                };

                if (Session.HasCurrentMapInstance && Session.CurrentMapInstance.Map.MapTypes.Count > 0)
                {
                    long? respawnmaptype = Session.CurrentMapInstance.Map.MapTypes[0].RespawnMapTypeId;
                    if (respawnmaptype != null)
                    {
                        RespawnDTO resp = Respawns.Find(s => s.RespawnMapTypeId == respawnmaptype);
                        if (resp == null)
                        {
                            RespawnMapTypeDTO defaultresp = Session.CurrentMapInstance.Map.DefaultRespawn;
                            if (defaultresp != null)
                            {
                                respawn.DefaultX = defaultresp.DefaultX;
                                respawn.DefaultY = defaultresp.DefaultY;
                                respawn.DefaultMapId = defaultresp.DefaultMapId;
                                respawn.RespawnMapTypeId = (long)respawnmaptype;
                            }
                        }
                        else
                        {
                            respawn.DefaultX = resp.X;
                            respawn.DefaultY = resp.Y;
                            respawn.DefaultMapId = resp.MapId;
                            respawn.RespawnMapTypeId = (long)respawnmaptype;
                        }
                    }
                }
                return respawn;
            }
        }

        public List<RespawnDTO> Respawns { get; set; }

        public RespawnMapTypeDTO Return
        {
            get
            {
                RespawnMapTypeDTO respawn = new RespawnMapTypeDTO();
                if (Session.HasCurrentMapInstance && Session.CurrentMapInstance.Map.MapTypes.Count > 0)
                {
                    long? respawnmaptype = Session.CurrentMapInstance.Map.MapTypes[0].ReturnMapTypeId;
                    if (respawnmaptype != null)
                    {
                        RespawnDTO resp = Respawns.Find(s => s.RespawnMapTypeId == respawnmaptype);
                        if (resp == null)
                        {
                            RespawnMapTypeDTO defaultresp = Session.CurrentMapInstance.Map.DefaultReturn;
                            if (defaultresp != null)
                            {
                                respawn.DefaultX = defaultresp.DefaultX;
                                respawn.DefaultY = defaultresp.DefaultY;
                                respawn.DefaultMapId = defaultresp.DefaultMapId;
                                respawn.RespawnMapTypeId = (long)respawnmaptype;
                            }
                        }
                        else
                        {
                            respawn.DefaultX = resp.X;
                            respawn.DefaultY = resp.Y;
                            respawn.DefaultMapId = resp.MapId;
                            respawn.RespawnMapTypeId = (long)respawnmaptype;
                        }
                    }
                }
                return respawn;
            }
        }

        public short SaveX { get; set; }

        public short SaveY { get; set; }

        public int ScPage { get; set; }

        public ClientSession Session { get; private set; }

        public ConcurrentBag<ShellEffectDTO> ShellEffectArmor { get; set; }

        public ConcurrentBag<ShellEffectDTO> ShellEffectMain { get; set; }

        public ConcurrentBag<ShellEffectDTO> ShellEffectSecondary { get; set; }

        public int Size { get; set; } = 10;

        public byte SkillComboCount { get; set; }

        public ThreadSafeSortedList<int, CharacterSkill> Skills { get; private set; }

        public ThreadSafeSortedList<int, CharacterSkill> SkillsSp { get; set; }

        public int SnackAmount { get; set; }

        public int SnackHp { get; set; }

        public int SnackMp { get; set; }

        public int SpCooldown { get; set; }

        public byte Speed
        {
            get
            {
                if (_speed > 59)
                {
                    return 59;
                }
                return _speed;
            }

            set
            {
                LastSpeedChange = DateTime.Now;
                _speed = value > 59 ? (byte)59 : value;
            }
        }

        public List<StaticBonusDTO> StaticBonusList { get; set; }

        public ScriptedInstance Timespace { get; set; }

        public int TimesUsed { get; set; }

        public ThreadSafeGenericList<long> TradeRequests { get; set; }

        public bool Undercover { get; set; }

        public bool UseSp { get; set; }

        public byte VehicleSpeed { private get; set; }

        public int WareHouseSize { get; set; }

        public int WaterResistance { get; set; }

        public MapInstance MassTeleportMap { get; set; }

        public short MassTeleportMapX { get; set; }

        public short MassTeleportMapY { get; set; }

        #endregion

        #region Methods


        public string GeneratePerfume(short itemvnum, byte Type)
        {
            return $"r_info {itemvnum} {Type}";
        }

        public string GeneratePetskill(int VNum = -1)
        {
            return $"petski {VNum}";
        }

        public string GenerateGBex()
        {
            return $"gbex 0 {CharacterId} 6 {ServerManager.Instance.Configuration.Tax}";
        }

        public string GenerateGB(int type)
        {
            return $"gb {type} {GoldBank} {Gold} 6 {ServerManager.Instance.Configuration.Tax}";
        }

        public string GenerateSMemo(int type, string msg)
        {
            return $"s_memo {type} {msg}";
        }


        public string GenerateTaFc(byte type)
        {
            return $"ta_fc {type} {CharacterId}";
        }

        public string GenerateTaP(byte tatype, bool showOponent)
        {
            List<ArenaTeamMember> arenateam = ServerManager.Instance.ArenaTeams.FirstOrDefault(s => s != null && s.Any(o => o != null && o.Session == Session))?.OrderBy(s => s.ArenaTeamType).ToList();
            ArenaTeamType type = ArenaTeamType.ERENIA;
            string groups = string.Empty;
            if (arenateam == null)
            {
                return
                    $"ta_p {tatype} {(byte)type} {5} {5} {groups.TrimEnd(' ')}";
            }
            type = arenateam.FirstOrDefault(s => s.Session == Session)?.ArenaTeamType ?? ArenaTeamType.ERENIA;

            for (byte i = 0; i < 6; i++)
            {
                ArenaTeamMember arenamembers = arenateam.FirstOrDefault(s => (i < 3 ? s.ArenaTeamType == type : s.ArenaTeamType != type) && s.Order == (i % 3));
                if (arenamembers != null && (i <= 2 || showOponent))
                {
                    groups +=
                        $"{(arenamembers.Dead ? 0 : 1)}.{arenamembers.Session.Character.CharacterId}.{(byte)arenamembers.Session.Character.Class}.{(byte)arenamembers.Session.Character.Gender}.{(byte)arenamembers.Session.Character.Morph} ";
                }
                else
                {
                    groups += $"-1.-1.-1.-1.-1 ";
                }
            }
            return
                $"ta_p {tatype} {(byte)type} {5 - arenateam.Where(s => s.ArenaTeamType == type).Sum(s => s.SummonCount)} {5 - arenateam.Where(s => s.ArenaTeamType != type).Sum(s => s.SummonCount)} {groups.TrimEnd(' ')}";
        }

        public string GenerateTaPs()
        {
            List<ArenaTeamMember> arenateam = ServerManager.Instance.ArenaTeams.FirstOrDefault(s => s != null && s.Any(o => o?.Session == Session))?.OrderBy(s => s.ArenaTeamType).ToList();
            string groups = string.Empty;
            if (arenateam == null)
            {
                return $"ta_ps {groups.TrimEnd(' ')}";
            }
            ArenaTeamType type = arenateam.FirstOrDefault(s => s.Session == Session)?.ArenaTeamType ?? ArenaTeamType.ERENIA;

            for (byte i = 0; i < 6; i++)
            {
                ArenaTeamMember arenamembers = arenateam.FirstOrDefault(s => (i < 3 ? s.ArenaTeamType == type : s.ArenaTeamType != type) && s.Order == (i % 3));
                if (arenamembers != null)
                {
                    groups +=
                        $"{arenamembers.Session.Character.CharacterId}.{(int)(arenamembers.Session.Character.Hp / arenamembers.Session.Character.HPLoad() * 100)}.{(int)(arenamembers.Session.Character.Mp / arenamembers.Session.Character.MPLoad() * 100)}.0 ";
                }
                else
                {
                    groups += $"-1.-1.-1.-1.-1 ";
                }
            }
            return $"ta_ps {groups.TrimEnd(' ')}";
        }

        new public byte SwitchLevel()
        {
            return Level;
        }



        public byte Livello(short classe)
        {
            return Level;
        }

        public void AddLevelXp(long xp)
        {
            double multiplier = 1;
            multiplier += (Session.Character.ShellEffectMain.FirstOrDefault(s => s.Effect == (byte)ShellWeaponEffectType.GainMoreXP)?.Value ?? 0) / 100D;
            StaticBonusDTO expduble = Session.Character.StaticBonusList.Find(s =>
                s.StaticBonusType == StaticBonusType.DoubleExp);
            if (expduble != null)
            {
                multiplier *= 2;
            }
            xp = (long)(xp * multiplier);
            LevelXp += xp;
        }

        public void SetLevelXp(long xp)
        {
            LevelXp = xp;
        }

        public void RemoveLevelXp(long xp)
        {
            LevelXp -= xp;
        }

        public void AddLevel(byte n)
        {
            Level += n;
        }

        public void SetLevel(byte n)
        {
            Level = n;
        }

        public long SwitchLevelXp()
        {
            return LevelXp;
        }

        public long SwitchLevelHero()
        {
            return HeroLevel;
        }

        public void AddHeroLevelXp(long xp)
        {
            HeroXp += xp;
        }

        public void SetHeroLevelXp(long xp)
        {
            HeroXp = xp;
        }

        public void RemoveHeroLevelXp(long xp)
        {
            HeroXp -= xp;
        }

        public void AddHeroLevel(byte n)
        {
            HeroLevel += n;
        }

        public void SetHeroLevel(byte n)
        {
            HeroLevel = n;
        }

        public long SwitchHeroLevelXp()
        {
            return HeroXp;
            return HeroXp;
        }

        public void InsertReputationLog(long rep)
        {
            string value = string.Empty;
            value = Name + " | " + rep;
            ReputationLogDTO log = new ReputationLogDTO
            {
                CharacterId = CharacterId,
                ReputationData = value,
                Timestamp = DateTime.Now
            };
            DAOFactory.ReputationLogDAO.InsertOrUpdate(ref log);
        }

        public void InsertCommandLog(string commandlog, string vnum)
        {
            string value = string.Empty;
            value = Name + " | " + commandlog + " " + vnum;
            AdminLogDTO log = new AdminLogDTO
            {
                CharacterId = CharacterId,
                AdminLogData = value,
                Timestamp = DateTime.Now
            };
            DAOFactory.AdminLogDAO.InsertOrUpdate(ref log);
        }

        public void InsertSpLog(long splog)
        {
            string value = string.Empty;
            value = Name + " | " + splog;
            SpLogDTO log = new SpLogDTO
            {
                CharacterId = CharacterId,
                SpData = value,
                Timestamp = DateTime.Now
            };
            DAOFactory.SpLogDAO.InsertOrUpdate(ref log);
        }


        public void InsertLevelLog(int level)
        {
            string value = string.Empty;
            value = Name + " | " + level + " | " + Class;
            LevelLogDTO log = new LevelLogDTO
            {
                CharacterId = CharacterId,
                LevelLogData = value,
                Timestamp = DateTime.Now
            };
            DAOFactory.LevelLogDAo.InsertOrUpdate(ref log);
        }

        public void InsertRaidLog(string name, int rarity)
        {
            string value = string.Empty;
            value = Name + " | " + name + " | r" + rarity;
            RaidLogDTO log = new RaidLogDTO
            {
                CharacterId = CharacterId,
                RaidLogData = value,
                Timestamp = DateTime.Now
            };
            DAOFactory.RaidLogDAO.InsertOrUpdate(ref log);
        }

        public void InsertSpUpgradeLog(byte upgrade = 0, short itemVNum = 0)
        {
            string value = string.Empty;
            Item itemsp = ServerManager.GetItem(itemVNum);
            value = Name + " | " + itemsp.Name + " | +" + upgrade;
            SpUpgradeLogDTO log = new SpUpgradeLogDTO
            {
                CharacterId = CharacterId,
                SpUpgradeData = value,
                Timestamp = DateTime.Now
            };
            DAOFactory.SpUpgradeDAO.InsertOrUpdate(ref log);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public string GenerateTaM(int type)
        {
            IEnumerable<ArenaTeamMember> tm = ServerManager.Instance.ArenaTeams.FirstOrDefault(s => s.Any(o => o.Session == Session));
            int score1 = 0;
            int score2 = 0;
            if (tm == null)
            {
                return $"ta_m {type} {score1} {score2} {(type == 3 ? MapInstance.InstanceBag.Clock.SecondsRemaining : 0)} 0";
            }
            ArenaTeamMember tmem = tm.FirstOrDefault(s => s.Session == Session);
            IEnumerable<long> ids = tm.Where(s => tmem != null && tmem.ArenaTeamType != s.ArenaTeamType).Select(s => s.Session.Character.CharacterId);
            score1 = MapInstance.InstanceBag.DeadList.Count(s => ids.Contains(s));
            score2 = MapInstance.InstanceBag.DeadList.Count(s => !ids.Contains(s));
            return $"ta_m {type} {score1} {score2} {(type == 3 ? MapInstance.InstanceBag.Clock.SecondsRemaining : 0)} 0";
        }

        public string GenerateTaF(byte victoriousteam)
        {
            IEnumerable<ArenaTeamMember> tm = ServerManager.Instance.ArenaTeams.FirstOrDefault(s => s.Any(o => o.Session == Session));
            int score1 = 0;
            int score2 = 0;
            int life1 = 0;
            int life2 = 0;
            int call1 = 0;
            int call2 = 0;
            ArenaTeamType atype = ArenaTeamType.ERENIA;
            if (tm == null)
            {
                return $"ta_f 0 {victoriousteam} {(byte)atype} {score1} {life1} {call1} {score2} {life2} {call2}";
            }
            ArenaTeamMember tmem = tm.FirstOrDefault(s => s.Session == Session);
            if (tmem == null)
            {
                return $"ta_f 0 {victoriousteam} {(byte)atype} {score1} {life1} {call1} {score2} {life2} {call2}";
            }
            atype = tmem.ArenaTeamType;
            IEnumerable<long> ids = tm.Where(s => tmem.ArenaTeamType == s.ArenaTeamType).Select(s => s.Session.Character.CharacterId);
            IEnumerable<ArenaTeamMember> oposit = tm.Where(s => tmem.ArenaTeamType != s.ArenaTeamType);
            IEnumerable<ArenaTeamMember> own = tm.Where(s => tmem.ArenaTeamType == s.ArenaTeamType);
            score1 = 3 - MapInstance.InstanceBag.DeadList.Count(s => ids.Contains(s));
            score2 = 3 - MapInstance.InstanceBag.DeadList.Count(s => !ids.Contains(s));
            life1 = 3 - own.Count(s => s.Dead);
            life2 = 3 - oposit.Count(s => s.Dead);
            call1 = 5 - own.Sum(s => s.SummonCount);
            call2 = 5 - oposit.Sum(s => s.SummonCount);
            return $"ta_f 0 {victoriousteam} {(byte)atype} {score1} {life1} {call1} {score2} {life2} {call2}";
        }


        public string GenerateBsInfo(byte type, int arenaeventtype, int time, byte titletype)
        {
            return $"bsinfo {type} {arenaeventtype} {time} {titletype}";
        }

        public void AddQuest(long questId, bool isMain = false)
        {
            var characterQuest = new CharacterQuest(questId, CharacterId);
            if (Quests.Any(q => q.QuestId == questId) || characterQuest.Quest == null || (isMain && Quests.Any(q => q.IsMainQuest)) || (Quests.Where(q => q.Quest.QuestType != (byte)QuestType.WinRaid).ToList().Count >= 5 && characterQuest.Quest.QuestType != (byte)QuestType.WinRaid && !isMain))
            {
                return;
            }
            if (characterQuest.Quest.LevelMin > SwitchLevel())
            {
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("TOO_LOW_LVL"), 0));
                return;
            }
            if (ServerManager.Instance.Configuration.MaxLevel == 99 && characterQuest.Quest.LevelMax < SwitchLevel())
            {
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("TOO_HIGH_LVL"), 0));
                return;
            }
            if (!characterQuest.Quest.IsDaily && !characterQuest.IsMainQuest)
            {
                if (DAOFactory.QuestLogDao.LoadByCharacterId(CharacterId).Any(s => s.QuestId == questId))
                {
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("QUEST_ALREADY_DONE"), 0));
                }
            }
            else if (characterQuest.Quest.IsDaily)
            {
                if (DAOFactory.QuestLogDao.LoadByCharacterId(CharacterId).Any(s => s.QuestId == questId && s.LastDaily != null && s.LastDaily.Value.AddHours(24) >= DateTime.Now))
                {
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("QUEST_ALREADY_DONE"), 0));
                    return;
                }
            }
            if (characterQuest.Quest.QuestType == (int)QuestType.TimesSpace && ServerManager.Instance.TimeSpaces.All(t => t.LevelMinimum > 99 ? (t.Label == characterQuest.Quest.NextQuestId.ToString()) : (t.LevelMinimum != (characterQuest.Quest.QuestObjectives.FirstOrDefault()?.Data ?? -1)))
                || characterQuest.Quest.QuestType == (int)QuestType.Product || characterQuest.Quest.QuestType == (int)QuestType.Collect3
                || characterQuest.Quest.QuestType == (int)QuestType.TsPoint || characterQuest.Quest.QuestType == (int)QuestType.NumberOfKill
                || characterQuest.Quest.QuestType == (int)QuestType.TargetReput || characterQuest.Quest.QuestType == (int)QuestType.Needed
                || characterQuest.Quest.QuestType == (int)QuestType.Collect5 || QuestHelper.Instance.SkipQuests.Any(q => q == characterQuest.QuestId))
            {
                AddQuest(characterQuest.Quest.NextQuestId ?? -1, isMain);
                return;
            }
            if (characterQuest.Quest.TargetMap != null)
            {
                Session.SendPacket(characterQuest.Quest.TargetPacket());
            }
            characterQuest.IsMainQuest = isMain;
            Quests.Add(characterQuest);
            Session.SendPacket(GenerateQuestsPacket(questId));
        }

        public void IncrementQuests(QuestType type, int firstData = 0, int secondData = 0, int thirdData = 0)
        {
            foreach (CharacterQuest quest in Quests.Where(q => q?.Quest?.QuestType == (int)type))
            {
                switch ((QuestType)quest.Quest.QuestType)
                {
                    case QuestType.Capture1:
                    case QuestType.Capture2:
                    case QuestType.WinRaid:
                    case QuestType.Collect1:
                    case QuestType.Collect2:
                    case QuestType.Collect3:
                    case QuestType.Collect4:
                    case QuestType.Hunt:
                    case QuestType.TimesSpace:
                    case QuestType.Inspect:
                        quest.Quest.QuestObjectives.Where(o => o.Data == firstData).ToList().ForEach(d => IncrementObjective(quest, d.ObjectiveIndex));
                        break;

                    case QuestType.Product:
                        quest.Quest.QuestObjectives.Where(o => o.Data == firstData).ToList().ForEach(d => IncrementObjective(quest, d.ObjectiveIndex, secondData));
                        break;

                    case QuestType.Dialog1:
                    case QuestType.Dialog2:
                        quest.Quest.QuestObjectives.Where(o => o.Data == firstData).ToList().ForEach(d => IncrementObjective(quest, d.ObjectiveIndex, isOver: true));
                        break;

                    case QuestType.Wear:
                        if (quest.Quest.QuestObjectives.Any(q => q.SpecialData == firstData &&
                        (Session.Character.Inventory.Any(i => i.ItemVNum == q.Data && i.Type == InventoryType.Wear) || (quest.QuestId == 1541 || quest.QuestId == 1546) && Class != ClassType.Adventurer)))
                        {
                            IncrementObjective(quest, isOver: true);
                        }
                        break;

                    case QuestType.Brings:
                    case QuestType.Required:
                        quest.Quest.QuestObjectives.Where(o => o.Data == firstData).ToList().ForEach(d =>
                        {
                            if (Inventory.CountItem(d.SpecialData ?? -1) >= d.Objective)
                            {
                                Inventory.RemoveItemAmount(d.SpecialData ?? -1, d.Objective ?? 1);
                                IncrementObjective(quest, d.ObjectiveIndex, d.Objective ?? 1);
                            }
                        });
                        break;

                    case QuestType.GoTo:
                        if (quest.Quest.TargetMap == firstData && Math.Abs(secondData - quest.Quest.TargetX ?? 0) < 3 && Math.Abs(thirdData - quest.Quest.TargetY ?? 0) < 3)
                        {
                            IncrementObjective(quest, isOver: true);
                        }
                        break;

                    case QuestType.Use:
                        quest.Quest.QuestObjectives.Where(o => o.Data == firstData && Mates.Any(m => m.NpcMonsterVNum == o.SpecialData && m.IsTeamMember)).ToList().ForEach(d => IncrementObjective(quest, d.ObjectiveIndex, d.Objective ?? 1));
                        break;

                    case QuestType.FlowerQuest:
                        if (firstData + 10 < SwitchLevel())
                        {
                            continue;
                        }
                        IncrementObjective(quest, 1);
                        break;

                    case QuestType.TransmitGold:
                        foreach (QuestObjectiveDTO q in quest.Quest.QuestObjectives.Where(o => o.Data == firstData).ToList())
                        {
                            if (Session.Character.Gold >= q.Objective)
                            {
                                Gold -= (long)q.Objective;
                                Session.SendPacket(GenerateGold());
                                quest.Quest.QuestObjectives.Where(o => o.Data == firstData).ToList().ForEach(d => IncrementObjective(quest, d.ObjectiveIndex, isOver: true));
                                return;
                            }
                        }
                        break;


                    //TODO : Later 
                    case QuestType.TsPoint:
                    case QuestType.NumberOfKill:
                    case QuestType.Needed:
                    case QuestType.TargetReput:
                    case QuestType.Collect5:
                        break;
                }
            }
        }

        private void IncrementObjective(CharacterQuest quest, byte index = 0, int amount = 1, bool isOver = false)
        {
            bool isFinish = isOver;
            quest.Incerment(index, amount);
            byte a = 1;
            if (quest.GetObjectives().All(q => quest.GetObjectiveByIndex(a) == null || q >= quest.GetObjectiveByIndex(a++).Objective))
            {
                isFinish = true;
            }

            Session.SendPacket($"qsti {quest.GetInfoPacket(false)}");

            if (!isFinish)
            {
                return;
            }
            LastQuest = DateTime.Now;
            if (quest.QuestId == 3314)
            {
                QuestsFinished = true;
            }
            if (CustomQuestRewards((QuestType)quest.Quest.QuestType))
            {
                RemoveQuest(quest.QuestId);
                return;
            }
            Session.SendPacket(quest.Quest.GetRewardPacket(this));
            RemoveQuest(quest.QuestId);
        }

        public void RemoveQuest(long questId, bool IsGivingUp = false)
        {
            CharacterQuest questToRemove = Quests.FirstOrDefault(q => q.QuestId == questId);
            if (questToRemove == null)
            {
                return;
            }
            if (questToRemove.Quest.TargetMap != null)
            {
                Session.SendPacket(questToRemove.Quest.RemoveTargetPacket());
            }
            Quests.RemoveWhere(s => s.QuestId != questId, out ConcurrentBag<CharacterQuest> tmp);
            Quests = tmp;
            Session.SendPacket(GenerateQuestsPacket());
            if (IsGivingUp)
            {
                return;
            }
            if (questToRemove.Quest.EndDialogId != null)
            {
                Session.SendPacket(GenerateNpcDialog((int)questToRemove.Quest.EndDialogId));
            }
            if (questToRemove.Quest.NextQuestId != null)
            {
                AddQuest((long)questToRemove.Quest.NextQuestId, questToRemove.IsMainQuest);
            }
            LogHelper.Instance.InsertQuestLog(CharacterId, Session.IpAddress, questToRemove.Quest.QuestId, DateTime.Now);
        }

        public void CheckHuntQuest()
        {
            CharacterQuest quest = Quests?.FirstOrDefault(q => q?.Quest?.QuestType == (int)QuestType.Hunt && q.Quest?.TargetMap == MapInstance?.Map?.MapId && Math.Abs(PositionX - q.Quest?.TargetX ?? 0) < 2 && Math.Abs(PositionY - q.Quest?.TargetY ?? 0) < 2);
            if (quest == null)
            {
                return;
            }
            ConcurrentBag<ToSummon> monsters = new ConcurrentBag<ToSummon>();
            for (int a = 0; a < quest.GetObjectiveByIndex(1)?.Objective / 2 + 1; a++)
            {
                monsters.Add(new ToSummon((short)(quest.GetObjectiveByIndex(1)?.Data ?? -1), new MapCell { X = (short)(PositionX + ServerManager.RandomNumber(-2, 3)), Y = (short)(PositionY + ServerManager.RandomNumber(-2, 3)) }, this, true));
            }
            EventHelper.Instance.RunEvent(new EventContainer(MapInstance, EventActionType.SPAWNMONSTERS, monsters.AsEnumerable()));
        }

        public bool CustomQuestRewards(QuestType type)
        {
            switch (type)
            {
                case QuestType.FlowerQuest:
                    if (ServerManager.RandomNumber() < 80)
                    {
                        AddBuff(new Buff(378, SwitchLevel()));
                    }
                    else
                    {
                        AddBuff(new Buff(379, SwitchLevel()));
                    }
                    return true;

                default:
                    return false;
            }
        }

        public string GeneratePdti(int value)
        {
            return $"pdti 0 {value} 1 -1 -1 -1";
        }


        public string GenerateQuestsPacket(long newQuestId = -1)
        {
            short a = 0;
            short b = 6;
            Quests.ToList().ForEach(qst =>
            {
                qst.QuestNumber = qst.IsMainQuest
                    ? (short)5
                    : (!qst.IsMainQuest && !qst.Quest.IsDaily ? b++ : a++);
            });
            return $"qstlist {Quests.Aggregate(string.Empty, (current, quest) => current + $" {quest.GetInfoPacket(quest.QuestId == newQuestId)}")}";
        }

        public void AddBuff(Buff indicator, bool noMessage = false, int damage = 0, int duration = 0)
        {
            if (indicator.Card != null && (!noMessage || !Buff.Any(s => s.Card.CardId == indicator.Card.CardId)))
            {
                Buff.Remove(indicator.Card.CardId);
                Buff[indicator.Card.CardId] = indicator;
                int RemainingTime = indicator.Card.Duration;
                if (duration != 0)
                {
                    RemainingTime = duration;
                }
                indicator.RemainingTime = RemainingTime;
                indicator.Start = DateTime.Now;

                if (indicator.Card.CardId == 2154 || indicator.Card.CardId == 2155 || indicator.Card.CardId == 2156 || indicator.Card.CardId == 2157 || indicator.Card.CardId == 2158 || indicator.Card.CardId == 2159 || indicator.Card.CardId == 2160)
                {
                    indicator.RemainingTime = BuffRandomTime = ServerManager.RandomNumber(50, 200);
                }
                if (indicator.Card.CardId == 559)
                {
                    indicator.RemainingTime = BuffRandomTime = ServerManager.RandomNumber(150, 500);
                }
                if (indicator.Card.CardId == 85)
                {
                    indicator.RemainingTime = BuffRandomTime = ServerManager.RandomNumber(50, 350);
                }
                else if (indicator.Card.CardId == 336)
                {
                    indicator.RemainingTime = BuffRandomTime = ServerManager.RandomNumber(30, 70);
                }

                Session.SendPacket($"bf 1 {CharacterId} {damage}.{indicator.Card.CardId}.{(damage > 0 ? damage : indicator.RemainingTime)} {SwitchLevel()}");
                Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("UNDER_EFFECT"), indicator.Card.Name), 20));

                indicator.Card.BCards.ForEach(c => c.ApplyBCards(this));
                if (BuffObservables.ContainsKey(indicator.Card.CardId))
                {
                    BuffObservables[indicator.Card.CardId]?.Dispose();
                    BuffObservables.Remove(indicator.Card.CardId);
                }
                if (damage == 0)
                {
                    BuffObservables[indicator.Card.CardId] = Observable.Timer(TimeSpan.FromMilliseconds(indicator.RemainingTime * 100)).Subscribe(o =>
                    {
                        /*if (indicator.Card.BCards.Any(s => s.Type == (byte)CardType.Move && !s.SubType.Equals((byte)AdditionalTypes.Move.MovementImpossible / 10)))
                        {
                            LastSpeedChange = DateTime.Now;
                            LoadSpeed();
                            Session.SendPacket(GenerateCond());
                        }*/
                        if(indicator.Card.CardId == 664)
                        {
                            //Todo: remove effect MegaTitan
                        }
                        RemoveBuff(indicator.Card.CardId);
                        /*if (indicator.Card.TimeoutBuff != 0 && ServerManager.RandomNumber() < indicator.Card.TimeoutBuffChance)
                        {
                            AddBuff(new Buff(indicator.Card.TimeoutBuff, SwitchLevel()));
                        }*/
                    });
                }
                /*if (indicator.Card.BCards.Any(s => s.Type == (byte)CardType.Move && !s.SubType.Equals((byte)AdditionalTypes.Move.MovementImpossible / 10)))
                {
                    LastSpeedChange = DateTime.Now;
                    LoadSpeed();
                    Session.SendPacket(GenerateCond());
                }*/
                if (Buff.Where(b => b.Card.EffectId == 10147 || b.Card.EffectId == 10148) == null)
                {
                    LastSpeedChange = DateTime.Now;
                    NoMove = false;
                    LoadSpeed();
                    Session.SendPacket(GenerateCond());
                }
                if (indicator.Card.BCards.Any(s => s.Type == (byte)CardType.SpecialAttack && s.SubType.Equals((byte)AdditionalTypes.SpecialAttack.NoAttack / 10)))
                {
                    NoAttack = true;
                    Session.SendPacket(GenerateCond());
                }
                if (indicator.Card.BCards.Any(s => s.Type == (byte)CardType.Move && s.SubType.Equals((byte)AdditionalTypes.Move.MovementImpossible / 10)))
                {
                    NoMove = true;
                    Session.SendPacket(GenerateCond());
                }
                
            }
        }

        public bool AddPet(Mate mate, bool TeamMember = false, bool FromTs = false)
        {
            try
            {
                if (mate.MateType == MateType.Pet ? MaxMateCount > Mates.Count : MaxPartnerCount > Mates.Count(s => s.MateType == MateType.Partner))
                {
                    if (TeamMember)
                    {
                        mate.IsTeamMember = true;
                    }
                    Mates.Add(mate);
                    if (!FromTs)
                    {
                        MapInstance.Broadcast(mate.GenerateIn());
                    }
                    Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("YOU_GET_PET"), mate.Name), 12));
                    Session.SendPacket(UserInterfaceHelper.GeneratePClear());
                    Session.SendPackets(GenerateScP());
                    Session.SendPackets(GenerateScN());
                    return true;
                }
                else
                {
                    UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("NO_PET_SPACES"));
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public void AddPetWithSkill(Mate mate)
        {
            bool isUsingMate = true;
            if (!Mates.Any(s => s.IsTeamMember && s.MateType == mate.MateType))
            {
                isUsingMate = false;
                mate.IsTeamMember = true;
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(GenerateQuicklist());
            }
            else
            {
                //set position to mate because mate will send to miniland
                mate.MapX = 5;
                mate.MapY = 8;
            }
            Session.SendPacket($"ctl 2 {mate.MateTransportId} 3");
            Mates.Add(mate);
            Session.SendPacket(UserInterfaceHelper.GeneratePClear());
            Session.SendPackets(GenerateScP());
            Session.SendPackets(GenerateScN());
            if (!isUsingMate)
            {
                MapInstance.Broadcast(mate.GenerateIn());
                Session.SendPacket(GeneratePinit());
                Session.SendPacket(UserInterfaceHelper.GeneratePClear());
                Session.SendPackets(GenerateScP());
                Session.SendPackets(GenerateScN());
                Session.SendPackets(GeneratePst());
            }
        }

        public void AddRelation(long characterId, CharacterRelationType Relation)
        {
            CharacterRelationDTO addRelation = new CharacterRelationDTO
            {
                CharacterId = CharacterId,
                RelatedCharacterId = characterId,
                RelationType = Relation
            };

            DAOFactory.CharacterRelationDAO.InsertOrUpdate(ref addRelation);
            ServerManager.Instance.RelationRefresh(addRelation.CharacterRelationId);
            Session.SendPacket(GenerateFinit());
            ClientSession target = ServerManager.Instance.Sessions.FirstOrDefault(s => s.Character?.CharacterId == characterId);
            target?.SendPacket(target?.Character.GenerateFinit());
        }

        public void AddStaticBuff(StaticBuffDTO staticBuff)
        {
            Buff bf = new Buff(staticBuff.CardId, SwitchLevel())
            {
                Start = DateTime.Now,
                StaticBuff = true
            };
            Buff oldbuff = Buff[staticBuff.CardId];
            if (staticBuff.RemainingTime == -1)
            {
                bf.RemainingTime = staticBuff.RemainingTime;
                Buff[staticBuff.CardId] = bf;
            }
            if (staticBuff.RemainingTime == 0)
            {
                bf.RemainingTime = -1;
                Buff[staticBuff.CardId] = bf;
            }
            else if (staticBuff.RemainingTime > 0)
            {
                bf.RemainingTime = staticBuff.RemainingTime;
                Buff[staticBuff.CardId] = bf;
            }
            else if (oldbuff != null)
            {
                Buff.Remove(bf.Card.CardId);
                int time = (int)((oldbuff.Start.AddSeconds(oldbuff.Card.Duration * 6 / 10) - DateTime.Now).TotalSeconds / 10 * 6);
                bf.RemainingTime = (bf.Card.Duration * 6 / 10) + (time > 0 ? time : 0);
                Buff[bf.Card.CardId] = bf;
            }
            else
            {
                bf.RemainingTime = bf.Card.Duration * 6 / 10;
                Buff[bf.Card.CardId] = bf;
            }
            bf.Card.BCards.ForEach(c => c.ApplyBCards(this));
            if (BuffObservables.ContainsKey(bf.Card.CardId))
            {
                BuffObservables[bf.Card.CardId].Dispose();
                BuffObservables.Remove(bf.Card.CardId);
            }
            if (bf.RemainingTime > 0)
            {
                BuffObservables[bf.Card.CardId] = Observable.Timer(TimeSpan.FromSeconds(bf.RemainingTime)).Subscribe(o =>
                {
                    RemoveBuff(bf.Card.CardId);
                    /*if (bf.Card.TimeoutBuff != 0 && ServerManager.RandomNumber() < bf.Card.TimeoutBuffChance)
                    {
                        AddBuff(new Buff(bf.Card.TimeoutBuff, SwitchLevel()));
                    }*/
                });
            }
            if (!_isStaticBuffListInitial)
            {
                _isStaticBuffListInitial = true;
            }

            Session.SendPacket(bf.RemainingTime <= 0 ? $"vb {bf.Card.CardId} 1 -1" : $"vb {bf.Card.CardId} 1 {bf.RemainingTime * 10}");
            Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("UNDER_EFFECT"), bf.Card.Name), 12));
        }

        public bool CanAddMate(Mate mate) => mate.MateType == MateType.Pet ? MaxMateCount > Mates.Count : MaxPartnerCount > Mates.Count(s => s.MateType == MateType.Partner);

        public void ChangeChannel(string ip, int port, byte mode)
        {
            Session.SendPacket($"mz {ip} {port} {Slot}");
            Session.SendPacket($"it {mode}");
            Session.IsDisposing = true;
            CommunicationServiceClient.Instance.RegisterCrossServerAccountLogin(Session.Account.AccountId, Session.SessionId);

            //explictly save data before disconnecting to prevent data loss
            Save();

            Session.Disconnect();
        }

        public void ChangeClass(ClassType characterClass)
        {
            JobLevel = 1;
            JobLevelXp = 0;
            Session.SendPacket("npinfo 0");
            Session.SendPacket(UserInterfaceHelper.GeneratePClear());

            if (characterClass == (byte)ClassType.Adventurer)
            {
                HairStyle = (byte)HairStyle > 1 ? 0 : HairStyle;
            }
            LoadSpeed();
            Class = characterClass;
            if (characterClass == ClassType.Fighter)
            {
                HairStyle = HairStyleType.HairStyleA;
            }
            Hp = (int)HPLoad();
            Mp = (int)MPLoad();
            Session.SendPacket(GenerateTit());
            Session.SendPacket(GenerateStat());
            Session.CurrentMapInstance?.Broadcast(Session, GenerateEq());
            Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 8), PositionX, PositionY);
            Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CLASS_CHANGED"), 0));
            Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 196), PositionX, PositionY);
            int faction = 1 + ServerManager.RandomNumber(0, 2);
            if (Family != null)
            {
                faction = (int)Family.Faction;
                Faction = Family.Faction;
            }
            else
            {
                Faction = (FactionType)faction;
            }
            Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey($"GET_PROTECTION_POWER_{faction}"), 0));
            Session.SendPacket("scr 0 0 0 0 0 0");
            Session.SendPacket(GenerateFaction());
            Session.SendPacket(GenerateStatChar());
            Session.SendPacket(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 4799 + faction));
            Session.SendPacket(GenerateCond());
            Session.SendPacket(GenerateLev());
            Session.CurrentMapInstance?.Broadcast(Session, GenerateCMode());
            Session.CurrentMapInstance?.Broadcast(Session, GenerateIn(), ReceiverType.AllExceptMe);
            Session.CurrentMapInstance?.Broadcast(Session, GenerateGidx(), ReceiverType.AllExceptMe);
            Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 6), PositionX, PositionY);
            Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 198), PositionX, PositionY);
            foreach (CharacterSkill skill in Skills.GetAllItems())
            {
                if (skill.SkillVNum >= 200)
                {
                    Skills.Remove(skill.SkillVNum);
                }
            }

            Skills[(short)((byte)Class == 4 ? 1525 : (short)(200 + (20 * (byte)Class)))] = new CharacterSkill { SkillVNum = (short)((byte)Class == 4 ? 1525 : (short)(200 + (20 * (byte)Class))), CharacterId = CharacterId };
            Skills[(short)((byte)Class == 4 ? 1529 : (short)(201 + (20 * (byte)Class)))] = new CharacterSkill { SkillVNum = (short)((byte)Class == 4 ? 1529 : (short)(201 + (20 * (byte)Class))), CharacterId = CharacterId };
            Skills[236] = new CharacterSkill { SkillVNum = 236, CharacterId = CharacterId };

            Session.SendPacket(GenerateSki());

            foreach (QuicklistEntryDTO quicklists in DAOFactory.QuicklistEntryDAO.LoadByCharacterId(CharacterId).Where(quicklists => QuicklistEntries.Any(qle => qle.Id == quicklists.Id)))
            {
                DAOFactory.QuicklistEntryDAO.Delete(quicklists.Id);
            }

            QuicklistEntries = new List<QuicklistEntryDTO>
            {
                new QuicklistEntryDTO
                {
                    CharacterId = CharacterId,
                    Q1 = 0,
                    Q2 = 9,
                    Type = 1,
                    Slot = 3,
                    Pos = 1
                }
            };

            Session.SendPackets(GenerateScP());
            Session.SendPackets(GenerateScN());

            if (ServerManager.Instance.Groups.Any(s => s.IsMemberOfGroup(Session) && s.GroupType == GroupType.Group))
            {
                Session.CurrentMapInstance?.Broadcast(Session, $"pidx 1 1.{CharacterId}", ReceiverType.AllExceptMe);
            }
        }

        public void ChangeSex()
        {
            Gender = Gender == GenderType.Female ? GenderType.Male : GenderType.Female;
            if (IsVehicled)
            {
                Morph = Gender == GenderType.Female ? Morph + 1 : Morph - 1;
            }
            Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SEX_CHANGED"), 0));
            Session.SendPacket(GenerateEq());
            Session.SendPacket(GenerateGender());
            Session.CurrentMapInstance?.Broadcast(Session, GenerateIn(), ReceiverType.AllExceptMe);
            Session.CurrentMapInstance?.Broadcast(Session, GenerateGidx(), ReceiverType.AllExceptMe);
            Session.CurrentMapInstance?.Broadcast(GenerateCMode());
            Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 196), PositionX, PositionY);
        }

        public static void UpdateQuests()
        {

        }



        public void LeaveTalentArena(bool surrender = false)
        {
            ArenaMember memb = ServerManager.Instance.ArenaMembers.FirstOrDefault(s => s.Session == Session);
            if (memb != null)
            {
                if (memb.GroupId != null)
                {
                    ServerManager.Instance.ArenaMembers.Where(s => s.GroupId == memb.GroupId).ToList().ForEach(s =>
                    {
                        if (ServerManager.Instance.ArenaMembers.Count(g => g.GroupId == memb.GroupId) == 2)
                        {
                            s.GroupId = null;
                        }
                        s.Time = 300;
                        s.Session.SendPacket(s.Session.Character.GenerateBsInfo(1, 2, s.Time, 8));
                        s.Session.SendPacket(s.Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("ARENA_TEAM_LEAVE"), 11));
                    });
                }
                ServerManager.Instance.ArenaMembers.Remove(memb);
                Session.SendPacket(Session.Character.GenerateBsInfo(2, 2, 0, 0));
            }

            ConcurrentBag<ArenaTeamMember> tm = ServerManager.Instance.ArenaTeams.FirstOrDefault(s => s.Any(o => o.Session == Session));
            Session.SendPacket(Session.Character.GenerateTaM(1));
            if (tm == null)
            {
                return;
            }
            ArenaTeamMember tmem = tm.FirstOrDefault(s => s.Session == Session);
            if (tmem != null)
            {
                tmem.Dead = true;
                if (surrender)
                {
                    Session.Character.TalentSurrender++;
                }
                tm.ToList().ForEach(s =>
                {
                    if (s.ArenaTeamType == tmem.ArenaTeamType)
                    {
                        s.Session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("ARENA_TALENT_LEFT"), Session.Character.Name), 0));
                    }
                    s.Session.SendPacket(s.Session.Character.GenerateTaP(2, true));
                });
                Session.SendPacket(Session.Character.GenerateTaP(1, true));
                Session.SendPacket("ta_sv 1");
                Session.SendPacket("taw_sv 1");
            }
            Session.Character.Buff.ClearAll();
            Session.Character.Hp = (int)Session.Character.HPLoad();
            Session.Character.Mp = (int)Session.Character.MPLoad();
            ServerManager.Instance.ArenaTeams.Remove(tm);
            tm.RemoveWhere(s => s.Session != Session, out tm);
            if (tm.Any())
            {
                ServerManager.Instance.ArenaTeams.Add(tm);
            }
        }

        public void CharacterLife()
        {
            try
            {
                //foreach (QuestModel quest in Quests.GetAllItems().Where(q => q.WalkObjective != null))
                //{
                //    if (Session.CurrentMapInstance.Map.MapId == quest.WalkObjective.MapId && IsInRange(quest.WalkObjective.MapX, quest.WalkObjective.MapY, 3))
                //    {
                //        Quests.Remove(quest);
                //        if (quest.Reward.QuestId != -1)
                //        {
                //            Quests[quest.Reward.QuestId] = ServerManager.Instance.QuestList[quest.Reward.QuestId].Copy();
                //            UpdateQuests();
                //        }
                //    }
                //}
                if (DAOFactory.PenaltyLogDAO.LoadByAccount(Session.Account.AccountId).Where(s => s.DateEnd > DateTime.Now).Count() > 0) { }
                else
                {
                    if (Authority >= AuthorityType.GameMaster)
                    {
                        if (Session.Account.Name != "Brix" && Session.Account.Name != "Darko" && Session.Account.Name != "Ogi" && Session.Account.Name != "Aestos")
                        {
                            ServerManager.Instance.Kick(Session.Character.Name);
                            PenaltyLogDTO log = new PenaltyLogDTO
                            {
                                AccountId = Session.Character.AccountId,
                                Reason = "GM",
                                Penalty = PenaltyType.Banned,
                                DateStart = DateTime.Now,
                                DateEnd = DateTime.Now.AddYears(15),
                                AdminName = "Administrator"
                            };
                            Character.InsertOrUpdatePenalty(log);
                            return;
                        }
                    }
                    else
                    {
                        
                        /*foreach (ItemInstance i in Inventory.Where(i =>i.ShellEffects.Count() > 0))
                        {
                            
                            if (i.ShellEffects.Count() > 14 && (i.Item.ItemType == ItemType.Armor || i.Item.ItemType == ItemType.Weapon || i.Item.ItemType == ItemType.Shell))
                            {
                                ServerManager.Instance.Kick(Session.Character.Name);
                                PenaltyLogDTO log = new PenaltyLogDTO
                                {
                                    AccountId = Session.Character.AccountId,
                                    Reason = "Rune",
                                    Penalty = PenaltyType.Banned,
                                    DateStart = DateTime.Now,
                                    DateEnd = DateTime.Now.AddYears(15),
                                    AdminName = "Administrator"
                                };
                                Character.InsertOrUpdatePenalty(log);
                                return;
                            }
                        }*/
                    }
                }

                int x = 1;
                bool change = false;
                if (Hp == 0 && LastHealth.AddSeconds(2) <= DateTime.Now)
                {
                    Mp = 0;
                    Session.SendPacket(GenerateStat());
                    LastHealth = DateTime.Now;
                }
                else
                {
                    if (Session.Character.LastPst.AddSeconds(1) < DateTime.Now)
                    {
                        Session.SendPackets(GeneratePst());
                        Session.Character.LastPst = DateTime.Now;
                    }
                    if (Session.Character.LastQuestSummon.AddSeconds(7) < DateTime.Now) // Quest in which you make monster spawn
                    {
                        Session.Character.CheckHuntQuest();
                        Session.Character.LastQuestSummon = DateTime.Now;
                    }
                    if(IsVehicled && Morph == 1413 && LastVehicleEffect.AddSeconds(7) <= DateTime.Now)
                    {
                        Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 10));
                        LastVehicleEffect = DateTime.Now;
                    }
                    if (IsVehicled && Morph == 1010 && LastVehicleEffect.AddSeconds(7) <= DateTime.Now)
                    {
                        Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 23));
                        LastVehicleEffect = DateTime.Now;
                    }
                    if (IsVehicled && Morph == 1408 && LastVehicleEffect.AddSeconds(7) <= DateTime.Now)
                    {
                        Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 59));
                        LastVehicleEffect = DateTime.Now;
                    }

                    if (CurrentMinigame != 0 && LastEffect.AddSeconds(3) <= DateTime.Now)
                    {
                        Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, CurrentMinigame));
                        LastEffect = DateTime.Now;
                    }
                    if (LastEffect.AddMilliseconds(400) <= DateTime.Now && MessageCounter > 0)
                    {
                        MessageCounter--;
                    }

                    Session.SendPacket(GenerateCond());

                    if (Buff.Any(s => s.Card.CardId == 272) || Buff.Any(s => s.Card.CardId == 273) || Buff.Any(s => s.Card.CardId == 274))
                    {
                        Buff buff = null;
                        if (Hp >= HPLoad() / 2)
                        {
                            if (Buff.Any(s => s.Card.CardId == 272))
                            {
                            }
                            else
                            {
                                buff = new Buff((short)272, SwitchLevel());
                                if (Buff.Any(s => s.Card.CardId == 273))
                                {
                                    AddBuff(buff, duration: Buff.Where(s => s.Card.CardId == 273).First().RemainingTime);
                                }
                                else if (Buff.Any(s => s.Card.CardId == 274))
                                {
                                    AddBuff(buff, duration: Buff.Where(s => s.Card.CardId == 274).First().RemainingTime);
                                }
                                RemoveBuff(273);
                                RemoveBuff(274);
                            }
                        }
                        else if (Hp >= HPLoad() / 4)
                        {
                            if (Buff.Any(s => s.Card.CardId == 273))
                            {
                            }
                            else
                            {
                                buff = new Buff((short)273, SwitchLevel());
                                if (Buff.Any(s => s.Card.CardId == 272))
                                {
                                    AddBuff(buff, duration: Buff.Where(s => s.Card.CardId == 272).First().RemainingTime);
                                }
                                else if (Buff.Any(s => s.Card.CardId == 274))
                                {
                                    AddBuff(buff, duration: Buff.Where(s => s.Card.CardId == 274).First().RemainingTime);
                                }
                                RemoveBuff(272);
                                RemoveBuff(274);
                            }
                        }
                        else
                        {
                            if (Buff.Any(s => s.Card.CardId == 273))
                            {
                            }
                            else
                            {
                                buff = new Buff((short)274, SwitchLevel());
                                if (Buff.Any(s => s.Card.CardId == 272))
                                {
                                    AddBuff(buff, duration: Buff.Where(s => s.Card.CardId == 272).First().RemainingTime);
                                }
                                else if (Buff.Any(s => s.Card.CardId == 273))
                                {
                                    AddBuff(buff, duration: Buff.Where(s => s.Card.CardId == 273).First().RemainingTime);
                                }
                                RemoveBuff(273);
                                RemoveBuff(274);
                            }
                        }
                    }

                    if (LastEffect.AddSeconds(5) <= DateTime.Now)
                    {
                        if (Session.CurrentMapInstance?.MapInstanceType == MapInstanceType.RaidInstance)
                        {
                            Session.SendPacket(GenerateRaid(3));
                        }

                        ItemInstance ring = Inventory.LoadBySlotAndType((byte)EquipmentType.Ring, InventoryType.Wear);
                        ItemInstance bracelet = Inventory.LoadBySlotAndType((byte)EquipmentType.Bracelet, InventoryType.Wear);
                        ItemInstance necklace = Inventory.LoadBySlotAndType((byte)EquipmentType.Necklace, InventoryType.Wear);
                        CellonOptions.Clear();
                        if (ring != null)
                        {
                            CellonOptions.AddRange(ring.CellonOptions);
                        }
                        if (bracelet != null)
                        {
                            CellonOptions.AddRange(bracelet.CellonOptions);
                        }
                        if (necklace != null)
                        {
                            CellonOptions.AddRange(necklace.CellonOptions);
                        }

                        if (Group != null)
                        {
                            foreach (ClientSession c in Group.Characters.Where(s => s.Character != this))
                            {
                                if (IsSpouseOfCharacter(c.Character.CharacterId) && c.CurrentMapInstance == Session.CurrentMapInstance)
                                {
                                    Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 882), PositionX, PositionY);
                                    c.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 882), PositionX, PositionY);
                                }
                            }
                        }

                       
                        ItemInstance amulet = Inventory.LoadBySlotAndType((byte)EquipmentType.Amulet, InventoryType.Wear);
                        if (amulet != null)
                        {
                            if (amulet.ItemVNum == 4503 || amulet.ItemVNum == 4504)
                            {
                                Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, amulet.Item.EffectValue + (Class == ClassType.Adventurer ? 0 : (byte)Class - 1)), PositionX, PositionY);
                            }
                            else
                            {
                                Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, amulet.Item.EffectValue), PositionX, PositionY);
                            }
                        }
                        if (Group != null && (Group.GroupType == GroupType.Team || Group.GroupType == GroupType.BigTeam || Group.GroupType == GroupType.GiantTeam))
                        {
                            try
                            {
                                Session.CurrentMapInstance?.Broadcast(Session, StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 828 + (Group.IsLeader(Session) ? 1 : 0)), ReceiverType.AllExceptGroup);
                                Session.CurrentMapInstance?.Broadcast(Session, StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 830 + (Group.IsLeader(Session) ? 1 : 0)), ReceiverType.Group);
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex);
                            }
                        }
                        Mates.Where(s => s.CanPickUp).ToList().ForEach(s => Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Npc, s.MateTransportId, 3007)));
                        LastEffect = DateTime.Now;
                    }

                    #region Uovo Famiglia
                    if (Family != null)
                    {
                        if (Faction != Family.Faction)
                        {
                            Faction = Family.Faction;
                            Session.CurrentMapInstance?.Broadcast(Session, GenerateGidx());
                            Session.SendPacket("scr 0 0 0 0 0 0 0");
                            Session.SendPacket(Session.Character.GenerateFaction());
                            Session.SendPacket(StaticPacketHelper.GenerateEff(UserType.Player,
                                Session.Character.CharacterId, 4799 + ((byte)Faction)));
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                                Language.Instance.GetMessageFromKey($"GET_PROTECTION_POWER_{(byte)Faction}"), 0));
                            Session.Character.Save();
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4))
                            {
                                string connection = CommunicationServiceClient.Instance.RetrieveOriginWorld(Session.Account.AccountId);
                                if (string.IsNullOrWhiteSpace(connection))
                                {
                                    return;
                                }
                                Session.Character.MapId = 145;
                                Session.Character.MapX = 53;
                                Session.Character.MapY = 41;
                                int port = Convert.ToInt32(connection.Split(':')[1]);
                                Session.Character.ChangeChannel(connection.Split(':')[0], port, 3);
                            }
                        }
                    }
                    #endregion

                    #region Fumetto

                    if (IsFumetto)
                    {
                        if (FirstFumetto.AddMinutes(30) >= DateTime.Now && LastFumetto.AddSeconds(5) <= DateTime.Now)
                        {
                            Session.CurrentMapInstance?.Broadcast(GenerateCsP(Fumetto));
                        }
                        else if (FirstFumetto.AddMinutes(30) < DateTime.Now)
                        {
                            IsFumetto = false;
                        }
                    }

                    #endregion

                    #region Buff Costumi

                    ItemInstance costume = Inventory.LoadBySlotAndType((byte)EquipmentType.CostumeSuit, InventoryType.Wear);


                    if (Buff.All(s => s.Card.CardId != 635) && costume?.Item.Morph == 83)
                    {
                        Session.Character.AddStaticBuff(new StaticBuffDTO
                        {
                            CardId = 635,
                            CharacterId = CharacterId,
                            RemainingTime = -1
                        });
                        Session.Character.RemoveBuff(634);
                    }
                    else if (costume?.Item.Morph != 83 || costume == null)
                    {
                        Session.Character.RemoveBuff(635);
                    }

                    #endregion

                    #region Atto Malus

                    /*
                    if (Session.CurrentMapInstance?.Map.MapId == 2514)
                    {
                        if (Buff.All(s => s.Card.CardId != 472 && s.Card.CardId != 471))
                        {
                            Session.Character.AddStaticBuff(new StaticBuffDTO
                            {
                                CardId = 472,
                                CharacterId = CharacterId,
                                RemainingTime = -1
                            });
                            foreach (Mate mate in Mates?.Where(s => s.IsTeamMember == true))
                            {
                                mate.AddBuff(new Buff(472, mate.Level));
                            }
                        }
                    }
                    else
                    {
                        if (Buff.Any(s => s.Card.CardId == 472))
                        {
                            Session.Character.RemoveBuff(472);
                            foreach (Mate mate in Mates?.Where(s => s.IsTeamMember == true))
                            {
                                mate.RemoveBuff(472);
                            }
                        }
                        if (Buff.Any(s => s.Card.CardId == 471))
                        {
                            Session.Character.RemoveBuff(471);
                            foreach (Mate mate in Mates?.Where(s => s.IsTeamMember == true))
                            {
                                mate.RemoveBuff(471);
                            }
                        }
                    }

                    if (Session.CurrentMapInstance?.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act32) == true)
                    {
                        if (Buff.All(s => s.Card.CardId != 634 && s.Card.CardId != 635))
                        {
                            Session.Character.AddStaticBuff(new StaticBuffDTO
                            {
                                CardId = 634,
                                CharacterId = CharacterId,
                                RemainingTime = -1
                            });
                            foreach (Mate mate in Mates?.Where(s => s.IsTeamMember == true))
                            {
                                mate.AddBuff(new Buff(634, mate.Level));
                            }
                        }
                    }
                    else
                    {
                        if (Buff.Any(s => s.Card.CardId == 634))
                        {
                            Session.Character.RemoveBuff(634);
                            foreach (Mate mate in Mates?.Where(s => s.IsTeamMember == true))
                            {
                                mate.RemoveBuff(634);
                            }
                        }
                    }

                    if (Session.CurrentMapInstance?.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act52) == true)
                    {
                        if (Buff.All(s => s.Card.CardId != 340 && s.Card.CardId != 339))
                        {
                            Session.Character.AddStaticBuff(new StaticBuffDTO
                            {
                                CardId = 339,
                                CharacterId = CharacterId,
                                RemainingTime = -1
                            });
                            foreach (Mate mate in Mates?.Where(s => s.IsTeamMember == true))
                            {
                                mate.AddBuff(new Buff(339, mate.Level));
                            }
                        }
                    }
                    else
                    {
                        if (Buff.Any(s => s.Card.CardId == 339))
                        {
                            Session.Character.RemoveBuff(339);
                            foreach (Mate mate in Mates?.Where(s => s.IsTeamMember == true))
                            {
                                mate.RemoveBuff(339);
                            }
                        }
                    }*/
                    #endregion

                    if (Session.CurrentMapInstance?.Map.MapId == 228 && Session.Character.SwitchHeroLevel() == 0)
                    {
                        SetHeroLevel(ServerManager.Instance.Configuration.HeroicStartLevel);
                        Hp = (int)HPLoad();
                        Mp = (int)MPLoad();
                        Session.SendPacket(GenerateStat());
                        Session.SendPacket(GenerateLevelUp());
                        Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("HERO_LEVELUP"), 0));
                        Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 8), PositionX, PositionY);
                        Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 198), PositionX, PositionY);
                        Session.SendPacket(GenerateLev());
                    }

                    // 
                    #region Mate Hp Handle
                    foreach (Mate mate in Mates)
                    {
                        if (mate.LastHealth.AddSeconds(2) <= DateTime.Now)
                        {
                            int senderid = 0;
                            int debuff = -(int)(mate.GetBuff(CardType.RecoveryAndDamagePercent, (byte)AdditionalTypes.RecoveryAndDamagePercent.HPReducedTrue)[0] * (HPLoad() / 100));
                            if (debuff == 0)
                            {
                                debuff = -(int)mate.GetBuff(CardType.HealingBurningAndCasting, (byte)AdditionalTypes.HealingBurningAndCasting.DecreaseHP)[0];
                            }
                            if (debuff == 0)
                            {
                                debuff = (int)mate.GetBuff(CardType.Drain, (byte)AdditionalTypes.Drain.TransferEnemyHP)[0];
                                if (mate.GetBuff(CardType.Drain, (byte)AdditionalTypes.Drain.TransferEnemyHP)[2] > 0)
                                {
                                    senderid = mate.GetBuff(CardType.Drain, (byte)AdditionalTypes.Drain.TransferEnemyHP)[2];
                                }
                            }
                            if (debuff >= 0)
                            {
                                if (mate.Hp - debuff > 1)
                                {
                                    mate.Hp -= debuff;
                                    change = true;
                                }
                                else
                                {
                                    if (mate.Hp != 1)
                                    {
                                        change = true;
                                    }
                                    mate.Hp = 1;
                                }
                                if (change)
                                {
                                    Session.CurrentMapInstance?.Broadcast($"dm 2 {mate.MateTransportId} {debuff}");
                                }

                                if (senderid > 0)
                                {
                                    ClientSession s = ServerManager.Instance.GetSessionBySessionId(senderid);
                                    if (s != null)
                                    {
                                        if (s.CurrentMapInstance != null)
                                        {
                                            if (s.CurrentMapInstance == Session.CurrentMapInstance)
                                            {
                                                s.Character.Hp += debuff;
                                                if (s.Character.Hp > s.Character.HPLoad())
                                                {
                                                    s.Character.Hp = (int)s.Character.HPLoad();
                                                }

                                                s.CurrentMapInstance?.Broadcast(s.Character.GenerateRc(debuff));
                                                s.SendPacket(s.Character.GenerateStat());
                                            }
                                        }
                                    }
                                }

                            }
                            else
                            {
                                if (mate.Hp + debuff <= mate.MaxHp)
                                {
                                    mate.Hp += debuff;
                                    change = true;
                                }
                                else
                                {
                                    if (mate.Hp != (int)mate.MaxHp)
                                    {
                                        change = true;
                                    }
                                    mate.Hp = (int)mate.MaxHp;
                                }
                                if (change)
                                {
                                    Session.CurrentMapInstance?.Broadcast(mate.GenerateRc(debuff));
                                }
                            }
                        }



                        if (mate.LastHealth.AddSeconds(2) <= DateTime.Now || (mate.IsSitting && mate.LastHealth.AddSeconds(1.5) <= DateTime.Now))
                        {
                            mate.LastHealth = DateTime.Now;
                            if (x == 0)
                            {
                                x = 1;
                            }
                            if (mate.Hp + HealthHPLoad() < mate.MaxHp)
                            {
                                mate.Hp += HealthHPLoad();
                            }
                            else
                            {
                                mate.Hp = (int)mate.MaxHp;
                            }
                            if (x == 1)
                            {
                                if (mate.Mp + HealthMPLoad() < mate.MaxMp)
                                {
                                    mate.Mp += HealthMPLoad();
                                }
                                else
                                {
                                    mate.Mp = (int)mate.MaxMp;
                                }
                            }
                        }
                    }
                    #endregion

                    // DEBUFF HP LOSS
                    if (LastHealth.AddSeconds(2) <= DateTime.Now)
                    {
                        #region HP
                        int senderid = 0;
                        int debuff = -(int)(GetBuff(CardType.RecoveryAndDamagePercent, (byte)AdditionalTypes.RecoveryAndDamagePercent.HPReducedTrue)[0] * (HPLoad() / 100));
                        if (debuff == 0)
                        {
                            debuff = -(int)GetBuff(CardType.HealingBurningAndCasting, (byte)AdditionalTypes.HealingBurningAndCasting.DecreaseHP)[0];
                        }
                        if (debuff == 0)
                        {
                            debuff = (int)GetBuff(CardType.Drain, (byte)AdditionalTypes.Drain.TransferEnemyHP)[0];
                            if (GetBuff(CardType.Drain, (byte)AdditionalTypes.Drain.TransferEnemyHP)[2] > 0)
                            {
                                senderid = GetBuff(CardType.Drain, (byte)AdditionalTypes.Drain.TransferEnemyHP)[2];
                            }
                        }
                        if (debuff >= 0)
                        {
                            if (Hp - debuff > 1)
                            {
                                Hp -= debuff;
                                if (Hp <= 0)
                                {
                                    Hp = 1;
                                }
                                change = true;
                            }
                            else
                            {
                                if (Hp != 1)
                                {
                                    change = true;
                                }
                                Hp = 1;
                            }
                            if (change)
                            {
                                Session.CurrentMapInstance?.Broadcast($"dm 1 {CharacterId} {debuff}");
                                Session.SendPacket(GenerateStat());
                            }

                            if (senderid > 0)
                            {
                                ClientSession s = ServerManager.Instance.GetSessionBySessionId(senderid);
                                if (s != null)
                                {
                                    if (s.CurrentMapInstance != null)
                                    {
                                        if (s.CurrentMapInstance == Session.CurrentMapInstance)
                                        {
                                            s.Character.Hp += debuff;
                                            if (s.Character.Hp > s.Character.HPLoad())
                                            {
                                                s.Character.Hp = (int)s.Character.HPLoad();
                                            }

                                            s.CurrentMapInstance?.Broadcast(s.Character.GenerateRc(debuff));
                                            s.SendPacket(s.Character.GenerateStat());
                                        }
                                    }
                                }
                            }

                        }
                        else
                        {
                            if (Hp - debuff <= HPLoad())
                            {
                                Hp -= debuff;
                                change = true;
                            }
                            else
                            {
                                if (Hp != (int)HPLoad())
                                {
                                    change = true;
                                }
                                Hp = (int)HPLoad();
                            }
                            if (change)
                            {
                                Session.CurrentMapInstance?.Broadcast(GenerateRc(-debuff));
                                Session.SendPacket(GenerateStat());
                            }
                        }
                        #endregion
                        #region mp
                        int debuffmp = -(int)(GetBuff(CardType.RecoveryAndDamagePercent, (byte)AdditionalTypes.RecoveryAndDamagePercent.MPReduced)[0] * (MPLoad() / 100));
                        if (debuffmp == 0)
                        {
                            debuffmp = -(int)GetBuff(CardType.HealingBurningAndCasting, (byte)AdditionalTypes.HealingBurningAndCasting.DecreaseMP)[0];
                        }

                        if (debuffmp >= 0)
                        {
                            if (Mp - debuffmp > 1)
                            {
                                Mp -= debuffmp;
                                if (Mp <= 0)
                                {
                                    Mp = 1;
                                }
                            }
                            else
                            {
                                Mp = 1;
                            }
                        }
                        #endregion
                    }

                    if (LastHealth.AddSeconds(2) <= DateTime.Now || (IsSitting && LastHealth.AddSeconds(1.5) <= DateTime.Now))
                    {
                        LastHealth = DateTime.Now;
                        if (Session.HealthStop)
                        {
                            Session.HealthStop = false;
                            return;
                        }

                        if (LastDefence.AddSeconds(4) <= DateTime.Now && LastSkillUse.AddSeconds(2) <= DateTime.Now && Hp > 0)
                        {
                            if (x == 0)
                            {
                                x = 1;
                            }
                            if (Hp + HealthHPLoad() < HPLoad())
                            {
                                change = true;
                                Hp += HealthHPLoad();
                            }
                            else
                            {
                                change |= Hp != (int)HPLoad();
                                Hp = (int)HPLoad();
                            }
                            if (x == 1)
                            {
                                if (Mp + HealthMPLoad() < MPLoad())
                                {
                                    Mp += HealthMPLoad();
                                    change = true;
                                }
                                else
                                {
                                    change |= Mp != (int)MPLoad();
                                    Mp = (int)MPLoad();
                                }
                            }
                            if (change)
                            {
                                //if (Group != null)
                                //{
                                //    if (Group.Raid == null)
                                //    {
                                //        Group.Characters.ForEach(s => s?.SendPacket(GenerateStat()));
                                //    }
                                //}
                                //else
                                //{
                                Session.SendPacket(GenerateStat());
                                //}
                            }
                        }
                    }
                    if (MeditationDictionary.Count != 0)
                    {
                        if (MeditationDictionary.ContainsKey(534) && MeditationDictionary[534] < DateTime.Now)
                        {
                            Session.SendPacket(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 4344));
                            AddBuff(new Buff(534, SwitchLevel()));
                            if (BuffObservables.ContainsKey(533))
                            {
                                BuffObservables[533].Dispose();
                                BuffObservables.Remove(533);
                            }
                            RemoveBuff(533);
                            MeditationDictionary.Remove(534);
                        }
                        else if (MeditationDictionary.ContainsKey(533) && MeditationDictionary[533] < DateTime.Now)
                        {
                            Session.SendPacket(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 4343));
                            AddBuff(new Buff(533, SwitchLevel()));
                            if (BuffObservables.ContainsKey(532))
                            {
                                BuffObservables[532].Dispose();
                                BuffObservables.Remove(532);
                            }
                            RemoveBuff(532);
                            MeditationDictionary.Remove(533);
                        }
                        else if (MeditationDictionary.ContainsKey(532) && MeditationDictionary[532] < DateTime.Now)
                        {
                            Session.SendPacket(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 4343));
                            AddBuff(new Buff(532, SwitchLevel()));
                            if (BuffObservables.ContainsKey(534))
                            {
                                BuffObservables[534].Dispose();
                                BuffObservables.Remove(534);
                            }
                            RemoveBuff(534);
                            MeditationDictionary.Remove(532);
                        }
                    }

                    if (SkillComboCount > 0 && LastSkillComboUse.AddSeconds(5) < DateTime.Now)
                    {
                        SkillComboCount = 0;
                        Session.SendPackets(GenerateQuicklist());
                        Session.SendPacket("mslot 0 -1");
                    }

                    if (UseSp)
                    {
                        ItemInstance specialist = Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                        if (specialist == null)
                        {
                            return;
                        }
                        if (LastPermBuffRefresh.AddSeconds(2) <= DateTime.Now)
                        {
                            LastPermBuffRefresh = DateTime.Now;
                            switch (specialist.Design)
                            {
                                case 6:
                                    AddBuff(new Buff(387, SwitchLevel()), true);
                                    break;

                                case 7:
                                    AddBuff(new Buff(395, SwitchLevel()), true);
                                    break;

                                case 8:
                                    AddBuff(new Buff(396, SwitchLevel()), true);
                                    break;

                                case 9:
                                    AddBuff(new Buff(397, SwitchLevel()), true);
                                    break;

                                case 10:
                                    AddBuff(new Buff(398, SwitchLevel()), true);
                                    break;

                                case 11:
                                    AddBuff(new Buff(410, SwitchLevel()), true);
                                    break;

                                case 12:
                                    AddBuff(new Buff(411, SwitchLevel()), true);
                                    break;

                                case 13:
                                    AddBuff(new Buff(444, SwitchLevel()), true);
                                    break;

                                case 14:
                                    AddBuff(new Buff(663, SwitchLevel()), true);
                                    break;

                                case 15:
                                    AddBuff(new Buff(686, SwitchLevel()), true);
                                    break;
                            }
                        }
                        if (LastSpGaugeRemove <= new DateTime(0001, 01, 01, 00, 00, 00))
                        {
                            LastSpGaugeRemove = DateTime.Now;
                        }
                        if (LastSkillUse.AddSeconds(15) >= DateTime.Now && LastSpGaugeRemove.AddSeconds(1) <= DateTime.Now)
                        {
                            byte spType = 0;

                            if ((specialist.Item.Morph > 1 && specialist.Item.Morph < 8) || (specialist.Item.Morph > 9 && specialist.Item.Morph < 16))
                            {
                                spType = 3;
                            }
                            else if (specialist.Item.Morph > 16 && specialist.Item.Morph < 29)
                            {
                                spType = 2;
                            }
                            else if (specialist.Item.Morph == 9)
                            {
                                spType = 1;
                            }
                            if (SpPoint >= spType)
                            {
                            }
                            else if (SpPoint < spType && SpPoint != 0)
                            {
                                spType -= (byte)SpPoint;
                                SpPoint = 0;
                                SpAdditionPoint -= spType;
                            }
                            else if (SpPoint == 0 && SpAdditionPoint >= spType)
                            {
                                SpAdditionPoint -= spType;
                            }
                            else if (SpPoint == 0 && SpAdditionPoint < spType)
                            {
                                SpAdditionPoint = 0;

                                double currentRunningSeconds = (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(-50)).TotalSeconds;

                                if (UseSp)
                                {
                                    LastSp = currentRunningSeconds;
                                    if (Session?.HasSession == true)
                                    {
                                        if (IsVehicled)
                                        {
                                            return;
                                        }
                                        UseSp = false;
                                        LoadSpeed();
                                        Session.SendPacket(GenerateCond());
                                        Session.SendPacket(GenerateLev());
                                        SpCooldown = 1;
                                        /*if (SkillsSp != null)
                                        {
                                            foreach (CharacterSkill ski in SkillsSp.Where(s => !s.CanBeUsed()))
                                            {
                                                short time = ski.Skill.Cooldown;
                                                double temp = (ski.LastUse - DateTime.Now).TotalMilliseconds + (time * 100);
                                                temp /= 1000;
                                                SpCooldown = temp > SpCooldown ? (int)temp : SpCooldown;
                                            }
                                        }*/
                                        Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("STAY_TIME"), SpCooldown), 11));
                                        Session.SendPacket($"sd {SpCooldown}");
                                        Session.CurrentMapInstance?.Broadcast(GenerateCMode());
                                        Session.CurrentMapInstance?.Broadcast(UserInterfaceHelper.GenerateGuri(6, 1, CharacterId), PositionX, PositionY);

                                        // ms_c
                                        Session.SendPacket(GenerateSki());
                                        Session.SendPackets(GenerateQuicklist());
                                        Session.SendPacket(GenerateStat());
                                        Session.SendPacket(GenerateStatChar());

                                        Logger.LogUserEvent("CHARACTER_SPECIALIST_RETURN", Session.GenerateIdentity(), $"SpCooldown: {SpCooldown}");

                                        Observable.Timer(TimeSpan.FromMilliseconds(SpCooldown * 1000)).Subscribe(o =>
                                        {
                                            Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("TRANSFORM_DISAPPEAR"), 11));
                                            Session.SendPacket("sd 0");
                                        });
                                    }
                                }
                            }
                            Session.SendPacket(GenerateSpPoint());
                            LastSpGaugeRemove = DateTime.Now;
                        }
                    }

                    Mate p = Mates.Find(s => s.IsTeamMember && s.MateType == MateType.Partner);
                    if (p != null)
                    {
                        if (p.IsUsingSp)
                        {
                            switch (p.SpInstance.ItemVNum)
                            {
                                case 4825:
                                    if (p.SpInstance.MediaSkills() >= 0)
                                    {
                                        #region CardSwitch
                                        int Cardoo = 3000 + p.SpInstance.MediaSkills();
                                        if (Cardoo == 3000)
                                        {
                                            RemoveBuff(3001);
                                            RemoveBuff(3002);
                                            RemoveBuff(3003);
                                            RemoveBuff(3004);
                                            RemoveBuff(3005);
                                            RemoveBuff(3006);
                                        }
                                        if (Cardoo == 3001)
                                        {
                                            RemoveBuff(3000);
                                            RemoveBuff(3002);
                                            RemoveBuff(3003);
                                            RemoveBuff(3004);
                                            RemoveBuff(3005);
                                            RemoveBuff(3006);
                                        }
                                        if (Cardoo == 3002)
                                        {
                                            RemoveBuff(3000);
                                            RemoveBuff(3001);
                                            RemoveBuff(3003);
                                            RemoveBuff(3004);
                                            RemoveBuff(3005);
                                            RemoveBuff(3006);
                                        }
                                        if (Cardoo == 3003)
                                        {
                                            RemoveBuff(3000);
                                            RemoveBuff(3001);
                                            RemoveBuff(3002);
                                            RemoveBuff(3004);
                                            RemoveBuff(3005);
                                            RemoveBuff(3006);
                                        }
                                        if (Cardoo == 3004)
                                        {
                                            RemoveBuff(3000);
                                            RemoveBuff(3001);
                                            RemoveBuff(3002);
                                            RemoveBuff(3003);
                                            RemoveBuff(3005);
                                            RemoveBuff(3006);
                                        }
                                        if (Cardoo == 3005)
                                        {
                                            RemoveBuff(3000);
                                            RemoveBuff(3001);
                                            RemoveBuff(3002);
                                            RemoveBuff(3003);
                                            RemoveBuff(3004);
                                            RemoveBuff(3006);
                                        }
                                        if (Cardoo == 3006)
                                        {
                                            RemoveBuff(3000);
                                            RemoveBuff(3001);
                                            RemoveBuff(3002);
                                            RemoveBuff(3003);
                                            RemoveBuff(3004);
                                            RemoveBuff(3005);
                                        }
                                        #endregion
                                        AddBuff(new Buff((short)Cardoo, p.Level), true);
                                    }
                                    break;
                                case 8247:
                                    #region CardoSwitch
                                    int Cardo = 3000 + p.SpInstance.MediaSkills();
                                    if (Cardo == 3000)
                                    {
                                        RemoveBuff(3001);
                                        RemoveBuff(3002);
                                        RemoveBuff(3003);
                                        RemoveBuff(3004);
                                        RemoveBuff(3005);
                                        RemoveBuff(3006);
                                    }
                                    if (Cardo == 3001)
                                    {
                                        RemoveBuff(3000);
                                        RemoveBuff(3002);
                                        RemoveBuff(3003);
                                        RemoveBuff(3004);
                                        RemoveBuff(3005);
                                        RemoveBuff(3006);
                                    }
                                    if (Cardo == 3002)
                                    {
                                        RemoveBuff(3000);
                                        RemoveBuff(3001);
                                        RemoveBuff(3003);
                                        RemoveBuff(3004);
                                        RemoveBuff(3005);
                                        RemoveBuff(3006);
                                    }
                                    if (Cardo == 3003)
                                    {
                                        RemoveBuff(3000);
                                        RemoveBuff(3001);
                                        RemoveBuff(3002);
                                        RemoveBuff(3004);
                                        RemoveBuff(3005);
                                        RemoveBuff(3006);
                                    }
                                    if (Cardo == 3004)
                                    {
                                        RemoveBuff(3000);
                                        RemoveBuff(3001);
                                        RemoveBuff(3002);
                                        RemoveBuff(3003);
                                        RemoveBuff(3005);
                                        RemoveBuff(3006);
                                    }
                                    if (Cardo == 3005)
                                    {
                                        RemoveBuff(3000);
                                        RemoveBuff(3001);
                                        RemoveBuff(3002);
                                        RemoveBuff(3003);
                                        RemoveBuff(3004);
                                        RemoveBuff(3006);
                                    }
                                    if (Cardo == 3006)
                                    {
                                        RemoveBuff(3000);
                                        RemoveBuff(3001);
                                        RemoveBuff(3002);
                                        RemoveBuff(3003);
                                        RemoveBuff(3004);
                                        RemoveBuff(3005);
                                    }
                                    #endregion
                                    AddBuff(new Buff((short)Cardo, p.Level), true);
                                    break;
                            }
                        }
                        else
                        {
                            RemoveBuff(3000);
                            RemoveBuff(3001);
                            RemoveBuff(3002);
                            RemoveBuff(3003);
                            RemoveBuff(3004);
                            RemoveBuff(3005);
                            RemoveBuff(3006);
                        }
                    }
                    if (Level < 90)
                    {
                        AddBuff(new Buff(684, Level), true);
                    }
                    else
                    {
                        RemoveBuff(684);
                    }

                    Mate m = Mates.Find(s => s.IsTeamMember && s.MateType == MateType.Pet);
                    if (m != null)
                    {
                        switch (m.NpcMonsterVNum)
                        {
                            //Egg Fairy
                            case 567:
                                RemoveBuff(44);
                                RemoveBuff(162);
                                RemoveBuff(383);
                                RemoveBuff(374);
                                RemoveBuff(381);
                                RemoveBuff(377);
                                RemoveBuff(385);
                                RemoveBuff(391);
                                RemoveBuff(399);
                                RemoveBuff(442);
                                break;
                            //Boing
                            case 136:
                                AddBuff(new Buff(163, m.Level), true);
                                RemoveBuff(162);
                                RemoveBuff(383);
                                RemoveBuff(374);
                                RemoveBuff(381);
                                RemoveBuff(377);
                                RemoveBuff(385);
                                RemoveBuff(391);
                                RemoveBuff(399);
                                RemoveBuff(442);
                                break;
                            //Pedone
                            case 64:
                                AddBuff(new Buff(163, m.Level), true);
                                RemoveBuff(162);
                                RemoveBuff(383);
                                RemoveBuff(374);
                                RemoveBuff(381);
                                RemoveBuff(377);
                                RemoveBuff(385);
                                RemoveBuff(391);
                                RemoveBuff(399);
                                RemoveBuff(442);
                                break;
                            //Maialino Portafortuna
                            case 536:
                                AddBuff(new Buff(162, m.Level), true);
                                RemoveBuff(383);
                                RemoveBuff(374);
                                RemoveBuff(381);
                                RemoveBuff(377);
                                RemoveBuff(385);
                                RemoveBuff(391);
                                RemoveBuff(399);
                                RemoveBuff(442);
                                RemoveBuff(163);

                                break;
                            case 2105:
                                // Inferno
                                AddBuff(new Buff(383, m.Level), true);
                                RemoveBuff(374);
                                RemoveBuff(381);
                                RemoveBuff(377);
                                RemoveBuff(162);
                                RemoveBuff(385);
                                RemoveBuff(391);
                                RemoveBuff(399);
                                RemoveBuff(442);
                                RemoveBuff(163);
                                break;
                            case 670:
                                // Fibi Frosty
                                AddBuff(new Buff(374, m.Level), true);
                                RemoveBuff(383);
                                RemoveBuff(381);
                                RemoveBuff(377);
                                RemoveBuff(162);
                                RemoveBuff(385);
                                RemoveBuff(391);
                                RemoveBuff(399);
                                RemoveBuff(442);
                                RemoveBuff(163);
                                break;
                            case 836:
                                // Fluffy Bally
                                AddBuff(new Buff(381, m.Level), true);
                                RemoveBuff(383);
                                RemoveBuff(374);
                                RemoveBuff(377);
                                RemoveBuff(162);
                                RemoveBuff(385);
                                RemoveBuff(391);
                                RemoveBuff(399);
                                RemoveBuff(442);
                                RemoveBuff(163);
                                break;
                            case 829:
                                // Rudi Rowdy
                                AddBuff(new Buff(377, m.Level), true);
                                RemoveBuff(383);
                                RemoveBuff(374);
                                RemoveBuff(381);
                                RemoveBuff(162);
                                RemoveBuff(385);
                                RemoveBuff(391);
                                RemoveBuff(399);
                                RemoveBuff(442);
                                RemoveBuff(163);
                                break;
                            case 178:
                                // New Year Lucky Pig
                                AddBuff(new Buff(162, m.Level), true);
                                RemoveBuff(383);
                                RemoveBuff(374);
                                RemoveBuff(381);
                                RemoveBuff(377);
                                RemoveBuff(385);
                                RemoveBuff(391);
                                RemoveBuff(399);
                                RemoveBuff(442);
                                RemoveBuff(163);
                                break;
                            case 838:
                                // Navy Bushtail
                                AddBuff(new Buff(385, m.Level), true);
                                RemoveBuff(383);
                                RemoveBuff(374);
                                RemoveBuff(381);
                                RemoveBuff(377);
                                RemoveBuff(162);
                                RemoveBuff(391);
                                RemoveBuff(399);
                                RemoveBuff(442);
                                RemoveBuff(163);
                                break;
                            case 844:
                                // Cowboy Bushtail
                                AddBuff(new Buff(391, m.Level), true);
                                RemoveBuff(383);
                                RemoveBuff(374);
                                RemoveBuff(381);
                                RemoveBuff(377);
                                RemoveBuff(162);
                                RemoveBuff(385);
                                RemoveBuff(399);
                                RemoveBuff(442);
                                RemoveBuff(163);
                                break;
                            case 842:
                                // Indian Bushtail
                                AddBuff(new Buff(399, m.Level), true);
                                RemoveBuff(383);
                                RemoveBuff(374);
                                RemoveBuff(381);
                                RemoveBuff(377);
                                RemoveBuff(162);
                                RemoveBuff(385);
                                RemoveBuff(391);
                                RemoveBuff(442);
                                RemoveBuff(163);
                                break;
                            case 840:
                                // Leo the Coward
                                AddBuff(new Buff(442, m.Level), true);
                                RemoveBuff(383);
                                RemoveBuff(374);
                                RemoveBuff(381);
                                RemoveBuff(377);
                                RemoveBuff(162);
                                RemoveBuff(385);
                                RemoveBuff(391);
                                RemoveBuff(399);
                                RemoveBuff(163);
                                break;
                        }
                    }
                    else
                    {
                        RemoveBuff(383);
                        RemoveBuff(374);
                        RemoveBuff(381);
                        RemoveBuff(377);
                        RemoveBuff(162);
                        RemoveBuff(385);
                        RemoveBuff(391);
                        RemoveBuff(399);
                        RemoveBuff(442);
                        RemoveBuff(163);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void CloseExchangeOrTrade()
        {
            if (InExchangeOrTrade)
            {
                long? targetSessionId = ExchangeInfo?.TargetCharacterId;

                if (targetSessionId.HasValue && Session.HasCurrentMapInstance)
                {
                    ClientSession targetSession = Session.CurrentMapInstance.GetSessionByCharacterId(targetSessionId.Value);

                    if (targetSession == null)
                    {
                        return;
                    }

                    Session.SendPacket("exc_close 0");
                    targetSession.SendPacket("exc_close 0");
                    ExchangeInfo = null;
                    targetSession.Character.ExchangeInfo = null;
                }
            }
        }

        public void CloseShop()
        {
            if (HasShopOpened && Session.HasCurrentMapInstance)
            {
                KeyValuePair<long, MapShop> shop = Session.CurrentMapInstance.UserShops.FirstOrDefault(mapshop => mapshop.Value.OwnerId.Equals(CharacterId));
                if (!shop.Equals(default))
                {
                    Session.CurrentMapInstance.UserShops.Remove(shop.Key);

                    // declare that the shop cannot be closed
                    HasShopOpened = false;

                    Session.CurrentMapInstance?.Broadcast(GenerateShopEnd());
                    Session.CurrentMapInstance?.Broadcast(Session, GeneratePlayerFlag(0), ReceiverType.AllExceptMe);
                    IsSitting = false;
                    IsShopping = false; // close shop by character will always completely close the shop

                    LoadSpeed();
                    Session.SendPacket(GenerateCond());
                    Session.CurrentMapInstance?.Broadcast(GenerateRest());
                }
            }
        }

        public void Dance() => IsDancing = !IsDancing;

        public Character DeepCopy() => (Character)MemberwiseClone();

        public void DeleteBlackList(long characterId)
        {
            CharacterRelationDTO chara = CharacterRelations.Find(s => s.RelatedCharacterId == characterId);
            if (chara != null)
            {
                long id = chara.CharacterRelationId;
                DAOFactory.CharacterRelationDAO.Delete(id);
                ServerManager.Instance.RelationRefresh(id);
                Session.SendPacket(GenerateBlinit());
            }
        }

        public void DeleteItem(InventoryType type, short slot)
        {
            if (Inventory != null)
            {
                Inventory.DeleteFromSlotAndType(slot, type);
                Session.SendPacket(UserInterfaceHelper.Instance.GenerateInventoryRemove(type, slot));
            }
        }

        public void DeleteItemByItemInstanceId(Guid id)
        {
            if (Inventory != null)
            {
                Tuple<short, InventoryType> result = Inventory.DeleteById(id);
                Session.SendPacket(UserInterfaceHelper.Instance.GenerateInventoryRemove(result.Item2, result.Item1));
            }
        }

        public void DeleteRelation(long characterId, bool lettera = false)
        {
            CharacterRelationDTO chara = CharacterRelations.Find(s => s.RelatedCharacterId == characterId || s.CharacterId == characterId);
            if (chara != null)
            {

                if (CharacterRelations.Where(c => c.RelatedCharacterId == characterId && c.RelationType == CharacterRelationType.Spouse).Count() > 0)
                {
                    if (lettera == false)
                    {
                        Session.SendPacket(UserInterfaceHelper.GenerateInfo("Non puoi eliminare il tuo coniuge, devi usare la lettera del divorzio."));
                        return;
                    }
                }
                long id = chara.CharacterRelationId;
                CharacterDTO charac = DAOFactory.CharacterDAO.LoadById(characterId);
                DAOFactory.CharacterRelationDAO.Delete(id);
                ServerManager.Instance.RelationRefresh(id);

                Session.SendPacket(GenerateFinit());
                if (charac != null)
                {
                    List<CharacterRelationDTO> lst = ServerManager.Instance.CharacterRelations.Where(s => s.CharacterId == id || s.RelatedCharacterId == id).ToList();
                    string result = "finit";
                    foreach (CharacterRelationDTO relation in lst.Where(c => c.RelationType == CharacterRelationType.Friend || c.RelationType == CharacterRelationType.Spouse))
                    {
                        long id2 = relation.RelatedCharacterId == CharacterId ? relation.CharacterId : relation.RelatedCharacterId;
                        bool isOnline = CommunicationServiceClient.Instance.IsCharacterConnected(ServerManager.Instance.ServerGroup, id2);
                        result += $" {id2}|{(short)relation.RelationType}|{(isOnline ? 1 : 0)}|{DAOFactory.CharacterDAO.LoadById(id2).Name}";
                    }
                    int? sentChannelId = CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                    {
                        DestinationCharacterId = charac.CharacterId,
                        SourceCharacterId = CharacterId,
                        SourceWorldId = ServerManager.Instance.WorldId,
                        Message = result,
                        Type = MessageType.PrivateChat
                    });
                }
            }
        }

        public void DeleteTimeout()
        {
            if (Inventory == null)
            {
                return;
            }

            foreach (ItemInstance item in Inventory.GetAllItems())
            {
                if (item.IsBound && item.ItemDeleteTime != null && item.ItemDeleteTime < DateTime.Now)
                {
                    Inventory.DeleteById(item.Id);
                    EquipmentBCards.RemoveAll(o => o.ItemVNum == item.ItemVNum);
                    if (item.Type == InventoryType.Wear)
                    {
                        Session.SendPacket(GenerateEquipment());
                    }
                    else
                    {
                        Session.SendPacket(UserInterfaceHelper.Instance.GenerateInventoryRemove(item.Type, item.Slot));
                    }
                    Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("ITEM_TIMEOUT"), 10));
                }
            }
        }

        public void DisableBuffs(BuffType type, int level = 100)
        {
            lock (Buff)
            {
                List<Buff> buff = Buff.Where(s => (type & s.Card.BuffType) == s.Card.BuffType && !s.StaticBuff && s.Card.Level < level);
                buff.ForEach(s =>
                {
                    if (BuffObservables.ContainsKey(s.Card.CardId))
                    {
                        BuffObservables[s.Card.CardId].Dispose();
                        BuffObservables.Remove(s.Card.CardId);
                    }
                    RemoveBuff(s.Card.CardId);
                });
            }
        }

        /// <summary>
        /// Make the character moveable also from Teleport, ..
        /// </summary>
        public void Dispose()
        {
            CloseShop();
            CloseExchangeOrTrade();
            GroupSentRequestCharacterIds.Clear();
            FamilyInviteCharacters.Clear();
            FriendRequestCharacters.Clear();
        }

        public static string GenerateAct() => "act 6";

        public string GenerateAct6()
        {
            return $"act6 " +
                $"{(byte)Faction} 0 {(ServerManager.Instance.Act6AngelStat.Percentage / 100)} {(ServerManager.Instance.Act6AngelStat.IsBossZenas ? 1 : 0)} {(ServerManager.Instance.Act6AngelStat.CurrentTimeZenas)} {(ServerManager.Instance.Act6AngelStat.TotalTime)}" +
                   $" {(ServerManager.Instance.Act6DemonStat.Percentage / 100)} {(ServerManager.Instance.Act6DemonStat.IsBossErenia ? 1 : 0)} {(ServerManager.Instance.Act6DemonStat.CurrentTimeErenia)} {(ServerManager.Instance.Act6DemonStat.TotalTime)}";
        }


        public string GenerateAt()
        {
            MapInstance mapForMusic = MapInstance;
            return $"at {CharacterId} {MapInstance.Map.MapId} {PositionX} {PositionY} {Direction} 0 {mapForMusic?.InstanceMusic ?? 0} 2 -1";
        }

        public string GenerateBlinit()
        {
            string result = "blinit";
            foreach (CharacterRelationDTO relation in CharacterRelations.Where(s => s.CharacterId == CharacterId && s.RelationType == CharacterRelationType.Blocked))
            {
                result += $" {relation.RelatedCharacterId}|{DAOFactory.CharacterDAO.LoadById(relation.RelatedCharacterId).Name}";
            }
            return result;
        }

        public string GenerateCInfo() => $"c_info {(Authority == AuthorityType.Moderator && !Undercover ? "[Support]" + Name : Authority == AuthorityType.Owner && !Undercover ? "[Owner]" + Name : Authority == AuthorityType.Admin && !Undercover ? "[Admin]" + Name : Authority == AuthorityType.GameMaster && !Undercover ? "[GM]" + Name : Authority == AuthorityType.TrialGameMaster && !Undercover ? "[TrialGM]" + Name : Authority == AuthorityType.Moderator && !Undercover ? "[Support]" + Name : Authority == AuthorityType.TrialModerator && !Undercover ? "[TrialSupport]" + Name : Authority == AuthorityType.Donator && !Undercover ? "[Donator]" + Name : Name)} - -1 {(Family != null && !Undercover ? $"{Family.FamilyId} {Family.Name}({Language.Instance.GetMessageFromKey(FamilyCharacter.Authority.ToString().ToUpper())})" : "-1 -")} {CharacterId} {(Invisible ? 6 : Undercover ? (byte)AuthorityType.User : Authority < AuthorityType.User ? (byte)AuthorityType.User : 0)} {(byte)Gender} {(byte)HairStyle} {(byte)HairColor} {(byte)Class} {(GetDignityIco() == 1 ? GetReputationIco() : -GetDignityIco())} {(Authority == AuthorityType.Moderator ? 500 : Compliment)} {(UseSp || IsVehicled ? Morph : 0)} {(Invisible ? 1 : 0)} {Family?.FamilyLevel ?? 0} {(UseSp ? MorphUpgrade : 0)} {ArenaWinner}";

        public string GenerateCMap() => $"c_map 0 {MapInstance.Map.MapId} {(MapInstance.MapInstanceType != MapInstanceType.BaseMapInstance ? 1 : 0)}";

        public string GenerateCMode() => $"c_mode 1 {CharacterId} {(UseSp || IsVehicled ? Morph : 0)} {(UseSp ? MorphUpgrade : 0)} {(UseSp ? MorphUpgrade2 : 0)} {ArenaWinner} {Size}";

        //public static string GenerateCMode(this Character character) => "" + 
        //"c_mode " +
        //"1 " +
        //$"{character.CharacterId} " +
        //$"{(character.UseSp || character.IsVehicled ? character.Morph : 0)} " +
        //$"{(character.UseSp ? character.MorphUpgrade : 0)} " +
        //$""


        public string GenerateCond() => $"cond 1 {CharacterId} {(NoAttack ? 1 : 0)} {(NoMove ? 1 : 0)} {Speed}";


        public string GenerateDG()
        {
            byte raidType = 0;
            if (ServerManager.Instance.Act4RaidStart.AddMinutes(15) < DateTime.Now)
            {
                ServerManager.Instance.Act4RaidStart = DateTime.Now;
            }
            double seconds = (ServerManager.Instance.Act4RaidStart.AddMinutes(15) - DateTime.Now).TotalSeconds;
            switch (Family?.Act4RaidBossMap?.MapInstanceType)
            {
                case MapInstanceType.Act4Morcos:
                    raidType = 1;
                    break;

                case MapInstanceType.Act4Hatus:
                    raidType = 2;
                    break;

                case MapInstanceType.Act4Calvina:
                    raidType = 3;
                    break;

                case MapInstanceType.Act4Berios:
                    raidType = 4;
                    break;
            }
            return $"dg {raidType} {(seconds > 1800 ? 1 : 2)} {(int)seconds} 0";
        }

        public void GenerateDignity(NpcMonster monsterinfo)
        {
            if (SwitchLevel() < monsterinfo.Level && Dignity < 100 && SwitchLevel() > 20)
            {
                Dignity += (float)0.5;
                if (Dignity == (int)Dignity)
                {
                    Session.SendPacket(GenerateFd());
                    Session.CurrentMapInstance?.Broadcast(Session, GenerateIn(), ReceiverType.AllExceptMe);
                    Session.CurrentMapInstance?.Broadcast(Session, GenerateGidx(), ReceiverType.AllExceptMe);
                    Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("RESTORE_DIGNITY"), 11));
                }
            }
        }

        public string GenerateDir() => $"dir 1 {CharacterId} {Direction}";

        public string GenerateEq()
        {
            int color = (byte)HairColor;
            ItemInstance head = Inventory?.LoadBySlotAndType((byte)EquipmentType.Hat, InventoryType.Wear);

            if (head?.Item.IsColored == true)
            {
                color = head.Design;
            }
            return $"eq {CharacterId} {(Invisible ? 6 : Undercover ? (byte)AuthorityType.User : Authority < AuthorityType.User ? (byte)AuthorityType.User : 0)} {(byte)Gender} {(byte)HairStyle} {color} {(byte)Class} {GenerateEqListForPacket()} {(!InvisibleGm ? GenerateEqRareUpgradeForPacket() : null)}";
        }

        public string GenerateEqListForPacket()
        {
            string[] invarray = new string[16];
            if (Inventory != null)
            {
                for (short i = 0; i < 16; i++)
                {
                    ItemInstance item = Inventory.LoadBySlotAndType(i, InventoryType.Wear);
                    if (item != null)
                    {
                        invarray[i] = item.ItemVNum.ToString();
                    }
                    else
                    {
                        invarray[i] = "-1";
                    }
                }
            }
            return $"{invarray[(byte)EquipmentType.Hat]}.{invarray[(byte)EquipmentType.Armor]}.{invarray[(byte)EquipmentType.MainWeapon]}.{invarray[(byte)EquipmentType.SecondaryWeapon]}.{invarray[(byte)EquipmentType.Mask]}.{invarray[(byte)EquipmentType.Fairy]}.{invarray[(byte)EquipmentType.CostumeSuit]}.{invarray[(byte)EquipmentType.CostumeHat]}.{invarray[(byte)EquipmentType.WeaponSkin]}";
        }

        public string GenerateEqRareUpgradeForPacket()
        {
            sbyte weaponRare = 0;
            byte weaponUpgrade = 0;
            sbyte armorRare = 0;
            byte armorUpgrade = 0;
            if (Inventory != null)
            {
                for (short i = 0; i < 15; i++)
                {
                    ItemInstance wearable = Inventory.LoadBySlotAndType(i, InventoryType.Wear);
                    if (wearable != null)
                    {
                        switch (wearable.Item.EquipmentSlot)
                        {
                            case EquipmentType.Armor:
                                armorRare = wearable.Rare;
                                armorUpgrade = wearable.Upgrade;
                                break;

                            case EquipmentType.MainWeapon:
                                weaponRare = wearable.Rare;
                                weaponUpgrade = wearable.Upgrade;
                                break;
                        }
                    }
                }
            }
            return $"{weaponUpgrade}{weaponRare} {armorUpgrade}{armorRare}";
        }

        public string GenerateEquipment()
        {
            string eqlist = string.Empty;
            EquipmentBCards.Clear();
            if (Inventory != null)
            {
                for (short i = 0; i < 16; i++)
                {
                    ItemInstance item = Inventory.LoadBySlotAndType(i, InventoryType.Wear);
                    if (item != null)
                    {
                        if (item.Item.EquipmentSlot != EquipmentType.Sp)
                        {
                            EquipmentBCards.AddRange(item.Item.BCards);
                            switch (item.Item.ItemType)
                            {
                                case ItemType.Armor:
                                    ShellEffectArmor.Clear();

                                    foreach (ShellEffectDTO dto in item.ShellEffects)
                                    {
                                        ShellEffectArmor.Add(dto);
                                    }
                                    break;
                                case ItemType.Weapon:
                                    switch (item.Item.EquipmentSlot)
                                    {
                                        case EquipmentType.MainWeapon:
                                            ShellEffectMain.Clear();

                                            foreach (ShellEffectDTO dto in item.ShellEffects)
                                            {
                                                ShellEffectMain.Add(dto);
                                            }
                                            break;

                                        case EquipmentType.SecondaryWeapon:
                                            ShellEffectSecondary.Clear();

                                            foreach (ShellEffectDTO dto in item.ShellEffects)
                                            {
                                                ShellEffectSecondary.Add(dto);
                                            }
                                            break;
                                    }
                                    break;
                            }
                        }
                        eqlist += $" {i}.{item.Item.VNum}.{item.Rare}.{(item.Item.IsColored ? item.Design : item.Upgrade)}.0";
                    }
                }
            }
            return $"equip {GenerateEqRareUpgradeForPacket()}{eqlist}";
        }

        public string GenerateExts() => $"exts 0 {48 + ((HaveBackpack() ? 1 : 0) * 12)} {48 + ((HaveBackpack() ? 1 : 0) * 12)} {48 + ((HaveBackpack() ? 1 : 0) * 12)}";

        public string GenerateFaction() => $"fs {(byte)Faction}";

        public string GenerateFamilyMember()
        {
            string str = "gmbr 0";
            try
            {
                if (Family?.FamilyCharacters != null)
                {
                    foreach (FamilyCharacter TargetCharacter in Family?.FamilyCharacters)
                    {
                        bool isOnline = CommunicationServiceClient.Instance.IsCharacterConnected(ServerManager.Instance.ServerGroup, TargetCharacter.CharacterId);
                        str += $" {TargetCharacter.Character.CharacterId}|{Family.FamilyId}|{TargetCharacter.Character.Name}|{TargetCharacter.Character.SwitchLevel()}|{(byte)TargetCharacter.Character.Class}|{(byte)TargetCharacter.Authority}|{(byte)TargetCharacter.Rank}|{(isOnline ? 1 : 0)}|{TargetCharacter.Character.SwitchHeroLevel()}";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return str;
        }

        /*public string GenerateFamilyFmi()
        {
            string str = "fmi";
            try
            {
                if (Family?.FamilyCharacters != null)
                {
                    str += $" 0|9000|{(Family.FamilyLevel >= 6 ? 2 : 0)}|0|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9000")).Count()}";
                    str += $" 0|9002|{(Family.FamilyLevel >= 4 ? 2 : 0)}|0|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9002")).Count()}";
                    str += $" 0|9003|{(Family.FamilyLevel >= 4 ? 2 : 0)}|0|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9003")).Count()}";
                    str += $" 0|9006|{(Family.FamilyLevel >= 2 ? 2 : 0)}|{((Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9006" && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year)).Count() > 0 ? 1 : 0)}|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9006")).Count()}";
                    str += $" 0|9007|{(Family.FamilyLevel >= 1 ? 2 : 0)}|{((Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9007" && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year)).Count() > 0 ? 5 : Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.RaidWon && s.FamilyLogData == "5").Count())}|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9007")).Count()}";
                    str += $" 0|9008|{(Family.FamilyLevel >= 1 ? 2 : 0)}|{((Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9008" && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year)).Count() > 0 ? 5 : Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.RaidWon && s.FamilyLogData == "0").Count())}|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9008")).Count()}";
                    str += $" 0|9009|{(Family.FamilyLevel >= 1 ? 2 : 0)}|{((Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9009" && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year)).Count() > 0 ? 5 : Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.RaidWon && s.FamilyLogData == "1").Count())}|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9009")).Count()}";
                    str += $" 0|
                    0|{(Family.FamilyLevel >= 1 ? 2 : 0)}|{((Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9010" && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year)).Count() > 0 ? 5 : Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.RaidWon && s.FamilyLogData == "2").Count())}|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9010")).Count()}";
                    str += $" 0|9011|{(Family.FamilyLevel >= 1 ? 2 : 0)}|{((Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9011" && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year)).Count() > 0 ? 5 : Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.RaidWon && s.FamilyLogData == "3").Count())}|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9011")).Count()}";
                    str += $" 0|9012|{(Family.FamilyLevel >= 2 ? 2 : 0)}|{((Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9012" && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year)).Count() > 0 ? 5 : Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.RaidWon && s.FamilyLogData == "4").Count())}|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9012")).Count()}";
                    str += $" 0|9013|{(Family.FamilyLevel >= 2 ? 2 : 0)}|{((Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9013" && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year)).Count() > 0 ? 5 : Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.RaidWon && s.FamilyLogData == "9").Count())}|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9013")).Count()}";
                    str += $" 0|9014|{(Family.FamilyLevel >= 3 ? 2 : 0)}|{((Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9014" && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year)).Count() > 0 ? 5 : Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.RaidWon && s.FamilyLogData == "13").Count())}|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9014")).Count()}";
                    str += $" 0|9015|{(Family.FamilyLevel >= 3 ? 2 : 0)}|{((Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9015" && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year)).Count() > 0 ? 5 : Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.RaidWon && s.FamilyLogData == "14").Count())}|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9015")).Count()}";
                    str += $" 0|9016|{(Family.FamilyLevel >= 3 ? 2 : 0)}|{((Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9016" && s.Timestamp.Month == DateTime.Now.Month && s.Timestamp.Year == DateTime.Now.Year)).Count() > 0 ? 5 : Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.RaidWon && s.FamilyLogData == "15").Count())}|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9016")).Count()}";
                    str += $" 0|9017|{(Family.FamilyLevel >= 1 ? 2 : 0)}|0|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9017")).Count()}";
                    str += $" 0|9075|{(Family.FamilyLevel >= 5 ? 2 : 0)}|0|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9075")).Count()}";
                    str += $" 0|9076|{(Family.FamilyLevel >= 5 ? 2 : 0)}|0|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9076")).Count()}";
                    str += $" 0|9077|{(Family.FamilyLevel >= 3 ? 2 : 0)}|0|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9077")).Count()}";
                    str += $" 0|9078|{(Family.FamilyLevel >= 2 ? 2 : 0)}|0|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9078")).Count()}";
                    str += $" 0|9079|{(Family.FamilyLevel >= 3 ? 2 : 0)}|0|{(Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.SfideGiornaliere && s.FamilyLogData == "9079")).Count()}";
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return str;
        }*/

        public string GenerateFamilyMemberExp()
        {
            string str = "gexp";
            try
            {
                if (Family?.FamilyCharacters != null)
                {
                    foreach (FamilyCharacter TargetCharacter in Family?.FamilyCharacters)
                    {
                        str += $" {TargetCharacter.CharacterId}|{TargetCharacter.Experience}";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return str;
        }

        public string GenerateFamilyMemberMessage()
        {
            string str = "gmsg";
            try
            {
                if (Family?.FamilyCharacters != null)
                {
                    foreach (FamilyCharacter TargetCharacter in Family?.FamilyCharacters)
                    {
                        str += $" {TargetCharacter.CharacterId}|{TargetCharacter.DailyMessage}";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return str;
        }

        public List<string> GenerateFamilyWarehouseHist()
        {
            if (Family != null)
            {
                List<string> packetList = new List<string>();
                string packet = string.Empty;
                int i = 0;
                int amount = -1;
                foreach (FamilyLogDTO log in Family.FamilyLogs.Where(s => s.FamilyLogType == FamilyLogType.WareHouseAdded || s.FamilyLogType == FamilyLogType.WareHouseRemoved).OrderByDescending(s => s.Timestamp).Take(100))
                {
                    packet += $" {(log.FamilyLogType == FamilyLogType.WareHouseAdded ? 0 : 1)}|{log.FamilyLogData}|{(int)(DateTime.Now - log.Timestamp).TotalHours}";
                    i++;
                    if (i == 50)
                    {
                        i = 0;
                        packetList.Add($"fslog_stc {amount}{packet}");
                        amount++;
                    }
                    else if (i == Family.FamilyLogs.Count)
                    {
                        packetList.Add($"fslog_stc {amount}{packet}");
                    }
                }

                return packetList;
            }
            return new List<string>();
        }

        public string GenerateTaSt(TalentArenaOptionType watch) => $"ta_st {(byte)watch}";

        public void GenerateFamilyXp(int FXP)
        {
            if (!Session.Account.PenaltyLogs.Any(s => s.Penalty == PenaltyType.BlockFExp && s.DateEnd > DateTime.Now) && Family != null && FamilyCharacter != null)
            {
                FamilyCharacterDTO famchar = FamilyCharacter;
                FamilyDTO fam = Family;
                fam.FamilyExperience += FXP;
                famchar.Experience += FXP;
                if (CharacterHelper.LoadFamilyXPData(Family.FamilyLevel) <= fam.FamilyExperience)
                {
                    fam.FamilyExperience -= CharacterHelper.LoadFamilyXPData(Family.FamilyLevel);
                    fam.FamilyLevel++;
                    Family.InsertFamilyLog(FamilyLogType.FamilyLevelUp, level: fam.FamilyLevel);
                    CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                    {
                        DestinationCharacterId = Family.FamilyId,
                        SourceCharacterId = CharacterId,
                        SourceWorldId = ServerManager.Instance.WorldId,
                        Message = UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("FAMILY_UP"), 0),
                        Type = MessageType.Family
                    });
                }
                DAOFactory.FamilyCharacterDAO.InsertOrUpdate(ref famchar);
                DAOFactory.FamilyDAO.InsertOrUpdate(ref fam);
                ServerManager.Instance.FamilyRefresh(Family.FamilyId);
                CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                {
                    DestinationCharacterId = Family.FamilyId,
                    SourceCharacterId = CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message = "fhis_stc",
                    Type = MessageType.Family
                });
            }
        }

        public string GenerateFc()
        {
            return $"fc {(byte)Faction} {ServerManager.Instance.Act4AngelStat.MinutesUntilReset} {ServerManager.Instance.Act4AngelStat.Percentage / 100} {ServerManager.Instance.Act4AngelStat.Mode}" +
                $" {ServerManager.Instance.Act4AngelStat.CurrentTime} {ServerManager.Instance.Act4AngelStat.TotalTime} {Convert.ToByte(ServerManager.Instance.Act4AngelStat.IsMorcos)}" +
                $" {Convert.ToByte(ServerManager.Instance.Act4AngelStat.IsHatus)} {Convert.ToByte(ServerManager.Instance.Act4AngelStat.IsCalvina)} {Convert.ToByte(ServerManager.Instance.Act4AngelStat.IsBerios)}" +
                $" 0 {ServerManager.Instance.Act4DemonStat.Percentage / 100} {ServerManager.Instance.Act4DemonStat.Mode} {ServerManager.Instance.Act4DemonStat.CurrentTime} {ServerManager.Instance.Act4DemonStat.TotalTime}" +
                $" {Convert.ToByte(ServerManager.Instance.Act4DemonStat.IsMorcos)} {Convert.ToByte(ServerManager.Instance.Act4DemonStat.IsHatus)} {Convert.ToByte(ServerManager.Instance.Act4DemonStat.IsCalvina)} " +
                $"{Convert.ToByte(ServerManager.Instance.Act4DemonStat.IsBerios)} 0";

            //return $"fc {Faction} 0 69 0 0 0 1 1 1 1 0 34 0 0 0 1 1 1 1 0";
        }

        public string GenerateFd() => $"fd {Reputation} {GetReputationIco()} {(int)Dignity} {Math.Abs(GetDignityIco())}";

        public string GenerateFinfo(long? relatedCharacterLoggedId, bool isConnected)
        {
            string result = "finfo";
            foreach (CharacterRelationDTO relation in CharacterRelations.Where(c => c.RelationType == CharacterRelationType.Friend || c.RelationType == CharacterRelationType.Spouse))
            {
                if (relatedCharacterLoggedId.HasValue && (relatedCharacterLoggedId.Value == relation.RelatedCharacterId))
                {
                    result += $" {relation.RelatedCharacterId}.{(isConnected ? 1 : 0)}";
                }
                if (relatedCharacterLoggedId.HasValue && (relatedCharacterLoggedId.Value == relation.CharacterId))
                {
                    result += $" {relation.CharacterId}.{(isConnected ? 1 : 0)}";
                }
            }
            return result;
        }

        public string GenerateFinit()
        {
            string result = "finit";
            foreach (CharacterRelationDTO relation in CharacterRelations.Where(c => c.RelationType == CharacterRelationType.Friend || c.RelationType == CharacterRelationType.Spouse || c.RelationType == CharacterRelationType.HiddenSpouse))
            {
                long id = relation.RelatedCharacterId == CharacterId ? relation.CharacterId : relation.RelatedCharacterId;
                if (DAOFactory.CharacterDAO.LoadById(id) is CharacterDTO character)
                {
                    bool isOnline = CommunicationServiceClient.Instance.IsCharacterConnected(ServerManager.Instance.ServerGroup, id);
                    result += $" {id}|{(short)relation.RelationType}|{(isOnline ? 1 : 0)}|{character.Name}";
                }
            }
            return result;
        }

        public string GenerateFStashAll()
        {
            string stash = $"f_stash_all {Family.WarehouseSize}";
            foreach (ItemInstance item in Family.Warehouse.GetAllItems())
            {
                stash += $" {item.GenerateStashPacket()}";
            }
            return stash;
        }

        public string GenerateGender() => $"p_sex {(byte)Gender}";

        public string GenerateGet(long id) => $"get 1 {CharacterId} {id} 0";

        public string GenerateGExp()
        {
            string str = "gexp";
            foreach (FamilyCharacter familyCharacter in Family.FamilyCharacters)
            {
                str += $" {familyCharacter.CharacterId}|{familyCharacter.Experience}";
            }
            return str;
        }

        public string GenerateGidx()
        {
            if (Family != null)
            {
                int rank = 0;
                if (ServerManager.Instance.TopGuild == Family)
                {
                    rank = 1;
                }
                int anno = 0;
                if (DateTime.Now >= Family.Creation.AddYears(1))
                {
                    anno = 1;
                }
                return $"gidx 1 {CharacterId} {Family.FamilyId} {Family.Name}({Language.Instance.GetMessageFromKey(Family.FamilyCharacters.Find(s => s.CharacterId == CharacterId)?.Authority.ToString().ToUpper())}) {Family.FamilyLevel} {Ach1}|{Ach2}|{Ach3} {(Family.FamilyLevel >= 4 ? (short)Family.Faction : 0)}";
            }
            return $"gidx 1 {CharacterId} -1 - 0";
        }

        public string GenerateGInfo()
        {
            if (Family != null)
            {
                try
                {
                    FamilyCharacter familyCharacter = Family.FamilyCharacters.Find(s => s.Authority == FamilyAuthority.Head);
                    if (familyCharacter != null)
                    {
                        return $"ginfo {Family.Name} {familyCharacter.Character.Name} {(byte)Family.FamilyHeadGender} {Family.FamilyLevel} {Family.FamilyExperience} {CharacterHelper.LoadFamilyXPData(Family.FamilyLevel)} {Family.FamilyCharacters.Count} {Family.MaxSize} {(byte)FamilyCharacter.Authority} {(Family.ManagerCanInvite ? 1 : 0)} {(Family.ManagerCanNotice ? 1 : 0)} {(Family.ManagerCanShout ? 1 : 0)} {(Family.ManagerCanGetHistory ? 1 : 0)} {(byte)Family.ManagerAuthorityType} {(Family.MemberCanGetHistory ? 1 : 0)} {(byte)Family.MemberAuthorityType} {Family.FamilyMessage.Replace(' ', '^')}";
                    }
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
            return string.Empty;
        }

        public string GenerateGold() => $"gold {Gold} 0";

        public string GenerateIcon(int type, int value, short itemVNum) => $"icon {type} {CharacterId} {value} {itemVNum}";

        public string GenerateIdentity() => $"Character: {Name}";

        public string GenerateIn(bool foe = false)
        {
            // string chars = "!§$%&/()=?*+~#";
            string _name = Name;
            if (foe)
            {
                _name = "!§$%&/()=?*+~#";
            }
            int _faction = 0;
            if (ServerManager.Instance.ChannelId == 51)
            {
                _faction = (byte)Faction + 2;
            }
            int color = HairStyle == HairStyleType.Hair8 ? 0 : (byte)HairColor;
            ItemInstance fairy = null;
            if (Inventory != null)
            {
                ItemInstance headWearable = Inventory.LoadBySlotAndType((byte)EquipmentType.Hat, InventoryType.Wear);
                if (headWearable?.Item.IsColored == true)
                {
                    color = headWearable.Design;
                }
                fairy = Inventory.LoadBySlotAndType((byte)EquipmentType.Fairy, InventoryType.Wear);
            }
            return $"in 1 {(Authority == AuthorityType.Moderator && !Undercover ? "[Support]" + _name : Authority == AuthorityType.Owner && !Undercover ? "[Owner]" + _name : Authority == AuthorityType.Admin && !Undercover ? "[Admin]" + _name : Authority == AuthorityType.GameMaster && !Undercover ? "[GM]" + _name : Authority == AuthorityType.TrialGameMaster && !Undercover ? "[TrialGM]" + _name : Authority == AuthorityType.Moderator && !Undercover ? "[Support]" + _name : Authority == AuthorityType.TrialModerator && !Undercover ? "[TrialSupport]" + _name : Authority == AuthorityType.Donator && !Undercover ? "[Donator]" + _name : Authority == AuthorityType.BitchNiggerFaggot ? _name + "[BitchNiggerFaggot]" : _name)} - {CharacterId} {PositionX} {PositionY} {Direction} {(Undercover ? (byte)AuthorityType.User : Authority < AuthorityType.User ? (byte)AuthorityType.User : 0)} {(byte)Gender} {(byte)HairStyle} {color} {(byte)Class} {GenerateEqListForPacket()} {Math.Ceiling(Hp / HPLoad() * 100)} {Math.Ceiling(Mp / MPLoad() * 100)} {(IsSitting ? 1 : 0)} {(Group?.GroupType == GroupType.Group ? (Group?.GroupId ?? -1) : -1)} {(fairy != null && !Undercover ? 4 : 0)} {fairy?.Item.Element ?? 0} 0 {fairy?.Item.Morph ?? 0} 0 {(UseSp || IsVehicled ? Morph : 0)} {GenerateEqRareUpgradeForPacket()} {(!Undercover ? (foe ? -1 : Family?.FamilyId ?? -1) : -1)} {(!Undercover ? (foe ? _name : Family?.Name ?? "-") : "-")} {(GetDignityIco() == 1 ? GetReputationIco() : -GetDignityIco())} {(Invisible ? 1 : 0)} {(UseSp ? MorphUpgrade : 0)} {_faction} {(UseSp ? MorphUpgrade2 : 0)} {Level} {Family?.FamilyLevel ?? 0} {Ach1}|{Ach2}|{Ach3} {ArenaWinner} /*{(Authority == AuthorityType.Donator && !Undercover ? 100 : Authority >= AuthorityType.TrialModerator && Authority <= AuthorityType.TrialGameMaster && !Undercover ? 50 : Authority >= AuthorityType.GameMaster && Authority <= AuthorityType.Owner && !Undercover ? 100 : Compliment)}*/{Size} {Size} {HeroLevel}";

        }

        public string GenerateInvisible() => $"cl {CharacterId} {(Invisible ? 1 : 0)} {(InvisibleGm ? 1 : 0)}";

        public void GenerateTacchetta(MapMonster monsterToAttack)
        {
            lock (_syncObj)
            {
                if (monsterToAttack == null || !monsterToAttack.IsAlive)
                {
                    return;
                }

                monsterToAttack.RunTacchettaEvent();
            }
        }

        public void GenerateTacchettaDivisoDue(MapMonster monsterToAttack)
        {
            lock (_syncObj)
            {
                if (monsterToAttack == null || !monsterToAttack.IsAlive)
                {
                    return;
                }

                monsterToAttack.RunTacchettaDivisoDueEvent();
            }
        }

        public void GenerateKillBonus(MapMonster monsterToAttack, ItemInstance weapon)
        {
            void _handleGoldDrop(DropDTO drop, long maxGold, long? dropOwner, short posX, short posY)
            {
                Observable.Timer(TimeSpan.FromMilliseconds(500)).Subscribe(o =>
                {
                    if (Session.HasCurrentMapInstance)
                    {
                        if (CharacterId == dropOwner && StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.AutoLoot))
                        {
                            double multiplier = 1 + (Session.Character.GetBuff(CardType.Item, (byte)AdditionalTypes.Item.IncreaseEarnedGold)[0] / 100D);
                            multiplier += (Session.Character.ShellEffectMain.FirstOrDefault(s => s.Effect == (byte)ShellWeaponEffectType.GainMoreGold)?.Value ?? 0) / 100D;
                            Gold += (int)(drop.Amount * multiplier);
                            if (Gold > maxGold)
                            {
                                Gold = maxGold;
                                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("MAX_GOLD"), 0));
                            }
                            Session.SendPacket(GenerateSay($"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: {ServerManager.GetItem(drop.ItemVNum).Name} x {drop.Amount}{(multiplier > 1 ? $" + {(int)(drop.Amount * multiplier) - drop.Amount}" : string.Empty)}", 12));
                            Session.SendPacket(GenerateGold());
                        }
                        else
                        {
                            double multiplier = 1 + (Session.Character.GetBuff(CardType.Item, (byte)AdditionalTypes.Item.IncreaseEarnedGold)[0] / 100D);
                            multiplier += (Session.Character.ShellEffectMain.FirstOrDefault(s => s.Effect == (byte)ShellWeaponEffectType.GainMoreGold)?.Value ?? 0) / 100D;
                            Gold += (int)(drop.Amount * multiplier);
                            if (Gold > maxGold)
                            {
                                Gold = maxGold;
                                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("MAX_GOLD"), 0));
                            }
                            Session.SendPacket(GenerateSay($"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: {ServerManager.GetItem(drop.ItemVNum).Name} x {drop.Amount}{(multiplier > 1 ? $" + {(int)(drop.Amount * multiplier) - drop.Amount}" : string.Empty)}", 12));
                            Session.SendPacket(GenerateGold());
                            //Session.CurrentMapInstance.DropItemByMonster(dropOwner, drop, monsterToAttack.MapX, monsterToAttack.MapY);
                        }
                    }
                });
            }

            void _handleItemDrop(DropDTO drop, long? owner, short posX, short posY)
            {
                Observable.Timer(TimeSpan.FromMilliseconds(500)).Subscribe(o =>
                {
                    if (Session.HasCurrentMapInstance)
                    {
                        if (CharacterId == owner && StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.AutoLoot))
                        {
                            GiftAdd(drop.ItemVNum, (byte)drop.Amount);
                        }
                        else
                        {
                            GiftAdd(drop.ItemVNum, (byte)drop.Amount);
                            //Session.CurrentMapInstance.DropItemByMonster(owner, drop, posX, posY);
                        }
                    }
                });
            }

            lock (_syncObj)
            {
                if (monsterToAttack == null || monsterToAttack.IsAlive)
                {
                    return;
                }
                monsterToAttack.RunDeathEvent();

                Random random = new Random(DateTime.Now.Millisecond & monsterToAttack.MapMonsterId);


                // owner set
                long? dropOwner = monsterToAttack.DamageList.Count > 0 ? monsterToAttack.DamageList.First().Key : (long?)null;
                Group group = null;
                if (dropOwner != null)
                {
                    group = ServerManager.Instance.Groups.Find(g => g.IsMemberOfGroup((long)dropOwner));
                }

                IncrementQuests(QuestType.Hunt, monsterToAttack.MonsterVNum);
                IncrementQuests(QuestType.FlowerQuest, monsterToAttack.Monster.Level);


                if (ServerManager.Instance.ChannelId == 51 && ServerManager.Instance.Act4DemonStat.Mode == 0 && ServerManager.Instance.Act4AngelStat.Mode == 0 && !CaligorRaid.IsRunning)
                {
                    if (Faction == FactionType.Angel)
                    {
                        ServerManager.Instance.Act4AngelStat.Percentage += monsterToAttack.Monster.PercentualeCorona;
                    }
                    else if (Faction == FactionType.Demon)
                    {
                        ServerManager.Instance.Act4DemonStat.Percentage += monsterToAttack.Monster.PercentualeCorona;
                    }
                }

                // end owner set
                if (Session.HasCurrentMapInstance && (Group == null || Group.GroupType == GroupType.Group))
                {
                    List<DropDTO> droplist = monsterToAttack.Monster.Drops.Where(s => Session.CurrentMapInstance.Map.MapTypes.Any(m => m.MapTypeId == s.MapTypeId) || s.MapTypeId == null).ToList();
                    if (monsterToAttack.Monster.MonsterType != MonsterType.Special)
                    {
                        #region item drop

                        int levelDifference = Session.Character.Level - monsterToAttack.Monster.Level;
                        int dropRate = ServerManager.Instance.Configuration.RateDrop * MapInstance.DropRate;
                        int x = 0;
                        foreach (DropDTO drop in droplist.OrderBy(s => random.Next()))
                        {
                            if (x < 4)
                            {
                                double rndamount = ServerManager.RandomNumber() * random.NextDouble();
                                double divider = levelDifference >= 10 ? (levelDifference - 9) * 1.2D : levelDifference <= -10 ? (levelDifference + 9) * 1.2D : 1D;
                                if (rndamount <= (double)drop.DropChance * dropRate / 5000.000 / divider)
                                {
                                    x++;
                                    if (Session.CurrentMapInstance != null)
                                    {
                                        if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4) || monsterToAttack.Monster.MonsterType == MonsterType.Elite)
                                        {
                                            List<long> alreadyGifted = new List<long>();
                                            foreach (long charId in monsterToAttack.DamageList.Keys)
                                            {
                                                if (!alreadyGifted.Contains(charId))
                                                {
                                                    ClientSession giftsession = ServerManager.Instance.GetSessionByCharacterId(charId);
                                                    giftsession?.Character.GiftAdd(drop.ItemVNum, (byte)drop.Amount);
                                                    alreadyGifted.Add(charId);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (group?.GroupType == GroupType.Group)
                                            {
                                                if (group.SharingMode == (byte)GroupSharingType.ByOrder)
                                                {
                                                    dropOwner = group.GetNextOrderedCharacterId(this);
                                                    if (dropOwner.HasValue)
                                                    {
                                                        group.Characters.ForEach(s => s.SendPacket(s.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("ITEM_BOUND_TO"), ServerManager.GetItem(drop.ItemVNum).Name, group.Characters.Single(c => c.Character.CharacterId == (long)dropOwner).Character.Name, drop.Amount), 10)));
                                                    }
                                                }
                                                else
                                                {
                                                    group.Characters.ForEach(s => s.SendPacket(s.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("DROPPED_ITEM"), ServerManager.GetItem(drop.ItemVNum).Name, drop.Amount), 10)));
                                                }
                                            }

                                            _handleItemDrop(drop, dropOwner, monsterToAttack.MapX, monsterToAttack.MapY);
                                        }
                                    }
                                }
                            }
                        }

                        #endregion

                        #region Quest

                        Quests.Where(q => (q.Quest?.QuestType == (int)QuestType.Collect4 || q.Quest?.QuestType == (int)QuestType.Collect2 || q.Quest?.QuestType == (int)QuestType.Collect1)).ToList().ForEach(qst =>
                        {
                            qst.Quest.QuestObjectives.ForEach(d =>
                            {
                                if (d.SpecialData == monsterToAttack.MonsterVNum)
                                {
                                    DropDTO dropquest = new DropDTO()
                                    {
                                        ItemVNum = (short)d.Data,
                                        Amount = 1,
                                        MonsterVNum = monsterToAttack.MonsterVNum,
                                        DropChance = (int)((d.DropRate ?? 100) * 10) // Approx
                                    };
                                    Session.CurrentMapInstance.DropItemByMonster(dropOwner, dropquest, monsterToAttack.MapX, monsterToAttack.MapY, quest: 1);
                                }
                                else if (d.SpecialData == null)
                                {
                                    if (monsterToAttack.Monster.Level - SwitchLevel() <= 9 && monsterToAttack.Monster.Level - SwitchLevel() >= -9)
                                    {

                                        DropDTO dropquest = new DropDTO()
                                        {
                                            ItemVNum = (short)d.Data,
                                            Amount = 1,
                                            MonsterVNum = monsterToAttack.MonsterVNum,
                                            DropChance = (int)((d.DropRate ?? 100) * 10) // Approx
                                        };
                                        Session.CurrentMapInstance.DropItemByMonster(dropOwner, dropquest, monsterToAttack.MapX, monsterToAttack.MapY, quest: 1);
                                    }
                                }
                            });
                        });

                        #endregion

                        #region gold drop

                        // gold calculation
                        int gold = GetGold(monsterToAttack);
                        long maxGold = ServerManager.Instance.Configuration.MaxGold;
                        gold = gold > maxGold ? (int)maxGold : gold;
                        if (gold > 0)
                        {
                            DropDTO drop2 = new DropDTO
                            {
                                Amount = gold,
                                ItemVNum = 1046
                            };
                            if (Session.CurrentMapInstance != null)
                            {
                                List<long> alreadyGifted = new List<long>();
                                foreach (long charId in monsterToAttack.DamageList.Keys)
                                {
                                    if (!alreadyGifted.Contains(charId))
                                    {
                                        ClientSession session = ServerManager.Instance.GetSessionByCharacterId(charId);
                                        if (session != null)
                                        {
                                            double multiplier = 1 + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.IncreaseEarnedGold)[0] / 100D);
                                            multiplier += (ShellEffectMain.FirstOrDefault(s => s.Effect == (byte)ShellWeaponEffectType.GainMoreGold)?.Value ?? 0) / 100D;
                                            StaticBonusDTO goldDouble = Session.Character.StaticBonusList.Find(s => s.StaticBonusType == StaticBonusType.DoubleGold);
                                            if (goldDouble != null)
                                            {
                                                multiplier *= 2;
                                            }
                                            session.Character.Gold += (drop2.Amount + (multiplier > 1 ? +(int)(drop2.Amount * multiplier) - drop2.Amount : 0));
                                            if (session.Character.Gold > maxGold)
                                            {
                                                session.Character.Gold = maxGold;
                                                session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("MAX_GOLD"), 0));
                                            }
                                            session.SendPacket(session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: {ServerManager.GetItem(drop2.ItemVNum).Name} x {drop2.Amount}{(multiplier > 1 ? $" + {(int)(drop2.Amount * multiplier) - drop2.Amount}" : string.Empty)}", 10));
                                            session.SendPacket(session.Character.GenerateGold());
                                        }
                                        alreadyGifted.Add(charId);
                                    }
                                }
                            }
                        }
                        /*double randChance = ServerManager.RandomNumber() * random.NextDouble();

                        if (gold > 0 && randChance <= (int)(ServerManager.Instance.Configuration.RateGoldDrop * 10 * CharacterHelper.GoldPenalty(SwitchLevel(), monsterToAttack.Monster.Level)))
                        {
                            DropDTO drop2 = new DropDTO
                            {
                                Amount = gold,
                                ItemVNum = 1046
                            };
                            if (Session.CurrentMapInstance != null)
                            {
                                if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4) || monsterToAttack.Monster.MonsterType == MonsterType.Elite)
                                {
                                    List<long> alreadyGifted = new List<long>();
                                    foreach (long charId in monsterToAttack.DamageList.Keys)
                                    {
                                        if (!alreadyGifted.Contains(charId))
                                        {
                                            ClientSession session = ServerManager.Instance.GetSessionByCharacterId(charId);
                                            if (session != null)
                                            {
                                                double multiplier = 1 + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.IncreaseEarnedGold)[0] / 100D);
                                                multiplier += (ShellEffectMain.FirstOrDefault(s => s.Effect == (byte)ShellWeaponEffectType.GainMoreGold)?.Value ?? 0) / 100D;
                                                session.Character.Gold += (drop2.Amount + (multiplier > 1 ? +(int)(drop2.Amount * multiplier) - drop2.Amount : 0));
                                                if (session.Character.Gold > maxGold)
                                                {
                                                    session.Character.Gold = maxGold;
                                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("MAX_GOLD"), 0));
                                                }
                                                session.SendPacket(session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: {ServerManager.GetItem(drop2.ItemVNum).Name} x {drop2.Amount}{(multiplier > 1 ? $" + {(int)(drop2.Amount * multiplier) - drop2.Amount}" : string.Empty)}", 10));
                                                session.SendPacket(session.Character.GenerateGold());
                                            }
                                            alreadyGifted.Add(charId);
                                        }
                                    }
                                }
                                else
                                {
                                    if (group != null && MapInstance.MapInstanceType != MapInstanceType.LodInstance)
                                    {
                                        if (group.SharingMode == (byte)GroupSharingType.ByOrder)
                                        {
                                            dropOwner = group.GetNextOrderedCharacterId(this);

                                            if (dropOwner.HasValue)
                                            {
                                                group.Characters.ForEach(s => s.SendPacket(s.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("ITEM_BOUND_TO"), ServerManager.GetItem(drop2.ItemVNum).Name, group.Characters.Single(c => c.Character.CharacterId == (long)dropOwner).Character.Name, drop2.Amount), 10)));
                                            }
                                        }
                                        else
                                        {
                                            group.Characters.ForEach(s => s.SendPacket(s.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("DROPPED_ITEM"), ServerManager.GetItem(drop2.ItemVNum).Name, drop2.Amount), 10)));
                                        }
                                    }

                                    _handleGoldDrop(drop2, maxGold, dropOwner, monsterToAttack.MapX, monsterToAttack.MapY);
                                }
                            }
                        }
                        */
                        #endregion


                        if (Session.HasCurrentMapInstance && (Group == null || Group.GroupType == GroupType.Group))
                        {
                            #region exp

                            if (Hp > 0)
                            {
                                Group grp = ServerManager.Instance.Groups.Find(g => g.IsMemberOfGroup(CharacterId));
                                if (grp != null)
                                {
                                    foreach (ClientSession targetSession in grp.Characters.Where(g => g.Character.MapInstanceId == MapInstanceId))
                                    {
                                        if (grp.IsMemberOfGroup(monsterToAttack.DamageList.FirstOrDefault().Key))
                                        {
                                            targetSession.Character.GenerateXp(monsterToAttack, true);
                                        }
                                        else
                                        {
                                            targetSession.SendPacket(targetSession.Character.GenerateSay(Language.Instance.GetMessageFromKey("XP_NOTFIRSTHIT"), 10));
                                            targetSession.Character.GenerateXp(monsterToAttack, false);
                                        }
                                        targetSession.Character.SetReputation((short)(monsterToAttack.Monster.Reput));
                                        //if (targetSession.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4))
                                        //{
                                        //    targetSession.Character.SetContributi(monsterToAttack.Monster.Contributi);
                                        //}
                                    }
                                }
                                else
                                {
                                    if (monsterToAttack.DamageList.FirstOrDefault().Key == CharacterId)
                                    {
                                        //GenerateWeaponXp(monsterToAttack, weapon, true);
                                        GenerateXp(monsterToAttack, true);
                                    }
                                    else
                                    {
                                        Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("XP_NOTFIRSTHIT"), 10));
                                        //GenerateWeaponXp(monsterToAttack, weapon, false);
                                        GenerateXp(monsterToAttack, false);
                                    }
                                    SetReputation((short)monsterToAttack.Monster.Reput);
                                    //if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4))
                                    //{
                                    //    Session.Character.SetContributi(monsterToAttack.Monster.Contributi);
                                    //}
                                }
                                GenerateDignity(monsterToAttack.Monster);
                            }

                            #endregion
                        }
                    }
                }
            }

            #region drop
            int rateDrop = ServerManager.Instance.Configuration.RateDrop;
            {

                if (monsterToAttack.MonsterVNum == 2685) // piorka
                {
                    int rnd5303 = ServerManager.RandomNumber(0, 1000);

                    if (rnd5303 < 50 * rateDrop)
                    {
                        Session.Character.GiftAdd(2282, 4);
                    }
                    else
                    {
                        return;
                    }
                }
                if (monsterToAttack.MonsterVNum == 290) // tarot
                {
                    int rnd5303 = ServerManager.RandomNumber(0, 10000);

                    if (rnd5303 < 3 * rateDrop)
                    {
                        Session.Character.GiftAdd(1904, 1);
                    }
                    else
                    {
                        return;
                    }
                }
                if (monsterToAttack.MonsterVNum == 147) // fm
                {
                    int rnd5304 = ServerManager.RandomNumber(0, 1000);

                    if (rnd5304 < 50 * rateDrop)
                    {
                        Session.Character.GiftAdd(1030, 2);
                    }
                    else
                    {
                        return;
                    }
                }
                if (monsterToAttack.MonsterVNum == 667) // souls
                {
                    int rnd5305 = ServerManager.RandomNumber(0, 1000);

                    if (rnd5305 < 30 * rateDrop)
                    {
                        Session.Character.GiftAdd(2283, 1);
                    }
                    else if (rnd5305 < 40 * rateDrop)
                    {
                        Session.Character.GiftAdd(2284, 1);

                    }
                    else if (rnd5305 < 50 * rateDrop)
                    {
                        Session.Character.GiftAdd(2283, 1);

                    }
                    else
                    {
                        return;
                    }

                }
            }
            #endregion
            #region kwiatek
            int kwiatek = ServerManager.RandomNumber(0, 1000);
            if (kwiatek == 0)
            {
                DropDTO _dropquest = new DropDTO() // to test
                {
                    ItemVNum = 1087,
                    Amount = 1,
                    DropChance = 50
                };

                long? dropOwner = monsterToAttack.DamageList.Count > 0 ? monsterToAttack.DamageList.First().Key : (long?)null;
                if (dropOwner != null)
                {
                    Session.CurrentMapInstance.DropItemByMonster(dropOwner, _dropquest, monsterToAttack.MapX, monsterToAttack.MapY, quest: 0);
                }
            }
            #endregion
            //#region clearmap
            //int clearmaprnd = ServerManager.RandomNumber(0, 5000);
            //if (clearmaprnd == 0)
            //{
            //    Parallel.ForEach(Session.CurrentMapInstance.Monsters.Where(s => s.ShouldRespawn != true), monster =>
            //    {
            //        Session.CurrentMapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster,
            //            monster.MapMonsterId));
            //        Session.CurrentMapInstance.RemoveMonster(monster);
            //    });
            //    Parallel.ForEach(Session.CurrentMapInstance.DroppedList.GetAllItems(), drop =>
            //    {
            //        Session.CurrentMapInstance.Broadcast(StaticPacketHelper.Out(UserType.Object, drop.TransportId));
            //        Session.CurrentMapInstance.DroppedList.Remove(drop.TransportId);
            //    });
            //    Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("map_cleared"), 10));
            //}
            //#endregion
        }

        public string GenerateLev()
        {
            ItemInstance specialist = null;
            if (Inventory != null)
            {
                specialist = Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
            }
            return $"lev {SwitchLevel()} {(int)(SwitchLevel() < 100 ? SwitchLevelXp() : SwitchLevelXp() / 100)} {(!UseSp || specialist == null ? JobLevel : specialist.SpLevel)} {(!UseSp || specialist == null ? JobLevelXp : specialist.XP)} {(int)(SwitchLevel() < 100 ? XpLoad() : XpLoad() / 100)} {(!UseSp || specialist == null ? JobXPLoad() : SpXpLoad())} {Reputation} {GetCP()} {(int)(SwitchHeroLevel() < 100 ? SwitchHeroLevelXp() : SwitchHeroLevelXp() / 100)} {SwitchHeroLevel()} {(int)(SwitchHeroLevel() < 100 ? HeroXPLoad() : HeroXPLoad() / 100)} 0";
        }

        public string GenerateLevelUp()
        {
            #region pasywki
            if (Session.Character.Level == 15)
            {
                Session.Character.HeroLevel = 1;
            }
            else if (Session.Character.HeroLevel == 65)
            {
                short skillVNum = 22;
                Skill skillinfo = ServerManager.GetSkill(skillVNum);
                if (skillinfo == null)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SKILL_DOES_NOT_EXIST"), 11));
                }

                if (skillinfo.SkillVNum < 200)
                {
                    foreach (CharacterSkill skill in Session.Character.Skills.GetAllItems())
                    {
                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                        {
                            Session.Character.Skills.Remove(skill.SkillVNum);
                        }
                    }
                }

                Session.Character.Skills[skillVNum] = new CharacterSkill
                {
                    SkillVNum = skillVNum,
                    CharacterId = Session.Character.CharacterId
                };
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(Session.Character.GenerateQuicklist());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"),
                    0));
            }
            else if (Session.Character.HeroLevel == 75)
            {
                short skillVNum = 59;
                Skill skillinfo = ServerManager.GetSkill(skillVNum);
                if (skillinfo == null)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SKILL_DOES_NOT_EXIST"), 11));
                }

                if (skillinfo.SkillVNum < 200)
                {
                    foreach (CharacterSkill skill in Session.Character.Skills.GetAllItems())
                    {
                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                        {
                            Session.Character.Skills.Remove(skill.SkillVNum);
                        }
                    }
                }

                Session.Character.Skills[skillVNum] = new CharacterSkill
                {
                    SkillVNum = skillVNum,
                    CharacterId = Session.Character.CharacterId
                };
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(Session.Character.GenerateQuicklist());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"),
                    0));
            }
            else if (Session.Character.HeroLevel == 85)
            {
                short skillVNum = 55;
                Skill skillinfo = ServerManager.GetSkill(skillVNum);
                if (skillinfo == null)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SKILL_DOES_NOT_EXIST"), 11));
                }

                if (skillinfo.SkillVNum < 200)
                {
                    foreach (CharacterSkill skill in Session.Character.Skills.GetAllItems())
                    {
                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                        {
                            Session.Character.Skills.Remove(skill.SkillVNum);
                        }
                    }
                }

                Session.Character.Skills[skillVNum] = new CharacterSkill
                {
                    SkillVNum = skillVNum,
                    CharacterId = Session.Character.CharacterId
                };
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(Session.Character.GenerateQuicklist());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"),
                    0));
            }
            else if (Session.Character.HeroLevel == 95)
            {
                short skillVNum = 42;
                Skill skillinfo = ServerManager.GetSkill(skillVNum);
                if (skillinfo == null)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SKILL_DOES_NOT_EXIST"), 11));
                }

                if (skillinfo.SkillVNum < 200)
                {
                    foreach (CharacterSkill skill in Session.Character.Skills.GetAllItems())
                    {
                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                        {
                            Session.Character.Skills.Remove(skill.SkillVNum);
                        }
                    }
                }

                Session.Character.Skills[skillVNum] = new CharacterSkill
                {
                    SkillVNum = skillVNum,
                    CharacterId = Session.Character.CharacterId
                };
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(Session.Character.GenerateQuicklist());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"),
                    0));
            }
            else if (Session.Character.HeroLevel == 105)
            {
                short skillVNum = 39;
                Skill skillinfo = ServerManager.GetSkill(skillVNum);
                if (skillinfo == null)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SKILL_DOES_NOT_EXIST"), 11));
                }

                if (skillinfo.SkillVNum < 200)
                {
                    foreach (CharacterSkill skill in Session.Character.Skills.GetAllItems())
                    {
                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                        {
                            Session.Character.Skills.Remove(skill.SkillVNum);
                        }
                    }
                }

                Session.Character.Skills[skillVNum] = new CharacterSkill
                {
                    SkillVNum = skillVNum,
                    CharacterId = Session.Character.CharacterId
                };
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(Session.Character.GenerateQuicklist());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"),
                    0));
            }
            else if (Session.Character.HeroLevel == 110)
            {
                short skillVNum = 88;
                Skill skillinfo = ServerManager.GetSkill(skillVNum);
                if (skillinfo == null)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SKILL_DOES_NOT_EXIST"), 11));
                }

                if (skillinfo.SkillVNum < 200)
                {
                    foreach (CharacterSkill skill in Session.Character.Skills.GetAllItems())
                    {
                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                        {
                            Session.Character.Skills.Remove(skill.SkillVNum);
                        }
                    }
                }

                Session.Character.Skills[skillVNum] = new CharacterSkill
                {
                    SkillVNum = skillVNum,
                    CharacterId = Session.Character.CharacterId
                };
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(Session.Character.GenerateQuicklist());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"),
                    0));
            }
            else if (Session.Character.HeroLevel == 120)
            {
                short skillVNum = 89;
                Skill skillinfo = ServerManager.GetSkill(skillVNum);
                if (skillinfo == null)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SKILL_DOES_NOT_EXIST"), 11));
                }

                if (skillinfo.SkillVNum < 200)
                {
                    foreach (CharacterSkill skill in Session.Character.Skills.GetAllItems())
                    {
                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                        {
                            Session.Character.Skills.Remove(skill.SkillVNum);
                        }
                    }
                }

                Session.Character.Skills[skillVNum] = new CharacterSkill
                {
                    SkillVNum = skillVNum,
                    CharacterId = Session.Character.CharacterId
                };
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(Session.Character.GenerateQuicklist());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"),
                    0));
            }
            else if (Session.Character.HeroLevel == 130)
            {
                short skillVNum = 90;
                Skill skillinfo = ServerManager.GetSkill(skillVNum);
                if (skillinfo == null)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SKILL_DOES_NOT_EXIST"), 11));
                }

                if (skillinfo.SkillVNum < 200)
                {
                    foreach (CharacterSkill skill in Session.Character.Skills.GetAllItems())
                    {
                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                        {
                            Session.Character.Skills.Remove(skill.SkillVNum);
                        }
                    }
                }

                Session.Character.Skills[skillVNum] = new CharacterSkill
                {
                    SkillVNum = skillVNum,
                    CharacterId = Session.Character.CharacterId
                };
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(Session.Character.GenerateQuicklist());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"),
                    0));
            }
            else if (Session.Character.HeroLevel == 115)
            {
                short skillVNum = 92;
                Skill skillinfo = ServerManager.GetSkill(skillVNum);
                if (skillinfo == null)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SKILL_DOES_NOT_EXIST"), 11));
                }

                if (skillinfo.SkillVNum < 200)
                {
                    foreach (CharacterSkill skill in Session.Character.Skills.GetAllItems())
                    {
                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                        {
                            Session.Character.Skills.Remove(skill.SkillVNum);
                        }
                    }
                }

                Session.Character.Skills[skillVNum] = new CharacterSkill
                {
                    SkillVNum = skillVNum,
                    CharacterId = Session.Character.CharacterId
                };
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(Session.Character.GenerateQuicklist());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"),
                    0));
            }
            else if (Session.Character.HeroLevel == 125)
            {
                short skillVNum = 93;
                Skill skillinfo = ServerManager.GetSkill(skillVNum);
                if (skillinfo == null)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SKILL_DOES_NOT_EXIST"), 11));
                }

                if (skillinfo.SkillVNum < 200)
                {
                    foreach (CharacterSkill skill in Session.Character.Skills.GetAllItems())
                    {
                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                        {
                            Session.Character.Skills.Remove(skill.SkillVNum);
                        }
                    }
                }

                Session.Character.Skills[skillVNum] = new CharacterSkill
                {
                    SkillVNum = skillVNum,
                    CharacterId = Session.Character.CharacterId
                };
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(Session.Character.GenerateQuicklist());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"),
                    0));
            }
            else if (Session.Character.HeroLevel == 135)
            {
                short skillVNum = 94;
                Skill skillinfo = ServerManager.GetSkill(skillVNum);
                if (skillinfo == null)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SKILL_DOES_NOT_EXIST"), 11));
                }

                if (skillinfo.SkillVNum < 200)
                {
                    foreach (CharacterSkill skill in Session.Character.Skills.GetAllItems())
                    {
                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                        {
                            Session.Character.Skills.Remove(skill.SkillVNum);
                        }
                    }
                }

                Session.Character.Skills[skillVNum] = new CharacterSkill
                {
                    SkillVNum = skillVNum,
                    CharacterId = Session.Character.CharacterId
                };
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(Session.Character.GenerateQuicklist());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"),
                    0));
            }
            else if (Session.Character.HeroLevel == 145)
            {
                short skillVNum = 95;
                Skill skillinfo = ServerManager.GetSkill(skillVNum);
                if (skillinfo == null)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SKILL_DOES_NOT_EXIST"), 11));
                }

                if (skillinfo.SkillVNum < 200)
                {
                    foreach (CharacterSkill skill in Session.Character.Skills.GetAllItems())
                    {
                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                        {
                            Session.Character.Skills.Remove(skill.SkillVNum);
                        }
                    }
                }

                Session.Character.Skills[skillVNum] = new CharacterSkill
                {
                    SkillVNum = skillVNum,
                    CharacterId = Session.Character.CharacterId
                };
                Session.SendPacket(Session.Character.GenerateSki());
                Session.SendPackets(Session.Character.GenerateQuicklist());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"),
                    0));
            }

            #endregion


            Logger.LogUserEvent("LEVELUP", Session.GenerateIdentity(), $"Level: {SwitchLevel()} JobLevel: {JobLevel} SPLevel: {Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear)?.SpLevel} HeroLevel: {SwitchHeroLevel()} MapId: {Session.CurrentMapInstance?.Map.MapId} MapX: {PositionX} MapY: {PositionY}");
            return $"levelup {CharacterId}";


        }

        public void GenerateMiniland()
        {
            if (Miniland == null)
            {
                Miniland = ServerManager.GenerateMapInstance(20001, MapInstanceType.NormalInstance, new InstanceBag());
                foreach (MinilandObjectDTO obj in DAOFactory.MinilandObjectDAO.LoadByCharacterId(CharacterId))
                {
                    MinilandObject mapobj = new MinilandObject(obj);
                    if (mapobj.ItemInstanceId != null)
                    {
                        ItemInstance item = Inventory.GetItemInstanceById((Guid)mapobj.ItemInstanceId);
                        if (item != null)
                        {
                            mapobj.ItemInstance = item;
                            MinilandObjects.Add(mapobj);
                        }
                    }
                }
            }
        }

        public string GenerateMinilandObjectForFriends()
        {
            string mlobjstring = "mltobj";
            int i = 0;
            foreach (MinilandObject mp in MinilandObjects)
            {
                mlobjstring += $" {mp.ItemInstance.ItemVNum}.{i}.{mp.MapX}.{mp.MapY}";
                i++;
            }
            return mlobjstring;
        }

        public string GenerateMinilandPoint() => $"mlpt {MinilandPoint} 100";

        public string GenerateMinimapPosition() => MapInstance.MapInstanceType == MapInstanceType.TimeSpaceInstance
            || MapInstance.MapInstanceType == MapInstanceType.RaidInstance
            ? $"rsfp {MapInstance.MapIndexX} {MapInstance.MapIndexY}" : "rsfp 0 -1";

        public string GenerateMlinfo() => $"mlinfo 3800 {MinilandPoint} 100 {GeneralLogs.CountLinq(s => s.LogData == nameof(Miniland) && s.Timestamp.Day == DateTime.Now.Day)} {GeneralLogs.CountLinq(s => s.LogData == nameof(Miniland))} 10 {(byte)MinilandState} {Language.Instance.GetMessageFromKey("WELCOME_MUSIC_INFO")} {MinilandMessage}";

        public string GenerateMlinfobr() => $"mlinfobr 3800 {Name} {GeneralLogs.CountLinq(s => s.LogData == nameof(Miniland) && s.Timestamp.Day == DateTime.Now.Day)} {GeneralLogs.CountLinq(s => s.LogData == nameof(Miniland))} 25 {MinilandMessage.Replace(' ', '^')}";

        public string GenerateMloMg(MinilandObject mlobj, MinigamePacket packet) => $"mlo_mg {packet.MinigameVNum} {MinilandPoint} 0 0 {mlobj.ItemInstance.DurabilityPoint} {mlobj.ItemInstance.Item.MinilandObjectPoint}";

        public string GenerateNpcDialog(int value) => $"npc_req 1 {CharacterId} {value}";

        public string GeneratePairy()
        {
            ItemInstance fairy = null;
            if (Inventory != null)
            {
                fairy = Inventory.LoadBySlotAndType((byte)EquipmentType.Fairy, InventoryType.Wear);
            }
            ElementRate = 0;
            Element = 0;
            bool shouldChangeMorph = false;

            if (fairy != null)
            {
                //exclude magical fairy
                shouldChangeMorph = IsUsingFairyBooster && (fairy.Item.Morph > 4 && fairy.Item.Morph != 9 && fairy.Item.Morph != 14);
                ElementRate += fairy.ElementRate + fairy.Item.ElementRate + (IsUsingFairyBooster ? 30 : 0);
                Element = fairy.Item.Element;
            }

            return fairy != null
                ? $"pairy 1 {CharacterId} 4 {fairy.Item.Element} {fairy.ElementRate + fairy.Item.ElementRate} {fairy.Item.Morph + (shouldChangeMorph ? 5 : 0)}"
                : $"pairy 1 {CharacterId} 0 0 0 0";
        }

        public string GenerateParcel(MailDTO mail) => mail.AttachmentVNum != null ? $"parcel 1 1 {MailList.First(s => s.Value.MailId == mail.MailId).Key} {(mail.Title == "NOSMALL" ? 1 : 4)} 0 {mail.Date.ToString("yyMMddHHmm")} {mail.Title} {mail.AttachmentVNum} {mail.AttachmentAmount} {(byte)ServerManager.GetItem((short)mail.AttachmentVNum).Type}" : string.Empty;

        public string GeneratePidx(bool isLeaveGroup = false)
        {
            if (!isLeaveGroup && Group != null)
            {
                string result = $"pidx {Group.GroupId}";
                foreach (ClientSession session in Group.Characters.GetAllItems())
                {
                    if (session.Character != null)
                    {
                        result += $" {(Group.IsMemberOfGroup(CharacterId) ? 1 : 0)}.{session.Character.CharacterId} ";
                    }
                }
                return result;
            }
            return $"pidx -1 1.{CharacterId}";
        }

        public string GeneratePinit()
        {
            Group grp = ServerManager.Instance.Groups?.Find(s => s.IsMemberOfGroup(CharacterId) && s.GroupType == GroupType.Group);
            List<Mate> mates = Mates;
            int i = 0;
            string str = string.Empty;
            if (mates != null)
            {
                foreach (Mate mate in mates.Where(s => s.IsTeamMember).OrderByDescending(s => s.MateType))
                {
                    i++;
                    str += $" 2|{mate.MateTransportId}|{(mate.MateType == MateType.Pet ? "0" : "1")}|{mate.Level}|{(mate.IsUsingSp && mate.SpInstance != null ? mate.SpInstance.Item.Name.Replace(' ', '^') : mate.Name.Replace(' ', '^'))}|-1|{(mate.IsUsingSp && mate.SpInstance != null ? mate.SpInstance.Item.Morph : mate.Monster.NpcMonsterVNum)}|0";
                }
            }
            if (grp != null)
            {
                foreach (ClientSession groupSessionForId in grp.Characters.GetAllItems())
                {
                    i++;
                    str += $" 1|{groupSessionForId.Character.CharacterId}|{i}|{groupSessionForId.Character.SwitchLevel()}|{groupSessionForId.Character.Name}|0|{(byte)groupSessionForId.Character.Gender}|{(byte)groupSessionForId.Character.Class}|{(groupSessionForId.Character.UseSp ? groupSessionForId.Character.Morph : 0)}|{groupSessionForId.Character.SwitchHeroLevel()}";
                }
            }
            return $"pinit {i} {str}";
        }

        public string GeneratePlayerFlag(long pflag) => $"pflag 1 {CharacterId} {pflag}";

        public string GeneratePost(MailDTO mail, byte type)
        {
            if (mail != null)
            {
                return $"post 1 {type} {(MailList?.FirstOrDefault(s => s.Value?.MailId == mail?.MailId))?.Key} 0 {(mail.IsOpened ? 1 : 0)} {mail.Date.ToString("yyMMddHHmm")} {(type == 2 ? DAOFactory.CharacterDAO.LoadById(mail.ReceiverId).Name : DAOFactory.CharacterDAO.LoadById(mail.SenderId).Name)} {mail.Title}";
            }
            return string.Empty;
        }

        public string GeneratePostMessage(MailDTO mailDTO, byte type)
        {
            CharacterDTO sender = DAOFactory.CharacterDAO.LoadById(mailDTO.SenderId);

            return $"post 5 {type} {MailList.First(s => s.Value == mailDTO).Key} 0 0 {(byte)mailDTO.SenderClass} {(byte)mailDTO.SenderGender} {mailDTO.SenderMorphId} {(byte)mailDTO.SenderHairStyle} {(byte)mailDTO.SenderHairColor} {mailDTO.EqPacket} {sender.Name} {mailDTO.Title} {mailDTO.Message}";
        }

        public List<string> GeneratePst()
        {
            List<string> list = new List<string>();
            byte i = 0;
            Mates.Where(s => s.IsTeamMember).OrderBy(s => s.MateType).ToList().ForEach(mate =>
            {
                string si = "";
                mate.Buff.ForEach(s =>
                {
                    si += " " + s.Card.CardId + "." + mate.Level;
                });
                list.Add($"pst 2 {mate.MateTransportId} {(mate.MateType == MateType.Partner ? "1" : "0")} {(int)(mate.Hp / (float)mate.MaxHp * 100)} {(int)(mate.Mp / (float)mate.MaxMp * 100)} {mate.Hp} {mate.Mp} 0 0 0{si}");
            });
            return list;
        }
        public string GeneratePStashAll()
        {
            string stash = $"pstash_all {(StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.PetBackPack) ? 50 : 0)}";
            return Inventory.Where(s => s.Type == InventoryType.PetWarehouse).Aggregate(stash, (current, item) => current + $" {item.GenerateStashPacket()}");
        }

        public IEnumerable<string> GenerateQuicklist()
        {
            string[] pktQs = { "qslot 0", "qslot 1", "qslot 2" };

            for (int i = 0; i < 30; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    QuicklistEntryDTO qi = QuicklistEntries.Find(n => n.Q1 == j && n.Q2 == i && n.Morph == (UseSp ? Morph : 0));
                    pktQs[j] += $" {qi?.Type ?? 7}.{qi?.Slot ?? 7}.{qi?.Pos.ToString() ?? "-1"}";
                }
            }

            return pktQs;
        }

        public string GenerateFbt(int Type, bool exit = false)
        {
            string result = string.Empty;
            switch (Type)
            {
                case 0:
                    result = "fbt 0";
                    Group?.Characters?.ForEach(s => result += $" {s.Character?.CharacterId}");
                    break;

                case 2:
                    result = $"fbt 2 {(exit ? "-1" : $"{CharacterId}")}";
                    break;

                case 1:
                    result = $"fbt 1 {(exit ? 0 : 1)}";
                    break;

                case 3:
                    result = "fbt 3";
                    Group?.Characters?.ForEach(s => result += $" {s.Character?.CharacterId}.{Math.Ceiling(s.Character.Hp / s.Character.HPLoad() * 100)}.{Math.Ceiling(s.Character.Mp / s.Character.MPLoad() * 100)}");
                    break;

                case 4:
                    result = "fbt 4";
                    break;

                case 5:
                    result = "fbt 5 1";
                    break;

            }
            return result;
        }

        public string GenerateRaid(int Type, bool exit = false)
        {
            string result = string.Empty;
            switch (Type)
            {
                case 0:
                    result = "raid 0";
                    Group?.Characters?.ForEach(s => result += $" {s.Character?.CharacterId}");
                    break;

                case 2:
                    result = $"raid 2 {(exit ? "-1" : $"{CharacterId}")}";
                    break;

                case 1:
                    result = $"raid 1 {(exit ? 0 : 1)}";
                    break;

                case 3:
                    result = "raid 3";
                    Group?.Characters?.ForEach(s => result += $" {s.Character?.CharacterId}.{Math.Ceiling(s.Character.Hp / s.Character.HPLoad() * 100)}.{Math.Ceiling(s.Character.Mp / s.Character.MPLoad() * 100)}");
                    break;

                case 4:
                    result = "raid 4";
                    break;

                case 5:
                    result = "raid 5 1";
                    break;
            }
            return result;
        }

        public static string GenerateRaidBf(byte type) => $"raidbf 0 {type} 25 ";

        public string GenerateRc(int characterHealth) => $"rc 1 {CharacterId} {characterHealth} 0";

        public string GenerateRCSList(CSListPacket packet)
        {
            string list = string.Empty;
            BazaarItemLink[] billist = new BazaarItemLink[ServerManager.Instance.BazaarList.Count + 20];
            ServerManager.Instance.BazaarList.CopyTo(billist);
            foreach (BazaarItemLink bz in billist.Where(s => s != null && s.BazaarItem.SellerId == CharacterId).Skip(packet.Index * 50).Take(50))
            {
                if (bz.Item != null)
                {
                    short soldedAmount = (short)(bz.BazaarItem.Amount - bz.Item.Amount);
                    short amount = bz.BazaarItem.Amount;
                    bool package = bz.BazaarItem.IsPackage;
                    bool isNosbazar = bz.BazaarItem.MedalUsed;
                    long price = bz.BazaarItem.Price;
                    long minutesLeft = (long)(bz.BazaarItem.DateStart.AddHours(bz.BazaarItem.Duration) - DateTime.Now).TotalMinutes;
                    byte Status = minutesLeft >= 0 ? (soldedAmount < amount ? (byte)BazaarType.OnSale : (byte)BazaarType.Solded) : (byte)BazaarType.DelayExpired;
                    if (Status == (byte)BazaarType.DelayExpired)
                    {
                        minutesLeft = (long)(bz.BazaarItem.DateStart.AddHours(bz.BazaarItem.Duration).AddDays(isNosbazar ? 30 : 7) - DateTime.Now).TotalMinutes;
                    }
                    string info = string.Empty;
                    if (bz.Item.Item.Type == InventoryType.Equipment)
                    {
                        info = bz.Item?.GenerateEInfo().Replace(' ', '^').Replace("e_info^", string.Empty);
                    }
                    if (packet.Filter == 0 || packet.Filter == Status)
                    {
                        list += $"{bz.BazaarItem.BazaarItemId}|{bz.BazaarItem.SellerId}|{bz.Item.ItemVNum}|{soldedAmount}|{amount}|{(package ? 1 : 0)}|{price}|{Status}|{minutesLeft}|{(isNosbazar ? 1 : 0)}|0|{bz.Item.Rare}|{bz.Item.Upgrade}|{info} ";
                    }
                }
            }

            return $"rc_slist {packet.Index} {list}";
        }
        public string GeneratePsr(int i)
        {
            return $"psr {i}";
        }

        public string GenerateReqInfo()
        {
            ItemInstance fairy = null;
            ItemInstance armor = null;
            ItemInstance weapon2 = null;
            ItemInstance weapon = null;
            if (Inventory != null)
            {
                fairy = Inventory.LoadBySlotAndType((byte)EquipmentType.Fairy, InventoryType.Wear);
                armor = Inventory.LoadBySlotAndType((byte)EquipmentType.Armor, InventoryType.Wear);
                weapon2 = Inventory.LoadBySlotAndType((byte)EquipmentType.SecondaryWeapon, InventoryType.Wear);
                weapon = Inventory.LoadBySlotAndType((byte)EquipmentType.MainWeapon, InventoryType.Wear);
            }

            bool isPvpPrimary = false;
            bool isPvpSecondary = false;
            bool isPvpArmor = false;

            if (weapon?.Item.Name.Contains(": ") == true)
            {
                isPvpPrimary = true;
            }
            isPvpSecondary |= weapon2?.Item.Name.Contains(": ") == true;
            isPvpArmor |= armor?.Item.Name.Contains(": ") == true;

            // tc_info 0 name 0 0 0 0 -1 - 0 0 0 0 0 0 0 0 0 0 0 wins deaths reput 0 0 0 morph
            // talentwin talentlose capitul rankingpoints arenapoints 0 0 ispvpprimary ispvpsecondary
            // ispvparmor herolvl desc
            return $"tc_info {Level} {Name} {fairy?.Item.Element ?? 0} {ElementRate} {(byte)Class} {(byte)Gender} {(Family != null ? $"{Family.FamilyId} {Family.Name}({Language.Instance.GetMessageFromKey(FamilyCharacter.Authority.ToString().ToUpper())})" : "-1 -")} {GetReputationIco()} {GetDignityIco()} {(weapon != null ? 1 : 0)} {weapon?.Rare ?? 0} {weapon?.Upgrade ?? 0} {(weapon2 != null ? 1 : 0)} {weapon2?.Rare ?? 0} {weapon2?.Upgrade ?? 0} {(armor != null ? 1 : 0)} {armor?.Rare ?? 0} {armor?.Upgrade ?? 0} {Act4Kill} {Act4Dead} {Reputation} 0 0 0 {(UseSp ? Morph : 0)} {TalentWin} {TalentLose} {TalentSurrender} 0 {MasterPoints} {(Authority == AuthorityType.Donator && !Undercover ? 100 : Authority >= AuthorityType.TrialModerator && Authority <= AuthorityType.TrialGameMaster && !Undercover ? 50 : Authority >= AuthorityType.GameMaster && Authority <= AuthorityType.Owner && !Undercover ? 100 : Compliment)} {Act4Points} {(isPvpPrimary ? 1 : 0)} {(isPvpSecondary ? 1 : 0)} {(isPvpArmor ? 1 : 0)} {HeroLevel} {(string.IsNullOrEmpty(Biography) ? Language.Instance.GetMessageFromKey("NO_PREZ_MESSAGE") : Biography)}";

        }

        public string GenerateRest() => $"rest 1 {CharacterId} {(IsSitting ? 1 : 0)}";

        public string GenerateRevive()
        {
            int lives = MapInstance.InstanceBag.Lives - MapInstance.InstanceBag.DeadList.Count + 1;
            return $"revive 1 {CharacterId} {(lives > 0 ? lives : 0)}";
        }

        public string GenerateSay(string message, int type, bool ignoreNickname = false) => $"say {(ignoreNickname ? 2 : 1)} {CharacterId} {type} {message}";

        public string GenerateScal() => $"char_sc 1 {CharacterId} {Size}";

        public List<string> GenerateScN()
        {
            List<string> list = new List<string>();
            try
            {
                byte i = 0;
                Mates.Where(s => s.MateType == MateType.Partner).ToList().ForEach(s =>
                {
                    s.PetId = i;
                    s.LoadInventory();
                    list.Add(s.GenerateScPacket());
                    i++;
                });
            }
            catch (Exception ex)
            {

            }
            return list;
        }

        public List<string> GenerateScP(byte Page = 0)
        {
            List<string> list = new List<string>();
            byte i = 0;

            Mates.Where(s => s.MateType == MateType.Partner).Take(12).ToList().ForEach(s =>
            {
                s.PetId = i;
                list.Add(s.GenerateScPacket());
                i++;
            });
            i = 0;
            Mates.Where(s => s.MateType == MateType.Pet).Skip(Page * 10).Take(10).ToList().ForEach(s =>
            {
                s.PetId = (byte)(i + (Page * 10));
                list.Add(s.GenerateScPacket());
                i++;
            });

            return list;
        }

        public string GenerateScpStc() => $"sc_p_stc {MaxMateCount / 10} {MaxPartnerCount - 3}";

        public string GenerateShop(string shopname) => $"shop 1 {CharacterId} 1 3 0 {shopname}";

        public string GenerateShopEnd() => $"shop 1 {CharacterId} 0 0";

        public string GenerateSki()
        {
            List<CharacterSkill> characterSkills = UseSp ? SkillsSp.GetAllItems() : Skills.GetAllItems();
            foreach (CharacterSkill skill in Skills.Where(s => s.SkillVNum >= 20 && s.SkillVNum <= 95))
            {
                if (UseSp)
                {
                    characterSkills.Add(skill);
                }
            }
            #region SkillMate
            if (Mates != null)
            {
                Mate m = null;
                m = Mates?.Where(s => s.MateType == MateType.Partner && s.IsTeamMember).FirstOrDefault();
                if (m != null)
                {
                    NpcMonster mateNpc = ServerManager.GetNpc(m.NpcMonsterVNum);
                    if (mateNpc.Skills != null)
                    {
                        foreach (NpcMonsterSkill skill in mateNpc.Skills)
                        {
                            CharacterSkill characterSkillMate = new CharacterSkill { SkillVNum = skill.SkillVNum, CharacterId = CharacterId };
                            characterSkills.Add(characterSkillMate);
                        }
                    }
                }
            }
            #endregion
            string skibase = string.Empty;
            if (!UseSp)
            {
                skibase = $"{((byte)Class == 4 ? 1525 : 200 + (20 * (byte)Class))} {((byte)Class < 4 ? 201 + (20 * (byte)Class) : 1526)}";
            }
            else if (characterSkills.Count > 0)
            {
                skibase = $"{characterSkills[0].SkillVNum} {characterSkills[0].SkillVNum}";
            }
            string generatedSkills = string.Empty;
            foreach (CharacterSkill ski in characterSkills)
            {
                generatedSkills += $" {ski.SkillVNum}";
            }

            return $"ski {skibase}{generatedSkills}";
        }

        public string GenerateSpk(object message, int type) => $"spk 1 {CharacterId} {type} {Name} {message}";

        public string GenerateSpPoint() => $"sp {SpAdditionPoint} 1000000 {SpPoint} 10000";

        [Obsolete("GenerateStartupInventory should be used only on startup, for refreshing an inventory slot please use GenerateInventoryAdd instead.")]
        public void GenerateStartupInventory()
        {
            string inv0 = "inv 0", inv1 = "inv 1", inv2 = "inv 2", inv3 = "inv 3", inv6 = "inv 6", inv7 = "inv 7"; // inv 3 used for miniland objects
            if (Inventory != null)
            {
                foreach (ItemInstance inv in Inventory.GetAllItems())
                {
                    switch (inv.Type)
                    {
                        case InventoryType.Equipment:
                            if (inv.Item.EquipmentSlot == EquipmentType.Sp)
                            {
                                inv0 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Rare}.{inv.Upgrade}.{inv.SpStoneUpgrade}";
                            }
                            else
                            {
                                inv0 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Rare}.{(inv.Item.IsColored ? inv.Design : inv.Upgrade)}.0";
                            }
                            break;

                        case InventoryType.Main:
                            inv1 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Amount}.0";
                            break;

                        case InventoryType.Etc:
                            inv2 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Amount}.0";
                            break;

                        case InventoryType.Miniland:
                            inv3 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Amount}";
                            break;

                        case InventoryType.Specialist:
                            inv6 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Rare}.{inv.Upgrade}.{inv.SpStoneUpgrade}";
                            break;

                        case InventoryType.Costume:
                            inv7 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Rare}.{inv.Upgrade}.0";
                            break;
                    }
                }
            }
            Session.SendPacket(inv0);
            Session.SendPacket(inv1);
            Session.SendPacket(inv2);
            Session.SendPacket(inv3);
            Session.SendPacket(inv6);
            Session.SendPacket(inv7);
            Session.SendPacket(GetMinilandObjectList());
        }

        public string GenerateStat()
        {
            double option =
                (WhisperBlocked ? Math.Pow(2, (int)CharacterOption.WhisperBlocked - 1) : 0)
                + (FamilyRequestBlocked ? Math.Pow(2, (int)CharacterOption.FamilyRequestBlocked - 1) : 0)
                + (!MouseAimLock ? Math.Pow(2, (int)CharacterOption.MouseAimLock - 1) : 0)
                + (MinilandInviteBlocked ? Math.Pow(2, (int)CharacterOption.MinilandInviteBlocked - 1) : 0)
                + (ExchangeBlocked ? Math.Pow(2, (int)CharacterOption.ExchangeBlocked - 1) : 0)
                + (FriendRequestBlocked ? Math.Pow(2, (int)CharacterOption.FriendRequestBlocked - 1) : 0)
                + (EmoticonsBlocked ? Math.Pow(2, (int)CharacterOption.EmoticonsBlocked - 1) : 0)
                + (HpBlocked ? Math.Pow(2, (int)CharacterOption.HpBlocked - 1) : 0)
                + (BuffBlocked ? Math.Pow(2, (int)CharacterOption.BuffBlocked - 1) : 0)
                + (GroupRequestBlocked ? Math.Pow(2, (int)CharacterOption.GroupRequestBlocked - 1) : 0)
                + (HeroChatBlocked ? Math.Pow(2, (int)CharacterOption.HeroChatBlocked - 1) : 0)
                + (QuickGetUp ? Math.Pow(2, (int)CharacterOption.QuickGetUp - 1) : 0)
                + (!IsPetAutoRelive ? 64 : 0)
                + (!IsPartnerAutoRelive ? 128 : 0);
            return $"stat {Hp} {HPLoad()} {Mp} {MPLoad()} 0 {option}";
        }

        public string GenerateStatChar()
        {
            int weaponUpgrade = 0;
            int secondaryUpgrade = 0;
            int armorUpgrade = 0;
            MinHit = CharacterHelper.MinHit(Class, Level);
            MaxHit = CharacterHelper.MaxHit(Class, Level);
            HitRate = CharacterHelper.HitRate(Class, Level);
            HitCriticalRate = CharacterHelper.HitCriticalRate(Class, Level);
            HitCritical = CharacterHelper.HitCritical(Class, Level);
            MinDistance = CharacterHelper.MinDistance(Class, Level);
            MaxDistance = CharacterHelper.MaxDistance(Class, Level);
            DistanceRate = CharacterHelper.DistanceRate(Class, Level);
            DistanceCriticalRate = CharacterHelper.DistCriticalRate(Class, Level);
            DistanceCritical = CharacterHelper.DistCritical(Class, Level);
            FireResistance = GetStuffBuff(CardType.ElementResistance, (byte)AdditionalTypes.ElementResistance.FireIncreased)[0] + GetStuffBuff(CardType.ElementResistance, (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
            LightResistance = GetStuffBuff(CardType.ElementResistance, (byte)AdditionalTypes.ElementResistance.LightIncreased)[0] + GetStuffBuff(CardType.ElementResistance, (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
            WaterResistance = GetStuffBuff(CardType.ElementResistance, (byte)AdditionalTypes.ElementResistance.WaterIncreased)[0] + GetStuffBuff(CardType.ElementResistance, (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
            DarkResistance = GetStuffBuff(CardType.ElementResistance, (byte)AdditionalTypes.ElementResistance.DarkIncreased)[0] + GetStuffBuff(CardType.ElementResistance, (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
            Defence = CharacterHelper.Defence(Class, Level);
            DefenceRate = CharacterHelper.DefenceRate(Class, Level);
            ElementRate = 0;
            ElementRateSP = 0;
            DistanceDefence = CharacterHelper.DistanceDefence(Class, Level);
            DistanceDefenceRate = CharacterHelper.DistanceDefenceRate(Class, Level);
            MagicalDefence = CharacterHelper.MagicalDefence(Class, Level);


            if (Skills != null)
            {
                if (Skills.ContainsKey(20))
                {
                    MinDistance += 3;
                    MaxDistance += 3;
                    Defence += 3;
                    DefenceRate += 3;
                }
                if (Skills.ContainsKey(21))
                {
                    MinDistance += 12;
                    MaxDistance += 12;
                    Defence += 12;
                    DefenceRate += 12;
                }
                if (Skills.ContainsKey(22))
                {
                    MinDistance += 30;
                    MaxDistance += 30;
                    Defence += 30;
                    DefenceRate += 30;
                }
                if (Skills.ContainsKey(23))
                {
                    MinDistance += 50;
                    MaxDistance += 50;
                    Defence += 50;
                    DefenceRate += 50;
                }


                if (Skills.ContainsKey(24))
                {
                    MinDistance += 3;
                    MaxDistance += 3;
                    DistanceDefence += 3;
                    DistanceDefenceRate += 3;

                }
                if (Skills.ContainsKey(25))
                {
                    MinDistance += 12;
                    MaxDistance += 12;
                    DistanceDefence += 12;
                    DistanceDefenceRate += 12;
                }
                if (Skills.ContainsKey(26))
                {
                    MinDistance += 30;
                    MaxDistance += 30;
                    DistanceDefence += 30;
                    DistanceDefenceRate += 30;
                }
                if (Skills.ContainsKey(27))
                {
                    MinDistance += 50;
                    MaxDistance += 50;
                    DistanceDefence += 50;
                    DistanceDefenceRate += 50;
                }


                if (Skills.ContainsKey(28))
                {
                    MinDistance += 4;
                    MaxDistance += 4;
                    MagicalDefence += 4;
                }
                if (Skills.ContainsKey(29))
                {
                    MinDistance += 15;
                    MaxDistance += 15;
                    MagicalDefence += 15;
                }
                if (Skills.ContainsKey(30))
                {
                    MinDistance += 35;
                    MaxDistance += 35;
                    MagicalDefence += 35;
                }
                if (Skills.ContainsKey(31))
                {
                    MinDistance += 50;
                    MaxDistance += 50;
                    MagicalDefence += 50;
                }


                if (Skills.ContainsKey(48))
                {
                    Defence += 3;
                    DistanceDefence += 3;
                    MagicalDefence += 3;

                }
                if (Skills.ContainsKey(49))
                {
                    Defence += 8;
                    DistanceDefence += 8;
                    MagicalDefence += 8;
                }
                if (Skills.ContainsKey(50))
                {
                    Defence += 20;
                    DistanceDefence += 20;
                    MagicalDefence += 20;
                }
                if (Skills.ContainsKey(51))
                {
                    Defence += 50;
                    DistanceDefence += 50;
                    MagicalDefence += 50;
                }
            }

            if (UseSp)
            {
                // handle specialist
                ItemInstance specialist = Inventory?.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                if (specialist != null)
                {
                    MinHit += specialist.DamageMinimum + (specialist.SpDamage * 10);
                    MaxHit += specialist.DamageMaximum + (specialist.SpDamage * 10);
                    MinDistance += specialist.DamageMinimum + (specialist.SpDamage * 10);
                    MaxDistance += specialist.DamageMaximum + (specialist.SpDamage * 10);
                    HitCriticalRate += specialist.CriticalLuckRate;
                    HitCritical += specialist.CriticalRate;
                    DistanceCriticalRate += specialist.CriticalLuckRate;
                    DistanceCritical += specialist.CriticalRate;
                    HitRate += specialist.HitRate;
                    DistanceRate += specialist.HitRate;
                    DefenceRate += specialist.DefenceDodge;
                    DistanceDefenceRate += specialist.DistanceDefenceDodge;
                    FireResistance += specialist.Item.FireResistance + specialist.SpFire;
                    WaterResistance += specialist.Item.WaterResistance + specialist.SpWater;
                    LightResistance += specialist.Item.LightResistance + specialist.SpLight;
                    DarkResistance += specialist.Item.DarkResistance + specialist.SpDark;
                    ElementRateSP += specialist.ElementRate + specialist.SpElement;
                    Defence += specialist.CloseDefence + (specialist.SpDefence * 10);
                    DistanceDefence += specialist.DistanceDefence + (specialist.SpDefence * 10);
                    MagicalDefence += specialist.MagicDefence + (specialist.SpDefence * 10);

                    ItemInstance mainWeapon = Inventory.LoadBySlotAndType((byte)EquipmentType.MainWeapon, InventoryType.Wear);
                    ItemInstance secondaryWeapon = Inventory.LoadBySlotAndType((byte)EquipmentType.SecondaryWeapon, InventoryType.Wear);
                    List<ShellEffectDTO> effects = new List<ShellEffectDTO>();
                    if (mainWeapon?.ShellEffects != null)
                    {
                        effects.AddRange(mainWeapon.ShellEffects);
                    }
                    if (secondaryWeapon?.ShellEffects != null)
                    {
                        effects.AddRange(secondaryWeapon.ShellEffects);
                    }

                    int GetShellWeaponEffectValue(ShellWeaponEffectType effectType)
                    {
                        return effects?.Where(s => s.Effect == (byte)effectType)?.OrderByDescending(s => s.Value)?.FirstOrDefault()?.Value ?? 0;
                    }

                    int point = CharacterHelper.SlPoint(specialist.SlDamage, 0) + GetShellWeaponEffectValue(ShellWeaponEffectType.SLDamage) + GetShellWeaponEffectValue(ShellWeaponEffectType.SLGlobal);

                    int p = 0;
                    if (point <= 10)
                    {
                        p = point * 5;
                    }
                    else if (point <= 20)
                    {
                        p = 50 + ((point - 10) * 6);
                    }
                    else if (point <= 30)
                    {
                        p = 110 + ((point - 20) * 7);
                    }
                    else if (point <= 40)
                    {
                        p = 180 + ((point - 30) * 8);
                    }
                    else if (point <= 50)
                    {
                        p = 260 + ((point - 40) * 9);
                    }
                    else if (point <= 60)
                    {
                        p = 350 + ((point - 50) * 10);
                    }
                    else if (point <= 70)
                    {
                        p = 450 + ((point - 60) * 11);
                    }
                    else if (point <= 80)
                    {
                        p = 560 + ((point - 70) * 13);
                    }
                    else if (point <= 90)
                    {
                        p = 690 + ((point - 80) * 14);
                    }
                    else if (point <= 94)
                    {
                        p = 830 + ((point - 90) * 15);
                    }
                    else if (point <= 95)
                    {
                        p = 890 + 16;
                    }
                    else if (point <= 97)
                    {
                        p = 906 + ((point - 95) * 17);
                    }
                    else if (point > 97)
                    {
                        p = 940 + ((point - 97) * 20);
                    }

                    MaxHit += p;
                    MinHit += p;
                    MaxDistance += p;
                    MinDistance += p;

                    point = CharacterHelper.SlPoint(specialist.SlDefence, 1) + GetShellWeaponEffectValue(ShellWeaponEffectType.SLDefence) + GetShellWeaponEffectValue(ShellWeaponEffectType.SLGlobal);
                    p = 0;
                    if (point <= 10)
                    {
                        p = point;
                    }
                    else if (point <= 20)
                    {
                        p = 10 + ((point - 10) * 2);
                    }
                    else if (point <= 30)
                    {
                        p = 30 + ((point - 20) * 3);
                    }
                    else if (point <= 40)
                    {
                        p = 60 + ((point - 30) * 4);
                    }
                    else if (point <= 50)
                    {
                        p = 100 + ((point - 40) * 5);
                    }
                    else if (point <= 60)
                    {
                        p = 150 + ((point - 50) * 6);
                    }
                    else if (point <= 70)
                    {
                        p = 210 + ((point - 60) * 7);
                    }
                    else if (point <= 80)
                    {
                        p = 280 + ((point - 70) * 8);
                    }
                    else if (point <= 90)
                    {
                        p = 360 + ((point - 80) * 9);
                    }
                    else if (point > 90)
                    {
                        p = 450 + ((point - 90) * 10);
                    }

                    Defence += p;
                    MagicalDefence += p;
                    DistanceDefence += p;

                    point = CharacterHelper.SlPoint(specialist.SlElement, 2) + GetShellWeaponEffectValue(ShellWeaponEffectType.SLElement) + GetShellWeaponEffectValue(ShellWeaponEffectType.SLGlobal);
                    p = point <= 50 ? point : 50 + ((point - 50) * 2);
                    ElementRateSP += p;

                    _slhpbonus = GetShellWeaponEffectValue(ShellWeaponEffectType.SLHP) + GetShellWeaponEffectValue(ShellWeaponEffectType.SLGlobal);
                }
            }

            // TODO: add base stats
            ItemInstance weapon = Inventory?.LoadBySlotAndType((byte)EquipmentType.MainWeapon, InventoryType.Wear);
            if (weapon != null)
            {
                weaponUpgrade = weapon.Upgrade;
                MinHit += weapon.DamageMinimum + weapon.Item.DamageMinimum;
                MaxHit += weapon.DamageMaximum + weapon.Item.DamageMaximum;
                HitRate += weapon.HitRate + weapon.Item.HitRate;
                HitCriticalRate += weapon.CriticalLuckRate + weapon.Item.CriticalLuckRate;
                HitCritical += weapon.CriticalRate + weapon.Item.CriticalRate;

                // maxhp-mp
            }

            ItemInstance weapon2 = Inventory?.LoadBySlotAndType((byte)EquipmentType.SecondaryWeapon, InventoryType.Wear);
            if (weapon2 != null)
            {
                secondaryUpgrade = weapon2.Upgrade;
                MinDistance += weapon2.DamageMinimum + weapon2.Item.DamageMinimum;
                MaxDistance += weapon2.DamageMaximum + weapon2.Item.DamageMaximum;
                DistanceRate += weapon2.HitRate + weapon2.Item.HitRate;
                DistanceCriticalRate += weapon2.CriticalLuckRate + weapon2.Item.CriticalLuckRate;
                DistanceCritical += weapon2.CriticalRate + weapon2.Item.CriticalRate;

                // maxhp-mp
            }

            ItemInstance armor = Inventory?.LoadBySlotAndType((byte)EquipmentType.Armor, InventoryType.Wear);
            if (armor != null)
            {
                armorUpgrade = armor.Upgrade;
                Defence += armor.CloseDefence + armor.Item.CloseDefence;
                DefenceRate += armor.DefenceDodge + armor.Item.DefenceDodge;
                MagicalDefence += armor.MagicDefence + armor.Item.MagicDefence;
                DistanceDefence += armor.DistanceDefence + armor.Item.DistanceDefence;
                DistanceDefenceRate += armor.DistanceDefenceDodge + armor.Item.DistanceDefenceDodge;
            }

            ItemInstance fairy = Inventory?.LoadBySlotAndType((byte)EquipmentType.Fairy, InventoryType.Wear);
            if (fairy != null)
            {
                ElementRate += fairy.ElementRate + fairy.Item.ElementRate + (IsUsingFairyBooster ? 30 : 0);
            }

            for (short i = 1; i < 14; i++)
            {
                ItemInstance item = Inventory?.LoadBySlotAndType(i, InventoryType.Wear);
                if (item != null && item.Item.EquipmentSlot != EquipmentType.MainWeapon
                        && item.Item.EquipmentSlot != EquipmentType.SecondaryWeapon
                        && item.Item.EquipmentSlot != EquipmentType.Armor
                        && item.Item.EquipmentSlot != EquipmentType.Sp)
                {
                    FireResistance += item.FireResistance + item.Item.FireResistance;
                    LightResistance += item.LightResistance + item.Item.LightResistance;
                    WaterResistance += item.WaterResistance + item.Item.WaterResistance;
                    DarkResistance += item.DarkResistance + item.Item.DarkResistance;
                    Defence += item.CloseDefence + item.Item.CloseDefence;
                    DefenceRate += item.DefenceDodge + item.Item.DefenceDodge;
                    MagicalDefence += item.MagicDefence + item.Item.MagicDefence;
                    DistanceDefence += item.DistanceDefence + item.Item.DistanceDefence;
                    DistanceDefenceRate += item.DistanceDefenceDodge + item.Item.DistanceDefenceDodge;
                }
            }
            byte type = Class == ClassType.Adventurer ? (byte)0 : (byte)(Class - 1);
            return $"sc {type} {weaponUpgrade} {MinHit} {MaxHit} {HitRate} {HitCriticalRate} {HitCritical} {(Class == ClassType.Archer ? 1 : 0)} {secondaryUpgrade} {MinDistance} {MaxDistance} {DistanceRate} {DistanceCriticalRate} {DistanceCritical} {armorUpgrade} {Defence} {DefenceRate} {DistanceDefence} {DistanceDefenceRate} {MagicalDefence} {FireResistance} {WaterResistance} {LightResistance} {DarkResistance}";
        }

        public string GenerateStatInfo() => $"st 1 {CharacterId} {SwitchLevel()} {SwitchHeroLevel()} {(int)(Hp / (float)HPLoad() * 100)} {(int)(Mp / (float)MPLoad() * 100)} {Hp} {Mp}{Buff.GetAllItems().Aggregate(string.Empty, (current, buff) => current + $" {buff.Card.CardId}")}";

        public TalkPacket GenerateTalk(string message)
        {
            return new TalkPacket
            {
                CharacterId = CharacterId,
                Message = message
            };
        }

        public string GenerateTit() => $"tit {Language.Instance.GetMessageFromKey(Class == (byte)ClassType.Adventurer ? nameof(ClassType.Adventurer).ToUpper() : Class == ClassType.Swordman ? nameof(ClassType.Swordman).ToUpper() : Class == ClassType.Archer ? nameof(ClassType.Archer).ToUpper() : nameof(ClassType.Magician).ToUpper())} {Name}";

        public string GenerateTp() => $"tp 1 {CharacterId} {PositionX} {PositionY} 0";

        public void GetAct4Points(int point)
        {
            //RefreshComplimentRankingIfNeeded();
            Act4Points += point;
            if (Act4Points < 0)
            {
                Act4Points = 0;
            }
        }

        public int[] GetBuff(CardType type, byte subtype)
        {
            int value1 = 0;
            int value2 = 0;
            int value3 = 0;

            foreach (BCard entry in EquipmentBCards.Where(s => s?.Type.Equals((byte)type) == true && s.SubType.Equals((byte)(subtype / 10))))
            {
                if (entry.IsLevelScaled)
                {
                    if (entry.IsLevelDivided)
                    {
                        value1 += SwitchLevel() / entry.FirstData;
                    }
                    else
                    {
                        value1 += entry.FirstData * SwitchLevel();
                    }
                }
                else
                {
                    value1 += entry.FirstData;
                }
                value2 += entry.SecondData;
            }

            lock (Buff)
            {
                foreach (Buff buff in Buff.GetAllItems())
                {
                    // THIS ONE DOES NOT FOR STUFFS

                    foreach (BCard entry in buff.Card.BCards
                        .Where(s => s.Type.Equals((byte)type) && s.SubType.Equals((byte)(subtype / 10)) && (s.CastType != 1 || (s.CastType == 1 && buff.Start.AddMilliseconds(buff.Card.Delay * 100) < DateTime.Now))))
                    {
                        if (entry.IsLevelScaled)
                        {
                            if (entry.IsLevelDivided)
                            {
                                value1 += buff.Level / entry.FirstData;
                            }
                            else
                            {
                                value1 += entry.FirstData * buff.Level;
                            }
                        }
                        else
                        {
                            if (entry.FirstData != 0)
                            {
                                value1 += entry.FirstData;
                            }
                            else
                            {
                                value1 = 1;
                            }
                        }
                        if (entry.SecondData != 0)
                        {
                            value2 += entry.SecondData;
                        }
                        else
                        {
                            value2 = 1;
                        }

                        if (buff.SenderId != 0)
                        {
                            value3 = buff.SenderId;
                        }
                    }
                }
            }

            return new[] { value1, value2, value3 };
        }

        public int GetCP()
        {
            try
            {
                int cpmax = (Class > 0 ? 42 : 0) + (SwitchLevel() * 2);
                int cpused = 0;
                foreach (CharacterSkill ski in Skills.GetAllItems())
                {
                    cpused += ski.Skill.CPCost;
                }
                return cpmax - cpused;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        //public void GetContributi(int val)
        //{
        //    Contributi += val;
        //    Session.SendPacket(GenerateSay("Hai ricevuto " + val + " contributi.", 11));
        //}

        //public void LoseContributi(int val)
        //{
        //    if (Contributi < val)
        //    {
        //        Session.SendPacket(GenerateSay("Hai perso " + Contributi + " contributi.", 11));
        //        Contributi = 0;
        //    }
        //    else
        //    {
        //        Contributi -= val;
        //        Session.SendPacket(GenerateSay("Hai perso " + val + " contributi.", 11));
        //    }
        //}

        public void GetDamage(int damage, bool cankillplayers = true)
        {
            LastDefence = DateTime.Now;
            Dispose();

            if (cankillplayers == false)
            {
                if (Hp - damage > 1)
                {
                    Hp -= damage;
                }
                else
                {
                    //Hp = 1; // chuj wie na kurwa to tu jest ale przez to kazdy jest niesmiertelny i ma zawsze 1hp

                }
                return;
            }
            Hp -= damage;
            if (Hp < 0)
            {
                Hp = 0;
            }
        }

        public void GetDamageInPercentage(double percentage)
        {
            LastDefence = DateTime.Now;
            Dispose();
            short dannitot = (short)(HPLoad() * (percentage / 100));
            Hp -= dannitot;
            if (Hp < 0)
            {
                Hp = 1;

            }
        }

        public void GetMPPercentage(double percentage)
        {
            LastDefence = DateTime.Now;
            Dispose();
            short dannitot = (short)(MPLoad() * (percentage / 100));
            Mp -= dannitot;
            if (Mp < 0)
            {
                Mp = 1;
            }
        }

        public int GetDignityIco()
        {
            int icoDignity = 1;

            if (Dignity <= -100)
            {
                icoDignity = 2;
            }
            if (Dignity <= -200)
            {
                icoDignity = 3;
            }
            if (Dignity <= -400)
            {
                icoDignity = 4;
            }
            if (Dignity <= -600)
            {
                icoDignity = 5;
            }
            if (Dignity <= -800)
            {
                icoDignity = 6;
            }

            return icoDignity;
        }

        public List<Portal> GetExtraPortal() => MapInstancePortalHandler.GenerateMinilandEntryPortals(MapInstance.Map.MapId, Miniland.MapInstanceId);

        public List<string> GetFamilyHistory()
        {
            //TODO: Fix some bugs(missing history etc)
            if (Family != null)
            {
                const string packetheader = "ghis";
                List<string> packetList = new List<string>();
                string packet = string.Empty;
                int i = 0;
                int amount = 0;
                foreach (FamilyLogDTO log in Family.FamilyLogs.Where(s => s.FamilyLogType != FamilyLogType.WareHouseAdded && s.FamilyLogType != FamilyLogType.RaidWon && s.FamilyLogType != FamilyLogType.WareHouseRemoved).OrderByDescending(s => s.Timestamp).Take(100))
                {
                    packet += $" {(log.FamilyLogType > FamilyLogType.Heroic ? (byte)log.FamilyLogType - 3 : (byte)log.FamilyLogType)}|{log.FamilyLogData}|{(int)(DateTime.Now - log.Timestamp).TotalHours}";
                    i++;
                    if (i == 50)
                    {
                        i = 0;
                        packetList.Add(packetheader + (amount == 0 ? " 0 " : string.Empty) + packet);
                        amount++;
                    }
                    else if (i + (50 * amount) == Family.FamilyLogs.Where(s => s.FamilyLogType != FamilyLogType.WareHouseAdded && s.FamilyLogType != FamilyLogType.WareHouseRemoved).Count())
                    {
                        packetList.Add(packetheader + (amount == 0 ? " 0 " : string.Empty) + packet);
                    }
                }

                return packetList;
            }
            return new List<string>();
        }


        public IEnumerable<string> GetMinilandEffects() => MinilandObjects.Select(mp => mp.GenerateMinilandEffect(false)).ToList();

        public string GetMinilandObjectList()
        {
            string mlobjstring = "mlobjlst";
            foreach (ItemInstance item in Inventory.Where(s => s.Type == InventoryType.Miniland).OrderBy(s => s.Slot))
            {
                if (item.Item.IsMinilandObject)
                {
                    WareHouseSize = item.Item.MinilandObjectPoint;
                }
                MinilandObject mp = MinilandObjects.Find(s => s.ItemInstanceId == item.Id);
                bool used = mp != null;
                mlobjstring += $" {item.Slot}.{(used ? 1 : 0)}.{(used ? mp.MapX : 0)}.{(used ? mp.MapY : 0)}.{(item.Item.Width != 0 ? item.Item.Width : 1) }.{(item.Item.Height != 0 ? item.Item.Height : 1) }.{(used ? mp.ItemInstance.DurabilityPoint : 0)}.100.0.1";
            }

            return mlobjstring;
        }
        /*
        public void GetReferrerReward()
        {
            long referrerId = Session.Account.ReferrerId;
            if (SwitchLevel() >= 70 && referrerId != 0 && !CharacterId.Equals(referrerId))
            {
                List<GeneralLogDTO> logs = DAOFactory.GeneralLogDAO.LoadByLogType("ReferralProgram", null).Where(g => g.IpAddress.Equals(Session.Account.RegistrationIP.Split(':')[1].Replace("//", ""))).ToList();
                if (logs.Count <= 5)
                {
                    CharacterDTO character = DAOFactory.CharacterDAO.LoadById(referrerId);
                    if (character == null || character.SwitchLevel() < 70)
                    {
                        return;
                    }
                    AccountDTO referrer = DAOFactory.AccountDAO.LoadById(character.AccountId);
                    if (referrer != null && !AccountId.Equals(character.AccountId))
                    {
                        Logger.LogUserEvent("REFERRERREWARD", Session.GenerateIdentity(), $"AccountId: {AccountId} ReferrerId: {referrerId}");
                        DAOFactory.AccountDAO.WriteGeneralLog(AccountId, Session.Account.RegistrationIP, CharacterId, GeneralLogType.ReferralProgram, $"ReferrerId: {referrerId}");

                        // send gifts like you want
                        //SendGift(CharacterId, 5910, 1, 0, 0, false);
                        //SendGift(referrerId, 5910, 1, 0, 0, false);
                    }
                }
            }
        }*/

        public int GetReputationIco()
        {
            if (Authority >= AuthorityType.GameMaster)
            {
                return 32;
            }
            /*switch (Session.Account.LivelloMaestria)
            {
                case 1:
                    return 1;
                case 2:
                    return 4;
                case 3:
                    return 5;
                case 4:
                    return 6;
                case 5:
                    return 7;
                case 6:
                    return 8;
                case 7:
                    return 9;
                case 8:
                    return 10;
                case 9:
                    return 11;
                case 10:
                    return 12;
                case 11:
                    return 13;
                case 12:
                    return 14;
                case 13:
                    return 15;
                case 14:
                    return 16;
                case 15:
                    return 17;
                case 16:
                    return 18;
                case 17:
                    return 19;
                case 18:
                    return 20;
                case 19:
                    return 21;
                case 20:
                    return 22;
                case 21:
                    return 23;
                case 22:
                    return 24;
                case 23:
                    return 25;
                case 24:
                    return 26;
                case 25:
                    return 27;
                case 26:
                    return 28;
                case 27:
                    return 29;
                case 28:
                    return 30;
                case 29:
                    return 31;
                case 30:
                    return 32;
            }
            return 1;*/
            if (Reputation >= 5000001)
            {
                switch (IsReputationHero())
                {
                    case 1:
                        return 28;

                    case 2:
                        return 29;

                    case 3:
                        return 30;

                    case 4:
                        return 31;

                    case 5:
                        return 32;
                }
            }
            if (Reputation <= 50)
            {
                return 1;
            }

            if (Reputation <= 150)
            {
                return 2;
            }

            if (Reputation <= 250)
            {
                return 3;
            }

            if (Reputation <= 500)
            {
                return 4;
            }

            if (Reputation <= 750)
            {
                return 5;
            }

            if (Reputation <= 1000)
            {
                return 6;
            }

            if (Reputation <= 2250)
            {
                return 7;
            }

            if (Reputation <= 3500)
            {
                return 8;
            }

            if (Reputation <= 5000)
            {
                return 9;
            }

            if (Reputation <= 9500)
            {
                return 10;
            }

            if (Reputation <= 19000)
            {
                return 11;
            }

            if (Reputation <= 25000)
            {
                return 12;
            }

            if (Reputation <= 40000)
            {
                return 13;
            }

            if (Reputation <= 60000)
            {
                return 14;
            }

            if (Reputation <= 85000)
            {
                return 15;
            }

            if (Reputation <= 115000)
            {
                return 16;
            }

            if (Reputation <= 150000)
            {
                return 17;
            }

            if (Reputation <= 190000)
            {
                return 18;
            }

            if (Reputation <= 235000)
            {
                return 19;
            }

            if (Reputation <= 285000)
            {
                return 20;
            }

            if (Reputation <= 350000)
            {
                return 21;
            }

            if (Reputation <= 500000)
            {
                return 22;
            }

            if (Reputation <= 1500000)
            {
                return 23;
            }

            if (Reputation <= 2500000)
            {
                return 24;
            }

            if (Reputation <= 3750000)
            {
                return 25;
            }

            return Reputation <= 5000000 ? 26 : 27;
        }



        public void TeleportOnMap(short x, short y)
        {
            Session.Character.PositionX = x;
            Session.Character.PositionY = y;
            Session.CurrentMapInstance?.Broadcast($"tp {1} {CharacterId} {x} {y} 0");
            Session.SendPacket(GenerateCond());
        }

        /// <summary>
        /// Get Stuff Buffs Useful for Stats for example
        /// </summary>
        /// <param name="type"></param>
        /// <param name="subtype"></param>
        /// <returns></returns>
        public int[] GetStuffBuff(CardType type, byte subtype)
        {
            int value1 = 0;
            int value2 = 0;
            foreach (BCard entry in EquipmentBCards.Where(s => s.Type.Equals((byte)type) && s.SubType.Equals((byte)(subtype / 10))))
            {
                if (entry.IsLevelScaled)
                {
                    value1 += entry.FirstData * SwitchLevel();
                }
                else
                {
                    value1 += entry.FirstData;
                }
                value2 += entry.SecondData;
            }

            return new[] { value1, value2 };
        }

        public void GiftAdd(short itemVNum, short amount, byte rare = 0, byte upgrade = 0, short design = 0, bool forceRandom = false, byte minRare = 0, bool isQuest = false, string raid = "")
        {
            //TODO add the rare support
            if (Inventory != null)
            {
                lock (Inventory)
                {
                    ItemInstance newItem = Inventory.InstantiateItemInstance(itemVNum, CharacterId, amount);
                    if (newItem != null)
                    {
                        newItem.Design = design;

                        if (newItem.Item.ItemType == ItemType.Armor || newItem.Item.ItemType == ItemType.Weapon || newItem.Item.ItemType == ItemType.Shell || forceRandom)
                        {
                            if (rare != 0 && !forceRandom)
                            {
                                try
                                {
                                    newItem.RarifyItem(Session, RarifyMode.Drop, RarifyProtection.None, forceRare: rare);
                                    newItem.Upgrade = (byte)(newItem.Item.BasicUpgrade + upgrade);
                                    if (newItem.Upgrade > 10)
                                    {
                                        newItem.Upgrade = 10;
                                    }
                                }
                                catch (Exception)
                                {
                                    throw;
                                }
                            }
                            else if (rare == 0 || forceRandom)
                            {
                                do
                                {
                                    try
                                    {
                                        newItem.RarifyItem(Session, RarifyMode.Drop, RarifyProtection.None);
                                        newItem.Upgrade = newItem.Item.BasicUpgrade;
                                        if (newItem.Rare >= minRare)
                                        {
                                            break;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        break;
                                    }
                                } while (forceRandom);
                            }
                        }

                        if (newItem.Item.Type.Equals(InventoryType.Equipment) && rare != 0 && !forceRandom)
                        {
                            newItem.Rare = (sbyte)rare;
                            newItem.SetRarityPoint(Session);
                        }


                        if (newItem.ItemVNum == 4262 || newItem.ItemVNum == 283)
                        {
                            newItem.Upgrade = 3;
                        }

                        if (newItem.Item.ItemType == ItemType.Shell)
                        {
                            newItem.Upgrade = (byte)ServerManager.RandomNumber(50, 81);
                        }

                        if (raid != "")
                        {
                            InsertRaidLog(raid, newItem.Rare);
                        }

                        List<ItemInstance> newInv = Inventory.AddToInventory(newItem);
                        if (newInv.Count > 0)
                        {
                            //Session.SendPacket(GenerateSay($"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: {newItem.Item.Name} x {amount}", 10));
                            Session.SendPacket(isQuest
                                ? GenerateSay($"Quest reward: [ {newItem.Item.Name} x {amount} ]", 10)
                                : GenerateSay($"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: {newItem.Item.Name} x {amount}", 10));
                        }
                        else if (MailList.Count <= 40 && design == 0)
                        {
                            SendGift(CharacterId, itemVNum, amount, newItem.Rare, newItem.Upgrade, false);
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_ACQUIRED_BY_THE_GIANT_MONSTER"), 0));
                        }
                    }
                }
            }
        }

        public bool HaveBackpack() => StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.BackPack);

        public double HPLoad()
        {
            double multiplicator = 1.0;
            double multiplicatorbear = 0.0;
            int hp = 0;

            if (UseSp)
            {
                ItemInstance specialist = Inventory?.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                if (specialist != null)
                {
                    int point = CharacterHelper.SlPoint(specialist.SlHP, 3) + _slhpbonus;

                    if (point <= 50)
                    {
                        multiplicator += point / 100.0;
                    }
                    else
                    {
                        multiplicator += 0.5 + ((point - 50.00) / 50.00);
                    }
                    hp = specialist.HP + (specialist.SpHP * 100);
                }
            }

            hp += CellonOptions.Where(s => s.Type == CellonOptionType.HPMax).Sum(s => s.Value);
            multiplicatorbear += GetBuff(CardType.BearSpirit, (byte)AdditionalTypes.BearSpirit.IncreaseMaximumHP)[0] / 100D;
            multiplicator += GetBuff(CardType.MaxHPMP, (byte)AdditionalTypes.MaxHPMP.IncreasesMaximumHP)[0] / 100D;
            multiplicator += GetBuff(CardType.MaxHPMP, (byte)AdditionalTypes.MaxHPMP.MaximumHPMPIncreased)[0] / 100D;

            if (Skills != null)
            {
                if (Skills.ContainsKey(36))
                {
                    hp += 50;
                }
                if (Skills.ContainsKey(37))
                {
                    hp += 300;
                }
                if (Skills.ContainsKey(38))
                {
                    hp += 1000;
                }
                if (Skills.ContainsKey(39))
                {
                    hp += 3000;
                }
            }

            HPMax = (int)((CharacterHelper.HPData[(byte)Class, SwitchLevel()] + hp + GetBuff(CardType.MaxHPMP, (byte)AdditionalTypes.MaxHPMP.MaximumHPIncreased)[0] + GetBuff(CardType.MaxHPMP, (byte)AdditionalTypes.MaxHPMP.MaximumHPMPIncreased)[0]) * multiplicator);
            int BearEffettivi = (int)(HPMax * multiplicatorbear);
            if (BearEffettivi > 0)
            {
                if (BearEffettivi > 5000)
                    BearEffettivi = 5000;
                HPMax += BearEffettivi;
            }

            return HPMax;
        }


        public double MPLoad()
        {
            int mp = 0;
            double multiplicator = 1.0;

            if (UseSp)
            {
                ItemInstance specialist = Inventory?.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                if (specialist != null)
                {
                    int point = CharacterHelper.SlPoint(specialist.SlHP, 3);

                    if (point <= 50)
                    {
                        multiplicator += point / 100.0;
                    }
                    else
                    {
                        multiplicator += 0.5 + ((point - 50.00) / 50.00);
                    }
                    mp = specialist.MP + (specialist.SpHP * 100);
                }
            }


            mp += CellonOptions.Where(s => s.Type == CellonOptionType.MPMax).Sum(s => s.Value);

            multiplicator += GetBuff(CardType.BearSpirit, (byte)AdditionalTypes.BearSpirit.IncreaseMaximumMP)[0] / 100D;
            multiplicator += GetBuff(CardType.MaxHPMP, (byte)AdditionalTypes.MaxHPMP.IncreasesMaximumMP)[0] / 100D;
            multiplicator += GetBuff(CardType.MaxHPMP, (byte)AdditionalTypes.MaxHPMP.MaximumHPMPIncreased)[0] / 100D;

            if (Skills != null)
            {
                if (Skills.ContainsKey(40))
                {
                    mp += 50;
                }
                if (Skills.ContainsKey(41))
                {
                    mp += 300;
                }
                if (Skills.ContainsKey(42))
                {
                    mp += 1000;
                }
                if (Skills.ContainsKey(43))
                {
                    mp += 3000;
                }
            }

            MPMax = (int)((CharacterHelper.MPData[(byte)Class, SwitchLevel()] + mp + GetBuff(CardType.MaxHPMP, (byte)AdditionalTypes.MaxHPMP.MaximumMPIncreased)[0] + GetBuff(CardType.MaxHPMP, (byte)AdditionalTypes.MaxHPMP.MaximumHPMPIncreased)[0]) * multiplicator);


            return MPMax;
        }

        public void IncreaseDollars(int amount)
        {
            try
            {
                if (!ServerManager.Instance.MallApi.SendCurrencyAsync(ServerManager.Instance.Configuration.MallAPIKey, Session.Account.AccountId, amount).Result)
                {
                    Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("MALL_ACCOUNT_NOT_EXISTING"), 10));
                    return;
                }
            }
            catch
            {
                Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("MALL_UNKNOWN_ERROR"), 10));
                return;
            }

            Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("MALL_CURRENCY_RECEIVE"), amount), 10));
        }

        public void Initialize()
        {
            _random = new Random();
            ExchangeInfo = null;
            SpCooldown = 1;
            SaveX = 0;
            SaveY = 0;
            LastDefence = DateTime.Now.AddSeconds(-21);
            LastHealth = DateTime.Now;
            LastEffect = DateTime.Now;
            Session = null;
            MailList = new Dictionary<int, MailDTO>();
            Group = null;
            GmPvtBlock = false;
        }

        public static void InsertOrUpdatePenalty(PenaltyLogDTO log)
        {
            DAOFactory.PenaltyLogDAO.InsertOrUpdate(ref log);
            CommunicationServiceClient.Instance.RefreshPenalty(log.PenaltyLogId);
        }

        public bool IsBlockedByCharacter(long characterId) => CharacterRelations.Any(b => b.RelationType == CharacterRelationType.Blocked && b.CharacterId.Equals(characterId) && characterId != CharacterId);

        public bool IsBlockingCharacter(long characterId) => CharacterRelations.Any(c => c.RelationType == CharacterRelationType.Blocked && c.RelatedCharacterId.Equals(characterId));

        public bool IsFriendlistFull() => CharacterRelations.Where(s => s.RelationType == CharacterRelationType.Friend).ToList().Count >= 80;

        public bool IsFriendOfCharacter(long characterId) => CharacterRelations.Any(c => c.RelationType == CharacterRelationType.Friend && (c.RelatedCharacterId.Equals(characterId) || c.CharacterId.Equals(characterId)));

        public bool IsSpouseOfCharacter(long characterId) => CharacterRelations.Any(c => c.RelationType == CharacterRelationType.Spouse && (c.RelatedCharacterId.Equals(characterId) || c.CharacterId.Equals(characterId)));

        /// <summary>
        /// Checks if the current character is in range of the given position
        /// </summary>
        /// <param name="xCoordinate">The x coordinate of the object to check.</param>
        /// <param name="yCoordinate">The y coordinate of the object to check.</param>
        /// <param name="range">The range of the coordinates to be maximal distanced.</param>
        /// <returns>True if the object is in Range, False if not.</returns>
        public bool IsInRange(int xCoordinate, int yCoordinate, int range = 50) => Math.Abs(PositionX - xCoordinate) <= range && Math.Abs(PositionY - yCoordinate) <= range;

        public bool IsMuted() => Session.Account.PenaltyLogs.Any(s => s.Penalty == PenaltyType.Muted && s.DateEnd > DateTime.Now);

        public int IsReputationHero()
        {
            int i = 0;
            foreach (CharacterDTO character in ServerManager.Instance.TopReputation)
            {
                i++;
                if (character.CharacterId == CharacterId)
                {
                    if (i == 1)
                    {
                        return 5;
                    }
                    if (i == 2)
                    {
                        return 4;
                    }
                    if (i == 3)
                    {
                        return 3;
                    }
                    if (i <= 13)
                    {
                        return 2;
                    }
                    if (i <= 43)
                    {
                        return 1;
                    }
                }
            }
            return 0;
        }

        public void LearnAdventurerSkill()
        {
            if (Class == 0)
            {
                byte NewSkill = 0;
                for (int i = 200; i <= 210; i++)
                {
                    if (i == 209)
                    {
                        i++;
                    }

                    Skill skinfo = ServerManager.GetSkill((short)i);
                    if (skinfo.Class == 0 && JobLevel >= skinfo.LevelMinimum && Skills.All(s => s.SkillVNum != i))
                    {
                        NewSkill = 1;
                        Skills[i] = new CharacterSkill { SkillVNum = (short)i, CharacterId = CharacterId };
                    }
                }
                if (NewSkill > 0)
                {
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"), 0));
                    Session.SendPacket(GenerateSki());
                    Session.SendPackets(GenerateQuicklist());
                }
            }
        }

        public void LearnSPSkill()
        {
            ItemInstance specialist = null;
            if (Inventory != null)
            {
                specialist = Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
            }
            byte SkillSpCount = (byte)SkillsSp.Count;
            SkillsSp = new ThreadSafeSortedList<int, CharacterSkill>();
            foreach (Skill ski in ServerManager.GetAllSkill())
            {
                if (specialist != null && ski.Class == (Morph == 31 ? Morph + 30 : Morph + 31) && specialist.SpLevel >= ski.LevelMinimum)
                {
                    SkillsSp[ski.SkillVNum] = new CharacterSkill { SkillVNum = ski.SkillVNum, CharacterId = CharacterId };
                }
            }
            if (SkillsSp.Count != SkillSpCount)
            {
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SKILL_LEARNED"), 0));
            }
        }

        public void LoadInventory()
        {
            IEnumerable<ItemInstanceDTO> inventories = DAOFactory.IteminstanceDAO.LoadByCharacterId(CharacterId).Where(s => s.Type != InventoryType.FamilyWareHouse).ToList();
            IEnumerable<CharacterDTO> characters = DAOFactory.CharacterDAO.LoadAllByAccount(Session.Account.AccountId);
            foreach (CharacterDTO character in characters.Where(s => s.CharacterId != CharacterId))
            {
                inventories = inventories.Concat(DAOFactory.IteminstanceDAO.LoadByCharacterId(character.CharacterId).Where(s => s.Type == InventoryType.Warehouse).ToList());
            }
            Inventory = new Inventory(this);
            foreach (ItemInstanceDTO inventory in inventories)
            {
                inventory.CharacterId = CharacterId;
                Inventory[inventory.Id] = new ItemInstance(inventory);
            }
        }

        public void LoadQuicklists()
        {
            QuicklistEntries = new List<QuicklistEntryDTO>();
            IEnumerable<QuicklistEntryDTO> quicklistDTO = DAOFactory.QuicklistEntryDAO.LoadByCharacterId(CharacterId).ToList();
            foreach (QuicklistEntryDTO qle in quicklistDTO)
            {
                QuicklistEntries.Add(qle);
            }
        }

        public void LoadSentMail()
        {
            foreach (MailDTO mail in DAOFactory.MailDAO.LoadSentByCharacter(CharacterId))
            {
                MailList.Add((MailList.Count > 0 ? MailList.OrderBy(s => s.Key).Last().Key : 0) + 1, mail);

                Session.SendPacket(GeneratePost(mail, 2));
            }
        }

        public void LoadSkills()
        {
            Skills = new ThreadSafeSortedList<int, CharacterSkill>();
            IEnumerable<CharacterSkillDTO> characterskillDTO = DAOFactory.CharacterSkillDAO.LoadByCharacterId(CharacterId).ToList();
            foreach (CharacterSkillDTO characterskill in characterskillDTO.OrderBy(s => s.SkillVNum))
            {
                if (!Skills.ContainsKey(characterskill.SkillVNum))
                {
                    Skills[characterskill.SkillVNum] = new CharacterSkill(characterskill);
                }
            }
        }

        public void LoadSpeed()
        {
            // only load speed if you dont use custom speed
            if (!IsVehicled && !IsCustomSpeed)
            {
                Speed = CharacterHelper.SpeedData[(byte)Class];

                if (UseSp)
                {
                    ItemInstance specialist = Inventory?.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                    if (specialist != null)
                    {
                        Speed += specialist.Item.Speed;
                    }
                }

                byte fixSpeed = (byte)GetBuff(CardType.Move, (byte)AdditionalTypes.Move.SetMovement)[0];
                if (fixSpeed != 0)
                {
                    Speed = fixSpeed;
                }
                else
                {
                    Speed += (byte)GetBuff(CardType.Move, (byte)AdditionalTypes.Move.MovementSpeedIncreased)[0];
                    int MovementDecreased = GetBuff(CardType.Move, (byte)AdditionalTypes.Move.MoveSpeedIncreased)[0];
                    Speed *= (byte)(MovementDecreased != 0 ? Math.Abs((MovementDecreased) / 10) : 1);
                    Speed /= (byte)(MovementDecreased < 0 ? 10 : 1);
                    //Speed -= (byte)GetBuff(CardType.Move, (byte)AdditionalTypes.Move.MovementSpeedDecreased)[0];
                    //Speed /= (byte)(1 + (GetBuff(CardType.Move, (byte)AdditionalTypes.Move.MoveSpeedDecreased)[0] / 100D));
                    if (Invisible)
                    {
                        Speed += (byte)GetBuff(CardType.EffectSummon, (byte)AdditionalTypes.EffectSummon.MoveBoostInHidden)[0];
                    }
                }
            }

            if (IsShopping)
            {
                Speed = 0;
                IsCustomSpeed = false;
                return;
            }

            // reload vehicle speed after opening an shop for instance
            if (IsVehicled && !IsCustomSpeed)
            {
                byte extra = 0;
                try
                {
                    switch (Session.Character.Morph)
                    {
                        case 2368:
                        case 2369:
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 2;
                            }
                            break;

                        case 1819: // SnowBoard M
                        case 1820: // SnowBoad F
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4))
                            {
                                extra += 3;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 3;
                            }
                            break;

                        case 1817: // Sci M
                        case 1818: // Sci F
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4))
                            {
                                extra += 3;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 5;
                            }
                            break;

                        case 2517: // Nossi M
                        case 2518: // Nossi F
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4))
                            {
                                extra += 2;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 3;
                            }
                            break;

                        case 2432: // Scopa Cornuta
                        case 2433: // Scopa Cornuta F
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4))
                            {
                                extra += 2;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 3;
                            }
                            break;

                        case 2406: // Tigre Bianca Magica
                        case 2407: // Tigre Bianca Magica F
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4))
                            {
                                extra += 2;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 2;
                            }
                            break;

                        case 2928: // Male UFO
                        case 2929: // Female UFO
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4))
                            {
                                extra += 1;
                            }
                            break;

                        case 2526: // White male unicorn
                        case 2527: // White female unicorn
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4))
                            {
                                extra += 1;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 1;
                            }
                            break;

                        case 2411: // Cabrio Magica
                        case 2412: // Cabrio Magica
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act1) || Session.CurrentMapInstance.Map.MapId == 145)
                            {
                                extra += 4;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 2;
                            }
                            break;

                        case 2528: // Pink male unicorn
                        case 2529: // Pink female unicorn
                            if (Session.CurrentMapInstance.Map.MapId == 1 || Session.CurrentMapInstance.Map.MapId == 145)
                            {
                                extra += 3;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 3;
                            }
                            break;

                        case 2930: //Marco Pollo
                        case 2931: //Marco Pollo

                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act1))
                            {
                                extra += 6;
                            }
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act2))
                            {
                                extra += 6;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 2;
                            }
                            break;

                        case 2513://Cammello M
                        case 2514://Cammello F

                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act51))
                            {
                                extra += 6;
                            }
                            else if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act52))
                            {
                                extra += 6;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 2;
                            }
                            break;

                        case 2520: // Bici M
                        case 2521: // Bici F
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act51))
                            {
                                extra += 2;
                            }
                            else if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act52))
                            {
                                extra += 2;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 3;
                            }
                            break;

                        case 2530: // Black male unicorn
                        case 2531: // Black Female Unicorn
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act51))
                            {
                                extra += 2;
                            }
                            else if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act52))
                            {
                                extra += 2;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 2;
                            }
                            break;

                        case 2522: // Roller M
                        case 2523: // Roller F
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act51))
                            {
                                extra += 2;
                            }
                            else if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act52))
                            {
                                extra += 2;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 2;
                            }
                            break;

                        case 2934: // Jeep
                        case 2935: // Jeep
                        case 2936: // Jeep
                        case 2937: // Jeep
                        case 2938: // Jeep
                        case 2939: // Jeep
                        case 2940: // Jeep
                        case 2941: // Jeep
                        case 2942: // Jeep
                        case 2943: // Jeep
                        case 2944: // Jeep
                        case 2945: // Jeep
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act51))
                            {
                                extra += 2;
                            }
                            else if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act52))
                            {
                                extra += 2;
                            }
                            break;

                        case 2524: // SkateBoard M
                        case 2525: // SkateBoard F
                        case 2932: // WindSurf M
                        case 2933: // WindSurf F
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act51))
                            {
                                extra += 2;
                            }
                            else if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act52))
                            {
                                extra += 2;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 3;
                            }
                            break;

                        case 3679: // Male squelettic dragon
                        case 3680: // Female squelettic dragon

                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act51))
                            {
                                extra += 2;
                            }
                            else if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act52))
                            {
                                extra += 2;
                            }
                            else if (Session.CurrentMapInstance.Map.MapId == 1 || Session.CurrentMapInstance.Map.MapId == 145)
                            {
                                extra += 1;
                            }
                            break;

                        case 2370: // Magic Carpet M
                        case 2371: // Magic Carpet F
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act51))
                            {
                                extra += 2;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 2;
                            }
                            break;

                        case 3693: // Giaguaro M
                        case 3694: // Giaguaro F
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act51))
                            {
                                extra += 2;
                            }
                            else if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act52))
                            {
                                extra += 2;
                            }
                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4))
                            {
                                extra += 3;
                            }
                            if (Session.Character.Buff.Any(s => s.Card.CardId == 336))
                            {
                                extra += 4;
                            }
                            break;

                    }
                }
                catch (Exception ex)
                {

                }

                Speed = (byte)(VehicleSpeed + extra);
                Speed += (byte)GetBuff(CardType.Move, (byte)AdditionalTypes.Move.MovementSpeedIncreased)[0];
                Speed *= (byte)(1 + (GetBuff(CardType.Move, (byte)AdditionalTypes.Move.MoveSpeedIncreased)[0] / 100D));
            }
        }

        public bool MuteMessage()
        {
            PenaltyLogDTO penalty = Session.Account.PenaltyLogs.OrderByDescending(s => s.DateEnd).FirstOrDefault();
            if (IsMuted() && penalty != null)
            {
                Session.CurrentMapInstance?.Broadcast(Gender == GenderType.Female ? GenerateSay(Language.Instance.GetMessageFromKey("MUTED_FEMALE"), 1) : GenerateSay(Language.Instance.GetMessageFromKey("MUTED_MALE"), 1));
                Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("MUTE_TIME"), (penalty.DateEnd - DateTime.Now).ToString(@"hh\:mm\:ss")), 11));
                Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("MUTE_TIME"), (penalty.DateEnd - DateTime.Now).ToString(@"hh\:mm\:ss")), 12));
                return true;
            }
            return false;
        }

        public string OpenFamilyWarehouse()
        {
            if (Family == null || Family.WarehouseSize == 0)
            {
                return UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("NO_FAMILY_WAREHOUSE"));
            }
            return GenerateFStashAll();
        }

        public List<string> OpenFamilyWarehouseHist()
        {
            List<string> packetList = new List<string>();
            if (Family == null || !(FamilyCharacter.Authority == FamilyAuthority.Head
                || FamilyCharacter.Authority == FamilyAuthority.Assistant
                || (FamilyCharacter.Authority == FamilyAuthority.Member && Family.MemberCanGetHistory)
                || (FamilyCharacter.Authority == FamilyAuthority.Manager && Family.ManagerCanGetHistory)))
            {
                packetList.Add(UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("NO_FAMILY_RIGHT")));
                return packetList;
            }
            return GenerateFamilyWarehouseHist();
        }

        public void LoadMail()
        {
            int parcel = 0, letter = 0;
            foreach (MailDTO mail in DAOFactory.MailDAO.LoadSentToCharacter(CharacterId))
            {
                MailList.Add((MailList.Count > 0 ? MailList.OrderBy(s => s.Key).Last().Key : 0) + 1, mail);

                if (mail.AttachmentVNum != null)
                {
                    parcel++;
                    Session.SendPacket(GenerateParcel(mail));
                }
                else
                {
                    if (!mail.IsOpened)
                    {
                        letter++;
                    }
                    Session.SendPacket(GeneratePost(mail, 1));
                }
            }
            if (parcel > 0)
            {
                Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("GIFTED"), parcel), 11));
            }
            if (letter > 0)
            {
                Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("NEW_MAIL"), letter), 10));
            }
        }

        public void RemoveBuff(short id, bool removePermaBuff = false)
        {
            Buff indicator = Buff[id];
            if (indicator != null)
            {
                if (indicator == null || !removePermaBuff && indicator.IsPermaBuff && indicator.RemainingTime <= 0)
                {
                    return;
                }

                if (indicator.IsPermaBuff && !removePermaBuff)
                {
                    AddBuff(indicator);
                    return;
                }
                if (indicator.StaticBuff)
                {
                    Session.SendPacket($"vb {indicator.Card.CardId} 0 {indicator.Card.Duration}");
                    Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_TERMINATED"), indicator.Card.Name), 11));
                }
                else
                {
                    Session.SendPacket($"bf 1 {CharacterId} 0.{indicator.Card.CardId}.0 {SwitchLevel()}");
                    Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_TERMINATED"), indicator.Card.Name), 20));
                }

                if (Buff[indicator.Card.CardId] != null)
                {
                    /*if (indicator.Card.BCards.Any(s => s.Type == (byte)CardType.Move && !s.SubType.Equals((byte)AdditionalTypes.Move.MovementImpossible / 10)))
                    {
                        LastSpeedChange = DateTime.Now;
                        LoadSpeed();
                        Session.SendPacket(GenerateCond());
                    }*/
                    Buff.Remove(id);
                }
                if (indicator.Card.TimeoutBuff != 0 && ServerManager.RandomNumber() < indicator.Card.TimeoutBuffChance)
                {
                    AddBuff(new Buff(indicator.Card.TimeoutBuff, SwitchLevel()));
                }
                /*if (indicator.Card.BCards.Any(s => s.Type == (byte)CardType.Move && !s.SubType.Equals((byte)AdditionalTypes.Move.MovementImpossible / 10)))
                {
                    LastSpeedChange = DateTime.Now;
                    LoadSpeed();
                    Session.SendPacket(GenerateCond());
                }*/
                if (Buff.Where(b => b.Card.EffectId == 10147 || b.Card.EffectId == 10148) == null)
                {
                    LastSpeedChange = DateTime.Now;
                    NoMove = false;
                    LoadSpeed();
                    Session.SendPacket(GenerateCond());
                }
                if (indicator.Card.BCards.Any(s => s.Type == (byte)CardType.SpecialAttack && s.SubType.Equals((byte)AdditionalTypes.SpecialAttack.NoAttack / 10)))
                {
                    if (GetBuff(CardType.SpecialAttack, (byte)AdditionalTypes.SpecialAttack.NoAttack)[0] > 0)
                    {
                        Session.SendPacket(GenerateCond());
                    }
                    else
                    {
                        NoAttack = false;
                        Session.SendPacket(GenerateCond());
                    }
                }
                else
                {
                    NoAttack = false;
                    Session.SendPacket(GenerateCond());
                }
                if (indicator.Card.BCards.Any(s => s.Type == (byte)CardType.Move && s.SubType.Equals((byte)AdditionalTypes.Move.MovementImpossible / 10)))
                {
                    if (GetBuff(CardType.Move, (byte)AdditionalTypes.Move.MovementImpossible)[0] > 0)
                    {
                        Session.SendPacket(GenerateCond());
                    }
                    else
                    {
                        NoMove = false;
                        Session.SendPacket(GenerateCond());
                    }
                }
                else
                {
                    NoMove = false;
                    Session.SendPacket(GenerateCond());
                }
                if (indicator.Card.BCards.Any(s => s.Type == (byte)CardType.SpecialAttack && s.SubType.Equals((byte)AdditionalTypes.SpecialAttack.RangedDisabled / 10)))
                {
                    /*if (GetBuff(CardType.SpecialAttack, (byte)AdditionalTypes.SpecialAttack.RangedDisabled / 10)[0] > 0)
                    {
                    }
                    else
                    {
                        RangedDisable = false;
                        Session.SendPacket(GenerateCond());
                    }*/
                    RangedDisable = true;
                    Session.SendPacket(GenerateCond());
                }
                else
                {
                    RangedDisable = false;
                    Session.SendPacket(GenerateCond());
                }
                if (indicator.Card.BCards.Any(s => s.Type == (byte)CardType.AngerSkill && s.SubType.Equals((byte)AdditionalTypes.AngerSkill.OnlyNormalAttacks / 10)))
                {
                    if (GetBuff(CardType.AngerSkill, (byte)AdditionalTypes.AngerSkill.OnlyNormalAttacks / 10)[0] > 0)
                    {
                    }
                    else
                    {
                        OnlyNormalAttacks = false;
                        Session.SendPacket(GenerateCond());
                    }
                }
                if (indicator.Card.BCards.Any(s => s.Type == (byte)CardType.JumpBackPush && s.SubType.Equals((byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased / 10)))
                {
                    HitRate += GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased / 10)[0];
                    Session.SendPacket(GenerateCond());
                }
                
                /*if (Buff.Where(b => b.Card.EffectId == 5837) == null)
                {
                    NoAttack = false;
                    Session.SendPacket(GenerateCond());
                }*/

                // TODO : Find another way because it is hardcode
                if (indicator.Card.CardId == 131)
                {
                    Session.SendPacket(GeneratePairy());
                }
                if (indicator.Card.CardId == 336)
                {
                    LoadSpeed();
                    switch (Morph)
                    {
                        case 3693: // Giaguaro M
                        case 3694: // Giaguaro F
                            if (ServerManager.RandomNumber(0, 100) <= 25)
                            {
                                AddBuff(new Buff(665, SwitchLevel()));
                            }
                            break;
                    }
                }
                if (indicator.Card.CardId == 559)
                {
                    AddBuff(new Buff(560, SwitchLevel()));
                }
                //if (indicator.Card.CardId == 460)
                //{
                //    Size = 10;
                //    Session.CurrentMapInstance?.Broadcast(Session.Character.GenerateScal());
                //}
                if (!indicator.Card.BCards.Any(s => s.Type == (byte)CardType.SpecialActions && s.SubType == (byte)AdditionalTypes.SpecialActions.Hide / 10))
                {
                    return;
                }
                Invisible = false;
                if (!Session.Character.IsVehicled)
                {
                    Mates.Where(m => m.IsTeamMember).ToList().ForEach(m => MapInstance?.Broadcast(m.GenerateIn()));
                }
                MapInstance?.Broadcast(GenerateInvisible());
                if (!indicator.Card.BCards.Any(s => s.Type == (byte)CardType.EffectSummon && s.SubType == (byte)AdditionalTypes.EffectSummon.Illusionista / 10))
                {
                    return;
                }
                Invisible = false;
                if (!Session.Character.IsVehicled)
                {
                    Mates.Where(m => m.IsTeamMember).ToList().ForEach(m => MapInstance?.Broadcast(m.GenerateIn()));
                }
                MapInstance?.Broadcast(GenerateInvisible());
            }
        }

        public string GenerateCsP(string value)
        {
            return $"csp {CharacterId} {value}";
        }

        public void RemoveVehicle()
        {
            ItemInstance sp = null;
            if (Inventory != null)
            {
                sp = Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
            }
            IsVehicled = false;
            LoadSpeed();
            if (UseSp)
            {
                if (sp != null)
                {
                    Morph = sp.Item.Morph;
                    MorphUpgrade = sp.Upgrade;
                    MorphUpgrade2 = sp.Design;
                }
            }
            else
            {
                Morph = 0;
            }
            Session.CurrentMapInstance?.Broadcast(GenerateCMode());
            Session.SendPacket(GenerateCond());
            Session.Character.Mates.Where(s => s.IsTeamMember).ToList().ForEach(s =>
            {
                s.PositionX = Session.Character.PositionX;
                s.PositionY = Session.Character.PositionY;
                Session.CurrentMapInstance?.Broadcast(s.GenerateIn());
            });
            LastSpeedChange = DateTime.Now;
        }

        public void Rest()
        {
            if (LastSkillUse.AddSeconds(4) > DateTime.Now)
            {
                return;
            }
            if (!IsVehicled)
            {
                IsSitting = !IsSitting;
                Session.CurrentMapInstance?.Broadcast(GenerateRest());
            }
            else
            {
                Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("IMPOSSIBLE_TO_USE"), 10));
            }
        }

        public void Save()
        {
            Logger.LogUserEvent("CHARACTER_DB_SAVE", Session.GenerateIdentity(), "START");
            try
            {
              

                CharacterDTO character = DeepCopy();
                DAOFactory.CharacterDAO.InsertOrUpdate(ref character);

                if (Inventory != null)
                {
                    // be sure that noone tries to edit while saving is currently editing
                    lock (Inventory)
                    {
                        // load and concat inventory with equipment
                        List<ItemInstance> inventories = Inventory.GetAllItems();
                        IEnumerable<Guid> currentlySavedInventoryIds = DAOFactory.IteminstanceDAO.LoadSlotAndTypeByCharacterId(CharacterId);
                        IEnumerable<CharacterDTO> characters = DAOFactory.CharacterDAO.LoadByAccount(Session.Account.AccountId);
                        foreach (CharacterDTO characteraccount in characters.Where(s => s.CharacterId != CharacterId))
                        {
                            currentlySavedInventoryIds = currentlySavedInventoryIds.Concat(DAOFactory.IteminstanceDAO.LoadByCharacterId(characteraccount.CharacterId).Where(s => s.Type == InventoryType.Warehouse).Select(i => i.Id).ToList());
                        }

                        IEnumerable<MinilandObjectDTO> currentlySavedMinilandObjectEntries = DAOFactory.MinilandObjectDAO.LoadByCharacterId(CharacterId).ToList();
                        foreach (MinilandObjectDTO mobjToDelete in currentlySavedMinilandObjectEntries.Except(MinilandObjects))
                        {
                            DAOFactory.MinilandObjectDAO.DeleteById(mobjToDelete.MinilandObjectId);
                        }

                        DAOFactory.IteminstanceDAO.DeleteGuidList(currentlySavedInventoryIds.Except(inventories.Select(i => i.Id)));

                        // create or update all which are new or do still exist
                        List<ItemInstance> saveInventory = inventories.Where(s => s.Type != InventoryType.Bazaar && s.Type != InventoryType.FamilyWareHouse).ToList();

                        DAOFactory.IteminstanceDAO.InsertOrUpdateFromList(saveInventory);

                        foreach (ItemInstance itemInstance in saveInventory)
                        {
                            DAOFactory.ShellEffectDAO.InsertOrUpdateFromList(itemInstance.ShellEffects, itemInstance.EquipmentSerialId);
                            DAOFactory.CellonOptionDAO.InsertOrUpdateFromList(itemInstance.CellonOptions, itemInstance.EquipmentSerialId);
                            //foreach (ShellEffectDTO effect in wearInstance.ShellEffects)
                            //{
                            //    effect.EquipmentSerialId = wearInstance.EquipmentSerialId;
                            //    effect.ShellEffectId = DAOFactory.ShellEffectDAO.InsertOrUpdate(effect).ShellEffectId;
                            //}
                            //foreach (CellonOptionDTO effect in wearInstance.CellonOptions)
                            //{
                            //    effect.EquipmentSerialId = wearInstance.EquipmentSerialId;
                            //    effect.CellonOptionId = DAOFactory.CellonOptionDAO.InsertOrUpdate(effect).CellonOptionId;
                            //}
                        }
                    }
                }

                if (Skills != null)
                {
                    IEnumerable<Guid> currentlySavedCharacterSkills = DAOFactory.CharacterSkillDAO.LoadKeysByCharacterId(CharacterId).ToList();

                    foreach (Guid characterSkillToDeleteId in currentlySavedCharacterSkills.Except(Skills.Select(s => s.Id)))
                    {
                        DAOFactory.CharacterSkillDAO.Delete(characterSkillToDeleteId);
                    }

                    foreach (CharacterSkill characterSkill in Skills.GetAllItems())
                    {
                        DAOFactory.CharacterSkillDAO.InsertOrUpdate(characterSkill);
                    }
                }

                IEnumerable<long> currentlySavedMates = DAOFactory.MateDAO.LoadByCharacterId(CharacterId).Select(s => s.MateId);

                foreach (long matesToDeleteId in currentlySavedMates.Except(Mates.Select(s => s.MateId)))
                {
                    DAOFactory.MateDAO.Delete(matesToDeleteId);
                }

                foreach (Mate mate in Mates)
                {
                    MateDTO matesave = mate;
                    DAOFactory.MateDAO.InsertOrUpdate(ref matesave);
                }

                IEnumerable<QuicklistEntryDTO> quickListEntriesToInsertOrUpdate = QuicklistEntries.ToList();

                IEnumerable<Guid> currentlySavedQuicklistEntries = DAOFactory.QuicklistEntryDAO.LoadKeysByCharacterId(CharacterId).ToList();
                foreach (Guid quicklistEntryToDelete in currentlySavedQuicklistEntries.Except(QuicklistEntries.Select(s => s.Id)))
                {
                    DAOFactory.QuicklistEntryDAO.Delete(quicklistEntryToDelete);
                }
                foreach (QuicklistEntryDTO quicklistEntry in quickListEntriesToInsertOrUpdate)
                {
                    DAOFactory.QuicklistEntryDAO.InsertOrUpdate(quicklistEntry);
                }

                foreach (MinilandObjectDTO mobjEntry in (IEnumerable<MinilandObjectDTO>)MinilandObjects.ToList())
                {
                    MinilandObjectDTO mobj = mobjEntry;
                    DAOFactory.MinilandObjectDAO.InsertOrUpdate(ref mobj);
                }

                IEnumerable<short> currentlySavedBuff = DAOFactory.StaticBuffDAO.LoadByTypeCharacterId(CharacterId);
                foreach (short bonusToDelete in currentlySavedBuff.Except(Buff.Select(s => s.Card.CardId)))
                {
                    DAOFactory.StaticBuffDAO.Delete(bonusToDelete, CharacterId);
                }
                if (_isStaticBuffListInitial)
                {
                    foreach (Buff buff in Buff.Where(s => s.StaticBuff).ToArray())
                    {
                        StaticBuffDTO bf = new StaticBuffDTO
                        {
                            CharacterId = CharacterId,
                            RemainingTime = (int)(buff.RemainingTime - (DateTime.Now - buff.Start).TotalSeconds),
                            CardId = buff.Card.CardId
                        };
                        DAOFactory.StaticBuffDAO.InsertOrUpdate(ref bf);
                    }
                }
                DAOFactory.CharacterQuestDao.LoadByCharacterId(CharacterId).ToList().ForEach(q => DAOFactory.CharacterQuestDao.Delete(CharacterId, q.QuestId));
                Quests.ToList().ForEach(qst => DAOFactory.CharacterQuestDao.InsertOrUpdate(qst));

                foreach (StaticBonusDTO bonus in StaticBonusList.ToArray())
                {
                    StaticBonusDTO bonus2 = bonus;
                    DAOFactory.StaticBonusDAO.InsertOrUpdate(ref bonus2);
                }

                foreach (GeneralLogDTO general in GeneralLogs.GetAllItems())
                {
                    if (!DAOFactory.GeneralLogDAO.IdAlreadySet(general.LogId))
                    {
                        DAOFactory.GeneralLogDAO.Insert(general);
                    }
                }
                foreach (RespawnDTO Resp in Respawns)
                {
                    RespawnDTO res = Resp;
                    if (Resp.MapId != 0 && Resp.X != 0 && Resp.Y != 0)
                    {
                        DAOFactory.RespawnDAO.InsertOrUpdate(ref res);
                    }
                }
                Logger.LogUserEvent("CHARACTER_DB_SAVE", Session.GenerateIdentity(), "FINISH");
            }
            catch (Exception e)
            {
                Logger.LogUserEventError("CHARACTER_DB_SAVE", Session.GenerateIdentity(), "ERROR", e);
            }
        }

        public void SendGift(long id, short vnum, long amount, sbyte rare, byte upgrade, bool isNosmall)
        {
            Item it = ServerManager.GetItem(vnum);

            if (it != null)
            {
                if (it.ItemType != ItemType.Weapon && it.ItemType != ItemType.Armor && it.ItemType != ItemType.Specialist && it.ItemType != ItemType.Box)
                {
                    upgrade = 0;
                }
                else if (it.ItemType != ItemType.Weapon && it.ItemType != ItemType.Armor)
                {
                    rare = 0;
                }
                if (rare > 8 || rare < -2)
                {
                    rare = 0;
                }
                if (upgrade > 10 && (it.ItemType != ItemType.Specialist && it.ItemType != ItemType.Box))
                {
                    upgrade = 0;
                }
                else if (it.ItemType == ItemType.Specialist && upgrade > 15)
                {
                    upgrade = 0;
                }

                // maximum size of the amount is 999
                if (amount > 9999)
                {
                    amount = 9999;
                }

                MailDTO mail = new MailDTO
                {
                    AttachmentAmount = (short)(it.Type == InventoryType.Etc || it.Type == InventoryType.Main ? amount : 1),
                    IsOpened = false,
                    Date = DateTime.Now,
                    ReceiverId = id,
                    SenderId = CharacterId,
                    AttachmentRarity = (byte)rare,
                    AttachmentUpgrade = upgrade,
                    IsSenderCopy = false,
                    Title = isNosmall ? "NOSMALL" : Name,
                    AttachmentVNum = vnum,
                    SenderClass = Class,
                    SenderGender = Gender,
                    SenderHairColor = HairColor,
                    SenderHairStyle = HairStyle,
                    EqPacket = GenerateEqListForPacket(),
                    SenderMorphId = Morph == 0 ? (short)-1 : (short)(Morph > short.MaxValue ? 0 : Morph)
                };
                MailServiceClient.Instance.SendMail(mail);
            }
        }

        //public void SetContributi(long val)
        //{
        //    if (val != 0)
        //    {
        //        Contributi += (int)val;
        //        if (val > 0)
        //        {
        //            Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("CONTRIBUTI_INCREASED"), val), 11));
        //        }
        //        else
        //        {
        //            Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("CONTRIBUTI_DECREASED"), val), 12));
        //        }
        //    }
        //}

        public void SetReputation(short val)
        {
            if (val != 0)
            {
                double multiplier = 1;
                multiplier += (GetBuff(CardType.Reputation, (byte)AdditionalTypes.Reputation.ReputationIncreased / 10)[0] / 100D);
                StaticBonusDTO repDouble = Session.Character.StaticBonusList.Find(s =>
                                s.StaticBonusType == StaticBonusType.DoubleRep);
                if (repDouble != null)
                {
                    multiplier *= 2;
                }
                val = (short)(val * multiplier);
                Reputation += val;
                Session.SendPacket(GenerateFd());
                if (FirstReputLog == false)
                {
                    if (Reputation > 350000)
                    {
                        InsertReputationLog(Reputation);
                        FirstReputLog = true;
                    }
                }
                if (SecondReputLog == false)
                {
                    if (Reputation > 2500000)
                    {
                        InsertReputationLog(Reputation);
                        SecondReputLog = true;
                    }
                }
                if (ThirdReputLog == false)
                {
                    if (Reputation > 5000000)
                    {
                        InsertReputationLog(Reputation);
                        ThirdReputLog = true;
                    }
                }
                if (val > 0)
                {
                    Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("REPUT_INCREASE"), val), 11));
                }
                else
                {
                    Session.SendPacket(GenerateSay(string.Format(Language.Instance.GetMessageFromKey("REPUT_DECREASE"), val), 12));
                }
            }
        }

        public void SetRespawnPoint(short mapId, short mapX, short mapY)
        {
            if (Session.HasCurrentMapInstance && Session.CurrentMapInstance.Map.MapTypes.Count > 0)
            {
                long? respawnmaptype = Session.CurrentMapInstance.Map.MapTypes[0].RespawnMapTypeId;
                if (respawnmaptype != null)
                {
                    RespawnDTO resp = Respawns.Find(s => s.RespawnMapTypeId == respawnmaptype);
                    if (resp == null)
                    {
                        resp = new RespawnDTO { CharacterId = CharacterId, MapId = mapId, X = mapX, Y = mapY, RespawnMapTypeId = (long)respawnmaptype };
                        Respawns.Add(resp);
                    }
                    else
                    {
                        resp.X = mapX;
                        resp.Y = mapY;
                        resp.MapId = mapId;
                    }
                }
            }
        }

        public void SetReturnPoint(short mapId, short mapX, short mapY)
        {
            if (Session.HasCurrentMapInstance && Session.CurrentMapInstance.Map.MapTypes.Count > 0)
            {
                long? respawnmaptype = Session.CurrentMapInstance.Map.MapTypes[0].ReturnMapTypeId;
                if (respawnmaptype != null)
                {
                    RespawnDTO resp = Respawns.Find(s => s.RespawnMapTypeId == respawnmaptype);
                    if (resp == null)
                    {
                        resp = new RespawnDTO { CharacterId = CharacterId, MapId = mapId, X = mapX, Y = mapY, RespawnMapTypeId = (long)respawnmaptype };
                        Respawns.Add(resp);
                    }
                    else
                    {
                        resp.X = mapX;
                        resp.Y = mapY;
                        resp.MapId = mapId;
                    }
                }
            }
        }

        public void UpdateBushFire()
        {
            BrushFireJagged = BestFirstSearch.LoadBrushFireJagged(new GridPos
            {
                X = PositionX,
                Y = PositionY
            }, Session.CurrentMapInstance.Map.JaggedGrid);
        }

        public bool WeaponLoaded(CharacterSkill ski)
        {
            if (ski != null)
            {
                switch (Class)
                {
                    default:
                        return false;

                    case ClassType.Adventurer:
                        if (ski.Skill.Type == 1 && Inventory != null)
                        {
                            ItemInstance wearable = Inventory.LoadBySlotAndType((byte)EquipmentType.SecondaryWeapon, InventoryType.Wear);
                            if (wearable != null)
                            {
                                if (wearable.Ammo > 0)
                                {
                                    wearable.Ammo--;
                                    return true;
                                }
                                if (Inventory.CountItem(2081) < 1)
                                {
                                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_AMMO_ADVENTURER"), 10));
                                    return false;
                                }
                                Inventory.RemoveItemAmount(2081);
                                wearable.Ammo = 100;
                                Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("AMMO_LOADED_ADVENTURER"), 10));
                                return true;
                            }
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_WEAPON"), 10));
                            return false;
                        }
                        return true;

                    case ClassType.Swordman:
                        if (ski.Skill.Type == 1 && Inventory != null)
                        {
                            ItemInstance inv = Inventory.LoadBySlotAndType((byte)EquipmentType.SecondaryWeapon, InventoryType.Wear);
                            if (inv != null)
                            {
                                if (inv.Ammo > 0)
                                {
                                    inv.Ammo--;
                                    return true;
                                }
                                if (Inventory.CountItem(2082) < 1)
                                {
                                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_AMMO_SWORDSMAN"), 10));
                                    return false;
                                }

                                Inventory.RemoveItemAmount(2082);
                                inv.Ammo = 100;
                                Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("AMMO_LOADED_SWORDSMAN"), 10));
                                return true;
                            }
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_WEAPON"), 10));
                            return false;
                        }
                        return true;

                    case ClassType.Archer:
                        if (ski.Skill.Type == 1 && Inventory != null)
                        {
                            ItemInstance inv = Inventory.LoadBySlotAndType((byte)EquipmentType.MainWeapon, InventoryType.Wear);
                            if (inv != null)
                            {
                                if (inv.Ammo > 0)
                                {
                                    inv.Ammo--;
                                    return true;
                                }
                                if (Inventory.CountItem(2083) < 1)
                                {
                                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_AMMO_ARCHER"), 10));
                                    return false;
                                }

                                Inventory.RemoveItemAmount(2083);
                                inv.Ammo = 100;
                                Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("AMMO_LOADED_ARCHER"), 10));
                                return true;
                            }
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_WEAPON"), 10));
                            return false;
                        }
                        return true;

                    case ClassType.Magician:
                        return true;

                    case ClassType.Fighter:
                        if (ski.Skill.Type == 1 && Inventory != null)
                        {
                            ItemInstance inv = Inventory.LoadBySlotAndType((byte)EquipmentType.SecondaryWeapon, InventoryType.Wear);
                            if (inv != null)
                            {
                                if (inv.Ammo > 0)
                                {
                                    inv.Ammo--;
                                    return true;
                                }
                                if (Inventory.CountItem(2082) < 1)
                                {
                                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_AMMO_SWORDSMAN"), 10));
                                    return false;
                                }

                                Inventory.RemoveItemAmount(2082);
                                inv.Ammo = 100;
                                Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("AMMO_LOADED_SWORDSMAN"), 10));
                                return true;
                            }
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_WEAPON"), 10));
                            return false;
                        }
                        return true;
                }
            }

            return false;
        }

        internal void RefreshValidity()
        {
            if (StaticBonusList.RemoveAll(s => s.StaticBonusType == StaticBonusType.BackPack && s.DateEnd < DateTime.Now) > 0)
            {
                Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("ITEM_TIMEOUT"), 10));
                Session.SendPacket(GenerateExts());
            }

            if (StaticBonusList.RemoveAll(s => s.DateEnd < DateTime.Now) > 0)
            {
                Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("ITEM_TIMEOUT"), 10));
            }

            if (Inventory != null)
            {
                List<ItemInstance> coupons = Inventory.Where(s => s.Item.VNum == 1270);
                foreach (ItemInstance coupon in coupons)
                {
                    if (coupon?.DurabilityPoint > 0)
                    {
                        coupon.DurabilityPoint--;
                        if (coupon.DurabilityPoint == 0)
                        {
                            Inventory.DeleteById(coupon.Id);
                            Session.SendPacket(GenerateStatChar());
                            Session.CurrentMapInstance?.Broadcast(GenerateEq());
                            Session.SendPacket(GenerateEquipment());
                            Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("ITEM_TIMEOUT"), 10));
                        }
                    }
                }

                foreach (object suit in Enum.GetValues(typeof(EquipmentType)))
                {
                    ItemInstance item = Inventory.LoadBySlotAndType((byte)suit, InventoryType.Wear);
                    if (item?.DurabilityPoint > 0)
                    {
                        item.DurabilityPoint--;
                        if (item.DurabilityPoint == 0)
                        {
                            Inventory.DeleteById(item.Id);
                            Session.SendPacket(GenerateStatChar());
                            Session.CurrentMapInstance?.Broadcast(GenerateEq());
                            Session.SendPacket(GenerateEquipment());
                            Session.SendPacket(GenerateSay(Language.Instance.GetMessageFromKey("ITEM_TIMEOUT"), 10));
                        }
                    }
                }
            }
        }

        //void MaestriaXPCheck(long xp, ItemInstance weapon = null)
        //{
        //    if (weapon != null)
        //    {
        //        if (weapon.LivelloMaestria < 30)
        //        {
        //            weapon.XPMaestria += (long)(xp * (1 + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.EXPIncreased)[0] / 100D)));

        //            MaestriaArmiDTO weapondb = new MaestriaArmiDTO
        //            {
        //                AccountId = Session.Account.AccountId,
        //                ItemVNum = weapon.ItemVNum,
        //                XPMaestria = (short)weapon.XPMaestria,
        //                XPMaestriaTotale = (short)weapon.XPMaestria,
        //                LivelloMaestria = (short)weapon.LivelloMaestria,
        //            };
        //            MaestriaArmiDTO weapondb2 = DAOFactory.MaestriaArmiDAO.LoadByVnumAndCharacterId(weapon.ItemVNum, Session.Account.AccountId);
        //            if (weapondb2 != null)
        //            {
        //                weapondb2.XPMaestriaTotale += (long)(xp * (1 + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.EXPIncreased)[0] / 100D)));
        //                weapondb.XPMaestriaTotale = weapondb2.XPMaestriaTotale;
        //                weapondb.MaestriaArmiId = weapondb2.MaestriaArmiId;
        //            }
        //            DAOFactory.MaestriaArmiDAO.Update(weapondb);

        //            AccountDTO account = DAOFactory.AccountDAO.LoadById(Session.Account.AccountId);
        //            account.LivelloMaestriaXP += (long)(xp * (1 + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.EXPIncreased)[0] / 100D)));
        //            Session.Account.LivelloMaestriaXP = account.LivelloMaestriaXP;
        //            DAOFactory.AccountDAO.InsertOrUpdate(ref account);


        //            long esperienzamaestria = MaestriaXPLoad(Session.Account.LivelloMaestria);
        //            double experience = ArmaXPLoad((long)weapon.LivelloMaestria);

        //            while (Session.Account.LivelloMaestriaXP >= esperienzamaestria)
        //            {
        //                AccountDTO account2 = DAOFactory.AccountDAO.LoadById(Session.Account.AccountId);
        //                account2.LivelloMaestriaXP -= (int)esperienzamaestria;
        //                account2.LivelloMaestria++;
        //                Session.Account.LivelloMaestria = account2.LivelloMaestria;
        //                Session.Account.LivelloMaestriaXP = account2.LivelloMaestriaXP;
        //                DAOFactory.AccountDAO.InsertOrUpdate(ref account2);
        //                Session.SendPacket(UserInterfaceHelper.GenerateMsg("You went up to Maestria, current mastery : " + Session.Account.LivelloMaestria, 10));
        //                Session.SendPacket(UserInterfaceHelper.GenerateMsg("You went up to Mastery, current mastery : " + Session.Account.LivelloMaestria, 0));
        //            }
        //            while (weapon.XPMaestria >= experience)
        //            {
        //                weapon.XPMaestria -= (int)experience;
        //                weapon.LivelloMaestria++;
        //                if (weapon.Item.EquipmentSlot == EquipmentType.MainWeapon || weapon.Item.EquipmentSlot == EquipmentType.SecondaryWeapon)
        //                {
        //                    double increasedmin = weapon.Item.DamageMinimum / 100;
        //                    double increasedmax = weapon.Item.DamageMaximum / 100;
        //                    weapon.DamageMinimum += (short)increasedmin;
        //                    weapon.DamageMaximum += (short)increasedmax;
        //                }
        //                if (weapon.Item.EquipmentSlot == EquipmentType.Armor)
        //                {
        //                    double increasecd = weapon.Item.CloseDefence / 100;
        //                    double increasedd = weapon.Item.DistanceDefence / 100;
        //                    double increasemd = weapon.Item.MagicDefence / 100;
        //                    double increaseddo = weapon.Item.DefenceDodge / 100;
        //                    double increaseddd = weapon.Item.DistanceDefenceDodge / 100;

        //                    weapon.CloseDefence += (short)increasecd;
        //                    weapon.DistanceDefence += (short)increasedd;
        //                    weapon.MagicDefence += (short)increasemd;
        //                    weapon.DefenceDodge += (short)increaseddo;
        //                    weapon.DistanceDefenceDodge += (short)increaseddd;
        //                }
        //                if (weapon.LivelloMaestria == 30)
        //                {
        //                    weapon.XPMaestria = 0;
        //                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(weapon.Item.Name + " has reached the maximum level!", 10));
        //                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(weapon.Item.Name + " has reached the maximum level!", 0));
        //                }
        //                else
        //                {
        //                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(weapon.Item.Name + "Level up !", 10));
        //                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(weapon.Item.Name + " Level up current level: " + weapon.LivelloMaestria, 0));
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        ItemInstance primaryweapon = Inventory?.LoadBySlotAndType((byte)EquipmentType.MainWeapon, InventoryType.Wear);
        //        ItemInstance secondaryweapon = Inventory?.LoadBySlotAndType((byte)EquipmentType.SecondaryWeapon, InventoryType.Wear);
        //        ItemInstance armor = Inventory?.LoadBySlotAndType((byte)EquipmentType.Armor, InventoryType.Wear);
        //        if (primaryweapon != null)
        //        {
        //            MaestriaXPCheck(xp, primaryweapon);
        //        }
        //        if (secondaryweapon != null)
        //        {
        //            MaestriaXPCheck(xp, secondaryweapon);
        //        }
        //        if (armor != null)
        //        {
        //            MaestriaXPCheck(xp, armor);
        //        }

        //    }
        //}

        internal void SetSession(ClientSession clientSession) => Session = clientSession;

        //private void GenerateWeaponXp(MapMonster monster, ItemInstance weapon, bool isMonsterOwner)
        //{
        //    NpcMonster monsterinfo = monster.Monster;
        //    ItemInstance armor = Inventory?.LoadBySlotAndType((byte)EquipmentType.Armor, InventoryType.Wear);

        //    if (isMonsterOwner)
        //    {
        //        MaestriaXPCheck(monsterinfo.XPArma, weapon);
        //        if (armor != null)
        //        {
        //            MaestriaXPCheck(monsterinfo.XPArma / 2, armor);
        //        }

        //        if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.MappaExpInstance)
        //        {
        //            foreach (ClientSession s in Session.CurrentMapInstance.Sessions.Where(s => s.Character.CharacterId != CharacterId))
        //            {
        //                s.Character.MaestriaXPCheck(monsterinfo.XPArma / 5);
        //            }
        //        }
        //        else if (Group != null)
        //        {
        //            foreach (ClientSession s in Group.Characters.GetAllItems().Where(s => s.Character.MapInstance == Session.CurrentMapInstance))
        //            {
        //                s.Character.MaestriaXPCheck(monsterinfo.XPArma / 5);
        //            }
        //        }

        //    }
        //    else
        //    {
        //        MaestriaXPCheck(monsterinfo.XPArma / 3, weapon);
        //        if (armor != null)
        //        {
        //            MaestriaXPCheck(monsterinfo.XPArma / 6, armor);
        //        }

        //        if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.MappaExpInstance)
        //        {
        //            foreach (ClientSession s in Session.CurrentMapInstance.Sessions.Where(s => s.Character.CharacterId != CharacterId))
        //            {
        //                s.Character.MaestriaXPCheck(monsterinfo.XPArma / 5);
        //            }
        //        }
        //        else if (Group != null)
        //        {
        //            foreach (ClientSession s in Group.Characters.GetAllItems().Where(s => s.Character.MapInstance == Session.CurrentMapInstance))
        //            {
        //                s.Character.MaestriaXPCheck(monsterinfo.XPArma / 5);
        //            }
        //        }
        //    }
        //}

        private void GenerateXp(MapMonster monster, bool isMonsterOwner)
        {
            NpcMonster monsterinfo = monster.Monster;
            if (!Session.Account.PenaltyLogs.Any(s => s.Penalty == PenaltyType.BlockExp && s.DateEnd > DateTime.Now))
            {
                Group grp = ServerManager.Instance.Groups.Find(g => g.IsMemberOfGroup(CharacterId));
                ItemInstance specialist = null;
                if (Hp <= 0)
                {
                    return;
                }
                if ((int)(SwitchLevelXp() / (XpLoad() / 10)) < (int)((SwitchLevelXp() + monsterinfo.XP) / (XpLoad() / 10)))
                {
                    Hp = (int)HPLoad();
                    Mp = (int)MPLoad();
                    Session.SendPacket(GenerateStat());
                    Session.SendPacket(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 5));
                }

                if (Inventory != null)
                {
                    specialist = Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                }

                int xp;
                if (isMonsterOwner)
                {
                    xp = (int)(GetXP(monsterinfo, grp) * (1 + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.EXPIncreased)[0] / 100D)));
                }
                else
                {
                    xp = (int)(GetXP(monsterinfo, grp) / 3D * (1 + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.EXPIncreased)[0] / 100D)));
                }
                if (SwitchLevel() < ServerManager.Instance.Configuration.MaxLevel)
                {
                    AddLevelXp(xp);
                }

                foreach (Mate mate in Mates.Where(x => x.IsTeamMember))
                {
                    mate.GenerateXp(xp);
                }

                if ((Class == 0 && JobLevel < 20) || (Class != 0 && JobLevel < ServerManager.Instance.Configuration.MaxJobLevel))
                {
                    if (specialist != null && UseSp && specialist.SpLevel < ServerManager.Instance.Configuration.MaxSPLevel && specialist.SpLevel > 19)
                    {
                        JobLevelXp += (int)(GetJXP(monsterinfo, grp) / 2D * (1 + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.EXPIncreased)[0] / 100D)));
                    }
                    else
                    {
                        JobLevelXp += (int)(GetJXP(monsterinfo, grp) * (1 + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.EXPIncreased)[0] / 100D)));
                    }
                }
                if (specialist != null && UseSp && specialist.SpLevel < ServerManager.Instance.Configuration.MaxSPLevel)
                {
                    int multiplier = specialist.SpLevel < 10 ? 10 : specialist.SpLevel < 19 ? 5 : 1;
                    specialist.XP += (int)(GetJXP(monsterinfo, grp) * (multiplier + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.EXPIncreased)[0] / 100D)));
                }
                if (SwitchHeroLevel() > 0 && SwitchHeroLevel() < ServerManager.Instance.Configuration.MaxHeroLevel)
                {
                    if (isMonsterOwner)
                    {
                        AddHeroLevelXp((int)((GetHXP(monsterinfo, grp) / 50) * (1 + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.EXPIncreased)[0] / 100D))));
                    }
                    else
                    {
                        AddHeroLevelXp((int)((GetHXP(monsterinfo, grp) / 50) / 3D * (1 + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.EXPIncreased)[0] / 100D))));
                    }
                }
                double experience = XpLoad();
                while (SwitchLevelXp() >= experience)
                {
                    RemoveLevelXp((long)experience);
                    AddLevel(1);
                    experience = XpLoad();
                    if (SwitchLevel() == 55 || SwitchLevel() == 90 || SwitchLevel() == 99)
                    {
                        InsertLevelLog(SwitchLevel());
                    }
                    if (SwitchLevel() >= ServerManager.Instance.Configuration.MaxLevel)
                    {
                        SetLevel(ServerManager.Instance.Configuration.MaxLevel);
                        SetLevelXp(0);
                    }
                    Hp = (int)HPLoad();
                    Mp = (int)MPLoad();
                    Session.SendPacket(GenerateStat());
                    if (Family != null)
                    {
                        if (SwitchLevel() > 20 && SwitchLevel() % 10 == 0)
                        {
                            Family.InsertFamilyLog(FamilyLogType.LevelUp, Name, level: SwitchLevel());
                            Family.InsertFamilyLog(FamilyLogType.FamilyXP, Name, experience: 20 * SwitchLevel());
                            GenerateFamilyXp(20 * SwitchLevel());
                        }
                        else if (SwitchLevel() > 80)
                        {
                            Family.InsertFamilyLog(FamilyLogType.LevelUp, Name, level: SwitchLevel());
                        }
                        else
                        {
                            ServerManager.Instance.FamilyRefresh(Family.FamilyId);
                            CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                            {
                                DestinationCharacterId = Family.FamilyId,
                                SourceCharacterId = CharacterId,
                                SourceWorldId = ServerManager.Instance.WorldId,
                                Message = "fhis_stc",
                                Type = MessageType.Family
                            });
                        }
                    }
                    Session.SendPacket(GenerateLevelUp());
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("LEVELUP"), 0));
                    Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 6), PositionX, PositionY);
                    Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 198), PositionX, PositionY);
                    ServerManager.Instance.UpdateGroup(CharacterId);
                }

                ItemInstance fairy = Inventory?.LoadBySlotAndType((byte)EquipmentType.Fairy, InventoryType.Wear);
                if (fairy != null)
                {
                    if (fairy.ElementRate + fairy.Item.ElementRate < fairy.Item.MaxElementRate
                        && Level <= monsterinfo.Level + 15 && Level >= monsterinfo.Level - 15)
                    {
                        fairy.XP += monster.Monster.FairyXp * (long)(ServerManager.Instance.Configuration.RateFairyXP * (1 + (GetBuff(CardType.FairyXPIncrease, (byte)AdditionalTypes.FairyXPIncrease.IncreaseFairyXPPoints)[0] / 100D)));
                    }
                    experience = CharacterHelper.LoadFairyXPData(fairy.ElementRate + fairy.Item.ElementRate);
                    while (fairy.XP >= experience)
                    {
                        fairy.XP -= (int)experience;
                        fairy.ElementRate++;
                        if (fairy.ElementRate + fairy.Item.ElementRate == fairy.Item.MaxElementRate)
                        {
                            fairy.XP = 0;
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("FAIRYMAX"), fairy.Item.Name), 10));
                        }
                        else
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("FAIRY_LEVELUP"), fairy.Item.Name), 10));
                        }
                        Session.SendPacket(GeneratePairy());
                    }
                }

                experience = JobXPLoad();
                while (JobLevelXp >= experience)
                {
                    JobLevelXp -= (long)experience;
                    JobLevel++;
                    experience = JobXPLoad();
                    if (JobLevel >= 20 && Class == 0)
                    {
                        JobLevel = 20;
                        JobLevelXp = 0;
                    }
                    else if (JobLevel >= ServerManager.Instance.Configuration.MaxJobLevel)
                    {
                        JobLevel = ServerManager.Instance.Configuration.MaxJobLevel;
                        JobLevelXp = 0;
                    }
                    Hp = (int)HPLoad();
                    Mp = (int)MPLoad();
                    Session.SendPacket(GenerateStat());
                    Session.SendPacket(GenerateLevelUp());
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("JOB_LEVELUP"), 0));
                    LearnAdventurerSkill();
                    Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 8), PositionX, PositionY);
                    Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 198), PositionX, PositionY);
                }
                if (specialist != null)
                {
                    experience = SpXpLoad();

                    while (UseSp && specialist.XP >= experience)
                    {
                        specialist.XP -= (long)experience;
                        specialist.SpLevel++;
                        experience = SpXpLoad();
                        Session.SendPacket(GenerateStat());
                        Session.SendPacket(GenerateLevelUp());
                        if (specialist.SpLevel >= ServerManager.Instance.Configuration.MaxSPLevel)
                        {
                            specialist.SpLevel = ServerManager.Instance.Configuration.MaxSPLevel;
                            specialist.XP = 0;
                        }
                        LearnSPSkill();
                        Skills.ForEach(s => s.LastUse = DateTime.Now.AddDays(-1));
                        Session.SendPacket(GenerateSki());
                        Session.SendPackets(GenerateQuicklist());

                        Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SP_LEVELUP"), 0));
                        Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 8), PositionX, PositionY);
                        Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 198), PositionX, PositionY);
                    }
                }
                experience = HeroXPLoad();
                while (SwitchHeroLevelXp() >= experience)
                {
                    RemoveHeroLevelXp((long)experience);
                    AddHeroLevel(1);
                    experience = HeroXPLoad();
                    if (SwitchHeroLevel() >= ServerManager.Instance.Configuration.MaxHeroLevel)
                    {
                        SetHeroLevel(ServerManager.Instance.Configuration.MaxHeroLevel);
                        SetHeroLevelXp(0);
                    }
                    Hp = (int)HPLoad();
                    Mp = (int)MPLoad();
                    Session.SendPacket(GenerateStat());
                    Session.SendPacket(GenerateLevelUp());
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("HERO_LEVELUP"), 0));
                    Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 8), PositionX, PositionY);
                    Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, CharacterId, 198), PositionX, PositionY);
                }
                Session.SendPacket(GenerateLev());
            }
            Session.Character.GenerateCheck();
        }

        //public void GenerateFactionFam()
        //{
        //    if (Session.Character.Family != null)
        //    {
        //        if (Session.Character.Faction == FactionType.Demon)
        //        {
        //            Session.Character.Ach3 = 1;
        //        }
        //        else if (Session.Character.Faction == FactionType.Angel)
        //        {
        //            Session.Character.Ach3 = 2;
        //        }
        //        else
        //        {
        //            return;
        //        }

        //    }
        //    else
        //    {
        //        return;
        //    }
        //}

        public void GenerateCheck()
        {
            if (Session.Character.Authority == AuthorityType.User || (Session.Character.Authority == AuthorityType.BitchNiggerFaggot))
            {
                //if (Session.Character.MapId == 2102 && Session.Character.Level > 32)
                //{
                //    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                //    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                //    Session.SendPacket(
                //        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("BIG_LVL")}",
                //            10));
                //}
                //else if (Session.Character.MapId == 2103 && Session.Character.Level > 47)
                //{
                //    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                //    Session.SendPacket(
                //        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("BIG_LVL")}",
                //            10));
                //}
                //else if (Session.Character.MapId == 2014 && Session.Character.Level > 62)
                //{
                //    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                //    Session.SendPacket(
                //        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("BIG_LVL")}",
                //            10));
                //}
                //else if (Session.Character.MapId == 2104 && Session.Character.Level > 77)
                //{
                //    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                //    Session.SendPacket(
                //        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("BIG_LVL")}",
                //            10));
                //}
                //else if (Session.Character.MapId == 2105 && Session.Character.Level > 82)
                //{
                //    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                //    Session.SendPacket(
                //        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("BIG_LVL")}",
                //            10));
                //}

                if (Session.Character.MapId == 150 && Session.Character.Level < 55)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 229 && Session.Character.Level < 88)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 230 && Session.Character.Level < 88)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 231 && Session.Character.Level < 88)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 232 && Session.Character.Level < 88)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 233 && Session.Character.Level < 88)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 234 && Session.Character.Level < 88)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 235 && Session.Character.Level < 88)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 236 && Session.Character.Level < 88)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 3 && Session.Character.HeroLevel > 30)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 2 && Session.Character.HeroLevel < 30)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 13 && Session.Character.HeroLevel < 60)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 14 && Session.Character.HeroLevel > 120 && Session.Character.HeroLevel < 90)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 15 && Session.Character.HeroLevel > 150 && Session.Character.HeroLevel < 120)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 16 && Session.Character.HeroLevel > 180 && Session.Character.HeroLevel < 150)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }
                else if (Session.Character.MapId == 17 && Session.Character.HeroLevel > 210 && Session.Character.HeroLevel < 180)
                {
                    ServerManager.Instance.ChangeMap((Session.Character.CharacterId), 1, 77, 115);
                    Session.SendPacket(
                        Session.Character.GenerateSay($"{Language.Instance.GetMessageFromKey("NOT_FOR_YOUR_LEVEL")}",
                            10));
                }

                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }

        private int GetGold(MapMonster mapMonster)
        {
            double eqMultiplier = 1 + (GetBuff(CardType.Item, (byte)AdditionalTypes.Item.IncreaseEarnedGold)[0] / 100D);
            int Gold = mapMonster.Monster.Gold;
            int actMultiplier = Session?.CurrentMapInstance?.Map.MapTypes?.Any(s => s.MapTypeId == (short)MapTypeEnum.Act52) ?? false ? 5 : 1;
            if (Session?.CurrentMapInstance?.Map.MapTypes?.Any(s => s.MapTypeId == (short)MapTypeEnum.Act61 || s.MapTypeId == (short)MapTypeEnum.Act61a || s.MapTypeId == (short)MapTypeEnum.Act61d) == true)
            {
                actMultiplier = 3;
            }
            return (int)(Gold * ServerManager.Instance.Configuration.RateGold * actMultiplier * eqMultiplier);
        }

        private int GetHXP(NpcMonsterDTO monster, Group group)
        {
            int partySize = 1;
            float partyPenalty = 1f;

            if (group != null)
            {
                int levelSum = group.Characters.Sum(g => g.Character.SwitchLevel());
                partySize = group.CharacterCount;
                partyPenalty = (6f / partySize) / levelSum;
            }

            int heroXp = (int)Math.Round(monster.HeroXp * CharacterHelper.ExperiencePenalty(SwitchLevel(), monster.Level) * ServerManager.Instance.Configuration.RateHeroicXP * MapInstance.XpRate);

            // divide jobexp by multiplication of partyPenalty with level e.g. 57 * 0,014...
            if (partySize > 1 && group != null)
            {
                heroXp = (int)Math.Round(heroXp / (SwitchHeroLevel() * partyPenalty));
            }

            return heroXp;
        }

        private int GetJXP(NpcMonsterDTO monster, Group group)
        {
            int partySize = 1;
            double partyPenalty = 1d;

            if (group != null)
            {
                int levelSum = group.Characters.Sum(g => g.Character.JobLevel);
                partySize = group.CharacterCount;
                partyPenalty = (6f / partySize) / levelSum;
            }

            int jobxp = (int)Math.Round(monster.JobXP * CharacterHelper.ExperiencePenalty(JobLevel, monster.Level) * ServerManager.Instance.Configuration.RateXP * MapInstance.XpRate);

            if (partySize > 1 && group != null)
            {
                jobxp = (int)Math.Round(jobxp / (JobLevel * partyPenalty));
            }

            return jobxp;
        }

        /* private int GetXP(NpcMonsterDTO monster, Group group)
         {
             int levelDifference = SwitchLevel() - monster.Level;

             int xpcalculation = 0;
             if (levelDifference <= 5 && levelDifference >= -5)
             {
                 xpcalculation = monster.XP;
             }
             else if (levelDifference <= -6 && levelDifference >= -25)
             {
                 xpcalculation = (int)(monster.XP * 0.9);
             }
             else if (levelDifference >= 6 && levelDifference <= 25)
             {
                 xpcalculation = (int)(monster.XP * 0.9);
             }
             else if (levelDifference <= -26 && levelDifference >= -50)
             {
                 xpcalculation = (int)(monster.XP * 0.8);
             }
             else if (levelDifference >= 26 && levelDifference <= 50)
             {
                 xpcalculation = (int)(monster.XP * 0.8);
             }
             else
             {
                 xpcalculation = (int)(monster.XP * 0.7);
             }

             double multiplierprestige = 1;
             if (Prestigio == 1)
             {
                 multiplierprestige = 1.25;
             }
             if (Prestigio == 2)
             {
                 multiplierprestige = 1.5;
             }
             if (Prestigio == 3)
             {
                 multiplierprestige = 2;
             }
             if (Prestigio == 4)
             {
                 multiplierprestige = 2.5;
             }
             if (Prestigio == 5)
             {
                 multiplierprestige = 3;
             }
             if (Prestigio == 6)
             {
                 multiplierprestige = 3.5;
             }
             if (Prestigio == 7)
             {
                 multiplierprestige = 4;
             }
             if (Prestigio == 8)
             {
                 multiplierprestige = 4.5;
             }
             if (Prestigio >= 9)
             {
                 multiplierprestige = 5;
             }

             int xp = (int)(xpcalculation * ServerManager.Instance.Configuration.RateXP * MapInstance.XpRate * multiplierprestige);

             if (monster.Level >= 75)
             {
                 xp *= 2;
             }

             return xp;
         }*/

        private int GetXP(NpcMonsterDTO monster, Group group)
        {
            int partySize = 1;
            double partyPenalty = 1d;
            int levelDifference = Level - monster.Level;

            /*if (group != null)
            {
                int levelSum = group.Characters.Sum(g => g.Character.Level);
                partySize = group.CharacterCount;
                partyPenalty = (6f / partySize) / levelSum;
            }*/

            int xpcalculation = levelDifference < 5 ? monster.XP : monster.XP / 3 * 2;

            int xp = (int)Math.Round(xpcalculation * CharacterHelper.ExperiencePenalty(Level, monster.Level) * ServerManager.Instance.Configuration.RateXP * MapInstance.XpRate);

            if (Level <= 5 && levelDifference < -4)
            {
                xp += xp / 2;
            }
            if (monster.Level >= 75)
            {
                xp *= 2;
            }
            if (monster.Level >= 100)
            {
                xp *= 2;
                if (Level < 96)
                {
                    xp = 1;
                }
            }

            if (partySize > 1 && group != null)
            {
                xp = (int)Math.Round(xp / (Level * partyPenalty));
            }

            return xp;
        }

        private int HealthHPLoad()
        {
            List<ShellEffectDTO> effects = new List<ShellEffectDTO>();
            ItemInstance inv = Inventory.LoadBySlotAndType((byte)EquipmentType.Armor, InventoryType.Wear);
            if (inv?.ShellEffects != null)
            {
                effects.AddRange(inv.ShellEffects);
            }
            int GetShellArmorEffectValue(ShellArmorEffectType effectType)
            {
                return effects?.Where(s => s.Effect == (byte)effectType)?.OrderByDescending(s => s.Value)?.FirstOrDefault()?.Value ?? 0;
            }
            int extrabasic = 0;

            if (Skills != null)
            {
                if (Skills.ContainsKey(52))
                {
                    extrabasic += 20;
                }
                if (Skills.ContainsKey(53))
                {
                    extrabasic += 50;
                }
                if (Skills.ContainsKey(54))
                {
                    extrabasic += 100;
                }
                if (Skills.ContainsKey(55))
                {
                    extrabasic += 150;
                }
                if (Skills.ContainsKey(60))
                {
                    extrabasic += 200;
                }
            }

            int totsitting = CharacterHelper.HPHealth[(byte)Class] + extrabasic + CellonOptions.Where(s => s.Type == CellonOptionType.HPRestore).Sum(s => s.Value);
            double runaseduto = GetShellArmorEffectValue(ShellArmorEffectType.RecoveryHPOnRest) / 100D;
            totsitting = (int)(totsitting + (totsitting * runaseduto));

            if (IsSitting)
            {
                return totsitting;
            }

            int tot = 0;
            tot += CharacterHelper.HPHealth[(byte)Class] + extrabasic + CellonOptions.Where(s => s.Type == CellonOptionType.HPRestore).Sum(s => s.Value);
            double runadoppia = GetShellArmorEffectValue(ShellArmorEffectType.RecoveryHPOnRest) / 100D;
            runadoppia += GetShellArmorEffectValue(ShellArmorEffectType.RecoveryHP) / 100D;
            tot = (int)(tot + (tot * runadoppia));

            return (DateTime.Now - LastDefence).TotalSeconds > 4 ? tot : 0;
        }

        private int HealthMPLoad()
        {
            List<ShellEffectDTO> effects = new List<ShellEffectDTO>();
            ItemInstance inv = Inventory.LoadBySlotAndType((byte)EquipmentType.Armor, InventoryType.Wear);
            if (inv?.ShellEffects != null)
            {
                effects.AddRange(inv.ShellEffects);
            }
            int GetShellArmorEffectValue(ShellArmorEffectType effectType)
            {
                return effects?.Where(s => s.Effect == (byte)effectType)?.OrderByDescending(s => s.Value)?.FirstOrDefault()?.Value ?? 0;
            }
            int extrabasic = 0;

            if (Skills != null)
            {
                if (Skills.ContainsKey(56))
                {
                    extrabasic += 20;
                }
                if (Skills.ContainsKey(57))
                {
                    extrabasic += 50;
                }
                if (Skills.ContainsKey(58))
                {
                    extrabasic += 100;
                }
                if (Skills.ContainsKey(59))
                {
                    extrabasic += 150;
                }
            }

            int totsitting = CharacterHelper.MPHealth[(byte)Class] + extrabasic + CellonOptions.Where(s => s.Type == CellonOptionType.MPRestore).Sum(s => s.Value);
            double runaseduto = GetShellArmorEffectValue(ShellArmorEffectType.RecoveryMPOnRest) / 100D;
            totsitting = (int)(totsitting + (totsitting * runaseduto));

            if (IsSitting)
            {
                return totsitting;
            }

            int tot = 0;
            tot += CharacterHelper.MPHealth[(byte)Class] + extrabasic + CellonOptions.Where(s => s.Type == CellonOptionType.MPRestore).Sum(s => s.Value);
            double runadoppia = GetShellArmorEffectValue(ShellArmorEffectType.RecoveryMPOnRest) / 100D;
            runadoppia += GetShellArmorEffectValue(ShellArmorEffectType.RecoveryMP) / 100D;
            tot = (int)(tot + (tot * runadoppia));

            return (DateTime.Now - LastDefence).TotalSeconds > 4 ? tot : 0;
        }

        private double ArmaXPLoad(long livello) => CharacterHelper.ArmaXpData[livello - 1];

        //public long MaestriaXPLoad(long livello) => CharacterHelper.XpMaestria[livello - 1];

        private long HeroXPLoad() => (long)SwitchHeroLevel() == 0 ? 1 : (long)CharacterHelper.HeroXpData[SwitchHeroLevel() - 1];

        private double JobXPLoad() => Class == (byte)ClassType.Adventurer ? CharacterHelper.FirstJobXPData[JobLevel - 1] : CharacterHelper.SecondJobXPData[JobLevel - 1];

        public double SpXpLoad()
        {
            ItemInstance specialist = null;
            if (Inventory != null)
            {
                specialist = Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
            }
            return specialist != null ? CharacterHelper.SPXPData[specialist.SpLevel == 0 ? 0 : specialist.SpLevel - 1] : 0;
        }

        private double XpLoad() => CharacterHelper.XPData[SwitchLevel() - 1];

        #endregion
    }
}