﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace OpenNos.GameObject
{
    public class ArcobalenoTeamMember
    {
        public ClientSession Session { get; set; }
        public ArcobalenoTeamType ArcobalenoTeamType { get; set; }
        public bool Dead { get; set; }
        public bool Freezed { get; set; }

        public ArcobalenoTeamMember(ClientSession session, ArcobalenoTeamType arcobalenoteamtype)
        {
            Session = session;
            ArcobalenoTeamType = arcobalenoteamtype;
        }
    }
}