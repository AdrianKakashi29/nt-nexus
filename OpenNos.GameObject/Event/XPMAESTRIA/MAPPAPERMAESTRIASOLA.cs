﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.GameObject.Networking;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;
using System.Linq;
using System.Collections.Concurrent;
using OpenNos.Data;
using OpenNos.PathFinder;

namespace OpenNos.GameObject.Event
{
    public class MAPPAPERMAESTRIASOLA
    {
        #region Methods
        public static void GenerateMappaperMaestriaSola()
        {
            long groupid = 0;
            int seconds = 0;
            IDisposable obs = Observable.Interval(TimeSpan.FromSeconds(1)).Subscribe(start2 =>
            {
                ServerManager.Instance.MapXpMembersSolo.Where(s => s.ArenaType == EventType.MAPPAXPMAESTRIASOLO).ToList().ForEach(s =>
                {
                    s.Time -= 1;
                    List<long> groupids = new List<long>();
                    ServerManager.Instance.MapXpMembersSolo.Where(o => o.GroupId != null).ToList().ForEach(o =>
                    {
                        if (ServerManager.Instance.MapXpMembersSolo.Count(g => g.GroupId == o.GroupId) != 1)
                        {
                            return;
                        }
                        if (o.GroupId != null)
                        {
                            groupids.Add(o.GroupId.Value);
                        }
                    });

                    if (s.Time > 0)
                    {
                        if (s.GroupId == null)
                        {
                            groupid++;
                            s.GroupId = groupid;
                            ServerManager.Instance.MapXpMembersSolo.Where(e => e.ArenaType == EventType.MAPPAXPMAESTRIASOLO && e.GroupId == s.GroupId).ToList().ForEach(o =>
                            {
                                o.Session.SendPacket(o.Session.Character.GenerateBsInfo(1, 2, -1, 6));
                                o.Session.SendPacket(o.Session.Character.GenerateSay("Team Trovato", 10));
                                Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(time =>
                                {
                                    s.Time = 300;
                                    if (ServerManager.Instance.MapXpMembersSolo.Count(g => g.GroupId == s.GroupId) < 1)
                                    {
                                        o.Session.SendPacket(o.Session.Character.GenerateBsInfo(0, 2, s.Time, 8));
                                        o.Session.SendPacket(o.Session.Character.GenerateSay("Cerco Team", 10));
                                    }
                                });
                            });

                        }
                        else
                        {
                            if (ServerManager.Instance.MapXpMembersSolo.Count(g => g.GroupId == s.GroupId) != 1)
                            {
                                return;
                            }

                            MapXpMember member = ServerManager.Instance.MapXpMembersSolo.FirstOrDefault(o => o.GroupId != null && groupids.Contains(o.GroupId.Value));
                            if (member == null)
                            {
                                return;
                            }

                            MapInstance map = ServerManager.GenerateMapInstance(405, MapInstanceType.MappaExpInstanceSolo, new InstanceBag());
                            MapInstance map2 = ServerManager.GenerateMapInstance(405, MapInstanceType.MappaExpInstanceSolo, new InstanceBag());
                            MapInstance map3 = ServerManager.GenerateMapInstance(405, MapInstanceType.MappaExpInstanceSolo, new InstanceBag());
                            MapInstance map4 = ServerManager.GenerateMapInstance(405, MapInstanceType.MappaExpInstanceSolo, new InstanceBag());
                            MapInstance map5 = ServerManager.GenerateMapInstance(405, MapInstanceType.MappaExpInstanceSolo, new InstanceBag());
                            MapInstance map6 = ServerManager.GenerateMapInstance(405, MapInstanceType.MappaExpInstanceSolo, new InstanceBag());
                            MapInstance map7 = ServerManager.GenerateMapInstance(405, MapInstanceType.MappaExpInstanceSolo, new InstanceBag());
                            MapInstance map8 = ServerManager.GenerateMapInstance(405, MapInstanceType.MappaExpInstanceSolo, new InstanceBag());
                            MapInstance map9 = ServerManager.GenerateMapInstance(405, MapInstanceType.MappaExpInstanceSolo, new InstanceBag());
                            MapInstance map10 = ServerManager.GenerateMapInstance(405, MapInstanceType.MappaExpInstanceSolo, new InstanceBag());
                            MapInstance map11 = ServerManager.GenerateMapInstance(405, MapInstanceType.MappaExpInstanceSolo, new InstanceBag());
                            MapInstance map12 = ServerManager.GenerateMapInstance(405, MapInstanceType.MappaExpInstanceSolo, new InstanceBag());

                            ConcurrentBag<MapXpTeamMember> mapxpTeam = new ConcurrentBag<MapXpTeamMember>();
                            ServerManager.Instance.MapXpTeams.Add(mapxpTeam);

                            MapXpMember[] arenamembers = ServerManager.Instance.MapXpMembersSolo.Where(o => o.GroupId == member.GroupId || o.GroupId == s.GroupId).OrderBy(o => o.GroupId)
                                .ToArray();

                            arenamembers.ToList().ForEach(o =>
                            {
                                o.Session.SendPacket(o.Session.Character.GenerateBsInfo(0, 2, s.Time, 2));

                                Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(time =>
                                {
                                    o.Session.SendPacket("ta_close");
                                    Observable.Timer(TimeSpan.FromSeconds(5)).Subscribe(time2 =>
                                    {
                                        o.Session.Character.GeneralLogs.Add(new GeneralLogDTO
                                        {
                                            AccountId = o.Session.Account.AccountId,
                                            CharacterId = o.Session.Character.CharacterId,
                                            IpAddress = o.Session.IpAddress,
                                            LogData = "Entry",
                                            LogType = "MappaXpMaestria Sola",
                                            Timestamp = DateTime.Now
                                        });
                                        o.Session.Character.Buff.ClearAll();
                                        int i = Array.IndexOf(arenamembers, o) + 1;
                                        o.Session.Character.Hp = (int)o.Session.Character.HPLoad();
                                        o.Session.Character.Mp = (int)o.Session.Character.MPLoad();
                                        MapCell celltp = map.Map.GetRandomPosition();
                                        ServerManager.Instance.ChangeMapInstance(o.Session.Character.CharacterId, map.MapInstanceId, Convert.ToInt16(celltp.X), Convert.ToInt16(celltp.Y));
                                        o.Session.Character.LoadSpeed();
                                        o.Session.SendPacket(UserInterfaceHelper.GenerateGuri(15, 1, o.Session.Character.CharacterId));
                                        Thread.Sleep(10000);

                                        short goldguadagno = 0;
                                        short guadagnorep = 0;
                                        short numeroperf = 0;
                                        short itemvnumroot2 = 0;
                                        short amountroot2 = 0;

                                        int amount = 150;
                                        while (map?.Sessions?.Any() == true)
                                        {
                                            try
                                            {
                                                if (map != null)
                                                {
                                                    if (map.Monsters.Count == 0 && amount <= 50 && map.Portals.Count == 0)
                                                    {
                                                        MapCell cellportal = map.Map.GetRandomPosition();
                                                        MapCell destination = map2.Map.GetRandomPosition();
                                                        Portal portale = new Portal { SourceMapId = map.Map.MapId, SourceX = cellportal.X, SourceY = cellportal.Y, DestinationMapInstanceId = map2.MapInstanceId, DestinationX = destination.X, DestinationY = destination.Y, Type = (short)PortalType.Open };
                                                        map.Portals.Add(portale);
                                                        map.Broadcast(portale.GenerateGp());

                                                    }
                                                    MapCell cell = map.Map.GetRandomPosition();

                                                    int monsterid = map.GetNextMonsterId();
                                                    short x = cell.X;
                                                    short y = cell.Y;
                                                    int randommonster = ServerManager.RandomNumber(0, 6);
                                                    short numeromob = 1020;
                                                    switch (randommonster)
                                                    {
                                                        case 0: numeromob = 1020; break;
                                                        case 1: numeromob = 1021; break;
                                                        case 2: numeromob = 1022; break;
                                                        case 3: numeromob = 1023; break;
                                                        case 4: numeromob = 1027; break;
                                                        case 5: numeromob = 1031; break;

                                                    }
                                                    MapMonster monster = new MapMonster { MonsterVNum = numeromob, MapX = x, MapY = y, MapMonsterId = monsterid, IsHostile = true, IsMoving = true, ShouldRespawn = false };
                                                    monster.Initialize(map);
                                                    map.AddMonster(monster);
                                                    map.Broadcast(monster.GenerateIn());
                                                }
                                            Thread.Sleep(60000 / amount);
                                            amount--;
                                            }
                                            catch (Exception ex)
                                            {

                                            }

                                        }
                                        amount = 175;
                                        while (map2?.Sessions?.Any() == true)
                                        {
                                            try
                                            {
                                                if (map2 != null)
                                                {
                                                    if (map2.Monsters.Count == 0 && amount <= 50 && map2.Portals.Count == 0)
                                                    {
                                                        MapCell cellportal = map2.Map.GetRandomPosition();
                                                        MapCell destination = map3.Map.GetRandomPosition();
                                                        Portal portale = new Portal { SourceMapId = map2.Map.MapId, SourceX = cellportal.X, SourceY = cellportal.Y, DestinationMapInstanceId = map3.MapInstanceId, DestinationX = destination.X, DestinationY = destination.Y, Type = (short)PortalType.Open };
                                                        map2.Portals.Add(portale);
                                                        map2.Broadcast(portale.GenerateGp());

                                                        int random = ServerManager.RandomNumber(0, 100);
                                                        if (random < 5)
                                                        {
                                                            goldguadagno = 100;
                                                            guadagnorep = 5000;
                                                            numeroperf = 25;
                                                        }
                                                        else if (random < 20)
                                                        {
                                                            goldguadagno = 50;
                                                            guadagnorep = 2500;
                                                            numeroperf = 20;
                                                        }
                                                        else if (random < 40)
                                                        {
                                                            goldguadagno = 25;
                                                            guadagnorep = 1000;
                                                            numeroperf = 15;
                                                        }
                                                        else if (random < 65)
                                                        {
                                                            goldguadagno = 10;
                                                            guadagnorep = 500;
                                                            numeroperf = 10;
                                                        }
                                                        else
                                                        {
                                                            goldguadagno = 5;
                                                            guadagnorep = 250;
                                                            numeroperf = 5;
                                                        }
                                                        int sceltarandom = ServerManager.RandomNumber(0, 3);
                                                        short perfezione = (short)ServerManager.RandomNumber(2514, 2518);

                                                        foreach (ClientSession sess in map2.Sessions)
                                                        {
                                                            switch (sceltarandom)
                                                            {
                                                                case 0:
                                                                    sess.Character.GiftAdd(2229, goldguadagno);
                                                                    break;
                                                                case 1:
                                                                    sess.Character.SetReputation(guadagnorep);
                                                                    break;
                                                                case 2:
                                                                    sess.Character.GiftAdd(perfezione, numeroperf);
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                    MapCell cell = map2.Map.GetRandomPosition();

                                                    int monsterid = map2.GetNextMonsterId();
                                                    short x = cell.X;
                                                    short y = cell.Y;
                                                    int randommonster = ServerManager.RandomNumber(0, 6);
                                                    short numeromob = 1020;
                                                    switch (randommonster)
                                                    {
                                                        case 0: numeromob = 1020; break;
                                                        case 1: numeromob = 1021; break;
                                                        case 2: numeromob = 1022; break;
                                                        case 3: numeromob = 1023; break;
                                                        case 4: numeromob = 1027; break;
                                                        case 5: numeromob = 1031; break;

                                                    }
                                                    MapMonster monster = new MapMonster { MonsterVNum = numeromob, MapX = x, MapY = y, MapMonsterId = monsterid, IsHostile = true, IsMoving = true, ShouldRespawn = false };
                                                    monster.Initialize(map2);
                                                    map2.AddMonster(monster);
                                                    map2.Broadcast(monster.GenerateIn());
                                                }
                                            Thread.Sleep(60000 / amount);
                                            amount--;
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                        amount = 200;
                                        while (map3?.Sessions?.Any() == true)
                                        {
                                            try
                                            {
                                                if (map3 != null)
                                                {
                                                    if (map3.Monsters.Count == 0 && amount <= 50 && map3.Portals.Count == 0)
                                                    {
                                                        MapCell cellportal = map3.Map.GetRandomPosition();
                                                        MapCell destination = map4.Map.GetRandomPosition();
                                                        Portal portale = new Portal { SourceMapId = map3.Map.MapId, SourceX = cellportal.X, SourceY = cellportal.Y, DestinationMapInstanceId = map4.MapInstanceId, DestinationX = destination.X, DestinationY = destination.Y, Type = (short)PortalType.Open };
                                                        map3.Portals.Add(portale);
                                                        map3.Broadcast(portale.GenerateGp());
                                                    }
                                                    MapCell cell = map3.Map.GetRandomPosition();

                                                    int monsterid = map3.GetNextMonsterId();
                                                    short x = cell.X;
                                                    short y = cell.Y;
                                                    MapMonster monster = new MapMonster { MonsterVNum = (short)(ServerManager.RandomNumber(1133, 1139)), MapX = x, MapY = y, MapMonsterId = monsterid, IsHostile = true, IsMoving = true, ShouldRespawn = false };
                                                    monster.Initialize(map3);
                                                    map3.AddMonster(monster);
                                                    map3.Broadcast(monster.GenerateIn());
                                                }
                                            Thread.Sleep(60000 / amount);
                                            amount--;
                                            }
                                            catch (Exception ex)
                                            {

                                            }

                                        }
                                        amount = 225;
                                        while (map4?.Sessions?.Any() == true)
                                        {
                                            try
                                            {
                                                if (map4 != null)
                                                {
                                                    if (map4.Monsters.Count == 0 && amount <= 50 && map4.Portals.Count == 0)
                                                    {
                                                        MapCell cellportal = map4.Map.GetRandomPosition();
                                                        MapCell destination = map5.Map.GetRandomPosition();
                                                        Portal portale = new Portal { SourceMapId = map4.Map.MapId, SourceX = cellportal.X, SourceY = cellportal.Y, DestinationMapInstanceId = map5.MapInstanceId, DestinationX = destination.X, DestinationY = destination.Y, Type = (short)PortalType.Open };
                                                        map4.Portals.Add(portale);
                                                        map4.Broadcast(portale.GenerateGp());

                                                        int random = ServerManager.RandomNumber(0, 100);
                                                        if (random < 5)
                                                        {
                                                            goldguadagno = 250;
                                                            guadagnorep = 10000;
                                                            numeroperf = 50;
                                                        }
                                                        else if (random < 20)
                                                        {
                                                            goldguadagno = 150;
                                                            guadagnorep = 7500;
                                                            numeroperf = 30;
                                                        }
                                                        else if (random < 40)
                                                        {
                                                            goldguadagno = 100;
                                                            guadagnorep = 5000;
                                                            numeroperf = 25;
                                                        }
                                                        else if (random < 65)
                                                        {
                                                            goldguadagno = 50;
                                                            guadagnorep = 2500;
                                                            numeroperf = 20;
                                                        }
                                                        else
                                                        {
                                                            goldguadagno = 25;
                                                            guadagnorep = 1000;
                                                            numeroperf = 15;
                                                        }
                                                        int sceltarandom = ServerManager.RandomNumber(0, 3);
                                                        short perfezione = (short)ServerManager.RandomNumber(2514, 2518);

                                                        foreach (ClientSession sess in map4.Sessions)
                                                        {
                                                            switch (sceltarandom)
                                                            {
                                                                case 0:
                                                                    sess.Character.GiftAdd(2229, goldguadagno);
                                                                    break;
                                                                case 1:
                                                                    sess.Character.SetReputation(guadagnorep);
                                                                    break;
                                                                case 2:
                                                                    sess.Character.GiftAdd(perfezione, numeroperf);
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                    MapCell cell = map4.Map.GetRandomPosition();

                                                    int monsterid = map4.GetNextMonsterId();
                                                    short x = cell.X;
                                                    short y = cell.Y;
                                                    MapMonster monster = new MapMonster { MonsterVNum = (short)(ServerManager.RandomNumber(1133, 1139)), MapX = x, MapY = y, MapMonsterId = monsterid, IsHostile = true, IsMoving = true, ShouldRespawn = false };
                                                    monster.Initialize(map4);
                                                    map4.AddMonster(monster);
                                                    map4.Broadcast(monster.GenerateIn());
                                                }
                                            Thread.Sleep(60000 / amount);
                                            amount--;
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                        amount = 250;
                                        while (map5?.Sessions?.Any() == true)
                                        {
                                            try
                                            {
                                                if (map5 != null)
                                                {
                                                    if (map5.Monsters.Count == 0 && amount <= 50 && map5.Portals.Count == 0)
                                                    {
                                                        MapCell cellportal = map5.Map.GetRandomPosition();
                                                        MapCell destination = map6.Map.GetRandomPosition();
                                                        Portal portale = new Portal { SourceMapId = map5.Map.MapId, SourceX = cellportal.X, SourceY = cellportal.Y, DestinationMapInstanceId = map6.MapInstanceId, DestinationX = destination.X, DestinationY = destination.Y, Type = (short)PortalType.Open };
                                                        map5.Portals.Add(portale);
                                                        map5.Broadcast(portale.GenerateGp());
                                                    }
                                                    MapCell cell = map5.Map.GetRandomPosition();

                                                    int monsterid = map5.GetNextMonsterId();
                                                    short x = cell.X;
                                                    short y = cell.Y;
                                                    MapMonster monster = new MapMonster { MonsterVNum = (short)(ServerManager.RandomNumber(1146, 1152)), MapX = x, MapY = y, MapMonsterId = monsterid, IsHostile = true, IsMoving = true, ShouldRespawn = false };
                                                    monster.Initialize(map5);
                                                    map5.AddMonster(monster);
                                                    map5.Broadcast(monster.GenerateIn());
                                                }
                                            Thread.Sleep(60000 / amount);
                                            amount--;
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                        amount = 275;
                                        while (map6?.Sessions?.Any() == true)
                                        {
                                            try
                                            {
                                                if (map6 != null)
                                                {
                                                    if (map6.Monsters.Count == 0 && amount <= 50 && map6.Portals.Count == 0)
                                                    {
                                                        MapCell cellportal = map6.Map.GetRandomPosition();
                                                        MapCell destination = map7.Map.GetRandomPosition();
                                                        Portal portale = new Portal { SourceMapId = map6.Map.MapId, SourceX = cellportal.X, SourceY = cellportal.Y, DestinationMapInstanceId = map7.MapInstanceId, DestinationX = destination.X, DestinationY = destination.Y, Type = (short)PortalType.Open };
                                                        map6.Portals.Add(portale);
                                                        map6.Broadcast(portale.GenerateGp());

                                                        int random = ServerManager.RandomNumber(0, 100);
                                                        if (random < 5)
                                                        {
                                                            goldguadagno = 500;
                                                            guadagnorep = 20000;
                                                            numeroperf = 100;
                                                        }
                                                        else if (random < 20)
                                                        {
                                                            goldguadagno = 300;
                                                            guadagnorep = 12500;
                                                            numeroperf = 85;
                                                        }
                                                        else if (random < 40)
                                                        {
                                                            goldguadagno = 250;
                                                            guadagnorep = 10000;
                                                            numeroperf = 65;
                                                        }
                                                        else if (random < 65)
                                                        {
                                                            goldguadagno = 150;
                                                            guadagnorep = 7500;
                                                            numeroperf = 45;
                                                        }
                                                        else
                                                        {
                                                            goldguadagno = 100;
                                                            guadagnorep = 5000;
                                                            numeroperf = 25;
                                                        }
                                                        int sceltarandom = ServerManager.RandomNumber(0, 3);
                                                        short perfezione = (short)ServerManager.RandomNumber(2514, 2518);

                                                        foreach (ClientSession sess in map6.Sessions)
                                                        {
                                                            switch (sceltarandom)
                                                            {
                                                                case 0:
                                                                    sess.Character.GiftAdd(2229, goldguadagno);
                                                                    break;
                                                                case 1:
                                                                    sess.Character.SetReputation(guadagnorep);
                                                                    break;
                                                                case 2:
                                                                    sess.Character.GiftAdd(perfezione, numeroperf);
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                    MapCell cell = map6.Map.GetRandomPosition();

                                                    int monsterid = map6.GetNextMonsterId();
                                                    short x = cell.X;
                                                    short y = cell.Y;
                                                    MapMonster monster = new MapMonster { MonsterVNum = (short)(ServerManager.RandomNumber(1146, 1152)), MapX = x, MapY = y, MapMonsterId = monsterid, IsHostile = true, IsMoving = true, ShouldRespawn = false };
                                                    monster.Initialize(map6);
                                                    map6.AddMonster(monster);
                                                    map6.Broadcast(monster.GenerateIn());
                                                }
                                            Thread.Sleep(60000 / amount);
                                            amount--;
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                        amount = 300;
                                        while (map7?.Sessions?.Any() == true)
                                        {
                                            try
                                            {
                                                if (map7 != null)
                                                {
                                                    if (map7.Monsters.Count == 0 && amount <= 50 && map7.Portals.Count == 0)
                                                    {
                                                        MapCell cellportal = map7.Map.GetRandomPosition();
                                                        MapCell destination = map8.Map.GetRandomPosition();
                                                        Portal portale = new Portal { SourceMapId = map7.Map.MapId, SourceX = cellportal.X, SourceY = cellportal.Y, DestinationMapInstanceId = map8.MapInstanceId, DestinationX = destination.X, DestinationY = destination.Y, Type = (short)PortalType.Open };
                                                        map7.Portals.Add(portale);
                                                        map7.Broadcast(portale.GenerateGp());
                                                    }
                                                    MapCell cell = map7.Map.GetRandomPosition();

                                                    int monsterid = map7.GetNextMonsterId();
                                                    short x = cell.X;
                                                    short y = cell.Y;
                                                    MapMonster monster = new MapMonster { MonsterVNum = (short)(ServerManager.RandomNumber(1100, 1104)), MapX = x, MapY = y, MapMonsterId = monsterid, IsHostile = true, IsMoving = true, ShouldRespawn = false };
                                                    monster.Initialize(map7);
                                                    map7.AddMonster(monster);
                                                    map7.Broadcast(monster.GenerateIn());
                                                }
                                            Thread.Sleep(60000 / amount);
                                            amount--;
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                        amount = 325;
                                        while (map8?.Sessions?.Any() == true)
                                        {
                                            try
                                            {
                                                if (map8 != null)
                                                {
                                                    if (map8.Monsters.Count == 0 && amount <= 50 && map8.Portals.Count == 0)
                                                    {
                                                        MapCell cellportal = map8.Map.GetRandomPosition();
                                                        MapCell destination = map9.Map.GetRandomPosition();
                                                        Portal portale = new Portal { SourceMapId = map8.Map.MapId, SourceX = cellportal.X, SourceY = cellportal.Y, DestinationMapInstanceId = map9.MapInstanceId, DestinationX = destination.X, DestinationY = destination.Y, Type = (short)PortalType.Open };
                                                        map8.Portals.Add(portale);
                                                        map8.Broadcast(portale.GenerateGp());

                                                        double random = ServerManager.RandomNumber(1, 100);
                                                        if (random <= 12.65)
                                                        {
                                                            itemvnumroot2 = 2229;
                                                            amountroot2 = 500;
                                                        }
                                                        else if (random <= 25.3)
                                                        {
                                                            itemvnumroot2 = (short)(ServerManager.RandomNumber(2514, 2518));
                                                            amountroot2 = 100;
                                                        }
                                                        else if (random < 37.95)
                                                        {
                                                            itemvnumroot2 = (short)(ServerManager.RandomNumber(2518, 2521));
                                                            amountroot2 = 100;
                                                        }
                                                        else if (random < 50.6)
                                                        {
                                                            itemvnumroot2 = 9999;
                                                            amountroot2 = 20;
                                                        }
                                                        else if (random < 63.25)
                                                        {
                                                            itemvnumroot2 = 2282;
                                                            amountroot2 = 500;
                                                        }
                                                        else if (random < 75.9)
                                                        {
                                                            itemvnumroot2 = 1286;
                                                            amountroot2 = 1;
                                                        }
                                                        else if (random < 78.11)
                                                        {
                                                            itemvnumroot2 = 2229;
                                                            amountroot2 = 999;
                                                        }
                                                        else if (random < 80.32)
                                                        {
                                                            itemvnumroot2 = (short)(ServerManager.RandomNumber(2514, 2518));
                                                            amountroot2 = 200;
                                                        }
                                                        else if (random < 82.53)
                                                        {
                                                            itemvnumroot2 = (short)(ServerManager.RandomNumber(2518, 2521));
                                                            amountroot2 = 100;
                                                        }
                                                        else if (random < 84.74)
                                                        {
                                                            itemvnumroot2 = 9999;
                                                            amountroot2 = 40;
                                                        }
                                                        else if (random < 86.95)
                                                        {
                                                            itemvnumroot2 = 2282;
                                                            amountroot2 = 999;
                                                        }
                                                        else if (random < 89.16)
                                                        {
                                                            itemvnumroot2 = 5499;
                                                            amountroot2 = 1;
                                                        }
                                                        else if (random < 91.37)
                                                        {
                                                            itemvnumroot2 = 5498;
                                                            amountroot2 = 1;
                                                        }
                                                        else if (random < 93.38)
                                                        {
                                                            itemvnumroot2 = 5560;
                                                            amountroot2 = 1;
                                                        }
                                                        else if (random < 95.39)
                                                        {
                                                            itemvnumroot2 = (short)(ServerManager.RandomNumber(583, 586)); ;
                                                            amountroot2 = 1;
                                                        }
                                                        else if (random < 97.4)
                                                        {
                                                            itemvnumroot2 = (short)(ServerManager.RandomNumber(571, 574)); ;
                                                            amountroot2 = 1;
                                                        }
                                                        else
                                                        {
                                                            itemvnumroot2 = 1286;
                                                            amountroot2 = 3;
                                                        }


                                                        foreach (ClientSession sess in map8.Sessions)
                                                        {
                                                            if (itemvnumroot2 == 9999)
                                                            {
                                                                sess.Character.SetReputation((short)(amountroot2 * 1000));
                                                            }
                                                            else
                                                            {
                                                                sess.Character.GiftAdd(itemvnumroot2, amountroot2);
                                                            }
                                                        }
                                                    }
                                                    MapCell cell = map8.Map.GetRandomPosition();

                                                    int monsterid = map8.GetNextMonsterId();
                                                    short x = cell.X;
                                                    short y = cell.Y;
                                                    MapMonster monster = new MapMonster { MonsterVNum = (short)(ServerManager.RandomNumber(1100, 1104)), MapX = x, MapY = y, MapMonsterId = monsterid, IsHostile = true, IsMoving = true, ShouldRespawn = false };
                                                    monster.Initialize(map8);
                                                    map8.AddMonster(monster);
                                                    map8.Broadcast(monster.GenerateIn());
                                                }
                                            Thread.Sleep(60000 / amount);
                                            amount--;
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                        amount = 350;
                                        while (map9?.Sessions?.Any() == true)
                                        {
                                            try
                                            {
                                                if (map9 != null)
                                                {
                                                    if (map9.Monsters.Count == 0 && amount <= 50 && map9.Portals.Count == 0)
                                                    {
                                                        MapCell cellportal = map9.Map.GetRandomPosition();
                                                        MapCell destination = map10.Map.GetRandomPosition();
                                                        Portal portale = new Portal { SourceMapId = map9.Map.MapId, SourceX = cellportal.X, SourceY = cellportal.Y, DestinationMapInstanceId = map10.MapInstanceId, DestinationX = destination.X, DestinationY = destination.Y, Type = (short)PortalType.Open };
                                                        map9.Portals.Add(portale);
                                                        map9.Broadcast(portale.GenerateGp());
                                                    }
                                                    MapCell cell = map9.Map.GetRandomPosition();

                                                    int monsterid = map9.GetNextMonsterId();
                                                    short x = cell.X;
                                                    short y = cell.Y;
                                                    MapMonster monster = new MapMonster { MonsterVNum = (short)(ServerManager.RandomNumber(1141, 1145)), MapX = x, MapY = y, MapMonsterId = monsterid, IsHostile = true, IsMoving = true, ShouldRespawn = false };
                                                    monster.Initialize(map9);
                                                    map9.AddMonster(monster);
                                                    map9.Broadcast(monster.GenerateIn());
                                                }
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                            Thread.Sleep(60000 / amount);
                                            amount--;
                                        }
                                        amount = 375;
                                        while (map10?.Sessions?.Any() == true)
                                        {
                                            try
                                            {
                                                if (map10 != null)
                                                {
                                                    if (map10.Monsters.Count == 0 && amount <= 50 && map10.Portals.Count == 0)
                                                    {
                                                        MapCell cellportal = map10.Map.GetRandomPosition();
                                                        MapCell destination = map11.Map.GetRandomPosition();
                                                        Portal portale = new Portal { SourceMapId = map10.Map.MapId, SourceX = cellportal.X, SourceY = cellportal.Y, DestinationMapInstanceId = map11.MapInstanceId, DestinationX = destination.X, DestinationY = destination.Y, Type = (short)PortalType.Open };
                                                        map10.Portals.Add(portale);
                                                        map10.Broadcast(portale.GenerateGp());
                                                    }
                                                    MapCell cell = map10.Map.GetRandomPosition();

                                                    int monsterid = map10.GetNextMonsterId();
                                                    short x = cell.X;
                                                    short y = cell.Y;
                                                    MapMonster monster = new MapMonster { MonsterVNum = (short)(ServerManager.RandomNumber(1141, 1145)), MapX = x, MapY = y, MapMonsterId = monsterid, IsHostile = true, IsMoving = true, ShouldRespawn = false };
                                                    monster.Initialize(map10);
                                                    map10.AddMonster(monster);
                                                    map10.Broadcast(monster.GenerateIn());
                                                }
                                            Thread.Sleep(60000 / amount);
                                            amount--;
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                        amount = 400;
                                        while (map11?.Sessions?.Any() == true)
                                        {
                                            try
                                            {
                                                if (map11 != null)
                                                {
                                                    if (map11.Monsters.Count == 0 && amount <= 50 && map11.Portals.Count == 0)
                                                    {
                                                        MapCell cellportal = map11.Map.GetRandomPosition();
                                                        MapCell destination = map12.Map.GetRandomPosition();
                                                        Portal portale = new Portal { SourceMapId = map11.Map.MapId, SourceX = cellportal.X, SourceY = cellportal.Y, DestinationMapInstanceId = map12.MapInstanceId, DestinationX = destination.X, DestinationY = destination.Y, Type = (short)PortalType.Open };
                                                        map11.Portals.Add(portale);
                                                        map11.Broadcast(portale.GenerateGp());
                                                    }
                                                    MapCell cell = map11.Map.GetRandomPosition();

                                                    int monsterid = map11.GetNextMonsterId();
                                                    short x = cell.X;
                                                    short y = cell.Y;
                                                    MapMonster monster = new MapMonster { MonsterVNum = (short)(ServerManager.RandomNumber(1154, 1158)), MapX = x, MapY = y, MapMonsterId = monsterid, IsHostile = true, IsMoving = true, ShouldRespawn = false };
                                                    monster.Initialize(map11);
                                                    map11.AddMonster(monster);
                                                    map11.Broadcast(monster.GenerateIn());
                                                }
                                            Thread.Sleep(60000 / amount);
                                            amount--;
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                        amount = 450;
                                        while (map12?.Sessions?.Any() == true)
                                        {
                                            try
                                            {
                                                if (map12 != null)
                                                {
                                                    if (map12.Monsters.Count == 0)
                                                    {
                                                        map12.Dispose();
                                                        ServerManager.Instance.MapXpMembersSolo.RemoveAll(all => all.GroupId == member.GroupId);
                                                    }
                                                    MapCell cell = map12.Map.GetRandomPosition();

                                                    int monsterid = map12.GetNextMonsterId();
                                                    short x = cell.X;
                                                    short y = cell.Y;
                                                    MapMonster monster = new MapMonster { MonsterVNum = (short)(ServerManager.RandomNumber(1154, 1158)), MapX = x, MapY = y, MapMonsterId = monsterid, IsHostile = true, IsMoving = true, ShouldRespawn = false };
                                                    monster.Initialize(map12);
                                                    map12.AddMonster(monster);
                                                    map12.Broadcast(monster.GenerateIn());
                                                }
                                            Thread.Sleep(60000 / amount);
                                            amount--;
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                    });
                                });
                            });
                            Thread.Sleep(5000);

                            ServerManager.Instance.MapXpMembersSolo.Where(o => o.GroupId == member.GroupId || o.GroupId == s.GroupId).ToList()
                                .ForEach(se => { se.Session.SendPacket(se.Session.Character.GenerateBsInfo(2, 2, 0, 0)); });

                            ServerManager.Instance.MapXpMembersSolo.RemoveAll(o => o.GroupId == member.GroupId || o.GroupId == s.GroupId);
                        }
                    }
                    else
                    {
                        if (s.GroupId == null)
                        {
                            if (s.Time != -1)
                            {
                                s.Session.SendPacket(s.Session.Character.GenerateBsInfo(1, 2, s.Time, 7));
                                s.Session.SendPacket(s.Session.Character.GenerateSay("Nessun Team Trovato", 10));
                            }
                            s.Time = 300;
                            s.Session.SendPacket(s.Session.Character.GenerateBsInfo(1, 2, s.Time, 5));
                            s.Session.SendPacket(s.Session.Character.GenerateSay("Cercando Team", 10));
                        }
                        else if (ServerManager.Instance.MapXpMembersSolo.Count(g => g.GroupId == s.GroupId) < 1)
                        {
                            s.Session.SendPacket(s.Session.Character.GenerateBsInfo(1, 2, -1, 4));
                            Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(time =>
                            {
                                s.Time = 300;
                                s.Session.SendPacket(s.Session.Character.GenerateBsInfo(1, 2, s.Time, 8));
                                s.Session.SendPacket(s.Session.Character.GenerateSay("Riprovo la ricerca", 10));
                            });
                        }
                        else
                        {
                            s.Session.SendPacket(s.Session.Character.GenerateBsInfo(0, 2, -1, 3));
                            Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(time =>
                            {
                                s.Time = 300;
                                s.Session.SendPacket(s.Session.Character.GenerateBsInfo(0, 2, s.Time, 1));
                                s.Session.SendPacket(s.Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SEARCH_RIVAL_ARENA_TEAM"), 10));
                            });
                        }
                    }
                });
                seconds++;
            });
        }
        #endregion
    }
}