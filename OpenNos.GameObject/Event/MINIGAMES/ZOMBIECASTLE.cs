﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.GameObject.Networking;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;
using System.Linq;
using System.Collections.Concurrent;
using OpenNos.Data;
using OpenNos.PathFinder;

namespace OpenNos.GameObject.Event
{
    public class ZOMBIECASTLE
    {
        #region Methods
        public static void GenerateZombieCastleLobby()
        {
            long groupid = 0;
            int seconds = 0;
            IDisposable obs = Observable.Interval(TimeSpan.FromSeconds(1)).Subscribe(start2 =>
            {
                ServerManager.Instance.ZombieCastleMembers.Where(s => s.ArenaType == EventType.ZOMBIECASTLE).ToList().ForEach(s =>
                {
                    s.Time -= 1;
                    List<long> groupids = new List<long>();
                    ServerManager.Instance.ZombieCastleMembers.Where(o => o.GroupId != null).ToList().ForEach(o =>
                    {
                        if (ServerManager.Instance.ZombieCastleMembers.Count(g => g.GroupId == o.GroupId) != 3)
                        {
                            return;
                        }
                        if (o.GroupId != null)
                        {
                            groupids.Add(o.GroupId.Value);
                        }
                    });

                    if (s.Time > 0)
                    {
                        if (s.GroupId == null)
                        {
                            List<ZombieCastleMember> members = ServerManager.Instance.ZombieCastleMembers
                                .Where(e => e.Session != s.Session && e.ArenaType == EventType.ZOMBIECASTLE).ToList();
                            members.RemoveAll(o => o.GroupId != null && groupids.Contains(o.GroupId.Value));
                            ZombieCastleMember member = members.FirstOrDefault();
                            if (member == null)
                            {
                                return;
                            }
                            {
                                if (member.GroupId == null)
                                {
                                    groupid++;
                                    member.GroupId = groupid;
                                }
                                s.GroupId = member.GroupId;
                                ServerManager.Instance.ZombieCastleMembers.Where(e => e.ArenaType == EventType.ZOMBIECASTLE && e.GroupId == member.GroupId).ToList().ForEach(o =>
                                {
                                    o.Session.SendPacket(o.Session.Character.GenerateBsInfo(1, 2, -1, 6));
                                    o.Session.SendPacket(o.Session.Character.GenerateSay("Team Found", 10));
                                    Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(time =>
                                    {
                                        s.Time = 300;
                                        if (ServerManager.Instance.ZombieCastleMembers.Count(g => g.GroupId == s.GroupId) < 3)
                                        {
                                            o.Session.SendPacket(o.Session.Character.GenerateBsInfo(0, 2, s.Time, 8));
                                            o.Session.SendPacket(o.Session.Character.GenerateSay("Looking for Team", 10));
                                        }
                                    });
                                });
                            }
                        }
                        else
                        {
                            if (ServerManager.Instance.ZombieCastleMembers.Count(g => g.GroupId == s.GroupId) != 3)
                            {
                                return;
                            }
                            
                            ZombieCastleMember member = ServerManager.Instance.ZombieCastleMembers.FirstOrDefault(o => o.GroupId != null && groupids.Contains(o.GroupId.Value));
                            if (member == null)
                            {
                                return;
                            }

                            MapInstance map = ServerManager.GenerateMapInstance(2514, MapInstanceType.ZombieCastleInstance, new InstanceBag());
                            #region lame
                            MapNpc lama1 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 31,
                                MapX = 64,
                                MapId = 2514,
                                Position = 0,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama1.Initialize(map);
                            map.AddNPC(lama1);
                            MapNpc lama2 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 31,
                                MapX = 64,
                                MapId = 2514,
                                Position = 2,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama2.Initialize(map);
                            map.AddNPC(lama2);
                            MapNpc lama3 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 34,
                                MapX = 62,
                                MapId = 2514,
                                Position = 0,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama3.Initialize(map);
                            map.AddNPC(lama3);
                            MapNpc lama4 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 34,
                                MapX = 62,
                                MapId = 2514,
                                Position = 2,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama4.Initialize(map);
                            map.AddNPC(lama4);
                            MapNpc lama5 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 36,
                                MapX = 60,
                                MapId = 2514,
                                Position = 0,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama5.Initialize(map);
                            map.AddNPC(lama5);
                            MapNpc lama6 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 36,
                                MapX = 60,
                                MapId = 2514,
                                Position = 2,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama6.Initialize(map);
                            map.AddNPC(lama6);
                            MapNpc lama7 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 39,
                                MapX = 57,
                                MapId = 2514,
                                Position = 0,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama7.Initialize(map);
                            map.AddNPC(lama7);
                            MapNpc lama8 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 39,
                                MapX = 57,
                                MapId = 2514,
                                Position = 2,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama8.Initialize(map);
                            map.AddNPC(lama8);
                            MapNpc lama9 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 39,
                                MapX = 55,
                                MapId = 2514,
                                Position = 0,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama9.Initialize(map);
                            map.AddNPC(lama9);
                            MapNpc lama10 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 39,
                                MapX = 55,
                                MapId = 2514,
                                Position = 2,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama10.Initialize(map);
                            map.AddNPC(lama10);
                            MapNpc lama11 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 36,
                                MapX = 53,
                                MapId = 2514,
                                Position = 0,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama11.Initialize(map);
                            map.AddNPC(lama11);
                            MapNpc lama12 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 36,
                                MapX = 53,
                                MapId = 2514,
                                Position = 2,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama12.Initialize(map);
                            map.AddNPC(lama12);
                            MapNpc lama13 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 36,
                                MapX = 53,
                                MapId = 2514,
                                Position = 0,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama13.Initialize(map);
                            map.AddNPC(lama13);
                            MapNpc lama14 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 36,
                                MapX = 53,
                                MapId = 2514,
                                Position = 2,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama14.Initialize(map);
                            map.AddNPC(lama14);
                            MapNpc lama15 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 34,
                                MapX = 51,
                                MapId = 2514,
                                Position = 0,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama15.Initialize(map);
                            map.AddNPC(lama15);
                            MapNpc lama16 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 34,
                                MapX = 51,
                                MapId = 2514,
                                Position = 2,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama16.Initialize(map);
                            map.AddNPC(lama16);
                            MapNpc lama17 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 31,
                                MapX = 50,
                                MapId = 2514,
                                Position = 0,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama17.Initialize(map);
                            map.AddNPC(lama17);
                            MapNpc lama18 = new MapNpc
                            {
                                NpcVNum = 2595,
                                MapY = 31,
                                MapX = 50,
                                MapId = 2514,
                                Position = 2,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            lama18.Initialize(map);
                            map.AddNPC(lama18);
                            #endregion
                            MapNpc sakura = new MapNpc
                            {
                                NpcVNum = 417,
                                MapY = 33,
                                MapX = 57,
                                MapId = 2514,
                                Position = 2,
                                IsMoving = false,
                                MapNpcId = map.GetNextMonsterId()
                            };
                            sakura.Initialize(map);
                            map.AddNPC(sakura);

                            ConcurrentBag<ZombieTeamMember> zombieTeam = new ConcurrentBag<ZombieTeamMember>();
                            ServerManager.Instance.ZombieTeams.Add(zombieTeam);

                            ZombieCastleMember[] arenamembers = ServerManager.Instance.ZombieCastleMembers.Where(o => o.GroupId == member.GroupId || o.GroupId == s.GroupId).OrderBy(o => o.GroupId)
                                .ToArray();
                            map.InstanceBag.Clock.SecondsRemaining = 30;
                            map.InstanceBag.Clock.StartClock();
                            IDisposable obs3 = null;
                            Observable.Timer(TimeSpan.FromSeconds(5)).Subscribe(time2 =>
                            {
                                obs3 = Observable.Interval(TimeSpan.FromSeconds(5)).Subscribe(effect =>
                                {
                                    arenamembers.ToList().ForEach(o => map.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, o.Session.Character.CharacterId, 3013)));

                                });
                            });

                            arenamembers.ToList().ForEach(o =>
                            {
                                o.Session.SendPacket(o.Session.Character.GenerateBsInfo(0, 2, s.Time, 2));

                                Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(time =>
                                {
                                    o.Session.SendPacket("ta_close");
                                    Observable.Timer(TimeSpan.FromSeconds(5)).Subscribe(time2 =>
                                    {
                                        o.Session.Character.GeneralLogs.Add(new GeneralLogDTO
                                        {
                                            AccountId = o.Session.Account.AccountId,
                                            CharacterId = o.Session.Character.CharacterId,
                                            IpAddress = o.Session.IpAddress,
                                            LogData = "Entry",
                                            LogType = "ZombieTeamArena",
                                            Timestamp = DateTime.Now
                                        });
                                        o.Session.Character.Buff.ClearAll();
                                        int i = Array.IndexOf(arenamembers, o) + 1;
                                        o.Session.Character.Hp = (int)o.Session.Character.HPLoad();
                                        o.Session.Character.Mp = (int)o.Session.Character.MPLoad();
                                        ServerManager.Instance.ChangeMapInstance(o.Session.Character.CharacterId, map.MapInstanceId, Convert.ToInt16(57), Convert.ToInt16(34));
                                        Thread.Sleep(10000);
                                        short round = 0;

                                        while (map?.Sessions?.Any() == true)
                                        {
                                            int amount = 60 + (20 * round);
                                            round++;
                                            int isd = amount;
                                            while (isd != 0)
                                            {
                                                map.Broadcast(UserInterfaceHelper.GenerateMsg("Prepare for the " + round + " round", 0));
                                                try
                                                {
                                                    if (map != null)
                                                    {
                                                        int zombieid = map.GetNextMonsterId();
                                                        short x = (short)(50 + (ServerManager.RandomNumber(0, 5)));
                                                        short y = (short)(75 + (ServerManager.RandomNumber(0, 5)));
                                                        MapMonster zombie = new MapMonster { MonsterVNum = (short)(297 + (ServerManager.RandomNumber(0, 2))), MapX = x, MapY = y, MapMonsterId = zombieid, IsHostile = false, IsMoving = true, ShouldRespawn = false };
                                                        zombie.Initialize(map);
                                                        map.AddMonster(zombie);
                                                        map.Broadcast(zombie.GenerateIn());
                                                        zombie.Path = BestFirstSearch.FindPathJagged(new Node { X = zombie.MapX, Y = zombie.MapY }, new Node { X = 57, Y = 33 }, map?.Map.JaggedGrid);

                                                        int zombieeid = map.GetNextMonsterId();
                                                        short x1 = (short)(78 + (ServerManager.RandomNumber(0, 5)));
                                                        short y1 = (short)(65 + (ServerManager.RandomNumber(0, 5)));
                                                        MapMonster zombiee = new MapMonster { MonsterVNum = (short)(297 + (ServerManager.RandomNumber(0, 2))), MapX = x, MapY = y, MapMonsterId = zombieeid, IsHostile = false, IsMoving = true, ShouldRespawn = false };
                                                        zombiee.Initialize(map);
                                                        map.AddMonster(zombiee);
                                                        map.Broadcast(zombiee.GenerateIn());
                                                        zombiee.Path = BestFirstSearch.FindPathJagged(new Node { X = zombiee.MapX, Y = zombiee.MapY }, new Node { X = 57, Y = 33 }, map?.Map.JaggedGrid);

                                                        int zombieeeid = map.GetNextMonsterId();
                                                        short x2 = (short)(20 + (ServerManager.RandomNumber(0, 5)));
                                                        short y2 = (short)(60 + (ServerManager.RandomNumber(0, 5)));
                                                        MapMonster zombieee = new MapMonster { MonsterVNum = (short)(297 + (ServerManager.RandomNumber(0, 2))), MapX = x, MapY = y, MapMonsterId = zombieeeid, IsHostile = false, IsMoving = true, ShouldRespawn = false };
                                                        zombieee.Initialize(map);
                                                        map.AddMonster(zombieee);
                                                        map.Broadcast(zombieee.GenerateIn());
                                                        zombieee.Path = BestFirstSearch.FindPathJagged(new Node { X = zombieee.MapX, Y = zombieee.MapY }, new Node { X = 57, Y = 33 }, map?.Map.JaggedGrid);
                                                    }
                                                    List<MapMonster> MapMonsters = map.GetListMonsterInRange(57, 33, 3);
                                                    if (MapMonsters.Count != 0)
                                                    {
                                                        foreach(ClientSession cs in map.Sessions)
                                                        {

                                                            if (cs.Character.StaticBonusList.Any(dds => dds.StaticBonusType == StaticBonusType.DoubleResource))
                                                            {
                                                                cs.Character.GiftAdd(1097, (short)(round * 2));
                                                            }
                                                            else
                                                            {
                                                                cs.Character.GiftAdd(1097, round);
                                                            }
                                                            cs.Character.Gold += round * 200000;
                                                            cs.Character.SetReputation((short)(round * 10000));
                                                            cs.SendPacket(cs.Character.GenerateGold());
                                                        }
                                                        map.Dispose();
                                                        ServerManager.Instance.ZombieCastleMembers.RemoveAll(all => all.GroupId == member.GroupId);

                                                    }
                                                }
                                                catch (Exception ex)
                                                {

                                                }
                                                Thread.Sleep(60000 / amount);
                                                isd--;
                                            }
                                        }
                                    });
                                });
                            });
                            Thread.Sleep(5000);

                            ServerManager.Instance.ZombieCastleMembers.Where(o => o.GroupId == member.GroupId || o.GroupId == s.GroupId).ToList()
                                .ForEach(se => { se.Session.SendPacket(se.Session.Character.GenerateBsInfo(2, 2, 0, 0)); });

                            ServerManager.Instance.ZombieCastleMembers.RemoveAll(o => o.GroupId == member.GroupId || o.GroupId == s.GroupId);
                        }
                    }
                    else
                    {
                        if (s.GroupId == null)
                        {
                            if (s.Time != -1)
                            {
                                s.Session.SendPacket(s.Session.Character.GenerateBsInfo(1, 2, s.Time, 7));
                                s.Session.SendPacket(s.Session.Character.GenerateSay("No Team Found", 10));
                            }
                            s.Time = 300;
                            s.Session.SendPacket(s.Session.Character.GenerateBsInfo(1, 2, s.Time, 5));
                            s.Session.SendPacket(s.Session.Character.GenerateSay("Looking for Team", 10));
                        }
                        else if (ServerManager.Instance.ZombieCastleMembers.Count(g => g.GroupId == s.GroupId) < 3)
                        {
                            s.Session.SendPacket(s.Session.Character.GenerateBsInfo(1, 2, -1, 4));
                            Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(time =>
                            {
                                s.Time = 300;
                                s.Session.SendPacket(s.Session.Character.GenerateBsInfo(1, 2, s.Time, 8));
                                s.Session.SendPacket(s.Session.Character.GenerateSay("Try search again", 10));
                            });
                        }
                        else
                        {
                            s.Session.SendPacket(s.Session.Character.GenerateBsInfo(0, 2, -1, 3));
                            Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(time =>
                            {
                                s.Time = 300;
                                s.Session.SendPacket(s.Session.Character.GenerateBsInfo(0, 2, s.Time, 1));
                                s.Session.SendPacket(s.Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("SEARCH_RIVAL_ARENA_TEAM"), 10));
                            });
                        }
                    }
                });
                seconds++;
            });
            Observable.Timer(TimeSpan.FromHours(2)).Subscribe(start2 =>
            {
                ServerManager.Instance.StartedEvents.Remove(EventType.ZOMBIECASTLE);
                obs.Dispose();
            });
        }
        #endregion
    }
}