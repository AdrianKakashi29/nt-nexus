﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using System;
using System.Linq;
using OpenNos.GameObject.Networking;
using System.Collections.Generic;
using System.Reactive.Linq;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;

namespace OpenNos.GameObject
{
    public class SpecialItem : Item
    {
        #region Instantiation

        public SpecialItem(ItemDTO item) : base(item)
        {
        }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte Option = 0, string[] packetsplit = null)
        {
            inv.Item.BCards.ForEach(c => c.ApplyBCards(session.Character));

            switch (Effect)
            {
                case 0:
                    switch (VNum)
                    {
                        case 882: // morcos
                            int rnd7 = ServerManager.RandomNumber(0, 1000);
                            int random = ServerManager.RandomNumber(5, 8);
                            short[] vnums7 = null;
                            if (rnd7 < 900)
                            {
                                vnums7 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                            }
                            else
                            {
                                vnums7 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                            }
                            session.Character.GiftAdd(vnums7[ServerManager.RandomNumber(0, 7)], 1, (byte)random);
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 185: // hatus
                            int rnd8 = ServerManager.RandomNumber(0, 1000);
                            int random8 = ServerManager.RandomNumber(5, 8);
                            short[] vnums8 = null;
                            if (rnd8 < 900)
                            {
                                vnums8 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                            }
                            else
                            {
                                vnums8 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                            }
                            session.Character.GiftAdd(vnums8[ServerManager.RandomNumber(0, 7)], 1, (byte)random8);
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 999: // berios
                            int rnd9 = ServerManager.RandomNumber(0, 1000);
                            int random9 = ServerManager.RandomNumber(5, 8);
                            short[] vnums9 = null;
                            if (rnd9 < 900)
                            {
                                vnums9 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                            }
                            else
                            {
                                vnums9 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                            }
                            session.Character.GiftAdd(vnums9[ServerManager.RandomNumber(0, 7)], 1, (byte)random9);
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 942: // morcos
                            int rnd10 = ServerManager.RandomNumber(0, 1000);
                            int random10 = ServerManager.RandomNumber(5, 8);
                            short[] vnums10 = null;
                            if (rnd10 < 900)
                            {
                                vnums10 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                            }
                            else
                            {
                                vnums10 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                            }
                            session.Character.GiftAdd(vnums10[ServerManager.RandomNumber(0, 7)], 1, (byte)random10);
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        case 5960: //better caligfor
                            {
                                int caligor1 = ServerManager.RandomNumber(0, 1000);
                                short[] caligorvnums1 = null;
                                if (caligor1 < 50)
                                {
                                    caligorvnums1 = new short[] { 4491, 4492, 4493, 4491, 9600, 4490, 4483, 4842, 4841 };
                                }
                                else
                                {
                                    caligorvnums1 = new short[] { 4490, 4329, 4330, 4334, 4315, 4304, 4305, 1296, 5370 };
                                }
                                session.Character.GiftAdd(caligorvnums1[ServerManager.RandomNumber(0, 8)], 1, 0, 0, 1);
                                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                break;
                            }

                        case 5961: // syf caligor drop
                            {
                                int caligorr1 = ServerManager.RandomNumber(0, 1000);
                                short[] caligorrvnums1 = null;
                                if (caligorr1 < 500)
                                {
                                    caligorrvnums1 = new short[] { 4490, 4329, 4491, 4493, 4492, 4304, 4305, 5553, 5553 };
                                }
                                else
                                {
                                    session.SendPacket(UserInterfaceHelper.GenerateInfo("NOT_THIS_TIME"));
                                }
                                session.Character.GiftAdd(caligorrvnums1[ServerManager.RandomNumber(0, 8)], 1, 0, 0, 1);
                                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                break;
                            }

                        case 5860: // czapki
                            {
                                int rndczapki = ServerManager.RandomNumber(0, 1000);
                                short[] czapkivnums = null;
                                if (rndczapki < 900)
                                {
                                    czapkivnums = new short[] { 198, 457, 558, 478, 561, 564, 652, 653, 654, 655, 549, 552, 555, 972, 4114, 4115, 4116, 4117, 4118, 4119, 4120, 4252, 4255 };
                                }
                                else
                                {
                                    czapkivnums = new short[] { 191, 4179, 4076, 4827, 4283, 4365, 854, 857, 4317, 4390, 4394, 4219, 4220, 4221, 8295, 4286, 4219, 4220, 4221, 8295, 4286 };
                                }
                                session.Character.GiftAdd(czapkivnums[ServerManager.RandomNumber(0, 22)], 1);
                                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                break;
                            }

                        case 5861: // kostium
                            {
                                int rndkostium = ServerManager.RandomNumber(0, 1000);
                                short[] kostiumvnums = null;
                                if (rndkostium < 900)
                                {
                                    kostiumvnums = new short[] { 195, 667, 670, 673, 4258, 970, 4113, 4112, 4111, 4110, 4109, 4108, 4107, 4248, 4244, 694, 697, 806, 809, 682, 679, 894, 4321 };
                                }
                                else
                                {
                                    kostiumvnums = new short[] { 8171, 189, 4075, 4829, 4285, 4367, 833, 830, 4392, 4398, 4388, 4404, 8299, 4177, 4185, 4187, 4268, 4380, 4187, 4268, 4380 };
                                }
                                session.Character.GiftAdd(kostiumvnums[ServerManager.RandomNumber(0, 22)], 1);
                                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                break;
                            }

                        case 9600: // family 50k pdr
                            {
                                if (session.Character.Family != null)
                                {
                                    if (session.Character.Family.FamilyLevel == 20)
                                    {
                                        session.SendPacket(UserInterfaceHelper.GenerateInfo("FAMILY_LEVEL_MAX"));
                                    }
                                    else
                                    {
                                        session.Character.GenerateFamilyXp(500000);
                                        session.Character.Inventory.RemoveItemAmount(9600, 1);
                                        CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                                        {
                                            DestinationCharacterId = null,
                                            SourceCharacterId = session.Character.CharacterId,
                                            SourceWorldId = ServerManager.Instance.WorldId,
                                            Message = $"{session.Character.Name} used bonus and got 50.000 Family Exp!",
                                            Type = MessageType.Shout
                                        });
                                    }
                                }
                                else
                                {
                                    session.SendPacket(UserInterfaceHelper.GenerateInfo("YOU_DONT_HAVE_FAMILY"));
                                }
                            }
                            break;

                        case 5836:
                            session.SendPacket(session.Character.GenerateGB(0));
                            session.SendPacket(session.Character.GenerateSMemo(6, "Account balance: " + session.Character.GoldBank + ".000 Gold; Gold in your possession:" + session.Character.Gold + " Thanks, for using Bacnk Cuarry services!"));
                            break;

                        case 5931: // reset skilli partnera wybrane
                            Mate mat = session.Character.Mates?.Find(s => s.PetId.ToString() == packetsplit[8] && s.MateType == MateType.Partner);
                            if (mat == null)
                            {
                                return;
                            }
                            if (mat.IsUsingSp == true)
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateInfo("You have to undo the Transformation!"));
                                return;
                            }
                            switch (packetsplit[9].First())
                            {
                                case '0':
                                    if (mat.SpInstance != null)
                                    {
                                        mat.SpInstance.Rare = 0;
                                        session.SendPacket(mat.GenerateScPacket());
                                    }
                                    break;
                                case '1':
                                    if (mat.SpInstance != null)
                                    {
                                        mat.SpInstance.Upgrade = 0;
                                        session.SendPacket(mat.GenerateScPacket());
                                    }
                                    break;
                                case '2':
                                    if (mat.SpInstance != null)
                                    {
                                        mat.SpInstance.SpStoneUpgrade = 0;
                                        session.SendPacket(mat.GenerateScPacket());
                                    }
                                    break;
                            }
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;
                        case 5932: // wszystkie
                            Mate mates = session.Character.Mates.Find(s => s.PetId.ToString() == packetsplit[8] && s.MateType == MateType.Partner);
                            if (mates == null)
                            {
                                return;
                            }
                            if (mates.IsUsingSp)
                            {

                                session.SendPacket(UserInterfaceHelper.GenerateInfo("Your partner doesn't have to be a specialist!"));
                                return;
                            }
                            if (mates.SpInstance != null)
                            {
                                mates.SpInstance.Rare = 0;
                                mates.SpInstance.Upgrade = 0;
                                mates.SpInstance.SpStoneUpgrade = 0;
                            }
                            session.SendPacket(mates.GenPski());
                            session.SendPacket(mates.GenerateScPacket());
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;
                        case 9116: // dopek expa wr
                            try
                            {
                                session.Character.AddStaticBuff(new StaticBuffDTO
                                {
                                    CardId = 393,
                                    CharacterId = session.Character.CharacterId,
                                    RemainingTime = 3600
                                });
                                session.Character.RemoveBuff(339);
                                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            }
                            catch(Exception ex) { }
                            break;
                        case 5370:
                            session.Character.AddStaticBuff(new StaticBuffDTO
                            {
                                CardId = 393,
                                CharacterId = session.Character.CharacterId,
                                RemainingTime = 3600
                            });
                            session.Character.RemoveBuff(339);
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;
                        case 5916:
                            session.Character.AddStaticBuff(new StaticBuffDTO
                            {
                                CardId = 340,
                                CharacterId = session.Character.CharacterId,
                                RemainingTime = 7200
                            });
                            session.Character.RemoveBuff(339);
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;
                        case 5929:
                            session.Character.AddStaticBuff(new StaticBuffDTO
                            {
                                CardId = 340,
                                CharacterId = session.Character.CharacterId,
                                RemainingTime = 600
                            });
                            session.Character.RemoveBuff(339);
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;
                    }
                    break;

                //// Madre Natura
                //case 8792:
                //    session.Character.Inventory.AddNewToInventory(4990, amount: 1);
                //    session.Character.Inventory.AddNewToInventory(4991, amount: 1);
                //    session.Character.Inventory.AddNewToInventory(4992, amount: 1);
                //    session.Character.Inventory.AddNewToInventory(4993, amount: 1);
                //    session.Character.Inventory.AddNewToInventory(4994, amount: 1);
                //    if (15 > ServerManager.RandomNumber(0, 100))
                //    {
                //        session.Character.Inventory.AddNewToInventory((short)(ServerManager.RandomNumber(4360, 4364)), amount: 1);
                //    }
                //    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                //    break;

                //case 4574:
                //    if (session.CurrentMapInstance?.Map.MapId == 2514)
                //    {
                //        session.Character.AddStaticBuff(new StaticBuffDTO
                //        {
                //            CardId = 471,
                //            CharacterId = session.Character.CharacterId,
                //            RemainingTime = 7200
                //        });
                //        if (session.Character.Buff.Any(s => s.Card.CardId == 472))
                //        {
                //            session.Character.Session.Character.RemoveBuff(472);
                //        }
                //        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                //    }
                //    break;

                // Honour Medals
                case 69:
                    session.Character.Reputation += ReputPrice;
                    session.SendPacket(session.Character.GenerateFd());
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;

                // SP Potions
                case 150:
                case 151:
                    session.Character.SpAdditionPoint += EffectValue;
                    if (session.Character.SpAdditionPoint > 1000000)
                    {
                        session.Character.SpAdditionPoint = 1000000;
                    }
                    session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("SP_POINTSADDED"), EffectValue), 0));
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    session.SendPacket(session.Character.GenerateSpPoint());
                    break;

                // Specialist Medal
                case 204:
                    session.Character.SpPoint += EffectValue;
                    session.Character.SpAdditionPoint += EffectValue * 3;
                    if (session.Character.SpAdditionPoint > 1000000)
                    {
                        session.Character.SpAdditionPoint = 1000000;
                    }
                    if (session.Character.SpPoint > 10000)
                    {
                        session.Character.SpPoint = 10000;
                    }
                    session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("SP_POINTSADDEDBOTH"), EffectValue, EffectValue * 3), 0));
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    session.SendPacket(session.Character.GenerateSpPoint());
                    break;

                case 210:
                    if (!session.Character.Buff.ContainsKey(122))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 122, RemainingTime = 3600 });
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                // Raid Seals
                case 301:
                    if (ServerManager.Instance.IsCharacterMemberOfGroup(session.Character.CharacterId))
                    {
                        session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("RAID_OPEN_GROUP"), 12));
                        return;
                    }
                    ItemInstance raidSeal = session.Character.Inventory.LoadBySlotAndType<ItemInstance>(inv.Slot, InventoryType.Main);

                    ScriptedInstance raid = ServerManager.Instance.Raids.FirstOrDefault(s => s.RequiredItems?.Any(obj => obj?.VNum == raidSeal.ItemVNum) == true)?.Copy();
                    if (raid != null)
                    {
                        if (session.Character.SwitchLevel() < raid.LevelMinimum)
                        {
                            session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("RAID_LEVEL_LOW"), 10));
                            return;
                        }
                        if (raid.Label == "Lord Draco")
                        {
                            ItemInstance amulet = session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Amulet, InventoryType.Wear);
                            if (amulet != null)
                            {
                                if (amulet.ItemVNum != 4503)
                                {
                                    session.SendPacket($"info {Language.Instance.GetMessageFromKey("NO_AMULET_DRACO")}");
                                    return;
                                }
                                /*else
                                {
                                    int volte = 10 - session.Character.GeneralLogs.CountLinq(s => s.LogType == "Raid" && s.LogData == "Lord Draco" && s.Timestamp.Date == DateTime.Today);
                                    if (volte == 0)
                                    {
                                        return;
                                    }
                                }*/
                            }
                            else
                            {
                                session.SendPacket($"info {Language.Instance.GetMessageFromKey("NO_AMULET_DRACO")}");
                                return;
                            }
                        }
                        if (raid.Label == "Glacerus")
                        {
                            ItemInstance amulet = session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Amulet, InventoryType.Wear);
                            if (amulet != null)
                            {
                                if (amulet.ItemVNum != 4504)
                                {
                                    session.SendPacket($"info {Language.Instance.GetMessageFromKey("NO_AMULET_GLACE")}");
                                    return;
                                }
                                /*else
                                {
                                    int volte = 10 - session.Character.GeneralLogs.CountLinq(s => s.LogType == "Raid" && s.LogData == "Raid Glacerus, Il Gelido" && s.Timestamp.Date == DateTime.Today);
                                    if (volte == 0)
                                    {
                                        return;
                                    }
                                }*/
                            }
                            else
                            {
                                session.SendPacket($"info {Language.Instance.GetMessageFromKey("NO_AMULET_GLACE")}");
                                return;
                            }
                        }
                        Group group = new Group();
                        group = new Group
                        {
                            GroupType = GroupType.BigTeam,
                            Raid = raid
                        };
                        group.JoinGroup(session.Character.CharacterId);
                        ServerManager.Instance.AddGroup(group);
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("RAID_LEADER"), session.Character.Name), 0));
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("RAID_LEADER"), session.Character.Name), 10));
                        session.SendPacket(session.Character.GenerateRaid(2));
                        session.SendPacket(session.Character.GenerateRaid(0));
                        session.SendPacket(session.Character.GenerateRaid(1));
                        session.SendPacket(group.GenerateRdlst());
                        session.Character.Inventory.RemoveItemFromInventory(raidSeal.Id);
                    }
                    break;

                // Partner Suits/Skins
                case 305:
                    Mate mate = session.Character.Mates.Find(s => s.MateTransportId == int.Parse(packetsplit[3]));
                    if (mate != null && EffectValue == mate.NpcMonsterVNum && mate.Skin == 0)
                    {
                        mate.Skin = Morph;
                        session.SendPacket(mate.GenerateCMode(mate.Skin));
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                    break;

                // Fairy Booster
                case 250:
                    if (!session.Character.Buff.ContainsKey(131))
                    {
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 131, RemainingTime = 3600 });
                        session.CurrentMapInstance?.Broadcast(session.Character.GeneratePairy());
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), inv.Item.Name), 0));
                        session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, session.Character.CharacterId, 3014), session.Character.MapX, session.Character.MapY);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                // Rainbow Pearl/Magic Eraser
                case 666:
                    if (EffectValue == 1 && byte.TryParse(packetsplit[9], out byte islot))
                    {
                        ItemInstance wearInstance = session.Character.Inventory.LoadBySlotAndType(islot, InventoryType.Equipment);

                        if (wearInstance != null && (wearInstance.Item.ItemType == ItemType.Weapon || wearInstance.Item.ItemType == ItemType.Armor) && wearInstance.ShellEffects.Count != 0) //&& !wearInstance.Item.IsHeroic
                        {
                            wearInstance.ShellEffects.Clear();
                            DAOFactory.ShellEffectDAO.DeleteByEquipmentSerialId(wearInstance.EquipmentSerialId);
                            if (wearInstance.EquipmentSerialId == Guid.Empty)
                            {
                                wearInstance.EquipmentSerialId = Guid.NewGuid();
                            }
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("OPTION_DELETE"), 0));
                        }
                    }
                    else
                    {
                        session.SendPacket("guri 18 0");
                    }
                    break;

                // Atk/Def/HP/Exp potions
                case 6600:
                    if (ServerManager.Instance.ChannelId == 51)
                    {
                        return;
                    }
                    else
                    {

                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                    break;

                // Ancelloan's Blessing
                case 208:
                    if (!session.Character.Buff.ContainsKey(121))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 121, RemainingTime = 3600 });
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;


                // Job SP
                case 7459:
                    if (!session.Character.Buff.ContainsKey(146))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 146, RemainingTime = 3600 });
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                case 2081:
                    if (!session.Character.Buff.ContainsKey(146))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 146, RemainingTime = 3600 });
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                // Divorce letter
                case 6969: // this is imaginary number I = √(-1)
                    CharacterRelationDTO rel = session.Character.CharacterRelations.FirstOrDefault(s => s.RelationType == CharacterRelationType.Spouse);
                    if (rel != null)
                    {
                        if (session.Character.Group == null)
                        {
                            session.Character.DeleteRelation(rel.CharacterId == session.Character.CharacterId ? rel.RelatedCharacterId : rel.CharacterId, true);
                            session.SendPacket(UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("DIVORCED")));
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id, 1);
                        }
                        else
                        {
                            session.SendPacket($"info {Language.Instance.GetMessageFromKey("IN_GROUP")}");

                        }
                    }
                    break;

                // Cupid's arrow
                case 34: // this is imaginary number I = √(-1)
                    if (packetsplit != null && packetsplit.Length > 3)
                    {
                        if (long.TryParse(packetsplit[3], out long characterId))
                        {
                            if (session.Character.CharacterRelations.Any(s => s.RelationType == CharacterRelationType.Spouse))
                            {
                                session.SendPacket($"info {Language.Instance.GetMessageFromKey("ALREADY_MARRIED")}");
                                return;
                            }
                            if (Option == 0)
                            {
                                session.SendPacket($"qna #u_i^1^{characterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("ASK_CUPID_ARROW")}");
                            }
                            else
                            {
                                ClientSession otherSession = ServerManager.Instance.GetSessionByCharacterId(characterId);
                                if (otherSession != null)
                                {
                                    if (otherSession.Character.CharacterRelations.Any(s => s.RelationType == CharacterRelationType.Spouse))
                                    {
                                        session.SendPacket($"info {Language.Instance.GetMessageFromKey("ALREADY_MARRIED")}");
                                        return;
                                    }

                                    if (session.Character.Group == null)
                                    {
                                        otherSession.SendPacket(UserInterfaceHelper.GenerateDialog(
                                        $"#fins^34^{session.Character.CharacterId} #fins^69^{session.Character.CharacterId} {string.Format(Language.Instance.GetMessageFromKey("MARRY_REQUEST"), session.Character.Name)}"));
                                        session.Character.FriendRequestCharacters.Add(characterId);
                                    }
                                    else
                                    {
                                        session.SendPacket($"info {Language.Instance.GetMessageFromKey("IN_GROUP")}");

                                    }
                                }
                            }
                        }
                    }
                    break;

                //speed booster
                case 998:
                    if (!session.Character.IsVehicled || session.Character.Buff.Any(s => s.Card.CardId == 336))
                    {
                        return;
                    }
                    session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, session.Character.CharacterId, 885), session.Character.MapX, session.Character.MapY);
                    switch (session.Character.Morph)
                    {
                        case 2368:
                        case 2369:
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 30);
                            session.Character.LoadSpeed();
                            break;
                        case 2517: // Nossi M
                        case 2518: // Nossi F
                        case 2522: // Roller M
                        case 2523: // Roller F
                                   // Removes <= lv 4 debuffs
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 50);
                            session.Character.LoadSpeed();
                            session.Character.DisableBuffs(BuffType.Bad, 4);
                            break;

                        case 3693: // Giaguaro M
                        case 3694: // Giaguaro F
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 20);
                            session.Character.LoadSpeed();
                            session.Character.DisableBuffs(BuffType.Bad, 4);
                            break;

                        case 2406: // Tigre Bianca Magica
                        case 2407: // Tigre Bianca Magica F
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 30);
                            session.Character.LoadSpeed();
                            break;

                        case 1819: // SnowBoard M
                        case 1820: // SnowBoad F
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 50);
                            session.Character.LoadSpeed();
                            break;

                        case 1817: // Sci M
                        case 1818: // Sci F
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 50);
                            session.Character.LoadSpeed();
                            break;

                        case 2530: // Black male unicorn
                        case 2531: // Black Female Unicorn
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 50);
                            session.Character.LoadSpeed();
                            ServerManager.Instance.TeleportOnRandomPlaceInMap(session, session.Character.MapInstanceId, true);
                            break;

                        case 2526: // White male unicorn
                        case 2527: // White female unicorn
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 50);
                            session.Character.LoadSpeed();
                            ServerManager.Instance.TeleportOnRandomPlaceInMap(session, session.Character.MapInstanceId, true);
                            break;

                        case 2528: // Pink male unicorn
                        case 2529: // Pink female unicorn
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 50);
                            session.Character.LoadSpeed();
                            ServerManager.Instance.TeleportOnRandomPlaceInMap(session, session.Character.MapInstanceId, true);
                            break;

                        case 2930: //Marco Pollo
                        case 2931: //Marco Pollo
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 30);
                            session.Character.LoadSpeed();
                            break;

                        case 2411: // Cabrio Magica
                        case 2412: // Cabrio Magica
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 30);
                            session.Character.LoadSpeed();
                            break;

                        case 2928: // Male UFO
                        case 2929: // Female UFO
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 50);
                            ServerManager.Instance.TeleportOnRandomPlaceInMap(session, session.Character.MapInstanceId, true);
                            break;

                        case 2513://Cammello M
                        case 2514://Cammello F
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 50);
                            session.Character.LoadSpeed();
                            break;

                        case 3679: // Male squelettic dragon
                        case 3680: // Female squelettic dragon
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 50);
                            ServerManager.Instance.TeleportOnRandomPlaceInMap(session, session.Character.MapInstanceId, true);
                            break;

                        case 2370: // Magic Carpet M
                        case 2371: // Magic Carpet F
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 50);
                            session.Character.LoadSpeed();
                            break;

                        case 2432: // Magic broom
                        case 2433: // Magic broom F
                        case 2520: // Bici M
                        case 2521: // Bici F
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 50);
                            session.Character.LoadSpeed();
                            switch (session.Character.Direction)
                            {
                                case 0:
                                    // -y
                                    ServerManager.Instance.TeleportForward(session, session.Character.MapInstanceId, session.Character.PositionX, (short)(session.Character.PositionY - 5));
                                    break;
                                case 1:
                                    // +x
                                    ServerManager.Instance.TeleportForward(session, session.Character.MapInstanceId, (short)(session.Character.PositionX + 5), session.Character.PositionY);
                                    break;
                                case 2:
                                    // +y
                                    ServerManager.Instance.TeleportForward(session, session.Character.MapInstanceId, session.Character.PositionX, (short)(session.Character.PositionY + 5));
                                    break;
                                case 3:
                                    // -x
                                    ServerManager.Instance.TeleportForward(session, session.Character.MapInstanceId, (short)(session.Character.PositionX - 5), session.Character.PositionY);
                                    break;
                                case 4:
                                    ServerManager.Instance.TeleportForward(session, session.Character.MapInstanceId, (short)(session.Character.PositionX - 5), (short)(session.Character.PositionY - 5));
                                    // -x -y
                                    break;
                                case 5:
                                    // +x +y
                                    ServerManager.Instance.TeleportForward(session, session.Character.MapInstanceId, (short)(session.Character.PositionX - 5), (short)(session.Character.PositionY - 5));
                                    break;
                                case 6:
                                    // +x -y
                                    ServerManager.Instance.TeleportForward(session, session.Character.MapInstanceId, (short)(session.Character.PositionX + 5), (short)(session.Character.PositionY + 5));
                                    break;
                                case 7:
                                    // -x +y
                                    ServerManager.Instance.TeleportForward(session, session.Character.MapInstanceId, (short)(session.Character.PositionX - 5), (short)(session.Character.PositionY + 5));
                                    break;
                            }
                            break;

                        case 2524: // SkateBoard
                        case 2525: // SkateBoard
                        case 2932: // WindSurf M
                        case 2933: // WindSurf F
                            session.Character.AddBuff(new Buff(336, session.Character.SwitchLevel()), duration: 50);
                            session.Character.LoadSpeed();
                            if (session.Character.Hp > 0)
                            {
                                session.Character.Hp += session.Character.SwitchLevel() * 15;
                                if (session.Character.Hp > session.Character.HPLoad())
                                {
                                    session.Character.Hp = (int)session.Character.HPLoad();
                                }
                                session.CurrentMapInstance?.Broadcast(session, session.Character.GenerateRc(session.Character.SwitchLevel() * 15));
                            }
                            break;
                    }
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;

                case 8787:
                    {
                        ItemInstance box = session.Character.Inventory.LoadBySlotAndType<ItemInstance>(inv.Slot, InventoryType.Main);
                        if (box != null)
                        {
                            if (Option == 0)
                            {
                                session.SendPacket($"qna #guri^300^8024^{inv.Slot} {Language.Instance.GetMessageFromKey("ASK_OPEN_BOX")}");
                            }
                            else
                            {
                                if (box.Item.EffectValue == 0)
                                {
                                    List<RollGeneratedItemDTO> roll = box.Item.RollGeneratedItems.Where(s => s.OriginalItemVNum == box.Item.VNum).ToList();
                                    foreach (RollGeneratedItemDTO rollitem in roll)
                                    {
                                        Item i = ServerManager.GetItem(rollitem.ItemGeneratedVNum);
                                        sbyte rare = 0;
                                        byte upgrade = 0;
                                        if (i.ItemType == ItemType.Shell)
                                        {
                                            upgrade = (byte)ServerManager.RandomNumber(50, 81);
                                        }
                                        short amount = (short)(rollitem.ItemGeneratedAmount);
                                        if (session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.DoubleResource))
                                        {
                                            amount *= 2;
                                        }
                                        session.Character.GiftAdd(rollitem.ItemGeneratedVNum, amount, (byte)rare, upgrade);
                                        session.SendPacket($"rdi {rollitem.ItemGeneratedVNum} {rollitem.ItemGeneratedAmount}");
                                    }
                                    session.Character.Inventory.RemoveItemFromInventory(box.Id);
                                    return;
                                }
                                else
                                {
                                    List<RollGeneratedItemDTO> roll = box.Item.RollGeneratedItems.Where(s => s.OriginalItemVNum == box.Item.VNum).ToList();
                                    int probabilities = roll.Sum(s => s.Probability);
                                    int rnd = ServerManager.RandomNumber(0, probabilities);
                                    int currentrnd = 0;
                                    foreach (RollGeneratedItemDTO rollitem in roll)
                                    {
                                        currentrnd += rollitem.Probability;
                                        if (currentrnd >= rnd)
                                        {
                                            Item i = ServerManager.GetItem(rollitem.ItemGeneratedVNum);
                                            sbyte rare = 0;
                                            byte upgrade = 0;
                                            if (i.ItemType == ItemType.Armor || i.ItemType == ItemType.Weapon || i.ItemType == ItemType.Shell)
                                            {
                                                rare = box.Rare;
                                            }
                                            if (i.ItemType == ItemType.Shell)
                                            {
                                                upgrade = (byte)ServerManager.RandomNumber(50, 81);
                                            }
                                            short amount = (short)(rollitem.ItemGeneratedAmount);
                                            if (session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.DoubleResource))
                                            {
                                                amount *= 2;
                                            }
                                            session.Character.GiftAdd(rollitem.ItemGeneratedVNum, amount, (byte)rare, upgrade);
                                            session.SendPacket($"rdi {rollitem.ItemGeneratedVNum} {rollitem.ItemGeneratedAmount}");
                                            session.Character.Inventory.RemoveItemFromInventory(box.Id);
                                            return;

                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;

                //case 5157:
                //    {
                //        if (session.Character.SwitchLevel() == ServerManager.Instance.Configuration.MaxLevel)
                //        {
                //            return;
                //        }
                //        else
                //        {
                //            session.Character.SetLevelXp(0);
                //            session.Character.AddLevel(1);
                //            if (session.Character.SwitchLevel() == 55 || session.Character.SwitchLevel() == 80 || session.Character.SwitchLevel() == 99)
                //            {
                //                session.Character.InsertLevelLog(session.Character.SwitchLevel());
                //            }
                //            if (session.Character.SwitchLevel() >= ServerManager.Instance.Configuration.MaxLevel)
                //            {
                //                session.Character.SetLevel(ServerManager.Instance.Configuration.MaxLevel);
                //                session.Character.SetLevelXp(0);
                //            }
                //            session.Character.Hp = (int)session.Character.HPLoad();
                //            session.Character.Mp = (int)session.Character.MPLoad();
                //            session.SendPacket(session.Character.GenerateStat());
                //            session.SendPacket(session.Character.GenerateStatChar());
                //            if (session.Character.Family != null)
                //            {
                //                if (session.Character.SwitchLevel() > 20 && session.Character.SwitchLevel() % 10 == 0)
                //                {
                //                    session.Character.Family.InsertFamilyLog(FamilyLogType.LevelUp, Name, level: session.Character.SwitchLevel());
                //                    session.Character.Family.InsertFamilyLog(FamilyLogType.FamilyXP, Name, experience: 20 * session.Character.SwitchLevel());
                //                    session.Character.GenerateFamilyXp(20 * session.Character.SwitchLevel());
                //                }
                //                else if (session.Character.SwitchLevel() > 80)
                //                {
                //                    session.Character.Family.InsertFamilyLog(FamilyLogType.LevelUp, Name, level: session.Character.SwitchLevel());
                //                }
                //                else
                //                {
                //                    ServerManager.Instance.FamilyRefresh(session.Character.Family.FamilyId);
                //                    CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                //                    {
                //                        DestinationCharacterId = session.Character.Family.FamilyId,
                //                        SourceCharacterId = session.Character.CharacterId,
                //                        SourceWorldId = ServerManager.Instance.WorldId,
                //                        Message = "fhis_stc",
                //                        Type = MessageType.Family
                //                    });
                //                }
                //            }
                //            session.SendPacket(session.Character.GenerateLevelUp());
                //            session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("LEVELUP"), 0));
                //            session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, session.Character.CharacterId, 6), session.Character.PositionX, session.Character.PositionY);
                //            session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, session.Character.CharacterId, 198), session.Character.PositionX, session.Character.PositionY);
                //            ServerManager.Instance.UpdateGroup(session.Character.CharacterId);
                //            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                //            session.SendPacket(session.Character.GenerateLev());
                //        }
                //    }
                //    break;

                //case 100:
                //    {

                //        if (Option == 0)
                //        {
                //            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("ASK_SEGNAVIA")}");
                //        }
                //        else
                //        {
                //            NpcMonster npcmonster = ServerManager.GetNpc((short)EffectValue);
                //            if (npcmonster == null)
                //            {
                //                return;
                //            }


                //            MapNpc npc = session.CurrentMapInstance.Npcs.Find(s => s.Segnavia == session.Character.CharacterId);

                //            if (npc != null)
                //            {
                //                return;
                //            }

                //            if (session.CurrentMapInstance.Segnavia == false)
                //            {
                //                return;
                //            }

                //            MapNpc segnavia = new MapNpc
                //            {
                //                NpcVNum = (short)EffectValue,
                //                MapX = session.Character.MapX,
                //                MapY = session.Character.MapY,
                //                MapId = session.Character.MapId,
                //                IsMoving = false,
                //                MapNpcId = session.CurrentMapInstance.GetNextMonsterId(),
                //                Dialog = 9714,
                //                Segnavia = session.Character.CharacterId
                //            };
                //            segnavia.Initialize(session.CurrentMapInstance);
                //            session.CurrentMapInstance.AddNPC(segnavia);
                //            session.CurrentMapInstance.Broadcast(segnavia.GenerateIn());
                //            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                //            Observable.Timer(TimeSpan.FromMinutes(30)).Subscribe(o =>
                //            {
                //                segnavia.MapInstance.RemoveNpc(segnavia);
                //                segnavia.MapInstance?.Broadcast(StaticPacketHelper.Out(UserType.Npc, segnavia.MapNpcId));
                //            });

                //        }
                //    }
                //    break;

                // Faction Egg
                case 570:
                    if (ServerManager.Instance.ChannelId == 51)
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CANT_USE_THERE"), 0));
                    }
                    else
                    {
                        if (session.Character.Faction == (FactionType)EffectValue)
                        {
                            Console.WriteLine("chdsds0");

                            return;
                        }
                        if (EffectValue < 3)
                        {
                            session.SendPacket(session.Character.Family == null
                                ? $"qna #guri^750^{EffectValue} {Language.Instance.GetMessageFromKey($"ASK_CHANGE_FACTION{EffectValue}")}"
                                : UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("IN_FAMILY"),
                                0));

                        }
                        else
                        {
                            session.SendPacket(session.Character.Family != null
                                ? $"qna #guri^750^{EffectValue} {Language.Instance.GetMessageFromKey($"ASK_CHANGE_FACTION{EffectValue}")}"
                                : UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_FAMILY"),
                                0));
                        }
                    }
                    break;


                // SP Wings
                case 650:
                    ItemInstance specialistInstance = session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                    if (session.Character.UseSp && specialistInstance != null)
                    {
                        if (Option == 0)
                        {
                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("ASK_WINGS_CHANGE")}");
                        }
                        else
                        {
                            void disposeBuff(short vNum)
                            {
                                if (session.Character.BuffObservables.ContainsKey(vNum))
                                {
                                    session.Character.BuffObservables[vNum].Dispose();
                                    session.Character.BuffObservables.Remove(vNum);
                                }
                                session.Character.RemoveBuff(vNum);
                            }

                            disposeBuff(387);
                            disposeBuff(395);
                            disposeBuff(396);
                            disposeBuff(397);
                            disposeBuff(398);
                            disposeBuff(410);
                            disposeBuff(411);
                            disposeBuff(444);

                            specialistInstance.Design = (byte)EffectValue;

                            session.Character.MorphUpgrade2 = EffectValue;
                            session.CurrentMapInstance?.Broadcast(session.Character.GenerateCMode());
                            session.SendPacket(session.Character.GenerateStat());
                            session.SendPacket(session.Character.GenerateStatChar());
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_SP"), 0));
                    }
                    break;

                // Self-Introduction
                case 203:
                    if (!session.Character.IsVehicled && Option == 0)
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateGuri(10, 2, session.Character.CharacterId, 1));
                    }
                    break;

                // Magic Lamp
                case 651:
                    if (session.Character.Inventory.All(i => i.Type != InventoryType.Wear))
                    {
                        if (Option == 0)
                        {
                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("ASK_USE")}");
                        }
                        else
                        {
                            session.Character.ChangeSex();
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("EQ_NOT_EMPTY"), 0));
                    }
                    break;

                // Vehicles
                case 1000:
                    if (EffectValue != 0 || session.CurrentMapInstance?.MapInstanceType == MapInstanceType.EventGameInstance)
                    {
                        return;
                    }
                    if (Morph > 0)
                    {
                        if (Option == 0 && !session.Character.IsVehicled)
                        {
                            if (session.Character.IsSitting)
                            {
                                session.Character.IsSitting = false;
                                session.CurrentMapInstance?.Broadcast(session.Character.GenerateRest());
                            }
                            if (session.Character.MapInstance.Map.MapTypes.Any(m => m.MapTypeId == (short)MapTypeEnum.Act4))
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateDelay(3000, 3, $"#u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2"));
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateDelay(100, 3, $"#u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2"));
                            }
                        }
                        else
                        {
                            if (!session.Character.IsVehicled && Option != 0)
                            {
                                    session.Character.IsVehicled = true;
                                    session.Character.VehicleSpeed = Speed;
                                    session.Character.MorphUpgrade = 0;
                                    session.Character.MorphUpgrade2 = 0;
                                    if (inv.ItemVNum < 30000)
                                        session.Character.Morph = Morph + (byte)session.Character.Gender;
                                    else
                                        session.Character.Morph = Morph;
                                    session.Character.LoadSpeed();
                                    session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, session.Character.CharacterId, 196), session.Character.MapX, session.Character.MapY);
                                    session.CurrentMapInstance?.Broadcast(session.Character.GenerateCMode());
                                    session.Character.Mates.Where(s => s.IsTeamMember).ToList().ForEach(s => session.CurrentMapInstance?.Broadcast(s.GenerateOut()));
                                    session.SendPacket(session.Character.GenerateCond());
                                    session.Character.LastSpeedChange = DateTime.Now;
                            }
                            else if (session.Character.IsVehicled)
                            {
                                session.Character.RemoveVehicle();
                            }
                        }
                    }
                    break;

                //SpQuestTs
                case 140:
                    switch (EffectValue)
                    {
                        case 401:
                            {
                                if (VNum == 1328)
                                {
                                    if (session.Character.Quests.Any(q => q.QuestId == 2015))
                                    {
                                        if (Option == 0)
                                        {
                                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2 {Language.Instance.GetMessageFromKey("ASK_TS_2_SP")}");
                                        }
                                        else
                                        {
                                            Guid MapInstanceId = ServerManager.GetBaseMapInstanceIdByMapId(session.Character.MapId);
                                            MapInstance map = ServerManager.GetMapInstance(MapInstanceId);
                                            ScriptedInstance timespace = map.ScriptedInstances.Find(s => s.ScriptedInstanceId == 121);
                                            ScriptedInstance instance = timespace.Copy();
                                                instance.LoadScript(MapInstanceType.TimeSpaceInstance);
                                                if (instance.FirstMap == null)
                                                {
                                                    return;
                                                }
                                                session.Character.MapX = instance.PositionX;
                                                session.Character.MapY = instance.PositionY;
                                                ServerManager.Instance.TeleportOnRandomPlaceInMap(session, instance.FirstMap.MapInstanceId);
                                                instance.InstanceBag.CreatorId = session.Character.CharacterId;
                                                session.SendPackets(instance.GenerateMinimap());
                                                session.SendPacket(instance.GenerateMainInfo());
                                                session.SendPacket(instance.FirstMap.InstanceBag.GenerateScore());

                                                session.Character.Timespace = instance;
                                        }
                                    }
                                    else
                                    {
                                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("WRONG_QUEST"), 0));
                                    }
                                }
                            }
                            break;

                        case 404:
                            {
                                if (VNum == 1357)
                                {
                                    if (session.Character.Quests.Any(q => q.QuestId == 2048))
                                    {
                                        if (Option == 0)
                                        {
                                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2 {Language.Instance.GetMessageFromKey("ASK_TS_3_SP")}");
                                        }
                                        else
                                        {
                                            Guid MapInstanceId = ServerManager.GetBaseMapInstanceIdByMapId(session.Character.MapId);
                                            MapInstance map = ServerManager.GetMapInstance(MapInstanceId);
                                            ScriptedInstance timespace = map.ScriptedInstances.Find(s => s.ScriptedInstanceId == 122);
                                            ScriptedInstance instance = timespace.Copy();
                                                instance.LoadScript(MapInstanceType.TimeSpaceInstance);
                                                if (instance.FirstMap == null)
                                                {
                                                    return;
                                                }
                                                session.Character.MapX = instance.PositionX;
                                                session.Character.MapY = instance.PositionY;
                                                ServerManager.Instance.TeleportOnRandomPlaceInMap(session, instance.FirstMap.MapInstanceId);
                                                instance.InstanceBag.CreatorId = session.Character.CharacterId;
                                                session.SendPackets(instance.GenerateMinimap());
                                                session.SendPacket(instance.GenerateMainInfo());
                                                session.SendPacket(instance.FirstMap.InstanceBag.GenerateScore());

                                                session.Character.Timespace = instance;
                                        }
                                    }
                                    else
                                    {
                                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("WRONG_QUEST"), 0));
                                    }
                                }
                            }
                            break;


                        case 668:
                            {
                                if (VNum == 5501)
                                {
                                    if (session.Character.Quests.Any(q => q.QuestId == 2074))
                                    {
                                        if (Option == 0)
                                        {
                                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2 {Language.Instance.GetMessageFromKey("ASK_TS_4_SP")}");
                                        }
                                        else
                                        {
                                            Guid MapInstanceId = ServerManager.GetBaseMapInstanceIdByMapId(session.Character.MapId);
                                            MapInstance map = ServerManager.GetMapInstance(MapInstanceId);
                                            ScriptedInstance timespace = map.ScriptedInstances.Find(s => s.ScriptedInstanceId == 124);
                                            ScriptedInstance instance = timespace.Copy();
                                                instance.LoadScript(MapInstanceType.TimeSpaceInstance);
                                                if (instance.FirstMap == null)
                                                {
                                                    return;
                                                }
                                                session.Character.MapX = instance.PositionX;
                                                session.Character.MapY = instance.PositionY;
                                                ServerManager.Instance.TeleportOnRandomPlaceInMap(session, instance.FirstMap.MapInstanceId);
                                                instance.InstanceBag.CreatorId = session.Character.CharacterId;
                                                session.SendPackets(instance.GenerateMinimap());
                                                session.SendPacket(instance.GenerateMainInfo());
                                                session.SendPacket(instance.FirstMap.InstanceBag.GenerateScore());

                                                session.Character.Timespace = instance;
                                        }
                                    }
                                    else
                                    {
                                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("WRONG_QUEST"), 0));
                                    }
                                }
                            }
                            break;

                    }
                    break;

                // Sealed Vessel
                case 1002:
                    if (EffectValue == 69)
                    {
                        int rnd = ServerManager.RandomNumber(0, 1000);
                        if (rnd < 5)
                        {
                            short[] vnums =
                            {
                                5560, 5591, 4099, 907, 1160, 4705, 4706, 4707, 4708, 4709, 4710, 4711, 4712, 4713, 4714,
                                4715, 4716
                            };
                            byte[] counts = { 1, 1, 1, 1, 10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                            int item = ServerManager.RandomNumber(0, 17);
                            session.Character.GiftAdd(vnums[item], counts[item]);
                        }
                        else if (rnd < 30)
                        {
                            short[] vnums = { 361, 362, 363, 366, 367, 368, 371, 372, 373 };
                            session.Character.GiftAdd(vnums[ServerManager.RandomNumber(0, 9)], 1);
                        }
                        else
                        {
                            short[] vnums =
                            {
                                1161, 2282, 1030, 1244, 1218, 5369, 1012, 1363, 1364, 2160, 2173, 5959, 5983, 2514,
                                2515, 2516, 2517, 2518, 2519, 2520, 2521, 1685, 1686, 5087, 5203, 2418, 2310, 2303,
                                2169, 2280, 2229, 2229, 2229, 2229, 2229, 2229, 2229, 2229, 5332, 5105, 2161, 2162
                            };
                            byte[] counts =
                            {
                                10, 10, 20, 5, 1, 1, 99, 1, 1, 5, 5, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 5, 20,
                                20, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
                            };
                            int item = ServerManager.RandomNumber(0, 42);
                            session.Character.GiftAdd(vnums[item], counts[item]);
                        }
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                    else if (session.HasCurrentMapInstance && session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance && (session.Character.LastVessel.AddSeconds(1) <= DateTime.Now || session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.FastVessels)))
                    {
                        short[] vnums = { 1386, 1387, 1388, 1389, 1390, 1391, 1392, 1393, 1394, 1395, 1396, 1397, 1398, 1399, 1400, 1401, 1402, 1403, 1404, 1405 };
                        short vnum = vnums[ServerManager.RandomNumber(0, 20)];

                        NpcMonster npcmonster = ServerManager.GetNpc(vnum);
                        if (npcmonster == null)
                        {
                            return;
                        }
                        MapMonster monster = new MapMonster
                        {
                            MonsterVNum = vnum,
                            MapY = session.Character.MapY,
                            MapX = session.Character.MapX,
                            MapId = session.Character.MapInstance.Map.MapId,
                            Position = session.Character.Direction,
                            IsMoving = true,
                            MapMonsterId = session.CurrentMapInstance.GetNextMonsterId(),
                            ShouldRespawn = false
                        };
                        monster.Initialize(session.CurrentMapInstance);
                        session.CurrentMapInstance.AddMonster(monster);
                        session.CurrentMapInstance.Broadcast(monster.GenerateIn());
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.LastVessel = DateTime.Now;
                    }
                    break;

                // Golden Bazaar Medal
                case 1003:
                    if (!session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.BazaarMedalGold || s.StaticBonusType == StaticBonusType.BazaarMedalSilver))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(EffectValue),
                            StaticBonusType = StaticBonusType.BazaarMedalGold
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                // Silver Bazaar Medal
                case 1004:
                    if (!session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.BazaarMedalGold || s.StaticBonusType == StaticBonusType.BazaarMedalGold))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(EffectValue),
                            StaticBonusType = StaticBonusType.BazaarMedalSilver
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                // Pet Slot Expansion
                case 1006:
                    if (Option == 0)
                    {
                        session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2 {Language.Instance.GetMessageFromKey("ASK_PET_MAX")}");
                    }
                    else if (session.Character.MaxMateCount < 30)
                    {
                        session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("GET_PET_PLACES"), 10));
                        session.Character.MaxMateCount += 10;
                        session.SendPacket(session.Character.GenerateScpStc());
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                    break;

                case 5012:
                    if (Option == 0)
                    {
                        session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2 {Language.Instance.GetMessageFromKey("ASK_PET_MAX")}");
                    }
                    else if (session.Character.MaxPartnerCount < 12)
                    {
                        session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("GET_PET_PLACES"), 10));
                        session.Character.MaxPartnerCount++;
                        session.SendPacket(session.Character.GenerateScpStc());
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                    break;

                // Pet Basket
                case 1007:
                    if (session.Character.StaticBonusList.All(s => s.StaticBonusType != StaticBonusType.PetBasket))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(EffectValue),
                            StaticBonusType = StaticBonusType.PetBasket
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateExts());
                        session.SendPacket("ib 1278 1");
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                // Partner's Backpack
                case 1008:
                    if (session.Character.StaticBonusList.All(s => s.StaticBonusType != StaticBonusType.PetBackPack))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(EffectValue),
                            StaticBonusType = StaticBonusType.PetBackPack
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateExts());
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                // Backpack Expansion
                case 1009:
                    if (session.Character.StaticBonusList.All(s => s.StaticBonusType != StaticBonusType.BackPack))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(EffectValue),
                            StaticBonusType = StaticBonusType.BackPack
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateExts());
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                // Sealed Tarot Card
                case 1005:
                    session.Character.GiftAdd((short)(VNum - Effect), 1);
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;

                // Tarot Card Game
                case 1894:
                    if (EffectValue == 0)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            session.Character.GiftAdd((short)(Effect + ServerManager.RandomNumber(0, 10)), 1);
                        }
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                    break;

                // Sealed Tarot Card
                case 2152:
                    session.Character.GiftAdd((short)(VNum + Effect), 1);
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;

                /* Sealed Tarot Card
                case 2152:
                    switch (VNum)
                    {
                        case 1894:
                                session.Character.GiftAdd(364, 1);
                            break;
                        case 1895:
                                session.Character.GiftAdd(365, 1);
                            break;
                        case 1896:
                                session.Character.GiftAdd(369, 1);
                            break;
                        case 1897:
                                session.Character.GiftAdd(370, 1);
                            break;
                        case 1898:
                                session.Character.GiftAdd(374, 1);
                            break;
                        case 1899:
                                session.Character.GiftAdd(375, 1);
                            break;
                        case 1900:
                                session.Character.GiftAdd(377, 1);
                            break;
                        case 1901:
                                session.Character.GiftAdd(378, 1);
                            break;
                        case 1902:
                                session.Character.GiftAdd(379, 1);
                            break;
                        case 1903:
                                session.Character.GiftAdd(380, 1);
                            break;
                    }
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;*/

                // Raid Natsu Box

                //case 4000:
                //    {
                //        int rnd = ServerManager.RandomNumber(0, 100);
                //        if (rnd <= 2)
                //            session.Character.GiftAdd(5498, 1);
                //        else if (rnd <= 4)
                //            session.Character.GiftAdd(5499, 1);
                //        else if (rnd <= 7)
                //            session.Character.GiftAdd(2229, 99);
                //        else if (rnd <= 12)
                //            session.Character.GiftAdd(1286, 1);
                //        else if (rnd <= 17)
                //            session.Character.GiftAdd(2282, 50);
                //        else if (rnd <= 22)
                //            session.Character.GiftAdd(5369, 5);
                //        else if (rnd <= 27)
                //            session.Character.GiftAdd(1242, 50);
                //        else if (rnd <= 32)
                //            session.Character.GiftAdd(1243, 50);
                //        else if (rnd <= 37)
                //            session.Character.GiftAdd(4129, 1);
                //        else if (rnd <= 42)
                //            session.Character.GiftAdd(4130, 1);
                //        else if (rnd <= 47)
                //            session.Character.GiftAdd(4131, 1);
                //        else if (rnd <= 52)
                //            session.Character.GiftAdd(4132, 1);
                //        else if (rnd <= 58)
                //            session.Character.GiftAdd(2514, 99);
                //        else if (rnd <= 64)
                //            session.Character.GiftAdd(2515, 99);
                //        else if (rnd <= 70)
                //            session.Character.GiftAdd(2516, 99);
                //        else if (rnd <= 76)
                //            session.Character.GiftAdd(2517, 99);
                //        else if (rnd <= 82)
                //            session.Character.GiftAdd(2518, 99);
                //        else if (rnd <= 88)
                //            session.Character.GiftAdd(2519, 99);
                //        else if (rnd <= 94)
                //            session.Character.GiftAdd(2520, 99);
                //        else
                //            session.Character.GiftAdd(2521, 99);

                //        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                //    }
                //    break;

                //End Raid Natsu Box

                default:
                    switch (VNum)
                    {
                        case 5841:
                            int rnd = ServerManager.RandomNumber(0, 1000);
                            short[] vnums = null;
                            if (rnd < 900)
                            {
                                vnums = new short[] { 4356, 4357, 4358, 4359 };
                            }
                            else
                            {
                                vnums = new short[] { 4360, 4361, 4362, 4363 };
                            }
                            session.Character.GiftAdd(vnums[ServerManager.RandomNumber(0, 4)], 1);
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;

                        default:
                            Logger.Warn(string.Format(Language.Instance.GetMessageFromKey("NO_HANDLER_ITEM"), GetType(), VNum, Effect, EffectValue));
                            break;
                    }
                    break;
            }
        }

        #endregion
    }
}