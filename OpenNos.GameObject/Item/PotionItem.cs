﻿///*
// * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
// *
// * This program is free software; you can redistribute it and/or modify
// * it under the terms of the GNU General Public License as published by
// * the Free Software Foundation; either version 2 of the License, or
// * (at your option) any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// * GNU General Public License for more details.
// */

//using OpenNos.Data;
//using System;
//using System.Linq;
//using OpenNos.GameObject.Networking;
//using OpenNos.Domain;

//namespace OpenNos.GameObject
//{
//    public class PotionItem : Item
//    {
//        #region Instantiation

//        public PotionItem(ItemDTO item) : base(item)
//        {
//        }

//        #endregion

//        #region Methods

//        public override void Use(ClientSession session, ref ItemInstance inv, byte Option = 0, string[] packetsplit = null)
//        {
//            if (!session.HasCurrentMapInstance)
//            {
//                return;
//            }
//            if ((DateTime.Now - session.Character.LastPotion).TotalMilliseconds < (session.CurrentMapInstance.Map.MapTypes.OrderByDescending(s => s.PotionDelay).FirstOrDefault()?.PotionDelay ?? 750))
//            {
//                return;
//            }
//            if (session.CurrentMapInstance.Map.MapId == 2006 || session.CurrentMapInstance.Map.MapId == 2106 || session.CurrentMapInstance.Map.MapId == 9305)
//            {
//                return;
//            }
//            if(session.CurrentMapInstance.Map.MapId == 134)
//            {
//                return;
//            }
//            short HpTot = Hp;
//            double multiplier = 1;
//            multiplier += (session.Character.ShellEffectArmor.FirstOrDefault(s => s.Effect == (byte)ShellArmorEffectType.IncreasedRecoveryItemSpeed)?.Value ?? 0) / 100D;
//            if (session.Character.GetBuff(BCardType.CardType.LeonaPassiveSkill, (byte)AdditionalTypes.LeonaPassiveSkill.IncreaseRecoveryItems)[0] != 0)
//            {
//                multiplier += (session.Character.GetBuff(BCardType.CardType.LeonaPassiveSkill, (byte)AdditionalTypes.LeonaPassiveSkill.IncreaseRecoveryItems)[0]) / 100D;
//                if(multiplier < 0)
//                {
//                    multiplier = multiplier + 1;
//                    multiplier = Math.Abs(multiplier);
//                }
//            }
//            HpTot = (short)(HpTot * multiplier);


//            session.Character.LastPotion = DateTime.Now;
//            switch (Effect)
//            {
//                default:
//                    int hpLoad = (int)session.Character.HPLoad();
//                    int mpLoad = (int)session.Character.MPLoad();
//                    if (session.Character.Hp <= 0)
//                    {
//                        return;
//                    }
//                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
//                    if (hpLoad - session.Character.Hp < HpTot)
//                    {
//                        session.CurrentMapInstance?.Broadcast(session.Character.GenerateRc(hpLoad - session.Character.Hp));
//                    }
//                    else if (hpLoad - session.Character.Hp > HpTot)
//                    {
//                        session.CurrentMapInstance?.Broadcast(session.Character.GenerateRc(HpTot));
//                    }
//                    session.Character.Mp += Mp;
//                    session.Character.Hp += HpTot;
//                    if (session.Character.Mp > mpLoad)
//                    {
//                        session.Character.Mp = mpLoad;
//                    }
//                    if (session.Character.Hp > hpLoad)
//                    {
//                        session.Character.Hp = hpLoad;
//                    }
//                    if (ServerManager.Instance.ChannelId != 51 || session.Character.MapId == 130 || session.Character.MapId == 131)
//                    {
//                        if (inv.ItemVNum == 1242)
//                        {
//                            session.CurrentMapInstance?.Broadcast(session.Character.GenerateRc(hpLoad - session.Character.Hp));
//                            session.Character.Hp = hpLoad;
//                        }
//                        else if (inv.ItemVNum == 1243)
//                        {
//                            session.Character.Mp = mpLoad;
//                        }
//                        else if (inv.ItemVNum == 1244)
//                        {
//                            session.CurrentMapInstance?.Broadcast(session.Character.GenerateRc(hpLoad - session.Character.Hp));
//                            session.Character.Hp = hpLoad;
//                            session.Character.Mp = mpLoad;
//                        }
//                    }
//                    session.SendPacket(session.Character.GenerateStat());

//                    foreach (Mate mate in session.Character.Mates.Where(s => s.IsTeamMember))
//                    {
//                        hpLoad = mate.MaxHp;
//                        mpLoad = mate.MaxMp;
//                        if ((mate.Hp == hpLoad && mate.Mp == mpLoad) || mate.Hp <= 0)
//                        {
//                            return;
//                        }

//                        if (hpLoad - mate.Hp < HpTot)
//                        {
//                            session.CurrentMapInstance?.Broadcast(mate.GenerateRc(hpLoad - mate.Hp));
//                        }
//                        else if (hpLoad - mate.Hp > HpTot)
//                        {
//                            session.CurrentMapInstance?.Broadcast(mate.GenerateRc(HpTot));
//                        }

//                        mate.Mp += Mp;
//                        mate.Hp += HpTot;
//                        if (mate.Mp > mpLoad)
//                        {
//                            mate.Mp = mpLoad;
//                        }

//                        if (mate.Hp > hpLoad)
//                        {
//                            mate.Hp = hpLoad;
//                        }

//                        if (ServerManager.Instance.ChannelId != 51 || session.Character.MapId == 130 ||
//                            session.Character.MapId == 131)
//                        {
//                            if (inv.ItemVNum == 1242 || inv.ItemVNum == 5582)
//                            {
//                                session.CurrentMapInstance?.Broadcast(
//                                    mate.GenerateRc(hpLoad - mate.Hp));
//                                mate.Hp = hpLoad;
//                            }
//                            else if (inv.ItemVNum == 1243 || inv.ItemVNum == 5583)
//                            {
//                                mate.Mp = mpLoad;
//                            }
//                            else if (inv.ItemVNum == 1244 || inv.ItemVNum == 5584)
//                            {
//                                session.CurrentMapInstance?.Broadcast(
//                                    mate.GenerateRc(hpLoad - mate.Hp));
//                                mate.Hp = hpLoad;
//                                mate.Mp = mpLoad;
//                            }
//                        }

//                        session.SendPacket(mate.GenerateStatInfo());
//                    }

//                    break;
//            }
//        }

//        #endregion
//    }
//}
/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Data;
using System;
using System.Linq;
using OpenNos.Domain;
using OpenNos.Core;
using OpenNos.Core.Handling;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Packets.ClientPackets;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using OpenNos.GameObject.Networking;
using static OpenNos.Domain.BCardType;
using System.IO;

namespace OpenNos.GameObject
{
    public class PotionItem : Item
    {
        #region Instantiation

        public PotionItem(ItemDTO item) : base(item)
        {
        }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte option = 0, string[] packetsplit = null)
        {
            if ((DateTime.Now - session.Character.LastPotion).TotalMilliseconds < 750)
            {
                return;
            }
            session.Character.LastPotion = DateTime.Now;
            if (session.Character.Hp == session.Character.HPLoad() && session.Character.Mp == session.Character.MPLoad())
            {
                return;
            }
            if (session.Character.Hp <= 0)
            {
                return;
            }
            switch (VNum)
            {
                // Full HP
                case 1242:
                case 5582:
                    if  (ServerManager.Instance.ChannelId != 51)
                    {
                        session.CurrentMapInstance?.Broadcast(session.Character.GenerateRc((int)session.Character.HPLoad() - session.Character.Hp));
                        session.Character.Hp = (int)session.Character.HPLoad();
                        foreach (Mate mate in session.Character.Mates.Where(m => m.IsTeamMember))
                        {
                            mate.Hp = mate.HpLoad();
                            session.CurrentMapInstance?.Broadcast(mate.GenerateRc(mate.HpLoad() - mate.Hp));
                        }
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateStat());
                    }
                    break;

                // Full MP
                case 1243:
                case 5583:
                    if (ServerManager.Instance.ChannelId != 51)
                    {
                        {
                            session.Character.Mp = (int)session.Character.MPLoad();
                            session.Character.Mates.Where(m => m.IsTeamMember).ToList().ForEach(m => m.Mp = m.MpLoad());
                        }
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateStat());
                    }
                    break;

                // Full Mp & Hp
                case 1244:
                case 5584:
                    if (ServerManager.Instance.ChannelId != 51)
                    {
                        {
                            session.CurrentMapInstance?.Broadcast(session.Character.GenerateRc((int)session.Character.HPLoad() - session.Character.Hp));
                            session.Character.Hp = (int)session.Character.HPLoad();
                            session.Character.Mp = (int)session.Character.MPLoad();
                        }
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateStat());
                    }
                    break;

                default:
                    int hpHeal = session.Character.Hp + Hp > session.Character.HPLoad() ? (int)session.Character.HPLoad() - session.Character.Hp : Hp;
                    session.CurrentMapInstance?.Broadcast(session.Character.GenerateRc(hpHeal));
                    session.Character.Hp += hpHeal;
                    session.Character.Mp += session.Character.Mp + Mp > session.Character.MPLoad() ? (int)session.Character.MPLoad() - session.Character.Mp : Mp;

                    foreach (Mate mate in session.Character.Mates.Where(m => m.IsTeamMember))
                    {
                        int mateHpHeal = mate.Hp + Hp > mate.HpLoad() ? mate.HpLoad() - mate.Hp : Hp;
                        mate.Hp += mateHpHeal;
                        mate.Mp += mate.Mp + Mp > mate.MpLoad() ? mate.MpLoad() : Mp;
                        session.CurrentMapInstance?.Broadcast(mate.GenerateRc(mateHpHeal));
                    }
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    session.SendPacket(session.Character.GenerateStat());
                    break;
            }
        }

        #endregion
    }
}