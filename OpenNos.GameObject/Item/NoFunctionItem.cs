﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.Data;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class NoFunctionItem : Item
    {
        #region Instantiation

        public NoFunctionItem(ItemDTO item) : base(item)
        {
        }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte Option = 0, string[] packetsplit = null)
        {
            switch (VNum)
            {
                case 2427:
                    {
                        int rnd0 = ServerManager.RandomNumber(0, 1000);
                        short[] vnums0 = null;
                        if (rnd0 < 100)
                        {
                            vnums0 = new short[] { 4705, 4706, 4707, 4708 };
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            session.Character.GiftAdd(vnums0[ServerManager.RandomNumber(0, 4)], 1);
                        }
                        else
                        {
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                        break;
                    }

                case 2431:
                    {
                        int rnd0 = ServerManager.RandomNumber(0, 1000);
                        short[] vnums0 = null;
                        if (rnd0 < 10)
                        {
                            vnums0 = new short[] { 4713, 4714, 4715, 4716 };
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            session.Character.GiftAdd(vnums0[ServerManager.RandomNumber(0, 4)], 1);
                        }
                        else
                        {
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                        break;
                    }

                case 2429:
                    {
                        int rnd01 = ServerManager.RandomNumber(0, 1000);
                        short[] vnums01 = null;
                        if (rnd01 < 100)
                        {
                            vnums01 = new short[] { 4709, 4710, 4711, 4712 };
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            session.Character.GiftAdd(vnums01[ServerManager.RandomNumber(0, 4)], 1);
                        }
                        else
                        {
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                        break;
                    }


            }

            switch (Effect)
            {
                case 10:
                    {
                        switch (EffectValue)
                        {
                            case 1:
                                if (session.Character.Inventory.CountItem(1036) < 1 || session.Character.Inventory.CountItem(1013) < 1)
                                {
                                    return;
                                }
                                session.Character.Inventory.RemoveItemAmount(1036);
                                session.Character.Inventory.RemoveItemAmount(1013);
                                if (ServerManager.RandomNumber() < 25)
                                {
                                    switch (ServerManager.RandomNumber(0, 2))
                                    {
                                        case 0:
                                            session.Character.GiftAdd(1015, 5);
                                            break;
                                        case 1:
                                            session.Character.GiftAdd(1016, 5);
                                            break;
                                    }
                                }
                                break;
                            case 2:
                                if (session.Character.Inventory.CountItem(1038) < 1 || session.Character.Inventory.CountItem(1013) < 1)
                                {
                                    return;
                                }
                                session.Character.Inventory.RemoveItemAmount(1038);
                                session.Character.Inventory.RemoveItemAmount(1013);
                                if (ServerManager.RandomNumber() < 25)
                                {
                                    switch (ServerManager.RandomNumber(0, 4))
                                    {
                                        case 0:
                                            session.Character.GiftAdd(1031, 1);
                                            break;
                                        case 1:
                                            session.Character.GiftAdd(1032, 1);
                                            break;
                                        case 2:
                                            session.Character.GiftAdd(1033, 1);
                                            break;
                                        case 3:
                                            session.Character.GiftAdd(1034, 1);
                                            break;
                                    }
                                }
                                break;
                            case 3:
                                if (session.Character.Inventory.CountItem(1037) < 1 || session.Character.Inventory.CountItem(1013) < 1)
                                {
                                    return;
                                }
                                session.Character.Inventory.RemoveItemAmount(1037);
                                session.Character.Inventory.RemoveItemAmount(1013);
                                if (ServerManager.RandomNumber() < 25)
                                {
                                    switch (ServerManager.RandomNumber(0, 17))
                                    {
                                        case 0:
                                        case 1:
                                        case 2:
                                        case 3:
                                        case 4:
                                            session.Character.GiftAdd(1017, 1);
                                            break;
                                        case 5:
                                        case 6:
                                        case 7:
                                        case 8:
                                            session.Character.GiftAdd(1018, 1);
                                            break;
                                        case 9:
                                        case 10:
                                        case 11:
                                            session.Character.GiftAdd(1019, 1);
                                            break;
                                        case 12:
                                        case 13:
                                            session.Character.GiftAdd(1020, 1);
                                            break;
                                        case 14:
                                            session.Character.GiftAdd(1021, 1);
                                            break;
                                        case 15:
                                            session.Character.GiftAdd(1022, 1);
                                            break;
                                        case 16:
                                            session.Character.GiftAdd(1023, 1);
                                            break;
                                    }
                                }
                                break;
                        }

                        session.Character.GiftAdd(1014, (byte)ServerManager.RandomNumber(50, 85));
                    }
                    break;
                default:
                    Logger.Warn(string.Format(Language.Instance.GetMessageFromKey("NO_HANDLER_ITEM"), GetType(), VNum, Effect, EffectValue));
                    break;
            }
        }

        #endregion
    }
}