﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using OpenNos.PathFinder;
using static OpenNos.Domain.BCardType;

namespace OpenNos.GameObject
{
    public class Mate : MateDTO
    {
        #region Members

        private NpcMonster _monster;

        private bool _noAttack;
        private bool _noMove;
        private Character _owner;
        public readonly object PveLockObject;

        #endregion

        #region Instantiation

        public Mate(MateDTO input)
        {
            PveLockObject = new object();
            Buff = new ThreadSafeSortedList<short, Buff>();
            Attack = input.Attack;
            CanPickUp = input.CanPickUp;
            CharacterId = input.CharacterId;
            Defence = input.Defence;
            Direction = input.Direction;
            Experience = input.Experience;
            Hp = input.Hp;
            IsSummonable = input.IsSummonable;
            IsTeamMember = input.IsTeamMember;
            Level = input.Level;
            Loyalty = input.Loyalty;
            MapX = input.MapX;
            MapY = input.MapY;
            MateId = input.MateId;
            MateType = input.MateType;
            Mp = input.Mp;
            Name = input.Name;
            NpcMonsterVNum = input.NpcMonsterVNum;
            Skin = input.Skin;
            GenerateMateTransportId();
            EquipmentBCards = new ThreadSafeGenericList<BCard>();
        }

        public Mate(Character owner, NpcMonster npcMonster, byte level, MateType matetype)
        {
            PveLockObject = new object();
            Buff = new ThreadSafeSortedList<short, Buff>();
            NpcMonsterVNum = npcMonster.NpcMonsterVNum;
            Monster = npcMonster;
            Level = level;
            Hp = MaxHp;
            Mp = MaxMp;
            Name = npcMonster.Name;
            MateType = matetype;
            Loyalty = 1000;
            PositionY = (short) (owner.PositionY + 1);
            PositionX = (short) (owner.PositionX + 1);
            MapX = (short) (owner.PositionX + 1);
            MapY = (short) (owner.PositionY + 1);
            Direction = 2;
            CharacterId = owner.CharacterId;
            Owner = owner;
            GenerateMateTransportId();
            EquipmentBCards = new ThreadSafeGenericList<BCard>();
        }

        #endregion

        #region Properties
        
        public ThreadSafeGenericList<BCard> EquipmentBCards { get; set; }

        public DateTime LastDeath { get; set; }

        public DateTime LastHealth { get; set; }

        public DateTime LastSanguinamento { get; set; }

        public bool IsAlive { get; set; }

        public ItemInstance ArmorInstance { get; set; }

        public ItemInstance BootsInstance { get; set; }

        public ThreadSafeSortedList<short, Buff> Buff { get; }

        public int BaseDamage => BaseDamageLoad();

        public ItemInstance GlovesInstance { get; set; }

        public bool IsSitting { get; set; }

        public bool IsUsingSp { get; set; }

        public DateTime LastSpeedChange { get; set; }

        public int MagicalDefense => MagicalDefenseLoad();

        public int MateTransportId { get; set; }

        public int MaxHp => HpLoad();

        public int MaxMp => MpLoad();

        public int MeleeDefense => MeleeDefenseLoad();

        public int MeleeDefenseDodge => MeleeDefenseDodgeLoad();

        public NpcMonster Monster
        {
            get => _monster ?? ServerManager.GetNpc(NpcMonsterVNum);

            set => _monster = value;
        }

        public Character Owner
        {
            get => _owner ?? ServerManager.Instance.GetSessionByCharacterId(CharacterId)?.Character;
            set => _owner = value;
        }

        public byte PetId { get; set; }

        public short PositionX { get; set; }

        public short PositionY { get; set; }

        public int RangeDefense => RangeDefenseLoad();

        public int RangeDefenseDodge => RangeDefenseDodgeLoad();

        public Skill[] Skills { get; set; }

        public byte Speed
        { get
            {
                byte bonusSpeed = (byte) (GetBuff(CardType.Move, (byte) AdditionalTypes.Move.SetMovementNegated)[0]
                                          + GetBuff(CardType.Move,
                                              (byte) AdditionalTypes.Move.MovementSpeedIncreased)[0]
                                          + GetBuff(CardType.Move,
                                              (byte) AdditionalTypes.Move.MovementSpeedDecreased)[0]);

                if (SpInstance != null)
                {
                    bonusSpeed += SpInstance.Item.Speed;
                }

                if (Owner.IsUsingBlessingMate)
                {
                    bonusSpeed += 2;
                }

                if (Monster.Speed + bonusSpeed > 59)
                {
                    return 59;
                }

                return (byte) (Monster.Speed + bonusSpeed);
            }
            set
            {
                LastSpeedChange = DateTime.Now;
                Monster.Speed = value > 59 ? (byte) 59 : value;
            }
        }

        public ItemInstance SpInstance { get; set; }

        public ItemInstance WeaponInstance { get; set; }
        public DateTime LastMonsterAggro { get; set; }
        public Node[][] BrushFireJagged { get; set; }

        #endregion

        #region Methods

        public void AddBuff(Buff indicator)
        {
            if (indicator?.Card != null)
            {
                Buff[indicator.Card.CardId] = indicator;
                indicator.RemainingTime = indicator.Card.Duration;
                indicator.Start = DateTime.Now;

                indicator.Card.BCards.ForEach(c => c.ApplyBCards(this));
                Observable.Timer(TimeSpan.FromMilliseconds(indicator.Card.Duration * 100)).Subscribe(o =>
                {
                    RemoveBuff(indicator.Card.CardId);
                    if (indicator.Card.TimeoutBuff != 0
                        && ServerManager.RandomNumber() < indicator.Card.TimeoutBuffChance)
                    {
                        AddBuff(new Buff(indicator.Card.TimeoutBuff, Monster.Level));
                    }
                });
                Owner.Session.SendPacket($"bf 2 {MateTransportId} 0.{indicator.Card.CardId}.{(indicator.RemainingTime)} {Level}");
                _noAttack |= indicator.Card.BCards.Any(s =>
                    s.Type == (byte) CardType.SpecialAttack
                    && s.SubType.Equals((byte) AdditionalTypes.SpecialAttack.NoAttack / 10));
                _noMove |= indicator.Card.BCards.Any(s =>
                    s.Type == (byte) CardType.Move
                    && s.SubType.Equals((byte) AdditionalTypes.Move.MovementImpossible / 10));
            }
        }

        public void RemoveBuff(short id, bool removePermaBuff = false)
        {
            Buff indicator = Buff[id];
            if (indicator != null)
            {
                Owner.Session.SendPacket($"bf 2 {MateTransportId} 0.{indicator.Card.CardId}.0 {Level}");

                if (Buff[indicator.Card.CardId] != null)
                {
                    Buff.Remove(id);
                }
            }
        }

        public void GenerateMateTransportId()
        {
            int nextId = ServerManager.Instance.MateIds.Count > 0 ? ServerManager.Instance.MateIds.Last() + 1 : 10000;
            ServerManager.Instance.MateIds.Add(nextId);
            MateTransportId = nextId;
        }

        public string GenerateCMode(short morphId) => $"c_mode 2 {MateTransportId} {morphId} 0 0";

        public string GenerateCond() => $"cond 2 {MateTransportId} 0 0 {Speed}";

        public string GenerateEInfo()
        {
            int FireResistance = 0;
            int WaterResistance = 0;
            int LightResistance = 0;
            int DarkResistance = 0;
            int Element = 0;
            int AttackUpgrade = 0;
            int DamageMinimum = 0;
            int DamageMaximum = 0;
            int Concentrate = 0;
            int CriticalLuckRate = 0;
            int CriticalRate = 0;
            int CloseDefence = 0;
            int DefenceDodge = 0;
            int DistanceDefence = 0;
            int DistanceDefenceDodge = 0;
            int MagicDefence = 0;
            int DefenceUpgrade = 0;

            if (GlovesInstance != null)
            {
                FireResistance += GlovesInstance.FireResistance + GlovesInstance.Item.FireResistance;
                WaterResistance += GlovesInstance.WaterResistance + GlovesInstance.Item.WaterResistance;
                LightResistance += GlovesInstance.LightResistance + GlovesInstance.Item.LightResistance;
                DarkResistance += GlovesInstance.DarkResistance + GlovesInstance.Item.DarkResistance;
            }
            if (BootsInstance != null)
            {
                FireResistance += BootsInstance.FireResistance + BootsInstance.Item.FireResistance;
                WaterResistance += BootsInstance.WaterResistance + BootsInstance.Item.WaterResistance;
                LightResistance += BootsInstance.LightResistance + BootsInstance.Item.LightResistance;
                DarkResistance += BootsInstance.DarkResistance + BootsInstance.Item.DarkResistance;
            }
            if (SpInstance != null && IsUsingSp)
            {
                FireResistance += SpInstance.FireResistance + SpInstance.Item.FireResistance;
                WaterResistance += SpInstance.WaterResistance + SpInstance.Item.WaterResistance;
                LightResistance += SpInstance.LightResistance + SpInstance.Item.LightResistance;
                DarkResistance += SpInstance.DarkResistance + SpInstance.Item.DarkResistance;
                Element = SpInstance.Item.Element;
            }
            if (ArmorInstance != null)
            {
                Item armorOrigin = ArmorInstance?.PartnerItem != 0 ? ServerManager.GetItem(ArmorInstance.PartnerItem) : null;
                if(armorOrigin != null)
                {
                    FireResistance += ArmorInstance.FireResistance + armorOrigin.FireResistance;
                    WaterResistance += ArmorInstance.WaterResistance + armorOrigin.WaterResistance;
                    LightResistance += ArmorInstance.LightResistance + armorOrigin.LightResistance;
                    DarkResistance += ArmorInstance.DarkResistance + armorOrigin.DarkResistance;
                    CloseDefence += ArmorInstance.CloseDefence;
                    DefenceDodge += ArmorInstance.DefenceDodge;
                    DistanceDefence += ArmorInstance.DistanceDefence;
                    DistanceDefenceDodge += ArmorInstance.DistanceDefenceDodge;
                    MagicDefence += ArmorInstance.MagicDefence;
                    DefenceUpgrade += ArmorInstance.Upgrade;
                }
                
            }
            if (WeaponInstance != null)
            {
                Item weaponOrigin = WeaponInstance?.PartnerItem != 0 ? ServerManager.GetItem(WeaponInstance.PartnerItem) : null;
                if(weaponOrigin != null)
                {
                FireResistance += WeaponInstance.FireResistance + weaponOrigin.FireResistance;
                WaterResistance += WeaponInstance.WaterResistance + weaponOrigin.WaterResistance;
                LightResistance += WeaponInstance.LightResistance + weaponOrigin.LightResistance;
                DarkResistance += WeaponInstance.DarkResistance + weaponOrigin.DarkResistance;
                AttackUpgrade += WeaponInstance.Upgrade;
                DamageMinimum += (WeaponInstance.DamageMinimum);
                DamageMaximum += (WeaponInstance.DamageMaximum);
                Concentrate += (weaponOrigin.HitRate);
                CriticalLuckRate += weaponOrigin.CriticalLuckRate;
                CriticalRate += weaponOrigin.CriticalRate;

                }
            }
            if(MateType == MateType.Partner && IsUsingSp)
            {
                return $"e_info 10 {NpcMonsterVNum} {Level} {Element} {Monster.AttackClass} {SpInstance.Item.ElementRate} {AttackUpgrade} {Monster.DamageMinimum + BaseDamage + DamageMinimum } {Monster.DamageMaximum + BaseDamage + DamageMaximum} {Monster.Concentrate + Concentrate} {CriticalLuckRate} {CriticalRate} {DefenceUpgrade} {CloseDefence + Monster.CloseDefence + MeleeDefense} {DefenceDodge + Monster.DefenceDodge + MeleeDefenseDodge} {DistanceDefence + Monster.DistanceDefence + RangeDefense} {DistanceDefenceDodge + Monster.DistanceDefenceDodge + RangeDefenseDodge} {MagicDefence + Monster.MagicDefence + MagicalDefense} {FireResistance} {WaterResistance} {LightResistance} {DarkResistance} {Monster.MaxHP} {Monster.MaxMP} -1 {Name.Replace(' ', '^')}";
            }
            return $"e_info 10 {NpcMonsterVNum} {Level} {Monster.Element} {Monster.AttackClass} {Monster.ElementRate} {Monster.AttackUpgrade} {Monster.DamageMinimum} {Monster.DamageMaximum} {Monster.Concentrate} {Monster.CriticalChance} {Monster.CriticalRate} {Monster.DefenceUpgrade} {Monster.CloseDefence} {Monster.DefenceDodge} {Monster.DistanceDefence} {Monster.DistanceDefenceDodge} {Monster.MagicDefence} {Monster.FireResistance} {Monster.WaterResistance} {Monster.LightResistance} {Monster.DarkResistance} {Monster.MaxHP} {Monster.MaxMP} -1 {Name.Replace(' ', '^')}";
        }
        public string GenerateIn(bool foe = false, bool isAct4 = false)
        {
            string name = Name.Replace(' ', '^');
            if (foe)
            {
                name = "!§$%&/()=?*+~#";
            }

            int faction = 0;
            if (isAct4)
            {
                faction = (byte) Owner.Faction + 2;
            }

            return
                $"in 2 {NpcMonsterVNum} {MateTransportId} {PositionX} {PositionY} {Direction} {(int) (Hp / (float) MaxHp * 100)} {(int) (Mp / (float) MaxMp * 100)} 0 {faction} 3 {CharacterId} 1 0 {(IsUsingSp && SpInstance != null ? SpInstance.Item.Morph : (Skin != 0 ? Skin : -1))} {name} 1 0 {(IsUsingSp ? 1 : 0 )} {(IsUsingSp && SpInstance != null ? (SpInstance.Rare > 0 ? SpInstance.PartnerToSkill(SpInstance.ItemVNum)[0] : 0) : 0)} {(IsUsingSp && SpInstance != null ? (SpInstance.Upgrade > 0 ? SpInstance.PartnerToSkill(SpInstance.ItemVNum)[1] : 0) : 0)} {(IsUsingSp && SpInstance != null ? (SpInstance.SpStoneUpgrade > 0 ? SpInstance.PartnerToSkill(SpInstance.ItemVNum)[2] : 0) : 0)} 0 0 0 0";
        }

        public string GenerateOut() => $"out 2 {MateTransportId}";

        public string GenerateRest()
        {
            IsSitting = !IsSitting;
            return $"rest 2 {MateTransportId} {(IsSitting ? 1 : 0)}";
        }

        public string GenerateScPacket()
        {
            double xp = XpLoad();
            if (xp > int.MaxValue)
            {
                xp = (int) (xp / 100);
            }
            int FireResistance = 0;
            int WaterResistance = 0;
            int LightResistance = 0;
            int DarkResistance = 0;
            int Element = 0;

            if(GlovesInstance != null)
            {
                FireResistance += GlovesInstance.FireResistance + GlovesInstance.Item.FireResistance;
                WaterResistance += GlovesInstance.WaterResistance + GlovesInstance.Item.WaterResistance;
                LightResistance += GlovesInstance.LightResistance + GlovesInstance.Item.LightResistance;
                DarkResistance += GlovesInstance.DarkResistance + GlovesInstance.Item.DarkResistance;
            }
            if (BootsInstance != null)
            {
                FireResistance += BootsInstance.FireResistance + BootsInstance.Item.FireResistance;
                WaterResistance += BootsInstance.WaterResistance + BootsInstance.Item.WaterResistance;
                LightResistance += BootsInstance.LightResistance + BootsInstance.Item.LightResistance;
                DarkResistance += BootsInstance.DarkResistance + BootsInstance.Item.DarkResistance;
            }
            if (SpInstance != null && IsUsingSp)
            {
                FireResistance += SpInstance.FireResistance + SpInstance.Item.FireResistance;
                WaterResistance += SpInstance.WaterResistance + SpInstance.Item.WaterResistance;
                LightResistance += SpInstance.LightResistance + SpInstance.Item.LightResistance;
                DarkResistance += SpInstance.DarkResistance + SpInstance.Item.DarkResistance;
                Element = SpInstance.Item.Element;
            }
            if (ArmorInstance != null)
            {
                if (ArmorInstance.Item != null)
                {
                    FireResistance += ArmorInstance.FireResistance + ArmorInstance.Item.FireResistance;
                    WaterResistance += ArmorInstance.WaterResistance + ArmorInstance.Item.WaterResistance;
                    LightResistance += ArmorInstance.LightResistance + ArmorInstance.Item.LightResistance;
                    DarkResistance += ArmorInstance.DarkResistance + ArmorInstance.Item.DarkResistance;
                }
            }
            if (WeaponInstance != null)
            {
                if (WeaponInstance.Item != null)
                {
                    FireResistance += WeaponInstance.FireResistance + WeaponInstance.Item.FireResistance;
                    WaterResistance += WeaponInstance.WaterResistance + WeaponInstance.Item.WaterResistance;
                    LightResistance += WeaponInstance.LightResistance + WeaponInstance.Item.LightResistance;
                    DarkResistance += WeaponInstance.DarkResistance + WeaponInstance.Item.DarkResistance;
                }
            }

            switch (MateType)
            {
                case MateType.Partner:
                    return
                        $"sc_n {PetId} {NpcMonsterVNum} {MateTransportId} {Level} {Loyalty} {Experience} {(WeaponInstance != null ? $"{WeaponInstance.ItemVNum}.{WeaponInstance.Rare}.{WeaponInstance.Upgrade}" : "-1")} {(ArmorInstance != null ? $"{ArmorInstance.ItemVNum}.{ArmorInstance.Rare}.{ArmorInstance.Upgrade}" : "-1")} {(GlovesInstance != null ? $"{GlovesInstance.ItemVNum}.0.0" : "-1")} {(BootsInstance != null ? $"{BootsInstance.ItemVNum}.0.0" : "-1")} 0 0 1 {WeaponInstance?.Upgrade ?? 0} {Monster.DamageMinimum + BaseDamage + (WeaponInstance?.DamageMinimum ?? 0)} {Monster.DamageMaximum + BaseDamage + (WeaponInstance?.DamageMaximum ?? 0)} {Monster.Concentrate + (WeaponInstance?.HitRate ?? 0)} {Monster.CriticalChance + (WeaponInstance?.CriticalLuckRate ?? 0)} {Monster.CriticalRate + (WeaponInstance?.CriticalRate ?? 0)} {ArmorInstance?.Upgrade ?? 0} {Monster.CloseDefence + MeleeDefense + (ArmorInstance?.CloseDefence ?? 0) + (GlovesInstance?.CloseDefence ?? 0) + (BootsInstance?.CloseDefence ?? 0)} {Monster.DefenceDodge + MeleeDefenseDodge + (ArmorInstance?.DefenceDodge ?? 0) + (GlovesInstance?.DefenceDodge ?? 0) + (BootsInstance?.DefenceDodge ?? 0)} {Monster.DistanceDefence + RangeDefense + (ArmorInstance?.DistanceDefence ?? 0) + (GlovesInstance?.DistanceDefence ?? 0) + (BootsInstance?.DistanceDefence ?? 0)} {Monster.DistanceDefenceDodge + RangeDefenseDodge + (ArmorInstance?.DistanceDefenceDodge ?? 0) + (GlovesInstance?.DistanceDefenceDodge ?? 0) + (BootsInstance?.DistanceDefenceDodge ?? 0)} {Monster.MagicDefence + MagicalDefense + (ArmorInstance?.MagicDefence ?? 0) + (GlovesInstance?.MagicDefence ?? 0) + (BootsInstance?.MagicDefence ?? 0)} {Element} {Monster.FireResistance + FireResistance } {Monster.WaterResistance + WaterResistance} {Monster.LightResistance + LightResistance} {(Monster.DarkResistance + DarkResistance)} {Hp} {MaxHp} {Mp} {MaxMp} 0 {xp} {(IsUsingSp ? SpInstance.Item.Name.Replace(' ', '^') : Name.Replace(' ', '^'))} {(IsUsingSp && SpInstance != null ? SpInstance.Item.Morph : Skin != 0 ? Skin : -1)} {(IsSummonable ? 1 : 0)} {(SpInstance != null ? $"{SpInstance.ItemVNum}.100" : "-1")} {(SpInstance != null ? $"{(SpInstance.Rare > 0 ? $"{(SpInstance.PartnerToSkill(SpInstance.ItemVNum)[0])}.{SpInstance.Rare}" : "0.0")} {(SpInstance.Upgrade > 0 ? $"{(SpInstance.PartnerToSkill(SpInstance.ItemVNum)[1])}.{SpInstance.Upgrade}" : "0.0")} {(SpInstance.SpStoneUpgrade > 0 ? $"{(SpInstance.PartnerToSkill(SpInstance.ItemVNum)[2])}.{SpInstance.SpStoneUpgrade}" : "0.0")}" : "-1 -1 -1")}";
                    //$"sc_n {PetId} {NpcMonsterVNum} {MateTransportId} {Level} {Loyalty} {Experience} {(WeaponInstance != null ? $"{WeaponInstance.ItemVNum}.{WeaponInstance.Rare}.{WeaponInstance.Upgrade}" : "-1")} {(ArmorInstance != null ? $"{ArmorInstance.ItemVNum}.{ArmorInstance.Rare}.{ArmorInstance.Upgrade}" : "-1")} {(GlovesInstance != null ? $"{GlovesInstance.ItemVNum}.0.0" : "-1")} {(BootsInstance != null ? $"{BootsInstance.ItemVNum}.0.0" : "-1")} 0 0 1 {WeaponInstance?.Upgrade ?? 0} {Monster.DamageMinimum + BaseDamage + (WeaponInstance?.DamageMinimum ?? 0)} {Monster.DamageMaximum + BaseDamage + (WeaponInstance?.DamageMaximum ?? 0)} {Monster.Concentrate + (WeaponInstance?.HitRate ?? 0)} {Monster.CriticalChance + (WeaponInstance?.CriticalLuckRate ?? 0)} {Monster.CriticalRate + (WeaponInstance?.CriticalRate ?? 0)} {ArmorInstance?.Upgrade ?? 0} {Monster.CloseDefence + MeleeDefense + (ArmorInstance?.CloseDefence ?? 0) + (GlovesInstance?.CloseDefence ?? 0) + (BootsInstance?.CloseDefence ?? 0)} {Monster.DefenceDodge + MeleeDefenseDodge + (ArmorInstance?.DefenceDodge ?? 0) + (GlovesInstance?.DefenceDodge ?? 0) + (BootsInstance?.DefenceDodge ?? 0)} {Monster.DistanceDefence + RangeDefense + (ArmorInstance?.DistanceDefence ?? 0) + (GlovesInstance?.DistanceDefence ?? 0) + (BootsInstance?.DistanceDefence ?? 0)} {Monster.DistanceDefenceDodge + RangeDefenseDodge + (ArmorInstance?.DistanceDefenceDodge ?? 0) + (GlovesInstance?.DistanceDefenceDodge ?? 0) + (BootsInstance?.DistanceDefenceDodge ?? 0)} {Monster.MagicDefence + MagicalDefense + (ArmorInstance?.MagicDefence ?? 0) + (GlovesInstance?.MagicDefence ?? 0) + (BootsInstance?.MagicDefence ?? 0)} {Element} {Monster.FireResistance + FireResistance } {Monster.WaterResistance + WaterResistance} {Monster.LightResistance + LightResistance} {(Monster.DarkResistance + DarkResistance)} {Hp} {MaxHp} {Mp} {MaxMp} 0 {xp} {(IsUsingSp ? SpInstance.Item.Name.Replace(' ', '^') : Name.Replace(' ', '^'))} {(IsUsingSp && SpInstance != null ? SpInstance.Item.Morph : Skin != 0 ? Skin : -1)} 0 140 160 60 60 60 {(IsSummonable ? 1 : 0)} {(SpInstance != null ? $"{SpInstance.ItemVNum}.100" : "-1")} {(SpInstance != null ? $"{(SpInstance.Rare > 0 ? $"{(SpInstance.PartnerToSkill(SpInstance.ItemVNum)[0])}.{SpInstance.Rare}" : "0.0")} {(SpInstance.Upgrade > 0 ? $"{(SpInstance.PartnerToSkill(SpInstance.ItemVNum)[1])}.{SpInstance.Upgrade}" : "0.0")} {(SpInstance.SpStoneUpgrade > 0 ? $"{(SpInstance.PartnerToSkill(SpInstance.ItemVNum)[2])}.{SpInstance.SpStoneUpgrade}" : "0.0")}" : "-1 -1 -1")}";

                case MateType.Pet:
                    return
                        $"sc_p {PetId} {NpcMonsterVNum} {MateTransportId} {Level} {Loyalty} {Experience} 0 {Monster.AttackUpgrade + Attack} {Monster.DamageMinimum + BaseDamage} {Monster.DamageMaximum + BaseDamage} {Monster.Concentrate} {Monster.CriticalChance} {Monster.CriticalRate} {Monster.DefenceUpgrade + Defence} {Monster.CloseDefence + MeleeDefense} {Monster.DefenceDodge + MeleeDefenseDodge} {Monster.DistanceDefence + RangeDefense} {Monster.DistanceDefenceDodge + RangeDefenseDodge} {Monster.MagicDefence + MagicalDefense} {Monster.Element} {Monster.FireResistance} {Monster.WaterResistance} {Monster.LightResistance} {Monster.DarkResistance} {Hp} {MaxHp} {Mp} {MaxMp} 0 {xp} {(CanPickUp ? 1 : 0)} {Name.Replace(' ', '^')} {(IsSummonable ? 1 : 0)}";
                    //$"sc_p {PetId} {NpcMonsterVNum} {MateTransportId} {Level} {Loyalty} {Experience} 0 {Monster.AttackUpgrade + Attack} {Monster.DamageMinimum + BaseDamage} {Monster.DamageMaximum + BaseDamage} {Monster.Concentrate} {Monster.CriticalChance} {Monster.CriticalRate} {Monster.DefenceUpgrade + Defence} {Monster.CloseDefence + MeleeDefense} {Monster.DefenceDodge + MeleeDefenseDodge} {Monster.DistanceDefence + RangeDefense} {Monster.DistanceDefenceDodge + RangeDefenseDodge} {Monster.MagicDefence + MagicalDefense} {Monster.Element} {Monster.FireResistance} {Monster.WaterResistance} {Monster.LightResistance} {Monster.DarkResistance} {Hp} {MaxHp} {Mp} {MaxMp} 0 {xp} {(CanPickUp ? 1 : 0)} {Name.Replace(' ', '^')} 1 0 100 0 100 {(IsSummonable ? 1 : 0)}";
            }

            return string.Empty;
        }

        public string GenerateStatInfo() =>
            $"st 2 {MateTransportId} {Level} 0 {(int) ((float) Hp / (float) MaxHp * 100)} {(int) ((float) Mp / (float) MaxMp * 100)} {Hp} {Mp}{Buff.GetAllItems().Aggregate(string.Empty, (current, buff) => current + $" {buff.Card.CardId}.{buff.Level}")}";

        
        public void GenerateXp(int xp)
        {
            if (Level < ServerManager.Instance.Configuration.MaxLevel)
            {

                if (Owner.IsUsingBlessingMate)
                {
                    xp *= 2;
                }

                Experience += xp;
                if (Experience >= XpLoad())
                {
                    if (Level + 1 < Owner.SwitchLevel())
                    {
                        Experience = (long) (Experience - XpLoad());
                        Level++;
                        Hp = MaxHp;
                        Mp = MaxMp;
                        Owner.MapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Npc, MateTransportId, 6),
                            PositionX, PositionY);
                        Owner.MapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Npc, MateTransportId, 198),
                            PositionX, PositionY);
                    }
                }
            }

            Owner.Session.SendPacket(GenerateScPacket());
        }

        public string GenPski()
        {
            string skills = string.Empty;
            if (IsUsingSp == true)
            {
                if (SpInstance != null)
                {
                    if (SpInstance.Rare > 0 && SpInstance.Upgrade > 0 && SpInstance.SpStoneUpgrade > 0)
                    {
                        Skills = new Skill[3];
                        Skills[0] = ServerManager.GetSkill((short)SpInstance.PartnerToSkill(SpInstance.ItemVNum)[0]);
                        Skills[1] = ServerManager.GetSkill((short)SpInstance.PartnerToSkill(SpInstance.ItemVNum)[1]);
                        Skills[2] = ServerManager.GetSkill((short)SpInstance.PartnerToSkill(SpInstance.ItemVNum)[2]);
                    }
                    else if (SpInstance.Rare > 0 && SpInstance.Upgrade > 0)
                    {
                        Skills = new Skill[2];
                        Skills[0] = ServerManager.GetSkill((short)SpInstance.PartnerToSkill(SpInstance.ItemVNum)[0]);
                        Skills[1] = ServerManager.GetSkill((short)SpInstance.PartnerToSkill(SpInstance.ItemVNum)[1]);
                    }
                    else if (SpInstance.Rare > 0 && SpInstance.SpStoneUpgrade > 0)
                    {
                        Skills = new Skill[2];
                        Skills[0] = ServerManager.GetSkill((short)SpInstance.PartnerToSkill(SpInstance.ItemVNum)[0]);
                        Skills[1] = ServerManager.GetSkill((short)SpInstance.PartnerToSkill(SpInstance.ItemVNum)[2]);
                    }
                    else if (SpInstance.Upgrade > 0 && SpInstance.SpStoneUpgrade > 0)
                    {
                        Skills = new Skill[2];
                        Skills[0] = ServerManager.GetSkill((short)SpInstance.PartnerToSkill(SpInstance.ItemVNum)[1]);
                        Skills[1] = ServerManager.GetSkill((short)SpInstance.PartnerToSkill(SpInstance.ItemVNum)[2]);
                    }

                    else if (SpInstance.Rare > 0)
                    {
                        Skills = new Skill[1];
                        Skills[0] = ServerManager.GetSkill((short)SpInstance.PartnerToSkill(SpInstance.ItemVNum)[0]);
                    }

                    else if (SpInstance.Upgrade > 0)
                    {
                        Skills = new Skill[1];
                        Skills[0] = ServerManager.GetSkill((short)SpInstance.PartnerToSkill(SpInstance.ItemVNum)[1]);
                    }

                    else if (SpInstance.SpStoneUpgrade > 0)
                    {
                        Skills = new Skill[1];
                        Skills[0] = ServerManager.GetSkill((short)SpInstance.PartnerToSkill(SpInstance.ItemVNum)[2]);
                    }
                }
            }
            //if (Skills != null)
            //{
            //    foreach (Skill s in Skills)
            //    {
            //        skills += $" {s.SkillVNum}";
            //    }
            //}

            return $"pski{skills}";
        }

        public int[] GetBuff(CardType type, byte subtype)
        {
            int value1 = 0;
            int value2 = 0;
            int value3 = 0;

            if (EquipmentBCards != null)
            {
                foreach (BCard entry in EquipmentBCards?.Where(s => s?.Type.Equals((byte)type) == true && s.SubType.Equals((byte)(subtype / 10))))
                {
                    if (entry.IsLevelScaled)
                    {
                        if (entry.IsLevelDivided)
                        {
                            value1 += Level / entry.FirstData;
                        }
                        else
                        {
                            value1 += entry.FirstData * Level;
                        }
                    }
                    else
                    {
                        value1 += entry.FirstData;
                    }
                    value2 += entry.SecondData;
                }
            }

            foreach (Buff buff in Buff.Where(s => s?.Card?.BCards != null))
            {
                foreach (BCard entry in buff.Card.BCards
                        .Where(s => s.Type.Equals((byte)type) && s.SubType.Equals((byte)(subtype / 10)) && (s.CastType != 1 || (s.CastType == 1 && buff.Start.AddMilliseconds(buff.Card.Delay * 100) < DateTime.Now))))
                {
                    if (entry.IsLevelScaled)
                    {
                        if (entry.IsLevelDivided)
                        {
                            value1 += buff.Level / entry.FirstData;
                        }
                        else
                        {
                            value1 += entry.FirstData * buff.Level;
                        }
                    }
                    else
                    {
                        value1 += entry.FirstData;
                    }

                    value2 += entry.SecondData;


                    if (buff.SenderId != 0)
                    {
                        value3 = buff.SenderId;
                    }
                }
            }

            return new[] { value1, value2, value3 };
        }

        public List<ItemInstance> GetInventory()
        {
            switch (PetId)
            {
                case 0:
                    return Owner.Inventory.Where(s => s.Type == InventoryType.FirstPartnerInventory);

                case 1:
                    return Owner.Inventory.Where(s => s.Type == InventoryType.SecondPartnerInventory);

                case 2:
                    return Owner.Inventory.Where(s => s.Type == InventoryType.ThirdPartnerInventory);
                case 3:
                    return Owner.Inventory.Where(s => s.Type == InventoryType.FourthPartnerInventory);

                case 4:
                    return Owner.Inventory.Where(s => s.Type == InventoryType.FifthPartnerInventory);

                case 5:
                    return Owner.Inventory.Where(s => s.Type == InventoryType.SixthPartnerInventory);
                case 6:
                    return Owner.Inventory.Where(s => s.Type == InventoryType.SeventhPartnerInventory);

                case 7:
                    return Owner.Inventory.Where(s => s.Type == InventoryType.EigthPartnerInventory);

                case 8:
                    return Owner.Inventory.Where(s => s.Type == InventoryType.NinethPartnerInventory);
                case 9:
                    return Owner.Inventory.Where(s => s.Type == InventoryType.TenthPartnerInventory);

                case 10:
                    return Owner.Inventory.Where(s => s.Type == InventoryType.EleventhPartnerInventory);

                case 11:
                    return Owner.Inventory.Where(s => s.Type == InventoryType.TwelvethPartnerInventory);
            }

            return new List<ItemInstance>();
        }

        public int HpLoad()
        {
            double multiplicator = 1.0;
            int hp = 0;

            multiplicator += GetBuff(CardType.BearSpirit, (byte) AdditionalTypes.BearSpirit.IncreaseMaximumHP)[0]
                             / 100D;
            multiplicator += GetBuff(CardType.MaxHPMP, (byte) AdditionalTypes.MaxHPMP.IncreasesMaximumHP)[0] / 100D;
            hp += GetBuff(CardType.MaxHPMP, (byte) AdditionalTypes.MaxHPMP.MaximumHPIncreased)[0];
            //hp -= GetBuff(CardType.MaxHPMP, (byte) AdditionalTypes.MaxHPMP.MaximumHPDecreased)[0];
            hp += GetBuff(CardType.MaxHPMP, (byte) AdditionalTypes.MaxHPMP.MaximumHPMPIncreased)[0];

            // Monster Bonus HP
            hp += (int) (Monster.MaxHP - MateHelper.Instance.HpData[Monster.Level]);

            return (int) ((MateHelper.Instance.HpData[Level] + hp) * multiplicator);
        }

        public int BaseDamageLoad()
        {
            return MateHelper.Instance.DamageData[GetMateType(), Level > 0 ? Level - 1 : 0];
        }
        public int MeleeDefenseLoad()
        {
            return MateHelper.Instance.MeleeDefenseData[GetMateType(), Level > 0 ? Level - 1 : 0];
        }
        public int MeleeDefenseDodgeLoad()
        {
            return MateHelper.Instance.MeleeDefenseDodgeData[GetMateType(), Level > 0 ? Level - 1 : 0];
        }
        public int RangeDefenseLoad()
        {
            return MateHelper.Instance.RangeDefenseData[GetMateType(), Level > 0 ? Level - 1 : 0];
        }
        public int RangeDefenseDodgeLoad()
        {
            return MateHelper.Instance.RangeDefenseDodgeData[GetMateType(), Level > 0 ? Level - 1 : 0];
        }
        public int MagicalDefenseLoad()
        {
            return MateHelper.Instance.MagicDefenseData[GetMateType(), Level > 0 ? Level - 1 : 0];
        }
        
        public string GenerateRc(int characterHealth) => $"rc 2 {MateTransportId} {characterHealth} 0";
        
        /// <summary>
        /// Checks if the current character is in range of the given position
        /// </summary>
        /// <param name="xCoordinate">The x coordinate of the object to check.</param>
        /// <param name="yCoordinate">The y coordinate of the object to check.</param>
        /// <param name="range">The range of the coordinates to be maximal distanced.</param>
        /// <returns>True if the object is in Range, False if not.</returns>
        public bool IsInRange(int xCoordinate, int yCoordinate, int range) =>
            Math.Abs(PositionX - xCoordinate) <= range && Math.Abs(PositionY - yCoordinate) <= range;

        public void LoadInventory()
        {
            List<ItemInstance> inv = GetInventory();
            if (inv.Count == 0)
            {
                return;
            }
            EquipmentBCards.Clear();

            WeaponInstance = inv.Find(s => s.Item.EquipmentSlot == EquipmentType.MainWeapon);
            ArmorInstance = inv.Find(s => s.Item.EquipmentSlot == EquipmentType.Armor);
            GlovesInstance = inv.Find(s => s.Item.EquipmentSlot == EquipmentType.Gloves);
            BootsInstance = inv.Find(s => s.Item.EquipmentSlot == EquipmentType.Boots);
            SpInstance = inv.Find(s => s.Item.EquipmentSlot == EquipmentType.Sp);
            if (WeaponInstance != null)
            {
                Item weaponOrigin = WeaponInstance?.PartnerItem != 0 ? ServerManager.GetItem(WeaponInstance.PartnerItem) : null;
                EquipmentBCards.AddRange(weaponOrigin?.BCards);

            }
            if (ArmorInstance != null)
            {
                Item armororigin = ArmorInstance?.PartnerItem != 0 ? ServerManager.GetItem(ArmorInstance.PartnerItem) : null;
                EquipmentBCards.AddRange(armororigin?.BCards);
            }

        }

        public int MpLoad()
        {
            int mp = 0;
            double multiplicator = 1.0;
            multiplicator += GetBuff(CardType.BearSpirit, (byte) AdditionalTypes.BearSpirit.IncreaseMaximumMP)[0]
                             / 100D;
            multiplicator += GetBuff(CardType.MaxHPMP, (byte) AdditionalTypes.MaxHPMP.IncreasesMaximumMP)[0] / 100D;
            mp += GetBuff(CardType.MaxHPMP, (byte) AdditionalTypes.MaxHPMP.MaximumMPIncreased)[0];
            mp -= GetBuff(CardType.MaxHPMP, (byte) AdditionalTypes.MaxHPMP.MaximumHPDecreased)[0];
            mp += GetBuff(CardType.MaxHPMP, (byte) AdditionalTypes.MaxHPMP.MaximumHPMPIncreased)[0];

            // Monster Bonus MP
            mp += (int) (Monster.MaxMP - (Monster.Race == 0
                             ? MateHelper.Instance.PrimaryMpData[Monster.Level]
                             : MateHelper.Instance.SecondaryMpData[Monster.Level]));

            return (int) (((Monster.Race == 0
                               ? MateHelper.Instance.PrimaryMpData[Level]
                               : MateHelper.Instance.SecondaryMpData[Level]) + mp) * multiplicator);
        }

        private void RemoveBuff(short id)
        {
            Buff indicator = Buff[id];

            if (indicator != null)
            {
                Buff.Remove(id);
                _noAttack &= !indicator.Card.BCards.Any(s =>
                    s.Type == (byte) CardType.SpecialAttack
                    && s.SubType.Equals((byte) AdditionalTypes.SpecialAttack.NoAttack / 10));
                _noMove &= !indicator.Card.BCards.Any(s =>
                    s.Type == (byte) CardType.Move
                    && s.SubType.Equals((byte) AdditionalTypes.Move.MovementImpossible / 10));
            }
        }

        private double XpLoad()
        {
            try
            {
                return MateHelper.Instance.XpData[Level - 1];
            }
            catch
            {
                return 0;
            }
        }

        #endregion

        public void UpdateBushFire()
        {
            if(PositionX < 1)
            {
                PositionX = 2; 
            }
            BrushFireJagged = BestFirstSearch.LoadBrushFireJagged(new GridPos
            {
                X = PositionX,
                Y = PositionY
            }, Owner.Session.CurrentMapInstance.Map.JaggedGrid);
        }

        public void GetDamage(int damage)
        {
            Owner.LastDefence = DateTime.Now;
            Owner.Dispose();

            Hp -= damage;
            if (Hp < 0)
            {
                Hp = 0;
            }
            Owner.GeneratePst();

        }

        private byte GetMateType()
        {
            byte b = 0;
            // Set b according to the desired Mate Type, Race or VNum
            if(MateType == MateType.Partner)
            {
                b = 1;
            }
            return b;
        }
    }
}