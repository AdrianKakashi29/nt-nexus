namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1v2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Character", "V1", c => c.Int(nullable: false));
            AddColumn("dbo.Character", "V2", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Character", "V2");
            DropColumn("dbo.Character", "V1");
        }
    }
}
