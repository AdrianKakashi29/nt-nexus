﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OpenNos.DAL.EF
{
    public class ReputationLog
    {

        public long CharacterId { get; set; }

        [MaxLength(255)]
        public string ReputationLogData { get; set; }

        [Key]
        public long ReputationLogId { get; set; }

        public DateTime Timestamp { get; set; }
    }
}