﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OpenNos.DAL.EF
{
    public class Darvo
    {

        #region Instantiation

        public Darvo()
        {
            DarvoList = new HashSet<DarvoList>();
        }

        #endregion

        #region Properties
        public virtual ICollection<DarvoList> DarvoList { get; set; }

        public byte Rare { get; set; }

        public byte Upgrade { get; set; }

        public short Price { get; set; }

        public DateTime? LastUpdate { get; set; }

        [Key]
        public short DarvoId { get; set; }

        #endregion
    }
}