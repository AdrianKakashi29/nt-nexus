﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using System.ComponentModel.DataAnnotations.Schema;

namespace OpenNos.DAL.EF
{
    public class MaestriaArmi
    {
        #region Properties
        
        public short ItemVNum { get; set; }
        
        [Index("IX_SlotAndType", 1, IsUnique = false, Order = 0)]
        public long AccountId { get; set; }

        public long XPMaestria { get; set; }

        public long XPMaestriaTotale { get; set; }

        public short LivelloMaestria { get; set; }

        public int MaestriaArmiId { get; set; }

        #endregion
    }
}