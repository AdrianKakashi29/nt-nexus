﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class DailyRewardsDAO : IDailyRewardsDAO
    {
        #region Methods


        #endregion
        public DailyRewardsDTO Insert(DailyRewardsDTO item)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    DailyRewards entity = new DailyRewards();
                    Mapper.Mappers.DailyRewardsMapper.ToDailyRewards(item, entity);
                    context.DailyRewards.Add(entity);
                    context.SaveChanges();
                    if (Mapper.Mappers.DailyRewardsMapper.ToDailyRewardsDTO(entity, item))
                    {
                        return item;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public IEnumerable<DailyRewardsDTO> LoadAll()
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<DailyRewardsDTO> result = new List<DailyRewardsDTO>();
                foreach (DailyRewards item in context.DailyRewards)
                {
                    DailyRewardsDTO dto = new DailyRewardsDTO();
                    Mapper.Mappers.DailyRewardsMapper.ToDailyRewardsDTO(item, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        public IEnumerable<DailyRewardsDTO> LoadByDay(short Day)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    List<DailyRewardsDTO> result = new List<DailyRewardsDTO>();
                    foreach (DailyRewards item in context.DailyRewards.Where(i => i.Day.Equals(Day))) 
                    {
                        DailyRewardsDTO dto = new DailyRewardsDTO();
                        Mapper.Mappers.DailyRewardsMapper.ToDailyRewardsDTO(item, dto);
                        result.Add(dto);
                    }

                    return result;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public DailyRewardsDTO LoadById(short id)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    DailyRewardsDTO dto = new DailyRewardsDTO();
                    if (Mapper.Mappers.DailyRewardsMapper.ToDailyRewardsDTO(context.DailyRewards.FirstOrDefault(i => i.DailyRewardsId.Equals(id)), dto))
                    {
                        return dto;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }
    }
}