﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class DarvoDAO : IDarvoDAO
    {

        protected static DarvoDTO Insert(DarvoDTO dto, OpenNosContext context)
        {
            Darvo entity = new Darvo();
            context.Set<Darvo>().Add(entity);
            context.SaveChanges();
            if (Mapper.Mappers.DarvoMapper.ToDarvoDTO(entity, dto))
            {
                return dto;
            }

            return null;
        }

        protected static DarvoDTO Update(Darvo entity, DarvoDTO inventory, OpenNosContext context)
        {
            if (entity != null)
            {
                if (Mapper.Mappers.DarvoMapper.ToDarvo(inventory, entity))
                {
                    context.SaveChanges();
                }
            }
            if (Mapper.Mappers.DarvoMapper.ToDarvoDTO(entity, inventory))
            {
                return inventory;
            }
            return null;
        }

        protected static DarvoDTO InsertOrUpdate(OpenNosContext context, DarvoDTO dto)
        {
            try
            {
                Darvo entity = context.Darvo.FirstOrDefault(c => c.DarvoId == dto.DarvoId);
                return entity == null ? Insert(dto, context) : Update(entity, dto, context);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public DarvoDTO Update(DarvoDTO dto)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    return InsertOrUpdate(context, dto);
                }
            }
            catch (Exception e)
            {
                Logger.Error($"Message: {e.Message}", e);
                return null;
            }
        }

        public DarvoDTO Insert(DarvoDTO item)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    Darvo entity = new Darvo();
                    Mapper.Mappers.DarvoMapper.ToDarvo(item, entity);
                    context.Darvo.Add(entity);
                    context.SaveChanges();
                    if (Mapper.Mappers.DarvoMapper.ToDarvoDTO(entity, item))
                    {
                        return item;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public IEnumerable<DarvoDTO> LoadAll()
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<DarvoDTO> result = new List<DarvoDTO>();
                foreach (Darvo Darvo in context.Darvo)
                {
                    DarvoDTO dto = new DarvoDTO();
                    Mapper.Mappers.DarvoMapper.ToDarvoDTO(Darvo, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        public DarvoDTO LoadById(short id)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    DarvoDTO dto = new DarvoDTO();
                    if (Mapper.Mappers.DarvoMapper.ToDarvoDTO(context.Darvo.SingleOrDefault(s => s.DarvoId.Equals(id)), dto))
                    {
                        return dto;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }
    }
}