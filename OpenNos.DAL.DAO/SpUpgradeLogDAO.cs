﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class SpUpgradeLogDAO : ISpUgradeLogDAO
    {
        #region Methods

        public DeleteResult Delete(long spupgradeid)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    SpUpgradeLog spupgradelog = context.SpUpgradeLog.FirstOrDefault(c => c.SpUpgradeId.Equals(spupgradeid));

                    if (spupgradelog != null)
                    {
                        context.SpUpgradeLog.Remove(spupgradelog);
                        context.SaveChanges();
                    }

                    return DeleteResult.Deleted;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("DELETE_ERROR"), spupgradeid, e.Message), e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref SpUpgradeLogDTO spupgradelog)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    long SpUpgradeLog = spupgradelog.SpUpgradeId;
                    SpUpgradeLog entity = context.SpUpgradeLog.FirstOrDefault(c => c.SpUpgradeId.Equals(SpUpgradeLog));

                    if (entity == null)
                    {
                        spupgradelog = insert(spupgradelog, context);
                        return SaveResult.Inserted;
                    }

                    spupgradelog = update(entity, spupgradelog, context);
                    return SaveResult.Updated;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("UPDATE_FAMILYLOG_ERROR"), spupgradelog.SpUpgradeId, e.Message), e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<SpUpgradeLogDTO> LoadByCharacterId(long SpUpgradeLogId)
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<SpUpgradeLogDTO> result = new List<SpUpgradeLogDTO>();
                foreach (SpUpgradeLog spupgradelog in context.SpUpgradeLog.Where(fc => fc.SpUpgradeId.Equals(SpUpgradeLogId)))
                {
                    SpUpgradeLogDTO dto = new SpUpgradeLogDTO();
                    Mapper.Mappers.SpUpgradeLogMapper.ToSpUpgradeLogDTO(spupgradelog, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        private static SpUpgradeLogDTO insert(SpUpgradeLogDTO spupgradedto, OpenNosContext context)
        {
            SpUpgradeLog entity = new SpUpgradeLog();
            Mapper.Mappers.SpUpgradeLogMapper.ToSpUpgradeLog(spupgradedto, entity);
            context.SpUpgradeLog.Add(entity);
            context.SaveChanges();
            if (Mapper.Mappers.SpUpgradeLogMapper.ToSpUpgradeLogDTO(entity, spupgradedto))
            {
                return spupgradedto;
            }

            return null;
        }

        private static SpUpgradeLogDTO update(SpUpgradeLog entity, SpUpgradeLogDTO spupglog, OpenNosContext context)
        {
            if (entity != null)
            {
                Mapper.Mappers.SpUpgradeLogMapper.ToSpUpgradeLog(spupglog, entity);
                context.SaveChanges();
            }

            if (Mapper.Mappers.SpUpgradeLogMapper.ToSpUpgradeLogDTO(entity, spupglog))
            {
                return spupglog;
            }

            return null;
        }

        #endregion
    }
}