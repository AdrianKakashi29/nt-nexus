﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class QuestDAO : IQuestDAO
    {
        #region Methods

        public void Insert(List<QuestDTO> quests)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    context.Configuration.AutoDetectChangesEnabled = false;
                    foreach (QuestDTO quest in quests)
                    {
                        Quest entity = new Quest();
                        Mapper.Mappers.QuestMapper.ToQuest(quest, entity);
                        context.Quest.Add(entity);
                    }
                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }

        public QuestDTO Insert(QuestDTO quest)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    Quest entity = new Quest();
                    Mapper.Mappers.QuestMapper.ToQuest(quest, entity);
                    context.Quest.Add(entity);
                    context.SaveChanges();
                    if (Mapper.Mappers.QuestMapper.ToQuestDTO(entity, quest))
                    {
                        return quest;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public List<QuestDTO> LoadAll()
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<QuestDTO> result = new List<QuestDTO>();
                foreach (Quest quest in context.Quest)
                {
                    QuestDTO dto = new QuestDTO();
                    Mapper.Mappers.QuestMapper.ToQuestDTO(quest, dto);
                    result.Add(dto);
                }
                return result;
            }
        }


        public QuestDTO LoadById(long questId)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    QuestDTO dto = new QuestDTO();
                    if (Mapper.Mappers.QuestMapper.ToQuestDTO(context.Quest.FirstOrDefault(i => i.QuestId.Equals(questId)), dto))
                    {
                        return dto;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        #endregion
    }
}