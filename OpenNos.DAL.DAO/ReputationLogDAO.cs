﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class ReputationLogDAO : IReputationLogDAO
    {
        #region Methods

        public DeleteResult Delete(long reputationlogid)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    ReputationLog raidlog = context.ReputationLog.FirstOrDefault(c => c.ReputationLogId.Equals(reputationlogid));

                    if (raidlog != null)
                    {
                        context.ReputationLog.Remove(raidlog);
                        context.SaveChanges();
                    }

                    return DeleteResult.Deleted;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("DELETE_ERROR"), reputationlogid, e.Message), e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref ReputationLogDTO raidlog)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    long raidlogid = raidlog.ReputationLogId;
                    ReputationLog entity = context.ReputationLog.FirstOrDefault(c => c.ReputationLogId.Equals(raidlogid));

                    if (entity == null)
                    {
                        raidlog = insert(raidlog, context);
                        return SaveResult.Inserted;
                    }

                    raidlog = update(entity, raidlog, context);
                    return SaveResult.Updated;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("UPDATE_FAMILYLOG_ERROR"), raidlog.ReputationLogId, e.Message), e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<ReputationLogDTO> LoadByCharacterId(long RaidLogId)
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<ReputationLogDTO> result = new List<ReputationLogDTO>();
                foreach (ReputationLog raidlog in context.ReputationLog.Where(fc => fc.ReputationLogId.Equals(RaidLogId)))
                {
                    ReputationLogDTO dto = new ReputationLogDTO();
                    Mapper.Mappers.ReputationLogMapper.ToReputationLogDTO(raidlog, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        private static ReputationLogDTO insert(ReputationLogDTO raidlogdto, OpenNosContext context)
        {
            ReputationLog entity = new ReputationLog();
            Mapper.Mappers.ReputationLogMapper.ToReputationLog(raidlogdto, entity);
            context.ReputationLog.Add(entity);
            context.SaveChanges();
            if (Mapper.Mappers.ReputationLogMapper.ToReputationLogDTO(entity, raidlogdto))
            {
                return raidlogdto;
            }

            return null;
        }

        private static ReputationLogDTO update(ReputationLog entity, ReputationLogDTO raidlogdto, OpenNosContext context)
        {
            if (entity != null)
            {
                Mapper.Mappers.ReputationLogMapper.ToReputationLog(raidlogdto, entity);
                context.SaveChanges();
            }

            if (Mapper.Mappers.ReputationLogMapper.ToReputationLogDTO(entity, raidlogdto))
            {
                return raidlogdto;
            }

            return null;
        }

        #endregion
    }
}