﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class SpLogDAO : ISpLogDAO
    {
        #region Methods

        public DeleteResult Delete(long spupgradeid)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    SpLog spupgradelog = context.SpLog.FirstOrDefault(c => c.SpId.Equals(spupgradeid));

                    if (spupgradelog != null)
                    {
                        context.SpLog.Remove(spupgradelog);
                        context.SaveChanges();
                    }

                    return DeleteResult.Deleted;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("DELETE_ERROR"), spupgradeid, e.Message), e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref SpLogDTO spupgradelog)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    long SpUpgradeLog = spupgradelog.SpId;
                    SpLog entity = context.SpLog.FirstOrDefault(c => c.SpId.Equals(SpUpgradeLog));

                    if (entity == null)
                    {
                        spupgradelog = insert(spupgradelog, context);
                        return SaveResult.Inserted;
                    }

                    spupgradelog = update(entity, spupgradelog, context);
                    return SaveResult.Updated;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("UPDATE_FAMILYLOG_ERROR"), spupgradelog.SpId, e.Message), e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<SpLogDTO> LoadByCharacterId(long SpUpgradeLogId)
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<SpLogDTO> result = new List<SpLogDTO>();
                foreach (SpLog spupgradelog in context.SpLog.Where(fc => fc.SpId.Equals(SpUpgradeLogId)))
                {
                    SpLogDTO dto = new SpLogDTO();
                    Mapper.Mappers.SpLogMapper.ToSpLogDTO(spupgradelog, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        private static SpLogDTO insert(SpLogDTO spupgradedto, OpenNosContext context)
        {
            SpLog entity = new SpLog();
            Mapper.Mappers.SpLogMapper.ToSpLog(spupgradedto, entity);
            context.SpLog.Add(entity);
            context.SaveChanges();
            if (Mapper.Mappers.SpLogMapper.ToSpLogDTO(entity, spupgradedto))
            {
                return spupgradedto;
            }

            return null;
        }

        private static SpLogDTO update(SpLog entity, SpLogDTO spupglog, OpenNosContext context)
        {
            if (entity != null)
            {
                Mapper.Mappers.SpLogMapper.ToSpLog(spupglog, entity);
                context.SaveChanges();
            }

            if (Mapper.Mappers.SpLogMapper.ToSpLogDTO(entity, spupglog))
            {
                return spupglog;
            }

            return null;
        }

        #endregion
    }
}