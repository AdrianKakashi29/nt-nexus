﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class RuotaFortunaDAO : IRuotaFortunaDAO
    {
        #region Methods

        public RuotaFortunaDTO Insert(RuotaFortunaDTO item)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    RuotaFortuna entity = new RuotaFortuna();
                    Mapper.Mappers.RuotaFortunaMapper.ToRuotaFortuna(item, entity);
                    context.RuotaFortuna.Add(entity);
                    context.SaveChanges();
                    if (Mapper.Mappers.RuotaFortunaMapper.ToRuotaFortunaDTO(entity, item))
                    {
                        return item;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public IEnumerable<RuotaFortunaDTO> LoadAll()
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<RuotaFortunaDTO> result = new List<RuotaFortunaDTO>();
                foreach (RuotaFortuna item in context.RuotaFortuna)
                {
                    RuotaFortunaDTO dto = new RuotaFortunaDTO();
                    Mapper.Mappers.RuotaFortunaMapper.ToRuotaFortunaDTO(item, dto);
                    result.Add(dto);
                }
                return result;
            }
        }


        public RuotaFortunaDTO LoadByVnum(short vnum)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    RuotaFortunaDTO dto = new RuotaFortunaDTO();
                    if (Mapper.Mappers.RuotaFortunaMapper.ToRuotaFortunaDTO(context.RuotaFortuna.FirstOrDefault(i => i.ItemGeneratedVNum.Equals(vnum)), dto))
                    {
                        return dto;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public IEnumerable<RuotaFortunaDTO> LoadBySettimana(byte id)
        {

            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<RuotaFortunaDTO> result = new List<RuotaFortunaDTO>();
                foreach (RuotaFortuna item in context.RuotaFortuna.Where(s => s.Settimana == id))
                {
                    RuotaFortunaDTO dto = new RuotaFortunaDTO();
                    Mapper.Mappers.RuotaFortunaMapper.ToRuotaFortunaDTO(item, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        public RuotaFortunaDTO LoadById(short id)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    RuotaFortunaDTO dto = new RuotaFortunaDTO();
                    if (Mapper.Mappers.RuotaFortunaMapper.ToRuotaFortunaDTO(context.RuotaFortuna.FirstOrDefault(i => i.RuotaDellaFortunaId.Equals(id)), dto))
                    {
                        return dto;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }
        
        #endregion
    }
}