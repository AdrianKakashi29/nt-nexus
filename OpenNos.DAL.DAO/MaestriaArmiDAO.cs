﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;

using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class MaestriaArmiDAO : IMaestriaArmiDAO
    {
        public DeleteResult Delete(short Vnum, short CharacterId)
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                MaestriaArmi entity = context.Set<MaestriaArmi>().FirstOrDefault(c => c.AccountId.Equals(CharacterId) && c.ItemVNum.Equals(Vnum));
                if (entity != null)
                {
                    context.Set<MaestriaArmi>().Remove(entity);
                    context.SaveChanges();
                }
                return DeleteResult.Deleted;
            }
        }
        #region Methods

        public void Insert(List<MaestriaArmiDTO> MaestriaArmis)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    context.Configuration.AutoDetectChangesEnabled = false;
                    foreach (MaestriaArmiDTO Item in MaestriaArmis)
                    {
                        MaestriaArmi entity = new MaestriaArmi();
                        Mapper.Mappers.MaestriaArmiMapper.ToMaestriaArmi(Item, entity);
                        context.MaestriaArmi.Add(entity);
                    }
                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }

        public MaestriaArmiDTO Insert(MaestriaArmiDTO maestriaarma)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    if (context.MaestriaArmi.FirstOrDefault(c => c.MaestriaArmiId.Equals(maestriaarma.MaestriaArmiId)) == null)
                    {
                        MaestriaArmi entity = new MaestriaArmi();
                        Mapper.Mappers.MaestriaArmiMapper.ToMaestriaArmi(maestriaarma, entity);
                        context.MaestriaArmi.Add(entity);
                        context.SaveChanges();
                        if (Mapper.Mappers.MaestriaArmiMapper.ToMaestriaArmiDTO(entity, maestriaarma))
                        {
                            return maestriaarma;
                        }

                        return null;
                    }
                    return new MaestriaArmiDTO();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public MaestriaArmiDTO LoadById(short MaestriArmiId)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    MaestriaArmiDTO dto = new MaestriaArmiDTO();
                    if (Mapper.Mappers.MaestriaArmiMapper.ToMaestriaArmiDTO(context.MaestriaArmi.FirstOrDefault(c => c.MaestriaArmiId.Equals(MaestriArmiId)), dto))
                    {
                        return dto;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public MaestriaArmiDTO LoadByVnumAndCharacterId(short Vnum, long CharacterId)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    MaestriaArmiDTO dto = new MaestriaArmiDTO();
                    if (Mapper.Mappers.MaestriaArmiMapper.ToMaestriaArmiDTO(context.MaestriaArmi.FirstOrDefault(c => c.ItemVNum.Equals(Vnum) && c.AccountId.Equals(CharacterId)), dto))
                    {
                        return dto;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        protected static MaestriaArmiDTO Insert(MaestriaArmiDTO dto, OpenNosContext context)
        {

            MaestriaArmi entity = new MaestriaArmi();
            Mapper.Mappers.MaestriaArmiMapper.ToMaestriaArmi(dto, entity);
            context.MaestriaArmi.Add(entity);
            context.SaveChanges();
            if (Mapper.Mappers.MaestriaArmiMapper.ToMaestriaArmiDTO(entity, dto))
            {
                return dto;
            }
            return null;
        }


        protected static MaestriaArmiDTO Update(MaestriaArmi entity, MaestriaArmiDTO inventory, OpenNosContext context)
        {
            if (entity != null)
            {
                if (Mapper.Mappers.MaestriaArmiMapper.ToMaestriaArmi(inventory, entity))
                {
                    context.SaveChanges();
                }
            }
            if (Mapper.Mappers.MaestriaArmiMapper.ToMaestriaArmiDTO(entity, inventory))
            {
                return inventory;
            }
            return null;
        }

        protected static MaestriaArmiDTO InsertOrUpdate(OpenNosContext context, MaestriaArmiDTO dto)
        {
            try
            {
                MaestriaArmi entity = context.MaestriaArmi.FirstOrDefault(c => c.AccountId == dto.AccountId && c.ItemVNum == dto.ItemVNum);
                return entity == null ? Insert(dto, context) : Update(entity, dto, context);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public MaestriaArmiDTO Update(MaestriaArmiDTO dto)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    return InsertOrUpdate(context, dto);
                }
            }
            catch (Exception e)
            {
                Logger.Error($"Message: {e.Message}", e);
                return null;
            }
        }

        #endregion
    }
}