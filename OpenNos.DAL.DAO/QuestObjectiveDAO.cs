﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class QuestObjectiveDAO : IQuestObjectiveDAO
    {
        #region Methods

        public void Insert(List<QuestObjectiveDTO> quests)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {

                    context.Configuration.AutoDetectChangesEnabled = false;
                    foreach (QuestObjectiveDTO objectives in quests)
                    {
                        QuestObjective entity = new QuestObjective();
                        Mapper.Mappers.QuestObjectMapper.ToQuestObject(objectives, entity);
                        context.QuestObjective.Add(entity);
                    }
                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }

        public QuestObjectiveDTO Insert(QuestObjectiveDTO quest)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    QuestObjective entity = new QuestObjective();
                    Mapper.Mappers.QuestObjectMapper.ToQuestObject(quest, entity);
                    context.QuestObjective.Add(entity);
                    context.SaveChanges();
                    if (Mapper.Mappers.QuestObjectMapper.ToQuestObjectiveDTO(entity, quest))
                    {
                        return quest;
                    }
                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public List<QuestObjectiveDTO> LoadAll()
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<QuestObjectiveDTO> result = new List<QuestObjectiveDTO>();
                foreach (QuestObjective entity in context.QuestObjective)
                {
                    QuestObjectiveDTO dto = new QuestObjectiveDTO();
                    Mapper.Mappers.QuestObjectMapper.ToQuestObjectiveDTO(entity, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        public IEnumerable<QuestObjectiveDTO> LoadByQuestId(long questId)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    List<QuestObjectiveDTO> result = new List<QuestObjectiveDTO>();
                    foreach (QuestObjective entity in context.QuestObjective.Where(i => i.QuestId == questId))
                    {
                        QuestObjectiveDTO output = new QuestObjectiveDTO();
                        Mapper.Mappers.QuestObjectMapper.ToQuestObjectiveDTO(entity, output);
                        result.Add(output);
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }
    }
        #endregion
}