﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class AdminLogDAO : IAdminLogDAO
    {
        #region Methods

        public DeleteResult Delete(long spupgradeid)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    AdminLog spupgradelog = context.AdminLog.FirstOrDefault(c => c.AdminLogId.Equals(spupgradeid));

                    if (spupgradelog != null)
                    {
                        context.AdminLog.Remove(spupgradelog);
                        context.SaveChanges();
                    }

                    return DeleteResult.Deleted;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("DELETE_ERROR"), spupgradeid, e.Message), e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref AdminLogDTO spupgradelog)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    long SpUpgradeLog = spupgradelog.AdminLogId;
                    AdminLog entity = context.AdminLog.FirstOrDefault(c => c.AdminLogId.Equals(SpUpgradeLog));

                    if (entity == null)
                    {
                        spupgradelog = insert(spupgradelog, context);
                        return SaveResult.Inserted;
                    }

                    spupgradelog = update(entity, spupgradelog, context);
                    return SaveResult.Updated;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("UPDATE_FAMILYLOG_ERROR"), spupgradelog.AdminLogId, e.Message), e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<AdminLogDTO> LoadByCharacterId(long SpUpgradeLogId)
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<AdminLogDTO> result = new List<AdminLogDTO>();
                foreach (AdminLog spupgradelog in context.AdminLog.Where(fc => fc.AdminLogId.Equals(SpUpgradeLogId)))
                {
                    AdminLogDTO dto = new AdminLogDTO();
                    Mapper.Mappers.AdminLogMapper.ToAdminLogDTO(spupgradelog, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        private static AdminLogDTO insert(AdminLogDTO spupgradedto, OpenNosContext context)
        {
            AdminLog entity = new AdminLog();
            Mapper.Mappers.AdminLogMapper.ToAdminLog(spupgradedto, entity);
            context.AdminLog.Add(entity);
            context.SaveChanges();
            if (Mapper.Mappers.AdminLogMapper.ToAdminLogDTO(entity, spupgradedto))
            {
                return spupgradedto;
            }

            return null;
        }

        private static AdminLogDTO update(AdminLog entity, AdminLogDTO spupglog, OpenNosContext context)
        {
            if (entity != null)
            {
                Mapper.Mappers.AdminLogMapper.ToAdminLog(spupglog, entity);
                context.SaveChanges();
            }

            if (Mapper.Mappers.AdminLogMapper.ToAdminLogDTO(entity, spupglog))
            {
                return spupglog;
            }

            return null;
        }

        #endregion
    }
}