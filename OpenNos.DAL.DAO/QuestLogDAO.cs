﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class QuestLogDAO : IQuestLogDAO
    {
        public SaveResult InsertOrUpdate(ref QuestLogDTO quest)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    long questId = quest.QuestId;
                    QuestLog entity = context.QuestLog.FirstOrDefault(c => c.QuestId.Equals(questId));

                    if (entity == null)
                    {
                        quest = Insert(quest, context);
                        return SaveResult.Inserted;
                    }

                    quest.QuestId = entity.QuestId;
                    quest = Update(entity, quest, context);
                    return SaveResult.Updated;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return SaveResult.Error;
            }
        }

        public QuestLogDTO Insert(QuestLogDTO quest, OpenNosContext context)
        {
            try
            {
                    QuestLog entity = new QuestLog();
                    Mapper.Mappers.QuestLogMapper.ToQuestLog(quest, entity);
                    context.QuestLog.Add(entity);
                    context.SaveChanges();
                    if (Mapper.Mappers.QuestLogMapper.ToQuestLogDTO(entity, quest))
                    {
                        return quest;
                    }

                    return null;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public QuestLogDTO Update(QuestLog old, QuestLogDTO replace, OpenNosContext context)
        {
            if (old != null)
            {
                Mapper.Mappers.QuestLogMapper.ToQuestLog(replace, old);
                context.SaveChanges();
            }

            if (Mapper.Mappers.QuestLogMapper.ToQuestLogDTO(old, replace))
            {
                return replace;
            }
            return null;
        }

        public QuestLogDTO LoadById(long id)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    List<QuestLogDTO> result = new List<QuestLogDTO>();
                    foreach (QuestLog entity in context.QuestLog.Where(i => i.Id == id))
                    {
                        QuestLogDTO output = new QuestLogDTO();
                        Mapper.Mappers.QuestLogMapper.ToQuestLogDTO(entity, output);
                        result.Add(output);
                    }
                    return result.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return null;
            }
        }

        public IEnumerable<QuestLogDTO> LoadByCharacterId(long characterId)
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<QuestLogDTO> result = new List<QuestLogDTO>();
                foreach (QuestLog entity in context.QuestLog.Where(i => i.Id == characterId))
                {
                    QuestLogDTO output = new QuestLogDTO();
                    Mapper.Mappers.QuestLogMapper.ToQuestLogDTO(entity, output);
                    result.Add(output);
                }
                return result;
            }
        }
    }
}