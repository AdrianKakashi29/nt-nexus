﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class LevelLogDAO : ILevelLogDAO
    {
        #region Methods

        public DeleteResult Delete(long levellogid)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    LevelLog levellog = context.LevelLog.FirstOrDefault(c => c.LevelLogId.Equals(levellogid));

                    if (levellog != null)
                    {
                        context.LevelLog.Remove(levellog);
                        context.SaveChanges();
                    }

                    return DeleteResult.Deleted;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("DELETE_ERROR"), levellogid, e.Message), e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref LevelLogDTO levellog)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    long levellogid = levellog.LevelLogId;
                    LevelLog entity = context.LevelLog.FirstOrDefault(c => c.LevelLogId.Equals(levellogid));

                    if (entity == null)
                    {
                        levellog = insert(levellog, context);
                        return SaveResult.Inserted;
                    }

                    levellog = update(entity, levellog, context);
                    return SaveResult.Updated;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("UPDATE_FAMILYLOG_ERROR"), levellog.LevelLogId, e.Message), e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<LevelLogDTO> LoadByCharacterId(long levellogid)
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<LevelLogDTO> result = new List<LevelLogDTO>();
                foreach (LevelLog levellog in context.LevelLog.Where(fc => fc.LevelLogId.Equals(levellogid)))
                {
                    LevelLogDTO dto = new LevelLogDTO();
                    Mapper.Mappers.LevelLogMapper.ToLevelLogDTO(levellog, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        private static LevelLogDTO insert(LevelLogDTO levellogdto, OpenNosContext context)
        {
            LevelLog entity = new LevelLog();
            Mapper.Mappers.LevelLogMapper.ToLevelLog(levellogdto, entity);
            context.LevelLog.Add(entity);
            context.SaveChanges();
            if (Mapper.Mappers.LevelLogMapper.ToLevelLogDTO(entity, levellogdto))
            {
                return levellogdto;
            }

            return null;
        }

        private static LevelLogDTO update(LevelLog entity, LevelLogDTO levellogdto, OpenNosContext context)
        {
            if (entity != null)
            {
                Mapper.Mappers.LevelLogMapper.ToLevelLog(levellogdto, entity);
                context.SaveChanges();
            }

            if (Mapper.Mappers.LevelLogMapper.ToLevelLogDTO(entity, levellogdto))
            {
                return levellogdto;
            }

            return null;
        }

        #endregion
    }
}