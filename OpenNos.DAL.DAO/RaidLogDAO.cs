﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class RaidLogDAO : IRaidLogDAO
    {
        #region Methods

        public DeleteResult Delete(long raidlogid)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    RaidLog raidlog = context.RaidLog.FirstOrDefault(c => c.RaidLogId.Equals(raidlogid));

                    if (raidlog != null)
                    {
                        context.RaidLog.Remove(raidlog);
                        context.SaveChanges();
                    }

                    return DeleteResult.Deleted;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("DELETE_ERROR"), raidlogid, e.Message), e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref RaidLogDTO raidlog)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    long raidlogid = raidlog.RaidLogId;
                    RaidLog entity = context.RaidLog.FirstOrDefault(c => c.RaidLogId.Equals(raidlogid));

                    if (entity == null)
                    {
                        raidlog = insert(raidlog, context);
                        return SaveResult.Inserted;
                    }

                    raidlog = update(entity, raidlog, context);
                    return SaveResult.Updated;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("UPDATE_FAMILYLOG_ERROR"), raidlog.RaidLogId, e.Message), e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<RaidLogDTO> LoadByCharacterId(long RaidLogId)
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<RaidLogDTO> result = new List<RaidLogDTO>();
                foreach (RaidLog raidlog in context.RaidLog.Where(fc => fc.RaidLogId.Equals(RaidLogId)))
                {
                    RaidLogDTO dto = new RaidLogDTO();
                    Mapper.Mappers.RaidLogMapper.ToRaidLogDTO(raidlog, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        private static RaidLogDTO insert(RaidLogDTO raidlogdto, OpenNosContext context)
        {
            RaidLog entity = new RaidLog();
            Mapper.Mappers.RaidLogMapper.ToRaidLog(raidlogdto, entity);
            context.RaidLog.Add(entity);
            context.SaveChanges();
            if (Mapper.Mappers.RaidLogMapper.ToRaidLogDTO(entity, raidlogdto))
            {
                return raidlogdto;
            }

            return null;
        }

        private static RaidLogDTO update(RaidLog entity, RaidLogDTO raidlogdto, OpenNosContext context)
        {
            if (entity != null)
            {
                Mapper.Mappers.RaidLogMapper.ToRaidLog(raidlogdto, entity);
                context.SaveChanges();
            }

            if (Mapper.Mappers.RaidLogMapper.ToRaidLogDTO(entity, raidlogdto))
            {
                return raidlogdto;
            }

            return null;
        }

        #endregion
    }
}