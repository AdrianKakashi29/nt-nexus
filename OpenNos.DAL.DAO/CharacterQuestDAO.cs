﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class CharacterQuestDAO : ICharacterQuestDAO
    {
        #region Methods

        public DeleteResult Delete(long characterId, long questId)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    CharacterQuest charQuest = context.CharacterQuest.FirstOrDefault(i => i.CharacterId == characterId && i.QuestId == questId);
                    if (charQuest != null)
                    {
                        context.CharacterQuest.Remove(charQuest);
                        context.SaveChanges();
                    }
                    return DeleteResult.Deleted;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return DeleteResult.Error;
            }
        }

        public IEnumerable<CharacterQuestDTO> LoadByCharacterId(long characterId)
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {

                List<CharacterQuestDTO> result = new List<CharacterQuestDTO>();
                foreach (CharacterQuest entity in context.CharacterQuest.Where(i => i.CharacterId == characterId))
                {
                    CharacterQuestDTO output = new CharacterQuestDTO();
                    Mapper.Mappers.CharacterQuestMapper.ToCharacterQuestDTO(entity, output);
                    result.Add(output);
                }
                return result;
            }
        }
        public CharacterQuestDTO InsertOrUpdate(CharacterQuestDTO cellonOption)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    long cellonOptionId = cellonOption.QuestId;
                    long characterquestcharacterid = cellonOption.CharacterId;
                    CharacterQuest entity = context.CharacterQuest.FirstOrDefault(c => c.QuestId.Equals(cellonOptionId) && c.CharacterId.Equals(characterquestcharacterid));

                    if (entity == null)
                    {
                        return insert(cellonOption, context);
                    }
                    return update(entity, cellonOption, context);
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("INSERT_ERROR"), cellonOption, e.Message), e);
                return cellonOption;
            }
        }


        private static CharacterQuestDTO insert(CharacterQuestDTO cellonOption, OpenNosContext context)
        {
            CharacterQuest entity = new CharacterQuest();
            Mapper.Mappers.CharacterQuestMapper.ToCharacterQuest(cellonOption, entity);
            context.CharacterQuest.Add(entity);
            context.SaveChanges();
            if (Mapper.Mappers.CharacterQuestMapper.ToCharacterQuestDTO(entity, cellonOption))
            {
                return cellonOption;
            }

            return null;
        }


        private static CharacterQuestDTO update(CharacterQuest entity, CharacterQuestDTO cellonOption, OpenNosContext context)
        {
            if (entity != null)
            {
                Mapper.Mappers.CharacterQuestMapper.ToCharacterQuest(cellonOption, entity);
                context.SaveChanges();
            }

            if (Mapper.Mappers.CharacterQuestMapper.ToCharacterQuestDTO(entity, cellonOption))
            {
                return cellonOption;
            }

            return null;
        }

        /*
        public IEnumerable<Guid> LoadKeysByCharacterId(long characterId)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    return context.CharacterQuest.Where(i => i.CharacterId == characterId).Select(c => c.Id).ToList();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }*/

        #endregion
    }
}