﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class DarvoListDAO : IDarvoListDAO
    {
        public DarvoListDTO Insert(DarvoListDTO darvoList)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    DarvoList entity = new DarvoList();
                    Mapper.Mappers.DarvoListMapper.ToDarvoList(darvoList, entity);
                    context.DarvoList.Add(entity);
                    context.SaveChanges();
                    if (Mapper.Mappers.DarvoListMapper.ToDarvoListDTO(entity, darvoList))
                    {
                        return darvoList;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public IEnumerable<DarvoListDTO> LoadAll()
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<DarvoListDTO> result = new List<DarvoListDTO>();
                foreach (DarvoList darvolist in context.DarvoList)
                {
                    DarvoListDTO dto = new DarvoListDTO();
                    Mapper.Mappers.DarvoListMapper.ToDarvoListDTO(darvolist, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        public IEnumerable<DarvoListDTO> LoadByDarvo(short darvoid)
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<DarvoListDTO> result = new List<DarvoListDTO>();
                foreach (DarvoList darvoList in context.DarvoList.Where(s => s.DarvoId.Equals(darvoid)))
                {
                    DarvoListDTO dto = new DarvoListDTO();
                    Mapper.Mappers.DarvoListMapper.ToDarvoListDTO(darvoList, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        public DarvoListDTO LoadByVnum(short darvoitem)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    DarvoListDTO dto = new DarvoListDTO();
                    if (Mapper.Mappers.DarvoListMapper.ToDarvoListDTO(context.DarvoList.FirstOrDefault(s => s.ItemVNum.Equals(darvoitem)), dto))
                    {
                        return dto;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }

        public DarvoListDTO LoadById(short darvolistid)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    DarvoListDTO dto = new DarvoListDTO();
                    if (Mapper.Mappers.DarvoListMapper.ToDarvoListDTO(context.DarvoList.FirstOrDefault(s => s.DarvoListId.Equals(darvolistid)), dto))
                    {
                        return dto;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                return null;
            }
        }
    }
}