using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class MaestriaArmiMapper
    {
        #region Methods

        public static bool ToMaestriaArmi(MaestriaArmiDTO input, MaestriaArmi output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.AccountId = input.AccountId;
            output.ItemVNum = input.ItemVNum;
            output.LivelloMaestria = input.LivelloMaestria;
            output.MaestriaArmiId = input.MaestriaArmiId;
            output.XPMaestria = input.XPMaestria;
            output.XPMaestriaTotale = input.XPMaestriaTotale;
            return true;
        }

        public static bool ToMaestriaArmiDTO(MaestriaArmi input, MaestriaArmiDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.AccountId = input.AccountId;
            output.ItemVNum = input.ItemVNum;
            output.LivelloMaestria = input.LivelloMaestria;
            output.MaestriaArmiId = input.MaestriaArmiId;
            output.XPMaestria = input.XPMaestria;
            output.XPMaestriaTotale = input.XPMaestriaTotale;
            return true;
        }

        #endregion
    }
}