using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class AccountMapper
    {
        #region Methods

        public static bool ToAccount(AccountDTO input, Account output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.AccountId = input.AccountId;
            output.Authority = input.Authority;
            output.Email = input.Email;
            output.Name = input.Name;
            output.Password = input.Password;
            output.RegistrationIP = input.RegistrationIP;
            output.VerificationToken = input.VerificationToken;
            output.ClientLanguage = input.ClientLanguage;
            output.LivelloMaestria = input.LivelloMaestria;
            output.LivelloMaestriaXP = input.LivelloMaestriaXP;
            output.CoinsLoliTale = input.CoinsLoliTale;
            output.CodiceBlocco = input.CodiceBlocco;
            output.UsaBlocco = input.UsaBlocco;
            output.V1 = input.V1;
            output.V2 = input.V2;
            return true;
        }

        public static bool ToAccountDTO(Account input, AccountDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.AccountId = input.AccountId;
            output.Authority = input.Authority;
            output.Email = input.Email;
            output.Name = input.Name;
            output.Password = input.Password;
            output.RegistrationIP = input.RegistrationIP;
            output.VerificationToken = input.VerificationToken;
            output.ClientLanguage = input.ClientLanguage;
            output.LivelloMaestria = input.LivelloMaestria;
            output.LivelloMaestriaXP = input.LivelloMaestriaXP;
            output.CoinsLoliTale = input.CoinsLoliTale;
            output.CodiceBlocco = input.CodiceBlocco;
            output.UsaBlocco = input.UsaBlocco;
            output.V1 = input.V1;
            output.V2 = input.V2;
            return true;
        }

        #endregion
    }
}