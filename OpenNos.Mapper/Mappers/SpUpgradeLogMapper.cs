using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class SpUpgradeLogMapper
    {
        #region Methods

        public static bool ToSpUpgradeLog(SpUpgradeLogDTO input, SpUpgradeLog output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.CharacterId = input.CharacterId;
            output.SpUpgradeData = input.SpUpgradeData;
            output.SpUpgradeId = input.SpUpgradeId;
            output.Timestamp = input.Timestamp;
            return true;
        }

        public static bool ToSpUpgradeLogDTO(SpUpgradeLog input, SpUpgradeLogDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.CharacterId = input.CharacterId;
            output.SpUpgradeData = input.SpUpgradeData;
            output.SpUpgradeId = input.SpUpgradeId;
            output.Timestamp = input.Timestamp;
            return true;
        }

        #endregion
    }
}