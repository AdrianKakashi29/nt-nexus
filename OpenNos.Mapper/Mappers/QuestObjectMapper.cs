using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class QuestObjectMapper
    {
        #region Methods

        public static bool ToQuestObject(QuestObjectiveDTO input, QuestObjective output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.Data = input.Data;
            output.DropRate = input.DropRate;
            output.Objective = input.Objective;
            output.ObjectiveIndex = input.ObjectiveIndex;
            output.QuestId = input.QuestId;
            output.QuestObjectiveId = input.QuestObjectiveId;
            output.SpecialData = input.SpecialData;
            return true;
        }

        public static bool ToQuestObjectiveDTO(QuestObjective input, QuestObjectiveDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.Data = input.Data;
            output.DropRate = input.DropRate;
            output.Objective = input.Objective;
            output.ObjectiveIndex = input.ObjectiveIndex;
            output.QuestId = input.QuestId;
            output.QuestObjectiveId = input.QuestObjectiveId;
            output.SpecialData = input.SpecialData;
            return true;
        }

        #endregion
    }
}