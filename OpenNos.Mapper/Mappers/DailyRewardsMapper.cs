using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class DailyRewardsMapper
    {
        #region Methods

        public static bool ToDailyRewards(DailyRewardsDTO input, DailyRewards output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.Amount = input.Amount;
            output.CardVnum = input.CardVnum;
            output.DailyRewardsId = input.DailyRewardsId;
            output.Day = input.Day;
            output.Design = input.Design;
            output.Duration = input.Duration;
            output.ItemVNum = input.ItemVNum;
            output.Probability = input.Probability;
            output.Rare = input.Rare;
            output.Upgrade = input.Upgrade;
            output.BoostGold = input.BoostGold;
            output.BoostExp = input.BoostExp;
            output.BoostRep = input.BoostRep;
            output.BoostResource = input.BoostResource;
            return true;
        }

        public static bool ToDailyRewardsDTO(DailyRewards input, DailyRewardsDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.Amount = input.Amount;
            output.CardVnum = input.CardVnum;
            output.DailyRewardsId = input.DailyRewardsId;
            output.Day = input.Day;
            output.Design = input.Design;
            output.Duration = input.Duration;
            output.ItemVNum = input.ItemVNum;
            output.Probability = input.Probability;
            output.Rare = input.Rare;
            output.Upgrade = input.Upgrade;
            output.BoostGold = input.BoostGold;
            output.BoostExp = input.BoostExp;
            output.BoostRep = input.BoostRep;
            output.BoostResource = input.BoostResource;
            return true;
        }

        #endregion
    }
}