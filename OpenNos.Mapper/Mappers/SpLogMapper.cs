using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class SpLogMapper
    {
        #region Methods

        public static bool ToSpLog(SpLogDTO input, SpLog output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.CharacterId = input.CharacterId;
            output.SpData = input.SpData;
            output.SpId = input.SpId;
            output.Timestamp = input.Timestamp;
            return true;
        }

        public static bool ToSpLogDTO(SpLog input, SpLogDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.CharacterId = input.CharacterId;
            output.SpData = input.SpData;
            output.SpId = input.SpId;
            output.Timestamp = input.Timestamp;
            return true;
        }

        #endregion
    }
}