using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class AdminLogMapper
    {
        #region Methods

        public static bool ToAdminLog(AdminLogDTO input, AdminLog output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.AdminLogId = input.AdminLogId;
            output.CharacterId = input.CharacterId;
            output.AdminLogData = input.AdminLogData;
            output.Timestamp = input.Timestamp;
            return true;
        }

        public static bool ToAdminLogDTO(AdminLog input, AdminLogDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.AdminLogId = input.AdminLogId;
            output.CharacterId = input.CharacterId;
            output.AdminLogData = input.AdminLogData;
            output.Timestamp = input.Timestamp;
            return true;
        }

        #endregion
    }
}