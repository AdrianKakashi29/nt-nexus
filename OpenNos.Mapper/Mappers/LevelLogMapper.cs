using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class LevelLogMapper
    {
        #region Methods

        public static bool ToLevelLog(LevelLogDTO input, LevelLog output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.CharacterId = input.CharacterId;
            output.LevelLogData = input.LevelLogData;
            output.LevelLogId = input.LevelLogId;
            output.Timestamp = input.Timestamp;
            return true;
        }

        public static bool ToLevelLogDTO(LevelLog input, LevelLogDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.CharacterId = input.CharacterId;
            output.LevelLogData = input.LevelLogData;
            output.LevelLogId = input.LevelLogId;
            output.Timestamp = input.Timestamp;
            return true;
        }

        #endregion
    }
}