using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class DarvoListMapper
    {
        #region Methods

        public static bool ToDarvoList(DarvoListDTO input, DarvoList output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.DarvoId = input.DarvoId;
            output.DarvoListId = input.DarvoListId;
            output.ItemVNum = input.ItemVNum;
            return true;
        }

        public static bool ToDarvoListDTO(DarvoList input, DarvoListDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.DarvoId = input.DarvoId;
            output.DarvoListId = input.DarvoListId;
            output.ItemVNum = input.ItemVNum;
            return true;
        }

        #endregion
    }
}