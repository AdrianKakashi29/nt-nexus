using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class QuestRewardMapper
    {
        #region Methods

        public static bool ToQuestReward(QuestRewardDTO input, QuestReward output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.Amount = input.Amount;
            output.Data = input.Data;
            output.Design = input.Design;
            output.QuestId = input.QuestId;
            output.QuestRewardId = input.QuestRewardId;
            output.Rarity = input.Rarity;
            output.RewardType = input.RewardType;
            output.Upgrade = input.Upgrade;
            return true;
        }

        public static bool ToQuestRewardDTO(QuestReward input, QuestRewardDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.Amount = input.Amount;
            output.Data = input.Data;
            output.Design = input.Design;
            output.QuestId = input.QuestId;
            output.QuestRewardId = input.QuestRewardId;
            output.Rarity = input.Rarity;
            output.RewardType = input.RewardType;
            output.Upgrade = input.Upgrade;
            return true;
        }

        #endregion
    }
}