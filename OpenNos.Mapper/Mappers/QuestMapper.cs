using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class QuestMapper
    {
        #region Methods

        public static bool ToQuest(QuestDTO input, Quest output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.QuestType = input.QuestType;
            output.QuestId = input.QuestId;
            output.NextQuestId = input.NextQuestId;
            output.EndDialogId = input.EndDialogId;
            output.InfoId = input.InfoId;
            output.IsDaily = input.IsDaily;
            output.LevelMax = input.LevelMax;
            output.LevelMin = input.LevelMin;
            output.TargetMap = input.TargetMap;
            output.StartDialogId = input.StartDialogId;
            output.TargetX = input.TargetX;
            output.TargetY = input.TargetY;
            return true;
        }

        public static bool ToQuestDTO(Quest input, QuestDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.QuestType = input.QuestType;
            output.QuestId = input.QuestId;
            output.NextQuestId = input.NextQuestId;
            output.EndDialogId = input.EndDialogId;
            output.InfoId = input.InfoId;
            output.IsDaily = input.IsDaily;
            output.LevelMax = input.LevelMax;
            output.LevelMin = input.LevelMin;
            output.TargetMap = input.TargetMap;
            output.StartDialogId = input.StartDialogId;
            output.TargetX = input.TargetX;
            output.TargetY = input.TargetY;
            return true;
        }

        #endregion
    }
}