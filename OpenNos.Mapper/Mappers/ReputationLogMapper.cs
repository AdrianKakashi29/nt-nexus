using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class ReputationLogMapper
    {
        #region Methods

        public static bool ToReputationLog(ReputationLogDTO input, ReputationLog output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.CharacterId = input.CharacterId;
            output.ReputationLogData = input.ReputationData;
            output.ReputationLogId = input.ReputationLogId;
            output.Timestamp = input.Timestamp;
            return true;
        }

        public static bool ToReputationLogDTO(ReputationLog input, ReputationLogDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.CharacterId = input.CharacterId;
            output.ReputationData = input.ReputationLogData;
            output.ReputationLogId = input.ReputationLogId;
            output.Timestamp = input.Timestamp;
            return true;
        }

        #endregion
    }
}