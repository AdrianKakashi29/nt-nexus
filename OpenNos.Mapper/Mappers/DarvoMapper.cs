using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class DarvoMapper
    {
        #region Methods

        public static bool ToDarvo(DarvoDTO input, Darvo output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.DarvoId = input.DarvoId;
            output.Rare = input.Rare;
            output.Upgrade = input.Upgrade;
            output.Price = input.Price;
            output.LastUpdate = input.LastUpdate;
            return true;
        }

        public static bool ToDarvoDTO(Darvo input, DarvoDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.DarvoId = input.DarvoId;
            output.Rare = input.Rare;
            output.Upgrade = input.Upgrade;
            output.Price = input.Price;
            output.LastUpdate = input.LastUpdate;
            return true;
        }

        #endregion
    }
}