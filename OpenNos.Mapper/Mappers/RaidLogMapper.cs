using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class RaidLogMapper
    {
        #region Methods

        public static bool ToRaidLog(RaidLogDTO input, RaidLog output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.CharacterId = input.CharacterId;
            output.RaidLogData = input.RaidLogData;
            output.RaidLogId = input.RaidLogId;
            output.Timestamp = input.Timestamp;
            return true;
        }

        public static bool ToRaidLogDTO(RaidLog input, RaidLogDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.CharacterId = input.CharacterId;
            output.RaidLogData = input.RaidLogData;
            output.RaidLogId = input.RaidLogId;
            output.Timestamp = input.Timestamp;
            return true;
        }

        #endregion
    }
}