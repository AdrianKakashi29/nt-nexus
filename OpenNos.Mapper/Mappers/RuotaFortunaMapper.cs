using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class RuotaFortunaMapper
    {
        #region Methods

        public static bool ToRuotaFortuna(RuotaFortunaDTO input, RuotaFortuna output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.RuotaDellaFortunaId = input.RuotaDellaFortunaId;
            output.Probability = input.Probability;
            output.ItemGeneratedVNum = input.ItemGeneratedVNum;
            output.ItemGeneratedAmount = input.ItemGeneratedAmount;
            output.ItemGeneratedAmount = input.ItemGeneratedAmount;
            output.IsRareRandom = input.IsRareRandom;
            output.Rare = input.Rare;
            output.Upgrade = input.Upgrade;
            output.Settimana = input.Settimana;
            return true;
        }

        public static bool ToRuotaFortunaDTO(RuotaFortuna input, RuotaFortunaDTO output)
        {
            if (input == null)
            {
                output = null;
                return false;
            }
            output.RuotaDellaFortunaId = input.RuotaDellaFortunaId;
            output.Probability = input.Probability;
            output.ItemGeneratedVNum = input.ItemGeneratedVNum;
            output.ItemGeneratedAmount = input.ItemGeneratedAmount;
            output.ItemGeneratedAmount = input.ItemGeneratedAmount;
            output.IsRareRandom = input.IsRareRandom;
            output.Rare = input.Rare;
            output.Upgrade = input.Upgrade;
            output.Settimana = input.Settimana;
            return true;
        }

        #endregion
    }
}