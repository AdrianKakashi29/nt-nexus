﻿using System;
using System.Xml.Serialization;

namespace OpenNos.XMLModel.Events
{
    [Serializable]
    public class MonsterOut
    {
        #region Properties

        [XmlAttribute]
        public short VNum { get; set; }

        [XmlAttribute]
        public short Duration { get; set; }

        #endregion
    }
}