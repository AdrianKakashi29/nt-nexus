﻿using System;
using System.Xml.Serialization;

namespace OpenNos.XMLModel.Events
{
    [Serializable]
    public class MonsterCenter
    {
        #region Properties
        
        [XmlAttribute]
        public short VNum { get; set; }

        #endregion
    }
}