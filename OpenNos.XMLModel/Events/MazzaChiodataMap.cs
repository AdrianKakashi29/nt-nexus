﻿using System;
using System.Xml.Serialization;

namespace OpenNos.XMLModel.Events
{
    [Serializable]
    public class MazzaChiodataMap
    {
        #region Properties
        
        [XmlAttribute]
        public short Amount { get; set; }

        #endregion
    }
}