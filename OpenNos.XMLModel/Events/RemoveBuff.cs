﻿using System;
using System.Xml.Serialization;

namespace OpenNos.XMLModel.Events
{
    [Serializable]
    public class RemoveBuff
    {
        #region Properties

        [XmlAttribute]
        public short VNum { get; set; }

        [XmlAttribute]
        public short Monster { get; set; }

        #endregion
    }
}