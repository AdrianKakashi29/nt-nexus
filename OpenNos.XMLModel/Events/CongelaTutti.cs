﻿using System;
using System.Xml.Serialization;

namespace OpenNos.XMLModel.Events
{
    [Serializable]
    public class CongelaTutti
    {
        #region Properties
        
        [XmlAttribute]
        public short Delay { get; set; }

        #endregion
    }
}