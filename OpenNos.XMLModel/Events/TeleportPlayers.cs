﻿using System;
using System.Xml.Serialization;

namespace OpenNos.XMLModel.Events
{
    [Serializable]
    public class TeleportPlayers
    {
        #region Properties

        [XmlAttribute]
        public short Number { get; set; }

        [XmlAttribute]
        public short SourceX { get; set; }

        [XmlAttribute]
        public short SourceY { get; set; }

        [XmlAttribute]
        public short DestinationX { get; set; }

        [XmlAttribute]
        public short DestinationY { get; set; }

        #endregion
    }
}