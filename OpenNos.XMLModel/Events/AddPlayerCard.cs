﻿using System;
using System.Xml.Serialization;

namespace OpenNos.XMLModel.Events
{
    [Serializable]
    public class AddPlayerCard
    {
        #region Properties

        [XmlAttribute]
        public short Card { get; set; }
        
        #endregion
    }
}