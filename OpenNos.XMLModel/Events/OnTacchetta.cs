﻿using System;
using System.Xml.Serialization;

namespace OpenNos.XMLModel.Events
{
    [Serializable]
    public class OnTacchetta
    {
        #region Properties
        
        [XmlElement]
        public SummonMonster[] SummonMonster { get; set; }

        [XmlElement]
        public MonsterCenter MonsterCenter { get; set; }

        [XmlElement]
        public AddBuff AddBuff { get; set; }

        [XmlElement]
        public AddPlayerCard AddPlayerCard { get; set; }

        [XmlElement]
        public BombaIbra BombaIbra { get; set; }

        [XmlElement]
        public BombaKertos BombaKertos { get; set; }

        #endregion
    }
}