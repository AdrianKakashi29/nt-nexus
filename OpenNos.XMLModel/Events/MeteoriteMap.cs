﻿using System;
using System.Xml.Serialization;

namespace OpenNos.XMLModel.Events
{
    [Serializable]
    public class MeteoriteMap
    {
        #region Properties

        [XmlAttribute]
        public short VNum { get; set; }

        [XmlAttribute]
        public short Amount { get; set; }

        #endregion
    }
}