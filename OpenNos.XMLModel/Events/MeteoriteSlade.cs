﻿using System;
using System.Xml.Serialization;

namespace OpenNos.XMLModel.Events
{
    [Serializable]
    public class MeteoriteSlade
    {
        #region Properties

        [XmlAttribute]
        public short Amount { get; set; }

        #endregion
    }
}