﻿using OpenNos.XMLModel.Events;
using System;
using System.Xml.Serialization;

namespace OpenNos.XMLModel.Objects
{
    [Serializable]
    public class Wave
    {
        #region Properties

        [XmlAttribute]
        public byte Delay { get; set; }

        [XmlAttribute]
        public byte Offset { get; set; }

        [XmlElement]
        public SendMessage SendMessage { get; set; }

        [XmlElement]
        public object SaettaMap { get; set; }

        [XmlElement]
        public object PirataMap { get; set; }

        [XmlElement]
        public TeleportPlayers TeleportPlayers { get; set; }

        [XmlElement]
        public AddPlayerCard AddPlayerCard { get; set; }

        [XmlElement]
        public SummonMonster[] SummonMonster { get; set; }

        [XmlElement]
        public SpawnPortal[] SpawnPortal { get; set; }
        
        [XmlElement]
        public MonsterOut MonsterOut { get; set; }

        [XmlElement]
        public MeteoriteMap MeteoriteMap { get; set; }

        [XmlElement]
        public MazzaChiodataMap MazzaChiodataMap { get; set; }

        [XmlElement]
        public MeteoriteSlade MeteoriteSlade { get; set; }

        [XmlElement]
        public BombaIbra BombaIbra { get; set; }

        [XmlElement]
        public BombaKertos BombaKertos { get; set; }

        [XmlElement]
        public CongelaTutti CongelaTutti { get; set; }

        #endregion
    }
}