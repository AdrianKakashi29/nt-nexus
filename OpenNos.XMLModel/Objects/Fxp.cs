﻿using System;
using System.Xml.Serialization;

namespace OpenNos.XMLModel.Objects
{
    [Serializable]
    public class Fxp
    {
        #region Properties

        [XmlAttribute]
        public short Amount { get; set; }

        #endregion
    }
}