/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.ChatLog.Networking;
using OpenNos.ChatLog.Shared;
using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Packets.ClientPackets;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reactive.Linq;
using System.Reflection;
using System.Threading.Tasks;
using OpenNos.GameObject.Networking;
using System.Collections.Concurrent;
using System.Globalization;
using OpenNos.GameObject.Event.BattleRoyale;
using System;
using System.Timers;


namespace OpenNos.Handler
{
    public class BasicPacketHandler : IPacketHandler
    {
        #region Instantiation

        public BasicPacketHandler(ClientSession session) => Session = session;


        #endregion

        #region Properties

        private ClientSession Session { get; }


        #endregion

        #region Methods

        /// <summary>
        /// gbox packet
        /// </summary>
        /// <param name="gboxPacket"></param>
        public void GBox(GBoxPacket gboxPacket)
        {
            switch (gboxPacket.type)
            {
                case 1:
                    if (Session.Character.GoldBank <= 100000000)
                    {
                        if (gboxPacket.value > Session.Character.Gold)
                        {
                            //ServerManager.Instance.Kick(Session.Character.Name);
                            //PenaltyLogDTO log = new PenaltyLogDTO
                            //{
                            //    AccountId = Session.Character.AccountId,
                            //    Reason = "GM",
                            //    Penalty = PenaltyType.Banned,
                            //    DateStart = DateTime.Now,
                            //    DateEnd = DateTime.Now.AddYears(15),
                            //    AdminName = "Administrator"
                            //};
                            //Character.InsertOrUpdatePenalty(log);
                            return;
                        }
                        if (Session.Character.Gold - (ServerManager.Instance.Configuration.Tax) >= (gboxPacket.value * 1000))
                        {
                            if (gboxPacket.value < 0)
                            {
                                return;
                            }
                            Session.Character.GoldBank += gboxPacket.value;
                            Session.Character.Gold -= (gboxPacket.value * 1000) + (ServerManager.Instance.Configuration.Tax);
                            Session.SendPacket(Session.Character.GenerateGold());
                            Session.SendPacket(Session.Character.GenerateGB(1));
                            Session.SendPacket(Session.Character.GenerateSay("Account balance: " + Session.Character.GoldBank + ".000 Gold; Gold in your possession: " + Session.Character.Gold, 12));
                            Session.SendPacket(Session.Character.GenerateSMemo(4, "Account balance: " + Session.Character.GoldBank + ".000 Gold; Gold in your possession: " + Session.Character.Gold));

                        }
                        else
                        {
                            Session.SendPacket(Session.Character.GenerateSay("You do not have enough money.", 11));
                        }
                    }
                    else
                    {
                        Session.SendPacket(Session.Character.GenerateSay("Maximum gold reached in the bank.", 11));
                    }
                    break;
                case 2:
                    if (gboxPacket.value > Session.Character.GoldBank)
                    {
                        return;
                    }
                    if (gboxPacket.value < 0)
                    {
                        return;
                    }

                    if (Session.Character.Gold + (gboxPacket.value * 1000) <= 2000000000)
                    {
                        Session.Character.GoldBank -= gboxPacket.value;
                        Session.Character.Gold += gboxPacket.value * 1000;
                        Session.SendPacket(Session.Character.GenerateGold());
                        Session.SendPacket(Session.Character.GenerateGB(1));
                        Session.SendPacket(Session.Character.GenerateSay("Account balance: " + Session.Character.GoldBank + ".000 Gold; Gold in your possession: " + Session.Character.Gold, 12));
                            Session.SendPacket(Session.Character.GenerateSMemo(4, "Account balance: " + Session.Character.GoldBank + ".000 Gold; Gold in your possession: " + Session.Character.Gold));
                    }
                    else
                    {
                        int gold = (int)((2000000000 - Session.Character.Gold) / 1000);
                        Session.Character.GoldBank -= gold;
                        Session.Character.Gold = 2000000000;
                        Session.SendPacket(Session.Character.GenerateGold());
                        Session.SendPacket(Session.Character.GenerateGB(1));
                        Session.SendPacket(Session.Character.GenerateSay("Account balance: " + Session.Character.GoldBank + ".000 Gold; Gold in your possession: " + Session.Character.Gold, 12));
                            Session.SendPacket(Session.Character.GenerateSMemo(4, "Account balance: " + Session.Character.GoldBank + ".000 Gold; Gold in your possession: " + Session.Character.Gold));

                    }
                    break;
            }

        }
       
        /// <summary>
        /// blins packet
        /// </summary>
        /// <param name="blInsPacket"></param>
        public void BlacklistAdd(BlInsPacket blInsPacket)
        {
            Session.Character.AddRelation(blInsPacket.CharacterId, CharacterRelationType.Blocked);
            Session.SendPacket(
                UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("BLACKLIST_ADDED")));
            Session.SendPacket(Session.Character.GenerateBlinit());
        }

        /// <summary>
        /// bldel packet
        /// </summary>
        /// <param name="blDelPacket"></param>
        public void BlacklistDelete(BlDelPacket blDelPacket)
        {
            Session.Character.DeleteBlackList(blDelPacket.CharacterId);
            Session.SendPacket(Session.Character.GenerateBlinit());
            Session.SendPacket(
                UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("BLACKLIST_DELETED")));
        }

        /// <summary>
        /// gop packet
        /// </summary>
        /// <param name="characterOptionPacket"></param>
        public void CharacterOptionChange(CharacterOptionPacket characterOptionPacket)
        {
            switch (characterOptionPacket.Option)
            {
                case CharacterOption.BuffBlocked:
                    Session.Character.BuffBlocked = characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.BuffBlocked
                            ? "BUFF_BLOCKED"
                            : "BUFF_UNLOCKED"), 0));
                    break;

                case CharacterOption.EmoticonsBlocked:
                    Session.Character.EmoticonsBlocked = characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.EmoticonsBlocked
                            ? "EMO_BLOCKED"
                            : "EMO_UNLOCKED"), 0));
                    break;

                case CharacterOption.ExchangeBlocked:
                    Session.Character.ExchangeBlocked = !characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.ExchangeBlocked
                            ? "EXCHANGE_BLOCKED"
                            : "EXCHANGE_UNLOCKED"), 0));
                    break;

                case CharacterOption.FriendRequestBlocked:
                    Session.Character.FriendRequestBlocked = !characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.FriendRequestBlocked
                            ? "FRIEND_REQ_BLOCKED"
                            : "FRIEND_REQ_UNLOCKED"), 0));
                    break;

                case CharacterOption.PetAutoRelive:
                    Session.Character.IsPetAutoRelive = characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.IsPetAutoRelive
                            ? "PET_AUTO_RELIVE_ENABLED"
                            : "PET_AUTO_RELIVE_DISABLED"), 0));
                    break;

                case CharacterOption.PartnerAutoRelive:
                    Session.Character.IsPartnerAutoRelive = characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.IsPartnerAutoRelive
                            ? "PARTNER_AUTO_RELIVE_ENABLED"
                            : "PARTNER_AUTO_RELIVE_DISABLED"), 0));
                    break;

                case CharacterOption.GroupRequestBlocked:
                    Session.Character.GroupRequestBlocked = !characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.GroupRequestBlocked
                            ? "GROUP_REQ_BLOCKED"
                            : "GROUP_REQ_UNLOCKED"), 0));
                    break;

                case CharacterOption.HeroChatBlocked:
                    Session.Character.HeroChatBlocked = characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.HeroChatBlocked
                            ? "HERO_CHAT_BLOCKED"
                            : "HERO_CHAT_UNLOCKED"), 0));
                    break;

                case CharacterOption.HpBlocked:
                    Session.Character.HpBlocked = characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.HpBlocked ? "HP_BLOCKED" : "HP_UNLOCKED"),
                        0));
                    break;

                case CharacterOption.MinilandInviteBlocked:
                    Session.Character.MinilandInviteBlocked = characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.MinilandInviteBlocked
                            ? "MINI_INV_BLOCKED"
                            : "MINI_INV_UNLOCKED"), 0));
                    break;

                case CharacterOption.MouseAimLock:
                    Session.Character.MouseAimLock = characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.MouseAimLock
                            ? "MOUSE_LOCKED"
                            : "MOUSE_UNLOCKED"), 0));
                    break;

                case CharacterOption.QuickGetUp:
                    Session.Character.QuickGetUp = characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.QuickGetUp
                            ? "QUICK_GET_UP_ENABLED"
                            : "QUICK_GET_UP_DISABLED"), 0));
                    break;

                case CharacterOption.WhisperBlocked:
                    Session.Character.WhisperBlocked = !characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.WhisperBlocked
                            ? "WHISPER_BLOCKED"
                            : "WHISPER_UNLOCKED"), 0));
                    break;

                case CharacterOption.FamilyRequestBlocked:
                    Session.Character.FamilyRequestBlocked = !characterOptionPacket.IsActive;
                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        Language.Instance.GetMessageFromKey(Session.Character.FamilyRequestBlocked
                            ? "FAMILY_REQ_LOCKED"
                            : "FAMILY_REQ_UNLOCKED"), 0));
                    break;

                case CharacterOption.GroupSharing:
                    Group grp = ServerManager.Instance.Groups.Find(
                        g => g.IsMemberOfGroup(Session.Character.CharacterId));
                    if (grp == null)
                    {
                        return;
                    }

                    if (!grp.IsLeader(Session))
                    {
                        Session.SendPacket(
                            UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NOT_MASTER"), 0));
                        return;
                    }

                    if (!characterOptionPacket.IsActive)
                    {
                        Group group =
                            ServerManager.Instance.Groups.Find(s => s.IsMemberOfGroup(Session.Character.CharacterId));
                        if (group != null)
                        {
                            group.SharingMode = 1;
                        }

                        Session.CurrentMapInstance?.Broadcast(Session,
                            UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SHARING"), 0),
                            ReceiverType.Group);
                    }
                    else
                    {
                        Group group =
                            ServerManager.Instance.Groups.Find(s => s.IsMemberOfGroup(Session.Character.CharacterId));
                        if (group != null)
                        {
                            group.SharingMode = 0;
                        }

                        Session.CurrentMapInstance?.Broadcast(Session,
                            UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SHARING_BY_ORDER"), 0),
                            ReceiverType.Group);
                    }

                    break;
            }

            Session.SendPacket(Session.Character.GenerateStat());
        }

        /// <summary>
        /// compl packet
        /// </summary>
        /// <param name="complimentPacket"></param>
        public void Compliment(ComplimentPacket complimentPacket)
        {
            if (complimentPacket != null)
            {
                ClientSession sess = ServerManager.Instance.GetSessionByCharacterId(complimentPacket.CharacterId);
                if (sess != null)
                {
                    if (Session.Character.SwitchLevel() >= 30)
                    {
                        GeneralLogDTO dto =
                            Session.Character.GeneralLogs.LastOrDefault(s =>
                                s.LogData == "World" && s.LogType == "Connection");
                        GeneralLogDTO lastcompliment =
                            Session.Character.GeneralLogs.LastOrDefault(s =>
                                s.LogData == "World" && s.LogType == nameof(Compliment));
                        if (dto?.Timestamp.AddMinutes(60) <= DateTime.Now)
                        {
                            if (lastcompliment == null || lastcompliment.Timestamp.AddDays(1) <= DateTime.Now.Date)
                            {
                                sess.Character.Compliment++;
                                Session.SendPacket(Session.Character.GenerateSay(
                                    string.Format(Language.Instance.GetMessageFromKey("COMPLIMENT_GIVEN"),
                                        sess.Character.Name), 12));
                                Session.Character.GeneralLogs.Add(new GeneralLogDTO
                                {
                                    AccountId = Session.Account.AccountId,
                                    CharacterId = Session.Character.CharacterId,
                                    IpAddress = Session.IpAddress,
                                    LogData = "World",
                                    LogType = nameof(Compliment),
                                    Timestamp = DateTime.Now
                                });

                                Session.CurrentMapInstance?.Broadcast(Session,
                                    Session.Character.GenerateSay(
                                        string.Format(Language.Instance.GetMessageFromKey("COMPLIMENT_RECEIVED"),
                                            Session.Character.Name), 12), ReceiverType.OnlySomeone,
                                    characterId: complimentPacket.CharacterId);
                            }
                            else
                            {
                                Session.SendPacket(
                                    Session.Character.GenerateSay(
                                        Language.Instance.GetMessageFromKey("COMPLIMENT_COOLDOWN"), 11));
                            }
                        }
                        else if (dto != null)
                        {
                            Session.SendPacket(Session.Character.GenerateSay(
                                string.Format(Language.Instance.GetMessageFromKey("COMPLIMENT_LOGIN_COOLDOWN"),
                                    (dto.Timestamp.AddMinutes(60) - DateTime.Now).Minutes), 11));
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("COMPLIMENT_NOT_MINLVL"),
                                11));
                    }
                }
            }
        }

        /// <summary>
        /// dir packet
        /// </summary>
        /// <param name="directionPacket"></param>
        public void Dir(DirectionPacket directionPacket)
        {
            ClientSession dirclient = ServerManager.Instance.GetSessionByCharacterId(directionPacket.CharacterId);
            if (dirclient != null)
            {
                if (dirclient.CurrentMapInstance != null)
                {
                    if (dirclient.CurrentMapInstance == Session.CurrentMapInstance)
                    {
                        dirclient.Character.Direction = directionPacket.Direction;
                        Session.CurrentMapInstance?.Broadcast(dirclient.Character.GenerateDir());

                    }
                }
            }


            MapMonster dirmonster = Session.CurrentMapInstance.Monsters?.Where(m => m.MapMonsterId == (int)directionPacket.CharacterId)?.FirstOrDefault();
            if (dirmonster != null)
            {
                if (dirmonster.MapInstance != null)
                {
                    if (dirmonster.MapInstance == Session.CurrentMapInstance)
                    {
                        dirmonster.Position = directionPacket.Direction;
                    }
                }
            }
        }




        /// <summary>
        /// fins packet
        /// </summary>
        /// <param name="fInsPacket"></param>
        public void FriendAdd(FInsPacket fInsPacket)
        {
            if (!Session.Character.IsFriendlistFull())
            {
                long characterId = fInsPacket.CharacterId;
                if (!Session.Character.IsFriendOfCharacter(characterId))
                {
                    if (!Session.Character.IsBlockedByCharacter(characterId))
                    {
                        if (!Session.Character.IsBlockingCharacter(characterId))
                        {
                            ClientSession otherSession = ServerManager.Instance.GetSessionByCharacterId(characterId);
                            if (otherSession != null)
                            {
                                if (otherSession.Character.FriendRequestBlocked)
                                {
                                    Session.SendPacket(
                                        $"info {Language.Instance.GetMessageFromKey("FRIEND_REJECTED")}");
                                    return;
                                }
                                if (fInsPacket.Type == 34)
                                {
                                    if (otherSession.Character.FriendRequestCharacters.GetAllItems()
                                        .Contains(Session.Character.CharacterId))
                                    {
                                        Session.Character.DeleteRelation(otherSession.Character.CharacterId);
                                    }
                                }
                                if (otherSession.Character.FriendRequestCharacters.GetAllItems()
                                    .Contains(Session.Character.CharacterId))
                                {
                                    switch (fInsPacket.Type)
                                    {
                                        case 1:
                                            Session.Character.AddRelation(characterId, CharacterRelationType.Friend);
                                            Session.SendPacket(
                                                $"info {Language.Instance.GetMessageFromKey("FRIEND_ADDED")}");
                                            otherSession.SendPacket(
                                                $"info {Language.Instance.GetMessageFromKey("FRIEND_ADDED")}");
                                            break;

                                        case 34:
                                            Session.Character.AddRelation(characterId, CharacterRelationType.Spouse);
                                            CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                                            {
                                                DestinationCharacterId = null,
                                                SourceCharacterId = Session.Character.CharacterId,
                                                SourceWorldId = ServerManager.Instance.WorldId,
                                                Message =
                                                    $"{Session.Character.Name} and {otherSession.Character.Name} just got married. Congratualtions!",
                                                Type = MessageType.Shout
                                            });
                                            try
                                            {
                                                otherSession.Character.Inventory.RemoveItemAmount(1981, 1);
                                            }
                                            catch (Exception ex)
                                            {
                                            }
                                            Session.SendPacket(
                                                $"info {Language.Instance.GetMessageFromKey("SPOUSE_ADDED")}");
                                            otherSession.SendPacket(
                                                $"info {Language.Instance.GetMessageFromKey("SPOUSE_ADDED")}");
                                            break;

                                        case 2:
                                            otherSession.SendPacket(
                                                $"info {Language.Instance.GetMessageFromKey("FRIEND_REJECTED")}");
                                            break;

                                        default:
                                            if (Session.Character.IsFriendlistFull())
                                            {
                                                Session.SendPacket(
                                                    $"info {Language.Instance.GetMessageFromKey("FRIEND_FULL")}");
                                                otherSession.SendPacket(
                                                    $"info {Language.Instance.GetMessageFromKey("FRIEND_FULL")}");
                                            }

                                            break;
                                    }
                                }
                                else
                                {
                                    otherSession.SendPacket(UserInterfaceHelper.GenerateDialog(
                                        $"#fins^1^{Session.Character.CharacterId} #fins^2^{Session.Character.CharacterId} {string.Format(Language.Instance.GetMessageFromKey("FRIEND_ADD"), Session.Character.Name)}"));
                                    Session.Character.FriendRequestCharacters.Add(characterId);
                                }
                            }
                        }
                        else
                        {
                            Session.SendPacket($"info {Language.Instance.GetMessageFromKey("BLACKLIST_BLOCKING")}");
                        }
                    }
                    else
                    {
                        Session.SendPacket($"info {Language.Instance.GetMessageFromKey("BLACKLIST_BLOCKED")}");
                    }
                }
                else
                {
                    Session.SendPacket($"info {Language.Instance.GetMessageFromKey("ALREADY_FRIEND")}");
                }
            }
            else
            {
                Session.SendPacket($"info {Language.Instance.GetMessageFromKey("FRIEND_FULL")}");
            }
        }


        /// <summary>
        /// fdel packet
        /// </summary>
        /// <param name="fDelPacket"></param>
        public void FriendDelete(FDelPacket fDelPacket)
        {
            Session.Character.DeleteRelation(fDelPacket.CharacterId);
            Session.SendPacket(UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("FRIEND_DELETED")));
        }

        /// <summary>
        /// btk packet
        /// </summary>
        /// <param name="btkPacket"></param>
        public void FriendTalk(BtkPacket btkPacket)
        {
            if (string.IsNullOrEmpty(btkPacket.Message))
            {
                return;
            }

            string message = btkPacket.Message;
            if (message.Length > 60)
            {
                message = message.Substring(0, 60);
            }

            message = message.Trim();

            CharacterDTO character = DAOFactory.CharacterDAO.LoadById(btkPacket.CharacterId);
            if (character != null)
            {
                int? sentChannelId = CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                {
                    DestinationCharacterId = character.CharacterId,
                    SourceCharacterId = Session.Character.CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message = PacketFactory.Serialize(Session.Character.GenerateTalk(message)),
                    Type = MessageType.PrivateChat
                });

                if (ServerManager.Instance.Configuration.UseChatLogService)
                {
                    ChatLogServiceClient.Instance.LogChatMessage(new ChatLogEntry()
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        Receiver = character.Name,
                        ReceiverId = character.CharacterId,
                        MessageType = ChatLogType.BuddyTalk,
                        Message = btkPacket.Message
                    });
                }

                if (!sentChannelId.HasValue) //character is even offline on different world
                {
                    Session.SendPacket(
                        UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("FRIEND_OFFLINE")));
                }
            }
        }

        /// <summary>
        /// pcl packet
        /// </summary>
        /// <param name="getGiftPacket"></param>
        public void GetGift(GetGiftPacket getGiftPacket)
        {
            int giftId = getGiftPacket.GiftId;
            if (Session.Character.MailList.ContainsKey(giftId))
            {
                MailDTO mail = Session.Character.MailList[giftId];
                if (getGiftPacket.Type == 4 && mail.AttachmentVNum != null)
                {
                    if (mail.AttachmentVNum > 29999)
                    {
                        string boost = "";
                        if (mail.AttachmentVNum == 30000)
                        {
                            Session.Character.StaticBonusList.Add(new StaticBonusDTO
                            {
                                CharacterId = Session.Character.CharacterId,
                                DateEnd = DateTime.Now.AddDays(1),
                                StaticBonusType = StaticBonusType.DoubleExp
                            });
                            boost = "Double Exp 24h";
                        }
                        else if (mail.AttachmentVNum == 30001)
                        {
                            Session.Character.StaticBonusList.Add(new StaticBonusDTO
                            {
                                CharacterId = Session.Character.CharacterId,
                                DateEnd = DateTime.Now.AddDays(1),
                                StaticBonusType = StaticBonusType.DoubleGold
                            });
                            boost = "Double Gold 24h";
                        }
                        else if (mail.AttachmentVNum == 30002)
                        {
                            Session.Character.StaticBonusList.Add(new StaticBonusDTO
                            {
                                CharacterId = Session.Character.CharacterId,
                                DateEnd = DateTime.Now.AddDays(1),
                                StaticBonusType = StaticBonusType.DoubleRep
                            });
                            boost = "Double Reputation 24h";
                        }

                        Logger.LogUserEvent("PARCEL_GET", Session.GenerateIdentity(),
                            $"Boost: {boost}");

                        Session.SendPacket(Session.Character.GenerateSay("You received the boost: " + boost, 12));

                        DAOFactory.MailDAO.DeleteById(mail.MailId);

                        Session.SendPacket($"parcel 2 1 {giftId}");

                        Session.Character.MailList.Remove(giftId);

                    }
                    else
                    {
                        if (Session.Character.Inventory.CanAddItem((short)mail.AttachmentVNum))
                        {
                            ItemInstance newInv = Session.Character.Inventory.AddNewToInventory((short)mail.AttachmentVNum,
                                    mail.AttachmentAmount, Upgrade: mail.AttachmentUpgrade,
                                    Rare: (sbyte)mail.AttachmentRarity)
                                .FirstOrDefault();
                            if (newInv != null)
                            {
                                if (newInv.Rare != 0)
                                {
                                    newInv.SetRarityPoint(Session);
                                }
                                switch (newInv.Item.EquipmentSlot)
                                {
                                    case EquipmentType.Boots:
                                    case EquipmentType.Gloves:
                                        newInv.FireResistance = (short)(newInv.Item.FireResistance * (newInv.Upgrade - 1));
                                        newInv.DarkResistance = (short)(newInv.Item.DarkResistance * (newInv.Upgrade - 1));
                                        newInv.LightResistance = (short)(newInv.Item.LightResistance * (newInv.Upgrade - 1));
                                        newInv.WaterResistance = (short)(newInv.Item.WaterResistance * (newInv.Upgrade - 1));
                                        break;
                                }

                                Logger.LogUserEvent("PARCEL_GET", Session.GenerateIdentity(),
                                    $"IIId: {newInv.Id} ItemVNum: {newInv.ItemVNum} Amount: {mail.AttachmentAmount} Sender: {mail.SenderId}");

                                Session.SendPacket(Session.Character.GenerateSay(
                                    string.Format(Language.Instance.GetMessageFromKey("ITEM_GIFTED"), newInv.Item.Name,
                                        mail.AttachmentAmount), 12));

                                DAOFactory.MailDAO.DeleteById(mail.MailId);

                                Session.SendPacket($"parcel 2 1 {giftId}");

                                Session.Character.MailList.Remove(giftId);
                            }
                        }
                        else
                        {
                            Session.SendPacket("parcel 5 1 0");
                            Session.SendPacket(
                                UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NOT_ENOUGH_PLACE"),
                                    0));
                        }
                    }
                }
                else if (getGiftPacket.Type == 5)
                {
                    Session.SendPacket($"parcel 7 1 {giftId}");

                    if (DAOFactory.MailDAO.LoadById(mail.MailId) != null)
                    {
                        DAOFactory.MailDAO.DeleteById(mail.MailId);
                    }

                    if (Session.Character.MailList.ContainsKey(giftId))
                    {
                        Session.Character.MailList.Remove(giftId);
                    }
                }
            }
        }

        /// <summary>
        /// ncif packet
        /// </summary>
        /// <param name="ncifPacket"></param>
        public void GetNamedCharacterInformation(NcifPacket ncifPacket)
        {
            switch (ncifPacket.Type)
            {
                // characters
                case 1:
                    Session.SendPacket(ServerManager.Instance.GetSessionByCharacterId(ncifPacket.TargetId)?.Character
                        ?.GenerateStatInfo());
                    break;

                // npcs/mates
                case 2:
                    if (Session.HasCurrentMapInstance)
                    {
                        Session.CurrentMapInstance.Npcs.Where(n => n.MapNpcId == (int)ncifPacket.TargetId).ToList()
                            .ForEach(npc =>
                            {
                                NpcMonster npcinfo = ServerManager.GetNpc(npc.NpcVNum);
                                if (npcinfo == null)
                                {
                                    return;
                                }

                                Session.Character.LastNpcMonsterId = npc.MapNpcId;
                                Session.SendPacket(
                                    $"st 2 {ncifPacket.TargetId} {npcinfo.Level} {npcinfo.HeroLevel} 100 100 50000 50000");
                            });
                        Parallel.ForEach(Session.CurrentMapInstance.Sessions, session =>
                        {
                            Mate mate = session.Character.Mates.Find(
                                s => s.MateTransportId == (int)ncifPacket.TargetId);
                            if (mate != null)
                            {
                                Session.SendPacket(mate.GenerateStatInfo());
                            }
                        });
                    }

                    break;

                // monsters
                case 3:
                    if (Session.HasCurrentMapInstance)
                    {
                        Session.CurrentMapInstance.Monsters.Where(m => m.MapMonsterId == (int)ncifPacket.TargetId)
                            .ToList().ForEach(monster =>
                            {
                                NpcMonster monsterinfo = ServerManager.GetNpc(monster.MonsterVNum);
                                if (monsterinfo == null)
                                {
                                    return;
                                }

                                Session.Character.LastNpcMonsterId = monster.MapMonsterId;
                                Session.SendPacket(
                                    $"st 3 {ncifPacket.TargetId} {monsterinfo.Level} {monsterinfo.HeroLevel} {(int)((float)monster.CurrentHp / (float)monster.MaxHp * 100)} {(int)((float)monster.CurrentMp / (float)monster.MaxMp * 100)} {monster.CurrentHp} {monster.CurrentMp}{monster.Buff.GetAllItems().Aggregate(string.Empty, (current, buff) => current + $" {buff.Card.CardId}")}");
                            });
                    }

                    break;
            }
        }

        /// <summary>
        /// RstartPacket packet
        /// </summary>
        /// <param name="rStartPacket"></param>
        public void GetRStart(RStartPacket rStartPacket)
        {
            if (rStartPacket.Type == 1 && Session.Character.Timespace?.InstanceBag != null)
            {
                Session.Character.Timespace.InstanceBag.Lock = true;
                foreach (ClientSession client in Session.Character.Timespace.FirstMap.Sessions)
                {
                    client.Character.Timespace = Session.Character.Timespace;
                }
                Preq(new PreqPacket());
            }
        }

        /// <summary>
        /// npinfo packet
        /// </summary>
        /// <param name="npinfoPacket"></param>
        public void GetStats(NpinfoPacket npinfoPacket)
        {
            Session.SendPacket(Session.Character.GenerateStatChar());
            if (npinfoPacket.Page != Session.Character.ScPage)
            {
                Session.Character.ScPage = npinfoPacket.Page;
                Session.SendPacket(UserInterfaceHelper.GeneratePClear());
                Session.SendPackets(Session.Character.GenerateScP(npinfoPacket.Page));
            }
        }

        /// <summary>
        /// pjoin packet
        /// </summary>
        /// <param name="pjoinPacket"></param>
        public void GroupJoin(PJoinPacket pjoinPacket)
        {
            if (pjoinPacket != null)
            {
                bool createNewGroup = true;
                ClientSession targetSession = ServerManager.Instance.GetSessionByCharacterId(pjoinPacket.CharacterId);

                if (targetSession == null && !pjoinPacket.RequestType.Equals(GroupRequestType.Sharing))
                {
                    return;
                }

                if (pjoinPacket.RequestType.Equals(GroupRequestType.Requested)
                    || pjoinPacket.RequestType.Equals(GroupRequestType.Invited))
                {
                    if (pjoinPacket.CharacterId == 0)
                    {
                        return;
                    }

                    if (ServerManager.Instance.IsCharactersGroupFull(pjoinPacket.CharacterId))
                    {
                        Session.SendPacket(
                            UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("GROUP_FULL")));
                        return;
                    }

                    if (ServerManager.Instance.IsCharacterMemberOfGroup(pjoinPacket.CharacterId)
                        && ServerManager.Instance.IsCharacterMemberOfGroup(Session.Character.CharacterId))
                    {
                        Session.SendPacket(
                            UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("ALREADY_IN_GROUP")));
                        return;
                    }

                    if (Session.Character.CharacterId != pjoinPacket.CharacterId && targetSession != null)
                    {
                        if (Session.Character.IsBlockedByCharacter(pjoinPacket.CharacterId))
                        {
                            Session.SendPacket(
                                UserInterfaceHelper.GenerateInfo(
                                    Language.Instance.GetMessageFromKey("BLACKLIST_BLOCKED")));
                            return;
                        }

                        if (targetSession.Character.GroupRequestBlocked)
                        {
                            Session.SendPacket(
                                UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("GROUP_BLOCKED"),
                                    0));
                        }
                        else
                        {
                            // save sent group request to current character
                            Session.Character.GroupSentRequestCharacterIds.Add(targetSession.Character.CharacterId);
                            if (Session.Character.Group == null || Session.Character.Group.GroupType == GroupType.Group)
                            {
                                if (targetSession.Character?.Group == null
                                    || targetSession.Character?.Group.GroupType == GroupType.Group)
                                {
                                    Session.SendPacket(UserInterfaceHelper.GenerateInfo(
                                        string.Format(Language.Instance.GetMessageFromKey("GROUP_REQUEST"),
                                            targetSession.Character.Name)));
                                    targetSession.SendPacket(UserInterfaceHelper.GenerateDialog(
                                        $"#pjoin^3^{Session.Character.CharacterId} #pjoin^4^{Session.Character.CharacterId} {string.Format(Language.Instance.GetMessageFromKey("INVITED_YOU"), Session.Character.Name)}"));
                                }
                                else
                                {
                                    //can't invite raid member
                                }
                            }
                            else
                            {
                                if (Session.Character.Group.Raid == null)
                                {
                                    targetSession.SendPacket(
                                        $"qna #fb^1^{Session.Character.CharacterId}^1 {string.Format(Language.Instance.GetMessageFromKey("INVITE_BA"), Session.Character.Name)}");
                                }
                                else
                                {
                                    targetSession.SendPacket(
                                        $"qna #rd^1^{Session.Character.CharacterId}^{Session.Character.Group.Raid.Id} {string.Format(Language.Instance.GetMessageFromKey("INVITE_RAID"), Session.Character.Name)}");
                                }
                            }
                        }
                    }
                }
                else if (pjoinPacket.RequestType.Equals(GroupRequestType.Sharing))
                {
                    if (Session.Character.Group != null)
                    {
                        Session.SendPacket(
                            UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("GROUP_SHARE_INFO")));
                        Session.Character.Group.Characters
                            .Where(s => s.Character.CharacterId != Session.Character.CharacterId).ToList().ForEach(s =>
                            {
                                s.SendPacket(UserInterfaceHelper.GenerateDialog(
                                    $"#pjoin^6^{Session.Character.CharacterId} #pjoin^7^{Session.Character.CharacterId} {string.Format(Language.Instance.GetMessageFromKey("INVITED_YOU_SHARE"), Session.Character.Name)}"));
                                Session.Character.GroupSentRequestCharacterIds.Add(s.Character.CharacterId);
                            });
                    }
                }
                else if (pjoinPacket.RequestType.Equals(GroupRequestType.Accepted))
                {
                    if (targetSession?.Character.GroupSentRequestCharacterIds.GetAllItems()
                            .Contains(Session.Character.CharacterId) == false)
                    {
                        return;
                    }

                    try
                    {
                        targetSession?.Character.GroupSentRequestCharacterIds.Remove(Session.Character.CharacterId);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex);
                    }

                    if (ServerManager.Instance.IsCharacterMemberOfGroup(Session.Character.CharacterId)
                        && ServerManager.Instance.IsCharacterMemberOfGroup(pjoinPacket.CharacterId))
                    {
                        // everyone is in group, return
                        return;
                    }

                    if (ServerManager.Instance.IsCharactersGroupFull(pjoinPacket.CharacterId)
                        || ServerManager.Instance.IsCharactersGroupFull(Session.Character.CharacterId))
                    {
                        Session.SendPacket(
                            UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("GROUP_FULL")));
                        targetSession?.SendPacket(
                            UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("GROUP_FULL")));
                        return;
                    }

                    // get group and add to group
                    if (ServerManager.Instance.IsCharacterMemberOfGroup(Session.Character.CharacterId))
                    {
                        // target joins source
                        Group currentGroup =
                            ServerManager.Instance.GetGroupByCharacterId(Session.Character.CharacterId);

                        if (currentGroup != null)
                        {
                            currentGroup.JoinGroup(targetSession);
                            targetSession?.SendPacket(
                                UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("JOINED_GROUP"),
                                    10));
                            currentGroup.Characters.ForEach(s =>
                            {
                                if (targetSession.Character.IsSpouseOfCharacter(s.Character.CharacterId))
                                {
                                    s.Character.AddStaticBuff(new StaticBuffDTO
                                    {
                                        CardId = 319,
                                        CharacterId = s.Character.CharacterId,
                                        RemainingTime = 0
                                    });
                                    targetSession.Character.AddStaticBuff(new StaticBuffDTO
                                    {
                                        CardId = 319,
                                        CharacterId = s.Character.CharacterId,
                                        RemainingTime = 0
                                    });
                                }
                            });
                            createNewGroup = false;
                        }
                    }
                    else if (ServerManager.Instance.IsCharacterMemberOfGroup(pjoinPacket.CharacterId))
                    {
                        // source joins target
                        Group currentGroup = ServerManager.Instance.GetGroupByCharacterId(pjoinPacket.CharacterId);

                        if (currentGroup != null)
                        {
                            createNewGroup = false;
                            if (currentGroup.GroupType == GroupType.Group)
                            {
                                currentGroup.JoinGroup(Session);
                            }
                            else
                            {
                                if (currentGroup.Raid != null)
                                {
                                    Session.SendPacket(
                                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("RAID_JOIN"),
                                            10));
                                    if (Session.Character.SwitchLevel() < currentGroup.Raid?.LevelMinimum)
                                    {
                                        Session.SendPacket(Session.Character.GenerateSay(
                                            Language.Instance.GetMessageFromKey("RAID_LEVEL_LOW"), 10));
                                        return;
                                    }
                                    if (Session.Character.SwitchLevel() > currentGroup.Raid?.LevelMaximum)
                                    {
                                        Session.SendPacket(Session.Character.GenerateSay(
                                            Language.Instance.GetMessageFromKey("RAID_LEVEL_INCORRECT"), 10));
                                        /*if (AlreadySuccededToday)
                                        {
                                            //modal 1 ALREADY_SUCCEDED_AS_ASSISTANT
                                        }*/
                                    }

                                        currentGroup.JoinGroup(Session);
                                    Session.SendPacket(Session.Character.GenerateRaid(1));
                                    currentGroup.Characters.ForEach(s =>
                                    {
                                        s.SendPacket(currentGroup.GenerateRdlst());
                                        s.SendPacket(s.Character.GenerateSay(
                                            string.Format(Language.Instance.GetMessageFromKey("JOIN_TEAM"),
                                                Session.Character.Name), 10));
                                        s.SendPacket(s.Character.GenerateRaid(0));
                                    });
                                }
                                else
                                {
                                    if (ServerManager.Instance.ArcobalenoMembersRegistered.Count() > 0)
                                    {
                                        if (ServerManager.Instance.ArcobalenoMembersRegistered?.Where(s => s.Session == Session).Count() > 0)
                                        {
                                            return;
                                        }
                                    }
                                    Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("BA_JOIN"), 10));

                                    currentGroup.JoinGroup(Session);
                                    Session.SendPacket(Session.Character.GenerateFbt(1));
                                    currentGroup.Characters.ForEach(s =>
                                    {
                                        s.SendPacket(currentGroup.GenerateFblst());
                                        s.SendPacket(s.Character.GenerateSay(
                                            string.Format(Language.Instance.GetMessageFromKey("JOIN_TEAM_BA"),
                                                Session.Character.Name), 10));
                                        s.SendPacket(s.Character.GenerateFbt(0));
                                    });

                                    ServerManager.Instance.ArcobalenoMembers.Add(new ArcobalenoMember
                                    {
                                        ArcobalenoType = EventType.ARCOBALENO,
                                        Session = Session,
                                        GroupId = currentGroup.GroupId,
                                    });
                                }
                            }
                        }
                    }

                    if (createNewGroup)
                    {
                        Group group = new Group
                        {
                            GroupType = GroupType.Group
                        };
                        group.JoinGroup(pjoinPacket.CharacterId);
                        Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                            string.Format(Language.Instance.GetMessageFromKey("GROUP_JOIN"),
                                targetSession?.Character.Name), 10));
                        group.JoinGroup(Session.Character.CharacterId);
                        ServerManager.Instance.AddGroup(group);
                        targetSession?.SendPacket(
                            UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("GROUP_ADMIN")));
                        if (Session.Character.CharacterRelations.Where(y => y.RelatedCharacterId == targetSession.Character.CharacterId && y.RelationType == CharacterRelationType.Spouse).Count() > 0)
                        {

                            Session.Character.AddStaticBuff(new StaticBuffDTO
                            {
                                CardId = 319,
                                CharacterId = Session.Character.CharacterId,
                                RemainingTime = 0
                            });

                            targetSession.Character.AddStaticBuff(new StaticBuffDTO
                            {
                                CardId = 319,
                                CharacterId = targetSession.Character.CharacterId,
                                RemainingTime = 0
                            });
                        }
                        if (targetSession.Character.CharacterRelations.Where(y => y.RelatedCharacterId == Session.Character.CharacterId && y.RelationType == CharacterRelationType.Spouse).Count() > 0)
                        {

                            Session.Character.AddStaticBuff(new StaticBuffDTO
                            {
                                CardId = 319,
                                CharacterId = Session.Character.CharacterId,
                                RemainingTime = 0
                            });

                            targetSession.Character.AddStaticBuff(new StaticBuffDTO
                            {
                                CardId = 319,
                                CharacterId = targetSession.Character.CharacterId,
                                RemainingTime = 0
                            });
                        }
                        // set back reference to group
                        Session.Character.Group = group;
                        if (targetSession != null)
                        {
                            targetSession.Character.Group = @group;
                        }
                    }

                    if (Session.Character.Group.GroupType == GroupType.Group)
                    {
                        // player join group
                        ServerManager.Instance.UpdateGroup(pjoinPacket.CharacterId);
                        Session.CurrentMapInstance?.Broadcast(Session.Character.GeneratePidx());
                    }
                }
                else if (pjoinPacket.RequestType == GroupRequestType.Declined)
                {
                    if (targetSession?.Character.GroupSentRequestCharacterIds.GetAllItems().Contains(Session.Character.CharacterId) == false)
                    {
                        return;
                    }

                    targetSession?.Character.GroupSentRequestCharacterIds.Remove(Session.Character.CharacterId);

                    targetSession?.SendPacket(Session.Character.GenerateSay(
                        string.Format(Language.Instance.GetMessageFromKey("REFUSED_GROUP_REQUEST"),
                            Session.Character.Name), 10));
                }
                else if (pjoinPacket.RequestType == GroupRequestType.AcceptedShare)
                {
                    if (targetSession?.Character.GroupSentRequestCharacterIds.GetAllItems().Contains(Session.Character.CharacterId) == false)
                    {
                        return;
                    }

                    targetSession?.Character.GroupSentRequestCharacterIds.Remove(Session.Character.CharacterId);

                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                        string.Format(Language.Instance.GetMessageFromKey("ACCEPTED_SHARE"),
                            targetSession?.Character.Name), 0));
                    if (Session.Character?.Group?.IsMemberOfGroup(pjoinPacket.CharacterId) == true && targetSession != null)
                    {
                        Session.Character.SetReturnPoint(targetSession.Character.Return.DefaultMapId,
                            targetSession.Character.Return.DefaultX, targetSession.Character.Return.DefaultY);
                        targetSession.SendPacket(UserInterfaceHelper.GenerateMsg(
                            string.Format(Language.Instance.GetMessageFromKey("CHANGED_SHARE"), targetSession.Character.Name), 0));
                    }
                }
                else if (pjoinPacket.RequestType == GroupRequestType.DeclinedShare)
                {
                    if (targetSession?.Character.GroupSentRequestCharacterIds.GetAllItems()
                            .Contains(Session.Character.CharacterId) == false)
                    {
                        return;
                    }

                    targetSession?.Character.GroupSentRequestCharacterIds.Remove(Session.Character.CharacterId);

                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("REFUSED_SHARE"), 0));
                }
            }
        }

        // This presumes that weeks start with Monday.
        // Week 1 is the 1st week of the year with a Thursday in it.
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        /// <summary>
        /// pleave packet
        /// </summary>
        /// <param name="pleavePacket"></param>
        public void GroupLeave(PLeavePacket pleavePacket) => ServerManager.Instance.GroupLeave(Session);

        /// <summary>
        /// ; packet
        /// </summary>
        /// <param name="groupSayPacket"></param>
        public void GroupTalk(GroupSayPacket groupSayPacket)
        {
            if (!string.IsNullOrEmpty(groupSayPacket.Message))
            {
                if (ServerManager.Instance.Configuration.UseChatLogService)
                {
                    ChatLogServiceClient.Instance.LogChatMessage(new ChatLogEntry()
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        Receiver = string.Empty,
                        ReceiverId = Session.Character.Group?.GroupId,
                        MessageType = ChatLogType.Group,
                        Message = groupSayPacket.Message
                    });
                }

                ServerManager.Instance.Broadcast(Session, Session.Character.GenerateSpk(groupSayPacket.Message, 3),
                    ReceiverType.Group);
            }
        }

        /// <summary>
        /// guri packet
        /// </summary>
        /// <param name="guriPacket"></param>
        public void Guri(GuriPacket guriPacket)
        {

            try
            {
                if (guriPacket != null)
                {
                    if (guriPacket.Data.HasValue && guriPacket.Type == 10 && guriPacket.Data.Value >= 973
                        && guriPacket.Data.Value <= 9999 && !Session.Character.EmoticonsBlocked)
                    {
                        if (guriPacket.User == Session.Character.CharacterId)
                        {
                            Session.CurrentMapInstance?.Broadcast(Session,
                                StaticPacketHelper.GenerateEff(UserType.Player, Session.Character.CharacterId,
                                    guriPacket.Data.Value + 4099), ReceiverType.AllNoEmoBlocked);
                        }
                        else if (int.TryParse(guriPacket.User.ToString(), out int mateTransportId))
                        {
                            Mate mate = Session.Character.Mates.Find(s => s.MateTransportId == mateTransportId);
                            if (mate != null)
                            {
                                Session.CurrentMapInstance?.Broadcast(Session,
                                    StaticPacketHelper.GenerateEff(UserType.Npc, mate.MateTransportId,
                                        guriPacket.Data.Value + 4099), ReceiverType.AllNoEmoBlocked);
                            }
                        }
                    }
                    else if (guriPacket.Type == 205)
                    {
                        const int perfumeVnum = 1428;
                        InventoryType perfumeInventoryType = (InventoryType)guriPacket.Argument; ;
                        ItemInstance eq = Session.Character.Inventory.LoadBySlotAndType((short)guriPacket.User, perfumeInventoryType);

                        if (eq.BoundCharacterId == Session.Character.CharacterId)
                        {
                            // ALREADY YOURS
                            return;
                        }
                        int perfumesNeeded = UserInterfaceHelper.Instance.PerfumeFromItemLevelAndShellRarity(eq.Item.LevelMinimum, (byte)eq.Rare);
                        if (Session.Character.Inventory.CountItem(perfumeVnum) < perfumesNeeded)
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NOT_ENOUGH_ITEM"), 0));
                            return;
                        }

                        Session.Character.Inventory.RemoveItemAmount(perfumeVnum, perfumesNeeded);
                        eq.BoundCharacterId = Session.Character.CharacterId;
                        Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_YOURS_NOW"), 0));
                    }
                    else if (guriPacket.Type == 204)
                    {
                        if (guriPacket.Argument == 0 && short.TryParse(guriPacket.User.ToString(), out short slot))
                        {
                            ItemInstance shell =
                                Session.Character.Inventory.LoadBySlotAndType(slot, InventoryType.Equipment);
                            if (shell?.ShellEffects.Count == 0 && shell.Upgrade > 0 && shell.Rare > 0
                                && Session.Character.Inventory.CountItem(1429) >= ((shell.Upgrade / 10) + shell.Rare))
                            {
                                shell.SetShellEffects();
                                Session.SendPacket(UserInterfaceHelper.GenerateMsg("Opzioni Identificate", 0));
                                Session.SendPacket(StaticPacketHelper.GenerateEff(UserType.Player, Session.Character.CharacterId, 3006));
                                Session.Character.Inventory.RemoveItemAmount(1429, (shell.Upgrade / 10) + shell.Rare);
                            }
                        }
                    }
                    else if (guriPacket.Type == 300)
                    {
                        if (guriPacket.Argument == 8023 && short.TryParse(guriPacket.User.ToString(), out short slot))
                        {
                            ItemInstance box = Session.Character.Inventory.LoadBySlotAndType(slot, InventoryType.Equipment);
                            if (box != null && box.Item.ItemType == ItemType.Box)
                            {
                                box.Item.Use(Session, ref box, 1, new[] { guriPacket.Data.ToString() });
                            }
                        }
                    }
                    else if (guriPacket.Argument == 8024 && short.TryParse(guriPacket.User.ToString(), out short slot2))
                    {
                        ItemInstance box = Session.Character.Inventory.LoadBySlotAndType(slot2, InventoryType.Main);
                        if (box != null)
                        {
                            box.Item.Use(Session, ref box, 1, new[] { guriPacket.Data.ToString() });
                        }
                    }
                }
                else if (guriPacket.Type == 2000)
                {
                    ClientSession targetSposo = ServerManager.Instance.GetSessionByCharacterId(Convert.ToInt32(guriPacket.Argument));
                    Session.Character.AddRelation(targetSposo.Character.CharacterId, CharacterRelationType.Spouse);
                    Session.SendPacket(Session.Character.GenerateFinit());
                    targetSposo.SendPacket(targetSposo.Character.GenerateFinit());
                    string message = targetSposo.Character.Name + " e " + Session.Character.Name + " sono ora sposati!";
                    Session.SendPacket(StaticPacketHelper.GenerateEff(UserType.Player, Session.Character.CharacterId, 839));
                    targetSposo.SendPacket(StaticPacketHelper.GenerateEff(UserType.Player, targetSposo.Character.CharacterId, 839));
                    ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(message), 5), 0);
                    ServerManager.Instance.Broadcast($"msg 0 {message}");
                }
                else if (guriPacket.Type == 501)
                {
                    if (ServerManager.Instance.IceBreakerInWaiting && IceBreaker.Map.Sessions.Count() < IceBreaker.MaxAllowedPlayers)
                    {
                        ServerManager.Instance.TeleportOnRandomPlaceInMap(Session, IceBreaker.Map.MapInstanceId);
                    }
                }
                else if (guriPacket.Type == 502)
                {
                    if (Session.Character.Group != null)
                    {
                        if (Session.Character.Group.Raid != null)
                        {
                            long? charidraid = guriPacket.User;
                            if (charidraid == null)
                            {
                                return;
                            }
                            ClientSession targetraid = ServerManager.Instance.GetSessionByCharacterId(charidraid.Value);
                            if (EventHelper.Instance.Save.Contains(targetraid.Character))
                            {
                                EventHelper.Instance.Save.Remove(targetraid.Character);
                            }
                            targetraid.Character.RemoveBuff(569);
                            targetraid?.CurrentMapInstance?.Broadcast(
                                UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("GLACE_PLAYER_UNFROZEN"), targetraid.Character?.Name), 0));
                            return;
                        }
                    }
                    long? charid = guriPacket.User;
                    if (charid == null)
                    {
                        return;
                    }
                    ClientSession target = ServerManager.Instance.GetSessionByCharacterId(charid.Value);
                    IceBreaker.FrozenPlayers.Remove(target);
                    IceBreaker.AlreadyFrozenPlayers.Add(target);
                    target?.CurrentMapInstance?.Broadcast(
                        UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("ICEBREAKER_PLAYER_UNFROZEN"), target.Character?.Name), 0));
                }
                else if (guriPacket.Type == 506)
                {
                    Session.Character.IsWaitingForEvent |= ServerManager.Instance.EventInWaiting;
                }

                else if (guriPacket.Type == 514)
                {
                    if (ServerManager.Instance.EventInWaiting)
                    {
                        Session.Character.IsWaitingForEvent = true;
                    }
                }
                else if (guriPacket.Type == 199 && guriPacket.Argument == 2)
                {
                    short[] listWingOfFriendship = { 2160, 2312, 10048 };
                    short vnumToUse = -1;
                    foreach (short vnum in listWingOfFriendship)
                    {
                        if (Session.Character.Inventory.CountItem(vnum) > 0)
                        {
                            vnumToUse = vnum;
                        }
                    }

                    if (vnumToUse != -1 || Session.Character.IsSpouseOfCharacter(guriPacket.User))
                    {
                        ClientSession session = ServerManager.Instance.GetSessionByCharacterId(guriPacket.User);
                        if (session != null)
                        {
                            if (Session.Character.IsFriendOfCharacter(guriPacket.User) || Session.Character.IsSpouseOfCharacter(guriPacket.User))
                            {
                                if (session.CurrentMapInstance?.MapInstanceType == MapInstanceType.BaseMapInstance)
                                {
                                    if (Session.Character.MapInstance.MapInstanceType
                                        != MapInstanceType.BaseMapInstance
                                        || (ServerManager.Instance.ChannelId == 51
                                         && Session.Character.Faction != session.Character.Faction))
                                    {
                                        Session.SendPacket(
                                            Session.Character.GenerateSay(
                                                Language.Instance.GetMessageFromKey("CANT_USE_THAT"), 10));
                                        return;
                                    }

                                    short mapy = session.Character.PositionY;
                                    short mapx = session.Character.PositionX;
                                    short mapId = session.Character.MapInstance.Map.MapId;

                                    ServerManager.Instance.ChangeMap(Session.Character.CharacterId, mapId, mapx, mapy);
                                    if (!Session.Character.IsSpouseOfCharacter(guriPacket.User))
                                    {
                                        Session.Character.Inventory.RemoveItemAmount(vnumToUse);
                                    }
                                }
                                else
                                {
                                    Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                                        Language.Instance.GetMessageFromKey("USER_ON_INSTANCEMAP"), 0));
                                }
                            }
                        }
                        else
                        {
                            Session.SendPacket(
                                UserInterfaceHelper.GenerateMsg(
                                    Language.Instance.GetMessageFromKey("USER_NOT_CONNECTED"), 0));
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NO_WINGS"), 10));
                    }
                }
                else if (guriPacket.Type == 400)
                {
                    if (!int.TryParse(guriPacket.Argument.ToString(), out int mapNpcId)
                        || !Session.HasCurrentMapInstance)
                    {
                        return;
                    }

                    MapNpc npc = Session.CurrentMapInstance.Npcs.Find(n => n.MapNpcId.Equals(mapNpcId));
                    if (npc != null)
                    {
                        NpcMonster mapobject = ServerManager.GetNpc(npc.NpcVNum);

                        int rateDrop = ServerManager.Instance.Configuration.RateDrop;
                        int delay = (int)Math.Round(
                            (3 + (mapobject.RespawnTime / 1000d)) * Session.Character.TimesUsed);
                        delay = delay > 11 ? 8 : delay;
                        if (Session.Character.LastMapObject.AddSeconds(delay) < DateTime.Now)
                        {
                            if (mapobject.Drops.Any(s => s.MonsterVNum != null) && mapobject.VNumRequired > 10
                                && Session.Character.Inventory.CountItem(mapobject.VNumRequired)
                                < mapobject.AmountRequired)
                            {
                                Session.SendPacket(
                                    UserInterfaceHelper.GenerateMsg(
                                        Language.Instance.GetMessageFromKey("NOT_ENOUGH_ITEM"), 0));
                                return;
                            }

                            Random random = new Random();
                            double randomAmount = ServerManager.RandomNumber() * random.NextDouble();
                            DropDTO drop = mapobject.Drops.Find(s => s.MonsterVNum == npc.NpcVNum);
                            if (drop != null)
                            {
                                int dropChance = drop.DropChance;
                                if (randomAmount <= (double)dropChance * rateDrop / 5000.000)
                                {
                                    short vnum = drop.ItemVNum;
                                    ItemInstance newInv = Session.Character.Inventory.AddNewToInventory(vnum)
                                        .FirstOrDefault();
                                    Session.Character.LastMapObject = DateTime.Now;
                                    Session.Character.TimesUsed++;
                                    if (Session.Character.TimesUsed >= 4)
                                    {
                                        Session.Character.TimesUsed = 0;
                                    }

                                    if (newInv != null)
                                    {
                                        Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                                            string.Format(Language.Instance.GetMessageFromKey("RECEIVED_ITEM"),
                                                newInv.Item.Name), 0));
                                        Session.SendPacket(Session.Character.GenerateSay(
                                            string.Format(Language.Instance.GetMessageFromKey("RECEIVED_ITEM"),
                                                newInv.Item.Name), 11));
                                        if (npc.NpcVNum == 2004)
                                        {
                                            Session.CurrentMapInstance.Broadcast(StaticPacketHelper.Out(UserType.Npc, npc.MapNpcId));
                                            Session.CurrentMapInstance.RemoveNpc(npc);
                                        }
                                    }
                                    else
                                    {
                                        Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                                            Language.Instance.GetMessageFromKey("NOT_ENOUGH_PLACE"), 0));
                                    }
                                }
                                else
                                {
                                    Session.SendPacket(
                                        UserInterfaceHelper.GenerateMsg(
                                            Language.Instance.GetMessageFromKey("TRY_FAILED"), 0));
                                }
                            }
                            Session.Character.IncrementQuests(QuestType.Inspect, npc.NpcVNum);
                        }
                        else
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                                string.Format(Language.Instance.GetMessageFromKey("TRY_FAILED_WAIT"),
                                    (int)(Session.Character.LastMapObject.AddSeconds(delay) - DateTime.Now)
                                    .TotalSeconds), 0));
                        }

                    }
                }
                else if (guriPacket.Type == 710)
                {
                    if (guriPacket.Value != null)
                    {
                        // TODO: MAP TELEPORTER
                    }
                }
                else if (guriPacket.Type == 8883)
                {
                    double currentRunningSeconds = (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(-50)).TotalSeconds;
                    if (Session.Character.Gold >= guriPacket.Argument && guriPacket.Argument >= 1000)
                    {
                        if (Session.Character.Family != null)
                        {
                            Session.Character.LastPortal = currentRunningSeconds;
                            Session.Character.Gold -= guriPacket.Argument;
                            Session.SendPacket(Session.Character.GenerateGold());
                            MapCell pos = ServerManager.Instance.FamilyArenaInstance.Map.GetRandomPosition();
                            ServerManager.Instance.ChangeMapInstance(Session.Character.CharacterId, ServerManager.Instance.FamilyArenaInstance.MapInstanceId, pos.X, pos.Y);
                        }
                        else
                        {
                            Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NO_FAMILY"), 10));
                        }
                    }
                    else
                    {
                        Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MONEY"), 10));
                    }

                }
                else if (guriPacket.Type == 8887)
                {
                    double currentRunningSeconds = (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(-50)).TotalSeconds;
                    if (Session.Character.Gold >= guriPacket.Argument && guriPacket.Argument >= 500)
                    {
                        Session.Character.LastPortal = currentRunningSeconds;
                        Session.Character.Gold -= guriPacket.Argument;
                        Session.SendPacket(Session.Character.GenerateGold());
                        MapCell pos = ServerManager.Instance.ArenaInstance.Map.GetRandomPosition();
                        ServerManager.Instance.ChangeMapInstance(Session.Character.CharacterId, ServerManager.Instance.ArenaInstance.MapInstanceId, pos.X, pos.Y);
                    }
                    else
                    {
                        Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MONEY"), 10));
                    }

                }

              
                //Session.SendPacket($"pdti 11 4500 1 29 0 1");
                
                else if (guriPacket.Type == 750)
                {
                    const short baseVnum = 1623;
                    if (Enum.TryParse(guriPacket.Argument.ToString(), out FactionType faction)
                        && Session.Character.Inventory.CountItem(baseVnum + (byte)faction) > 0)
                    {
                        if ((byte)faction < 3)
                        {
                            if (Session.Character.Family != null)
                            {
                                Session.SendPacket(
                                    UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("IN_FAMILY"),
                                        0));
                                return;
                            }

                            Session.Character.Faction = faction;
                            Session.Character.Inventory.RemoveItemAmount(baseVnum + (byte)faction);
                            Session.SendPacket("scr 0 0 0 0 0 0 0");
                            Session.SendPacket(Session.Character.GenerateFaction());
                            Session.SendPacket(StaticPacketHelper.GenerateEff(UserType.Player,
                                Session.Character.CharacterId, 4799 + (byte)faction));
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                                Language.Instance.GetMessageFromKey($"GET_PROTECTION_POWER_{(byte)faction}"), 0));
                        }
                        else
                        {
                            if (Session.Character.Family == null || Session.Character.Family.FamilyCharacters
                                    .Find(s => s.Authority.Equals(FamilyAuthority.Head))?.CharacterId
                                    .Equals(Session.Character.CharacterId) != true)
                            {
                                Session.SendPacket(
                                    UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_FAMILY"),
                                        0));
                                return;
                            }


                            Session.Character.Family.Faction = (FactionType)((byte)faction / 2);
                            FamilyDTO savefam = Session.Character.Family;
                            DAOFactory.FamilyDAO.InsertOrUpdate(ref savefam);
                            Session.Character.Inventory.RemoveItemAmount(baseVnum + (byte)faction);
                            Session.SendPacket("scr 0 0 0 0 0 0 0");
                            Session.SendPacket(Session.Character.GenerateFaction());
                            Session.SendPacket(StaticPacketHelper.GenerateEff(UserType.Player,
                                Session.Character.CharacterId, 4799 + ((byte)faction / 2)));
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                                Language.Instance.GetMessageFromKey($"GET_PROTECTION_POWER_{(byte)faction / 2}"), 0));
                            Session.Character.Save();
                            ServerManager.Instance.FamilyRefresh(Session.Character.Family.FamilyId);
                            CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                            {
                                DestinationCharacterId = Session.Character.Family.FamilyId,
                                SourceCharacterId = 0,
                                SourceWorldId = ServerManager.Instance.WorldId,
                                Message = "fhis_stc",
                                Type = MessageType.Family
                            });
                        }
                    }
                }
                else if (guriPacket.Type == 2)
                {
                    Session.CurrentMapInstance?.Broadcast(
                        UserInterfaceHelper.GenerateGuri(2, 1, Session.Character.CharacterId),
                        Session.Character.PositionX, Session.Character.PositionY);
                }
                else if (guriPacket.Type == 4)
                {
                    const int speakerVNum = 2173;
                    const int speakerVNumLimited = 10028;
                    const int bubbleVNum = 2174;
                    const int petnameVNum = 2157;
                    if (guriPacket.Argument == 1)
                    {
                        Mate mate = Session.Character.Mates.Find(s => s.MateTransportId == guriPacket.Data);
                        if (mate != null && Session.Character.Inventory.CountItem(petnameVNum) > 0)
                        {
                            mate.Name = guriPacket.Value.Truncate(16);
                            Session.CurrentMapInstance?.Broadcast(mate.GenerateOut(), ReceiverType.AllExceptMe);
                            Session.CurrentMapInstance?.Broadcast(mate.GenerateIn());
                            Session.SendPacket(
                                UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("NEW_NAME_PET")));
                            Session.SendPacket(Session.Character.GeneratePinit());
                            Session.SendPackets(Session.Character.GeneratePst());
                            Session.SendPackets(Session.Character.GenerateScP());
                            Session.Character.Inventory.RemoveItemAmount(petnameVNum);
                        }
                    }

                    // presentation message
                    if (guriPacket.Argument == 2)
                    {
                        int presentationVNum = Session.Character.Inventory.CountItem(1117) > 0
                            ? 1117
                            : (Session.Character.Inventory.CountItem(9013) > 0 ? 9013 : -1);
                        if (presentationVNum != -1)
                        {
                            string message = string.Empty;
                            string[] valuesplit = guriPacket.Value?.Split(' ');
                            if (valuesplit == null)
                            {
                                return;
                            }

                            for (int i = 0; i < valuesplit.Length; i++)
                            {
                                message += valuesplit[i] + "^";
                            }

                            message = message.Substring(0, message.Length - 1); // Remove the last ^
                            message = message.Trim();
                            if (message.Length > 60)
                            {
                                message = message.Substring(0, 60);
                            }

                            Session.Character.Biography = message;
                            Session.SendPacket(
                                Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("INTRODUCTION_SET"),
                                    10));
                            Session.Character.Inventory.RemoveItemAmount(presentationVNum);
                        }
                    }

                    // Speaker
                    if (guriPacket.Argument == 3 && Session.Character.Inventory.CountItem(speakerVNum) > 0)
                    {
                        string message =
                            $"<{Language.Instance.GetMessageFromKey("SPEAKER")}> [{Session.Character.Name}]:";
                        int baseLength = message.Length;
                        string[] valuesplit = guriPacket.Value?.Split(' ');
                        if (valuesplit == null)
                        {
                            return;
                        }

                        for (int i = 0; i < valuesplit.Length; i++)
                        {
                            message += valuesplit[i] + " ";
                        }

                        if (message.Length > 120 + baseLength)
                        {
                            message = message.Substring(0, 120 + baseLength);
                        }

                        message = message.Trim();

                        if (Session.Character.IsMuted())
                        {
                            Session.SendPacket(
                                Session.Character.GenerateSay(
                                    Language.Instance.GetMessageFromKey("SPEAKER_CANT_BE_USED"), 10));
                            return;
                        }

                        if (Session.Character.LastSpeakerUse.AddSeconds(40) > DateTime.Now)
                        {
                            Session.SendPacket(
                                Session.Character.GenerateSay(
                                    Language.Instance.GetMessageFromKey("SPEAKER_40_SECONDS"), 10));
                            return;
                        }

                        Session.Character.Inventory.RemoveItemAmount(speakerVNum);
                        ServerManager.Instance.Broadcast(Session.Character.GenerateSay(message, 13));
                        Session.Character.LastSpeakerUse = DateTime.Now;

                        if (ServerManager.Instance.Configuration.UseChatLogService)
                        {
                            ChatLogServiceClient.Instance.LogChatMessage(new ChatLogEntry()
                            {
                                Sender = Session.Character.Name,
                                SenderId = Session.Character.CharacterId,
                                Receiver = null,
                                ReceiverId = null,
                                MessageType = ChatLogType.Speaker,
                                Message = message
                            });
                        }
                    }
                    else if (guriPacket.Argument == 3 && Session.Character.Inventory.CountItem(speakerVNumLimited) > 0)
                    {
                        string message =
                            $"<{Language.Instance.GetMessageFromKey("SPEAKER")}> [{Session.Character.Name}]:";
                        int baseLength = message.Length;
                        string[] valuesplit = guriPacket.Value?.Split(' ');
                        if (valuesplit == null)
                        {
                            return;
                        }

                        for (int i = 0; i < valuesplit.Length; i++)
                        {
                            message += valuesplit[i] + " ";
                        }

                        if (message.Length > 120 + baseLength)
                        {
                            message = message.Substring(0, 120 + baseLength);
                        }

                        message = message.Trim();

                        if (Session.Character.IsMuted())
                        {
                            Session.SendPacket(
                                Session.Character.GenerateSay(
                                    Language.Instance.GetMessageFromKey("SPEAKER_CANT_BE_USED"), 10));
                            return;
                        }

                        if (Session.Character.LastSpeakerUse.AddSeconds(40) > DateTime.Now)
                        {
                            Session.SendPacket(
                                Session.Character.GenerateSay(
                                    Language.Instance.GetMessageFromKey("SPEAKER_40_SECONDS"), 10));
                            return;
                        }

                        Session.Character.Inventory.RemoveItemAmount(speakerVNumLimited);
                        ServerManager.Instance.Broadcast(Session.Character.GenerateSay(message, 13));
                        Session.Character.LastSpeakerUse = DateTime.Now;

                        if (ServerManager.Instance.Configuration.UseChatLogService)
                        {
                            ChatLogServiceClient.Instance.LogChatMessage(new ChatLogEntry()
                            {
                                Sender = Session.Character.Name,
                                SenderId = Session.Character.CharacterId,
                                Receiver = null,
                                ReceiverId = null,
                                MessageType = ChatLogType.Speaker,
                                Message = message
                            });
                        }
                    }
                    //Bubble
                    if (guriPacket.Argument == 4 && Session.Character.Inventory.CountItem(bubbleVNum) > 0)
                    {
                        Session.Character.Fumetto = guriPacket.Value;
                        Session.Character.IsFumetto = true;
                        Session.Character.FirstFumetto = DateTime.Now;
                        Session.Character.Inventory.RemoveItemAmount(bubbleVNum);
                    }
                }
                else if (guriPacket.Type == 199 && guriPacket.Argument == 1)
                {
                    if (!Session.Character.IsFriendOfCharacter(guriPacket.User) && !Session.Character.IsSpouseOfCharacter(guriPacket.User))
                    {
                        Session.SendPacket(Language.Instance.GetMessageFromKey("CHARACTER_NOT_IN_FRIENDLIST"));
                        return;
                    }
                    if (Session.Character.MapInstance.Map.MapTypes.Any(m => m.MapTypeId == (short)MapTypeEnum.Act4))
                    {
                        Session.SendPacket(UserInterfaceHelper.GenerateDelay(3000, 4, $"#guri^199^2^{guriPacket.User}"));
                    }
                    else
                    {
                        Session.SendPacket(UserInterfaceHelper.GenerateDelay(100, 4, $"#guri^199^2^{guriPacket.User}"));
                    }
                }
                else if (guriPacket.Type == 201)
                {
                }
                else if (guriPacket.Type == 202)
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("PARTNER_BACKPACK"), 10));
                    Session.SendPacket(Session.Character.GeneratePStashAll());
                }
                else if (guriPacket.Type == 208 && guriPacket.Argument == 0)
                {
                    if (short.TryParse(guriPacket.Value, out short mountSlot)
                        && short.TryParse(guriPacket.User.ToString(), out short pearlSlot))
                    {
                        ItemInstance mount =
                            Session.Character.Inventory.LoadBySlotAndType<ItemInstance>(mountSlot, InventoryType.Main);
                        ItemInstance pearl =
                            Session.Character.Inventory.LoadBySlotAndType(pearlSlot, InventoryType.Equipment);
                        if (mount != null && pearl != null)
                        {
                            pearl.HoldingVNum = mount.ItemVNum;
                            Session.Character.Inventory.RemoveItemFromInventory(mount.Id);
                        }
                    }
                }
                else if (guriPacket.Type == 5010)
                {
                    ItemInstance equip = Session?.Character.Inventory.LoadBySlotAndType(0, InventoryType.Equipment);
                    if (equip != null)
                    {
                        if (equip.Rare >= 5)
                        {
                            int qti = 0;
                            switch (equip.Rare)
                            {
                                case 5:
                                    qti = 15000000;
                                    break;
                                case 6:
                                    qti = 20000000;
                                    break;
                                case 7:
                                    qti = 100000000;
                                    break;
                            }
                            if (Session.Character.Gold >= qti)
                            {
                                Session.Character.Gold -= qti;
                                switch (equip.Item.EquipmentSlot)
                                {
                                    case EquipmentType.Armor:
                                    case EquipmentType.MainWeapon:
                                    case EquipmentType.SecondaryWeapon:
                                        equip.ShellEffects.Clear();
                                        DAOFactory.ShellEffectDAO.DeleteByEquipmentSerialId(equip.EquipmentSerialId);
                                        equip.Rare = 0;
                                        equip.SetRarityPoint(Session);
                                        break;
                                }
                                Session.SendPacket(Session.Character.GenerateGold());
                                Session.SendPacket(equip.GenerateInventoryAdd());
                                Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("DERARIFY_SUCCESS"), 10));
                                Logger.LogUserEvent("DERARIFY", Session.GenerateIdentity(), $"[DeRarifyItem] IIId: {equip.Id} ItemVnum: {equip.ItemVNum} Result: Success");
                            }
                            else
                            {
                                Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MONEY"), 10));
                            }
                        }
                    }
                    else
                    {
                        Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("DERARIFY_FAILED"), 10));
                    }

                }
                else if (guriPacket.Type == 209 && guriPacket.Argument == 0)
                {
                    if (short.TryParse(guriPacket.Value, out short mountSlot)
                        && short.TryParse(guriPacket.User.ToString(), out short pearlSlot))
                    {
                        ItemInstance fairy =
                            Session.Character.Inventory.LoadBySlotAndType(mountSlot, InventoryType.Equipment);
                        ItemInstance pearl =
                            Session.Character.Inventory.LoadBySlotAndType(pearlSlot, InventoryType.Equipment);
                        if (fairy != null && pearl != null)
                        {
                            pearl.HoldingVNum = fairy.ItemVNum;
                            pearl.ElementRate = fairy.ElementRate;
                            Session.Character.Inventory.RemoveItemFromInventory(fairy.Id);
                        }
                    }
                }
                else if (guriPacket.Type == 203 && guriPacket.Argument == 0)
                {
                    // SP points initialization
                    int[] listPotionResetVNums = { 1366, 1427, 5115, 9040 };
                    int vnumToUse = -1;
                    foreach (int vnum in listPotionResetVNums)
                    {
                        if (Session.Character.Inventory.CountItem(vnum) > 0)
                        {
                            vnumToUse = vnum;
                        }
                    }

                    if (vnumToUse != -1)
                    {
                        if (Session.Character.UseSp)
                        {
                            ItemInstance specialistInstance =
                                Session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Sp,
                                    InventoryType.Wear);
                            if (specialistInstance != null)
                            {
                                specialistInstance.SlDamage = 0;
                                specialistInstance.SlDefence = 0;
                                specialistInstance.SlElement = 0;
                                specialistInstance.SlHP = 0;

                                specialistInstance.DamageMinimum = 0;
                                specialistInstance.DamageMaximum = 0;
                                specialistInstance.HitRate = 0;
                                specialistInstance.CriticalLuckRate = 0;
                                specialistInstance.CriticalRate = 0;
                                specialistInstance.DefenceDodge = 0;
                                specialistInstance.DistanceDefenceDodge = 0;
                                specialistInstance.ElementRate = 0;
                                specialistInstance.DarkResistance = 0;
                                specialistInstance.LightResistance = 0;
                                specialistInstance.FireResistance = 0;
                                specialistInstance.WaterResistance = 0;
                                specialistInstance.CriticalDodge = 0;
                                specialistInstance.CloseDefence = 0;
                                specialistInstance.DistanceDefence = 0;
                                specialistInstance.MagicDefence = 0;
                                specialistInstance.HP = 0;
                                specialistInstance.MP = 0;

                                Session.Character.Inventory.RemoveItemAmount(vnumToUse);
                                Session.Character.Inventory.DeleteFromSlotAndType((byte)EquipmentType.Sp,
                                    InventoryType.Wear);
                                Session.Character.Inventory.AddToInventoryWithSlotAndType(specialistInstance,
                                    InventoryType.Wear, (byte)EquipmentType.Sp);
                                Session.SendPacket(Session.Character.GenerateCond());

                                ItemInstance mainWeapon = Session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.MainWeapon, InventoryType.Wear);
                                ItemInstance secondaryWeapon = Session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.SecondaryWeapon, InventoryType.Wear);
                                List<ShellEffectDTO> effects = new List<ShellEffectDTO>();
                                if (mainWeapon?.ShellEffects != null)
                                {
                                    effects.AddRange(mainWeapon.ShellEffects);
                                }
                                if (secondaryWeapon?.ShellEffects != null)
                                {
                                    effects.AddRange(secondaryWeapon.ShellEffects);
                                }

                                Session.SendPacket(specialistInstance.GenerateSlInfo());
                                Session.SendPacket(Session.Character.GenerateLev());
                                Session.SendPacket(Session.Character.GenerateStatChar());
                                Session.SendPacket(
                                    UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("POINTS_RESET"),
                                        0));
                            }
                        }
                        else
                        {
                            Session.SendPacket(
                                Session.Character.GenerateSay(
                                    Language.Instance.GetMessageFromKey("TRANSFORMATION_NEEDED"), 10));
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_POINTS"),
                                10));
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// hero packet
        /// </summary>
        /// <param name="heroPacket"></param>
        public void Hero(HeroPacket heroPacket)
        {
            if (!string.IsNullOrEmpty(heroPacket.Message))
            {
                if (Session.Character.IsReputationHero() >= 3 || Session.Account.Authority >= AuthorityType.GameMaster)
                {
                    if (Session.CurrentMapInstance.Map.MapId != (short)MapTypeEnum.Act4)
                    {
                        heroPacket.Message = heroPacket.Message.Trim();
                        ServerManager.Instance.Broadcast(Session, $"msg 5 [{Session.Character.Name}]:{heroPacket.Message}",
                            ReceiverType.AllNoHeroBlocked);
                    }
                }
                else
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("USER_NOT_HERO"), 11));
                }
            }
        }

        /// <summary>
        /// PreqPacket packet
        /// </summary>
        /// <param name="packet"></param>
        public void Preq(PreqPacket packet)
        {
            double currentRunningSeconds =
                (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(-50)).TotalSeconds;
            double timeSpanSinceLastPortal = currentRunningSeconds - Session.Character.LastPortal;
            if (!(timeSpanSinceLastPortal >= 4) || !Session.HasCurrentMapInstance)
            {
                Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("CANT_MOVE"), 10));
                return;
            }
            if (Session.Character.Morph == 1564)
            {
                Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("CANT_MOVE"), 10));
                return;
            }

            if (Session.CurrentMapInstance.Portals.Concat(Session.Character.GetExtraPortal())
                    .FirstOrDefault(s => s != null &&
                        Session.Character.PositionY >= s?.SourceY - 1 && Session.Character.PositionY <= s?.SourceY + 1
                                                                     && Session.Character.PositionX >= s?.SourceX - 1
                                                                     && Session.Character.PositionX
                                                                     <= s?.SourceX + 1) is Portal
                portal)
            {
                switch (portal.Type)
                {
                    case (sbyte)PortalType.MapPortal:
                    case (sbyte)PortalType.TSNormal:
                    case (sbyte)PortalType.Open:
                    case (sbyte)PortalType.Miniland:
                    case (sbyte)PortalType.TSEnd:
                    case (sbyte)PortalType.Exit:
                    case (sbyte)PortalType.Effect:
                    case (sbyte)PortalType.ShopTeleport:
                        break;

                    case (sbyte)PortalType.Raid:
                        if (Session.Character.Group?.Raid != null)
                        {
                            if (Session.Character.Group.IsLeader(Session))
                            {
                                Session.SendPacket(
                                    $"qna #mkraid^0^275 {Language.Instance.GetMessageFromKey("RAID_START_QUESTION")}");
                            }
                            else
                            {
                                Session.SendPacket(
                                    Session.Character.GenerateSay(
                                        Language.Instance.GetMessageFromKey("NOT_TEAM_LEADER"), 10));
                            }
                        }
                        else
                        {
                            Session.SendPacket(
                                Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NEED_TEAM"), 10));
                        }

                        return;

                    case (sbyte)PortalType.BlueRaid:
                    case (sbyte)PortalType.DarkRaid:
                        if ((int)Session.Character.Faction == portal.Type - 9
                            && Session.Character.Family?.Act4RaidBossMap != null
                            && Session.Character.Level > 59
                            && Session.Character.Reputation > 60000)
                        {
                            //Session.Character.SetReputation(Session.Character.Level * -50);

                            Session.Character.LastPortal = currentRunningSeconds;

                            switch (Session.Character.Family.Act4RaidBossMap.MapInstanceType)
                            {
                                case MapInstanceType.Act4Morcos:
                                    ServerManager.Instance.ChangeMapInstance(Session.Character.CharacterId,
                                        Session.Character.Family.Act4RaidBossMap.MapInstanceId, 55, 80);
                                    break;

                                case MapInstanceType.Act4Hatus:
                                    ServerManager.Instance.ChangeMapInstance(Session.Character.CharacterId,
                                        Session.Character.Family.Act4RaidBossMap.MapInstanceId, 36, 58);
                                    break;

                                case MapInstanceType.Act4Calvina:
                                    ServerManager.Instance.ChangeMapInstance(Session.Character.CharacterId,
                                        Session.Character.Family.Act4RaidBossMap.MapInstanceId, 9, 41);
                                    break;

                                case MapInstanceType.Act4Berios:
                                    ServerManager.Instance.ChangeMapInstance(Session.Character.CharacterId,
                                        Session.Character.Family.Act4RaidBossMap.MapInstanceId, 29, 54);
                                    break;
                            }

                        }
                        else
                        {
                            Session.SendPacket(
                                Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("PORTAL_BLOCKED"),
                                    10));
                        }

                        return;

                    default:
                        return;
                }
                if (Session.Character.Timespace != null)
                {
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.TimeSpaceInstance
                        && !Session.Character.Timespace.InstanceBag.Lock)
                    {
                        if (Session.Character.CharacterId == Session.Character.Timespace.InstanceBag.CreatorId)
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateDialog(
                                $"#rstart^1 rstart {Language.Instance.GetMessageFromKey("FIRST_ROOM_START")}"));
                        }
                        return;
                    }
                }
                portal.OnTraversalEvents.ForEach(e => EventHelper.Instance.RunEvent(e));
                if (portal.DestinationMapInstanceId == default)
                {
                    return;
                }

                if (ServerManager.Instance.ChannelId == 51)
                {
                    if ((Session.Character.Faction == FactionType.Angel && portal.DestinationMapId == 131)
                        || (Session.Character.Faction == FactionType.Demon && portal.DestinationMapId == 130))
                    {
                        Session.SendPacket(
                            Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("PORTAL_BLOCKED"), 10));
                        return;
                    }

                    if ((portal.DestinationMapId == 130 || portal.DestinationMapId == 131)
                        && timeSpanSinceLastPortal < 10)
                    {
                        Session.SendPacket(
                            Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("CANT_MOVE"), 10));
                        return;
                    }
                }


                Session.SendPacket(Session.CurrentMapInstance.GenerateRsfn());

                Session.Character.LastPortal = currentRunningSeconds;

                if (ServerManager.GetMapInstance(portal.SourceMapInstanceId).MapInstanceType
                    != MapInstanceType.BaseMapInstance
                    && ServerManager.GetMapInstance(portal.DestinationMapInstanceId).MapInstanceType
                    == MapInstanceType.BaseMapInstance)
                {
                    ServerManager.Instance.ChangeMap(Session.Character.CharacterId, Session.Character.MapId,
                        Session.Character.MapX, Session.Character.MapY);
                }
                else if (portal.DestinationMapInstanceId == Session.Character.Miniland.MapInstanceId)
                {
                    ServerManager.Instance.JoinMiniland(Session, Session);
                }
                else if (portal.DestinationMapId == 20000)
                {
                    ClientSession sess = ServerManager.Instance.Sessions.FirstOrDefault(s =>
                        s.Character.Miniland.MapInstanceId == portal.DestinationMapInstanceId);
                    if (sess != null)
                    {
                        ServerManager.Instance.JoinMiniland(Session, sess);
                    }
                }
                else
                {
                    if (ServerManager.Instance.ChannelId == 51)
                    {
                        short destinationX = portal.DestinationX;
                        short destinationY = portal.DestinationY;

                        if (portal.DestinationMapInstanceId == CaligorRaid.CaligorMapInstance?.MapInstanceId
                        ) /* Caligor Raid Map */
                        {
                            switch (Session.Character.Faction)
                            {
                                case FactionType.Angel:
                                    destinationX = 50;
                                    destinationY = 172;
                                    break;
                                case FactionType.Demon:
                                    destinationX = 130;
                                    destinationY = 172;
                                    break;
                            }
                        }
                        else if (portal.DestinationMapId == 153) /* Unknown Land */
                        {
                            switch (Session.Character.Faction)
                            {
                                case FactionType.Angel:
                                    destinationX = 50;
                                    destinationY = 172;
                                    break;
                                case FactionType.Demon:
                                    destinationX = 130;
                                    destinationY = 172;
                                    break;
                            }
                        }

                        ServerManager.Instance.ChangeMapInstance(Session.Character.CharacterId,
                            portal.DestinationMapInstanceId, destinationX, destinationY);
                    }
                    else
                    {
                        ServerManager.Instance.ChangeMapInstance(Session.Character.CharacterId,
                            portal.DestinationMapInstanceId, portal.DestinationX, portal.DestinationY);
                        if (Session.CurrentMapInstance?.MapInstanceType == MapInstanceType.MappaExpInstance)
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateGuri(15, 1, Session.Character.CharacterId));
                        }
                        if (Session.CurrentMapInstance?.MapInstanceType == MapInstanceType.MappaExpInstanceSolo)
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateGuri(15, 1, Session.Character.CharacterId));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// pulse packet
        /// </summary>
        /// <param name="pulsepacket"></param>
        public void Pulse(PulsePacket pulsepacket)
        {
            if (Session.Character.LastPulse.AddMilliseconds(80000) >= DateTime.Now
                && DateTime.Now >= Session.Character.LastPulse.AddMilliseconds(40000))
            {
                Session.Character.LastPulse = DateTime.Now;
            }
            else
            {
                Session.Disconnect();
            }

            Session.Character.MuteMessage();
            Session.Character.DeleteTimeout();
            CommunicationServiceClient.Instance.PulseAccount(Session.Account.AccountId);
        }

        /// <summary>
        /// rlPacket packet
        /// </summary>
        /// <param name="rlPacket"></param>
        public void RaidListRegister(RlPacket rlPacket)
        {
            try
            {
                switch (rlPacket.Type)
                {
                    case 0:
                        if (Session.Character.Group?.IsLeader(Session) == true
                            && Session.Character.Group.GroupType != GroupType.Group
                            && ServerManager.Instance.GroupList.Any(s => s.GroupId == Session.Character.Group.GroupId))
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateRl(1));
                        }
                        else if (Session.Character.Group != null && Session.Character.Group.GroupType != GroupType.Group
                                 && Session.Character.Group.IsLeader(Session))
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateRl(2));
                        }
                        else if (Session.Character.Group != null)
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateRl(3));
                        }
                        else
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateRl(0));
                        }

                        break;

                    case 1:
                        if (Session.Character.Group != null && Session.Character.Group.GroupType != GroupType.Group
                            && !ServerManager.Instance.GroupList.Any(s => s.GroupId == Session.Character.Group.GroupId))
                        {
                            ServerManager.Instance.GroupList.Add(Session.Character.Group);
                            Session.SendPacket(UserInterfaceHelper.GenerateRl(1));
                            Session.SendPacket(
                                UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("RAID_REGISTERED")));
                            ServerManager.Instance.Broadcast(Session,
                                $"qnaml 100 #rl {string.Format(Language.Instance.GetMessageFromKey("SEARCH_TEAM_MEMBERS"), Session.Character.Name, Session.Character.Group.Raid?.Label)}",
                                ReceiverType.AllExceptGroup);

                            Session.Character.GeneralLogs.Add(new GeneralLogDTO
                            {
                                AccountId = Session.Account.AccountId,
                                CharacterId = Session.Character.CharacterId,
                                IpAddress = Session.IpAddress,
                                LogData = "RaidList | " + ServerManager.Instance.ChannelId,
                                LogType = Session.Character.Group.Raid?.Label,
                                Timestamp = DateTime.Now
                            });
                        }

                        break;

                    case 2:
                        try
                        {
                            if (Session.Character.Group != null && Session.Character.Group.GroupType != GroupType.Group
                                && ServerManager.Instance.GroupList.Any(s => s.GroupId == Session.Character.Group.GroupId))
                            {
                                ServerManager.Instance.GroupList.Remove(Session.Character.Group);
                                Session.SendPacket(UserInterfaceHelper.GenerateRl(2));
                                Session.SendPacket(
                                    UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("RAID_UNREGISTERED")));
                            }
                        }
                        catch (Exception ex)
                        {

                        }

                        break;

                    case 3:
                        ClientSession cl = ServerManager.Instance.GetSessionByCharacterName(rlPacket.CharacterName);
                        if (cl != null)
                        {
                            ItemInstance amulet = Session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Amulet, InventoryType.Wear);
                            #region Draco
                            if (cl.Character.Group.Raid.Label == "Lord Draco")
                            {
                                if (amulet != null)
                                {
                                    if (amulet.ItemVNum != 4503)
                                    {
                                        Session.SendPacket($"info {Language.Instance.GetMessageFromKey("NO_AMULET_DRACO")}");
                                        return;
                                    }
                                    /*else
                                    {
                                        int volte = 10 - Session.Character.GeneralLogs.CountLinq(s => s.LogType == "Raid" && s.LogData == "Lord Draco" && s.Timestamp.Date == DateTime.Today);
                                        if (volte == 0)
                                        {
                                            Session.SendPacket(
                                                UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("DRACO_MAX")),
                                                    0));
                                            return;
                                        }
                                    }*/
                                }
                                else
                                {
                                    Session.SendPacket($"info {Language.Instance.GetMessageFromKey("NO_AMULET_DRACO")}");
                                    return;
                                }
                            }
                            #endregion

                            #region Glacerus
                            if (cl.Character.Group.Raid.Label == "Glacerus the Ice Cold")
                            {
                                if (amulet != null)
                                {
                                    if (amulet.ItemVNum != 4504)
                                    {
                                        Session.SendPacket($"info {Language.Instance.GetMessageFromKey("NO_AMULET_GLACE")}");
                                        return;
                                    }
                                    /*else
                                    {
                                        int volte = 10 - Session.Character.GeneralLogs.CountLinq(s => s.LogType == "Raid" && s.LogData == "Raid Glacerus, Il Gelido" && s.Timestamp.Date == DateTime.Today);
                                        if (volte == 0)
                                        {
                                            Session.SendPacket(
                                                UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("GLACE_MAX")),
                                                    0));
                                            return;
                                        }
                                    }*/
                                }
                                else
                                {
                                    Session.SendPacket($"info {Language.Instance.GetMessageFromKey("NO_AMULET_GLACE")}");
                                    return;
                                }
                            }
                            #endregion

                            cl.Character.GroupSentRequestCharacterIds.Add(Session.Character.CharacterId);
                            GroupJoin(new PJoinPacket
                            {
                                RequestType = GroupRequestType.Accepted,
                                CharacterId = cl.Character.CharacterId
                            });
                        }

                        break;
                }
            }
            catch (Exception ex) { }
        }



        /// <summary>
        /// fbPacket packet
        /// </summary>
        /// <param name="fbPacket"></param>
        public void ArcobalenoManage(FbPacket fbPacket)
        {
            Group grp;
            switch (fbPacket.Type)
            {
                // Join BaTeam
                case 1:
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.BAInstance)
                    {
                        return;
                    }

                    ClientSession target = ServerManager.Instance.GetSessionByCharacterId(fbPacket.CharacterId);
                    if (fbPacket.Parameter == null && target?.Character?.Group == null
                        && Session.Character.Group.IsLeader(Session))
                    {
                        GroupJoin(new PJoinPacket
                        {
                            RequestType = GroupRequestType.Invited,
                            CharacterId = fbPacket.CharacterId
                        });
                    }
                    else if (Session.Character.Group == null)
                    {
                        GroupJoin(new PJoinPacket
                        {
                            RequestType = GroupRequestType.Accepted,
                            CharacterId = fbPacket.CharacterId
                        });
                    }

                    break;

                // Leave BaTeam
                case 2:
                    ClientSession sender = ServerManager.Instance.GetSessionByCharacterId(fbPacket.CharacterId);
                    if (sender?.Character?.Group == null)
                    {
                        return;
                    }

                    Session.SendPacket(
                        UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("LEFT_TEAM")),
                            0));
                    if (Session?.CurrentMapInstance?.MapInstanceType == MapInstanceType.BAInstance)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, Session.Character.MapId,
                            Session.Character.MapX, Session.Character.MapY);
                    }

                    grp = sender.Character?.Group;
                    Session.SendPacket(Session.Character.GenerateFbt(1, true));
                    Session.SendPacket(Session.Character.GenerateFbt(2, true));

                    grp.Characters.ForEach(s =>
                    {
                        s.SendPacket(grp.GenerateFblst());
                        //s.SendPacket(grp.GeneraterRaidmbf(s));
                        s.SendPacket(s.Character.GenerateFbt(0));
                    });

                    ArcobalenoMember arcobalenoTeamMember = ServerManager.Instance.ArcobalenoMembers.Where(s => s.Session == Session).First();
                    if (arcobalenoTeamMember != null)
                    {
                        ServerManager.Instance.ArcobalenoMembers.Remove(arcobalenoTeamMember);
                    }


                    ArcobalenoMember arcobalenoTeamMemberRegistered = ServerManager.Instance.ArcobalenoMembersRegistered.Where(s => s.Session == Session).First();
                    if (arcobalenoTeamMemberRegistered != null)
                    {
                        ServerManager.Instance.ArcobalenoMembersRegistered.Remove(arcobalenoTeamMember);
                    }
                    break;

                // Kick from BaTeam
                case 3:
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.BAInstance)
                    {
                        return;
                    }

                    if (Session.Character.Group?.IsLeader(Session) == true)
                    {
                        ClientSession chartokick = ServerManager.Instance.GetSessionByCharacterId(fbPacket.CharacterId);
                        if (chartokick.Character?.Group == null)
                        {
                            return;
                        }

                        ArcobalenoMember arcobalenoTeamMemberRegisteredd = ServerManager.Instance.ArcobalenoMembersRegistered.Where(s => s.Session == chartokick).First();
                        if (arcobalenoTeamMemberRegisteredd != null)
                        {
                            Session.SendPacket(
                                UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CANNOT_KICK_BA"), 0));
                            return;
                        }

                        chartokick.SendPacket(
                            UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("KICK_BA"), 0));
                        grp = chartokick.Character?.Group;
                        chartokick.SendPacket(chartokick.Character?.GenerateFbt(1, true));
                        chartokick.SendPacket(chartokick.Character?.GenerateFbt(2, true));
                        grp?.LeaveGroup(chartokick);
                        grp?.Characters.ForEach(s =>
                        {
                            s.SendPacket(grp.GenerateRdlst());
                            s.SendPacket(s.Character.GenerateFbt(0));
                        });
                        ArcobalenoMember arcobalenoTeamMemberr = ServerManager.Instance.ArcobalenoMembers.Where(s => s.Session == chartokick).First();
                        if (arcobalenoTeamMemberr != null)
                        {
                            ServerManager.Instance.ArcobalenoMembers.Remove(arcobalenoTeamMemberr);
                        }
                    }

                    break;

                // Disolve BaTeam
                case 4:
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.BAInstance)
                    {
                        return;
                    }

                    if (Session.Character.Group?.IsLeader(Session) == true)
                    {
                        ArcobalenoMember arcobalenoTeamMemberRegistereddd = ServerManager.Instance.ArcobalenoMembersRegistered?.Where(s => s.Session == Session).First();
                        if (arcobalenoTeamMemberRegistereddd != null)
                        {
                            Session.SendPacket(
                                UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CANNOT_DISSOLVE_BA"), 0));
                            return;
                        }

                        grp = Session.Character.Group;

                        ClientSession[] grpmembers = new ClientSession[40];
                        grp.Characters.CopyTo(grpmembers);
                        foreach (ClientSession targetSession in grpmembers)
                        {
                            if (targetSession != null)
                            {
                                targetSession.SendPacket(targetSession.Character.GenerateFbt(1, true));
                                targetSession.SendPacket(targetSession.Character.GenerateFbt(2, true));
                                targetSession.SendPacket(
                                    UserInterfaceHelper.GenerateMsg(
                                        Language.Instance.GetMessageFromKey("BA_DISOLVED"), 0));
                                grp.LeaveGroup(targetSession);


                                ArcobalenoMember arcobalenoTeamMemberrr = ServerManager.Instance.ArcobalenoMembers.Where(s => s.Session == targetSession).First();
                                if (arcobalenoTeamMemberrr != null)
                                {
                                    ServerManager.Instance.ArcobalenoMembers.Remove(arcobalenoTeamMemberrr);
                                }
                            }
                        }

                        ServerManager.Instance.GroupList.RemoveAll(s => s.GroupId == grp.GroupId);
                        ServerManager.Instance.GroupsThreadSafe.Remove(grp.GroupId);
                    }

                    break;
            }
        }

        /// <summary>
        /// rdPacket packet
        /// </summary>
        /// <param name="rdPacket"></param>
        public void RaidManage(RdPacket rdPacket)
        {
            Group grp;
            switch (rdPacket.Type)
            {
                // Join Raid
                case 1:

                    if (rdPacket.Parameter != null)
                    {
                        #region Draco
                        if (rdPacket.Parameter.Value == 16)
                        {
                            ItemInstance amulet = Session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Amulet, InventoryType.Wear);
                            if (amulet != null)
                            {
                                if (amulet.ItemVNum != 4503)
                                {
                                    Session.SendPacket($"info {Language.Instance.GetMessageFromKey("NO_AMULET_DRACO")}");
                                    return;
                                }
                                /*else
                                {
                                    int volte = 10 - Session.Character.GeneralLogs.CountLinq(s => s.LogType == "Raid" && s.LogData == "Lord Draco" && s.Timestamp.Date == DateTime.Today);
                                    if (volte == 0)
                                    {
                                        Session.SendPacket(
                                            UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("DRACO_MAX")),
                                                0));
                                        return;
                                    }
                                }*/
                            }
                            else
                            {
                                Session.SendPacket($"info {Language.Instance.GetMessageFromKey("NO_AMULET_DRACO")}");
                                return;
                            }
                        }
                        #endregion

                        #region Glace
                        if (rdPacket.Parameter.Value == 17)
                        {
                            ItemInstance amulet = Session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Amulet, InventoryType.Wear);
                            if (amulet != null)
                            {
                                if (amulet.ItemVNum != 4504)
                                {
                                    Session.SendPacket($"info {Language.Instance.GetMessageFromKey("NO_AMULET_GLACE")}");
                                    return;
                                }
                                /*else
                                {
                                    int volte = 10 - Session.Character.GeneralLogs.CountLinq(s => s.LogType == "Raid" && s.LogData == "Raid Glacerus, Il Gelido" && s.Timestamp.Date == DateTime.Today);
                                    if (volte == 0)
                                    {
                                        Session.SendPacket(
                                            UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("GLACE_MAX")),
                                                0));
                                        return;
                                    }
                                }*/
                            }
                            else
                            {
                                Session.SendPacket($"info {Language.Instance.GetMessageFromKey("NO_AMULET_GLACE")}");
                                return;
                            }
                        }
                        #endregion
                    }

                    if (Session.CurrentMapInstance?.MapInstanceType == MapInstanceType.RaidInstance)
                    {
                        return;
                    }
                    try
                    {
                        ClientSession target = ServerManager.Instance.GetSessionByCharacterId(rdPacket.CharacterId);
                        if (rdPacket.Parameter == null && target?.Character?.Group == null
                            && Session.Character.Group.IsLeader(Session))
                        {
                            GroupJoin(new PJoinPacket
                            {
                                RequestType = GroupRequestType.Invited,
                                CharacterId = rdPacket.CharacterId
                            });
                        }
                        else if (Session.Character.Group == null)
                        {
                            GroupJoin(new PJoinPacket
                            {
                                RequestType = GroupRequestType.Accepted,
                                CharacterId = rdPacket.CharacterId
                            });
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    break;

                // Leave Raid
                case 2:
                    ClientSession sender = ServerManager.Instance.GetSessionByCharacterId(rdPacket.CharacterId);
                    if (sender?.Character?.Group == null)
                    {
                        return;
                    }

                    if (Session?.CurrentMapInstance?.MapInstanceType == MapInstanceType.RaidInstance)
                    {
                        ServerManager.Instance.ChangeMap(Session.Character.CharacterId, Session.Character.MapId,
                            Session.Character.MapX, Session.Character.MapY);
                    }

                    Session.SendPacket(
                        UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("LEFT_RAID")),
                            0));

                    grp = sender.Character?.Group;
                    Session.SendPacket(Session.Character.GenerateRaid(1, true));
                    Session.SendPacket(Session.Character.GenerateRaid(2, true));

                    if (Session.Character.Group.IsLeader(Session))
                    {
                        Session.SendPacket("rd^4");
                    }
                    else
                    {
                        Session.Character.Group.LeaveGroup(Session);
                        grp.Characters.ForEach(s =>
                        {
                            s.SendPacket(grp.GenerateRdlst());

                            if (s?.CurrentMapInstance?.MapInstanceType == MapInstanceType.RaidInstance)
                            {
                                s.SendPacket(grp.GeneraterRaidmbf(s));
                            }
                            s.SendPacket(s.Character.GenerateRaid(0, false));
                        });
                    }
                    break;

                // Kick from Raid
                case 3:
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.RaidInstance)
                    {
                        return;
                    }

                    if (Session.Character.Group?.IsLeader(Session) == true)
                    {
                        ClientSession chartokick = ServerManager.Instance.GetSessionByCharacterId(rdPacket.CharacterId);
                        if (chartokick.Character?.Group == null)
                        {
                            return;
                        }

                        chartokick.SendPacket(
                            UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("KICK_RAID"), 0));
                        grp = chartokick.Character?.Group;
                        chartokick.SendPacket(chartokick.Character?.GenerateRaid(1, true));
                        chartokick.SendPacket(chartokick.Character?.GenerateRaid(2, true));
                        grp?.LeaveGroup(chartokick);
                        grp?.Characters.ForEach(s =>
                        {
                            s.SendPacket(grp.GenerateRdlst());
                            s.SendPacket(s.Character.GenerateRaid(0));
                        });
                    }

                    break;

                // Disolve Raid
                case 4:
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.RaidInstance)
                    {
                        return;
                    }

                    if (Session.Character.Group?.IsLeader(Session) == true)
                    {
                        grp = Session.Character.Group;

                        ClientSession[] grpmembers = new ClientSession[40];
                        grp.Characters.CopyTo(grpmembers);
                        foreach (ClientSession targetSession in grpmembers)
                        {
                            if (targetSession != null)
                            {
                                targetSession.SendPacket(targetSession.Character.GenerateRaid(1, true));
                                targetSession.SendPacket(targetSession.Character.GenerateRaid(2, true));
                                targetSession.SendPacket(
                                    UserInterfaceHelper.GenerateMsg(
                                        Language.Instance.GetMessageFromKey("RAID_DISOLVED"), 0));
                                grp.LeaveGroup(targetSession);
                            }
                        }

                        ServerManager.Instance.GroupList.RemoveAll(s => s.GroupId == grp.GroupId);
                        ServerManager.Instance.GroupsThreadSafe.Remove(grp.GroupId);
                    }

                    break;
            }
        }


        public void QtPacket(QtPacket qtPacket)
        {
            switch (qtPacket.Type)
            {
                // On Target Dest
                case 1:
                    Session.Character.IncrementQuests(QuestType.GoTo, Session.CurrentMapInstance.Map.MapId, Session.Character.PositionX, Session.Character.PositionY);
                    break;

                // Give Up Quest
                case 3:
                    CharacterQuest charQuest = Session.Character.Quests?.FirstOrDefault(q => q.QuestNumber == qtPacket.Data);
                    if (charQuest == null || charQuest.IsMainQuest)
                    {
                        return;
                    }
                    Session.Character.RemoveQuest(charQuest.QuestId, true);
                    break;

                // Ask for rewards
                case 4:
                    break;
            }
        }

        ///<summary>
        ///ps_op packet
        ///</summary>
        ///<param name="AddPetSkillPacket"></param>
        public void AddPetSkill(AddPetSkillPacket AddPetSkillPacket)
        {
            List<Mate> mates = Session.Character.Mates.Where(s => s.MateType == MateType.Partner).ToList();
            Mate m = mates[AddPetSkillPacket.PositionPartner];
            int r = ServerManager.RandomNumber(0, 100);
            if (r < 15)
            {
                r = 1;
            }
            else if (r < 35)
            {
                r = 2;
            }
            else if (r < 50)
            {
                r = 3;
            }
            else if (r < 65)
            {
                r = 4;
            }
            else if (r < 80)
            {
                r = 5;
            }
            else if (r < 90)
            {
                r = 6;
            }
            else
            {
                r = 7;
            }

            switch (AddPetSkillPacket.PositionSkill)
            {
                case 0:
                    if (m.SpInstance != null)
                    {
                        if (m.SpInstance.Rare == 0)
                        {
                            m.SpInstance.Rare = (sbyte)r;
                            Session.SendPacket(m.GenerateScPacket());
                        }
                        else
                        {

                        }
                    }
                    break;
                case 1:
                    if (m.SpInstance != null)
                    {
                        if (m.SpInstance.Upgrade == 0)
                        {
                            m.SpInstance.Upgrade = (byte)r;
                            Session.SendPacket(m.GenerateScPacket());
                        }
                        else
                        {

                        }
                    }
                    break;
                case 2:
                    if (m.SpInstance != null)
                    {
                        if (m.SpInstance.SpStoneUpgrade == 0)
                        {
                            m.SpInstance.SpStoneUpgrade = (byte)r;
                            Session.SendPacket(m.GenerateScPacket());
                        }
                        else
                        {

                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// req_info packet
        /// </summary>
        /// <param name="reqInfoPacket"></param>
        public void ReqInfo(ReqInfoPacket reqInfoPacket)
        {
            if (reqInfoPacket.Type == 6)
            {
                if (reqInfoPacket.MateVNum.HasValue)
                {
                    Mate mate = Session.CurrentMapInstance.Sessions
                        .FirstOrDefault(s =>
                            s.Character?.Mates != null
                            && s.Character.Mates.Any(o => o.MateTransportId == reqInfoPacket.MateVNum.Value))?.Character
                        .Mates.Find(o => o.MateTransportId == reqInfoPacket.MateVNum.Value);
                    Session.SendPacket(mate?.GenerateEInfo());
                }
            }
            else if (reqInfoPacket.Type == 5)
            {
                NpcMonster npc = ServerManager.GetNpc((short)reqInfoPacket.TargetVNum);
                if (npc != null)
                {
                    Session.SendPacket(npc.GenerateEInfo());
                }
            }
            else if (reqInfoPacket.Type == 12)
            {
                ItemInstance eq = Session.Character.Inventory.LoadBySlotAndType((short)reqInfoPacket.TargetVNum, InventoryType.Equipment);
                if (eq.BoundCharacterId == Session.Character.CharacterId)
                {
                    Session.SendPacket(Session.Character.GeneratePerfume(eq.ItemVNum, 0));
                    return;
                }
                Session.SendPacket(Session.Character.GeneratePerfume(eq.ItemVNum, 2));
            }
            else
            {
                Session.SendPacket(ServerManager.Instance.GetSessionByCharacterId(reqInfoPacket.TargetVNum)?.Character
                    ?.GenerateReqInfo());
            }
        }

        /// <summary>
        /// rest packet
        /// </summary>
        /// <param name="sitpacket"></param>
        public void Rest(SitPacket sitpacket)
        {
            if (Session.Character.MeditationDictionary.Count != 0)
            {
                Session.Character.MeditationDictionary.Clear();
            }

            sitpacket.Users?.ForEach(u =>
            {
                if (u.UserType == 1)
                {
                    Session.Character.Rest();
                }
                else
                {
                    Session.CurrentMapInstance.Broadcast(Session.Character.Mates
                        .Find(s => s.MateTransportId == (int)u.UserId)?.GenerateRest());
                }
            });
        }

        /// <summary>
        /// revival packet
        /// </summary>
        /// <param name="revivalPacket"></param>
        public void Revive(RevivalPacket revivalPacket)
        {
            if (Session.Character.Hp > 0)
            {
                return;
            }

            switch (revivalPacket.Type)
            {
                case 0:
                    switch (Session.CurrentMapInstance.MapInstanceType)
                    {
                        case MapInstanceType.LodInstance:
                            const int saver = 1211;
                            if (Session.Character.Inventory.CountItem(saver) < 1)
                            {
                                Session.SendPacket(
                                    UserInterfaceHelper.GenerateMsg(
                                        Language.Instance.GetMessageFromKey("NOT_ENOUGH_SAVER"), 0));
                                ServerManager.Instance.ReviveFirstPosition(Session.Character.CharacterId);
                            }
                            else
                            {
                                Session.Character.Inventory.RemoveItemAmount(saver);
                                Session.Character.Hp = (int)Session.Character.HPLoad();
                                Session.Character.Mp = (int)Session.Character.MPLoad();
                                Session.CurrentMapInstance?.Broadcast(Session, Session.Character.GenerateRevive());
                                Session.SendPacket(Session.Character.GenerateStat());
                            }

                            break;

                        case MapInstanceType.MappaExpInstanceSolo:
                            Session.Character.Hp = (int)Session.Character.HPLoad();
                            Session.Character.Mp = (int)Session.Character.MPLoad();
                            Session.CurrentMapInstance?.Broadcast(Session, Session.Character.GenerateRevive());
                            Session.SendPacket(Session.Character.GenerateStat());
                            break;

                        case MapInstanceType.MappaExpInstance:
                            Session.Character.Hp = (int)Session.Character.HPLoad();
                            Session.Character.Mp = (int)Session.Character.MPLoad();
                            Session.CurrentMapInstance?.Broadcast(Session, Session.Character.GenerateRevive());
                            Session.SendPacket(Session.Character.GenerateStat());
                            break;

                        case MapInstanceType.Act4Berios:
                        case MapInstanceType.Act4Calvina:
                        case MapInstanceType.Act4Hatus:
                        case MapInstanceType.Act4Morcos:
                            {
                                //Session.Character.SetContributi(-ServerManager.Instance.Configuration.ContributiARinascitaRaid);
                                Session.Character.Hp = (int)Session.Character.HPLoad();
                                Session.Character.Mp = (int)Session.Character.MPLoad();
                                Session.CurrentMapInstance?.Broadcast(Session, Session.Character.GenerateRevive());
                                Session.SendPacket(Session.Character.GenerateStat());
                            }

                            break;

                        default:
                            Session.Character.Hp = (int)Session.Character.HPLoad();
                            Session.Character.Mp = (int)Session.Character.MPLoad();

                            Session.CurrentMapInstance?.Broadcast(Session, Session.Character.GenerateTp());
                            Session.CurrentMapInstance?.Broadcast(Session, Session.Character.GenerateRevive());
                            Session.SendPacket(Session.Character.GenerateStat());

                            break;
                    }

                    break;

                case 1:
                    ServerManager.Instance.ReviveFirstPosition(Session.Character.CharacterId);
                    break;

                case 2:
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance)
                    {
                        ConcurrentBag<ArenaTeamMember> team = ServerManager.Instance.ArenaTeams.FirstOrDefault(s => s.Any(o => o.Session == Session));
                        ArenaTeamMember member = team?.FirstOrDefault(s => s.Session == Session);
                        if (member != null)
                        {
                            if (member.LastSummoned == null)
                            {
                                Session.CurrentMapInstance.InstanceBag.DeadList.Add(Session.Character.CharacterId);
                                member.Dead = true;
                                team.ToList().Where(s => s.LastSummoned != null).ToList().ForEach(s =>
                                {
                                    s.LastSummoned = null;
                                    s.Session.Character.PositionX = s.ArenaTeamType == ArenaTeamType.ERENIA ? (short)120 : (short)19;
                                    s.Session.Character.PositionY = s.ArenaTeamType == ArenaTeamType.ERENIA ? (short)39 : (short)40;
                                    Session.CurrentMapInstance.Broadcast(s.Session.Character.GenerateTp());
                                    s.Session.SendPacket(Session.Character.GenerateTaSt(TalentArenaOptionType.Watch));
                                });
                                ArenaTeamMember killer = team.OrderBy(s => s.Order).FirstOrDefault(s => !s.Dead && s.ArenaTeamType != member.ArenaTeamType);
                                Session.CurrentMapInstance.Broadcast(Session.Character.GenerateSay(string.Format("TEAM_WINNER_ARENA_ROUND", killer?.Session.Character.Name, killer?.ArenaTeamType), 10));
                                Session.CurrentMapInstance.Broadcast(
                                    UserInterfaceHelper.GenerateMsg(string.Format("TEAM_WINNER_ARENA_ROUND", killer?.Session.Character.Name, killer?.ArenaTeamType), 0));
                                Session.CurrentMapInstance.Sessions.Except(team.Where(s => s.ArenaTeamType == killer?.ArenaTeamType).Select(s => s.Session)).ToList().ForEach(o =>
                                {
                                    if (killer?.ArenaTeamType == ArenaTeamType.ERENIA)
                                    {
                                        o.SendPacket(killer.Session.Character.GenerateTaM(2));
                                        o.SendPacket(killer.Session.Character.GenerateTaP(2, true));
                                    }
                                    else
                                    {
                                        o.SendPacket(member.Session.Character.GenerateTaM(2));
                                        o.SendPacket(member.Session.Character.GenerateTaP(2, true));
                                    }
                                    o.SendPacket($"taw_d {member.Session.Character.CharacterId}");
                                    o.SendPacket(member.Session.Character.GenerateSay(
                                        string.Format("WINNER_ARENA_ROUND", killer?.Session.Character.Name, killer?.ArenaTeamType, member.Session.Character.Name), 10));
                                    o.SendPacket(UserInterfaceHelper.GenerateMsg(
                                        string.Format("WINNER_ARENA_ROUND", killer?.Session.Character.Name, killer?.ArenaTeamType, member.Session.Character.Name), 0));
                                });
                            }

                            member.Session.Character.PositionX = member.ArenaTeamType == ArenaTeamType.ERENIA ? (short)120 : (short)19;
                            member.Session.Character.PositionY = member.ArenaTeamType == ArenaTeamType.ERENIA ? (short)39 : (short)40;
                            Session.CurrentMapInstance.Broadcast(member.Session, member.Session.Character.GenerateTp());
                            Session.SendPacket(Session.Character.GenerateTaSt(TalentArenaOptionType.Watch));
                            team.Where(friends => friends.ArenaTeamType == member.ArenaTeamType).ToList().ForEach(friends => { friends.Session.SendPacket(friends.Session.Character.GenerateTaFc(0)); });
                            team.ToList().ForEach(arenauser =>
                            {
                                arenauser.Session.SendPacket(arenauser.Session.Character.GenerateTaP(2, true));
                                arenauser.Session.SendPacket(arenauser.Session.Character.GenerateTaM(2));
                            });

                            Session.Character.Hp = (int)Session.Character.HPLoad();
                            Session.Character.Mp = (int)Session.Character.MPLoad();
                            Session.CurrentMapInstance?.Broadcast(Session, Session.Character.GenerateRevive());
                            Session.SendPacket(Session.Character.GenerateStat());
                        }
                    }
                    if (Session.Character.Gold >= 100)
                    {
                        Session.Character.Hp = (int)Session.Character.HPLoad();
                        Session.Character.Mp = (int)Session.Character.MPLoad();
                        Session.CurrentMapInstance?.Broadcast(Session, Session.Character.GenerateTp());
                        Session.CurrentMapInstance?.Broadcast(Session, Session.Character.GenerateRevive());
                        Session.SendPacket(Session.Character.GenerateStat());
                        Session.Character.Gold -= 100;
                        Session.SendPacket(Session.Character.GenerateGold());
                        Session.Character.LastPVPRevive = DateTime.Now;
                        Observable.Timer(TimeSpan.FromSeconds(5)).Subscribe(observer =>
                            Session.SendPacket(
                                Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("PVP_ACTIVE"), 10)));
                    }
                    else
                    {
                        ServerManager.Instance.ReviveFirstPosition(Session.Character.CharacterId);
                    }

                    break;
            }
        }

        /// <summary>
        /// say packet
        /// </summary>
        /// <param name="sayPacket"></param>
        public void Say(SayPacket sayPacket)
        {
            if (string.IsNullOrWhiteSpace(sayPacket.Message))
            {
                return;
            }

            Session.Character.MessageCounter += 2;
            if (Session.Character.MessageCounter > 11)
            {
                return;
            }

            bool isMuted = Session.Character.MuteMessage();
            string message = sayPacket.Message;
            if (ServerManager.Instance.Configuration.UseChatLogService)
            {
                ChatLogServiceClient.Instance.LogChatMessage(new ChatLogEntry()
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    Receiver = Session.CurrentMapInstance?.Map.Name,
                    ReceiverId = Session.CurrentMapInstance?.Map.MapId,
                    MessageType = ChatLogType.Map,
                    Message = message
                });
            }

            if (!isMuted)
            {
                byte type = 0;
                if (Session.Character.Authority == AuthorityType.Moderator)
                {
                    type = 12;
                    Session.CurrentMapInstance?.Broadcast(Session, Session.Character.GenerateSay(message.Trim(), 1),
                        ReceiverType.AllExceptMe);
                    message = $"[Support {Session.Character.Name}]: {message}";
                }

                Session.CurrentMapInstance?.Broadcast(Session, Session.Character.GenerateSay(message.Trim(), type),
                    ReceiverType.AllExceptMe);
            }
        }

        /// <summary>
        /// pst packet
        /// </summary>
        /// <param name="pstPacket"></param>
        public void SendMail(PstPacket pstPacket)
        {
            if (pstPacket?.Data != null)
            {
                CharacterDTO receiver = DAOFactory.CharacterDAO.LoadByName(pstPacket.Receiver);
                if (receiver != null)
                {
                    string[] datasplit = pstPacket.Data.Split(' ');
                    if (datasplit.Length < 2)
                    {
                        return;
                    }

                    if (datasplit[1].Length > 250)
                    {
                        //PenaltyLogDTO log = new PenaltyLogDTO
                        //{
                        //    AccountId = Session.Character.AccountId,
                        //    Reason = "You are an idiot!",
                        //    Penalty = PenaltyType.Banned,
                        //    DateStart = DateTime.Now,
                        //    DateEnd = DateTime.Now.AddYears(69),
                        //    AdminName = "Your mom's ass"
                        //};
                        //Session.Character.InsertOrUpdatePenalty(log);
                        //ServerManager.Instance.Kick(Session.Character.Name);
                        return;
                    }

                    ItemInstance headWearable =
                        Session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Hat, InventoryType.Wear);
                    byte color = headWearable?.Item.IsColored == true
                        ? (byte)headWearable.Design
                        : (byte)Session.Character.HairColor;
                    MailDTO mailcopy = new MailDTO
                    {
                        AttachmentAmount = 0,
                        IsOpened = false,
                        Date = DateTime.Now,
                        Title = datasplit[0],
                        Message = datasplit[1],
                        ReceiverId = receiver.CharacterId,
                        SenderId = Session.Character.CharacterId,
                        IsSenderCopy = true,
                        SenderClass = Session.Character.Class,
                        SenderGender = Session.Character.Gender,
                        SenderHairColor = Enum.IsDefined(typeof(HairColorType), color) ? (HairColorType)color : 0,
                        SenderHairStyle = Session.Character.HairStyle,
                        EqPacket = Session.Character.GenerateEqListForPacket(),
                        SenderMorphId = Session.Character.Morph == 0
                            ? (short)-1
                            : (short)(Session.Character.Morph > short.MaxValue ? 0 : Session.Character.Morph)
                    };
                    MailDTO mail = new MailDTO
                    {
                        AttachmentAmount = 0,
                        IsOpened = false,
                        Date = DateTime.Now,
                        Title = datasplit[0],
                        Message = datasplit[1],
                        ReceiverId = receiver.CharacterId,
                        SenderId = Session.Character.CharacterId,
                        IsSenderCopy = false,
                        SenderClass = Session.Character.Class,
                        SenderGender = Session.Character.Gender,
                        SenderHairColor = Enum.IsDefined(typeof(HairColorType), color) ? (HairColorType)color : 0,
                        SenderHairStyle = Session.Character.HairStyle,
                        EqPacket = Session.Character.GenerateEqListForPacket(),
                        SenderMorphId = Session.Character.Morph == 0
                            ? (short)-1
                            : (short)(Session.Character.Morph > short.MaxValue ? 0 : Session.Character.Morph)
                    };

                    MailServiceClient.Instance.SendMail(mailcopy);
                    MailServiceClient.Instance.SendMail(mail);

                    //Session.Character.MailList.Add((Session.Character.MailList.Count > 0 ? Session.Character.MailList.OrderBy(s => s.Key).Last().Key : 0) + 1, mailcopy);
                    Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("MAILED"),
                        11));
                    //Session.SendPacket(Session.Character.GeneratePost(mailcopy, 2));
                }
                else
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("USER_NOT_FOUND"), 10));
                }
            }
            else if (pstPacket != null && int.TryParse(pstPacket.Id.ToString(), out int id)
                     && byte.TryParse(pstPacket.Type.ToString(), out byte type))
            {
                if (pstPacket.Argument == 3)
                {
                    if (Session.Character.MailList.ContainsKey(id))
                    {
                        if (!Session.Character.MailList[id].IsOpened)
                        {
                            Session.Character.MailList[id].IsOpened = true;
                            MailDTO mailupdate = Session.Character.MailList[id];
                            DAOFactory.MailDAO.InsertOrUpdate(ref mailupdate);
                        }

                        Session.SendPacket(Session.Character.GeneratePostMessage(Session.Character.MailList[id], type));
                    }
                }
                else if (pstPacket.Argument == 2)
                {
                    if (Session.Character.MailList.ContainsKey(id))
                    {
                        MailDTO mail = Session.Character.MailList[id];
                        Session.SendPacket(
                            Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("MAIL_DELETED"), 11));
                        Session.SendPacket($"post 2 {type} {id}");
                        if (DAOFactory.MailDAO.LoadById(mail.MailId) != null)
                        {
                            DAOFactory.MailDAO.DeleteById(mail.MailId);
                        }

                        if (Session.Character.MailList.ContainsKey(id))
                        {
                            Session.Character.MailList.Remove(id);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// qset packet
        /// </summary>
        /// <param name="qSetPacket"></param>
        public void SetQuicklist(QSetPacket qSetPacket)
        {
            short data1 = 0, data2 = 0, type = qSetPacket.Type, q1 = qSetPacket.Q1, q2 = qSetPacket.Q2;
            if (qSetPacket.Data1.HasValue)
            {
                data1 = qSetPacket.Data1.Value;
            }

            if (qSetPacket.Data2.HasValue)
            {
                data2 = qSetPacket.Data2.Value;
            }

            switch (type)
            {
                case 0:
                case 1:

                    // client says qset 0 1 3 2 6 answer -> qset 1 3 0.2.6.0
                    Session.Character.QuicklistEntries.RemoveAll(n =>
                        n.Q1 == q1 && n.Q2 == q2
                        && (Session.Character.UseSp ? n.Morph == Session.Character.Morph : n.Morph == 0));
                    Session.Character.QuicklistEntries.Add(new QuicklistEntryDTO
                    {
                        CharacterId = Session.Character.CharacterId,
                        Type = type,
                        Q1 = q1,
                        Q2 = q2,
                        Slot = data1,
                        Pos = data2,
                        Morph = Session.Character.UseSp ? (short)Session.Character.Morph : (short)0
                    });
                    Session.SendPacket($"qset {q1} {q2} {type}.{data1}.{data2}.0");
                    break;

                case 2:
                    try
                    {
                        // DragDrop / Reorder qset type to1 to2 from1 from2 vars -> q1 q2 data1 data2
                        QuicklistEntryDTO qlFrom = Session.Character.QuicklistEntries.SingleOrDefault(n =>
                            n.Q1 == data1 && n.Q2 == data2
                            && (Session.Character.UseSp ? n.Morph == Session.Character.Morph : n.Morph == 0));
                        if (qlFrom != null)
                        {
                            QuicklistEntryDTO qlTo = Session.Character.QuicklistEntries.SingleOrDefault(n =>
                                n.Q1 == q1 && n.Q2 == q2 && (Session.Character.UseSp
                                    ? n.Morph == Session.Character.Morph
                                    : n.Morph == 0));
                            qlFrom.Q1 = q1;
                            qlFrom.Q2 = q2;
                            if (qlTo == null)
                            {
                                // Put 'from' to new position (datax)
                                Session.SendPacket(
                                    $"qset {qlFrom.Q1} {qlFrom.Q2} {qlFrom.Type}.{qlFrom.Slot}.{qlFrom.Pos}.0");

                                // old 'from' is now empty.
                                Session.SendPacket($"qset {data1} {data2} 7.7.-1.0");
                            }
                            else
                            {
                                // Put 'from' to new position (datax)
                                Session.SendPacket(
                                    $"qset {qlFrom.Q1} {qlFrom.Q2} {qlFrom.Type}.{qlFrom.Slot}.{qlFrom.Pos}.0");

                                // 'from' is now 'to' because they exchanged
                                qlTo.Q1 = data1;
                                qlTo.Q2 = data2;
                                Session.SendPacket($"qset {qlTo.Q1} {qlTo.Q2} {qlTo.Type}.{qlTo.Slot}.{qlTo.Pos}.0");
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                    break;

                case 3:

                    // Remove from Quicklist
                    Session.Character.QuicklistEntries.RemoveAll(n =>
                        n.Q1 == q1 && n.Q2 == q2
                        && (Session.Character.UseSp ? n.Morph == Session.Character.Morph : n.Morph == 0));
                    Session.SendPacket($"qset {q1} {q2} 7.7.-1.0");
                    break;

                default:
                    return;
            }
        }

        /// <summary>
        /// game_start packet
        /// </summary>
        /// <param name="gameStartPacket"></param>
        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        public void StartGame(GameStartPacket gameStartPacket)
        {
            if (Session.IsOnMap || !Session.HasSelectedCharacter)
            {
                // character should have been selected in SelectCharacter
                return;
            }

            if (Session.Character.MapInstance.Map.MapTypes.Any(m => m.MapTypeId == (short)MapTypeEnum.Act4)
                && ServerManager.Instance.ChannelId != 51)
            {
                // Change IP to yours
                Session.Character.ChangeChannel(ServerManager.Instance.Configuration.Act4IP,
                    ServerManager.Instance.Configuration.Act4Port, 2);

            }

            Session.CurrentMapInstance = Session.Character.MapInstance;
            if (ServerManager.Instance.Configuration.SceneOnCreate
                && Session.Character.GeneralLogs.CountLinq(s => s.LogType == "Connection") < 2)
            {
                Session.SendPacket("scene 40");
            }

            if (ServerManager.Instance.Configuration.WorldInformation)
            {
                Assembly assembly = Assembly.GetEntryAssembly();
                string productVersion = assembly?.Location != null
                    ? FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion
                    : "1337";
                Session.SendPacket(Session.Character.GenerateSay($"Welcome, {Session.Character.Name}!", 12));
            }

            if (Session.Character.MapInstance.Map.MapTypes.Any(m => m.MapTypeId == (short)MapTypeEnum.Act4))
            {
                short x = (short)(39 + ServerManager.RandomNumber(-2, 3));
                short y = (short)(42 + ServerManager.RandomNumber(-2, 3));

                if (Session.Character.Faction == FactionType.Angel)
                {
                    ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 130, x, y);
                }
                else if (Session.Character.Faction == FactionType.Demon)
                {
                    ServerManager.Instance.ChangeMap(Session.Character.CharacterId, 131, x, y);
                }
            }

            Session.Character.LoadSpeed();
            Session.Character.LoadSkills();
            Session.SendPacket(Session.Character.GenerateTit());
            Session.SendPacket(Session.Character.GenerateSpPoint());
            Session.Character.Size = 10;
            Session.SendPacket("rsfi 1 1 0 9 0 9");
            Session.Character.Quests?.Where(q => q?.Quest?.TargetMap != null).ToList().ForEach(qst => Session.SendPacket(qst.Quest.TargetPacket()));
            if (Session.Character.Hp <= 0)
            {
                ServerManager.Instance.ReviveFirstPosition(Session.Character.CharacterId);
            }
            else
            {
                ServerManager.Instance.ChangeMap(Session.Character.CharacterId);
            }

            Session.SendPacket(Session.Character.GenerateSki());
            Session.SendPacket(
                $"fd {Session.Character.Reputation} 0 {(int)Session.Character.Dignity} {Math.Abs(Session.Character.GetDignityIco())}");
            Session.SendPacket(Session.Character.GenerateFd());
            Session.SendPacket("rage 0 250000");
            Session.SendPacket("rank_cool 0 0 18000");
            ItemInstance specialistInstance = Session.Character.Inventory.LoadBySlotAndType(8, InventoryType.Wear);
            StaticBonusDTO medal = Session.Character.StaticBonusList.Find(s =>
                s.StaticBonusType == StaticBonusType.BazaarMedalGold
                || s.StaticBonusType == StaticBonusType.BazaarMedalSilver);
            if (medal != null)
            {
                Session.SendPacket(
                    Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("LOGIN_MEDAL"), 12));
            }

            if (Session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.PetBasket))
            {
                Session.SendPacket("ib 1278 1");
            }

            if (Session.Character.MapInstance.Map.MapTypes.Any(m => m.MapTypeId == (short)MapTypeEnum.CleftOfDarkness))
            {
                Session.SendPacket("bc 0 0 0");
            }

            if (specialistInstance != null)
            {
                Session.SendPacket(Session.Character.GenerateSpPoint());
            }

            Session.SendPacket("scr " + Session.Character.Act4Kill + " " + Session.Character.Act4Dead + " " + Session.Character.Act4Points + " " + Session.Character.Compliment + "  0 0");

            for (int i = 0; i < 10; i++)
            {
                Session.SendPacket($"bn {i} {Language.Instance.GetMessageFromKey($"BN{i}")}");
            }

            Session.SendPacket(Session.Character.GenerateExts());
            Session.SendPacket(Session.Character.GenerateMlinfo());
            Session.SendPacket(UserInterfaceHelper.GeneratePClear());

            Session.SendPacket("zzim");
            Session.SendPacket(
                $"twk 1 {Session.Character.CharacterId} {Session.Account.Name} {Session.Character.Name} shtmxpdlfeoqkr");

            long? familyId = DAOFactory.FamilyCharacterDAO.LoadByCharacterId(Session.Character.CharacterId)?.FamilyId;
            if (familyId.HasValue)
            {
                Session.Character.Family = ServerManager.Instance.FamilyList[familyId.Value];
            }

            if (Session.Character.Family != null && Session.Character.FamilyCharacter != null)
            {
                Session.SendPacket(Session.Character.GenerateGInfo());
                Session.SendPackets(Session.Character.GetFamilyHistory());
                Session.SendPacket(Session.Character.GenerateFamilyMember());
                Session.SendPacket(Session.Character.GenerateFamilyMemberMessage());
                Session.SendPacket(Session.Character.GenerateFamilyMemberExp());
                //Session.SendPacket(Session.Character.GenerateFamilyFmi());
                try
                {
                    Session.Character.Faction = Session.Character.Family.FamilyCharacters
                        .Find(s => s.Authority.Equals(FamilyAuthority.Head)).Character.Faction;
                }
                catch (Exception)
                {
                    // do nothing
                }

                if (!string.IsNullOrWhiteSpace(Session.Character.Family.FamilyMessage))
                {
                    Session.SendPacket(
                        UserInterfaceHelper.GenerateInfo("--- Family Message ---\n" +
                                                         Session.Character.Family.FamilyMessage));
                }
            }

            // qstlist target sqst bf
            Session.SendPacket("act6");
            Session.SendPacket(Session.Character.GenerateFaction());
#pragma warning disable 618
            Session.Character.GenerateStartupInventory();
#pragma warning restore 618

            Session.SendPacket(Session.Character.GenerateGold());
            Session.SendPackets(Session.Character.GenerateQuicklist());


            Session.SendPacket(Session.Character.GeneratePinit());
            Session.SendPackets(Session.Character.GeneratePst());

            string clinit = "clinit";
            string flinit = "flinit";
            string kdlinit = "kdlinit";
            foreach (CharacterDTO character in ServerManager.Instance.TopComplimented)
            {
                clinit +=
                    $" {character.CharacterId}|{character.SwitchLevel()}|{character.SwitchHeroLevel()}|{character.Compliment}|{character.Name}";
            }

            foreach (CharacterDTO character in ServerManager.Instance.TopReputation)
            {
                flinit +=
                    $" {character.CharacterId}|{character.SwitchLevel()}|{character.SwitchHeroLevel()}|{character.Reputation}|{character.Name}";
            }

            foreach (CharacterDTO character in ServerManager.Instance.TopPoints)
            {
                kdlinit +=
                    $" {character.CharacterId}|{character.SwitchLevel()}|{character.SwitchHeroLevel()}|{character.Act4Points}|{character.Name}";
            }

            Session.CurrentMapInstance?.Broadcast(Session.Character.GenerateGidx());

            Session.CurrentMapInstance?.Broadcast(Session.Character.GenerateScal());

            Session.SendPacket(Session.Character.GenerateFinit());
            Session.SendPacket(Session.Character.GenerateBlinit());
            Session.SendPacket(clinit);
            Session.SendPacket(flinit);
            Session.SendPacket(kdlinit);

            Session.Character.LastPVPRevive = DateTime.Now;

            IEnumerable<PenaltyLogDTO> warning = DAOFactory.PenaltyLogDAO.LoadByAccount(Session.Character.AccountId)
                .Where(p => p.Penalty == PenaltyType.Warning);
            if (warning.Any())
            {
                Session.SendPacket(UserInterfaceHelper.GenerateInfo(
                    string.Format(Language.Instance.GetMessageFromKey("WARNING_INFO"), warning.Count())));
            }

            var LastDaily = Session.Character.GeneralLogs.LastOrDefault(s => s.LogData == "World" && s.LogType == "Daily" && s.CharacterId == Session.Character.CharacterId && s.Timestamp.Day == DateTime.Now.Day);
            if (LastDaily == null)
            {
                Session.Character.GiftAdd(1, 1);
                Logger.Info($"[DAILY_REWARD] {Session.Character.Name} claimed his Daily Reward");
                var GeneralLogAdd = new GeneralLogDTO
                {
                    LogData = "World",
                    LogType = "Daily",
                    CharacterId = Session.Character.CharacterId,
                    AccountId = Session.Account.AccountId,
                    Timestamp = DateTime.Now
                };
                DAOFactory.GeneralLogDAO.Insert(GeneralLogAdd);
            }
            
            /*
             * FirstLogin Bonus
             * Swordsman = 
             * Archer = 
             * Magician = 
             * Fighter = 
             */

            if (Session.Character.FirstLogin == true && Session.Character.Class == ClassType.Swordsman)
            {
                Session.SendPacket($"Welcome to NosTale Nexus, {Session.Character.Name}!")
                Session.SendPacket(Session.Character.GenerateSay("Your Starterpack has been claimed!", 12));
                //Session.Character.GiftAdd(Add Vnum);
            }
            else if (Session.Character.FirstLogin == true && Session.Character.Class == ClassType.Archer)
            {
                Session.SendPacket($"Welcome to NosTale Nexus, {Session.Character.Name}!")
                Session.SendPacket(Session.Character.GenerateSay("Your Starterpack has been claimed!", 12));
                //Session.Character.GiftAdd(Add Vnum);
            }
            else if (Session.Character.FirstLogin == true && Session.Character.Class == ClassType.Magician)
            {
                Session.SendPacket($"Welcome to NosTale Nexus, {Session.Character.Name}!")
                Session.SendPacket(Session.Character.GenerateSay("Your Starterpack has been claimed!", 12));
                //Session.Character.GiftAdd(Add Vnum);
            }
            else if (Session.Character.FirstLogin == true && Session.Character.Class == ClassType.Fighter)
            {
                Session.SendPacket($"Welcome to NosTale Nexus, {Session.Character.Name}!")
                Session.SendPacket(Session.Character.GenerateSay("Your Starterpack has been claimed!", 12));
                //Session.Character.GiftAdd(Add Vnum);
            }

            // finfo - friends info
            Session.Character.LoadMail();
            Session.Character.LoadSentMail();
            Session.Character.DeleteTimeout();
            Session.SendPackets(Session.Character.GenerateScP());
            Session.SendPackets(Session.Character.GenerateScN());

            foreach (StaticBuffDTO staticBuff in DAOFactory.StaticBuffDAO.LoadByCharacterId(Session.Character
                .CharacterId))
            {
                Session.Character.AddStaticBuff(staticBuff);
            }

            if (Session.Character.Quests.Any())
            {
                Session.SendPacket(Session.Character.GenerateQuestsPacket());
            }

            if (Session.Character.Authority == AuthorityType.Owner)
            {
                CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                {
                    DestinationCharacterId = null,
                    SourceCharacterId = Session.Character.CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message =
                        $"[Owner] {Session.Character.Name} join to server!",
                    Type = MessageType.Shout
                });
            }

            if (Session.Character.Authority == AuthorityType.TrialGameMaster)
            {
                CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                {
                    DestinationCharacterId = null,
                    SourceCharacterId = Session.Character.CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message =
                        $"[TrialGM] {Session.Character.Name} join to server!",
                    Type = MessageType.Shout
                });
            }

            if (Session.Character.Authority == AuthorityType.TrialModerator)
            {
                CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                {
                    DestinationCharacterId = null,
                    SourceCharacterId = Session.Character.CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message =
                        $"[TrialSupporter] {Session.Character.Name} join to server!",
                    Type = MessageType.Shout
                });
            }

            if (Session.Character.Authority == AuthorityType.GameMaster)
            {
                CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                {
                    DestinationCharacterId = null,
                    SourceCharacterId = Session.Character.CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message =
                        $"[GM] {Session.Character.Name} join to server!",
                    Type = MessageType.Shout
                });
            }

            if (Session.Character.Authority == AuthorityType.Moderator)
            {
                CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                {
                    DestinationCharacterId = null,
                    SourceCharacterId = Session.Character.CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message =
                        $"[Supporter] {Session.Character.Name} join to server!",
                    Type = MessageType.Shout
                });
            }

            if (Session.Character.Authority == AuthorityType.BitchNiggerFaggot)
            {
                CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                {
                    DestinationCharacterId = null,
                    SourceCharacterId = Session.Character.CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message =
                        $"User {Session.Character.Name} with rank BitchNiggerFaggot has logged in, don't trust *it*!",
                    Type = MessageType.Shout
                });
            }

            //QuestModel quest = ServerManager.Instance.QuestList.Where(s => s.QuestGiver.Type == QuestGiverType.InitialQuest).FirstOrDefault();
            //if(quest != null)
            //{
            //    quest = quest.Copy();

            //    int current = 0;
            //    int max = 0;

            //    if (quest.KillObjectives != null)
            //    {
            //        max = quest.KillObjectives[0].GoalAmount;
            //        current = quest.KillObjectives[0].CurrentAmount;
            //    }

            //    if(quest.WalkObjective != null)
            //    {
            //        Session.SendPacket($"target {quest.WalkObjective.MapX} {quest.WalkObjective.MapY} {quest.WalkObjective.MapId} {quest.QuestDataVNum}");
            //    }

            //    //Quest Packet Definition: qstlist {Unknown}.{QuestVNUM}.{QuestVNUM}.{GoalType}.{Current}.{Goal}.{Finished}.{GoalType}.{Current}.{Goal}.{Finished}.{GoalType}.{Current}.{Goal}.{Finished}.{ShowDialog}
            //    //Same for qsti
            //    Session.SendPacket($"qstlist 5.{quest.QuestDataVNum}.{quest.QuestDataVNum}.{quest.QuestGoalType}.{current}.{max}.0.0.0.0.0.0.0.0.0.1");

            //}

            if (Session.Character.Family != null)
            {
                if (Session.Character.Family.FamilyLevel >= 5)
                {
                    if (!Session.Character.Buff.ContainsKey(166))
                    {
                        Session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 166 });
                        Session.SendPacket(UserInterfaceHelper.GenerateInfo("FAMILY_5_LVL"));
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
                if (Session.Character.Family.FamilyLevel >= 7)
                {
                    if (!Session.Character.Buff.ContainsKey(249))
                    {
                        Session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 249 });
                        Session.SendPacket(UserInterfaceHelper.GenerateInfo("FAMILY_7_LVL"));
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
                if (Session.Character.Family.FamilyLevel >= 10)
                {
                    if (!Session.Character.Buff.ContainsKey(248))
                    {
                        Session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 248 });
                        Session.SendPacket(UserInterfaceHelper.GenerateInfo("FAMILY_10_LVL"));
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }

            }

            if (Session.Character.ArenaWinner != 0)
            {
                if (!Session.Character.Buff.ContainsKey(492))
                {
                    Session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 492 });
                    Session.SendPacket(UserInterfaceHelper.GenerateInfo("BUFF_ALREADY"));
                }
                else
                {
                    return;
                }
                if (!Session.Character.Buff.ContainsKey(493))
                {
                    Session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 493 });
                    Session.SendPacket(UserInterfaceHelper.GenerateInfo("BUFF_ALREADY"));
                }
                else
                {
                    return;
                }
                if (!Session.Character.Buff.ContainsKey(494))
                {
                    Session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 494 });
                    Session.SendPacket(UserInterfaceHelper.GenerateInfo("BUFF_ALREADY"));
                }
                else
                {
                    return;
                }
                if (!Session.Character.Buff.ContainsKey(495))
                {
                    Session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 495 });
                    Session.SendPacket(UserInterfaceHelper.GenerateInfo("BUFF_ALREADY"));
                }
                else
                {
                    return;
                }
                if (!Session.Character.Buff.ContainsKey(3005))
                {
                    Session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 3005 });
                    Session.SendPacket(UserInterfaceHelper.GenerateInfo("BUFF_ALREADY"));
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }

        /// <summary>
        /// walk packet
        /// </summary>
        /// <param name="walkPacket"></param>
        public void Walk(WalkPacket walkPacket)
        {
            if (!Session.Character.NoMove)
            {
                if (Session.Character.MeditationDictionary.Count != 0)
                {
                    Session.Character.MeditationDictionary.Clear();
                }

                double currentRunningSeconds =
                    (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(-50)).TotalSeconds;
                double timeSpanSinceLastPortal = currentRunningSeconds - Session.Character.LastPortal;
                int distance =
                    Map.GetDistance(new MapCell { X = Session.Character.PositionX, Y = Session.Character.PositionY },
                        new MapCell { X = walkPacket.XCoordinate, Y = walkPacket.YCoordinate });

                if (Session.HasCurrentMapInstance
                    && !Session.CurrentMapInstance.Map.IsBlockedZone(walkPacket.XCoordinate, walkPacket.YCoordinate)
                    && !Session.Character.IsChangingMapInstance && !Session.Character.HasShopOpened)
                {
                    if ((Session.Character.Speed >= walkPacket.Speed
                         || Session.Character.LastSpeedChange.AddSeconds(5) > DateTime.Now)
                        && !(distance > 60 && timeSpanSinceLastPortal > 10))
                    {
                        if (Session.Character.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                        {
                            Session.Character.MapX = walkPacket.XCoordinate;
                            Session.Character.MapY = walkPacket.YCoordinate;

                        }

                        Session.Character.PositionX = walkPacket.XCoordinate;
                        Session.Character.PositionY = walkPacket.YCoordinate;

                        if (Session.Character.LastMonsterAggro.AddSeconds(5) > DateTime.Now)
                        {
                            Session.Character.UpdateBushFire();
                        }

                        if (!Session.Character.InvisibleGm)
                        {
                            Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.Move(UserType.Player,
                                Session.Character.CharacterId, Session.Character.PositionX, Session.Character.PositionY,
                                Session.Character.Speed));
                        }

                        Session.SendPacket(Session.Character.GenerateCond());
                        Session.Character.LastMove = DateTime.Now;

                        Session.CurrentMapInstance?.OnAreaEntryEvents
                            ?.Where(s => s.InZone(Session.Character.PositionX, Session.Character.PositionY)).ToList()
                            .ForEach(e => e.Events.ForEach(evt => EventHelper.Instance.RunEvent(evt)));
                        Session.CurrentMapInstance?.OnAreaEntryEvents?.RemoveAll(s =>
                            s.InZone(Session.Character.PositionX, Session.Character.PositionY));

                        Session.CurrentMapInstance?.OnMoveOnMapEvents?.ForEach(e => EventHelper.Instance.RunEvent(e));
                        Session.CurrentMapInstance?.OnMoveOnMapEvents?.RemoveAll(s => s != null);
                    }
                    else
                    {
                        Session.SendPacket(UserInterfaceHelper.GenerateInfo("Something is wrong! Try re-login!"));
                    }
                }
            }
            Session.Character.Size = 10;
        }

        /// <summary>
        /// / packet
        /// </summary>
        /// <param name="whisperPacket"></param>
        public void Whisper(WhisperPacket whisperPacket)
        {
            try
            {
                // TODO: Implement WhisperSupport
                if (string.IsNullOrEmpty(whisperPacket.Message))
                {
                    return;
                }

                string characterName =
                    whisperPacket.Message.Split(' ')[
                            whisperPacket.Message.StartsWith("GM ", StringComparison.CurrentCulture) ? 1 : 0]
                        .Replace("[Support]", string.Empty).Replace("[Vip]", string.Empty).Replace("[EventMaster]", string.Empty).Replace("[BitchNiggerFaggot]", string.Empty);
                string message = string.Empty;
                string[] packetsplit = whisperPacket.Message.Split(' ');
                for (int i = packetsplit[0] == "GM" ? 2 : 1; i < packetsplit.Length; i++)
                {
                    message += packetsplit[i] + " ";
                }

                if (message.Length > 60)
                {
                    message = message.Substring(0, 60);
                }

                message = message.Trim();
                Session.SendPacket(Session.Character.GenerateSpk(message, 5));
                CharacterDTO receiver = DAOFactory.CharacterDAO.LoadByName(characterName);
                int? sentChannelId = null;
                if (receiver != null)
                {
                    if (receiver.CharacterId == Session.Character.CharacterId)
                    {
                        return;
                    }

                    if (Session.Character.IsBlockedByCharacter(receiver.CharacterId))
                    {
                        Session.SendPacket(
                            UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("BLACKLIST_BLOCKED")));
                        return;
                    }

                    ClientSession receiverSession =
                        ServerManager.Instance.GetSessionByCharacterId(receiver.CharacterId);
                    if (receiverSession?.CurrentMapInstance?.Map.MapId == Session.CurrentMapInstance?.Map.MapId
                        && Session.Account.Authority >= AuthorityType.Moderator)
                    {
                        receiverSession?.SendPacket(Session.Character.GenerateSay(message, 2));
                    }

                    if (ServerManager.Instance.Configuration.UseChatLogService)
                    {
                        ChatLogServiceClient.Instance.LogChatMessage(new ChatLogEntry()
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            Receiver = receiver.Name,
                            ReceiverId = receiver.CharacterId,
                            MessageType = ChatLogType.Whisper,
                            Message = message
                        });
                    }

                    sentChannelId = CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                    {
                        DestinationCharacterId = receiver.CharacterId,
                        SourceCharacterId = Session.Character.CharacterId,
                        SourceWorldId = ServerManager.Instance.WorldId,
                        Message = Session.Character.Authority == AuthorityType.Moderator
                            ? Session.Character.GenerateSay(
                                $"(whisper)(From Support {Session.Character.Name}):{message}", 11)
                            : Session.Character.GenerateSpk(message,
                                Session.Account.Authority == AuthorityType.GameMaster ? 15 : 5),
                        Type = packetsplit[0] == "GM" ? MessageType.WhisperGM : MessageType.Whisper
                    });
                }

                if (sentChannelId == null)
                {
                    Session.SendPacket(
                        UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("USER_NOT_CONNECTED")));
                }
            }
            catch (Exception e)
            {
                Logger.Error("Whisper failed.", e);
            }
        }


        #endregion
    }
}
