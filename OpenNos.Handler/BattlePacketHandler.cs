﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Battle;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.Master.Library.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using static OpenNos.Domain.BCardType;

namespace OpenNos.Handler
{
    public class BattlePacketHandler : IPacketHandler
    {
        #region Instantiation

        public BattlePacketHandler(ClientSession session) => Session = session;

        #endregion

        #region Properties

        private ClientSession Session { get; }

        #endregion

        #region Methods


        /// <summary>
        /// ranksk packet
        /// </summary>
        /// <param name="rankSkPacket"></param>
        public void RankSk(RankSkPacket rankSkPacket)
        {
            if (!Session.HasCurrentMapInstance)
            {
                return;
            }

            if (ServerManager.Instance.ChannelId != 51)
            {
                return;
            }
            

            bool isMuted = Session.Character.MuteMessage();
            if (isMuted || Session.Character.IsVehicled)
            {
                Session.SendPacket(StaticPacketHelper.Cancel());
                return;
            }

            if(Session.Character.LastAncelloan.AddSeconds(1800) > DateTime.Now)
            {
                return;
            }

            if (Session.Character.IsReputationHero() >= 3 || Session.Account.Authority >= AuthorityType.Moderator)
            {
                if (rankSkPacket.index == 0)
                {
                    if (Session.Character.Inventory.CountItem(5996) > 0)
                    {
                        Session.Character.AddBuff(new Buff(460, Session.Character.SwitchLevel()));
                        Session.Character.Size = 15;
                        //Session.CurrentMapInstance?.Broadcast(Session.Character.GenerateScal());
                        Session.Character.Inventory.RemoveItemAmount(5996);
                        Session.Character.LastAncelloan = DateTime.Now;
                    }
                    else
                    {
                        Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey(string.Format(Language.Instance.GetMessageFromKey("NOT_ENOUGH_ITEMS"), ServerManager.GetItem(5996).Name, 1)), 10));
                        return;
                    }
                }
            }
            else
            {
                return;
            }

        }

        /// <summary>
        /// mtlist packet
        /// </summary>
        /// <param name="mutliTargetListPacket"></param>
        public void MultiTargetListHit(MultiTargetListPacket mutliTargetListPacket)
        {
            if (!Session.HasCurrentMapInstance)
            {
                return;
            }

            bool isMuted = Session.Character.MuteMessage();
            if (isMuted || Session.Character.IsVehicled)
            {
                Session.SendPacket(StaticPacketHelper.Cancel());
                return;
            }

            if ((DateTime.Now - Session.Character.LastTransform).TotalSeconds < 3)
            {
                Session.SendPacket(StaticPacketHelper.Cancel());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CANT_ATTACK"),
                    0));
                return;
            }

            if (mutliTargetListPacket.TargetsAmount > 0 && mutliTargetListPacket.Targets == null)
            {
                Session.SendPacket($"say 1 0 10 Nope");
                Logger.Log.Debug($"WARNING: user {Session.Character.Name} tried an Crash: MultiTargetListHit");
                return;
            }

            if (mutliTargetListPacket.TargetsAmount > 0
                && mutliTargetListPacket.TargetsAmount == mutliTargetListPacket.Targets.Count
                && mutliTargetListPacket.Targets != null)
            {
                Session.Character.MTListTargetQueue.Clear();
                foreach (MultiTargetListSubPacket subpacket in mutliTargetListPacket.Targets)
                {
                    Session.Character.MTListTargetQueue.Push(new MTListHitTarget(subpacket.TargetType,
                        subpacket.TargetId));
                }
            }
        }

        public void MatePVPHit(Mate attacker, Skill skill, Character target, int r = 0)
        {
            if (attacker == null || target == null)
            {
                return;
            }

            if (target.Hp > 0 && attacker.Hp > 0)
            {
                if ((Session.CurrentMapInstance.MapInstanceId == ServerManager.Instance.ArenaInstance.MapInstanceId
                     || Session.CurrentMapInstance.MapInstanceId
                     == ServerManager.Instance.FamilyArenaInstance.MapInstanceId)
                    && (Session.CurrentMapInstance.Map.JaggedGrid[Session.Character.PositionX][
                            Session.Character.PositionY]?.Value != 0
                        || target.Session.CurrentMapInstance.Map.JaggedGrid[target.PositionX][
                                target.PositionY]
                            ?.Value != 0))
                {
                    // User in SafeZone
                    Session.SendPacket(StaticPacketHelper.Cancel(2, target.CharacterId));
                    return;
                }

                if (target.IsSitting)
                {
                    target.Rest();
                }

                int hitmode = 0;
                bool onyxWings = false;
                BattleEntity battleEntity = new BattleEntity(attacker);
                BattleEntity battleEntityDefense = new BattleEntity(target, null);

                int damage = DamageHelper.Instance.CalculateDamage(battleEntity, battleEntityDefense, skill,
                    ref hitmode, ref onyxWings, rare: r);
                if (target.HasGodMode)
                {
                    damage = 0;
                    hitmode = 1;
                }
                else if (target.LastPVPRevive > DateTime.Now.AddSeconds(-10)
                         || Session.Character.LastPVPRevive > DateTime.Now.AddSeconds(-10))
                {
                    damage = 0;
                    hitmode = 1;
                }
                int[] manaShield = target.GetBuff(BCardType.CardType.LightAndShadow,
                    (byte)AdditionalTypes.LightAndShadow.InflictDamageToMP);
                int[] invisible = target.GetBuff(BCardType.CardType.SpecialActions,
                   (byte)AdditionalTypes.SpecialActions.Hide);
                int[] invisibleillusionista = target.GetBuff(CardType.EffectSummon,
                    (byte)AdditionalTypes.EffectSummon.Illusionista);
                if (invisibleillusionista[0] != 0 && hitmode != 1)
                {
                    target.RemoveBuff(412); // Hideout
                }

                if (invisible[0] != 0 && hitmode != 1)
                {
                    target.RemoveBuff(2154);
                    target.RemoveBuff(2155);
                    target.RemoveBuff(2156);
                    target.RemoveBuff(2157);
                    target.RemoveBuff(2158);
                    target.RemoveBuff(2159);
                    target.RemoveBuff(2160);
                    target.RemoveBuff(85); // Hideout
                }
                target.RemoveBuff(548);
                if (manaShield[0] != 0 && hitmode != 1)
                {
                    int reduce = damage / 100 * manaShield[0];
                    if (target.Mp < reduce)
                    {
                        target.Mp = 0;
                    }
                    else
                    {
                        target.Mp -= reduce;
                    }
                }
                int[] sacrificio = target.GetBuff(BCardType.CardType.DamageConvertingSkill,
                    (byte)AdditionalTypes.DamageConvertingSkill.TransferInflictedDamage);
                if (sacrificio[0] != 0)
                {
                    int senderid = target.GetBuff(CardType.DamageConvertingSkill, (byte)AdditionalTypes.DamageConvertingSkill.TransferInflictedDamage)[2];
                    if (senderid > 0)
                    {
                        ClientSession s = ServerManager.Instance.GetSessionBySessionId(senderid);
                        if (s != null)
                        {
                            if (s.CurrentMapInstance != null)
                            {
                                if (s.Character.IsInRange(Session.Character.PositionX, Session.Character.PositionY, 10))
                                {
                                    int dannoricevuto = 0;
                                    if(damage / 2 > s.Character.SwitchLevel() * 8)
                                    {
                                        dannoricevuto = s.Character.SwitchLevel() * 8;
                                        s.Character.Hp -= dannoricevuto;
                                        if (s.Character.Hp <= 0)
                                        {
                                            s.Character.Hp = 1;
                                        }
                                        Session.CurrentMapInstance?.Broadcast($"dm 1 {s.Character.CharacterId} {s.Character.SwitchLevel() * 8}");
                                        damage -= s.Character.SwitchLevel() * 4;
                                        s.SendPacket(s.Character.GenerateStat());
                                    }
                                    else
                                    {
                                        s.Character.Hp -= (short)((damage / 2) * 0.75);
                                        if (s.Character.Hp <= 0)
                                        {
                                            s.Character.Hp = 1;
                                        }
                                        Session.CurrentMapInstance?.Broadcast($"dm 1 {s.Character.CharacterId} {(short)((damage / 2) * 0.75)}");
                                        s.SendPacket(s.Character.GenerateStat());
                                        damage = (short)((damage / 2) * 0.75) * 2;
                                    }
                                }
                            }
                        }
                    }
                }

                target.GetDamage(damage / 2);
                target.LastDefence = DateTime.Now;
                target.Session.SendPacket(target.GenerateStat());
                bool isAlive = target.Hp > 0;
                if (!isAlive && target.Session.HasCurrentMapInstance)
                {
                    if (target.Session?.IpAddress != Session.IpAddress)
                    {
                        if (target.Session.CurrentMapInstance.Map?.MapTypes.Any(
                                s => s.MapTypeId == (short)MapTypeEnum.Act4)
                            == true)
                        {
                            if (ServerManager.Instance.ChannelId == 51 && ServerManager.Instance.Act4DemonStat.Mode == 0
                                                                       && ServerManager.Instance.Act4AngelStat.Mode == 0)
                            {
                                switch (Session.Character.Faction)
                                {
                                    case FactionType.Angel:
                                        ServerManager.Instance.Act4AngelStat.Percentage += 1000;
                                        break;

                                    case FactionType.Demon:
                                        ServerManager.Instance.Act4DemonStat.Percentage += 1000;
                                        break;
                                }
                            }

                            if (Session.Character.UseSp)
                            {
                                ItemInstance Sp = Session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                                if (Sp != null)
                                {
                                    if (Session.Character.Group != null)
                                    {
                                        if (Session.Character.Group.GroupType == GroupType.Group)
                                        {
                                            foreach (ClientSession c in Session.Character.Group.Characters.GetAllItems())
                                            {
                                                if (c.CurrentMapInstance == Session.CurrentMapInstance)
                                                {
                                                    if (c.Character.UseSp)
                                                    {
                                                        ItemInstance Spin = c.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                                                        if (Spin != null)
                                                        {
                                                            if (Spin.SpLevel < 99)
                                                            {
                                                                if (target.SwitchLevel() < 20)
                                                                {
                                                                }
                                                                else if (target.SwitchLevel() < 30)
                                                                {
                                                                    Spin.XP += 50000;
                                                                    if (Spin.XP >= c.Character.SpXpLoad())
                                                                    {
                                                                        Spin.SpLevel++;
                                                                        c.SendPacket(Session.Character.GenerateLevelUp());
                                                                    }
                                                                }
                                                                else if (target.SwitchLevel() < 40)
                                                                {
                                                                    Spin.SpLevel++;
                                                                }
                                                                else if (target.SwitchLevel() < 50)
                                                                {
                                                                    Spin.SpLevel++;
                                                                    Spin.XP += 50000;
                                                                    if (Spin.XP >= c.Character.SpXpLoad())
                                                                    {
                                                                        Spin.SpLevel++;
                                                                    }
                                                                    c.SendPacket(c.Character.GenerateLevelUp());
                                                                }
                                                                else if (target.SwitchLevel() < 60)
                                                                {
                                                                    Spin.SpLevel += 2;
                                                                    c.SendPacket(c.Character.GenerateLevelUp());
                                                                }
                                                                else if (target.SwitchLevel() < 70)
                                                                {
                                                                    Spin.SpLevel += 2;
                                                                    Spin.XP += 50000;
                                                                    if (Spin.XP >= c.Character.SpXpLoad())
                                                                    {
                                                                        Spin.SpLevel++;
                                                                    }
                                                                    c.SendPacket(c.Character.GenerateLevelUp());
                                                                }
                                                                else if (target.SwitchLevel() < 80)
                                                                {
                                                                    Spin.SpLevel += 3;
                                                                    c.SendPacket(c.Character.GenerateLevelUp());
                                                                }
                                                                else if (target.SwitchLevel() < 90)
                                                                {
                                                                    Spin.SpLevel += 4;
                                                                    c.SendPacket(c.Character.GenerateLevelUp());
                                                                }
                                                                else
                                                                {
                                                                    Spin.SpLevel += 5;
                                                                    c.SendPacket(c.Character.GenerateLevelUp());
                                                                }
                                                                if (Spin.SpLevel > 99)
                                                                {
                                                                    Spin.SpLevel = 99;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (Sp.SpLevel < 99)
                                    {
                                        if (target.SwitchLevel() < 20)
                                        {
                                        }
                                        else if (target.SwitchLevel() < 30)
                                        {
                                            Sp.XP += 50000;
                                            if (Sp.XP >= Session.Character.SpXpLoad())
                                            {
                                                Sp.SpLevel++; Session.SendPacket(Session.Character.GenerateLevelUp());
                                            }
                                        }
                                        else if (target.SwitchLevel() < 40)
                                        {
                                            Sp.SpLevel++;
                                        }
                                        else if (target.SwitchLevel() < 50)
                                        {
                                            Sp.SpLevel++;
                                            Sp.XP += 50000;
                                            if (Sp.XP >= Session.Character.SpXpLoad())
                                            {
                                                Sp.SpLevel++;
                                            }
                                            Session.SendPacket(Session.Character.GenerateLevelUp());
                                        }
                                        else if (target.SwitchLevel() < 60)
                                        {
                                            Sp.SpLevel += 2;
                                            Session.SendPacket(Session.Character.GenerateLevelUp());
                                        }
                                        else if (target.SwitchLevel() < 70)
                                        {
                                            Sp.SpLevel += 2;
                                            Sp.XP += 50000;
                                            if (Sp.XP >= Session.Character.SpXpLoad())
                                            {
                                                Sp.SpLevel++;
                                            }
                                            Session.SendPacket(Session.Character.GenerateLevelUp());
                                        }
                                        else if (target.SwitchLevel() < 80)
                                        {
                                            Sp.SpLevel += 3;
                                            Session.SendPacket(Session.Character.GenerateLevelUp());
                                        }
                                        else if (target.SwitchLevel() < 90)
                                        {
                                            Sp.SpLevel += 4;
                                            Session.SendPacket(Session.Character.GenerateLevelUp());
                                        }
                                        else
                                        {
                                            Sp.SpLevel += 5;
                                            Session.SendPacket(Session.Character.GenerateLevelUp());
                                        }
                                        if (Sp.SpLevel > 99)
                                        {
                                            Sp.SpLevel = 99;
                                        }
                                    }

                                }
                            }

                            //Session.Character.SetContributi(1500);
                            Session.Character.Act4Kill++;
                            target.Act4Dead++;
                            target.GetAct4Points(-1);
                            if (target.SwitchLevel() + 10 >= Session.Character.SwitchLevel())
                            {
                                Session.Character.GetAct4Points(2);
                            }

                            if (CaligorRaid.IsRunning)
                            {

                            }
                            else if (ServerManager.Instance.Act4DemonStat.Mode != 0 || ServerManager.Instance.Act4AngelStat.Mode != 0)
                            {

                                if (target.Reputation < 50000)
                                {
                                    target.Session.SendPacket(Session.Character.GenerateSay(
                                        string.Format(Language.Instance.GetMessageFromKey("LOSE_REP"), 0), 11));
                                }
                                else
                                {
                                    target.SetReputation((short)(-(target.SwitchLevel() * 50)));
                                    Session.Character.SetReputation((short)(target.SwitchLevel() * 50));
                                    Session.SendPacket(Session.Character.GenerateLev());
                                    target.Session.SendPacket(target.GenerateSay(
                                        string.Format(Language.Instance.GetMessageFromKey("LOSE_REP"),
                                            (short)(target.SwitchLevel() * 50)), 11));
                                }
                                //target.SetContributi(-300);
                            }
                            else
                            {
                                if (target.Reputation >= 50000)
                                {
                                    Session.Character.SetReputation((short)(target.SwitchLevel() * 50));
                                    Session.SendPacket(Session.Character.GenerateFd());
                                    Session.SendPacket(Session.Character.GenerateLev());
                                }
                            }

                            if (target.Session.CurrentMapInstance.Map.MapId == 154)
                            {
                                foreach (ClientSession sess in ServerManager.Instance.Sessions.Where(
                                    s => s.HasSelectedCharacter))
                                {
                                    if (sess.Character.Faction == Session.Character.Faction)
                                    {
                                        sess.SendPacket(sess.Character.GenerateSay(
                                            string.Format(
                                                Language.Instance.GetMessageFromKey(
                                                    $"ACT4_PVP_KILL{(int)target.Faction}"), Session.Character.Name),
                                            12));
                                    }
                                    else if (sess.Character.Faction == target.Faction)
                                    {
                                        sess.SendPacket(sess.Character.GenerateSay(
                                            string.Format(
                                                Language.Instance.GetMessageFromKey(
                                                    $"ACT4_PVP_DEATH{(int)target.Faction}"), target.Name),
                                            11));
                                    }
                                    target.Session.SendPacket(target.GenerateFd());
                                    target.DisableBuffs(BuffType.All);
                                    target.Session.CurrentMapInstance.Broadcast(target.Session, target.GenerateIn(),
                                        ReceiverType.AllExceptMe);
                                    target.Session.CurrentMapInstance.Broadcast(target.Session, target.GenerateGidx(),
                                        ReceiverType.AllExceptMe);
                                    target.Session.SendPacket(
                                        target.GenerateSay(Language.Instance.GetMessageFromKey("ACT4_PVP_DIE"), 11));
                                    target.Session.SendPacket(
                                        UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ACT4_PVP_DIE"), 0));
                                    Observable.Timer(TimeSpan.FromMilliseconds(2500)).Subscribe(o =>
                                    {
                                        if (target.Faction == FactionType.Angel)
                                        {
                                            target.Hp = (int)target.HPLoad();
                                            target.Mp = (int)target.MPLoad();
                                            short x = (short)(64 + ServerManager.RandomNumber(-2, 3));
                                            short y = (short)(160 + ServerManager.RandomNumber(-2, 3));
                                            ServerManager.Instance.ChangeMapInstance(target.CharacterId, CaligorRaid.CaligorMapInstance.MapInstanceId, x, y);
                                        }
                                        else if (target.Faction == FactionType.Demon)
                                        {
                                            target.Hp = (int)target.HPLoad();
                                            target.Mp = (int)target.MPLoad();
                                            short x = (short)(122 + ServerManager.RandomNumber(-2, 3));
                                            short y = (short)(160 + ServerManager.RandomNumber(-2, 3));
                                            ServerManager.Instance.ChangeMapInstance(target.CharacterId, CaligorRaid.CaligorMapInstance.MapInstanceId, x, y);
                                        }
                                        else
                                        {
                                            target.MapId = 145;
                                            target.MapX = 51;
                                            target.MapY = 41;
                                            string connection =
                                                CommunicationServiceClient.Instance.RetrieveOriginWorld(Session.Account.AccountId);
                                            if (string.IsNullOrWhiteSpace(connection))
                                            {
                                                return;
                                            }

                                            int port = Convert.ToInt32(connection.Split(':')[1]);
                                            Session.Character.ChangeChannel(connection.Split(':')[0], port, 3);
                                            return;
                                        }

                                        target.Session.CurrentMapInstance?.Broadcast(target.Session, target.GenerateTp());
                                        target.Session.CurrentMapInstance?.Broadcast(target.GenerateRevive());
                                        target.Session.SendPacket(target.GenerateStat());
                                    });
                                }
                            }
                            else
                            {
                                foreach (ClientSession sess in ServerManager.Instance.Sessions.Where(
                                    s => s.HasSelectedCharacter))
                                {
                                    if (sess.Character.Faction == Session.Character.Faction)
                                    {
                                        sess.SendPacket(sess.Character.GenerateSay(
                                            string.Format(
                                                Language.Instance.GetMessageFromKey(
                                                    $"ACT4_PVP_KILL{(int)target.Faction}"), Session.Character.Name),
                                            12));
                                    }
                                    else if (sess.Character.Faction == target.Faction)
                                    {
                                        sess.SendPacket(sess.Character.GenerateSay(
                                            string.Format(
                                                Language.Instance.GetMessageFromKey(
                                                    $"ACT4_PVP_DEATH{(int)target.Faction}"), target.Name),
                                            11));
                                    }
                                }

                                target.Session.SendPacket(target.GenerateFd());
                                target.DisableBuffs(BuffType.All);
                                target.Session.CurrentMapInstance.Broadcast(target.Session, target.GenerateIn(),
                                    ReceiverType.AllExceptMe);
                                target.Session.CurrentMapInstance.Broadcast(target.Session, target.GenerateGidx(),
                                    ReceiverType.AllExceptMe);
                                target.Session.SendPacket(
                                    target.GenerateSay(Language.Instance.GetMessageFromKey("ACT4_PVP_DIE"), 11));
                                target.Session.SendPacket(
                                    UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ACT4_PVP_DIE"), 0));
                                Observable.Timer(TimeSpan.FromMilliseconds(2000)).Subscribe(o =>
                                {
                                    target.Session.CurrentMapInstance?.Broadcast(target.Session,
                                        $"c_mode 1 {target.CharacterId} 1564 0 0 0");
                                    target.Session.CurrentMapInstance?.Broadcast(target.GenerateRevive());
                                });
                                Observable.Timer(TimeSpan.FromMilliseconds(30000)).Subscribe(o =>
                                {
                                    target.Hp = (int)target.HPLoad();
                                    target.Mp = (int)target.MPLoad();
                                    short x = (short)(39 + ServerManager.RandomNumber(-2, 3));
                                    short y = (short)(42 + ServerManager.RandomNumber(-2, 3));
                                    if (target.Faction == FactionType.Angel)
                                    {
                                        ServerManager.Instance.ChangeMap(target.CharacterId, 130, x, y);
                                    }
                                    else if (target.Faction == FactionType.Demon)
                                    {
                                        ServerManager.Instance.ChangeMap(target.CharacterId, 131, x, y);
                                    }
                                    else
                                    {
                                        target.MapId = 145;
                                        target.MapX = 51;
                                        target.MapY = 41;
                                        string connection =
                                            CommunicationServiceClient.Instance.RetrieveOriginWorld(Session.Account.AccountId);
                                        if (string.IsNullOrWhiteSpace(connection))
                                        {
                                            return;
                                        }

                                        int port = Convert.ToInt32(connection.Split(':')[1]);
                                        Session.Character.ChangeChannel(connection.Split(':')[0], port, 3);
                                        return;
                                    }

                                    target.Session.CurrentMapInstance?.Broadcast(target.Session, target.GenerateTp());
                                    target.Session.CurrentMapInstance?.Broadcast(target.GenerateRevive());
                                    target.Session.SendPacket(target.GenerateStat());
                                });
                            }
                        }
                        else
                        {
                            Session.Character.TalentWin++;
                            target.TalentLose++;
                            Session.CurrentMapInstance?.Broadcast(Session.Character.GenerateSay(
                                string.Format(Language.Instance.GetMessageFromKey("PVP_KILL"),
                                    Session.Character.Name, target.Name), 10));
                            Observable.Timer(TimeSpan.FromMilliseconds(1000)).Subscribe(o =>
                                ServerManager.Instance.AskPvpRevive(target.CharacterId));
                        }
                    }
                }

                if (hitmode != 1)
                {
                    skill?.BCards.Where(s => s.Type.Equals((byte)BCardType.CardType.Buff)).ToList()
                        .ForEach(s => s.ApplyBCards(target, Session.Character, rare: r));
                }

                Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Npc,
                    attacker.MateTransportId, 1, target.CharacterId, (short)(skill?.SkillVNum != null ? skill.SkillVNum : 0),
                    (short)(skill?.Cooldown != null ? skill.Cooldown : 12), (short)(skill?.AttackAnimation != null ? skill.AttackAnimation : 11),
                    (short)(skill?.Effect != null ? skill.Effect : 200), attacker.MapX, attacker.MapY, isAlive,
                    (int)(target.Hp / target.HPLoad() * 100), damage, hitmode, 0));

                //
                //                switch (hitRequest.TargetHitType)
                //                {
                //                    case TargetHitType.SingleTargetHit:
                //                        hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                //                            hitRequest.Session.Character.CharacterId, 1, target.Character.CharacterId,
                //                            hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown, hitRequest.Skill.AttackAnimation,
                //                            hitRequest.SkillEffect, hitRequest.Session.Character.PositionX,
                //                            hitRequest.Session.Character.PositionY, isAlive,
                //                            (int) (target.Character.Hp / (float) target.Character.HPLoad() * 100), damage, hitmode,
                //                            (byte) (hitRequest.Skill.SkillType - 1)));
                //                        break;
                //
                //                    case TargetHitType.SingleTargetHitCombo:
                //                        hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                //                            hitRequest.Session.Character.CharacterId, 1, target.Character.CharacterId,
                //                            hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown, hitRequest.SkillCombo.Animation,
                //                            hitRequest.SkillCombo.Effect, hitRequest.Session.Character.PositionX,
                //                            hitRequest.Session.Character.PositionY, isAlive,
                //                            (int) (target.Character.Hp / (float) target.Character.HPLoad() * 100), damage, hitmode,
                //                            (byte) (hitRequest.Skill.SkillType - 1)));
                //                        break;
                //
                //                    case TargetHitType.SingleAOETargetHit:
                //                        switch (hitmode)
                //                        {
                //                            case 1:
                //                                hitmode = 4;
                //                                break;
                //
                //                            case 3:
                //                                hitmode = 6;
                //                                break;
                //
                //                            default:
                //                                hitmode = 5;
                //                                break;
                //                        }
                //
                //                        if (hitRequest.ShowTargetHitAnimation)
                //                        {
                //                            hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(
                //                                UserType.Player, hitRequest.Session.Character.CharacterId, 1,
                //                                target.Character.CharacterId, hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown,
                //                                hitRequest.Skill.AttackAnimation, hitRequest.SkillEffect, 0, 0, isAlive,
                //                                (int) (target.Character.Hp / (float) target.Character.HPLoad() * 100), 0, 0,
                //                                (byte) (hitRequest.Skill.SkillType - 1)));
                //                        }
                //
                //                        hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                //                            hitRequest.Session.Character.CharacterId, 1, target.Character.CharacterId,
                //                            hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown, hitRequest.Skill.AttackAnimation,
                //                            hitRequest.SkillEffect, hitRequest.Session.Character.PositionX,
                //                            hitRequest.Session.Character.PositionY, isAlive,
                //                            (int) (target.Character.Hp / (float) target.Character.HPLoad() * 100), damage, hitmode,
                //                            (byte) (hitRequest.Skill.SkillType - 1)));
                //                        break;
                //
                //                    case TargetHitType.AOETargetHit:
                //                        switch (hitmode)
                //                        {
                //                            case 1:
                //                                hitmode = 4;
                //                                break;
                //
                //                            case 3:
                //                                hitmode = 6;
                //                                break;
                //
                //                            default:
                //                                hitmode = 5;
                //                                break;
                //                        }
                //
                //                        hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                //                            hitRequest.Session.Character.CharacterId, 1, target.Character.CharacterId,
                //                            hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown, hitRequest.Skill.AttackAnimation,
                //                            hitRequest.SkillEffect, hitRequest.Session.Character.PositionX,
                //                            hitRequest.Session.Character.PositionY, isAlive,
                //                            (int) (target.Character.Hp / (float) target.Character.HPLoad() * 100), damage, hitmode,
                //                            (byte) (hitRequest.Skill.SkillType - 1)));
                //                        break;
                //
                //                    case TargetHitType.ZoneHit:
                //                        hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                //                            hitRequest.Session.Character.CharacterId, 1, target.Character.CharacterId,
                //                            hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown, hitRequest.Skill.AttackAnimation,
                //                            hitRequest.SkillEffect, hitRequest.MapX, hitRequest.MapY, isAlive,
                //                            (int) (target.Character.Hp / (float) target.Character.HPLoad() * 100), damage, 5,
                //                            (byte) (hitRequest.Skill.SkillType - 1)));
                //                        break;
                //
                //                    case TargetHitType.SpecialZoneHit:
                //                        hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                //                            hitRequest.Session.Character.CharacterId, 1, target.Character.CharacterId,
                //                            hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown, hitRequest.Skill.AttackAnimation,
                //                            hitRequest.SkillEffect, hitRequest.Session.Character.PositionX,
                //                            hitRequest.Session.Character.PositionY, isAlive,
                //                            (int) (target.Character.Hp / target.Character.HPLoad() * 100), damage, 0,
                //                            (byte) (hitRequest.Skill.SkillType - 1)));
                //                        break;
                //
                //                    default:
                //                        Logger.Warn("Not Implemented TargetHitType Handling!");
                //                        break;
                //                }
            }
            else
            {
                // monster already has been killed, send cancel
                Session.SendPacket(StaticPacketHelper.Cancel(2, target.CharacterId));
            }
        }

        ///<summary>
        ///u_ps packet
        ///</summary>
        ///<param name="usePartnerSkillPacket"></param>
        public void UsePartnerSkill(UsePartnerSkillPacket usePartnerSkillPacket)
        {
            try { 
            Mate m = Session.Character.Mates.Where(s => s.IsTeamMember && s.MateTransportId == usePartnerSkillPacket.MateTransportId).First();
                if (usePartnerSkillPacket != null)
                {
                    if (usePartnerSkillPacket.MapX.HasValue && usePartnerSkillPacket.MapY.HasValue)
                    {
                        m.PositionX = usePartnerSkillPacket.MapX.Value;
                        m.PositionY = usePartnerSkillPacket.MapY.Value;
                    }
                    if (m.Hp <= 0)
                    {
                        return;
                    }

                    if (m.IsSitting)
                    {
                        m.GenerateRest();
                    }
                    if (m == null)
                    {
                        return;
                    }
                    if (m.Skills == null)
                    {
                        return;
                    }
                    if (m.SpInstance == null)
                    {
                        return;
                    }
                    Skill skill = m.Skills.Where(x => x.SkillVNum == m.SpInstance.PartnerToSkill(m.SpInstance.ItemVNum)[usePartnerSkillPacket.Position]).First();
                    int r = 0;
                    if (usePartnerSkillPacket.Position == 0)
                    {
                        r = m.SpInstance.Rare;
                    }
                    if (usePartnerSkillPacket.Position == 1)
                    {
                        r = m.SpInstance.Upgrade;
                    }
                    if (usePartnerSkillPacket.Position == 2)
                    {
                        r = m.SpInstance.SpStoneUpgrade;
                    }
                    Session.CurrentMapInstance?.Broadcast(
                        StaticPacketHelper.GenerateEff(UserType.Npc, m.MateTransportId, 5005),
                        m.MapX, m.MapY);

                    Observable.Timer(TimeSpan.FromSeconds(skill.Cooldown / 10)).Subscribe(o =>
                    {
                        Session.SendPacket(Session.Character.GeneratePsr(usePartnerSkillPacket.Position));
                    });

                    if (m.Mp >= skill.MpCost && Session.HasCurrentMapInstance)
                    {
                        if (skill.TargetType == 1 && skill.HitType == 1)
                        {
                            m.Mp -= skill.MpCost;
                            Session.SendPacket(Session.Character.GenerateStat());
                            Session.CurrentMapInstance.Broadcast(StaticPacketHelper.CastOnTarget(UserType.Npc,
                                m.MateTransportId, 2, m.MateTransportId,
                                skill.CastAnimation, skill.CastEffect,
                                skill.SkillVNum));

                            // Generate scp
                            if (skill.CastEffect != 0)
                            {

                                int CastTime = skill.CastTime * 100;
                                double riduzione = 0;
                                double riduzionevera = 0;
                                if (skill.Type == 0)
                                {
                                    if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] != 0)
                                    {
                                        riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                        riduzionevera = CastTime * riduzione;
                                    }
                                }
                                if (skill.Type == 1)
                                {
                                    if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.RangedDurationIncreased)[0] != 0)
                                    {
                                        riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                        riduzionevera = CastTime * riduzione;
                                    }
                                }
                                if (skill.Type == 2)
                                {
                                    if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MagicalDurationIncreased)[0] != 0)
                                    {
                                        riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                        riduzionevera = CastTime * riduzione;
                                    }
                                }
                                CastTime += (int)riduzionevera;
                                Thread.Sleep(CastTime);
                            }

                            Session.CurrentMapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Npc,
                                 m.MateTransportId, 2, m.MateTransportId, skill.SkillVNum,
                                 skill.Cooldown, skill.AttackAnimation,
                                 skill.Effect, m.MapX,
                                 m.MapY, true,
                                 (int)(Session.Character.Hp / Session.Character.HPLoad() * 100), 0, -2,
                                 (byte)(skill.SkillType - 1)));
                            if (skill.TargetRange != 0)
                            {
                                foreach (ClientSession character in ServerManager.Instance.Sessions.Where(y =>
                                    y.CurrentMapInstance == Session.CurrentMapInstance
                                    && y.Character.CharacterId != Session.Character.CharacterId
                                    && y.Character.IsInRange(Session.Character.PositionX, Session.Character.PositionY,
                                        skill.TargetRange) && y.Character.Hp > 0))
                                {
                                    if (Session.CurrentMapInstance.Map.MapTypes.Any(y =>
                                        y.MapTypeId == (short)MapTypeEnum.Act4))
                                    {
                                        if (Session.Character.Faction != character.Character.Faction
                                            && Session.CurrentMapInstance.Map.MapId != 130
                                            && Session.CurrentMapInstance.Map.MapId != 131)
                                        {
                                            MatePVPHit(m, skill, character.Character, r);
                                        }
                                    }
                                    else if (Session.CurrentMapInstance.Map.MapId == 2106)
                                    {
                                        if (Session.Character.Family == null)
                                        {
                                            if (character.Character.Family != null)
                                            {
                                                MatePVPHit(m, skill, character.Character, r);
                                            }
                                        }
                                        else
                                        {
                                            if (character.Character.Family == null)
                                            {
                                                MatePVPHit(m, skill, character.Character, r);
                                            }
                                            else if (Session.Character.Family != character.Character.Family)
                                            {
                                                MatePVPHit(m, skill, character.Character, r);
                                            }
                                        }
                                    }
                                    else if (Session.CurrentMapInstance.Map.MapTypes.Any(y =>
                                        y.MapTypeId == (short)MapTypeEnum.PVPMap))
                                    {
                                        if (Session.Character.Group == null
                                            || !Session.Character.Group.IsMemberOfGroup(character.Character.CharacterId)
                                        )
                                        {
                                            MatePVPHit(m, skill, character.Character, r);
                                        }
                                    }
                                    else if (Session.CurrentMapInstance.IsPVP)
                                    {
                                        if (Session.Character.Group == null
                                            || !Session.Character.Group.IsMemberOfGroup(character.Character.CharacterId)
                                        )
                                        {
                                            MatePVPHit(m, skill, character.Character, r);
                                        }
                                    }
                                }

                                foreach (MapMonster mon in Session.CurrentMapInstance
                                    .GetListMonsterInRange(m.MapX, m.MapY,
                                        skill.TargetRange).Where(y => y.CurrentHp > 0))
                                {
                                    mon.HitQueue.Enqueue(new HitRequest(TargetHitType.AOETargetHit, Session, m, skill, rare: r));
                                }
                            }

                        }
                        else if (skill.TargetType == 1 && skill.HitType != 1)
                        {
                            Session.CurrentMapInstance.Broadcast(StaticPacketHelper.CastOnTarget(UserType.Npc,
                                m.MateTransportId, 2, m.MateTransportId,
                                skill.CastAnimation, skill.CastEffect,
                                skill.SkillVNum));
                            Session.CurrentMapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Npc,
                                 m.MateTransportId, 2, m.MateTransportId, skill.SkillVNum,
                                 skill.Cooldown, skill.AttackAnimation,
                                 skill.Effect, m.MapX,
                                 m.MapY, true,
                                 (int)(Session.Character.Hp / Session.Character.HPLoad() * 100), 0, -2,
                                 (byte)(skill.SkillType - 1)));
                            switch (skill.HitType)
                            {
                                case 2:
                                    IEnumerable<ClientSession> clientSessions =
                                        Session.CurrentMapInstance.Sessions?.Where(s =>
                                            s.Character.IsInRange(m.MapX,
                                                m.MapY, skill.TargetRange));
                                    if (clientSessions != null)
                                    {
                                        foreach (ClientSession target in clientSessions)
                                        {
                                            skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                                .ToList().ForEach(s =>
                                                    s.ApplyBCards(target.Character, Session.Character, rare: r));
                                        }
                                    }

                                    break;
                                case 4:
                                case 0:
                                    skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                        .ToList().ForEach(s => s.ApplyBCards(Session.Character, rare: r));
                                    break;
                            }
                        }
                        else if (skill.TargetType == 0) // monster target
                        {
                            if (usePartnerSkillPacket.UserType == UserType.Player && usePartnerSkillPacket.TargetId != Session.Character.CharacterId)
                            {
                                ClientSession playerToAttack = ServerManager.Instance.GetSessionByCharacterId(usePartnerSkillPacket.TargetId);
                                if (playerToAttack != null && Session.Character.Mp >= skill.MpCost)
                                {
                                    int Range = skill.Range;
                                    if (Session.Character.GetBuff(CardType.FearSkill, (byte)AdditionalTypes.FearSkill.AttackRangedIncreased)[0] > 0)
                                    {
                                        Range += Session.Character.GetBuff(CardType.FearSkill, (byte)AdditionalTypes.FearSkill.AttackRangedIncreased)[0];
                                    }
                                    if (Map.GetDistance(
                                            new MapCell
                                            {
                                                X = Session.Character.PositionX,
                                                Y = Session.Character.PositionY
                                            },
                                            new MapCell
                                            {
                                                X = playerToAttack.Character.PositionX,
                                                Y = playerToAttack.Character.PositionY
                                            }) <= Range + 5)
                                    {
                                        if (!Session.Character.HasGodMode)
                                        {
                                            m.Mp -= skill.MpCost;
                                        }


                                        Session.SendPacket(Session.Character.GenerateStat());

                                        Session.CurrentMapInstance.Broadcast(StaticPacketHelper.CastOnTarget(UserType.Npc,
                                            m.MateTransportId, 2, m.MateTransportId,
                                            skill.CastAnimation, skill.CastEffect,
                                            skill.SkillVNum));

                                        Session.CurrentMapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Npc,
                                             m.MateTransportId, 2, m.MateTransportId, skill.SkillVNum,
                                             skill.Cooldown, skill.AttackAnimation,
                                             skill.Effect, m.MapX,
                                             m.MapY, true,
                                             (int)(Session.Character.Hp / Session.Character.HPLoad() * 100), 0, -2,
                                             (byte)(skill.SkillType - 1)));

                                        if (skill.CastEffect != 0)
                                        {

                                            int CastTime = skill.CastTime * 100;
                                            double riduzione = 0;
                                            double riduzionevera = 0;
                                            if (skill.Type == 0)
                                            {
                                                if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] != 0)
                                                {
                                                    riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                                    riduzionevera = CastTime * riduzione;
                                                }
                                            }
                                            if (skill.Type == 1)
                                            {
                                                if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.RangedDurationIncreased)[0] != 0)
                                                {
                                                    riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                                    riduzionevera = CastTime * riduzione;
                                                }
                                            }
                                            if (skill.Type == 2)
                                            {
                                                if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MagicalDurationIncreased)[0] != 0)
                                                {
                                                    riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                                    riduzionevera = CastTime * riduzione;
                                                }
                                            }
                                            CastTime += (int)riduzionevera;
                                            Thread.Sleep(CastTime);
                                        }

                                        if (skill.HitType == 3)
                                        {
                                            int count = 0;
                                            if (playerToAttack.CurrentMapInstance == Session.CurrentMapInstance
                                                && playerToAttack.Character.CharacterId !=
                                                Session.Character.CharacterId)
                                            {
                                                if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                    s.MapTypeId == (short)MapTypeEnum.Act4))
                                                {
                                                    if (Session.Character.Faction != playerToAttack.Character.Faction
                                                        && Session.CurrentMapInstance.Map.MapId != 130
                                                        && Session.CurrentMapInstance.Map.MapId != 131)
                                                    {
                                                        count++;
                                                        MatePVPHit(m, skill, playerToAttack.Character, r);
                                                    }
                                                }
                                                else if (Session.CurrentMapInstance.Map.MapId == 2106)
                                                {
                                                    if (Session.Character.Family == null)
                                                    {
                                                        if (playerToAttack.Character.Family != null)
                                                        {
                                                            count++;
                                                            MatePVPHit(m, skill, playerToAttack.Character, r);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (playerToAttack.Character.Family == null)
                                                        {
                                                            count++;
                                                            MatePVPHit(m, skill, playerToAttack.Character, r);
                                                        }
                                                        else if (Session.Character.Family != playerToAttack.Character.Family)
                                                        {
                                                            count++;
                                                            MatePVPHit(m, skill, playerToAttack.Character, r);
                                                        }
                                                    }
                                                }
                                                else if (Session.CurrentMapInstance.Map.MapTypes.Any(y =>
                                                    y.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                {
                                                    if (Session.Character.Group == null
                                                        || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                            .Character.CharacterId))
                                                    {
                                                        count++;
                                                        MatePVPHit(m, skill, playerToAttack.Character, r);
                                                    }
                                                }
                                                else if (Session.CurrentMapInstance.IsPVP)
                                                {
                                                    if (Session.Character.Group == null
                                                        || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                            .Character.CharacterId))
                                                    {
                                                        count++;
                                                        MatePVPHit(m, skill, playerToAttack.Character, r);
                                                    }
                                                }
                                            }

                                            foreach (long id in Session.Character.MTListTargetQueue
                                                .Where(s => s.EntityType == UserType.Player).Select(s => s.TargetId))
                                            {
                                                ClientSession character =
                                                    ServerManager.Instance.GetSessionByCharacterId(id);
                                                if (character != null
                                                    && character.CurrentMapInstance == Session.CurrentMapInstance
                                                    && character.Character.CharacterId != Session.Character.CharacterId)
                                                {
                                                    if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                        s.MapTypeId == (short)MapTypeEnum.Act4))
                                                    {
                                                        if (Session.Character.Faction != character.Character.Faction
                                                            && Session.CurrentMapInstance.Map.MapId != 130
                                                            && Session.CurrentMapInstance.Map.MapId != 131)
                                                        {
                                                            count++;
                                                            MatePVPHit(m, skill, playerToAttack.Character, r);
                                                        }
                                                    }
                                                    else if (Session.CurrentMapInstance.Map.MapId == 2106)
                                                    {
                                                        if (Session.Character.Family == null)
                                                        {
                                                            if (playerToAttack.Character.Family != null)
                                                            {
                                                                count++;
                                                                MatePVPHit(m, skill, playerToAttack.Character, r);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (playerToAttack.Character.Family == null)
                                                            {
                                                                count++;
                                                                MatePVPHit(m, skill, playerToAttack.Character, r);
                                                            }
                                                            else if (Session.Character.Family != playerToAttack.Character.Family)
                                                            {
                                                                count++;
                                                                MatePVPHit(m, skill, playerToAttack.Character, r);
                                                            }
                                                        }
                                                    }
                                                    else if (Session.CurrentMapInstance.Map.MapTypes.Any(y =>
                                                        y.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                    {
                                                        if (Session.Character.Group == null
                                                            || !Session.Character.Group.IsMemberOfGroup(character
                                                                .Character
                                                                .CharacterId))
                                                        {
                                                            count++;
                                                            MatePVPHit(m, skill, playerToAttack.Character, r);
                                                        }
                                                    }
                                                    else if (Session.CurrentMapInstance.IsPVP)
                                                    {
                                                        if (Session.Character.Group == null
                                                            || !Session.Character.Group.IsMemberOfGroup(character
                                                                .Character
                                                                .CharacterId))
                                                        {
                                                            count++;
                                                            MatePVPHit(m, skill, playerToAttack.Character, r);
                                                        }
                                                    }
                                                }
                                            }

                                            if (count == 0)
                                            {
                                                Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                            }
                                        }
                                        else
                                        {
                                            // check if we will hit mutltiple targets
                                            if (skill.TargetRange != 0)
                                            {
                                                IEnumerable<ClientSession> playersInAoeRange =
                                                    ServerManager.Instance.Sessions.Where(s =>
                                                        s.CurrentMapInstance == Session.CurrentMapInstance
                                                        && s.Character.CharacterId != Session.Character.CharacterId
                                                        && s.Character.IsInRange(m.MapX,
                                                            m.MapY, skill.TargetRange));

                                                // hit the targetted monster
                                                if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                    s.MapTypeId == (short)MapTypeEnum.Act4))
                                                {
                                                    if (Session.Character.Faction
                                                        != playerToAttack.Character.Faction)
                                                    {
                                                        if (Session.CurrentMapInstance.Map.MapId != 130
                                                            && Session.CurrentMapInstance.Map.MapId != 131)
                                                        {
                                                            MatePVPHit(m, skill, playerToAttack.Character, r);
                                                        }
                                                        else
                                                        {
                                                            Session.SendPacket(
                                                                StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                    }
                                                }
                                                else if (Session.CurrentMapInstance.Map.MapTypes.Any(y =>
                                                    y.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                {
                                                    if (Session.Character.Group == null
                                                        || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                            .Character.CharacterId))
                                                    {
                                                        MatePVPHit(m, skill, playerToAttack.Character, r);
                                                    }
                                                    else
                                                    {
                                                        Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                    }
                                                }
                                                else if (Session.CurrentMapInstance.IsPVP)
                                                {
                                                    if (Session.Character.Group == null
                                                        || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                            .Character.CharacterId))
                                                    {
                                                        MatePVPHit(m, skill, playerToAttack.Character, r);
                                                    }
                                                    else
                                                    {
                                                        Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                    }
                                                }
                                                else
                                                {
                                                    Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                }

                                                //hit all other monsters
                                                foreach (ClientSession character in playersInAoeRange)
                                                {
                                                    if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                        s.MapTypeId == (short)MapTypeEnum.Act4))
                                                    {
                                                        if (Session.Character.Faction
                                                            != character.Character.Faction
                                                            && Session.CurrentMapInstance.Map.MapId != 130
                                                            && Session.CurrentMapInstance.Map.MapId != 131)
                                                        {
                                                            MatePVPHit(m, skill, playerToAttack.Character, r);
                                                        }
                                                    }
                                                    else if (Session.CurrentMapInstance.Map.MapTypes.Any(y =>
                                                        y.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                    {
                                                        if (Session.Character.Group == null
                                                            || !Session.Character.Group.IsMemberOfGroup(
                                                                character.Character.CharacterId))
                                                        {
                                                            MatePVPHit(m, skill, playerToAttack.Character, r);
                                                        }
                                                    }
                                                    else if (Session.CurrentMapInstance.IsPVP)
                                                    {
                                                        if (Session.Character.Group == null
                                                            || !Session.Character.Group.IsMemberOfGroup(
                                                                character.Character.CharacterId))
                                                        {
                                                            MatePVPHit(m, skill, playerToAttack.Character, r);
                                                        }
                                                    }
                                                }

                                                if (playerToAttack.Character.Hp <= 0)
                                                {
                                                    Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                }

                                            }
                                            else
                                            {
                                                if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                    s.MapTypeId == (short)MapTypeEnum.Act4))
                                                {
                                                    if (Session.Character.Faction
                                                        != playerToAttack.Character.Faction)
                                                    {
                                                        if (Session.CurrentMapInstance.Map.MapId != 130
                                                            && Session.CurrentMapInstance.Map.MapId != 131)
                                                        {
                                                            MatePVPHit(m, skill, playerToAttack.Character, r);
                                                        }
                                                        else
                                                        {
                                                            Session.SendPacket(
                                                                StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                    }
                                                }
                                                else if (Session.CurrentMapInstance.Map.MapTypes.Any(y =>
                                                    y.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                {
                                                    if (Session.Character.Group == null
                                                        || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                            .Character.CharacterId))
                                                    {
                                                        MatePVPHit(m, skill, playerToAttack.Character, r);
                                                    }
                                                    else
                                                    {
                                                        Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                    }
                                                }
                                                else if (Session.CurrentMapInstance.IsPVP)
                                                {
                                                    if (Session.Character.Group == null
                                                        || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                            .Character.CharacterId))
                                                    {
                                                        MatePVPHit(m, skill, playerToAttack.Character, r);
                                                    }
                                                    else
                                                    {
                                                        Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                    }
                                                }
                                                else
                                                {
                                                    Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                    }
                                }
                                else
                                {
                                    Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                }
                            }
                            else
                            {
                                MapMonster monsterToAttack = Session.CurrentMapInstance.GetMonster(usePartnerSkillPacket.TargetId);
                                if (monsterToAttack != null && m.Mp >= skill.MpCost)
                                {
                                    int Range = skill.Range;
                                    if (Session.Character.GetBuff(CardType.FearSkill, (byte)AdditionalTypes.FearSkill.AttackRangedIncreased)[0] > 0)
                                    {
                                        Range += Session.Character.GetBuff(CardType.FearSkill, (byte)AdditionalTypes.FearSkill.AttackRangedIncreased)[0];
                                    }
                                    if (Map.GetDistance(
                                            new MapCell
                                            {
                                                X = m.MapX,
                                                Y = m.MapY
                                            },
                                            new MapCell { X = monsterToAttack.MapX, Y = monsterToAttack.MapY })
                                        <= Range + 5 + monsterToAttack.Monster.BasicArea)
                                    {
                                        m.Mp -= skill.MpCost;

                                        /*monsterToAttack.Monster.BCards.Where(s => s.CastType == 1).ToList()
                                            .ForEach(s => s.ApplyBCards(this, rare: r));
                                        Session.SendPacket(Session.Character.GenerateStat());*/

                                        Session.CurrentMapInstance.Broadcast(StaticPacketHelper.CastOnTarget(UserType.Npc,
                                            m.MateTransportId, 2, m.MateTransportId,
                                            skill.CastAnimation, skill.CastEffect,
                                            skill.SkillVNum));

                                        Session.CurrentMapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Npc,
                                             m.MateTransportId, 2, m.MateTransportId, skill.SkillVNum,
                                             skill.Cooldown, skill.AttackAnimation,
                                             skill.Effect, m.MapX,
                                             m.MapY, true,
                                             (int)(Session.Character.Hp / Session.Character.HPLoad() * 100), 0, -2,
                                             (byte)(skill.SkillType - 1)));

                                        if (skill.CastEffect != 0)
                                        {

                                            int CastTime = skill.CastTime * 100;
                                            double riduzione = 0;
                                            double riduzionevera = 0;
                                            if (skill.Type == 0)
                                            {
                                                if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] != 0)
                                                {
                                                    riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                                    riduzionevera = CastTime * riduzione;
                                                }
                                            }
                                            if (skill.Type == 1)
                                            {
                                                if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.RangedDurationIncreased)[0] != 0)
                                                {
                                                    riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                                    riduzionevera = CastTime * riduzione;
                                                }
                                            }
                                            if (skill.Type == 2)
                                            {
                                                if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MagicalDurationIncreased)[0] != 0)
                                                {
                                                    riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                                    riduzionevera = CastTime * riduzione;
                                                }
                                            }
                                            CastTime += (int)riduzionevera;
                                            Thread.Sleep(CastTime);
                                        }

                                        if (skill.HitType == 3)
                                        {
                                            monsterToAttack.HitQueue.Enqueue(new HitRequest(
                                                TargetHitType.SingleAOETargetHit, Session, m, skill, rare: r));

                                            foreach (long id in Session.Character.MTListTargetQueue
                                                .Where(s => s.EntityType == UserType.Monster).Select(s => s.TargetId))
                                            {
                                                MapMonster mon = Session.CurrentMapInstance.GetMonster(id);
                                                if (mon?.CurrentHp > 0)
                                                {
                                                    monsterToAttack.HitQueue.Enqueue(new HitRequest(
                                                        TargetHitType.SingleAOETargetHit, Session, m, skill, rare: r));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (skill.TargetRange != 0) // check if we will hit mutltiple targets
                                            {
                                                List<MapMonster> monstersInAoeRange = Session.CurrentMapInstance?
                                                                                          .GetListMonsterInRange(
                                                                                              monsterToAttack.MapX,
                                                                                              monsterToAttack.MapY,
                                                                                              skill.TargetRange)
                                                                                          ?.ToList();

                                                //hit the targetted monster
                                                monsterToAttack.HitQueue.Enqueue(new HitRequest(
                                                    TargetHitType.SingleAOETargetHit, Session, m, skill, rare: r));

                                                //hit all other monsters
                                                if (monstersInAoeRange != null && monstersInAoeRange.Count != 0)
                                                {
                                                    foreach (MapMonster mon in monstersInAoeRange.Where(y =>
                                                        y.MapMonsterId != monsterToAttack.MapMonsterId)
                                                    ) //exclude targetted monster
                                                    {
                                                        monsterToAttack.HitQueue.Enqueue(new HitRequest(
                                                            TargetHitType.SingleAOETargetHit, Session, m, skill, rare: r));
                                                    }
                                                }
                                                else
                                                {
                                                    Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                }

                                                if (!monsterToAttack.IsAlive)
                                                {
                                                    Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                                }
                                            }
                                            else
                                            {
                                                monsterToAttack.HitQueue.Enqueue(new HitRequest(
                                                    TargetHitType.SingleTargetHit, Session, m, skill, rare: r));

                                            }
                                        }
                                    }
                                    else
                                    {
                                        Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                    }
                                }
                                else
                                {
                                    Session.SendPacket(StaticPacketHelper.Cancel(2, usePartnerSkillPacket.TargetId));
                                }
                            }

                            if (skill.HitType == 3)
                            {
                                Session.Character.MTListTargetQueue.Clear();
                            }
                        }

                    }


                }
            }
            catch (Exception ex)
            {

            }
        }


        /// <summary>
        /// ob_a packet
        /// </summary>
        /// <param name="obaPacket"></param>
        public void ObA(ObAPacket obaPacket)
        {

        }

        /// <summary>
        /// u_s packet
        /// </summary>
        /// <param name="useSkillPacket"></param>
        public void UseSkill(UseSkillPacket useSkillPacket)
        {
            if (useSkillPacket != null && Session.Character.LastAction.AddMilliseconds(500) < DateTime.Now)
            {
                Session.Character.LastAction = DateTime.Now;
                if (Session.Character.NoAttack)
                {
                    Session.SendPacket(StaticPacketHelper.Cancel(2));
                    return;
                }

                if (Session.Character.CanFight && useSkillPacket != null)
                {
                    Session.Character.RemoveBuff(614);
                    Session.Character.RemoveBuff(615);
                    Session.Character.RemoveBuff(616);
                    bool isMuted = Session.Character.MuteMessage();
                    if (isMuted || Session.Character.IsVehicled || Session.Character.InvisibleGm)
                    {
                        Session.SendPacket(StaticPacketHelper.Cancel());
                        return;
                    }


                    if (useSkillPacket.MapX.HasValue && useSkillPacket.MapY.HasValue)
                    {
                        Session.Character.PositionX = useSkillPacket.MapX.Value;
                        Session.Character.PositionY = useSkillPacket.MapY.Value;
                    }

                    if (Session.Character.IsSitting)
                    {
                        Session.Character.Rest();
                    }


                    switch (useSkillPacket.UserType)
                    {
                        case UserType.Monster:
                            if (Session.Character.Hp > 0)
                            {
                                TargetHit(useSkillPacket.CastId, useSkillPacket.MapMonsterId);
                                int[] fairyWings = Session.Character.GetBuff(CardType.EffectSummon, 11);
                                int random = ServerManager.RandomNumber();
                                if (fairyWings[0] > random)
                                {
                                    Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(o =>
                                    {
                                        CharacterSkill ski =
                                        (Session.Character.UseSp
                                            ? Session.Character.SkillsSp?.GetAllItems()
                                            : Session.Character.Skills?.GetAllItems())?.Find(s =>
                                            s.Skill?.CastId == useSkillPacket.CastId && s.Skill?.UpgradeSkill == 0);
                                        if (ski != null)
                                        {
                                            double CooldownReduce = (short)Session.Character.GetBuff(CardType.Morale, (byte)AdditionalTypes.Morale.SkillCooldownDecreased)[0];
                                            short Cooldown = ski.Skill.Cooldown;

                                            if (CooldownReduce != 0)
                                            {
                                                CooldownReduce = (Cooldown * (CooldownReduce / 100D));
                                                Cooldown += (short)CooldownReduce;
                                                if (Cooldown <= 0)
                                                {
                                                    Cooldown = 1;
                                                }
                                            }

                                            ski.LastUse = DateTime.Now.AddMilliseconds(Cooldown * 100 * -1);
                                            Session.SendPacket(StaticPacketHelper.SkillReset(useSkillPacket.CastId));
                                        }
                                    });
                                }
                            }

                            break;

                        case UserType.Player:
                            if (Session.Character.Hp > 0)
                            {
                                if (useSkillPacket.MapMonsterId != Session.Character.CharacterId)
                                {
                                    TargetHit(useSkillPacket.CastId, useSkillPacket.MapMonsterId, true);
                                }
                                else
                                {
                                    TargetHit(useSkillPacket.CastId, useSkillPacket.MapMonsterId);
                                }

                                int[] fairyWings = Session.Character.GetBuff(CardType.EffectSummon, 11);
                                int random = ServerManager.RandomNumber();
                                if (fairyWings[0] > random)
                                {
                                    Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(o =>
                                    {
                                        CharacterSkill ski =
                                        (Session.Character.UseSp
                                            ? Session.Character.SkillsSp?.GetAllItems()
                                            : Session.Character.Skills?.GetAllItems())?.Find(s =>
                                            s.Skill?.CastId == useSkillPacket.CastId && s.Skill?.UpgradeSkill == 0);
                                        if (ski != null)
                                        {

                                            double CooldownReduce = (short)Session.Character.GetBuff(CardType.Morale, (byte)AdditionalTypes.Morale.SkillCooldownDecreased)[0];
                                            short Cooldown = ski.Skill.Cooldown;

                                            if (CooldownReduce != 0)
                                            {
                                                CooldownReduce = (Cooldown * (CooldownReduce / 100D));
                                                Cooldown += (short)CooldownReduce;
                                                if (Cooldown <= 0)
                                                {
                                                    Cooldown = 1;
                                                }
                                            }
                                            ski.LastUse = DateTime.Now.AddMilliseconds(Cooldown * 100 * -1);
                                            Session.SendPacket(StaticPacketHelper.SkillReset(useSkillPacket.CastId));
                                        }
                                    });
                                }
                            }
                            else
                            {
                                Session.SendPacket(StaticPacketHelper.Cancel(2));
                            }

                            break;

                        case UserType.Npc:
                            Session.SendPacket(StaticPacketHelper.Cancel(2));
                            break;
                        /*if (Session.Character.Hp > 0)
                        {
                            if(Session.Character.Mates != null)
                            {
                                if (Session.Character.Mates.Where(s => s.IsTeamMember).FirstOrDefault() != null)
                                {
                                    if(useSkillPacket.MapMonsterId != Session.Character.Mates.Where(s => s.IsTeamMember).FirstOrDefault().MateTransportId)
                                    {
                                        TargetHit(useSkillPacket.CastId, useSkillPacket.MapMonsterId);
                                    }
                                    else
                                    {
                                        Session.SendPacket(StaticPacketHelper.Cancel(2));
                                    }
                                }
                                else
                                {
                                    TargetHit(useSkillPacket.CastId, useSkillPacket.MapMonsterId);
                                }
                            }
                            else
                            {
                                TargetHit(useSkillPacket.CastId, useSkillPacket.MapMonsterId);
                            }
                        }
                        else
                        {
                            Session.SendPacket(StaticPacketHelper.Cancel(2));
                        }
                        break;*/

                        default:
                            Session.SendPacket(StaticPacketHelper.Cancel(2));
                            return;
                    }
                }
                else
                {
                    Session.SendPacket(StaticPacketHelper.Cancel(2));
                }
            }
            else
            {
                Session.SendPacket(StaticPacketHelper.Cancel(2));
            }
        }

        /// <summary>
        /// u_as packet
        /// </summary>
        /// <param name="useAoeSkillPacket"></param>
        public void UseZonesSkill(UseAOESkillPacket useAoeSkillPacket)
        {
            if (useAoeSkillPacket != null && Session.Character.LastAction.AddMilliseconds(250) < DateTime.Now)
            {
                Session.Character.LastAction = DateTime.Now;
                bool isMuted = Session.Character.MuteMessage();
                if (isMuted || Session.Character.IsVehicled)
                {
                    Session.SendPacket(StaticPacketHelper.Cancel());
                }
                else
                {
                    if (Session.Character.LastTransform.AddSeconds(3) > DateTime.Now)
                    {
                        Session.SendPacket(StaticPacketHelper.Cancel());
                        Session.SendPacket(
                            UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CANT_ATTACK"), 0));
                        return;
                    }

                    if (Session.Character.CanFight && Session.Character.Hp > 0)
                    {
                        ZoneHit(useAoeSkillPacket.CastId, useAoeSkillPacket.MapX, useAoeSkillPacket.MapY);
                    }
                }
            }
            else
            {
                Session.SendPacket(StaticPacketHelper.Cancel(2));
            }
        }

        private void PvpHit(HitRequest hitRequest, ClientSession target)
        {
            if (target?.Character.Hp > 0 && hitRequest?.Session.Character.Hp > 0)
            {
                if ((Session.CurrentMapInstance.MapInstanceId == ServerManager.Instance.ArenaInstance.MapInstanceId
                     || Session.CurrentMapInstance.MapInstanceId
                     == ServerManager.Instance.FamilyArenaInstance.MapInstanceId)
                    && (Session.CurrentMapInstance.Map.JaggedGrid[Session.Character.PositionX][
                                Session.Character.PositionY]
                            ?.Value != 0
                        || target.CurrentMapInstance.Map.JaggedGrid[target.Character.PositionX][
                                target.Character.PositionY]
                            ?.Value != 0))
                {
                    // User in SafeZone
                    hitRequest.Session.SendPacket(StaticPacketHelper.Cancel(2, target.Character.CharacterId));
                    return;
                }

                if (target.Character.IsSitting)
                {
                    target.Character.Rest();
                }

                int hitmode = 0;
                bool onyxWings = false;
                BattleEntity battleEntity = new BattleEntity(hitRequest.Session.Character, hitRequest.Skill);
                BattleEntity battleEntityDefense = new BattleEntity(target.Character, null);
                int damage = DamageHelper.Instance.CalculateDamage(battleEntity, battleEntityDefense, hitRequest.Skill,
                    ref hitmode, ref onyxWings);
                if (battleEntity.Session.Character.Ricarica > 0)
                {
                    damage += battleEntity.Session.Character.Ricarica;
                    battleEntity.Session.Character.Ricarica = 0;
                    battleEntity.Session.Character.RemoveBuff(0);
                }
                if (target.Character.HasGodMode)
                {
                    damage = 0;
                    hitmode = 1;
                }
                else if (target.Character.LastPVPRevive > DateTime.Now.AddSeconds(-10)
                         || hitRequest.Session.Character.LastPVPRevive > DateTime.Now.AddSeconds(-10))
                {
                    damage = 0;
                    hitmode = 1;
                }

                int[] manaShield = target.Character.GetBuff(CardType.LightAndShadow,
                    (byte) AdditionalTypes.LightAndShadow.InflictDamageToMP);
                int[] invisible = target.Character.GetBuff(CardType.SpecialActions,
                    (byte)AdditionalTypes.SpecialActions.Hide);


                if (invisible[0] != 0 && hitmode != 1)
                {
                    target.Character.RemoveBuff(2154);
                    target.Character.RemoveBuff(2155);
                    target.Character.RemoveBuff(2156);
                    target.Character.RemoveBuff(2157);
                    target.Character.RemoveBuff(2158);
                    target.Character.RemoveBuff(2159);
                    target.Character.RemoveBuff(2160);
                    target.Character.RemoveBuff(85); // Hideout
                }

                target.Character.RemoveBuff(548);


                int[] invisibleillusionista = target.Character.GetBuff(CardType.EffectSummon,
                    (byte)AdditionalTypes.EffectSummon.Illusionista);
                if (invisibleillusionista[0] != 0 && hitmode != 1)
                {
                    target.Character.RemoveBuff(412); // Hideout
                }

                if (manaShield[0] != 0 && hitmode != 1)
                {
                    int reduce = damage / 100 * manaShield[0];
                    if (target.Character.Mp < reduce)
                    {
                        target.Character.Mp = 0;
                    }
                    else
                    {
                        target.Character.Mp -= reduce;
                    }
                }

                if (onyxWings && hitmode != 1)
                {
                    short onyxX = (short) (hitRequest.Session.Character.PositionX + 2);
                    short onyxY = (short) (hitRequest.Session.Character.PositionY + 2);
                    int onyxId = target.CurrentMapInstance.GetNextMonsterId();
                    MapMonster onyx = new MapMonster
                    {
                        MonsterVNum = 2371,
                        MapX = onyxX,
                        MapY = onyxY,
                        MapMonsterId = onyxId,
                        IsHostile = false,
                        IsMoving = false,
                        ShouldRespawn = false
                    };
                    target.CurrentMapInstance.Broadcast(UserInterfaceHelper.GenerateGuri(31, 1,
                        hitRequest.Session.Character.CharacterId, onyxX, onyxY));
                    onyx.Initialize(target.CurrentMapInstance);
                    target.CurrentMapInstance.AddMonster(onyx);
                    target.CurrentMapInstance.Broadcast(onyx.GenerateIn());
                    target.Character.Hp -= damage / 2;
                    Observable.Timer(TimeSpan.FromMilliseconds(350)).Subscribe(o =>
                    //{ niby crash
                    //    if (target != null)
                    //    {
                    //        target.CurrentMapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Monster, onyxId, 1,
                    //            target.Character.CharacterId, -1, 0, -1, hitRequest.Skill.Effect, -1, -1, true, 92,
                    //            damage / 2, 0, 0));
                    //        target.CurrentMapInstance.RemoveMonster(onyx);
                    //        target.CurrentMapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster,
                    //            onyx.MapMonsterId));
                    //    }
                    //});
                    {
                        target.CurrentMapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Monster, onyxId, 1,
                            target.Character.CharacterId, -1, 0, -1, hitRequest.Skill.Effect, -1, -1, true, 92,
                            damage / 2, 0, 0));
                        target.CurrentMapInstance.RemoveMonster(onyx);
                        target.CurrentMapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster,
                            onyx.MapMonsterId));
                    });
                }

                //Reflect Damage
                if (!target.Character.Buff.ContainsKey(664) && !target.Character.Buff.ContainsKey(502) && !target.Character.Buff.ContainsKey(666))
                {
                    target.Character.GetDamage(damage / 2);
                    target.Character.LastDefence = DateTime.Now;
                    target.SendPacket(target.Character.GenerateStat());
                }
                else
                {
                    if (target.Character.Buff.ContainsKey(664))
                        if (damage / 2 > hitRequest.Session.Character.Level * 100)
                            hitRequest.Session.Character.GetDamage(hitRequest.Session.Character.Level * 100);
                        else
                            hitRequest.Session.Character.GetDamage(damage / 2);
                    else
                    if (damage / 2 > hitRequest.Session.Character.Level * 50)
                        hitRequest.Session.Character.GetDamage(hitRequest.Session.Character.Level * 50);
                    else
                        hitRequest.Session.Character.GetDamage(damage / 2);
                    target.Character.LastDefence = DateTime.Now;
                    if (hitRequest.Session.Character.Hp < 1)
                        hitRequest.Session.Character.Hp = 1;
                    hitRequest.Session.SendPacket(hitRequest.Session.Character.GenerateStat());

                    if (target.Character.Buff.ContainsKey(666))
                    {
                        if (damage / 2 > hitRequest.Session.Character.Level * 20)
                            hitRequest.Session.Character.GetDamage(hitRequest.Session.Character.Level * 20);
                        else
                            hitRequest.Session.Character.GetDamage(damage / 2);
                    }
                    damage = 0;
                    hitRequest.Session.SendPacket(hitRequest.Session.Character.GenerateStat());
                }
                //End Reflect Damage

                if (damage > 0)
                {
                    Session.Character.RemoveBuff(2154);
                    Session.Character.RemoveBuff(2155);
                    Session.Character.RemoveBuff(2156);
                    Session.Character.RemoveBuff(2157);
                    Session.Character.RemoveBuff(2158);
                    Session.Character.RemoveBuff(2159);
                    Session.Character.RemoveBuff(2160);
                    Session.Character.RemoveBuff(548);
                    Session.Character.RemoveBuff(85);
                    Session.Character.RemoveBuff(559);
                }
                if(hitRequest.Skill.SkillVNum == 848 || hitRequest.Skill.SkillVNum == 847 || hitRequest.Skill.SkillVNum == 817 || hitRequest.Skill.SkillVNum == 815)
                {
                    damage = 0;
                }

                int[] sacrificio = target.Character.GetBuff(BCardType.CardType.DamageConvertingSkill,
                    (byte)AdditionalTypes.DamageConvertingSkill.TransferInflictedDamage);
                if (sacrificio[0] != 0)
                {
                    int senderid = target.Character.GetBuff(CardType.DamageConvertingSkill, (byte)AdditionalTypes.DamageConvertingSkill.TransferInflictedDamage)[2];
                    if (senderid > 0)
                    {
                        ClientSession s = ServerManager.Instance.GetSessionBySessionId(senderid);
                        if (s != null)
                        {
                            if (s.CurrentMapInstance != null)
                            {
                                if (s.Character.IsInRange(Session.Character.PositionX, Session.Character.PositionY, 10))
                                {
                                    int dannoricevuto = 0;
                                    if (damage / 2 > s.Character.SwitchLevel() * 8)
                                    {
                                        dannoricevuto = s.Character.SwitchLevel() * 8;
                                        s.Character.Hp -= dannoricevuto;
                                        if (s.Character.Hp <= 0)
                                        {
                                            s.Character.Hp = 1;
                                        }
                                        Session.CurrentMapInstance?.Broadcast($"dm 1 {s.Character.CharacterId} {s.Character.SwitchLevel() * 8}");

                                        s.SendPacket(s.Character.GenerateStat());
                                        damage -= s.Character.SwitchLevel() * 4;
                                    }
                                    else
                                    {
                                        s.Character.Hp -= (short)((damage / 2) * 0.75);
                                        if (s.Character.Hp <= 0)
                                        {
                                            s.Character.Hp = 1;
                                        }
                                        Session.CurrentMapInstance?.Broadcast($"dm 1 {s.Character.CharacterId} {(short)((damage / 2) * 0.75)}");
                                        s.SendPacket(s.Character.GenerateStat());
                                        damage = (short)((damage / 2) * 0.75) * 2;
                                    }
                                }
                            }
                        }
                    }
                }

                target.Character.GetDamage(damage / 2);
                if (damage != 0)
                {
                    hitRequest.Skill.BCards.Where(s => s.Type.Equals((byte)CardType.Reflection)).ToList()
                            .ForEach(s => s.ApplyBCards(target.Character, Session.Character));
                }
                target.Character.LastDefence = DateTime.Now;
                target.SendPacket(target.Character.GenerateStat());
                bool isAlive = target.Character.Hp > 0;
                if (!isAlive && target.HasCurrentMapInstance)
                {
                    try
                    {
                        if (target.Character.GetBuff(CardType.SniperAttack, (byte)AdditionalTypes.SniperAttack.KillerHPIncreasing)[0] != 0)
                        {
                            int qty = Math.Abs(target.Character.GetBuff(CardType.SniperAttack, (byte)AdditionalTypes.SniperAttack.KillerHPIncreasing)[0]);
                            if(hitRequest.Session != null)
                            {
                                short totqty = (short)(hitRequest.Session.Character.Hp * (qty / 100));
                                hitRequest.Session.Character.Hp += totqty;
                                hitRequest.Session.CurrentMapInstance?.Broadcast(hitRequest.Session.Character.GenerateRc(totqty));
                                if (hitRequest.Session.Character.Hp > hitRequest.Session.Character.HPLoad())
                                {
                                    hitRequest.Session.Character.Hp = (int)hitRequest.Session.Character.HPLoad();
                                }
                                hitRequest.Session.SendPacket(hitRequest.Session.Character.GenerateStat());
                            }
                        }
                        if (hitRequest.Skill.BCards?.Where(s => s.Type == 64 && s.SubType == 3).First().FirstData > 0)
                        {
                            hitRequest.Session.Character.AddBuff(new Buff((short)hitRequest.Skill.BCards.Where(s => s.Type == 64 && s.SubType == 3).First().SecondData, hitRequest.Session.Character.SwitchLevel()));
                        }
                    }
                    catch(Exception ex)
                    {

                    }
                    if (target.CurrentMapInstance.Map?.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4)
                        == true)
                    {
                        if (hitRequest.Session.IpAddress != target.IpAddress)
                        {
                            if (ServerManager.Instance.ChannelId == 51 && ServerManager.Instance.Act4DemonStat.Mode == 0
                                                                       && ServerManager.Instance.Act4AngelStat.Mode == 0)
                            {
                                switch (Session.Character.Faction)
                                {
                                    case FactionType.Angel:
                                        ServerManager.Instance.Act4AngelStat.Percentage += 40;
                                        break;

                                    case FactionType.Demon:
                                        ServerManager.Instance.Act4DemonStat.Percentage += 40;
                                        break;
                                }
                            }

                            //if (hitRequest.Session.Character.Group == null)
                            //{
                            //    hitRequest.Session.Character.GetContributi(1500);
                            //    //hitRequest.Session.Character.Contributi += target.Character.Level * 4;
                            //}
                            //else
                            //{
                            //    hitRequest.Session.Character.GetContributi(750);
                            //    foreach (ClientSession s in hitRequest.Session.Character.Group.Characters.GetAllItems())
                            //    {
                            //        s.Character.GetContributi(250);
                            //    }
                            //}

                            hitRequest.Session.Character.Act4Kill++;
                            target.Character.Act4Dead++;
                            target.Character.GetAct4Points(-1);
                            if (target.Character.SwitchLevel() + 10 >= hitRequest.Session.Character.SwitchLevel())
                            {
                                hitRequest.Session.Character.GetAct4Points(2);
                            }

                            if (CaligorRaid.IsRunning)
                            {

                            }
                            else if (ServerManager.Instance.Act4DemonStat.Mode != 0 || ServerManager.Instance.Act4AngelStat.Mode != 0)
                            {

                                if (target.Character.Reputation < 50000)
                                {
                                    target.SendPacket(Session.Character.GenerateSay(
                                        string.Format(Language.Instance.GetMessageFromKey("LOSE_REP"), 0), 11));
                                }
                                else
                                {
                                    target.Character.SetReputation((short)-(target.Character.SwitchLevel() * 50));
                                    Session.Character.SetReputation((short)(target.Character.SwitchLevel() * 50));
                                    Session.SendPacket(Session.Character.GenerateLev());
                                    target.Character.Session.SendPacket(target.Character.GenerateSay(
                                        string.Format(Language.Instance.GetMessageFromKey("LOSE_REP"),
                                            (short)(target.Character.SwitchLevel() * 50)), 11));
                                }
                                //target.Character.SetContributi(-300);
                            }
                            else
                            {
                                if (target.Character.Reputation >= 50000)
                                {
                                    Session.Character.SetReputation((short)(target.Character.SwitchLevel() * 50));
                                    Session.SendPacket(Session.Character.GenerateFd());
                                    Session.SendPacket(Session.Character.GenerateLev());
                                }
                            }

                            if (target.CurrentMapInstance.Map.MapId == 154)
                            {
                                foreach (ClientSession sess in ServerManager.Instance.Sessions.Where(
                                    s => s.HasSelectedCharacter))
                                {
                                    if (sess.Character.Faction == Session.Character.Faction)
                                    {
                                        sess.SendPacket(sess.Character.GenerateSay(
                                            string.Format(
                                                Language.Instance.GetMessageFromKey(
                                                    $"ACT4_PVP_KILL{(int)target.Character.Faction}"), Session.Character.Name),
                                            12));
                                    }
                                    else if (sess.Character.Faction == target.Character.Faction)
                                    {
                                        sess.SendPacket(sess.Character.GenerateSay(
                                            string.Format(
                                                Language.Instance.GetMessageFromKey(
                                                    $"ACT4_PVP_DEATH{(int)target.Character.Faction}"), target.Character.Name),
                                            11));
                                    }
                                    target.SendPacket(target.Character.GenerateFd());
                                    target.Character.DisableBuffs(BuffType.All);
                                    target.CurrentMapInstance.Broadcast(target, target.Character.GenerateIn(),
                                        ReceiverType.AllExceptMe);
                                    target.CurrentMapInstance.Broadcast(target, target.Character.GenerateGidx(),
                                        ReceiverType.AllExceptMe);
                                    target.SendPacket(
                                        target.Character.GenerateSay(Language.Instance.GetMessageFromKey("ACT4_PVP_DIE"), 11));
                                    target.SendPacket(
                                        UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ACT4_PVP_DIE"), 0));
                                    Observable.Timer(TimeSpan.FromMilliseconds(2500)).Subscribe(o =>
                                    {
                                        if (target.Character.Faction == FactionType.Angel)
                                        {
                                            target.Character.Hp = (int)target.Character.HPLoad();
                                            target.Character.Mp = (int)target.Character.MPLoad();
                                            short x = (short)(64 + ServerManager.RandomNumber(-2, 3));
                                            short y = (short)(160 + ServerManager.RandomNumber(-2, 3));
                                            ServerManager.Instance.ChangeMapInstance(target.Character.CharacterId, CaligorRaid.CaligorMapInstance.MapInstanceId, x, y);
                                        }
                                        else if (target.Character.Faction == FactionType.Demon)
                                        {
                                            target.Character.Hp = (int)target.Character.HPLoad();
                                            target.Character.Mp = (int)target.Character.MPLoad();
                                            short x = (short)(122 + ServerManager.RandomNumber(-2, 3));
                                            short y = (short)(160 + ServerManager.RandomNumber(-2, 3));
                                            ServerManager.Instance.ChangeMapInstance(target.Character.CharacterId, CaligorRaid.CaligorMapInstance.MapInstanceId, x, y);
                                        }
                                        else
                                        {
                                            target.Character.MapId = 145;
                                            target.Character.MapX = 51;
                                            target.Character.MapY = 41;
                                            string connection =
                                                CommunicationServiceClient.Instance.RetrieveOriginWorld(Session.Account.AccountId);
                                            if (string.IsNullOrWhiteSpace(connection))
                                            {
                                                return;
                                            }

                                            int port = Convert.ToInt32(connection.Split(':')[1]);
                                            Session.Character.ChangeChannel(connection.Split(':')[0], port, 3);
                                            return;
                                        }

                                        target.Character.Session.CurrentMapInstance?.Broadcast(target.Character.Session, target.Character.GenerateTp());
                                        target.Character.Session.CurrentMapInstance?.Broadcast(target.Character.GenerateRevive());
                                        target.Character.Session.SendPacket(target.Character.GenerateStat());
                                    });
                                }
                            }
                            else
                            {
                                foreach (ClientSession sess in ServerManager.Instance.Sessions.Where(
                                s => s.HasSelectedCharacter))
                                {
                                    if (sess.Character.Faction == Session.Character.Faction)
                                    {
                                        sess.SendPacket(sess.Character.GenerateSay(
                                            string.Format(
                                                Language.Instance.GetMessageFromKey(
                                                    $"ACT4_PVP_KILL{(int)target.Character.Faction}"), Session.Character.Name),
                                            12));
                                    }
                                    else if (sess.Character.Faction == target.Character.Faction)
                                    {
                                        sess.SendPacket(sess.Character.GenerateSay(
                                            string.Format(
                                                Language.Instance.GetMessageFromKey(
                                                    $"ACT4_PVP_DEATH{(int)target.Character.Faction}"), target.Character.Name),
                                            11));
                                    }
                                }

                                target.SendPacket(target.Character.GenerateFd());
                                target.Character.DisableBuffs(BuffType.All);
                                target.CurrentMapInstance.Broadcast(target, target.Character.GenerateIn(),
                                    ReceiverType.AllExceptMe);
                                target.CurrentMapInstance.Broadcast(target, target.Character.GenerateGidx(),
                                    ReceiverType.AllExceptMe);
                                target.SendPacket(
                                    target.Character.GenerateSay(Language.Instance.GetMessageFromKey("ACT4_PVP_DIE"), 11));
                                target.SendPacket(
                                    UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ACT4_PVP_DIE"), 0));
                                Observable.Timer(TimeSpan.FromMilliseconds(2000)).Subscribe(o =>
                                {
                                    target.CurrentMapInstance?.Broadcast(target,
                                        $"c_mode 1 {target.Character.CharacterId} 1564 0 0 0");
                                    target.CurrentMapInstance?.Broadcast(target.Character.GenerateRevive());
                                });
                                Observable.Timer(TimeSpan.FromMilliseconds(30000)).Subscribe(o =>
                                {
                                    target.Character.Hp = (int)target.Character.HPLoad();
                                    target.Character.Mp = (int)target.Character.MPLoad();
                                    short x = (short)(39 + ServerManager.RandomNumber(-2, 3));
                                    short y = (short)(42 + ServerManager.RandomNumber(-2, 3));
                                    if (target.Character.Faction == FactionType.Angel)
                                    {
                                        ServerManager.Instance.ChangeMap(target.Character.CharacterId, 130, x, y);
                                    }
                                    else if (target.Character.Faction == FactionType.Demon)
                                    {
                                        ServerManager.Instance.ChangeMap(target.Character.CharacterId, 131, x, y);
                                    }
                                    else
                                    {
                                        target.Character.MapId = 145;
                                        target.Character.MapX = 51;
                                        target.Character.MapY = 41;
                                        string connection =
                                            CommunicationServiceClient.Instance.RetrieveOriginWorld(Session.Account.AccountId);
                                        if (string.IsNullOrWhiteSpace(connection))
                                        {
                                            return;
                                        }

                                        int port = Convert.ToInt32(connection.Split(':')[1]);
                                        Session.Character.ChangeChannel(connection.Split(':')[0], port, 3);
                                        return;
                                    }

                                    target.CurrentMapInstance?.Broadcast(target, target.Character.GenerateTp());
                                    target.CurrentMapInstance?.Broadcast(target.Character.GenerateRevive());
                                    target.SendPacket(target.Character.GenerateStat());
                                });
                            }
                        }
                    }
                    if (target.CurrentMapInstance.MapInstanceType == MapInstanceType.IceBreakerInstance)
                    {
                        if (IceBreaker.AlreadyFrozenPlayers.Contains(target))
                        {
                            IceBreaker.AlreadyFrozenPlayers.Remove(target);
                            target.CurrentMapInstance?.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("ICEBREAKER_PLAYER_OUT"), target?.Character?.Name), 0));
                            target.Character.Hp = 1;
                            target.Character.Mp = 1;
                            RespawnMapTypeDTO respawn = target?.Character?.Respawn;
                            ServerManager.Instance.ChangeMap(target.Character.CharacterId, respawn.DefaultMapId);
                        }
                        else
                        {
                            IceBreaker.FrozenPlayers.Add(target);
                            target.CurrentMapInstance?.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("ICEBREAKER_PLAYER_FROZEN"), target?.Character?.Name), 0));
                            Task.Run(() =>
                            {
                                target.Character.Hp = (int)target.Character.HPLoad();
                                target.Character.Mp = (int)target.Character.MPLoad();
                                target.SendPacket(target?.Character?.GenerateStat());
                                target.Character.NoMove = true;
                                target.Character.NoAttack = true;
                                target.SendPacket(target?.Character?.GenerateCond());
                                while (IceBreaker.FrozenPlayers.Contains(target))
                                {

                                    target?.CurrentMapInstance?.Broadcast(
                                        StaticPacketHelper.GenerateEff(UserType.Player, target.Character.CharacterId, 35),
                                        target.Character.PositionX, target.Character.PositionY);
                                    Thread.Sleep(1000);
                                }
                            });
                        }
                    }
                    else if(ServerManager.Instance.ChannelId != 51)
                    {
                        hitRequest.Session.Character.TalentWin++;
                        target.Character.TalentLose++;
                        hitRequest.Session.CurrentMapInstance?.Broadcast(Session.Character.GenerateSay(
                            string.Format(Language.Instance.GetMessageFromKey("PVP_KILL"),
                                hitRequest.Session.Character.Name, target.Character.Name), 10));
                        Observable.Timer(TimeSpan.FromMilliseconds(1000)).Subscribe(o =>
                            ServerManager.Instance.AskPvpRevive(target.Character.CharacterId));
                    }
                }

                if (hitmode != 1)
                {
                    Buff buffswitch = null;
                    foreach ( BCard b in hitRequest.Skill.BCards.Where(s => s.Type.Equals((byte)CardType.Buff)).ToList())
                    {
                        buffswitch = new Buff((short)b.SecondData, Session.Character.SwitchLevel());
                        if (buffswitch.Card.BuffType == BuffType.Good)
                        {
                            b.ApplyBCards(Session.Character, Session.Character);
                        }
                        else
                        {

                            if (target.Character.GetBuff(CardType.TauntSkill, (byte)AdditionalTypes.TauntSkill.ReflectBadEffect)[0] > 0)
                            {
                                b.ApplyBCards(Session.Character, Session.Character);
                            }
                            else
                            {
                                b.ApplyBCards(target.Character, Session.Character);
                            }
                        }
                    }
                    hitRequest.Skill?.BCards.Where(s => s.Type.Equals((byte)CardType.SpecialActions)).ToList()
                        .ForEach(s => s.ApplyBCards(target.Character, Session.Character));
                    hitRequest.Skill?.BCards.Where(s => s.Type.Equals((byte)CardType.JumpBackPush)).ToList()
                        .ForEach(s => s.ApplyBCards(target.Character, Session.Character));
                    hitRequest.Skill?.BCards.Where(s => s.Type.Equals((byte)CardType.DrainAndSteal)).ToList()
                        .ForEach(s => s.ApplyBCards(target.Character, Session.Character));
                    hitRequest.Skill?.BCards.Where(s => s.Type.Equals((byte)CardType.SpecialisationBuffResistance)).ToList()
                        .ForEach(s => s.ApplyBCards(target.Character, Session.Character));
                    hitRequest.Skill?.BCards.Where(s => s.Type.Equals((byte)CardType.TauntSkill)).ToList()
                        .ForEach(s => s.ApplyBCards(target.Character, Session.Character));
                    hitRequest.Skill?.BCards.Where(s => s.Type.Equals((byte)CardType.TeleportEnemy)).ToList()
                        .ForEach(s => s.ApplyBCards(Session.Character, target.Character));


                    if (battleEntity?.ShellWeaponEffects != null)
                    {
                        foreach (ShellEffectDTO shell in battleEntity.ShellWeaponEffects)
                        {
                            switch (shell.Effect)
                            {
                                case (byte)ShellWeaponEffectType.Blackout:
                                    {
                                        Buff buff = new Buff(7, battleEntity.Level);
                                        double? antimalus = ((battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                      s.Effect == (byte)ShellArmorEffectType.ReducedStun)?.Value
                                                  + battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                      s.Effect == (byte)ShellArmorEffectType.ReducedAllStun)?.Value
                                                  + battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                          s.Effect == (byte)ShellArmorEffectType.ReducedAllNegativeEffect)
                                                      ?.Value) / 100D);
                                        if (ServerManager.RandomNumber() < shell.Value
                                            - (shell.Value
                                               * antimalus == null ? 0 : antimalus))
                                        {
                                            target.Character.AddBuff(buff);
                                        }

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.DeadlyBlackout:
                                    {
                                        Buff buff = new Buff(66, battleEntity.Level);
                                        double? antimalus = ((battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                      s.Effect == (byte)ShellArmorEffectType.ReducedAllStun)?.Value
                                                  + battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                          s.Effect == (byte)ShellArmorEffectType.ReducedAllNegativeEffect)
                                                      ?.Value) / 100D);
                                        if (ServerManager.RandomNumber() < shell.Value
                                            - (shell.Value
                                               * antimalus == null ? 0 : antimalus))
                                        {
                                            target.Character.AddBuff(buff);
                                        }

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.MinorBleeding:
                                    {
                                        Buff buff = new Buff(1, battleEntity.Level);
                                        double? antimalus = ((battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                                  s.Effect == (byte)ShellArmorEffectType
                                                                      .ReducedMinorBleeding)?.Value
                                                              + battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                                  s.Effect == (byte)ShellArmorEffectType
                                                                      .ReducedBleedingAndMinorBleeding)?.Value
                                                              + battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                                  s.Effect == (byte)ShellArmorEffectType
                                                                      .ReducedAllBleedingType)?.Value
                                                              + battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                                  s.Effect == (byte)ShellArmorEffectType
                                                                      .ReducedAllNegativeEffect)?.Value) / 100D);
                                        if (ServerManager.RandomNumber() < shell.Value
                                            - (shell.Value * antimalus == null ? 0 : antimalus))
                                        {
                                            target.Character.AddBuff(buff);
                                        }

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.Bleeding:
                                    {
                                        Buff buff = new Buff(21, battleEntity.Level);
                                        double? antimalus = ((battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                              s.Effect == (byte)ShellArmorEffectType
                                                                  .ReducedBleedingAndMinorBleeding)?.Value
                                                          + battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                              s.Effect == (byte)ShellArmorEffectType
                                                                  .ReducedAllBleedingType)?.Value
                                                          + battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                              s.Effect == (byte)ShellArmorEffectType
                                                                  .ReducedAllNegativeEffect)?.Value) / 100D);
                                        if (ServerManager.RandomNumber() < shell.Value - (shell.Value * antimalus == null ? 0 : antimalus))
                                        {
                                            target.Character.AddBuff(buff);
                                        }

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.HeavyBleeding:
                                    {
                                        Buff buff = new Buff(42, battleEntity.Level);
                                        double? antimalus = ((battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                              s.Effect == (byte)ShellArmorEffectType
                                                                  .ReducedAllBleedingType)?.Value
                                                          + battleEntityDefense.ShellArmorEffects?.Find(s =>
                                                              s.Effect == (byte)ShellArmorEffectType
                                                                  .ReducedAllNegativeEffect)?.Value) / 100D);
                                        if (ServerManager.RandomNumber() < shell.Value
                                            - (shell.Value * antimalus == null ? 0 : antimalus))
                                        {
                                            target.Character.AddBuff(buff);
                                        }

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.Freeze:
                                    {
                                        Buff buff = new Buff(27, battleEntity.Level);
                                        double? antimalus = ((battleEntityDefense.ShellArmorEffects?.Find(s => s.Effect ==
                                                                                                     (byte)
                                                                                                     ShellArmorEffectType
                                                                                                         .ReducedFreeze)
                                                                                             ?.Value
                                                                                         + battleEntityDefense
                                                                                             .ShellArmorEffects?.Find(
                                                                                                 s =>
                                                                                                     s.Effect ==
                                                                                                     (byte)
                                                                                                     ShellArmorEffectType
                                                                                                         .ReducedAllNegativeEffect)
                                                                                             ?.Value) / 100D);
                                        if (ServerManager.RandomNumber() < shell.Value - (shell.Value * antimalus == null ? 0 : antimalus))
                                        {
                                            target.Character.AddBuff(buff);
                                        }

                                        break;
                                    }
                            }
                        }
                    }
                }

                switch (hitRequest.TargetHitType)
                {
                    case TargetHitType.SingleTargetHit:
                        hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                            hitRequest.Session.Character.CharacterId, 1, target.Character.CharacterId,
                            hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown, hitRequest.Skill.AttackAnimation,
                            hitRequest.SkillEffect, hitRequest.Session.Character.PositionX,
                            hitRequest.Session.Character.PositionY, isAlive,
                            (int) (target.Character.Hp / (float) target.Character.HPLoad() * 100), damage, hitmode,
                            (byte) (hitRequest.Skill.SkillType - 1)));
                        break;

                    case TargetHitType.SingleTargetHitCombo:
                        hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                            hitRequest.Session.Character.CharacterId, 1, target.Character.CharacterId,
                            hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown, hitRequest.SkillCombo.Animation,
                            hitRequest.SkillCombo.Effect, hitRequest.Session.Character.PositionX,
                            hitRequest.Session.Character.PositionY, isAlive,
                            (int) (target.Character.Hp / (float) target.Character.HPLoad() * 100), damage, hitmode,
                            (byte) (hitRequest.Skill.SkillType - 1)));
                        break;

                    case TargetHitType.SingleAOETargetHit:
                        switch (hitmode)
                        {
                            case 1:
                                hitmode = 4;
                                break;

                            case 3:
                                hitmode = 6;
                                break;

                            default:
                                hitmode = 5;
                                break;
                        }

                        if (hitRequest.ShowTargetHitAnimation)
                        {
                            hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(
                                UserType.Player, hitRequest.Session.Character.CharacterId, 1,
                                target.Character.CharacterId, hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown,
                                hitRequest.Skill.AttackAnimation, hitRequest.SkillEffect, 0, 0, isAlive,
                                (int) (target.Character.Hp / (float) target.Character.HPLoad() * 100), 0, 0,
                                (byte) (hitRequest.Skill.SkillType - 1)));
                        }

                        hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                            hitRequest.Session.Character.CharacterId, 1, target.Character.CharacterId,
                            hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown, hitRequest.Skill.AttackAnimation,
                            hitRequest.SkillEffect, hitRequest.Session.Character.PositionX,
                            hitRequest.Session.Character.PositionY, isAlive,
                            (int) (target.Character.Hp / (float) target.Character.HPLoad() * 100), damage, hitmode,
                            (byte) (hitRequest.Skill.SkillType - 1)));
                        break;

                    case TargetHitType.AOETargetHit:
                        switch (hitmode)
                        {
                            case 1:
                                hitmode = 4;
                                break;

                            case 3:
                                hitmode = 6;
                                break;

                            default:
                                hitmode = 5;
                                break;
                        }

                        hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                            hitRequest.Session.Character.CharacterId, 1, target.Character.CharacterId,
                            hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown, hitRequest.Skill.AttackAnimation,
                            hitRequest.SkillEffect, hitRequest.Session.Character.PositionX,
                            hitRequest.Session.Character.PositionY, isAlive,
                            (int) (target.Character.Hp / (float) target.Character.HPLoad() * 100), damage, hitmode,
                            (byte) (hitRequest.Skill.SkillType - 1)));
                        break;

                    case TargetHitType.ZoneHit:
                        hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                            hitRequest.Session.Character.CharacterId, 1, target.Character.CharacterId,
                            hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown, hitRequest.Skill.AttackAnimation,
                            hitRequest.SkillEffect, hitRequest.MapX, hitRequest.MapY, isAlive,
                            (int) (target.Character.Hp / (float) target.Character.HPLoad() * 100), damage, 5,
                            (byte) (hitRequest.Skill.SkillType - 1)));
                        break;

                    case TargetHitType.SpecialZoneHit:
                        hitRequest.Session.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                            hitRequest.Session.Character.CharacterId, 1, target.Character.CharacterId,
                            hitRequest.Skill.SkillVNum, hitRequest.Skill.Cooldown, hitRequest.Skill.AttackAnimation,
                            hitRequest.SkillEffect, hitRequest.Session.Character.PositionX,
                            hitRequest.Session.Character.PositionY, isAlive,
                            (int) (target.Character.Hp / target.Character.HPLoad() * 100), damage, 0,
                            (byte) (hitRequest.Skill.SkillType - 1)));
                        break;

                    default:
                        Logger.Warn("Not Implemented TargetHitType Handling!");
                        break;
                }
            }
            else
            {
                // monster already has been killed, send cancel
                if (target != null)
                {
                    hitRequest?.Session.SendPacket(StaticPacketHelper.Cancel(2, target.Character.CharacterId));
                }
            }
        }

        private void TargetHit(int castingId, int targetId, bool isPvp = false)
        {
            bool shouldCancel = true;
            if ((DateTime.Now - Session.Character.LastTransform).TotalSeconds < 3)
            {
                Session.SendPacket(StaticPacketHelper.Cancel());
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CANT_ATTACK"),
                    0));
                return;
            }

            List<CharacterSkill> skills = Session.Character.UseSp
                ? Session.Character.SkillsSp?.GetAllItems()
                : Session.Character.Skills?.GetAllItems();

            if (Session.Character.Mates != null)
            {
                Mate m = null;
                m = Session.Character.Mates?.Where(s => s.MateType == MateType.Partner && s.IsTeamMember).FirstOrDefault();
                if (m != null)
                {
                    NpcMonster mateNpc = ServerManager.GetNpc(m.NpcMonsterVNum);
                    if (mateNpc.Skills != null)
                    {
                        foreach (NpcMonsterSkill skill in mateNpc.Skills)
                        {
                            CharacterSkill characterSkillMate = new CharacterSkill { SkillVNum = skill.SkillVNum, CharacterId = Session.Character.CharacterId };
                            skills.Add(characterSkillMate);
                        }
                    }
                }
            }

            if (skills != null)
            {
                CharacterSkill
                    ski = skills.Find(s =>
                        s.Skill?.CastId
                        == castingId); // && (s.Skill?.UpgradeSkill == 0 || s.Skill?.UpgradeSkill == 3));
                if (castingId != 0)
                {
                    Session.SendPacket("ms_c 0");
                }

                if (ski != null)
                {
                    if (!Session.Character.WeaponLoaded(ski) || !ski.CanBeUsed(Session))
                    {
                        Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                        return;
                    }

                    if (ski.Skill.Type == 1 && Session.Character.RangedDisable == true)
                    {
                        Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                        return;
                    }
                    
                    if (ski.Skill.CastId != 0 && Session.Character.OnlyNormalAttacks == true)
                    {
                        Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                        return;
                    }

                    double CooldownReduce = (short)Session.Character.GetBuff(CardType.Morale, (byte)AdditionalTypes.Morale.SkillCooldownDecreased)[0];
                    short Cooldown = ski.Skill.Cooldown;

                    if (CooldownReduce != 0)
                    {
                        CooldownReduce = (Cooldown * (CooldownReduce / 100D));
                        Cooldown += (short)CooldownReduce;
                        if(Cooldown <= 0)
                        {
                            Cooldown = 1;
                        }
                    }

                    foreach (BCard bc in ski.Skill.BCards.Where(s => s.Type.Equals((byte) CardType.MeditationSkill)))
                    {
                        shouldCancel = false;
                        bc.ApplyBCards(Session.Character);
                    }

                    int MpCost = ski.Skill.MpCost;
                    double riduzionemp = 0;
                    riduzionemp += ((Session.Character.CellonOptions.Where(s => s.Type == CellonOptionType.MPUsage).Sum(s => s.Value)) / 100D);
                    riduzionemp -= ((Session.Character.GetBuff(CardType.Casting, (byte)AdditionalTypes.Casting.ManaForSkillsDecreased)[0]) / 100D);
                    riduzionemp = 1 - riduzionemp;
                    if(riduzionemp < 0)
                    {
                        riduzionemp = 0;
                    }
                    MpCost = (int)(MpCost * riduzionemp); 
                    
                    if (Session.Character.Mp >= MpCost && Session.HasCurrentMapInstance)
                    {

                        int[] VelenoMentale = Session.Character.GetBuff(CardType.HealingBurningAndCasting,
                            (byte)AdditionalTypes.HealingBurningAndCasting.HPDecreasedByConsumingMP);

                        if (!Session.Character.HasGodMode)
                        {
                            if(VelenoMentale[0] < 0)
                            {
                                Session.Character.Hp = (int)(Session.Character.Hp + (MpCost * VelenoMentale[0] / 100D));
                                if(Session.Character.Hp <= 0)
                                {
                                    Session.Character.Hp = 1;
                                }
                                Session.CurrentMapInstance?.Broadcast($"dm 1 {Session.Character.CharacterId} {MpCost}");
                            }
                            Session.Character.Mp -= MpCost;
                            Session.SendPacket(Session.Character.GenerateStat());
                        }


                        // AOE Target hit
                        if (ski.Skill.TargetType == 1 && ski.Skill.HitType == 1)
                        {

                            if (Session.Character.UseSp && ski.Skill.CastEffect != -1)
                            {
                                Session.SendPackets(Session.Character.GenerateQuicklist());
                            }

                            Session.SendPacket(Session.Character.GenerateStat());
                            CharacterSkill skillinfo = Session.Character.Skills.FirstOrDefault(s =>
                                s.Skill.UpgradeSkill == ski.Skill.SkillVNum && s.Skill.Effect > 0
                                                                            && s.Skill.SkillType == 2);
                            Session.CurrentMapInstance.Broadcast(StaticPacketHelper.CastOnTarget(UserType.Player,
                                Session.Character.CharacterId, 1, Session.Character.CharacterId,
                                ski.Skill.CastAnimation, skillinfo?.Skill.CastEffect ?? ski.Skill.CastEffect,
                                ski.Skill.SkillVNum));

                            // Generate scp
                            ski.LastUse = DateTime.Now;
                            if (ski.Skill.CastEffect != 0)
                            {
                                int CastTime = ski.Skill.CastTime * 100;
                                double riduzione = 0;
                                double riduzionevera = 0;
                                if (ski.Skill.Type == 0)
                                {
                                    if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] != 0)
                                    {
                                        riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                        riduzionevera = CastTime * riduzione;
                                    }
                                }
                                if (ski.Skill.Type == 1)
                                {
                                    if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.RangedDurationIncreased)[0] != 0)
                                    {
                                        riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                        riduzionevera = CastTime * riduzione;
                                    }
                                }
                                if (ski.Skill.Type == 2)
                                {
                                    if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MagicalDurationIncreased)[0] != 0)
                                    {
                                        riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                        riduzionevera = CastTime * riduzione;
                                    }
                                }
                                CastTime += (int)riduzionevera;
                                Thread.Sleep(CastTime);
                            }

                            Session.CurrentMapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                                Session.Character.CharacterId, 1, Session.Character.CharacterId, ski.Skill.SkillVNum,
                                Cooldown, ski.Skill.AttackAnimation,
                                skillinfo?.Skill.Effect ?? ski.Skill.Effect, Session.Character.PositionX,
                                Session.Character.PositionY, true,
                                (int)(Session.Character.Hp / Session.Character.HPLoad() * 100), 0, -2,
                                (byte)(ski.Skill.SkillType - 1)));
                            if (ski.Skill.TargetRange != 0)
                            {
                                foreach (ClientSession character in ServerManager.Instance.Sessions.Where(s =>
                                    s.CurrentMapInstance == Session.CurrentMapInstance
                                    && s.Character.CharacterId != Session.Character.CharacterId
                                    && s.Character.IsInRange(Session.Character.PositionX, Session.Character.PositionY,
                                        ski.Skill.TargetRange)))
                                {
                                    if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                        s.MapTypeId == (short)MapTypeEnum.Act4))
                                    {
                                        if (Session.Character.Faction != character.Character.Faction
                                            && Session.CurrentMapInstance.Map.MapId != 130
                                            && Session.CurrentMapInstance.Map.MapId != 131)
                                        {
                                            PvpHit(new HitRequest(TargetHitType.AOETargetHit, Session, ski.Skill),
                                                character);
                                        }
                                    }
                                    else if (Session.CurrentMapInstance.Map.MapId == 2106)
                                    {

                                        if (Session.Character.Family == null)
                                        {
                                            if (character.Character.Family != null)
                                            {
                                                PvpHit(new HitRequest(TargetHitType.AOETargetHit, Session, ski.Skill),
                                                    character);
                                            }
                                        }
                                        else
                                        {
                                            if (character.Character.Family == null)
                                            {
                                                PvpHit(new HitRequest(TargetHitType.AOETargetHit, Session, ski.Skill),
                                                    character);
                                            }
                                            else if (Session.Character.Family != character.Character.Family)
                                            {
                                                PvpHit(new HitRequest(TargetHitType.AOETargetHit, Session, ski.Skill),
                                                    character);
                                            }
                                        }
                                    }
                                    else if (Session.CurrentMapInstance.Map.MapTypes.Any(m =>
                                        m.MapTypeId == (short)MapTypeEnum.PVPMap))
                                    {
                                        if (Session.Character.Group == null
                                            || !Session.Character.Group.IsMemberOfGroup(character.Character.CharacterId)
                                        )
                                        {
                                            PvpHit(new HitRequest(TargetHitType.AOETargetHit, Session, ski.Skill),
                                                character);
                                        }
                                    }
                                    else if (Session.CurrentMapInstance.IsPVP)
                                    {
                                        if (Session.Character.Group == null
                                            || !Session.Character.Group.IsMemberOfGroup(character.Character.CharacterId)
                                        )
                                        {
                                            PvpHit(new HitRequest(TargetHitType.AOETargetHit, Session, ski.Skill),
                                                character);
                                        }
                                    }
                                    else
                                    {
                                        Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                    }
                                }

                                foreach (MapMonster mon in Session.CurrentMapInstance
                                    .GetListMonsterInRange(Session.Character.PositionX, Session.Character.PositionY,
                                        ski.Skill.TargetRange).Where(s => s.CurrentHp > 0))
                                {
                                    mon.HitQueue.Enqueue(new HitRequest(TargetHitType.AOETargetHit, Session, ski.Skill,
                                        skillinfo?.Skill.Effect ?? ski.Skill.Effect));
                                }

                                ski.Skill?.BCards.Where(s => s.Type.Equals((byte)CardType.SpecialEffects2)).ToList()
                                    .ForEach(s => s.ApplyBCards(Session.Character));
                            }
                        }
                        else if (ski.Skill.TargetType == 2 && ski.Skill.HitType == 0)
                        {
                            Session.CurrentMapInstance.Broadcast(StaticPacketHelper.CastOnTarget(UserType.Player,
                                Session.Character.CharacterId, 1, Session.Character.CharacterId,
                                ski.Skill.CastAnimation, ski.Skill.CastEffect, ski.Skill.SkillVNum));
                            ClientSession target = ServerManager.Instance.GetSessionByCharacterId(targetId) ?? Session;
                            if (Session.CurrentMapInstance.IsPVP)
                            {
                                if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                           s.MapTypeId == (short)MapTypeEnum.Act4))
                                {
                                    if (target.Character.Faction != Session.Character.Faction)
                                    {
                                        target = Session;
                                    }
                                }
                                else if (Session.CurrentMapInstance.Map.MapId == 2106)
                                {

                                    if (Session.Character.Family == null)
                                    {
                                        if (target.Character.Family != null)
                                        {
                                            target = Session;
                                        }
                                    }
                                    else
                                    {
                                        if (target.Character.Family == null)
                                        {
                                            target = Session;
                                        }
                                        else if (Session.Character.Family != target.Character.Family)
                                        {
                                            target = Session;
                                        }
                                    }
                                }
                                else if (Session.Character.Group != null)
                                {
                                    if (Session.Character.Group.Characters.Where(s => s.Character.CharacterId == target.Character.CharacterId).FirstOrDefault() == null)
                                    {
                                        target = Session;
                                    }
                                }
                                else
                                {
                                    target = Session;
                                }
                            }
                            Session.CurrentMapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
    Session.Character.CharacterId, 1, target.Character.CharacterId, ski.Skill.SkillVNum, Cooldown,
    ski.Skill.AttackAnimation, ski.Skill.Effect, Session.Character.PositionX,
    Session.Character.PositionY, true,
    (int)(Session.Character.Hp / Session.Character.HPLoad() * 100), 0, -1,
    (byte)(ski.Skill.SkillType - 1)));
                            ski.Skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill)).ToList()
                                .ForEach(s => s.ApplyBCards(target?.Character, Session.Character));
                        }
                        else if (ski.Skill.TargetType == 1 && ski.Skill.HitType != 1)
                        {
                            Session.CurrentMapInstance.Broadcast(StaticPacketHelper.CastOnTarget(UserType.Player,
                                Session.Character.CharacterId, 1, Session.Character.CharacterId,
                                ski.Skill.CastAnimation, ski.Skill.CastEffect, ski.Skill.SkillVNum));
                            Session.CurrentMapInstance.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player,
                                Session.Character.CharacterId, 1, Session.Character.CharacterId, ski.Skill.SkillVNum,
                                Cooldown, ski.Skill.AttackAnimation, ski.Skill.Effect,
                                Session.Character.PositionX, Session.Character.PositionY, true,
                                (int)(Session.Character.Hp / Session.Character.HPLoad() * 100), 0, -1,
                                (byte)(ski.Skill.SkillType - 1)));
                            int HitType = ski.Skill.HitType;

                            if (Session.CurrentMapInstance.IsPVP)
                            {
                                if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                        s.MapTypeId == (short)MapTypeEnum.Act4))
                                {
                                    HitType = 6;
                                }
                                else if (Session.CurrentMapInstance.Map.MapId == 2106)
                                {

                                    if (Session.Character.Family == null)
                                    {
                                        HitType = 8;
                                    }
                                    else
                                    {
                                        HitType = 7;
                                    }
                                }
                                else if (Session.Character.Group != null)
                                {
                                    HitType = 5;
                                }
                                else
                                {
                                    HitType = 0;
                                }
                            }
                            switch (HitType)
                            {

                                case 8:
                                    IEnumerable<ClientSession> clientSessionsguildnofamily =
                                        Session.CurrentMapInstance.Sessions?.Where(s =>
                                            s.Character.IsInRange(Session.Character.PositionX,
                                                Session.Character.PositionY, ski.Skill.TargetRange) && s.Character.Family == null);
                                    if (clientSessionsguildnofamily != null)
                                    {
                                        foreach (ClientSession target in clientSessionsguildnofamily)
                                        {
                                            ski.Skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                                .ToList().ForEach(s =>
                                                    s.ApplyBCards(target.Character, Session.Character));
                                        }
                                        IEnumerable<Mate> matesguildnofamily = clientSessionsguildnofamily.SelectMany(x => x.Character.Mates).Where(s =>
                                            s.Owner != null && s.IsInRange(Session.Character.PositionX,
                                                Session.Character.PositionY, ski.Skill.TargetRange) && s.Owner?.Family == null);
                                        if (matesguildnofamily != null)
                                        {
                                            foreach (Mate target in matesguildnofamily)
                                            {
                                                ski.Skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                                    .ToList().ForEach(s =>
                                                        s.ApplyBCards(target, Session.Character));
                                            }
                                        }
                                    }
                                    break;
                                case 7:
                                    IEnumerable<ClientSession> clientSessionsguild =
                                        Session.CurrentMapInstance.Sessions?.Where(s =>
                                            s.Character.IsInRange(Session.Character.PositionX,
                                                Session.Character.PositionY, ski.Skill.TargetRange) && s.Character.Family == Session.Character.Family);
                                    if (clientSessionsguild != null)
                                    {
                                        foreach (ClientSession target in clientSessionsguild)
                                        {
                                            ski.Skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                                .ToList().ForEach(s =>
                                                    s.ApplyBCards(target.Character, Session.Character));
                                        }

                                        IEnumerable<Mate> matesguild =
                                            clientSessionsguild.SelectMany(x => x.Character.Mates).Where(s =>
                                            s.Owner != null && s.IsInRange(Session.Character.PositionX,
                                        Session.Character.PositionY, ski.Skill.TargetRange) && s.Owner?.Family == null);
                                        if (matesguild != null)
                                        {
                                            foreach (Mate target in matesguild)
                                            {
                                                ski.Skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                                    .ToList().ForEach(s =>
                                                        s.ApplyBCards(target, Session.Character));
                                            }
                                        }
                                    }
                                    break;
                                case 6:
                                    IEnumerable<ClientSession> clientSessionsfaction =
                                        Session.CurrentMapInstance.Sessions?.Where(s =>
                                            s.Character.IsInRange(Session.Character.PositionX,
                                                Session.Character.PositionY, ski.Skill.TargetRange) && s.Character.Faction == Session.Character.Faction);
                                    if (clientSessionsfaction != null)
                                    {
                                        foreach (ClientSession target in clientSessionsfaction)
                                        {
                                            ski.Skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                                .ToList().ForEach(s =>
                                                    s.ApplyBCards(target.Character, Session.Character));
                                        }

                                        IEnumerable<Mate> matesfaction =
                                            clientSessionsfaction.SelectMany(x => x.Character.Mates).Where(s =>
                                            s.Owner != null && s.IsInRange(Session.Character.PositionX,
                                        Session.Character.PositionY, ski.Skill.TargetRange) && s.Owner?.Faction == Session.Character.Faction);
                                        if (matesfaction != null)
                                        {
                                            foreach (Mate target in matesfaction)
                                            {
                                                ski.Skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                                    .ToList().ForEach(s =>
                                                        s.ApplyBCards(target, Session.Character));
                                            }
                                        }
                                    }
                                    break;
                                case 5:
                                    IEnumerable<ClientSession> clientSessionsgroup =
                                        Session.CurrentMapInstance.Sessions?.Where(s =>
                                            s.Character.IsInRange(Session.Character.PositionX,
                                                Session.Character.PositionY, ski.Skill.TargetRange) && s.Character.Group?.GroupId == Session.Character.Group.GroupId);
                                    if (clientSessionsgroup != null)
                                    {
                                        foreach (ClientSession target in clientSessionsgroup)
                                        {
                                            ski.Skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                                .ToList().ForEach(s =>
                                                    s.ApplyBCards(target.Character, Session.Character));
                                        }

                                        IEnumerable<Mate> matesgroup =
                                            clientSessionsgroup.SelectMany(x => x.Character.Mates).Where(s =>
                                            s.Owner != null && s.IsInRange(Session.Character.PositionX,
                                        Session.Character.PositionY, ski.Skill.TargetRange) && s.Owner?.Group.GroupId == Session.Character.Group.GroupId);
                                        if (matesgroup != null)
                                        {
                                            foreach (Mate target in matesgroup)
                                            {
                                                ski.Skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                                    .ToList().ForEach(s =>
                                                        s.ApplyBCards(target, Session.Character));
                                            }
                                        }
                                    }
                                    break;
                                case 2:
                                    IEnumerable<ClientSession> clientSessions =
                                        Session.CurrentMapInstance.Sessions?.Where(s =>
                                            s.Character.IsInRange(Session.Character.PositionX,
                                                Session.Character.PositionY, ski.Skill.TargetRange));
                                    if (clientSessions != null)
                                    {
                                        foreach (ClientSession target in clientSessions)
                                        {
                                            ski.Skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                                .ToList().ForEach(s =>
                                                    s.ApplyBCards(target.Character, Session.Character));
                                        }

                                        IEnumerable<Mate> mates =
                                            clientSessions.SelectMany(x => x.Character.Mates).Where(s =>
                                            s.Owner != null && s.IsInRange(Session.Character.PositionX,
                                        Session.Character.PositionY, ski.Skill.TargetRange));
                                        if (mates != null)
                                        {
                                            foreach (Mate target in mates)
                                            {
                                                ski.Skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                                    .ToList().ForEach(s =>
                                                        s.ApplyBCards(target, Session.Character));
                                            }
                                        }
                                    }

                                    break;

                                case 4:
                                case 0:
                                    ski.Skill.BCards.Where(s => !s.Type.Equals((byte)CardType.MeditationSkill))
                                        .ToList().ForEach(s => s.ApplyBCards(Session.Character));
                                    break;
                            }
                        }
                        else if (ski.Skill.TargetType == 0) // monster target
                        {
                            if (isPvp)
                            {
                                ClientSession playerToAttack = ServerManager.Instance.GetSessionByCharacterId(targetId);
                                if (playerToAttack != null && Session.Character.Mp >= MpCost)
                                {

                                    int Range = ski.Skill.Range;
                                    if (Session.Character.GetBuff(CardType.FearSkill, (byte)AdditionalTypes.FearSkill.AttackRangedIncreased)[0] > 0)
                                    {
                                        Range += Session.Character.GetBuff(CardType.FearSkill, (byte)AdditionalTypes.FearSkill.AttackRangedIncreased)[0];
                                    }
                                    if (Map.GetDistance(
                                            new MapCell
                                            {
                                                X = Session.Character.PositionX,
                                                Y = Session.Character.PositionY
                                            },
                                            new MapCell
                                            {
                                                X = playerToAttack.Character.PositionX,
                                                Y = playerToAttack.Character.PositionY
                                            }) <= Range + 5)
                                    {

                                        if (Session.Character.UseSp && ski.Skill.CastEffect != -1)
                                        {
                                            Session.SendPackets(Session.Character.GenerateQuicklist());
                                        }

                                        Session.SendPacket(Session.Character.GenerateStat());
                                        CharacterSkill characterSkillInfo = Session.Character.Skills.FirstOrDefault(s =>
                                            s.Skill.UpgradeSkill == ski.Skill.SkillVNum && s.Skill.Effect > 0
                                                                                        && s.Skill.SkillType == 2);
                                        Session.CurrentMapInstance.Broadcast(
                                            StaticPacketHelper.CastOnTarget(UserType.Player,
                                                Session.Character.CharacterId, 3, targetId, ski.Skill.CastAnimation,
                                                characterSkillInfo?.Skill.CastEffect ?? ski.Skill.CastEffect,
                                                ski.Skill.SkillVNum));
                                        Session.Character.Skills.Where(s => s.Id != ski.Id).ForEach(i => i.Hit = 0);

                                        // Generate scp
                                        if ((DateTime.Now - ski.LastUse).TotalSeconds > 3)
                                        {
                                            ski.Hit = 0;
                                        }
                                        else
                                        {
                                            ski.Hit++;
                                        }

                                        ski.LastUse = DateTime.Now;

                                        if (ski.Skill.CastEffect != 0)
                                        {
                                            int CastTime = ski.Skill.CastTime * 100;
                                            double riduzione = 0;
                                            double riduzionevera = 0;
                                            if (ski.Skill.Type == 0)
                                            {
                                                if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] != 0)
                                                {
                                                    riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                                    riduzionevera = CastTime * riduzione;
                                                }
                                            }
                                            if (ski.Skill.Type == 1)
                                            {
                                                if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.RangedDurationIncreased)[0] != 0)
                                                {
                                                    riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                                    riduzionevera = CastTime * riduzione;
                                                }
                                            }
                                            if (ski.Skill.Type == 2)
                                            {
                                                if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MagicalDurationIncreased)[0] != 0)
                                                {
                                                    riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                                    riduzionevera = CastTime * riduzione;
                                                }
                                            }
                                            CastTime += (int)riduzionevera;
                                            Thread.Sleep(CastTime);
                                        }

                                        if (ski.Skill.HitType == 3)
                                        {
                                            int count = 0;
                                            if (playerToAttack.CurrentMapInstance == Session.CurrentMapInstance
                                                && playerToAttack.Character.CharacterId !=
                                                Session.Character.CharacterId)
                                            {
                                                if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                    s.MapTypeId == (short)MapTypeEnum.Act4))
                                                {
                                                    if (Session.Character.Faction != playerToAttack.Character.Faction
                                                        && Session.CurrentMapInstance.Map.MapId != 130
                                                        && Session.CurrentMapInstance.Map.MapId != 131)
                                                    {
                                                        count++;
                                                        PvpHit(
                                                            new HitRequest(TargetHitType.SingleAOETargetHit, Session,
                                                                ski.Skill), playerToAttack);
                                                    }
                                                }
                                                else if (Session.CurrentMapInstance.Map.MapId == 2106)
                                                {

                                                    if (Session.Character.Family == null)
                                                    {
                                                        if (playerToAttack.Character.Family != null)
                                                        {
                                                            count++;
                                                            PvpHit(
                                                                new HitRequest(TargetHitType.SingleAOETargetHit, Session,
                                                                    ski.Skill), playerToAttack);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (playerToAttack.Character.Family == null)
                                                        {
                                                            count++;
                                                            PvpHit(
                                                                new HitRequest(TargetHitType.SingleAOETargetHit, Session,
                                                                    ski.Skill), playerToAttack);
                                                        }
                                                        else if (Session.Character.Family != playerToAttack.Character.Family)
                                                        {
                                                            count++;
                                                            PvpHit(
                                                                new HitRequest(TargetHitType.SingleAOETargetHit, Session,
                                                                    ski.Skill), playerToAttack);
                                                        }
                                                    }
                                                }
                                                else if (Session.CurrentMapInstance.Map.MapTypes.Any(m =>
                                                    m.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                {
                                                    if (Session.Character.Group == null
                                                        || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                            .Character.CharacterId))
                                                    {
                                                        count++;
                                                        PvpHit(
                                                            new HitRequest(TargetHitType.SingleAOETargetHit, Session,
                                                                ski.Skill), playerToAttack);
                                                    }
                                                }
                                                else if (Session.CurrentMapInstance.IsPVP)
                                                {
                                                    if (Session.Character.Group == null
                                                        || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                            .Character.CharacterId))
                                                    {
                                                        count++;
                                                        PvpHit(
                                                            new HitRequest(TargetHitType.SingleAOETargetHit, Session,
                                                                ski.Skill), playerToAttack);
                                                    }
                                                }
                                            }

                                            foreach (long id in Session.Character.MTListTargetQueue
                                                .Where(s => s.EntityType == UserType.Player).Select(s => s.TargetId))
                                            {
                                                ClientSession character =
                                                    ServerManager.Instance.GetSessionByCharacterId(id);
                                                if (character != null
                                                    && character.CurrentMapInstance == Session.CurrentMapInstance
                                                    && character.Character.CharacterId != Session.Character.CharacterId)
                                                {
                                                    if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                        s.MapTypeId == (short)MapTypeEnum.Act4))
                                                    {
                                                        if (Session.Character.Faction != character.Character.Faction
                                                            && Session.CurrentMapInstance.Map.MapId != 130
                                                            && Session.CurrentMapInstance.Map.MapId != 131)
                                                        {
                                                            count++;
                                                            PvpHit(
                                                                new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                    Session, ski.Skill), character);
                                                        }
                                                    }
                                                    else if (Session.CurrentMapInstance.Map.MapId == 2106)
                                                    {
                                                        if (Session.Character.Family == null)
                                                        {
                                                            if (playerToAttack.Character.Family != null)
                                                            {
                                                                count++;
                                                                PvpHit(
                                                                    new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                        Session, ski.Skill), character);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (playerToAttack.Character.Family == null)
                                                            {
                                                                count++;
                                                                PvpHit(
                                                                    new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                        Session, ski.Skill), character);
                                                            }
                                                            else if (Session.Character.Family != playerToAttack.Character.Family)
                                                            {
                                                                count++;
                                                                PvpHit(
                                                                    new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                        Session, ski.Skill), character);
                                                            }
                                                        }
                                                    }
                                                    else if (Session.CurrentMapInstance.Map.MapTypes.Any(m =>
                                                        m.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                    {
                                                        if (Session.Character.Group == null
                                                            || !Session.Character.Group.IsMemberOfGroup(character
                                                                .Character
                                                                .CharacterId))
                                                        {
                                                            count++;
                                                            PvpHit(
                                                                new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                    Session, ski.Skill), character);
                                                        }
                                                    }
                                                    else if (Session.CurrentMapInstance.IsPVP)
                                                    {
                                                        if (Session.Character.Group == null
                                                            || !Session.Character.Group.IsMemberOfGroup(character
                                                                .Character
                                                                .CharacterId))
                                                        {
                                                            count++;
                                                            PvpHit(
                                                                new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                    Session, ski.Skill), character);
                                                        }
                                                    }
                                                }
                                            }

                                            if (count == 0)
                                            {
                                                Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                            }
                                        }
                                        else
                                        {
                                            try
                                            {
                                                // check if we will hit mutltiple targets
                                                if (ski.Skill.TargetRange != 0)
                                                {
                                                    ComboDTO skillCombo = ski.Skill.Combos.Find(s => ski.Hit == s.Hit);
                                                    if (skillCombo != null)
                                                    {
                                                        if (ski.Skill.Combos.OrderByDescending(s => s.Hit).First().Hit
                                                            == ski.Hit)
                                                        {
                                                            ski.Hit = 0;
                                                        }

                                                        IEnumerable<ClientSession> playersInAoeRange =
                                                            ServerManager.Instance.Sessions.Where(s =>
                                                                s.CurrentMapInstance == Session.CurrentMapInstance
                                                                && s.Character.CharacterId != Session.Character.CharacterId
                                                                && s.Character.IsInRange(Session.Character.PositionX,
                                                                    Session.Character.PositionY, ski.Skill.TargetRange));
                                                        int count = 0;
                                                        if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                            s.MapTypeId == (short)MapTypeEnum.Act4))
                                                        {
                                                            if (Session.Character.Faction
                                                                != playerToAttack.Character.Faction
                                                                && Session.CurrentMapInstance.Map.MapId != 130
                                                                && Session.CurrentMapInstance.Map.MapId != 131)
                                                            {
                                                                count++;
                                                                PvpHit(
                                                                    new HitRequest(TargetHitType.SingleTargetHitCombo,
                                                                        Session, ski.Skill, skillCombo: skillCombo),
                                                                    playerToAttack);
                                                            }
                                                        }
                                                        else if (Session.CurrentMapInstance.Map.MapTypes.Any(m =>
                                                            m.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                        {
                                                            if (Session.Character.Group == null
                                                                || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                                    .Character.CharacterId))
                                                            {
                                                                count++;
                                                                PvpHit(
                                                                    new HitRequest(TargetHitType.SingleTargetHitCombo,
                                                                        Session, ski.Skill, skillCombo: skillCombo),
                                                                    playerToAttack);
                                                            }
                                                        }
                                                        else if (Session.CurrentMapInstance.IsPVP)
                                                        {
                                                            if (Session.Character.Group == null
                                                                || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                                    .Character.CharacterId))
                                                            {
                                                                count++;
                                                                PvpHit(
                                                                    new HitRequest(TargetHitType.SingleTargetHitCombo,
                                                                        Session, ski.Skill, skillCombo: skillCombo),
                                                                    playerToAttack);
                                                            }
                                                        }

                                                        foreach (ClientSession character in playersInAoeRange)
                                                        {
                                                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                                s.MapTypeId == (short)MapTypeEnum.Act4))
                                                            {
                                                                if (Session.Character.Faction
                                                                    != character.Character.Faction
                                                                    && Session.CurrentMapInstance.Map.MapId != 130
                                                                    && Session.CurrentMapInstance.Map.MapId != 131)
                                                                {
                                                                    count++;
                                                                    PvpHit(
                                                                        new HitRequest(TargetHitType.SingleTargetHitCombo,
                                                                            Session, ski.Skill, skillCombo: skillCombo),
                                                                        character);
                                                                }
                                                            }
                                                            else if (Session.CurrentMapInstance.Map.MapTypes.Any(m =>
                                                                m.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                            {
                                                                if (Session.Character.Group == null
                                                                    || !Session.Character.Group.IsMemberOfGroup(
                                                                        character.Character.CharacterId))
                                                                {
                                                                    count++;
                                                                    PvpHit(
                                                                        new HitRequest(TargetHitType.SingleTargetHitCombo,
                                                                            Session, ski.Skill, skillCombo: skillCombo),
                                                                        character);
                                                                }
                                                            }
                                                            else if (Session.CurrentMapInstance.IsPVP)
                                                            {
                                                                if (Session.Character.Group == null
                                                                    || !Session.Character.Group.IsMemberOfGroup(
                                                                        character.Character.CharacterId))
                                                                {
                                                                    count++;
                                                                    PvpHit(
                                                                        new HitRequest(TargetHitType.SingleTargetHitCombo,
                                                                            Session, ski.Skill, skillCombo: skillCombo),
                                                                        character);
                                                                }
                                                            }
                                                        }

                                                        if (playerToAttack.Character.Hp <= 0 || count == 0)
                                                        {
                                                            Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        IEnumerable<ClientSession> playersInAoeRange =
                                                            ServerManager.Instance.Sessions.Where(s =>
                                                                s.CurrentMapInstance == Session.CurrentMapInstance
                                                                && s.Character.CharacterId != Session.Character.CharacterId
                                                                && s.Character.IsInRange(Session.Character.PositionX,
                                                                    Session.Character.PositionY, ski.Skill.TargetRange));

                                                        // hit the targetted monster
                                                        if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                            s.MapTypeId == (short)MapTypeEnum.Act4))
                                                        {
                                                            if (Session.Character.Faction
                                                                != playerToAttack.Character.Faction)
                                                            {
                                                                if (Session.CurrentMapInstance.Map.MapId != 130
                                                                    && Session.CurrentMapInstance.Map.MapId != 131)
                                                                {
                                                                    PvpHit(
                                                                        new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                            Session, ski.Skill), playerToAttack);
                                                                }
                                                                else
                                                                {
                                                                    Session.SendPacket(
                                                                        StaticPacketHelper.Cancel(2, targetId));
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                            }
                                                        }
                                                        else if (Session.CurrentMapInstance.Map.MapTypes.Any(m =>
                                                            m.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                        {
                                                            if (Session.Character.Group == null
                                                                || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                                    .Character.CharacterId))
                                                            {
                                                                PvpHit(
                                                                    new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                        Session, ski.Skill), playerToAttack);
                                                            }
                                                            else
                                                            {
                                                                Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                            }
                                                        }
                                                        else if (Session.CurrentMapInstance.IsPVP)
                                                        {
                                                            if (Session.Character.Group == null
                                                                || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                                    .Character.CharacterId))
                                                            {
                                                                PvpHit(
                                                                    new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                        Session, ski.Skill), playerToAttack);
                                                            }
                                                            else
                                                            {
                                                                Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                        }

                                                        //hit all other monsters
                                                        foreach (ClientSession character in playersInAoeRange)
                                                        {
                                                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                                s.MapTypeId == (short)MapTypeEnum.Act4))
                                                            {
                                                                if (Session.Character.Faction
                                                                    != character.Character.Faction
                                                                    && Session.CurrentMapInstance.Map.MapId != 130
                                                                    && Session.CurrentMapInstance.Map.MapId != 131)
                                                                {
                                                                    PvpHit(
                                                                        new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                            Session, ski.Skill), character);
                                                                }
                                                            }
                                                            else if (Session.CurrentMapInstance.Map.MapTypes.Any(m =>
                                                                m.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                            {
                                                                if (Session.Character.Group == null
                                                                    || !Session.Character.Group.IsMemberOfGroup(
                                                                        character.Character.CharacterId))
                                                                {
                                                                    PvpHit(
                                                                        new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                            Session, ski.Skill), character);
                                                                }
                                                            }
                                                            else if (Session.CurrentMapInstance.IsPVP)
                                                            {
                                                                if (Session.Character.Group == null
                                                                    || !Session.Character.Group.IsMemberOfGroup(
                                                                        character.Character.CharacterId))
                                                                {
                                                                    PvpHit(
                                                                        new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                            Session, ski.Skill), character);
                                                                }

                                                            }
                                                        }

                                                        if (playerToAttack.Character.Hp <= 0)
                                                        {
                                                            Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    ComboDTO skillCombo = ski.Skill.Combos.Find(s => ski.Hit == s.Hit);
                                                    if (skillCombo != null)
                                                    {
                                                        if (ski.Skill.Combos.OrderByDescending(s => s.Hit).First().Hit
                                                            == ski.Hit)
                                                        {
                                                            ski.Hit = 0;
                                                        }

                                                        if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                            s.MapTypeId == (short)MapTypeEnum.Act4))
                                                        {
                                                            if (Session.Character.Faction
                                                                != playerToAttack.Character.Faction)
                                                            {
                                                                if (Session.CurrentMapInstance.Map.MapId != 130
                                                                    && Session.CurrentMapInstance.Map.MapId != 131)
                                                                {
                                                                    PvpHit(
                                                                        new HitRequest(TargetHitType.SingleTargetHitCombo,
                                                                            Session, ski.Skill, skillCombo: skillCombo),
                                                                        playerToAttack);
                                                                }
                                                                else
                                                                {
                                                                    Session.SendPacket(
                                                                        StaticPacketHelper.Cancel(2, targetId));
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                            }
                                                        }
                                                        else if (Session.CurrentMapInstance.Map.MapTypes.Any(m =>
                                                            m.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                        {
                                                            if (Session.Character.Group == null
                                                                || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                                    .Character.CharacterId))
                                                            {
                                                                PvpHit(
                                                                    new HitRequest(TargetHitType.SingleTargetHitCombo,
                                                                        Session, ski.Skill, skillCombo: skillCombo),
                                                                    playerToAttack);
                                                            }
                                                            else
                                                            {
                                                                Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                            }
                                                        }
                                                        else if (Session.CurrentMapInstance.IsPVP)
                                                        {
                                                            if (Session.CurrentMapInstance.MapInstanceId
                                                                != ServerManager.Instance.FamilyArenaInstance.MapInstanceId)
                                                            {
                                                                if (Session.Character.Group == null
                                                                    || !Session.Character.Group.IsMemberOfGroup(
                                                                        playerToAttack
                                                                            .Character.CharacterId))
                                                                {
                                                                    PvpHit(new HitRequest(TargetHitType.SingleTargetHit,
                                                                        Session,
                                                                        ski.Skill), playerToAttack);
                                                                }
                                                                else
                                                                {
                                                                    Session.SendPacket(
                                                                        StaticPacketHelper.Cancel(2, targetId));
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (Session.Character.Faction
                                                                    != playerToAttack.Character.Faction)
                                                                {
                                                                    PvpHit(
                                                                        new HitRequest(TargetHitType.SingleTargetHit,
                                                                            Session, ski.Skill), playerToAttack);
                                                                }
                                                                else
                                                                {
                                                                    Session.SendPacket(
                                                                        StaticPacketHelper.Cancel(2, targetId));
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                            s.MapTypeId == (short)MapTypeEnum.Act4))
                                                        {
                                                            if (Session.Character.Faction
                                                                != playerToAttack.Character.Faction)
                                                            {
                                                                if (Session.CurrentMapInstance.Map.MapId != 130
                                                                    && Session.CurrentMapInstance.Map.MapId != 131)
                                                                {
                                                                    PvpHit(
                                                                        new HitRequest(TargetHitType.SingleTargetHit,
                                                                            Session, ski.Skill), playerToAttack);
                                                                }
                                                                else
                                                                {
                                                                    Session.SendPacket(
                                                                        StaticPacketHelper.Cancel(2, targetId));
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                            }
                                                        }
                                                        else if (Session.CurrentMapInstance.Map.MapTypes.Any(m =>
                                                            m.MapTypeId == (short)MapTypeEnum.PVPMap))
                                                        {
                                                            if (Session.Character.Group == null
                                                                || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                                    .Character.CharacterId))
                                                            {
                                                                PvpHit(
                                                                    new HitRequest(TargetHitType.SingleTargetHit, Session,
                                                                        ski.Skill), playerToAttack);
                                                            }
                                                            else
                                                            {
                                                                Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                            }
                                                        }
                                                        else if (Session.CurrentMapInstance.IsPVP)
                                                        {
                                                            if (Session.Character.Group == null
                                                                || !Session.Character.Group.IsMemberOfGroup(playerToAttack
                                                                    .Character.CharacterId))
                                                            {
                                                                PvpHit(
                                                                    new HitRequest(TargetHitType.SingleTargetHit, Session,
                                                                        ski.Skill), playerToAttack);
                                                            }
                                                            else
                                                            {
                                                                Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                        }
                                                    }
                                                }
                                            }catch(Exception ex)
                                            {

                                            }
                                        }
                                    }
                                    else
                                    {
                                        Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                    }
                                }
                                else
                                {
                                    Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                }
                            }
                            else
                            {
                                MapMonster monsterToAttack = Session.CurrentMapInstance.GetMonster(targetId);
                                if (monsterToAttack != null)
                                {

                                    int Range = ski.Skill.Range;
                                    if (Session.Character.GetBuff(CardType.FearSkill, (byte)AdditionalTypes.FearSkill.AttackRangedIncreased)[0] > 0)
                                    {
                                        Range += Session.Character.GetBuff(CardType.FearSkill, (byte)AdditionalTypes.FearSkill.AttackRangedIncreased)[0];
                                    }
                                    if (Map.GetDistance(
                                            new MapCell
                                            {
                                                X = Session.Character.PositionX,
                                                Y = Session.Character.PositionY
                                            },
                                            new MapCell { X = monsterToAttack.MapX, Y = monsterToAttack.MapY })
                                        <= Range + 5 + monsterToAttack.Monster.BasicArea)
                                    {

                                        if (Session.Character.UseSp && ski.Skill.CastEffect != -1)
                                        {
                                            Session.SendPackets(Session.Character.GenerateQuicklist());
                                        }
#warning check this
                                        monsterToAttack.Monster.BCards.Where(s => s.CastType == 1).ToList()
                                            .ForEach(s => s.ApplyBCards(this));
                                        Session.SendPacket(Session.Character.GenerateStat());
                                        CharacterSkill characterSkillInfo = Session.Character.Skills.FirstOrDefault(s =>
                                            s.Skill.UpgradeSkill == ski.Skill.SkillVNum && s.Skill.Effect > 0
                                                                                        && s.Skill.SkillType == 2);

                                        Session.CurrentMapInstance.Broadcast(StaticPacketHelper.CastOnTarget(
                                            UserType.Player, Session.Character.CharacterId, 3,
                                            monsterToAttack.MapMonsterId, ski.Skill.CastAnimation,
                                            characterSkillInfo?.Skill.CastEffect ?? ski.Skill.CastEffect,
                                            ski.Skill.SkillVNum));
                                        Session.Character.Skills.Where(s => s.Id != ski.Id).ForEach(i => i.Hit = 0);

                                        // Generate scp
                                        if ((DateTime.Now - ski.LastUse).TotalSeconds > 3)
                                        {
                                            ski.Hit = 0;
                                        }
                                        else
                                        {
                                            ski.Hit++;
                                        }

                                        ski.LastUse = DateTime.Now;
                                        if (ski.Skill.CastEffect != 0)
                                        {
                                            int CastTime = ski.Skill.CastTime * 100;
                                            double riduzione = 0;
                                            double riduzionevera = 0;
                                            if (ski.Skill.Type == 0)
                                            {
                                                if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] != 0)
                                                {
                                                    riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                                    riduzionevera = CastTime * riduzione;
                                                }
                                            }
                                            if (ski.Skill.Type == 1)
                                            {
                                                if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.RangedDurationIncreased)[0] != 0)
                                                {
                                                    riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                                    riduzionevera = CastTime * riduzione;
                                                }
                                            }
                                            if (ski.Skill.Type == 2)
                                            {
                                                if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MagicalDurationIncreased)[0] != 0)
                                                {
                                                    riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                                                    riduzionevera = CastTime * riduzione;
                                                }
                                            }
                                            CastTime += (int)riduzionevera;
                                            Thread.Sleep(CastTime);
                                        }

                                        if (ski.Skill.HitType == 3)
                                        {
                                            monsterToAttack.HitQueue.Enqueue(new HitRequest(
                                                TargetHitType.SingleAOETargetHit, Session, ski.Skill,
                                                characterSkillInfo?.Skill.Effect ?? ski.Skill.Effect,
                                                showTargetAnimation: true));

                                            foreach (long id in Session.Character.MTListTargetQueue
                                                .Where(s => s.EntityType == UserType.Monster).Select(s => s.TargetId))
                                            {
                                                MapMonster mon = Session.CurrentMapInstance.GetMonster(id);
                                                if (mon?.CurrentHp > 0)
                                                {
                                                    mon.HitQueue.Enqueue(new HitRequest(
                                                        TargetHitType.SingleAOETargetHit, Session, ski.Skill,
                                                        characterSkillInfo?.Skill.Effect ?? ski.Skill.Effect));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ski.Skill.TargetRange != 0) // check if we will hit mutltiple targets
                                            {
                                                ComboDTO skillCombo = ski.Skill.Combos.Find(s => ski.Hit == s.Hit);
                                                if (skillCombo != null)
                                                {
                                                    if (ski.Skill.Combos.OrderByDescending(s => s.Hit).First().Hit
                                                        == ski.Hit)
                                                    {
                                                        ski.Hit = 0;
                                                    }

                                                    List<MapMonster> monstersInAoeRange = Session.CurrentMapInstance?
                                                        .GetListMonsterInRange(monsterToAttack.MapX,
                                                            monsterToAttack.MapY, ski.Skill.TargetRange).ToList();
                                                    if (monstersInAoeRange.Count != 0)
                                                    {
                                                        foreach (MapMonster mon in monstersInAoeRange)
                                                        {
                                                            mon.HitQueue.Enqueue(
                                                                new HitRequest(TargetHitType.SingleTargetHitCombo,
                                                                    Session, ski.Skill, skillCombo: skillCombo));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                    }

                                                    if (!monsterToAttack.IsAlive)
                                                    {
                                                        Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                    }
                                                }
                                                else
                                                {
                                                    List<MapMonster> monstersInAoeRange = Session.CurrentMapInstance?
                                                                                              .GetListMonsterInRange(
                                                                                                  monsterToAttack.MapX,
                                                                                                  monsterToAttack.MapY,
                                                                                                  ski.Skill.TargetRange)
                                                                                              ?.ToList();

                                                    //hit the targetted monster
                                                    monsterToAttack.HitQueue.Enqueue(
                                                        new HitRequest(TargetHitType.SingleAOETargetHit, Session,
                                                            ski.Skill,
                                                            characterSkillInfo?.Skill.Effect ?? ski.Skill.Effect,
                                                            showTargetAnimation: true));

                                                    //hit all other monsters
                                                    if (monstersInAoeRange != null && monstersInAoeRange.Count != 0)
                                                    {
                                                        foreach (MapMonster mon in monstersInAoeRange.Where(m =>
                                                            m.MapMonsterId != monsterToAttack.MapMonsterId)
                                                        ) //exclude targetted monster
                                                        {
                                                            mon.HitQueue.Enqueue(
                                                                new HitRequest(TargetHitType.SingleAOETargetHit,
                                                                    Session, ski.Skill,
                                                                    characterSkillInfo?.Skill.Effect ??
                                                                    ski.Skill.Effect));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                    }

                                                    if (!monsterToAttack.IsAlive)
                                                    {
                                                        Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                ComboDTO skillCombo = ski.Skill.Combos.Find(s => ski.Hit == s.Hit);
                                                if (skillCombo != null)
                                                {
                                                    if (ski.Skill.Combos.OrderByDescending(s => s.Hit).First().Hit
                                                        == ski.Hit)
                                                    {
                                                        ski.Hit = 0;
                                                    }

                                                    monsterToAttack.HitQueue.Enqueue(
                                                        new HitRequest(TargetHitType.SingleTargetHitCombo, Session,
                                                            ski.Skill, skillCombo: skillCombo));
                                                }
                                                else
                                                {
                                                    monsterToAttack.HitQueue.Enqueue(
                                                        new HitRequest(TargetHitType.SingleTargetHit, Session,
                                                            ski.Skill));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                    }
                                }
                                else
                                {
                                    Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                                }
                            }

                            if (ski.Skill.HitType == 3)
                            {
                                Session.Character.MTListTargetQueue.Clear();
                            }
                        }
                        else
                        {
                            Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                        }

                        if (ski.Skill.UpgradeSkill == 3 && ski.Skill.SkillType == 1)
                        {
                            Session.SendPacket(
                                StaticPacketHelper.SkillResetWithCoolDown(castingId, Cooldown));
                        }
                        Session.SendPacketAfter(StaticPacketHelper.SkillReset(castingId), Cooldown * 100);
                    }
                    else
                    {
                        Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
                        Session.SendPacket(
                            Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MP"), 10));
                    }
                }
            }
            else
            {
                Session.SendPacket(StaticPacketHelper.Cancel(2, targetId));
            }

            if ((castingId != 0 && castingId < 11 && shouldCancel) || Session.Character.SkillComboCount > 7)
            {
                Session.SendPackets(Session.Character.GenerateQuicklist());
                //Session.SendPacket("mslot 0 -1");
            }

            Session.Character.LastSkillUse = DateTime.Now;
        }

        private void ZoneHit(int castingId, short x, short y)
        {
            List<CharacterSkill> skills = Session.Character.UseSp
                ? Session.Character.SkillsSp.GetAllItems()
                : Session.Character.Skills.GetAllItems();
            CharacterSkill characterSkill = skills?.Find(s => s.Skill?.CastId == castingId);
            if (characterSkill == null || !Session.Character.WeaponLoaded(characterSkill)
                                       || !Session.HasCurrentMapInstance
                                       || !characterSkill.CanBeUsed(Session))
            {
                Session.SendPacket(StaticPacketHelper.Cancel(2));
                return;
            }
                if (Session.Character.Mp >= characterSkill.Skill.MpCost && Session.HasCurrentMapInstance)
                {
                    Session.CurrentMapInstance.Broadcast(
                        $"ct_n 1 {Session.Character.CharacterId} 3 -1 {characterSkill.Skill.CastAnimation}" +
                        $" {characterSkill.Skill.CastEffect} {characterSkill.Skill.SkillVNum}");
                    characterSkill.LastUse = DateTime.Now;
                    if (!Session.Character.HasGodMode)
                    {
                        Session.Character.Mp -= characterSkill.Skill.MpCost;
                    }

                    Session.SendPacket(Session.Character.GenerateStat());
                    characterSkill.LastUse = DateTime.Now;
                int CastTime = characterSkill.Skill.CastTime * 100;
                double riduzione = 0;
                double riduzionevera = 0;
                if (characterSkill.Skill.Type == 0)
                {
                    if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] != 0)
                    {
                        riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                        riduzionevera = CastTime * riduzione;
                    }
                }
                if (characterSkill.Skill.Type == 1)
                {
                    if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.RangedDurationIncreased)[0] != 0)
                    {
                        riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                        riduzionevera = CastTime * riduzione;
                    }
                }
                if (characterSkill.Skill.Type == 2)
                {
                    if (Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MagicalDurationIncreased)[0] != 0)
                    {
                        riduzione = Session.Character.GetBuff(CardType.JumpBackPush, (byte)AdditionalTypes.JumpBackPush.MeleeDurationIncreased)[0] / 100;
                        riduzionevera = CastTime * riduzione;
                    }
                }
                CastTime += (int)riduzionevera;
                Observable.Timer(TimeSpan.FromMilliseconds(CastTime)).Subscribe(o =>
                    {
                        Session.Character.LastSkillUse = DateTime.Now;

                        Session.CurrentMapInstance.Broadcast(
                            $"bs 1 {Session.Character.CharacterId} {x} {y} {characterSkill.Skill.SkillVNum}" +
                            $" {characterSkill.Skill.Cooldown} {characterSkill.Skill.AttackAnimation}" +
                            $" {characterSkill.Skill.Effect} 0 0 1 1 0 0 0");
                        if (characterSkill.Skill.BCards.Where(s => s.Type == 69 && s.SubType == 1).Count() > 0)
                        {
                            characterSkill.Skill?.BCards.Where(s => s.Type.Equals((byte)CardType.FalconSkill)).ToList()
                                .ForEach(s => s.ApplyBCards(Session.Character, Session.Character, mapx: x, mapy: y));
                        }
                        else if (characterSkill.Skill.BCards.Where(s => s.Type == 76 && s.SubType == 1).Count() > 0)
                        {
                            characterSkill.Skill?.BCards.ForEach(s => s.ApplyBCards(Session.Character, Session.Character, mapx: x, mapy: y));
                        }
                        else
                        {

                            foreach (long id in Session.Character.MTListTargetQueue
                                .Where(s => s.EntityType == UserType.Monster).Select(s => s.TargetId))
                            {
                                MapMonster mon = Session.CurrentMapInstance.GetMonster(id);
                                if (mon?.CurrentHp > 0)
                                {
                                    mon.HitQueue.Enqueue(new HitRequest(TargetHitType.ZoneHit, Session,
                                        characterSkill.Skill, characterSkill.Skill.Effect, x, y));
                                }
                            }

                            foreach (long id in Session.Character.MTListTargetQueue
                                .Where(s => s.EntityType == UserType.Player).Select(s => s.TargetId))
                            {
                                ClientSession character = ServerManager.Instance.GetSessionByCharacterId(id);
                                if (character != null && character.CurrentMapInstance == Session.CurrentMapInstance
                                                      && character.Character.CharacterId != Session.Character.CharacterId)
                                {
                                    if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                        s.MapTypeId == (short)MapTypeEnum.Act4))
                                    {
                                        if (Session.Character.Faction != character.Character.Faction
                                            && Session.CurrentMapInstance.Map.MapId != 130
                                            && Session.CurrentMapInstance.Map.MapId != 131)
                                        {
                                            PvpHit(
                                                new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                character);
                                        }
                                    }
                                    else if (Session.CurrentMapInstance.Map.MapId == 2106)
                                    {

                                        if (Session.Character.Family == null)
                                        {
                                            if (character.Character.Family != null)
                                            {
                                                PvpHit(
                                                    new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                    character);
                                            }
                                        }
                                        else
                                        {
                                            if (character.Character.Family == null)
                                            {
                                                PvpHit(
                                                    new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                    character);
                                            }
                                            else if (Session.Character.Family != character.Character.Family)
                                            {
                                                PvpHit(
                                                    new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                    character);
                                            }
                                        }
                                    }
                                    else if (Session.CurrentMapInstance.Map.MapTypes.Any(m =>
                                        m.MapTypeId == (short)MapTypeEnum.PVPMap))
                                    {
                                        if (Session.Character.Group == null
                                            || !Session.Character.Group.IsMemberOfGroup(character.Character.CharacterId))
                                        {
                                            PvpHit(
                                                new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                character);
                                        }
                                    }
                                    else if (Session.CurrentMapInstance.IsPVP)
                                    {
                                        if (Session.Character.Group == null
                                            || !Session.Character.Group.IsMemberOfGroup(character.Character.CharacterId))
                                        {
                                            PvpHit(
                                                new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                character);
                                        }
                                    }
                                }
                            }

                            if (Session.Character.MTListTargetQueue.Count == 0)
                            {
                                IEnumerable<MapMonster> monstersInRange = Session.CurrentMapInstance?.GetListMonsterInRange(x, y, characterSkill.Skill.TargetRange).ToList();
                                if (monstersInRange != null)
                                {
                                    foreach (MapMonster mon in monstersInRange.Where(s => s.CurrentHp > 0))
                                    {
                                        if (mon?.CurrentHp > 0)
                                        {
                                            mon.HitQueue.Enqueue(new HitRequest(TargetHitType.ZoneHit, Session,
                                                characterSkill.Skill, characterSkill.Skill.Effect, x, y));
                                        }
                                    }

                                    IEnumerable<ClientSession> playersInAoeRange = ServerManager.Instance.Sessions.Where(s => s.CurrentMapInstance == Session.CurrentMapInstance && s.Character.CharacterId != Session.Character.CharacterId && s.Character.IsInRange(x, y, characterSkill.Skill.TargetRange)).ToList();
                                    foreach (long id in playersInAoeRange.Select(s => s.Character.CharacterId))
                                    {
                                        ClientSession character = ServerManager.Instance.GetSessionByCharacterId(id);
                                        if (character != null && character.CurrentMapInstance == Session.CurrentMapInstance
                                                              && character.Character.CharacterId != Session.Character.CharacterId)
                                        {
                                            if (Session.CurrentMapInstance.Map.MapTypes.Any(s =>
                                                s.MapTypeId == (short)MapTypeEnum.Act4))
                                            {
                                                if (Session.Character.Faction != character.Character.Faction
                                                    && Session.CurrentMapInstance.Map.MapId != 130
                                                    && Session.CurrentMapInstance.Map.MapId != 131)
                                                {
                                                    PvpHit(
                                                        new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                        character);
                                                }
                                            }
                                            else if (Session.CurrentMapInstance.Map.MapId == 2106)
                                            {

                                                if (Session.Character.Family == null)
                                                {
                                                    if (character.Character.Family != null)
                                                    {
                                                        PvpHit(
                                                            new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                            character);
                                                    }
                                                }
                                                else
                                                {
                                                    if (character.Character.Family == null)
                                                    {
                                                        PvpHit(
                                                            new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                            character);
                                                    }
                                                    else if (Session.Character.Family != character.Character.Family)
                                                    {
                                                        PvpHit(
                                                            new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                            character);
                                                    }
                                                }
                                                if (Session.Character.Family != character.Character.Family)
                                                {
                                                    PvpHit(
                                                        new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                        character);
                                                }
                                            }
                                            else if (Session.CurrentMapInstance.Map.MapTypes.Any(m =>
                                                m.MapTypeId == (short)MapTypeEnum.PVPMap))
                                            {
                                                if (Session.Character.Group == null
                                                    || !Session.Character.Group.IsMemberOfGroup(character.Character.CharacterId))
                                                {
                                                    PvpHit(
                                                        new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                        character);
                                                }
                                            }
                                            else if (Session.CurrentMapInstance.IsPVP)
                                            {
                                                if (Session.Character.Group == null
                                                    || !Session.Character.Group.IsMemberOfGroup(character.Character.CharacterId))
                                                {
                                                    PvpHit(
                                                        new HitRequest(TargetHitType.ZoneHit, Session, characterSkill.Skill, x, y),
                                                        character);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            Session.Character.MTListTargetQueue.Clear();
                            
                        }
                    });
                    Observable.Timer(TimeSpan.FromMilliseconds(characterSkill.Skill.Cooldown * 100))
                        .Subscribe(o => Session.SendPacket(StaticPacketHelper.SkillReset(castingId)));
                }
                else
                {
                    Session.SendPacket(
                        Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("NOT_ENOUGH_MP"), 10));
                    Session.SendPacket(StaticPacketHelper.Cancel(2));
                }
            
        }

        #endregion
    }
}