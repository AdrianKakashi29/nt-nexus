/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using log4net;
using OpenNos.Core;
using OpenNos.DAL;
using OpenNos.DAL.EF.Helpers;
using OpenNos.Data;
using OpenNos.GameObject;
using OpenNos.Handler;
using OpenNos.Master.Library.Client;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;

namespace OpenNos.Login
{
    public static class Program
    {
        #region Members

        private static bool _isDebug;

        #endregion

        #region Methods

        public static void Main(string[] args)
        {
            try
            {
                CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo("en-US");

                string IPAdress = ConfigurationManager.AppSettings["IPAddress"];
                int port = Convert.ToInt32(ConfigurationManager.AppSettings["LoginPort"]);

                Console.Title = $"NosTale - LoginServer {IPAdress}:{port}";

                Logger.InitializeLogger(LogManager.GetLogger(typeof(Program)));

                if (CommunicationServiceClient.Instance.Authenticate(ConfigurationManager.AppSettings["MasterAuthKey"]))
                {
                    Logger.Info(Language.Instance.GetMessageFromKey("API_INITIALIZED"));
                }

                if (!DataAccessHelper.Initialize())
                {
                    Console.ReadLine();
                    return;
                }

                Logger.Info(Language.Instance.GetMessageFromKey("CONFIG_LOADED"));

                PacketFactory.Initialize<WalkPacket>();

                NetworkManager<LoginCryptography> networkManager = new NetworkManager<LoginCryptography>(IPAdress, port, typeof(LoginPacketHandler), typeof(LoginCryptography), false);
            }
            catch (Exception ex)
            {
                Logger.Error("General Error", ex);
                Console.ReadLine();
            }
            
        }

        #endregion
    }
}